# Known issues

Updated: 2022-11-28

#### jquery and other system packages

On Debian/Ubuntu, installing system-wide JavaScript packages like jquery seems not to be enough, at least with apache2: `sudo a2enconf javascript-common` (followed by `sudo service apache2 reload`) must be run on the server to enable the use of `/javascript` virtual directory.

#### Missing ffmpeg

The scanner is able to detect if `ffmpeg` is installed; if `ffmpeg` isn't installed, audios and videos will not be "seen", and the user will be warned. If the user installs it afterwards, the videos will be see without any problem.

Besides that, old `ffmpeg`'s could not be able to transcode some audio/video. Be sure you have the last version installed.

#### Video/audio date and time

The scanner can get audio/video creation date-time in various ways:
- from file modification time (legacy, default); be sure you do not modify the audio/video; or, if you modify it, touch it to desidered date-time
- from file name: the scanner extract the date/time from it, looking for the format YYYYMMDDHHMMSS, with any punctuation character allowed as a separator between the fields
- from embedded metadata: NOT RECOMMENDED: easily there will be errors with time zone interpretation. Use it only if you are sure that all the audios/videos has local dates as exif dates

`audio_video_date_look_for_preference` option instructs the scanner about what to look for and what order to do it.

#### Pillow bug

If you are using a very old pillow package (pillow 5.0, 2018, still is problematic) a pillow bug makes that exif data cannot not be read for some photos which have two APP1 (0xFFE1/65505) aka Exif segments. A fix is available, see https://github.com/python-pillow/Pillow/issues/2944#issuecomment-356316273: a line of code is to be added in a python source file. Or, simpler, update pillow to a more recent version.

#### Oriental languages

Oriental languages like chinese do not define easily word separation, so whole word search could thrown no results, try inside word search.

#### Slow web site

If there are many images in the album (say > 1000) you may experience slowlyness in the web site. This is expecially true when the photos are geotagged.

You can speed up the site enabling `json` compression in your web server. On `debian`'s `apache2` this can be easily achieved adding the following line in the proper place to the file `/etc/apache2/mods-enabled/deflate.conf`:

```apache
    AddOutputFilterByType DEFLATE application/json
```

For other web servers/distributions a solution can be found easily.

#### Inexplicable malfunctions in web page due to browser cache

Sometimes something inexplicable happens in javascript, and a common reason is the cache: some json files aren't still in the cache, and the correct ones aren't used. Cleaning the cache solves these problems.

The cache can be the browser's one or the web server one.

#### Privacy concern with password implementation

Passwords can be used to protect content, but carefully read the [explication and warnings](Authentication.md).

#### Album download limitations

Downloading a whole album is permitted only if the total content is less than a given amount (now, 500MB), because the current code needs to generated the zip file in ram before saving it to the disk.

Actually, the limit is browser and ram dependent, so the user can get a notice of preparing the download, but it doesn't ever get an end.

A different approach should be used, saving the zip file while is created.

#### Minified scripts

If you run into strange bugs like photos not displaying, check that this is not caused by minified JavaScript scripts. Set `debug_js = true` into `/etc/myphotoshare/myphotoshare.conf` and run the scanner (or patch `cache/options.json`).

If that's the case, try another minifier. `uglifyjs` is known to create problems; `terser` or `jsmin3` are good options.
