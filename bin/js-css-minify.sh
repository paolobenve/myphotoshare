#!/bin/bash

PROJECT_DIR="$(dirname $(realpath $0))/.."
DEFAULT_CONF="$PROJECT_DIR/myphotoshare.conf.defaults"
CONF="$1"

if [ -z "$CONF" ]; then
	# The script would be better launched with the user's config file
	( >&2 echo )
	( >&2 echo "Usage: ./$0 MYPHOTOSHARE_CONFIG_FILE" )
	( >&2 echo )
	( >&2 echo "config file not provided: DO NOT DO THIS ON PRODUCTION INSTALLATIONS" )
	( >&2 echo "Keeping on using default options in myphotoshare.conf.defaults" )
	CONF=$DEFAULT_CONF
elif [ ! -f "$CONF" ]; then
	( >&2 echo )
	( >&2 echo "Error: file '$CONF' does not exist" )
	( >&2 echo )
	( >&2 echo "Quitting" )
	exit 1
fi

# If can't find default config, try from parent directory in case the script
# is run from 'bin' directory.
if [ ! -e "$DEFAULT_CONF" ]; then
	PROJECT_DIR="$(realpath "$PROJECT_DIR/..")"
	DEFAULT_CONF="$PROJECT_DIR/myphotoshare.conf.defaults"
fi
if [ ! -e "$DEFAULT_CONF" ]; then
	( >&2 echo )
	( >&2 echo "Can't find default config file 'myphotoshare.conf.defaults'." )
	( >&2 echo "Run $0 from MyPhotoShare root directory." )
	( >&2 echo )
	( >&2 echo "Quitting" )
	exit 1
fi

# Do we use the system JavaScript libraries? Set the variable $OS_JS if that's the case.
OS_JS="$(sed -nr 's/^\s*use_system_js_libraries\s*=\s*(\w+)\s*.*$/\1/p' $CONF)"
DEFAULT_OS_JS="$(sed -nr 's/^\s*use_system_js_libraries\s*=\s*(\w+)\s*.*$/\1/p' $DEFAULT_CONF)"
OS_JS=${OS_JS:-$DEFAULT_OS_JS}
if [ "$OS_JS" = "true" ] || [ "$OS_JS" = "1" ]; then
	OS_JS="true"
else
	unset OS_JS
fi

# Check that js and css directories exist and are writable
if [ ! -d "$PROJECT_DIR/web/js" ] || [ ! -w "$PROJECT_DIR/web/js" ]; then
	( >&2 echo "$PROJECT_DIR/web/js is not a writable directory with MyPhotoShare js files" )
	( >&2 echo "Run $0 from MyPhotoShare root directory." )
	( >&2 echo )
	( >&2 echo "Quitting" )
	exit 1
fi
if [ ! -d "$PROJECT_DIR/web/css" ] || [ ! -w "$PROJECT_DIR/web/css" ]; then
	( >&2 echo "$PROJECT_DIR/web/css is not a writable directory with MyPhotoShare css files" )
	( >&2 echo "Run $0 from MyPhotoShare root directory." )
	( >&2 echo )
	( >&2 echo "Quitting" )
	exit 1
fi

# Is save_data option set to true? if that's the case custom fonts won't be used.
SAVE_DATA="$(sed -nr 's/^\s*save_data\s*=\s*(\w+)\s*.*$/\1/p' $CONF)"
DEFAULT_SAVE_DATA="$(sed -nr 's/^\s*save_data\s*=\s*(\w+)\s*.*$/\1/p' $DEFAULT_CONF)"
SAVE_DATA=${SAVE_DATA:-$DEFAULT_SAVE_DATA}
if [ "$SAVE_DATA" = "true" ] || [ "$SAVE_DATA" = "1" ]; then
	SAVE_DATA="true"
	echo
	echo "save_data option is set to true, custom fonts won't be included in minified css file"
else
	SAVE_DATA="false"
fi


# Parse which minifiers to use from configuration file
MINIFY_JS="$(sed -nr 's/^\s*js_minifier\s*=\s*(\w+)\s*.*$/\1/p' $CONF)"
DEFAULT_MINIFY_JS="$(sed -nr 's/^\s*js_minifier\s*=\s*(\w+)\s*.*$/\1/p' $DEFAULT_CONF)"
MINIFY_JS=${MINIFY_JS:-$DEFAULT_MINIFY_JS}

MINIFY_CSS="$(sed -nr 's/^\s*css_minifier\s*=\s*(\w+)\s*.*$/\1/p' $CONF)"
DEFAULT_MINIFY_CSS="$(sed -nr 's/^\s*css_minifier\s*=\s*(\w+)\s*.*$/\1/p' $DEFAULT_CONF)"
MINIFY_CSS=${MINIFY_CSS:-$DEFAULT_MINIFY_CSS}

echo
echo "$MINIFY_CSS" set as CSS minifier in $CONF
echo "$MINIFY_JS" set as JS minifier in $CONF
if [ "$OS_JS" ]; then
	echo "We'll use system JavaScript libraries (/usr/share/javascript/) when found"
else
	echo "We'll use JavaScript libraries provided by MyPhotoShare"
fi

unixseconds=$(date +%s)

# check what terser command is installed
TERSER_COMMAND=""
uglifyjs.terser -V > /dev/null 2>&1
if [ $? -eq 0 ]; then
	TERSER_COMMAND="uglifyjs.terser"
else
	terser -V > /dev/null 2>&1
	if [ $? -eq 0 ]; then
		TERSER_COMMAND="terser"
	fi
fi

# Check that requested local minifiers are installed
case $MINIFY_JS in
	web_service)
		curl https://javascript-minifier.com/ > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			( >&2 echo "'curl' not installed or 'https://javascript-minifier.com/' down" )
			( >&2 echo "Aborting..." )
			exit 1
		fi
	;;
	jsmin3)
		python3 -c 'import jsmin' > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			( >&2 echo "'jsmin' for Python3 is not installed. Look for package 'python3-jsmin' or 'https://github.com/tikitu/jsmin'" )
			( >&2 echo "Aborting..." )
			exit 1
		fi
	;;
	terser | mangler)
		if [ "$TERSER_COMMAND" == "" ]; then
			( >&2 echo "neither 'uglifyjs.terser' nor 'terser' is installed. Look for package 'uglifyjs.terser' or 'terser', or visit 'https://terser.org/'" )
			( >&2 echo "Aborting..." )
			exit 1
		fi
esac

case $MINIFY_CSS in
	web_service)
		curl https://cssminifier.com/ > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			( >&2 echo "'curl' not installed or 'https://cssminifier.com/' down" )
			( >&2 echo "Aborting..." )
			exit 1
		fi
	;;
	cssmin)
		cssmin -h > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			( >&2 echo "'cssmin' is not installed. Look for package 'cssmin' or 'https://github.com/zacharyvoase/cssmin'" )
			( >&2 echo "Aborting..." )
			exit 1
		fi
	;;
	rcssmin3)
		python3 -c 'import rcssmin' > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			( >&2 echo "'rcssmin' for Python3 is not installed. Look for package 'python3-rcssmin' or 'https://github.com/ndparker/rcssmin'" )
			( >&2 echo "Aborting..." )
			exit 1
		fi
	;;
esac

# minify all .js-files
cd "$PROJECT_DIR/web/js"
echo
echo "== Minifying js files in $PROJECT_DIR/web/js directory with $MINIFY_JS =="
echo
CAT_LIST_1=""
CAT_LIST_2=""
CAT_LIST_3=""
CAT_LIST_MINIFIED_1=""
CAT_LIST_MINIFIED_2=""
CAT_LIST_MINIFIED_3=""

rm -f *.min.js

if [ $? -ne 0 ]; then
	( >&2 echo "Can't write files. Aborting..." )
	exit 1
fi
ls -1 *.js | grep -Ev "min.js$" | while read js_file; do
	minified_js_file="${js_file%.*}.min.js"

	# Check if minified versions are provided by the system (Debian/Ubuntu)
	case $js_file in
		000-jquery-*)
			if [ -n "$OS_JS" ] && [ -e /usr/share/javascript/jquery/jquery.min.js ]
			then
				echo "... Found system jquery; using it in 'scripts.1.min.js'"
				cat /usr/share/javascript/jquery/jquery.min.js >> scripts.1.min.js
				cat /usr/share/javascript/jquery/jquery.js >> scripts.not.1.min.js
				continue
			fi
			echo "minifying $js_file in 'scripts.1.min.js'"
		;;

		003-mousewheel*)
			if [ -n "$OS_JS" ] && [ -e /usr/share/javascript/jquery-mousewheel/jquery.mousewheel.min.js ]
			then
				echo "... Found system jquery-mousewheel; using it in 'scripts.1.min.js'"
				cat /usr/share/javascript/jquery-mousewheel/jquery.mousewheel.min.js >> scripts.1.min.js
				cat /usr/share/javascript/jquery-mousewheel/jquery.mousewheel.js >> scripts.not.1.min.js
				continue
			fi
			echo "minifying $js_file in 'scripts.1.min.js'"
		;;

		004-fullscreen*)
			# Currently, there is no minified library in the Debian package... So this test is
			# skipped and will be used in future Debian versions
			if [ -n "$OS_JS" ] && [ -e /usr/share/javascript/jquery-fullscreen/jquery.fullscreen.min.js ]
			then
				echo "... Found system jquery-fullscreen; using it in 'scripts.1.min.js'"
				cat /usr/share/javascript/jquery-fullscreen/jquery.fullscreen.min.js >> scripts.1.min.js
				cat /usr/share/javascript/jquery-fullscreen/jquery.fullscreen.js >> scripts.not.1.min.js
				continue
			fi
			echo "minifying $js_file in 'scripts.1.min.js'"
		;;

		005-modernizr*)
			if [ -n "$OS_JS" ] && [ -e /usr/share/javascript/modernizr/modernizr.min.js ]
			then
				echo "... Found system modernizr; using it in 'scripts.1.min.js'"
				cat /usr/share/javascript/modernizr/modernizr.min.js >> scripts.1.min.js
				cat /usr/share/javascript/modernizr/modernizr.js >> scripts.not.1.min.js
				continue
			fi
			echo "minifying $js_file in 'scripts.1.min.js'"
		;;

		007-jquery-lazyload*)
			if [ -n "$OS_JS" ] && [ -e /usr/share/javascript/jquery-lazyload/jquery.lazyload.min.js ]
			then
				echo "... Found system jquery-lazyload; using it in 'scripts.1.min.js'"
				cat /usr/share/javascript/jquery-lazyload/jquery.lazyload.min.js >> scripts.1.min.js
				cat /usr/share/javascript/jquery-lazyload/jquery.lazyload.js >> scripts.not.1.min.js
				continue
			fi
			echo "minifying $js_file in 'scripts.1.min.js'"
		;;

		006-jquery-touchswipe.js | 008-leaflet.js | 010-social.js | 011-jszip.js | 012-jszip-utils.js | 013-md5.js | 014-file-saver.js | 015-jquery-mark.js | 033-utilities-bis.js | 035-pinch-swipe.js | 038-top-functions-bis.js | 041-album-methods-bis.js | 043-single-media-methods-bis.js | 045-positions-and-media-methods-bis.js | 047-selection-methods-bis.js | 048-map-methods-bis.js | 050-display-bis.js)
			echo "minifying $js_file in 'scripts.2.min.js'"
		;;

		009-leaflet-prunecluster.js | 037-map.js)
			echo "minifying $js_file in 'scripts.3.min.js'"
		;;

		*)
			echo "minifying $js_file in 'scripts.1.min.js'"
		;;
	esac

	case $MINIFY_JS in
		web_service)
			curl -X POST -s --data-urlencode "input@$js_file" https://javascript-minifier.com/raw > $minified_js_file
		;;

		jsmin3)
			python3 -m jsmin $js_file > $minified_js_file
		;;

		terser)
			"$TERSER_COMMAND" -o $minified_js_file $js_file
		;;

		mangler)
			"$TERSER_COMMAND" -o $minified_js_file $js_file -m
		;;

		*)
			echo "Unsupported Javascript minifier: $MINIFY_JS. Check option 'js_minifier' in '$CONF'"
			echo "Doing nothing on file $js_file"
			minified_js_file=$js_file
	esac

	case $js_file in
		006-jquery-touchswipe.js | 008-leaflet.js | 010-social.js | 011-jszip.js | 012-jszip-utils.js | 013-md5.js | 014-file-saver.js | 015-jquery-mark.js | 033-utilities-bis.js | 035-pinch-swipe.js | 038-top-functions-bis.js | 041-album-methods-bis.js | 043-single-media-methods-bis.js | 045-positions-and-media-methods-bis.js | 047-selection-methods-bis.js | 048-map-methods-bis.js | 050-display-bis.js)
			cat $minified_js_file >> scripts.2.min.js
			cat $js_file >> scripts.not.2.min.js
		;;

		009-leaflet-prunecluster.js | 037-map.js)
			cat $minified_js_file >> scripts.3.min.js
			cat $js_file >> scripts.not.3.min.js
		;;

		*)
			cat $minified_js_file >> scripts.1.min.js
			cat $js_file >> scripts.not.1.min.js
		;;
	esac
done


# minify all .css-files
cd "$PROJECT_DIR/web/css"
echo
echo "== Minifying css files in $PROJECT_DIR/web/css directory with $MINIFY_CSS =="
echo
rm -f *.min.css
if [ $? -ne 0 ]; then
	( >&2 echo "Can't write files. Aborting..." )
	exit 1
fi

ls -1 *.css | grep -Ev "min.css$" | while read css_file; do
	minified_css_file="${css_file%.*}.min.css"

	case $css_file in
		000-controls.css | 002-mobile.css)
			echo "minifying $css_file in 'styles.1.min.css'"
		;;

		001-fonts.css)
			if [ "${SAVE_DATA}" = "false" ]
			then
				echo "minifying $css_file in 'styles.2.min.css'"
			fi
		;;

		*)
			echo "minifying $css_file in 'styles.2.min.css'"
		;;
	esac

	case $MINIFY_CSS in
		web_service)
			curl -X POST -s --data-urlencode "input@$css_file" https://cssminifier.com/raw > $minified_css_file
		;;

		cssmin)
			cssmin < $css_file > $minified_css_file
		;;

		rcssmin3)
			python3 -m rcssmin < $css_file > $minified_css_file
		;;

		*)
			( >&2 echo "Unsupported CSS minifier: $MINIFY_CSS. Check option 'css_minifier' in '$CONF'" )
			( >&2 echo "Doing nothing on file $css_file" )
	esac

	case $css_file in
		000-controls.css | 002-mobile.css)
			# this file will be used by index.php in no css_debug mode and by index.html
			cat $minified_css_file >> styles.1.min.css
		;;

		001-fonts.css)
			if [ "${SAVE_DATA}" = "false" ]
			then
				# the following file will be used in index.html
				cat $minified_css_file >> styles.2.min.css
				# the following file will be used in index.php
				cat $minified_css_file >> styles.no_save_data.2.min.css
			fi
		;;

		*)
			cat $minified_css_file >> styles.2.min.css
			# the following file will be used by index.php
			cat $minified_css_file >> styles.save_data.2.min.css
			# this file will be used by index.php, too
			cat $minified_css_file >> styles.no_save_data.2.min.css
		;;
	esac
done

echo
echo "Completed in $((`date +%s` - $unixseconds)) s."
