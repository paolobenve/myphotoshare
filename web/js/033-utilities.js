(function() {
	/* constructor */
	function Utilities() {
	}

	Utilities.prototype.readPostData = function() {
		if (postData.guessedPasswordsMd5) {
			env.guessedPasswordsMd5 = postData.guessedPasswordsMd5.split('-');
			env.guessedPasswordCodes = postData.guessedPasswordCodes.split('-');
			delete postData.guessedPasswordsMd5;
		}
		env.map = JSON.parse(postData.mapClick);
		env.map.clickHistory = Utilities.decodeMapClickHistory(postData.mapClickHistory);

		let stringifiedPackedAlbum, packedAlbum, albumName, uncompressedAlbum;
		let index = 0;
		let indexS = index.toString();

		while (postData.hasOwnProperty("albumName_" + indexS)) {
			albumName = postData["albumName_" + indexS];
			stringifiedPackedAlbum = postData["stringifiedPackedAlbum_" + indexS];
			if (postData["typeOfPackedAlbum_" + indexS] === "array") {
				packedAlbum = stringifiedPackedAlbum.split(',').map(x => parseInt(x, 10));
			} else {
				packedAlbum = stringifiedPackedAlbum;
			}
			uncompressedAlbum = new Album(JSON.parse(lzwCompress.unpack(packedAlbum)));
			if (albumName === "mapAlbum") {
				env.mapAlbum = uncompressedAlbum;
			} else if (albumName === "selectionAlbum") {
				env.selectionAlbum = uncompressedAlbum;
			} else if (albumName === "searchAlbum") {
				env.searchAlbum = uncompressedAlbum;
			}
			index ++;
			indexS = index.toString();
		}

		if (postData.currentAlbumIs === "mapAlbum")
			env.currentAlbum = env.mapAlbum;
		else if (postData.currentAlbumIs === "selectionAlbum")
			env.currentAlbum = env.selectionAlbum;
		else if (postData.currentAlbumIs === "searchAlbum")
			env.currentAlbum = env.searchAlbum;

		// invalidate the variable so that it's not used any more
		postData = null;
	};

	Utilities.decodeMapClickHistory = function(cacheBaseTrailer) {
		if (! cacheBaseTrailer)
			return null;

		let trailerElements = cacheBaseTrailer.split(",");
		let mapClickHistory = [];

		trailerElements.forEach(
			function(trailerElement) {
				let trailerElementArray = trailerElement.split("");
				let mapClickHistoryElement = {
					ctrlKey: parseInt(trailerElementArray.shift()),
					shiftKey: parseInt(trailerElementArray.shift()),
					zoom: parseInt(trailerElementArray.shift()) * 10 + parseInt(trailerElementArray.shift()),
				};
				let numberOfDecimals = parseInt(trailerElementArray.shift()) * 10 + parseInt(trailerElementArray.shift());

				let multiplier = Math.pow(10, numberOfDecimals);

				let modifiedTrailierElement = trailerElementArray.join("");
				let wIndex = modifiedTrailierElement.indexOf("W");
				let eIndex = modifiedTrailierElement.indexOf("E");
				let centerStringEnd;
				if (wIndex === -1)
					centerStringEnd = eIndex;
				else if (eIndex === -1)
					centerStringEnd = wIndex;
				else
					centerStringEnd = Math.min(wIndex, eIndex);
				let centerString = modifiedTrailierElement.substr(0, centerStringEnd + 1);

				let nIndex = centerString.indexOf("N");
				let sIndex = centerString.indexOf("S");
				let latEnd;
				if (nIndex === -1)
					latEnd = sIndex;
				else if (sIndex === -1)
					latEnd = nIndex;
				else
					latEnd = Math.min(nIndex, sIndex);
				let centerLat = centerString.substr(0, latEnd + 1);
				if (centerLat.substr(-1) === "N")
					centerLat = centerLat.substring(0, centerLat.length - 1);
				else
					centerLat = - centerLat.substring(0, centerLat.length - 1);
				let centerLng = centerString.substr(latEnd + 1);
				if (centerLng.substr(-1) === "E")
					centerLng = centerLng.substring(0, centerLng.length - 1);
				else
					centerLng = - centerLng.substring(0, centerLng.length - 1);

				let latLngString = modifiedTrailierElement.substr(centerStringEnd + 1);
				nIndex = latLngString.indexOf("N");
				sIndex = latLngString.indexOf("S");
				if (nIndex === -1)
					latEnd = sIndex;
				else if (sIndex === -1)
					latEnd = nIndex;
				else
					latEnd = Math.min(nIndex, sIndex);
				let lat = latLngString.substr(0, latEnd + 1);
				if (lat.substr(-1) === "N")
					lat = lat.substring(0, lat.length - 1);
				else
					lat = - lat.substring(0, lat.length - 1);
				let lng = latLngString.substr(latEnd + 1);
				if (lng.substr(-1) === "E")
					lng = lng.substring(0, lng.length - 1);
				else
					lng = - lng.substring(0, lng.length - 1);

				mapClickHistoryElement.latlng = {
					lat: parseInt(lat) / multiplier,
					lng: parseInt(lng) / multiplier
				};
				mapClickHistoryElement.center = {
					lat: parseInt(centerLat) / multiplier,
					lng: parseInt(centerLng) / multiplier
				};

				mapClickHistory.push(mapClickHistoryElement);
			}
		);

		return mapClickHistory;
	};

	Utilities.prototype.initializeOrGetMapRootAlbum = function() {
		// prepare the root of the map albums and put it in the cache
		var rootMapAlbum = env.cache.getAlbum(env.options.by_map_string);
		if (! rootMapAlbum) {
			rootMapAlbum = new Album(env.options.by_map_string);
			rootMapAlbum.includedFilesByCodesSimpleCombination = new IncludedFiles({",": {}});
			env.cache.putAlbum(rootMapAlbum);
		}
		return rootMapAlbum;
	};

	Utilities.prototype.initializeSearchAlbumBegin = function(albumCacheBase) {
		var newSearchAlbum = new Album(albumCacheBase);
		newSearchAlbum.path = newSearchAlbum.cacheBase.replace(new RegExp(env.options.cache_folder_separator, "g"), "/");
		newSearchAlbum.ancestorsNames = [env.options.by_search_string, albumCacheBase];
		newSearchAlbum.ancestorsCacheBase = [env.options.by_search_string, albumCacheBase];
		newSearchAlbum.includedFilesByCodesSimpleCombination = new IncludedFiles({",": {}});
		newSearchAlbum.name = "(" + Utilities._t("#by-search") + ")";

		return newSearchAlbum;
	};

	Utilities.initializeSearchRootAlbum = function() {
		var rootSearchAlbum = new Album(env.options.by_search_string);
		env.cache.putAlbum(rootSearchAlbum);
	};


	Utilities.prototype.initializeSelectionRootAlbum = function() {
		// prepare the root of the selections albums and put it in the cache
		var rootSelectionAlbum = env.cache.getAlbum(env.options.by_selection_string);
		rootSelectionAlbum.includedFilesByCodesSimpleCombination = new IncludedFiles({",": {}});

		if (! rootSelectionAlbum) {
			rootSelectionAlbum = new SelectionAlbum(env.options.by_selection_string);
			rootSelectionAlbum.includedFilesByCodesSimpleCombination = new IncludedFiles({",": {}});

			env.cache.putAlbum(rootSelectionAlbum);
		}

		return rootSelectionAlbum;
	};

	Utilities.initializeSelectionAlbum = function() {
		// initializes the selection album

		let rootSelectionAlbum = env.cache.getAlbum(env.options.by_selection_string);

		let newCacheBase = Utilities.encodeSelectionCacheBase([]);

		let selectionAlbum = new SelectionAlbum(newCacheBase);

		selectionAlbum.ancestorsCacheBase = rootSelectionAlbum.ancestorsCacheBase.slice();
		selectionAlbum.ancestorsNames = [env.options.by_selection_string];

		selectionAlbum.changeCacheBase(newCacheBase);
		selectionAlbum.name = "(" + Utilities._t("#by-selection") + ")";

		selectionAlbum.includedFilesByCodesSimpleCombination = new IncludedFiles({",": {}});

		rootSelectionAlbum.numsMediaInSubTree.sum(selectionAlbum.numsMediaInSubTree);
		rootSelectionAlbum.subalbums.push(selectionAlbum);
		if (env.options.expose_image_positions && ! env.options.save_data) {
			rootSelectionAlbum.positionsAndMediaInTree.mergePositionsAndMedia(selectionAlbum.positionsAndMediaInTree);
			rootSelectionAlbum.numPositionsInTree = rootSelectionAlbum.positionsAndMediaInTree.length;
		}

		return selectionAlbum;
	};

	Utilities.prototype.changeCacheBase = function(album, newCacheBase) {
		// this function is to be used with selection and map album only
		album.cacheBase = newCacheBase;
		if (album.hasOwnProperty("ancestorsCacheBase"))
			album.ancestorsCacheBase[1] = newCacheBase;
		if (album.hasOwnProperty("ancestorsNames"))
			album.ancestorsNames[1] = newCacheBase;
		album.path = newCacheBase.replace(new RegExp(env.options.cache_folder_separator, 'g'), "/");

		env.cache.putAlbum(album);
	};


	Utilities.prototype.convertProtectedCacheBaseToCodesSimpleCombination = function(protectedCacheBase) {
		var protectedDirectory = protectedCacheBase.split("/")[0];
		var [albumMd5, mediaMd5] = protectedDirectory.substring(env.options.protected_directories_prefix.length).split(',');
		if (albumMd5 && mediaMd5)
			return [Utilities.convertMd5ToCode(albumMd5), Utilities.convertMd5ToCode(mediaMd5)].join(',');
		else if (albumMd5)
			return [Utilities.convertMd5ToCode(albumMd5), ''].join(',');
		else if (mediaMd5)
			return ['', Utilities.convertMd5ToCode(mediaMd5)].join(',');
		else
			return "";
	};

	Utilities.prototype.convertProtectedDirectoryToCodesSimpleCombination = function(protectedDirectory) {
		var [albumMd5, mediaMd5] = protectedDirectory.substring(env.options.protected_directories_prefix.length).split(',');
		if (albumMd5 && mediaMd5)
			return [Utilities.convertMd5ToCode(albumMd5), Utilities.convertMd5ToCode(mediaMd5)].join(',');
		else if (albumMd5)
			return [Utilities.convertMd5ToCode(albumMd5), ''].join(',');
		else if (mediaMd5)
			return ['', Utilities.convertMd5ToCode(mediaMd5)].join(',');
		else
			return "";
	};

	Utilities._s = function(key) {
		return env.shortcuts[env.language][key];
	};

	Utilities._t = function(key, firstParam = "", addShortcut = true) {
		var shortcut = "";
		if (! env.isAnyMobile) {
			shortcut = env.shortcuts[env.language][key + "-shortcut"];
			if (shortcut !== undefined && addShortcut)
				shortcut = " [" + shortcut + "]";
			else
				shortcut = "";
		}

		var translation = env.translations[env.language][key];
		if (firstParam && translation.indexOf("%") !== -1)
			translation = translation.replace("%", firstParam);
		return translation + shortcut;
	};

	Utilities.prototype.translate = function() {
		for (let key in env.translations.en) {
			if (env.translations[env.language].hasOwnProperty(key)) {
				if (key === '.title-string' && document.title.substr(0, 5) != "<?php" || key === "#started-slideshow-interval")
					// don't set page title, php has already set it
					continue;
				let keyObject = $(key);
				if (keyObject.length)
					keyObject.html(Utilities._t(key));
			}
		}
		for (let key in env.shortcuts.en) {
			if (env.shortcuts[env.language].hasOwnProperty(key)) {
				let keyObject = $(key);
				if (keyObject.length)
					keyObject.html(Utilities._s(key));
			}
		}
		$("#save-data").attr("title", Utilities._t("#save-data-tip"));
	};

	Utilities.windowVerticalScrollbarWidth = function() {
		// from https://davidwalsh.name/detect-scrollbar-width

		// Create the measurement node
		var scrollDiv = document.createElement("div");
		scrollDiv.className = "scrollbar-measure";
		document.body.appendChild(scrollDiv);

		// Get the scrollbar width
		var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;

		// Delete the DIV
		document.body.removeChild(scrollDiv);
		return scrollbarWidth;
	};

	Utilities.prototype.addBySomethingSubdir = function(cacheBase)	{
		var table = {};
		table[env.options.folders_string] = env.options.regular_album_subdir;
		table[env.options.by_date_string] = env.options.by_date_album_subdir;
		table[env.options.by_gps_string] = env.options.by_gps_album_subdir;
		table[env.options.by_search_string] = env.options.by_search_album_subdir;
		for (const string in table) {
			if (cacheBase.startsWith(string)) {
				cacheBase = Utilities.pathJoin([table[string], cacheBase]);
				break;
			}
		}
		return cacheBase;
	};

	Utilities.prototype.cloneObject = function(object) {
		return Object.assign({}, object);
	};

	Utilities.prototype.arrayIntersect = function(a, b) {
		if (b.length > a.length) {
			// indexOf to loop over shorter
			[a, b] = [b, a];
		}

		let intersection = [];
		for (let i = 0; i < b.length; i ++) {
			let elementB = b[i];
			if (a.indexOf(elementB) !== -1)
				intersection.push(elementB);
		}

		return intersection;
	};

	Utilities.mediaOrSubalbumsIntersectionForSearches = function(a, b) {
		if (! a.length || ! b.length) {
			a.length = 0;
			return;
		}

		var property = 'albumName';
		if (! a[0].hasOwnProperty('albumName'))
			// searched albums hasn't albumName property
			property = 'path';

		b.forEach(
			function(elementB) {
				let found = false;
				let indexA;
				for (indexA = a.length - 1; indexA >= 0; indexA --) {
					let elementA = a[indexA];
					var aValue = elementA[property];
					var bValue = elementB[property];
					if (property === 'albumName') {
						aValue = Utilities.pathJoin([aValue, elementA.name]);
						bValue = Utilities.pathJoin([bValue, elementB.name]);
					}
					if (Utilities.normalizeAccordingToOptions(bValue) === Utilities.normalizeAccordingToOptions(aValue)) {
						found = true;
						break;
					}
				}
				if (! found)
					a.splice(indexA, 1);
			}
		);
	};


	Utilities.prototype.mediaOrSubalbumsUnionForSearches = function(a, b) {
		if (! a.length) {
			a.push(... b);
			return;
		}
		if (! b.length) {
			return;
		}

		var property = 'albumName';
		if (! a[0].hasOwnProperty('albumName'))
			// searched albums haven't albumName property
			property = 'path';

		for (var i = 0; i < b.length; i ++) {
			let elementB = b[i];
			if (! a.some(
				function (elementA) {
					var bValue = elementB[property];
					var aValue = elementA[property];
					if (property === 'albumName') {
						bValue = Utilities.pathJoin([bValue, elementB.name]);
						aValue = Utilities.pathJoin([aValue, elementA.name]);
					}
					return Utilities.normalizeAccordingToOptions(bValue) === Utilities.normalizeAccordingToOptions(aValue);
				})
			)
				a.push(elementB);
		}
	};

	Utilities.prototype.arrayUnion = function(a, b, equalityFunction = null) {
		if (! a.length)
			return b;
		if (! b.length)
			return a;
		// begin cloning the first array
		var union = a.slice(0);
		var i;

		if (equalityFunction === null) {
			for (i = 0; i < b.length; i ++) {
				let elementB = b[i];
				if (union.indexOf(elementB) === -1)
					union.push(elementB);
			}
		} else {
			for (i = 0; i < b.length; i ++) {
				let elementB = b[i];
				if (
					a.every(
						function notEqual(element) {
							return ! equalityFunction(element, elementB);
						}
					)
				)
					union.push(elementB);
			}
		}
		return union;
	};

	Utilities.normalizeAccordingToOptions = function(object) {
		var string = object;
		if (typeof object === "object")
			string = string.join('|');

		if (! env.search.caseSensitive)
			string = string.toLowerCase();
		if (! env.search.accentSensitive)
			string = Utilities.removeAccents(string);

		if (typeof object === "object")
			object = string.split('|');
		else
			object = string;

		return object;
	};

	Utilities.prototype.encodeNonLetters = function(object) {
		var string = object;
		if (typeof object === "object")
			string = string.join('|');

		string = string.replace(
			/([^\p{L}_])/ug,
			match => match === "-" ? "%2D" : encodeURIComponent(match)
		);

		if (typeof object === "object")
			object = string.split('|');
		else
			object = string;

		return object;
	};

	Utilities.removeAccents = function(string) {
		string = decodeURIComponent(string).normalize('NFD');
		var stringArray = Array.from(string);
		var resultString = '';
		for (var i = 0; i < stringArray.length; i ++) {
			if (env.options.unicode_combining_marks.indexOf(stringArray[i]) === -1)
				resultString += stringArray[i];
		}
		return resultString;
	};

	Utilities.pathJoin = function(pathArr) {
		var result = '';
		for (var i = 0; i < pathArr.length; ++i) {
			if (i < pathArr.length - 1 && pathArr[i] && pathArr[i].at(-1) != "/")
				pathArr[i] += '/';
			if (i && pathArr[i] && pathArr[i][0] === "/")
				pathArr[i] = pathArr[i].slice(1);
			result += pathArr[i];
		}
		return result;
	};

	// see https://stackoverflow.com/questions/1069666/sorting-javascript-object-by-property-value
	Utilities.prototype.sortBy = function(albumOrMediaList, fieldArray) {
		albumOrMediaList.sort(
			function(a,b) {
				var aValue, bValue, iField;
				for (iField = 0; iField < fieldArray.length; iField ++) {
					if (a.hasOwnProperty(fieldArray[iField])) {
						aValue = a[fieldArray[iField]].toLowerCase();
						break;
					}
				}
				for (iField = 0; iField < fieldArray.length; iField ++) {
					if (b.hasOwnProperty(fieldArray[iField])) {
						bValue = b[fieldArray[iField]].toLowerCase();
						break;
					}
				}
				return aValue < bValue ? -1 : aValue > bValue ? 1 : 0;
			}
		);
	};

	Utilities.prototype.sortByExifDateMax = function(subalbums) {
		subalbums.sort(
			function(a, b) {
				return a.exifDateMax < b.exifDateMax ? -1 : a.exifDateMax > b.exifDateMax ? 1 : 0;
			}
		);
	};

	Utilities.prototype.sortByExifDateMin = function(subalbums) {
		subalbums.sort(
			function(a, b) {
				return a.exifDateMin < b.exifDateMin ? -1 : a.exifDateMin > b.exifDateMin ? 1 : 0;
			}
		);
	};

	Utilities.prototype.sortByFileDateMax = function(subalbums) {
		subalbums.sort(
			function(a, b) {
				return a.fileDateMax < b.fileDateMax ? -1 : a.fileDateMax > b.fileDateMax ? 1 : 0;
			}
		);
	};

	Utilities.prototype.sortByFileDateMin = function(subalbums) {
		subalbums.sort(
			function(a, b) {
				return a.fileDateMin < b.fileDateMin ? -1 : a.fileDateMin > b.fileDateMin ? 1 : 0;
			}
		);
	};

	Utilities.prototype.sortByPixelSizeMax = function(subalbums) {
		subalbums.sort(
			function(a, b) {
				const aSize = a.pixelSizeMax;
				const bSize = b.pixelSizeMax;
				return aSize < bSize ? -1 : aSize > bSize ? 1 : 0;
			}
		);
	};

	Utilities.prototype.sortByPixelSizeMin = function(subalbums) {
		subalbums.sort(
			function(a, b) {
				const aSize = a.pixelSizeMin;
				const bSize = b.pixelSizeMin;
				return aSize < bSize ? -1 : aSize > bSize ? 1 : 0;
			}
		);
	};

	Utilities.prototype.sortByFileSize = function(subalbums) {
		subalbums.sort(
			function(a, b) {
				const aSize = a.sizesOfSubTree.original.imagesAudiosVideosTotal();
				const bSize = b.sizesOfSubTree.original.imagesAudiosVideosTotal();
				return aSize < bSize ? -1 : aSize > bSize ? 1 : 0;
			}
		);
	};

	Utilities.prototype.isVirtualCacheBase = function(cacheBase) {
		var result =
			Utilities.isMapCacheBase(cacheBase) ||
			Utilities.isSelectionCacheBase(cacheBase);
		return result;
	};

	Utilities.isAnyRootCacheBase = function(cacheBase) {
		var result = [env.options.folders_string, env.options.by_date_string, env.options.by_gps_string].indexOf(cacheBase) !== -1;
			// Utilities.isSearchCacheBase(cacheBase) ||
			// Utilities.isMapCacheBase(cacheBase) ||
			// Utilities.isSelectionCacheBase(cacheBase);
		return result;
	};

	Utilities.prototype.isAnyRootOrCollectionCacheBase = function(cacheBase) {
		var result = (
			Utilities.isAnyRootCacheBase(cacheBase) ||
			Utilities.isSearchCacheBase(cacheBase) ||
			Utilities.isMapCacheBase(cacheBase) ||
			Utilities.isSelectionCacheBase(cacheBase)
		);
		return result;
	};

	Utilities.prototype.trimExtension = function(name) {
		var index = name.lastIndexOf(".");
		if (index !== -1)
			return name.substring(0, index);
		return name;
	};

	Utilities.isPhp = function() {
		return typeof isPhp === "function";
	};

	Utilities.isFolderCacheBase = function(cacheBase) {
		return cacheBase === env.options.folders_string || cacheBase.startsWith(env.options.foldersStringWithTrailingSeparator);
	};

	Utilities.isByDateCacheBase = function(cacheBase) {
		return cacheBase === env.options.by_date_string || cacheBase.startsWith(env.options.byDateStringWithTrailingSeparator);
	};

	Utilities.isByGpsCacheBase = function(cacheBase) {
		return cacheBase === env.options.by_gps_string || cacheBase.startsWith(env.options.byGpsStringWithTrailingSeparator);
	};

	Utilities.isSearchCacheBase = function(cacheBase) {
		return (
			cacheBase.startsWith(env.options.bySearchStringWithTrailingSeparator) &&
			cacheBase.split(env.options.search_options_separator).length > 1
		);
	};

	Utilities.isSelectionCacheBase = function(cacheBase) {
		return cacheBase.startsWith(env.options.bySelectionStringWithTrailingSeparator);
	};

	Utilities.prototype.isCollectionCacheBase = function(cacheBase) {
		return Utilities.isMapCacheBase(cacheBase) || Utilities.isSearchCacheBase(cacheBase) || Utilities.isSelectionCacheBase(cacheBase);
	};

	Utilities.isMapCacheBase = function(cacheBase) {
		return cacheBase.startsWith(env.options.byMapStringWithTrailingSeparator);
	};

	Utilities.isSearchHash = function() {
		var cacheBase = PhotoFloat.convertHashToCacheBase(location.hash);
		if (
			Utilities.isSearchCacheBase(cacheBase) ||
			env.hashComponents.collectionCacheBase !== null && Utilities.isSearchCacheBase(env.hashComponents.collectionCacheBase)
		)
			return true;
		else
			return false;
	};

	Utilities.prototype.stripHtmlAndReplaceEntities = function(htmlString) {
		// converto for for html page title
		// strip html (https://stackoverflow.com/questions/822452/strip-html-from-text-javascript#822464)
		// and replaces &raquo; with \u00bb
		return htmlString.replace(/<(?:.|\n)*?>/gm, '').replace(/&raquo;/g, '\u00bb');
	};

	Utilities.transformAltPlaceName = function(altPlaceName, withBr = true) {
		var underscoreIndex = altPlaceName.lastIndexOf('_');
		if (underscoreIndex != -1) {
			var number = altPlaceName.substring(underscoreIndex + 1);
			var base = altPlaceName.substring(0, underscoreIndex);
			let br = " ";
			if (withBr)
				br = "<br />";
			return base + br + "<span class='subalbum-number'>(" + Utilities._t(".subalbum") + " " + parseInt(number) + ")<span>";
		} else {
			return altPlaceName;
		}
	};

	Utilities.albumButtonWidth = function(thumbnailWidth) {
		if (env.options.albums_slide_style) {
			return Math.ceil(thumbnailWidth * (1 + 2 * env.slideMarginFactor) + 2 * env.slideAlbumButtonBorder + 2 * env.slideBorder);
		} else {
			return Math.ceil(thumbnailWidth + 1 * env.options.spacing);
		}
	};

	Utilities.thumbnailWidth = function(albumButtonWidth) {
		if (env.options.albums_slide_style) {
			return Math.floor((albumButtonWidth - 2 * env.slideAlbumButtonBorder - 2 * env.slideBorder) / (1 + 2 * env.slideMarginFactor));
		} else {
			return Math.floor(albumButtonWidth - 1 * env.options.spacing);
		}
	};

	Utilities.removeFolderString = function (cacheBase) {
		if (Utilities.isFolderCacheBase(cacheBase)) {
			cacheBase = cacheBase.substring(env.options.folders_string.length);
			if (cacheBase.length > 0)
				cacheBase = cacheBase.substring(1);
		}
		return cacheBase;
	};

	Utilities.somethingIsInMapAlbum = function(mapAlbum = env.mapAlbum) {
		if (typeof mapAlbum === "undefined" || mapAlbum.isEmpty())
			return false;

		if (mapAlbum.hasOwnProperty("positionsAndMediaInTree") && mapAlbum.positionsAndMediaInTree.length)
			return true;
		else
			return false;
	};

	Utilities.absolutelySomethingIsSearched = function(searchAlbum = env.searchAlbum) {
		if (searchAlbum.isEmpty())
			return false;

		if (
			searchAlbum.hasOwnProperty("numsMediaInSubTree") && searchAlbum.numsMediaInSubTree.imagesAudiosVideosTotal() ||
			searchAlbum.hasOwnProperty("subalbums") && searchAlbum.subalbums.length
		)
			return true;
		else
			return false;
	};

	Utilities.prototype.somethingIsSearched = function(searchAlbum = env.searchAlbum) {
		if (searchAlbum.isEmpty())
			return false;

		if (
			searchAlbum.hasOwnProperty("numsMediaInSubTree") &&
			searchAlbum.visibleNumsMediaInSubTree().imagesAudiosVideosTotal()
			||
			searchAlbum.hasOwnProperty("subalbums") &&
			searchAlbum.visibleSubalbums().length
		)
			return true;
		else
			return false;
	};

	Utilities.absolutelyNothingIsSelected = function(selectionAlbum = env.selectionAlbum) {
		if (typeof selectionAlbum === "undefined")
			return true;

		if (selectionAlbum.media.length || selectionAlbum.subalbums.length)
			return false;
		else
			return true;
	};

	Utilities.prototype.nothingIsSelected = function(selectionAlbum = env.selectionAlbum) {
		if (Utilities.absolutelyNothingIsSelected(selectionAlbum))
			return true;

		if (selectionAlbum.numVisibleMedia() || selectionAlbum.visibleSubalbums().length)
			return false;
		else
			return true;
	};

	Utilities.absolutelySomethingIsSelected = function(selectionAlbum = env.selectionAlbum) {
		return ! Utilities.absolutelyNothingIsSelected(selectionAlbum);
	};

	Utilities.albumOrMediaSelectionIsChecked = function(parentSelector) {
		if ($(parentSelector + " .select-box").attr("src").indexOf("-checked-") === -1)
			return false;
		else
			return true;
	};

	Utilities.prototype.albumIsSelected = function(album, selectionAlbum = env.selectionAlbum) {
		if (Utilities.absolutelyNothingIsSelected(selectionAlbum))
			return false;

		var index = selectionAlbum.subalbums.findIndex(x => x.isEqual(album));
		if (index > -1)
			return true;
		else
			return false;
	};

	Utilities.isShiftOrControl = function() {
		return $(".shift-or-control").length ? true : false;
	};

	Utilities.isPopup = function() {
		return $(".media-popup.leaflet-popup").html() ? true : false;
	};

	Utilities.isMap = function() {
		return ($('#mapdiv').html() ? true : false) && ! Utilities.isPopup();
	};

	Utilities.setTitleOptions = function(id) {
		if (env.options.hide_title) {
			$(".title").addClass("hidden-by-option");
		} else {
			$(".title").removeClass("hidden-by-option");
			if (! id || (id === "center" || id === "album")) {
				$.executeAfterEvent(
					"otherJsFilesLoadedEvent",
					function() {
						let newWidth = env.windowWidth;
						if (env.currentMedia)
							newWidth -= 5;
						// avoid that the title remain below right buttons
						newWidth -= parseInt($("#menu-and-padlock").css("width")) + 5;
						$(".title").css("width", newWidth);
						Utilities.scrollTitleToBestPosition();
					}
				);
			}
		}

		if (! env.options.show_album_media_count)
			$(".title-count").addClass("hidden-by-option");
		else
			$(".title-count").removeClass("hidden-by-option");

		$(".title").css("font-size", env.options.title_font_size);
		$(".title-anchor").css("color", env.options.title_color);
		$(".title-anchor").on(
			"mouseenter",
			function() {
				//mouse over
				$(this).css("color", env.options.title_color_hover);
			}
		).on(
			"mouseleave",
			function() {
				//mouse out
				$(this).css("color", env.options.title_color);
			}
		);
	};

	Utilities.encodeHash = function(
		cacheBase,
		singleMediaCacheBase,
		singleMediaFoldersCacheBase,
		collectedAlbumCacheBase,
		collectionCacheBase
	) {
		var hash;

		// if (collectionCacheBase !== undefined && collectionCacheBase !== null) {
		// 	collectionCacheBase = PhotoFloat.convertHashToCacheBase(collectionCacheBase);
		// 	collectedAlbumCacheBase = PhotoFloat.convertHashToCacheBase(collectedAlbumCacheBase);
		// }

		if (singleMediaCacheBase) {
			// media hash
			if (Utilities.isFolderCacheBase(cacheBase)) {
				if (collectionCacheBase === undefined || collectionCacheBase === null)
					// media in folders album, count = 2
					hash = Utilities.pathJoin([
						cacheBase,
						singleMediaCacheBase
					]);
				else
					// media in found album or in one of its subalbum, count = 4
					hash = Utilities.pathJoin([
						cacheBase,
						collectedAlbumCacheBase,
						collectionCacheBase,
						singleMediaCacheBase
					]);
			} else if (
				Utilities.isByDateCacheBase(cacheBase) ||
				Utilities.isByGpsCacheBase(cacheBase) ||
				Utilities.isSearchCacheBase(cacheBase) && (collectionCacheBase === undefined || collectionCacheBase === null) ||
				Utilities.isSelectionCacheBase(cacheBase) ||
				Utilities.isMapCacheBase(cacheBase)
			)
				// media in date or gps album, count = 3
				hash = Utilities.pathJoin([
					cacheBase,
					singleMediaFoldersCacheBase,
					singleMediaCacheBase
				]);
		} else {
			// no media: album hash
			if (collectionCacheBase !== undefined && collectionCacheBase !== null)
				// found album or one of its subalbums, count = 3
				hash = Utilities.pathJoin([
					cacheBase,
					collectedAlbumCacheBase,
					collectionCacheBase
				]);
			else
				// plain search album, count = 1
				// folders album, count = 1
				hash = cacheBase;
		}
		return env.hashBeginning + hash;
	};

	Utilities.prototype.changeToBySelectionView = function(ev, thisMedia = null) {
		TopFunctions.showBrowsingModeMessage(ev, "#by-selection-browsing");
		var isShiftOrControl = Utilities.isShiftOrControl();
		var isPopup = Utilities.isPopup();
		var isMap = ($('#mapdiv').html() ? true : false) && ! isPopup;
		if (isShiftOrControl) {
			// close the shift/control buttons
			$(".shift-or-control .leaflet-popup-close-button")[0].click();
		}
		if (isPopup) {
			// the popup is there: close it
			env.highlightedObjectId = null;
			$("media-popup .leaflet-popup-close-button")[0].click();
		}
		if (isMap || isPopup) {
			// we are in a map: close it
			$('.modal-close')[0].click();
		}

		if (thisMedia) {
			$(".title").removeClass("hidden-by-pinch");
			$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-pinch");
			window.location.href = Utilities.encodeHash(env.selectionAlbum.cacheBase, thisMedia.cacheBase, thisMedia.foldersCacheBase);
		} else {
			window.location.href = Utilities.encodeHash(env.selectionAlbum.cacheBase, null);
		}
		return false;
	};

	Utilities.setMediaOptions = function() {
		$(".media-name").css("color", env.options.media_name_color);

		if (! env.options.show_media_names_below_thumbs)
			$(".thumb-and-caption-container .media-name").addClass("hidden-by-option");
		else
			$(".thumb-and-caption-container .media-name").removeClass("hidden-by-option");

		$(".thumb-and-caption-container").css("margin-right", env.options.spacing.toString() + "px");
		if ($("#album-and-media-container").hasClass("single-media"))
			$(".thumb-and-caption-container").css("margin-bottom", "0");
		else
			$(".thumb-and-caption-container").css("margin-bottom", env.options.spacing.toString() + "px");

		if (env.options.hide_descriptions || ! env.options.show_media_names_below_thumbs)
			$(".media-description").addClass("hidden-by-option");
		else
			$(".media-description").removeClass("hidden-by-option");

		if (env.options.hide_tags || ! env.options.show_media_names_below_thumbs)
			$(".media-tags").addClass("hidden-by-option");
		else
			$(".media-tags").removeClass("hidden-by-option");

		$("#album-and-media-container #thumbs").removeClass("hidden-by-option");
		if (env.options.hide_bottom_thumbnails) {
			$("#album-and-media-container.single-media #thumbs").addClass("hidden-by-option");
		}
	};

	Utilities.prototype.setSubalbumsOptions = function() {
		// resize down the album buttons if they are too wide
		let albumViewWidth =
			$("body").width() -
			parseInt($("#album-and-media-container:not(.single-media) #album-view").css("margin-left")) -
			parseInt($("#album-view").css("padding-left")) -
			parseInt($("#album-view").css("padding-right")) -
			Utilities.windowVerticalScrollbarWidth();
		env.correctedAlbumThumbSize = env.options.album_thumb_size;
		let correctedAlbumButtonSize = Utilities.albumButtonWidth(env.options.album_thumb_size);
		if (albumViewWidth / (correctedAlbumButtonSize + env.options.spacing) < env.options.min_album_thumbnail) {
			env.correctedAlbumThumbSize = Math.floor(Utilities.thumbnailWidth(albumViewWidth / env.options.min_album_thumbnail - env.options.spacing)) - 1;
			correctedAlbumButtonSize = Utilities.albumButtonWidth(env.correctedAlbumThumbSize);
		}
		let captionFontSize = Math.round(Utilities.em2px("body", 1) * env.correctedAlbumThumbSize / env.options.album_thumb_size);
		let captionHeight = parseInt(captionFontSize * 3);
		let margin = 0;
		if (env.options.albums_slide_style)
			margin = Math.round(env.correctedAlbumThumbSize * env.slideMarginFactor);

		let buttonAndCaptionHeight = correctedAlbumButtonSize + captionHeight;

		let slideBorder = 0;
		if (env.options.albums_slide_style)
			slideBorder = env.slideBorder;

		if (env.currentAlbum.isFolder() && ! env.options.show_album_names_below_thumbs)
			$(".album-name").addClass("hidden-by-option");
		else
			$(".album-name").removeClass("hidden-by-option");

		if (env.options.albums_slide_style) {
			$(".album-name").css("color", env.options.album_slide_name_color);
			$(".album-button-and-caption").css("background-color", env.options.album_slide_background_color);
			$(".album-button").css("background-color", env.options.album_slide_background_color);
			$(".album-caption, .album-caption .real-name").css("color", env.options.album_slide_caption_color);
			$(".album-button").css("border", "");
			$(".album-button-and-caption").addClass("slide");
		} else {
			$(".album-name").css("color", env.options.album_name_color);
			$(".album-button-and-caption").css("background-color", "");
			$(".album-button").css("background-color", "");
			$(".album-caption, .album-caption .real-name").css("color", env.options.album_caption_color);
			$(".album-button").css("border", "none");
			$(".album-button-and-caption").removeClass("slide");
		}

		let marginBottom = env.options.spacing;
		if (! env.options.albums_slide_style)
			marginBottom += Utilities.em2px("body", 2);
		$(".album-button-and-caption").css("width", (correctedAlbumButtonSize - 2 * slideBorder) + "px");
		$(".album-button-and-caption").css("height", buttonAndCaptionHeight + "px");
		$(".album-button-and-caption").css("margin-right", env.options.spacing.toString() + "px");
		$(".album-button-and-caption").css("margin-bottom", marginBottom.toString() + "px");

		$(".album-button").css("margin", margin + "px");
		$(".album-button").css("width", env.correctedAlbumThumbSize + "px");
		$(".album-button").css("height", env.correctedAlbumThumbSize + "px");

		$(".album-caption").css("width", env.correctedAlbumThumbSize + "px");
		$(".album-caption").css("height", captionHeight + "px");
		$(".album-caption").css("font-size", captionFontSize + "px");

		$(".album-tags").css("font-size", (Math.round(captionFontSize * 0.75)) + "px");

		if (
			$("#subalbums .album-button img.thumbnail").length &&
			$("#subalbums").is(":visible")
		) {
			let firstThumbnail = $("#subalbums .album-button img.thumbnail").first();
			let attribute;
			if (firstThumbnail.attr("src") === "img/image-placeholder.jpg")
				// this value is for thumbnails not processed by LazyLoad yet
				attribute = "data-src";
			else
				attribute = "src";
			if (firstThumbnail.attr("src") !== "img/image-placeholder.jpg") {
				let mustBeSquare = (env.options.only_square_thumbnails || env.options.album_thumb_type.indexOf("square") > -1);
				$("#subalbums .album-button img.thumbnail").each(
					function() {
						if ($(this).attr("width") !== $(this).attr("height")) {
							let src = $(this).attr(attribute);
							let extension = src.substring(src.length - env.options.format.length);
							let hasSquareSuffix = (src.substr(- extension.length - 3, 2) === "sq");
							let srcBeforeSquareSuffix;
							if (hasSquareSuffix)
								srcBeforeSquareSuffix = src.substr(0, src.length - extension.length - 3);
							else
								srcBeforeSquareSuffix = src.substr(0, src.length - extension.length - 1);
							if (hasSquareSuffix && ! mustBeSquare)
								src = srcBeforeSquareSuffix + "." + extension;
							else if (! hasSquareSuffix && mustBeSquare)
								src = srcBeforeSquareSuffix + "sq." + extension;
							$(this).attr(attribute, src);
						}
					}
				);
			}
		}


		if (! env.options.show_album_media_count)
			$(".album-caption-count").addClass("hidden-by-option");
		else
			$(".album-caption-count").removeClass("hidden-by-option");


		if (env.options.hide_descriptions)
			$(".album-description").addClass("hidden-by-option");
		else
			$(".album-description").removeClass("hidden-by-option");

		if (env.options.hide_tags)
			$(".album-tags").addClass("hidden-by-option");
		else
			$(".album-tags").removeClass("hidden-by-option");
	};

	Utilities.prototype.showMediaNameInAudio = function() {
		$(".thumb-container .name-in-audio").removeClass("hidden");
		$(".thumb-container .name-in-audio").css(
			"width",
			(parseInt($(".thumb-container .name-in-audio").parent().css("width")) - 10 - 3 - 10 - 3) + "px"
		);
	};

	Utilities.prototype.hideMediaNameInAudio = function() {
		$(".thumb-container .name-in-audio").addClass("hidden");
	};

	Utilities.prototype.highlightedObjectSelector = function(inThumbs = false) {
		let aSubalbumWasHighlighted = Utilities.aSubalbumIsHighlighted();
		let highlightedObject = Utilities.highlightedObject(inThumbs);
		let highlightedObjectSelector;
		if (highlightedObject.length) {
			if (! inThumbs && Utilities.isPopup())
				highlightedObjectSelector = "#" + highlightedObject.attr("id");
			else if (aSubalbumWasHighlighted)
				highlightedObjectSelector = "#" + highlightedObject.attr("id");
			else
				highlightedObjectSelector = "#" + highlightedObject.parent().attr("id");
		} else if (! inThumbs && 	Utilities.isPopup()) {
			highlightedObjectSelector = "#" + $("#popup-images-wrapper").children().first().attr("id");
		} else if (! $("#subalbums a").length) {
			highlightedObjectSelector = "#" + $("#subalbums").children().first().children().first().attr("id");
		} else if (! $("#thumbs a").length) {
			highlightedObjectSelector = "#" + $("#thumbs").children().first().attr("id");
		}
		return highlightedObjectSelector;
	};

	Utilities.highlighObject = function(highlightedObjectSelector, aSubalbumWasHighlighted, inThumbs = false) {
		if (Utilities.isPopup() && ! inThumbs) {
			Utilities.addHighlightToMediaOrSubalbum($(highlightedObjectSelector));
			env.mapAlbum.updatePopup();
			Utilities.scrollPopupToHighlightedThumb($(highlightedObjectSelector));
		} else {
			if (aSubalbumWasHighlighted) {
				Utilities.addHighlightToMediaOrSubalbum($(highlightedObjectSelector));
				Utilities.scrollAlbumViewToHighlightedSubalbum($("#subalbums .highlighted-object"));
			} else if (! aSubalbumWasHighlighted) {
				let remove = true;
				if (inThumbs)
					remove = false;
				Utilities.addHighlightToMediaOrSubalbum($(highlightedObjectSelector).children(), remove);
				Utilities.scrollAlbumViewToHighlightedMedia($("#thumbs .highlighted-object"));
			}
		}
	};

	Utilities.addTagLink = function(tag) {
		// tags can be phrases (e.g. with automatic tags from person recognition)

		// now replace space -> underscore
		var tagForHref = tag.replace(/ /g, "_");
		// all non-letter character must be converted to space
		tagForHref = tagForHref.replace(/([^\p{L}_])/ug, match => match === "-" ? "%2D" : encodeURIComponent(match));

		var hash = (
			env.hashBeginning + env.options.by_search_string + env.options.cache_folder_separator +
			"t" + env.options.search_options_separator +
			"o" + env.options.search_options_separator +
			tagForHref + env.options.search_options_separator + Utilities.encodeAllNonAlfaNum(env.currentAlbum.cacheBase)
		);
		return "<a href='" + hash + "'>" + tag + "</a>";
	};

	Utilities.em2px = function(selector, em) {
		var emSize = parseFloat($(selector).css("font-size"));
		return (em * emSize);
	};

	Utilities.currentSizeAndIndex = function() {
		// Returns the pixel size of the image in DOM and the corresponding reduction index
		// If the original image is in the DOM, returns its size and -1

		var currentReduction = $(".media-box#center .media-box-inner img").attr("src");

		// check if it's a reduction
		for (var i = 0; i < env.options.reduced_sizes.length; i ++) {
			if (currentReduction === env.currentMedia.reducedMediaPath(env.options.reduced_sizes[i])) {
				return [env.options.reduced_sizes[i], i];
			}
		}

		// default: it's the original image
		return [Math.max(env.currentMedia.width(), env.currentMedia.height()), -1];
	};

	Utilities.nextSizeAndIndex = function() {
		// Returns the size of the reduction immediately bigger than that in the DOM and its reduction index
		// Returns the original image size and -1 if the reduction in the DOM is the biggest one
		// Returns [false, false] if the original image is already in the DOM

		var [fake_currentReductionSize, currentReductionIndex] = Utilities.currentSizeAndIndex();
		if (currentReductionIndex === -1)
			return [false, false];

		if (currentReductionIndex === 0) {
			if (env.options.expose_original_media || env.options.expose_full_size_media_in_cache)
				return [Math.max(env.currentMedia.width(), env.currentMedia.height()), -1];
			else
				return [env.options.reduced_sizes[0], 0];
		} else {
			return [env.options.reduced_sizes[currentReductionIndex - 1], currentReductionIndex - 1];
		}
	};

	Utilities.prototype.nextReduction = function() {
		// Returns the file name of the reduction with the next bigger size than the reduction in DOM,
		// possibly the original image
		// Returns false if the original image is already in the DOM

		var [nextReductionSize, nextReductionIndex] = Utilities.nextSizeAndIndex();

		if (nextReductionIndex === false)
			// it's already the original image
			return false;
		if (nextReductionIndex === -1)
			return env.currentMedia.fullSizeMediaPath();

		return env.currentMedia.reducedMediaPath(nextReductionSize);
	};

	Utilities.prototype.mediaBoxContainerHeight = function() {
		var heightForMediaAndTitle;
		// env.windowHeight = $(window).innerHeight();
		heightForMediaAndTitle = env.windowHeight;
		if ($("#album-view").is(":visible"))
			heightForMediaAndTitle -= $("#album-view").outerHeight();

		return heightForMediaAndTitle;
	};

	Utilities.setFinalOptions = function(singleMedia, initialize) {
		Utilities.setSelectButtonVisibility();
		Utilities.setTitleOptions();
		Utilities.setMediaOptions();
		Utilities.setPrevNextVisibility();
		Utilities.setPrevNextPosition();
		Utilities.setPinchButtonsVisibility();
		Utilities.setMapButtonVisibility();
		if (singleMedia) {
			if (singleMedia.isImage()) {
				$.executeAfterEvent(
					"pinchSwipeFunctionsLoadedEvent",
					function() {
						if (initialize)
							PinchSwipe.initializePincheSwipe();
						if (! env.currentZoom || env.currentZoom === env.initialZoom || initialize)
							Utilities.setPinchButtonsPosition();
					}
				);
			}
			if (
				singleMedia.hasGpsData() ||
				Utilities.isPhp() &&
				env.options.user_may_suggest_location &&
				env.options.request_password_email
			) {
				Utilities.setMapButtonPosition();
			}
			Utilities.setSelectButtonPosition();
			Utilities.setSlideshowButtonsPosition();
		}
		Utilities.setDescriptionOptions();
		Utilities.setUpButtonVisibility();
		Utilities.correctElementPositions();
	};

	Utilities.roundDecimals = function (float, decimalPlaces = env.zoomDecimalPlaces) {
		if (Math.abs(float) < Number("10e" + (-decimalPlaces)))
			return Utilities.roundDecimals(float, decimalPlaces + 1);
		else
			return Number(Math.round(float + "e" + decimalPlaces) + "e" + (- decimalPlaces));
	};

	Utilities.prototype.isLoaded = function(imgSrc) {
		return env.loadedImages.indexOf(imgSrc) !== -1;
	};

	Utilities.prototype.setLoaded = function(imgSrc) {
		env.loadedImages.push(imgSrc);
	};

	Utilities.geotaggedContentIsHidden = function() {
		return env.options.expose_image_positions && $("#fullscreen-wrapper").hasClass("hide-geotagged");
	};

	Utilities.prototype.selectBoxObject = function(object) {
		var isPopup = Utilities.isPopup();
		var selector = "", selectBoxObject;
		if (isPopup)
			selector = "#popup-images-wrapper ";
		if (Utilities.objectIsASubalbum(object) || isPopup) {
			selector += "#" + object.attr("id");
		} else {
			selector += "#" + object.parent().attr("id");
		}
		selectBoxObject = $(selector + " .select-box");
		return selectBoxObject;
	};

	Utilities.prototype.addClickToHiddenGeotaggedMediaPhrase = function() {
		// if ($(".hidden-geotagged-media").is(":visible")) {
		$(".hidden-geotagged-media").off("click").on(
			"click",
			function() {
				$("#hide-geotagged-media").trigger("click");
			}
		);
		// }
	};

	Utilities.prototype.decideWhatToHighlight = function() {
		let subalbumFilter = "*";
		let selector;
		if (Utilities.geotaggedContentIsHidden())
			subalbumFilter = ":not(.all-gps)";

		if (env.currentMedia === null) {
			if (
				env.previousMedia !== null &&
				env.albumOfPreviousState !== null &&
				env.albumOfPreviousState.cacheBase === env.currentAlbum.cacheBase
			) {
				// we are showing an album coming from a single media inside it
				// the media which was shown has to be highlighted

				selector =
					"#album-view" + env.options.cache_folder_separator +
					env.previousMedia.foldersCacheBase + "--" + env.previousMedia.cacheBase;
				Utilities.highlighObject(selector, false);
			}

			if (
				env.previousAlbum !== null &&
				env.previousAlbum.cacheBase.length > env.currentAlbum.cacheBase.length &&
				env.previousAlbum.cacheBase.startsWith(env.currentAlbum.cacheBase)
			) {
				// we are showing an album coming from inside it
				// the subalbum which the previous album was inside has to be highlighted
				let candidateForHighlighting = $("#subalbums").children(subalbumFilter).filter(
					(index, element) => env.previousAlbum.cacheBase == $(element).children().attr("id")
					// (index, element) => env.previousAlbum.cacheBase.indexOf($(element).children().attr("id")) === 0
				);
				if (candidateForHighlighting.length) {
					selector = "#" + candidateForHighlighting.children().attr("id");
					Utilities.highlighObject(selector, true);
				}
			}
		}

		if (! Utilities.aSubalbumIsHighlighted() && ! Utilities.aSingleMediaIsHighlighted()) {
			if (env.currentAlbum.visibleSubalbums().length) {
				selector = "#" + $("#subalbums").children(subalbumFilter).first().children().attr("id");
				Utilities.highlighObject(selector, true);
			} else if (env.currentAlbum.numVisibleMedia()) {
				let mediaFilter = "*";
				if (Utilities.geotaggedContentIsHidden())
					mediaFilter = ":not(.gps)";

				selector = "#" + $("#thumbs").children(mediaFilter).first().attr("id");
				Utilities.highlighObject(selector, false);
			}
		}
	};

	Utilities.removeHighligths = function() {
		if (Utilities.isPopup()) {
			$("#popup-images-wrapper .highlighted-object").removeClass("highlighted-object");
		} else {
			$("#thumbs .highlighted-object").removeClass("highlighted-object");
			$("#subalbums .highlighted-object").removeClass("highlighted-object");
		}
	};

	Utilities.addHighlightToMediaOrSubalbum = function(object, remove = true) {
		if (remove)
			Utilities.removeHighligths();
		object.addClass("highlighted-object");
	};

	Utilities.highlightedObject = function(inThumbs = false) {
		if (Utilities.isPopup() && ! inThumbs) {
			return $("#popup-images-wrapper .highlighted-object");
		} else {
			return $("#album-and-media-container .highlighted-object");
		}
	};

	Utilities.objectIsASubalbum = function(object) {
		return object.hasClass("album-button-and-caption");
	};

	Utilities.objectIsASingleMedia = function(object) {
		return object.hasClass("thumb-and-caption-container");
	};

	Utilities.aSubalbumIsHighlighted = function() {
		return $("#subalbums .highlighted-object").length > 0;
	};

	Utilities.aSingleMediaIsHighlighted = function() {
		return $("#thumbs .highlighted-object").length > 0;
	};

	Utilities.scrollAlbumViewToHighlightedSubalbum = function(object) {
		var numVisibleSubalbums = env.currentAlbum.visibleSubalbums().length;
		var filter = "*";
		if (Utilities.geotaggedContentIsHidden())
			filter = ":not(.all-gps)";

		if (! Utilities.isPopup() && $("#subalbums").is(":visible") && numVisibleSubalbums) {
			if (object !== undefined && object.length) {
				$(window).scrollTop(object.offset().top + object.height() / 2 - env.windowHeight / 2);
				// // the following instruction is needed to activate the lazy loader
				// $(window).trigger("scroll");
			}
		}
	};

	Utilities.scrollAlbumViewToHighlightedMedia = function(object) {
		var filter = "*";
		if (Utilities.geotaggedContentIsHidden())
			filter = ":not(.gps)";

		if ($("#thumbs").is(":visible")) {
			let thumbObject = object.children(".thumb-container").children(".thumbnail");

			if (thumbObject.length) {
				let offset;
				offset = thumbObject.offset().top - $("#album-view").offset().top + thumbObject.height() / 2 - env.windowHeight / 2;
				if (offset < 0)
					offset = 0;

				let scrollableObject = $(window);
				scrollableObject.scrollTop(offset);
				// // the following instruction is needed to activate the lazy loader
				// scrollableObject.trigger("scroll");
			}
		}
	};

	Utilities.scrollPopupToHighlightedThumb = function(object) {
		if (typeof object !== "undefined") {
			let scrollableObject = $("#popup-images-wrapper");
			let popupHeight = scrollableObject.height();

			let thumbObject = object.children(".thumb-container").children(".thumbnail");
			if (thumbObject.length) {
				let offset = scrollableObject.scrollTop() + thumbObject.offset().top - scrollableObject.offset().top + thumbObject.height() / 2 - popupHeight / 2;
				if (offset < 0)
					offset = 0;
				scrollableObject.scrollTop(offset);
				// // the following instruction is needed to activate the lazy loader
				// scrollableObject.trigger("scroll");
			}
		}
	};

	Utilities.prototype.scrollBottomMediaToHighlightedThumb = function() {
		if (! Utilities.isPopup() && $("#thumbs").is(":visible") && env.currentMedia !== null) {
			let thumbObject = $("#" + env.currentMedia.foldersCacheBase + "--" + env.currentMedia.cacheBase);
			if (thumbObject[0] !== undefined && ! env.currentAlbumIsAlbumWithOneMedia) {
				let scroller = $("#album-view");
				scroller.scrollLeft(thumbObject.parent().position().left + scroller.scrollLeft() - scroller.width() / 2 + thumbObject.width() / 2);
				$(".thumb-container").removeClass("current-thumb");
				thumbObject.parent().addClass("current-thumb");
				Utilities.addMediaLazyLoader(true);
			}
		}
	};

	Utilities.scrollTitleToBestPosition = function() {
		if (! Utilities.isPopup() && $(".title").is(":visible")) {
			let div = "#album-view";
			if (env.currentMedia)
				div = "#center";
			let scroller = $(div + " .title");
			if (env.currentMedia || ! $(div + " .title-count").is(":visible")) {
				// the best position it completely at right
				scroller.scrollLeft(5000);
			} else {
				// the best position is when the counts are just outside
				let newPosition = Math.max(0, parseInt($(div + " .title-main").css("width")) - parseInt(scroller.css("width")) + 10);
				scroller.scrollLeft(newPosition);
			}
		}
	};

	Utilities.decodeAllNonAlfaNum = function(string) {
		if (! string)
			return null;
		return string.replace(
			/%[0-9A-Fa-f]{2}/g,
			function(match) {
				return String.fromCharCode(parseInt(match.substr(1), 16));
			}
		);
	};


	Utilities.prototype.checkAlbumWithOneMedia = function() {
		env.currentAlbumIsAlbumWithOneMedia = false;
		if (env.currentAlbum !== null)
			env.currentAlbumIsAlbumWithOneMedia = env.currentAlbum.isAlbumWithOneMedia();

		env.previousAlbumIsAlbumWithOneMedia = false;
		if (env.previousAlbum !== null)
			env.previousAlbumIsAlbumWithOneMedia = env.previousAlbum.isAlbumWithOneMedia();

		if (env.currentAlbumIsAlbumWithOneMedia) {
			env.currentMedia = env.currentAlbum.visibleMedia()[0];
			env.currentMediaIndex = env.currentAlbum.media.findIndex(singleMedia => singleMedia.isEqual(env.currentMedia));
			env.nextMedia = null;
			env.prevMedia = null;
		}
	};

	Utilities.prototype.showHideSlideshowIcon = function() {
		// manage slideshow

		if (
			Utilities.isMap() ||
			Utilities.isPopup() ||
			env.currentMedia !== null && env.currentAlbumIsAlbumWithOneMedia ||
			env.currentMedia === null && ! env.currentAlbum.visibleNumsMedia().imagesAudiosVideosTotal() ||
			env.currentAlbum.isABigTransversalAlbum()
		) {
			$("#slideshow-icon").hide();
			$("#slideshow-icon").off("click");
		} else {
			$("#slideshow-icon").show();
			$("#slideshow-icon").off("click").on(
				"click",
				function(ev) {
					ev.stopPropagation();
					env.currentAlbum.slideshow(ev);
				}
			);
		}
	};

	Utilities.prototype.setSocialButtons = function() {
		function socialButtonsOptionToArray() {
			if (env.options.social_buttons === "none")
				return [];
			return env.options.social_buttons.replace(/"/g, "").replace(/,/g, " ").replace(/  /g, " ").trim().split(" ");
		}

		function setCopyButtonAttributes() {
			$(".ssk-show-url img")
				.attr("alt", Utilities._t("#tiny-url-icon-alt-text"))
				.attr("title", Utilities._t("#tiny-url-icon-title"));
		}

		function getReducedSizeIndex(optimalSize) {
			let absoluteDifferences = env.options.reduced_sizes.map(size => Math.abs(size - optimalSize));
			let minimumDifference = Math.min(... absoluteDifferences);
			reducedSizesIndex = absoluteDifferences.findIndex(size => size === minimumDifference);
			return reducedSizesIndex;
		}

		function showSelectedSocials() {
			for (let socialButton of socialButtonsOptionToArray()) {
				if (socialButton === "google")
					socialButton += "-plus";
				$(".ssk.ssk-" + socialButton).removeClass("hidden");
			}
		}

		function setSocialButtonsOptions() {
			var socialSizeClass;

			if (env.options.social_size === "small")
				socialSizeClass = "ssk-xs";
			else if (env.options.social_size === "large")
				socialSizeClass = "ssk-lg";
			else
				socialSizeClass = "ssk-sm";
			$(".ssk-group").addClass(socialSizeClass);

			if (! env.options.social_color)
				$(".ssk-group").addClass("ssk-grayscale");
		}
		////// end of nested functions /////////////////////////

		// if (typeof SocialShareKit === "undefined")
		// 	return;

		if (
			env.options.social_buttons === "none" ||
			Utilities.isMap()
		) {
			$("#social").hide();
			return;
		} else {
			$("#social").show();
		}

		showSelectedSocials();
		setSocialButtonsOptions();
		setCopyButtonAttributes();

		var hash, myShareUrl = "";
		var mediaParameter, mediaWidth, mediaHeight, whatsappMediaWidth, whatsappMediaHeight;
		var widthParameter, heightParameter, whatsappWidthParameter, whatsappHeightParameter;
		var myShareText;

		if (! env.isAnyMobile) {
			$(".ssk-whatsapp").hide();
		} else {
			$(".ssk-whatsapp").show();
		}

		var urlWithoutHash = location.href.split("#")[0];

		// image size for sharing must be > 200 and ~ 1200x630, https://kaydee.net/blog/open-graph-image/
		// but whatsapp doesn't show some image with size 1200 => reduce
		var reducedSizesIndex, whatsappReducedSizesIndex;
		if (! env.options.reduced_sizes.length || Math.max(... env.options.reduced_sizes) < 200)
			// false means original image: it won't be used
			reducedSizesIndex = false;
		else {
			// use the size nearest to optimal
			reducedSizesIndex = getReducedSizeIndex(env.sharingSize);
			whatsappReducedSizesIndex = getReducedSizeIndex(env.whatsappSharingSize);
		}

		var whatsAppMediaParameter = "";

		if (
			env.currentMedia === null ||
			reducedSizesIndex === false
		) {
			// use the album composite image, if it exists; otherwise, use MyPhotoShare logo
			if (env.currentAlbum && env.currentAlbum.hasOwnProperty("compositeImageSize")) {
				mediaParameter = Utilities.pathJoin([
					"cache",
					env.options.composite_images_subdir,
					// always use jpg image for sharing
					env.currentAlbum.cacheBase + ".jpg"
					]);
				widthParameter = env.currentAlbum.compositeImageSize;
				heightParameter = env.currentAlbum.compositeImageSize;
			} else {
				mediaParameter = Utilities.pathJoin([
					"cache",
					env.options.composite_images_subdir_js,
					// always use jpg image for sharing
					env.currentAlbum.cacheBase + ".jpg"
					]);
				// width and height values will be calculated by php from the composite image passed by POST
			}
		} else {
			// current media !== null
			if (env.currentMedia.hasOwnProperty("protected")) {
				mediaParameter = env.logo;
				widthParameter = env.logoSize;
				heightParameter = env.logoSize;
			} else {
				let prefix = Utilities.removeFolderString(env.currentMedia.foldersCacheBase);
				if (prefix)
					prefix += env.options.cache_folder_separator;

				if (env.currentMedia && env.currentMedia.isAudio() || env.currentMedia.isVideo() || ! env.options.reduced_sizes.length) {
					mediaParameter = env.currentMedia.sharingMediaPath(true);
					widthParameter = env.currentMedia.width();
					heightParameter = env.currentMedia.height();
				} else if (env.currentMedia && env.currentMedia.isImage()) {
					mediaWidth = env.currentMedia.width();
					mediaHeight = env.currentMedia.height();
					whatsappMediaWidth = mediaWidth;
					whatsappMediaHeight = mediaHeight;

					// always use jpg image for sharing
					mediaParameter = env.currentMedia.reducedMediaPath(env.options.reduced_sizes[reducedSizesIndex], "jpg");
					whatsAppMediaParameter = env.currentMedia.reducedMediaPath(env.options.reduced_sizes[whatsappReducedSizesIndex], "jpg");

					if (mediaWidth > mediaHeight) {
						widthParameter = env.options.reduced_sizes[reducedSizesIndex];
						heightParameter = Math.round(env.options.reduced_sizes[reducedSizesIndex] * mediaHeight / mediaWidth);
						whatsappWidthParameter = env.options.reduced_sizes[whatsappReducedSizesIndex];
						whatsappHeightParameter = Math.round(env.options.reduced_sizes[whatsappReducedSizesIndex] * mediaHeight / mediaWidth);
					} else {
						heightParameter = env.options.reduced_sizes[reducedSizesIndex];
						widthParameter = Math.round(env.options.reduced_sizes[reducedSizesIndex] * mediaWidth / mediaHeight);
						whatsappWidthParameter = env.options.reduced_sizes[whatsappReducedSizesIndex];
						whatsappHeightParameter = Math.round(env.options.reduced_sizes[whatsappReducedSizesIndex] * mediaWidth / mediaHeight);
					}
				}
			}
		}

		myShareText = env.options.page_title[env.language] + " : ";
		if (env.currentAlbum) {
			if (env.currentMedia)
					myShareText += env.currentMedia.name;
				else if (Utilities.isPopup())
					myShareText += env.mapAlbum.name;
				else
					myShareText += env.currentAlbum.name;

			myShareUrl = urlWithoutHash;
			myShareUrl += "?url=" + encodeURIComponent(urlWithoutHash);
			myShareUrl += "&title=" + encodeURIComponent(myShareText);
			if (env.currentMedia !== null && env.currentMedia.metadata.hasOwnProperty("title") && env.currentMedia.metadata.title)
				myShareUrl += "&desc=" + encodeURIComponent(env.currentMedia.metadata.title);
			else if (! env.currentMedia && env.currentAlbum.hasOwnProperty("title") && env.currentAlbum.title)
				myShareUrl += "&desc=" + encodeURIComponent(env.currentAlbum.title);
			hash = location.hash;
			if (Utilities.isPopup() && ! env.isPlayingMapClickHistoryInGetAlbum)
				hash = env.hashBeginning + env.mapAlbum.cacheBase;
			if (hash) {
				myShareUrl += "&hash=" + encodeURIComponent(hash.substring(1));
			} else {
				myShareUrl += "&hash=";
			}
			if (env.options.debug_js)
				// the following line is needed for debugging purposes in order to bypass the server cache;
				// without the random number, the php code is executed only the first time the url is loaded,
				// in subsequent requests the content is fetched from cache
				myShareUrl += "&r=" + Math.floor(Math.random() * 10000000);
			var whatsAppUrl = myShareUrl;
			if (widthParameter !== undefined) {
				myShareUrl += "&w=" + widthParameter;
				myShareUrl += "&h=" + heightParameter;
			}
			myShareUrl += "&m=" + encodeURIComponent(mediaParameter);
			if (whatsAppMediaParameter) {
				if (whatsappWidthParameter !== undefined) {
					whatsAppUrl += "&w=" + whatsappWidthParameter;
					whatsAppUrl += "&h=" + whatsappHeightParameter;
				}
				whatsAppUrl += "&m=" + encodeURIComponent(whatsAppMediaParameter);
			} else {
				whatsAppUrl = myShareUrl;
			}
			if (hash) {
				myShareUrl += hash;
				whatsAppUrl += hash;
			}

			jQuery.removeData(".ssk");
			$(".ssk").attr("data-text", myShareText);
			$(".ssk-facebook").attr("data-url", myShareUrl);
			$(".ssk-whatsapp").attr("data-url", whatsAppUrl);
			$(".ssk-twitter").attr("data-url", whatsAppUrl);
			$(".ssk-google-plus").attr("data-url", myShareUrl);
			$(".ssk-email").attr("data-url", location.href);
		}

		$(".ssk-show-url").off("click").on(
			"click",
			function(ev) {
				let promise = Utilities.generateCompositeImageAndGetMd5ForTinyUrl(whatsAppUrl);
				promise.then(
					function(tinyUrlParameter) {
						$("#album-and-media-container").stop().fadeTo(500, 0.1);
						var shareUrl = window.location.origin + window.location.pathname + "?t=" + tinyUrlParameter;
						$("#tiny-url").html(Utilities._t("#tiny-url"));
						$("#tiny-url").append(
							"<form id='tiny-url-form'>" +
								"<input type='text' id='tiny-url-for-copying' name='url' value='" + shareUrl + "'>" +
								"<input type='submit' id='tiny-url-button' value='" + Utilities._t("#tiny-url-button") + "'>" +
							"</form>"
						);
						$("#tiny-url-button").off("click").on(
							"click",
							function() {
								document.execCommand('copy');
								$("#tiny-url").append("<div>" + Utilities._t("#tiny-url-copied") + "</div>");
								$("#tiny-url").fadeOut(4000);
								$("#album-and-media-container").stop().fadeTo(4000, 1);
								return false;
							}
						);
						$("#tiny-url").fadeIn(500);
						$("#tiny-url-for-copying").trigger("focus").select();
					}
				);

				return false;
			}
		);

		// initialize social buttons (http://socialsharekit.com/)
		SocialShareKit.init({});
		if (! Modernizr.flexbox && Utilities.bottomSocialButtons()) {
			var numSocial = 5;
			var socialWidth = Math.floor(window.innerWidth / numSocial);
			$('.ssk').width(socialWidth * 2 + "px");
		}
	};

	Utilities.generateCompositeImageAndGetMd5ForTinyUrl = function(whatsAppUrl) {
		function generateCompositeImage(imagePaths) {
			return new Promise(
				function(resolve_generateCompositeImage) {
					// this function loads the image
					function loadImage(path) {
						return new Promise(
							function (resolve_loadImage, reject_loadImage) {
								let image = new Image();
								image.onload = function() {
									resolve_loadImage(image);
								};
								image.src = path;
							}
						);
					}

					// this function redraws the images into the canvas
					function drawImages(images) {
						if (imagePaths.length === 1) {
							if (env.currentMedia) {
								canvas.width = env.options.reduced_size_for_sharing;
								canvas.height = canvas.width / env.currentMedia.width() * env.currentMedia.height();
							} else {
								canvas.width = images[0].width;
								canvas.height = images[0].height;
							}

							context.drawImage(images[0], 0, 0);
						} else {
							let cellSize = Math.round((env.whatsappSharingSize) / linearNumMedia);
							canvas.width = ((cellSize + 1) * linearNumMedia) + 1;
							// canvas.width = (env.options.media_thumb_size + 1) * linearNumMedia + 1;
							canvas.height = canvas.width;

							// fill with white
							context.fillStyle = "white";
							context.fillRect(0, 0, canvas.width, canvas.height);


							images.forEach(
								function(image, index) {
									let row = Math.floor(index / linearNumMedia);
									let col = index % linearNumMedia;
									let positionX = col * (cellSize + 1) + 1;
									let positionY = row * (cellSize + 1) + 1;
									context.drawImage(image, positionX, positionY, cellSize, cellSize);
								}
							);
						}
					}
					// end of nested functions of generateCompositeImage()

					// create a canvas element
					let canvas = document.createElement("canvas");
					let context = canvas.getContext("2d");

					// load and resize the images
					var images = [];
					var loadedCount = 0;
					imagePaths.forEach(
						function(path) {
							let loadImagePromise = loadImage(path);
							loadImagePromise.then(
								function(image) {
									images.push(image);
									loadedCount++;
									if (loadedCount === imagePaths.length) {
										drawImages(images);
										resolve_generateCompositeImage(canvas);
									}
								},
								function() {
									console.trace();
								}
							);
						}
					);
				}
			);
		}
		// end of nested function

		let linearNumMedia;
		let albumToUse = env.currentAlbum;
		if (Utilities.isPopup())
			albumToUse = env.mapAlbum;
		$("#working").show();
		return new Promise(
			function(resolve_generateCompositeImageAndGetMd5ForTinyUrl) {
				let numMedia, randomPromise;
				if (albumToUse.isCollection() && ! env.currentMedia && env.options.reduced_size_for_sharing) {
					// calculate how many images are to be put into the composite image
					numMedia = env.options.max_composite_image_thumbnails_number;
					let numsVisibleMediaInSubtree = albumToUse.numsVisibleMediaInSubtree();
					let numVisibleImagesAndVideosInSubtree = numsVisibleMediaInSubtree.images + numsVisibleMediaInSubtree.videos;
					while (numMedia > 1 && numMedia > numVisibleImagesAndVideosInSubtree) {
						numMedia = Math.pow((Math.sqrt(numMedia) - 1), 2);
					}
					linearNumMedia = Math.sqrt(numMedia);

					// get the random media for the composite image
					randomPromise = albumToUse.pickRandomMediaForCompositeImage(numMedia);
					randomPromise.then(
						function(relativePaths) {
							if (relativePaths !== null) {
								let compositeImagePromise = generateCompositeImage(relativePaths);
								compositeImagePromise.then(
									function(canvas) {
										let imageData = canvas.toDataURL("image/jpeg", env.options.jpeg_quality);
										// replace the base64 characters which are not url-safe: "+/=" -> "._-"
										let imageDataUrl = imageData.replace(/\+/g, '.').replace(/\//g, '_').replace(/\=/g, '-');
										let md5Promise = Utilities.getMd5ForTinyUrl(whatsAppUrl, imageDataUrl);
										md5Promise.then(
											function(tinyUrlParameter) {
												$("#working").hide();
												resolve_generateCompositeImageAndGetMd5ForTinyUrl(tinyUrlParameter);
											}
										);
									}
								);
							}
						}
					);
				} else {
					let md5Promise = Utilities.getMd5ForTinyUrl(whatsAppUrl);
					md5Promise.then(
						function(tinyUrlParameter) {
							$("#working").hide();
							resolve_generateCompositeImageAndGetMd5ForTinyUrl(tinyUrlParameter);
						}
					);
				}
			}
		);
	};

	Utilities.prototype.specifyNumberOfImagesAudiosVideos = function(nums, numsNonGeotagged) {
		let text = "", thisClass, numImages, numAudios, numVideos;
		let textImages, textAudios, textVideos, textTotal;
		let first = true;
		for (let what of [nums, numsNonGeotagged]) {
			if (first) {
				thisClass = "all";
				numImages = what.images;
				numAudios = what.audios;
				numVideos = what.videos;
				first = false;
			} else {
				thisClass = "non-gps";
				numImages = env.options.expose_image_positions ? what.images : 0;
				numAudios = env.options.expose_image_positions ? what.audios : 0;
				numVideos = env.options.expose_image_positions ? what.videos : 0;
			}

			let arrayText = [];
			if (numImages) {
				textImages =
					env.numberFormat.format(numImages) +
					"&nbsp;" +
					Utilities._t(".title-images");
				arrayText.push(textImages);
			}
			if (numAudios) {
				textAudios =
					env.numberFormat.format(numAudios) +
					"&nbsp;" +
					Utilities._t(".title-audios");
				arrayText.push(textAudios);
			}
			if (numVideos) {
				textVideos =
					env.numberFormat.format(numVideos) +
					"&nbsp;" +
					Utilities._t(".title-videos");
				arrayText.push(textVideos);
			}

			if (arrayText.length)
				text += "<span class='" + thisClass + "'>";

			if (arrayText.length > 1) {
				textTotal =
					Utilities._t(".title-total") +
					"&nbsp;" +
					env.numberFormat.format(what.imagesAudiosVideosTotal());
				text += arrayText.join(", ");
			} else if (numImages) {
				text += textImages;
			} else if (numAudios) {
				text += textAudios;
			} else if (numVideos) {
				text += textVideos;
			}

			if (arrayText.length)
				text += "</span>";
		}

		return text;
	};

	Utilities.getMd5ForTinyUrl = function(myphotoshareUrl, imageDataUrl = null) {
		return new Promise(
			function(resolve_getMd5ForTinyUrl) {
				if (Utilities.isPhp()) {
					let request = new XMLHttpRequest();
					let url = window.location.origin + window.location.pathname;
					// let url = window.location.origin + window.location.pathname + "?longurl=" + encodeURIComponent(myphotoshareUrl);
					let params = "longurl=" + encodeURIComponent(myphotoshareUrl);
					if (imageDataUrl)
						params += "&compositeimage=" + imageDataUrl;
					request.onreadystatechange = function() {
						if (this.readyState === 4) {
							if (this.status >= 200 && this.status < 400) {
								resolve_getMd5ForTinyUrl(this.responseText);
							}
						}
					};
					request.open("POST", url, true);
					request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					// request.setRequestHeader('Content-Type', 'application/json');
					request.send(params);
				} else {
					// not php
					resolve_getMd5ForTinyUrl(false);
				}
			}
		);
	};

	Utilities.prototype.albumSelectBoxSelector = function(cacheBase) {
		return "#" + env.albumSelectBoxIdBeginning + PhotoFloat.convertCacheBaseToId(cacheBase);
	};

	Utilities.modifyCheckedState = function(selector, state) {
		if (state)
			$(selector + " img").attr("src", "img/checkbox-checked-48px.png").attr("title", Utilities._t("#unselect-single-media"));
		else
			$(selector + " img").attr("src", "img/checkbox-unchecked-48px.png").attr("title", Utilities._t("#select-single-media"));
	};

	Utilities.prototype.modifyCheckedStates = function(multipleSelector, state) {
		if (state)
			$(multipleSelector).attr("src", "img/checkbox-checked-48px.png").attr("title", Utilities._t("#unselect-single-media"));
		else
			$(multipleSelector).attr("src", "img/checkbox-unchecked-48px.png").attr("title", Utilities._t("#select-single-media"));
	};


	Utilities.addClickToPopupImage = function(jqueryObject) {
		jqueryObject.parent().parent().off("click").on(
			"click",
			function(ev) {
				$("#album-and-media-container").addClass("single-media");
				ev.stopPropagation();
				ev.preventDefault();
				var imgData = JSON.parse(jqueryObject.attr("data"));
				// called after the jqueryObject was successfully handled
				env.highlightedObjectId = null;
				if (Utilities.isShiftOrControl())
					$(".shift-or-control .leaflet-popup-close-button")[0].click();
				$(".media-popup .leaflet-popup-close-button")[0].click();
				// $('#popup #popup-content').html("");
				$('.modal-close')[0].click();
				env.popupRefreshType = "previousAlbum";
				env.mapRefreshType = null;
				window.location.href = imgData.mediaHash;
			}
		);
		if (Utilities.isPhp) {
			// execution enters here if we are using index.php
			jqueryObject.parent().parent().off("auxclick").on(
				"auxclick",
				function (ev) {
					if (ev.which === 2) {
						var imgData = JSON.parse(jqueryObject.attr("data"));
						Utilities.openInNewTab(imgData.mediaHash);
						return false;
					}
				}
			);
		}

		var mediaBoxSelectElement = jqueryObject.siblings('a');
		var id = mediaBoxSelectElement.attr("id");
		mediaBoxSelectElement.off("click").on(
			"click",
			{id: id, clickedSelector: "#" + id},
			function(ev) {
				ev.stopPropagation();
				ev.preventDefault();
				var imgData = JSON.parse(jqueryObject.attr("data"));
				var cachedAlbum = env.cache.getAlbum(imgData.albumCacheBase);
				var name = imgData.mediaHash.split('/').pop();
				var matchedMedia = cachedAlbum.media.find(singleMedia => name === singleMedia.cacheBase);
				var promise = PhotoFloat.getAlbum(matchedMedia.foldersCacheBase, null, {getMedia: true, getPositions: ! env.options.save_data});
				promise.then(
					function(foldersAlbum) {
						if (Utilities.absolutelyNothingIsSelected())
							env.selectionAlbum = Utilities.initializeSelectionAlbum();

						env.selectionAlbum.addToSelectionClickHistory(foldersAlbum, ev.data.clickedSelector);
						if (! matchedMedia.isSelected())
							matchedMedia.generateCaptionsForSelection(env.mapAlbum);
						matchedMedia.toggleSingleMediaSelection(env.selectionAlbum);
						Utilities.modifyCheckedState(ev.data.clickedSelector, matchedMedia.isSelected());

						if (env.currentAlbum.isSelection()) {
							if (Utilities.isShiftOrControl())
								$(".shift-or-control .leaflet-popup-close-button")[0].click();
							$(".media-popup .leaflet-popup-close-button")[0].click();
							if (env.mapAlbum.media.length > 1) {
								env.popupRefreshType = "mapAlbum";
								// close the map and reopen it
								$('.modal-close')[0].click();
								$(ev.data.clickedSelector).trigger("click", ["fromTrigger"]);
							} else {
								// close the map
								$('.modal-close')[0].click();
							}
						}

						if (Utilities.absolutelyNothingIsSelected()) {
							env.selectionAlbum = Utilities.initializeSelectionAlbum();
							window.location.hash = Utilities.upHash();
						} else {
							env.isASelectionChange = true;
							env.currentAlbum.showMedia();
							env.isASelectionChange = false;
							// window.location.hash = Utilities.encodeHash(env.selectionAlbum.cacheBase, null);
						}

						MenuFunctions.updateMenu();
					}
				);
			}
		);
	};

	Utilities.conditionalLoadMatomoTracking = function() {
		if (! env.matomoLoaded) {
			TopFunctions.trackMatomo();
			env.matomoLoaded = true;
		}
	};

	Utilities.conditionalLoadOthersJsFiles = function() {
		if (! env.otherJsFilesTriggeredForLoading) {
			env.otherJsFilesTriggeredForLoading = true;
			$(document).ready(
				function() {
					$.triggerEvent("readyToLoadOtherJsFilesEvent");
				}
			);
		}
	};

	Utilities.prototype.loadOtherJsCssFiles = function() {
		if (env.loadOtherJsCssFilesAlreadyCalled)
			return;

		if (env.options.save_data)
			$.triggerEvent("cssFontsLoadedEvent");

		let loadJSPromises = [];
		env.loadOtherJsCssFilesAlreadyCalled = true;

		if (! env.options.debug_js) {
			loadJSPromises.push(loadJS("scripts.2.min.js"));

			let script_Promise = new Promise(
				function(resolve_script3Promise) {
					$.executeAfterEvent(
						"leafletLoadedEvent",
						function() {
							loadJS("scripts.3.min.js").then(
								function() {
									resolve_script3Promise();
								}
							);
						}
					);
				}
			);
			loadJSPromises.push(script_Promise);
		} else {
			let jsFiles = [
				"006-jquery-touchswipe.js",
				"008-leaflet.js",
				"009-leaflet-prunecluster.js",
				"010-social.js",
				"011-jszip.js",
				"012-jszip-utils.js",
				"013-md5.js",
				"014-file-saver.js",
				"015-jquery-mark.js",
				// "016-lzw-compress.js", DO NOT ADD AGAIN, IT IS NEEDED VERY EARLY
				"033-utilities-bis.js",
				"035-pinch-swipe.js",
				"037-map.js",
				"038-top-functions-bis.js",
				"041-album-methods-bis.js",
				"043-single-media-methods-bis.js",
				"045-positions-and-media-methods-bis.js",
				"047-selection-methods-bis.js",
				"048-map-methods-bis.js",
				"050-display-bis.js"
			];
			for (let i = 0; i < jsFiles.length; i ++) {
				if (jsFiles[i] === "009-leaflet-prunecluster.js" || jsFiles[i] === "037-map.js") {
					let leafletOrMapPromise = new Promise(
						function(resolve_leafletOrMapPromise) {
							$.executeAfterEvent(
								"leafletLoadedEvent",
								function() {
									loadJS(jsFiles[i]).then(
										function() {
											resolve_leafletOrMapPromise();
										}
									);
								}
							);
						}
					);
					loadJSPromises.push(leafletOrMapPromise);
				} else {
					loadJSPromises.push(loadJS(jsFiles[i]));
				}
			}
		}
		Promise.all(loadJSPromises).then(
			function() {
				$.triggerEvent("otherJsFilesLoadedEvent");
			}
		);

		if (! env.options.debug_css) {
			if (env.options.save_data)
				loadCss("styles.save_data.2.min.css");
			else
				loadCss("styles.no_save_data.2.min.css");
		} else {
			let cssFiles2 = [];
			cssFiles2.push("000-controls-bis.css");

			if (! env.options.save_data)
				cssFiles2.push("001-fonts.css");

			cssFiles2 = cssFiles2.concat(
				[
					"003-social.css",
					"005-leaflet.css",
					"006-map-popup.css",
					"010-leaflet-prunecluster.css"
				]
			);

			for (let i = 0; i < cssFiles2.length; i ++)
				loadCss(cssFiles2[i]);
		} //
	};


	Utilities.addMediaLazyLoader = function(trackMatomo) {
		// the boolean trackMatomo is in order to decide whether to track or not
		var threshold = env.options.media_thumb_size;
		if (env.options.save_data)
			threshold = 0;
		$(
			function() {
				$("img.lazyload-popup-media").Lazy(
					{
						afterLoad: function(jqueryObject) {
							Utilities.addClickToPopupImage(jqueryObject);
						},
						autoDestroy: true,
						onError: function(jqueryObject) {
							console.log(jqueryObject[0]);
						},
						chainable: false,
						threshold: threshold,
						scrollDirection: "vertical",
						appendScroll: $('#popup-images-wrapper')
					}
				);

				$("#album-and-media-container:not(.single-media) #thumbs img.lazyload-media").Lazy(
					{
						afterLoad: function() {
							if (trackMatomo)
								Utilities.conditionalLoadMatomoTracking();
							Utilities.conditionalLoadOthersJsFiles();
						},
						threshold: threshold,
						scrollDirection: "vertical",
						appendScroll: $(window)
					}
				);

				$("#album-and-media-container.single-media #thumbs img.lazyload-media").Lazy(
					{
						afterLoad: function() {
							if (trackMatomo)
								Utilities.conditionalLoadMatomoTracking();
								// Utilities.conditionalLoadOthersJsFiles();
						},
						threshold: threshold,
						scrollDirection: "horizontal",
						appendScroll: $("#album-view")
					}
				);
			}
		);
	};

	Utilities.prototype.getMediaFromSubalbumObject = function(object) {
		let hash = object.parent().children(".random-media-link").attr("href");
		let splittedHash = hash.split("/");
		let randomMediaCacheBase = splittedHash.pop();
		let randomMediaAlbumCacheBase = splittedHash.pop();
		let randomMedia = env.cache.getMedia(randomMediaAlbumCacheBase, randomMediaCacheBase);
		return [randomMedia, randomMediaAlbumCacheBase];
	};

	Utilities.prototype.dateElementForFolderName = function(folderArray, index) {
		if (index === 1 || index === 3)
			return parseInt(folderArray[index]);
		else if (index === 2)
			return Utilities._t("#month-" + folderArray[index]);
	};

	Utilities.prototype.addSpanToFirstAndSecondLine = function(firstLine, secondLine) {
		var result = [], index = 0;
		if (firstLine) {
			result[index] = "<span class='first-line'>" + firstLine + "</span>";
			index ++;
		}
		if (secondLine)
			result[index] = "<span class='second-line'>" + secondLine + "</span>";
		return result;
	};

	Utilities.prototype.convertByDateAncestorNames = function(ancestorsNames) {
		if (ancestorsNames[0] === env.options.by_date_string && ancestorsNames.length > 2) {
			let result = ancestorsNames.slice();
			result[2] = Utilities._t("#month-" + result[2]);
			return result;
		} else {
			return ancestorsNames;
		}

	};

	Utilities.prototype.nameForShowing = function(albumOrSubalbum, parentAlbum, html, br) {
		var folderName = '';
		if (albumOrSubalbum.cacheBase === env.options.by_date_string) {
			folderName = "(" + Utilities._t("#by-date") + ")";
		} else if (albumOrSubalbum.cacheBase === env.options.by_gps_string) {
			folderName = "(" + Utilities._t("#by-gps") + ")";
		} else if (parentAlbum && parentAlbum.isByDate() || albumOrSubalbum.isByDate()) {
			let folderArray = albumOrSubalbum.cacheBase.split(env.options.cache_folder_separator);
			if (folderArray.length === 2) {
				folderName += parseInt(folderArray[1]);
			} else if (folderArray.length === 3)
				folderName += " " + Utilities._t("#month-" + folderArray[2]);
			else if (folderArray.length === 4)
				folderName += Utilities._t("#day") + " " + parseInt(folderArray[3]);
		} else if (parentAlbum && parentAlbum.isByGps() || albumOrSubalbum.isByGps()) {
			if (albumOrSubalbum.name === '')
				folderName = Utilities._t('.not-specified');
			else if (albumOrSubalbum.hasOwnProperty('altName'))
				folderName = Utilities.transformAltPlaceName(albumOrSubalbum.altName);
			else
				folderName = albumOrSubalbum.name;
		} else if (albumOrSubalbum.hasOwnProperty("title") && albumOrSubalbum.title && albumOrSubalbum.title !== albumOrSubalbum.name) {
			folderName = albumOrSubalbum.title;
			if (! br) {
				// remove the tags fronm the title
				folderName = folderName.replace(/<[^>]*>?/gm, ' ');
			}

			if (albumOrSubalbum.name) {
				if (html && br)
					folderName += env.br + "<span class='real-name'>[" + albumOrSubalbum.name + "]</span>";
				else if (html)
					folderName += " <span class='real-name'>[" + albumOrSubalbum.name + "]</span>";
				else
					folderName += " [" + albumOrSubalbum.name + "]";
			}
		} else {
			folderName = albumOrSubalbum.name;
		}

		return folderName;
	};

	Utilities.setPrevNextPosition = function() {
		var titleHeight = 0;
		if ($(".media-box#center .title").is(":visible"))
			titleHeight = $(".media-box#center .title").outerHeight();

		var prevNextHeight = parseInt($("#next").outerHeight());
		if (! env.fullScreenStatus || ! $(".media-box#center").hasClass("rotation")) {
			// position next/prev buttons verticallly centered in media-box-inner
			var mediaBoxInnerHeight = parseInt($(".media-box#center .media-box-inner").css("height"));
			$("#next, #prev").css("top", titleHeight + (mediaBoxInnerHeight - prevNextHeight) / 2);
			$("#prev, #next").css("right", "");
			$("#prev, #next").removeClass("rotate-90");
			$("#prev").css("left", 0);
			// $("#prev").css("top", "");
			$("#next").css("right", 0);
			$("#next").css("bottom", "");

			Utilities.setLinksVisibility();
		} else {
			let prevNextWidth = parseInt($("#next").outerWidth());
			let mediaBoxInnerWidth = parseInt($(".media-box#center .media-box-inner").css("width"));
			$("#prev, #next").css("top", "");
			$("#prev, #next").css("right", (mediaBoxInnerWidth - prevNextHeight) / 2);
			$("#prev, #next").addClass("rotate-90");
			$("#prev").css("left", "unset");
			$("#prev").css("top", - prevNextWidth / 2);
			$("#next").css("bottom", - prevNextWidth / 2);
		}
	};

	Utilities.setSlideshowButtonsPosition = function() {
		let slideshowContainerHeight = parseInt($("#slideshow-buttons").outerHeight());
		let slideshowContainerWidth = parseInt($("#slideshow-buttons").outerWidth());
		if (env.slideshowId && env.currentMedia.needsAutoRotation()) {
			$("#slideshow-buttons").addClass("rotate-90");
			$("#slideshow-buttons").css("bottom", ((env.windowHeight - slideshowContainerHeight) / 2).toString() + "px");
			$("#slideshow-buttons").css("left", "");
			$("#slideshow-buttons").css("right", "");
			if (env.currentMedia.isImage() || env.currentMedia.isAudio())
				$("#slideshow-buttons").css(
					"left",
					($("#prev").outerWidth(true) + 10 - (slideshowContainerWidth - slideshowContainerHeight) / 2).toString() + "px"
				);
			else if (env.currentMedia.isVideo())
				$("#slideshow-buttons").css(
					"right",
					($("#next").outerWidth(true) + 10 - (slideshowContainerWidth - slideshowContainerHeight) / 2).toString() + "px"
				);
		} else {
			$("#slideshow-buttons").removeClass("rotate-90");
			$("#slideshow-buttons").css("left", ((env.windowWidth - slideshowContainerWidth) / 2).toString() + "px");
			$("#slideshow-buttons").css("bottom", "20px");
		}
	};

	Utilities.setMapButtonPosition = function(containerHeight, containerWidth) {
		// calculate and set pinch buttons position

		var mediaElement;
		if (env.currentMedia) {
			if (env.currentMedia.isImage())
				mediaElement = $(".media-box#center .media-box-inner img");
			else if (env.currentMedia.isAudio())
				mediaElement = $(".media-box#center .media-box-inner audio");
			else if (env.currentMedia.isVideo())
				mediaElement = $(".media-box#center .media-box-inner video");
			if (mediaElement[0] === undefined)
				return;
		} else {
			return;
		}

		var titleHeight, thumbsHeight;
		if ($(".media-box#center .title").is(":visible"))
			titleHeight = $(".media-box#center .title").outerHeight();
		else
			titleHeight = 0;
		if ($("#thumbs").is(":visible"))
			thumbsHeight = $("#album-view").outerHeight();
		else
			thumbsHeight = 0;
		var distanceFromImageBorder = 15;
		containerHeight = env.windowHeight - titleHeight - thumbsHeight;
		containerWidth = env.windowWidth;

		let right;
		let imageRatio = env.currentMedia.width() / env.currentMedia.height();
		let mapButtonWidth = parseInt($("#map-button-container").outerWidth());
		if (env.slideshowId && env.currentMedia.needsAutoRotation()) {
			$("#map-button-container").css("top", "");
			let windowRatio = env.windowWidth / env.windowHeight;
			let bottom;
			let rotatedImageRatio = 1 / imageRatio;
			$("#map-button-container").addClass("rotate-90");
			if (windowRatio > rotatedImageRatio) {
				right = Math.round((env.windowWidth - env.windowHeight * rotatedImageRatio) / 2) + mapButtonWidth + distanceFromImageBorder;
				bottom = distanceFromImageBorder;
			} else {
				right = mapButtonWidth + distanceFromImageBorder;
				bottom = Math.round((env.windowHeight - env.windowWidth / rotatedImageRatio) / 2) + distanceFromImageBorder;
			}
			$("#map-button-container").css("bottom", bottom.toString() + "px");
		} else {
			$("#map-button-container").css("bottom", "");
			$("#map-button-container").removeClass("rotate-90");
			let containerRatio = containerWidth / containerHeight;
			let top;
			if (containerRatio > imageRatio) {
				right = Math.round((containerWidth - containerHeight * imageRatio) / 2) + distanceFromImageBorder;
				top = titleHeight + distanceFromImageBorder;
			} else {
				right = distanceFromImageBorder;
				top = titleHeight + Math.round((containerHeight - containerWidth / imageRatio) / 2) + distanceFromImageBorder;
			}
			$("#map-button-container").css("top", top.toString() + "px");
		}
		$("#map-button-container").css("right", right.toString() + "px");
	};

	Utilities.setSelectButtonVisibility = function() {
		$("#select-box").show();
	};

	Utilities.setUpButtonVisibility = function() {
		if (
			Utilities.isMap() ||
			Utilities.isPopup() ||
			! env.currentAlbum || (
				env.currentMedia === null && (
					! env.currentAlbum.isMap() &&
					$("#album-view .title").is(":visible") &&
					! env.isAnyMobile ||
					env.currentAlbum.cacheBase === env.options.folders_string
				) ||
				env.currentMedia !== null && (
					$(".media-box#center .title").is(":visible") &&
					! env.isAnyMobile ||
					env.currentZoom > env.initialZoom ||
					env.fullScreenStatus
				)
			)
		) {
			$("#up-button").hide();
		} else {
			$("#up-button").show();

			// // move the up button below the title
			// $("#up-button").css(
			// 	"top",
			// 	($("#album-view .title").outerHeight(true) + 15) + "px"
			// );
		}
	};


	Utilities.setMapButtonVisibility = function() {
		$("#map-button-container").removeClass("hidden");

		if (! env.currentMedia)
			return;

		if (
			env.currentMedia.hasGpsData() || (
				Utilities.isPhp() &&
				env.options.user_may_suggest_location &&
				env.options.request_password_email &&
				! env.currentMedia.isAudio()
			)
		) {
			let imgHtml;
			if (env.currentMedia.hasGpsData())
				imgHtml = "<img class='title-img gps' width='30' height='42' src='img/ic_place_white_24dp_2x.png'>";
			else
				imgHtml = "<img class='title-img' width='48' height='42' src='img/ic_place_white_24dp_2x_with_plus.png'>";
			let imgObject = $(imgHtml);
			let imgTitle;
			if (env.currentMedia.hasGpsData())
				imgTitle = Utilities._t("#show-on-map");
			else
				imgTitle = Utilities._t("#suggest-position-on-map");
			if (! env.isAnyMobile)
				imgTitle += " [" + Utilities._s(".map-link-shortcut") + "]";
			imgObject.attr("title", imgTitle);
			imgObject.attr("alt", imgTitle);
			$("#map-button-container").html(imgObject);

			$("#map-button-container").show();
		} else {
			$("#map-button-container").hide();
		}
	};

	Utilities.getScale = function(jqueryObject) {
		let scaleParameter = jqueryObject[0].style.transform.match(/scale\((.*?)\)/);
		if (scaleParameter === null)
			return 1;
		else
			return scaleParameter[1];
	};

	Utilities.scaledWidth = function(jqueryObject) {
		let originalWidth = jqueryObject.width();
		return originalWidth * Utilities.getScale(jqueryObject);
	};

	Utilities.scaledHeight = function(jqueryObject) {
		let originalHeight = jqueryObject.height();
		return originalHeight * Utilities.getScale(jqueryObject);
	};

	Utilities.setPinchButtonsPosition = function(containerHeight, containerWidth) {
		// calculate and set pinch buttons position

		var mediaElement = $(".media-box#center .media-box-inner img");
		if (mediaElement[0] === undefined)
			return;
		var titleHeight, thumbsHeight;
		if ($(".media-box#center .title").is(":visible"))
			titleHeight = $(".media-box#center .title").outerHeight();
		else
			titleHeight = 0;
		if ($("#thumbs").is(":visible"))
			thumbsHeight = $("#album-view").outerHeight();
		else
			thumbsHeight = 0;
		var distanceFromImageBorder = 15;
		containerHeight = env.windowHeight - titleHeight - thumbsHeight;
		containerWidth = env.windowWidth;

		let top;
		let imageRatio = env.currentMedia.width() / env.currentMedia.height();
		let pinchContainerWidth = parseInt($("#pinch-container").outerWidth());
		if (env.slideshowId && env.currentMedia.needsAutoRotation()) {
			$("#pinch-container").css("left", "");
			let windowRatio = env.windowWidth / env.windowHeight;
			let right;
			let rotatedImageRatio = 1 / imageRatio;
			$("#pinch-container").addClass("rotate-90");
			if (windowRatio > rotatedImageRatio) {
				right = Math.round(env.windowWidth / 2 - env.windowHeight * rotatedImageRatio / 2) + pinchContainerWidth + distanceFromImageBorder;
				top = distanceFromImageBorder;
			} else {
				right = pinchContainerWidth + distanceFromImageBorder;
				top = Math.round(env.windowHeight / 2 - env.windowWidth / rotatedImageRatio / 2) + distanceFromImageBorder;
			}
			$("#pinch-container").css("right", right.toString() + "px");
		} else {
			$("#pinch-container").css("right", "");
			$("#pinch-container").removeClass("rotate-90");
			let containerRatio = containerWidth / containerHeight;
			let left;
			if (containerRatio > imageRatio) {
				left = Math.round(containerWidth / 2 - containerHeight * imageRatio / 2) + distanceFromImageBorder;
				top = titleHeight + distanceFromImageBorder;
			} else {
				left = distanceFromImageBorder;
				top = titleHeight + Math.round(containerHeight / 2 - containerWidth / imageRatio / 2) + distanceFromImageBorder;
			}
			$("#pinch-container").css("left", left.toString() + "px");
		}
		$("#pinch-container").css("top", top.toString() + "px");
	};

	Utilities.setPinchButtonsVisibility = function() {
		$("#pinch-container").removeClass("hidden");

		if (! env.currentMedia || env.currentMedia.isVideo() || env.currentMedia.isAudio()) {
			$("#pinch-container").hide();
		} else {
			$("#pinch-container").show();

			$("#pinch-in").off("click");
			$("#pinch-in").off("click").on(
				"click",
				function(ev) {
					PinchSwipe.pinchIn(null, null);
				}
			);
			$("#pinch-in").removeClass("disabled");

			$("#pinch-out").off("click");
			if (
				$("#center .title").hasClass("hidden-by-pinch") ||
				(env.fullScreenStatus || env.isAnyMobile) && env.currentZoom > env.initialZoom
			) {
				$("#pinch-out").removeClass("disabled");
				$("#pinch-out").off("click").on(
					"click",
					function(ev) {
						PinchSwipe.pinchOut(null, null);
					}
				);
			} else {
				$("#pinch-out").addClass("disabled");
			}
		}
	};

	Utilities.prototype.horizontalScrollBarThickness = function(element) {
		var thickness = element.offsetHeight - element.clientHeight;
		if (! thickness && env.currentAlbum.hasOwnProperty("media")) {
			// sometimes thickness is 0, but the scroll bar could be there
			// let's try to suppose if it's there
			let totalThumbsSize = env.options.media_thumb_size * env.currentAlbum.numVisibleMedia();
			if (! env.options.only_square_thumbnails && env.options.media_thumb_type.indexOf("fixed_height") > -1) {
				let sum = 0;
				totalThumbsSize = env.currentAlbum.media.forEach(
					singleMedia => {
						sum += env.options.media_thumb_size / singleMedia.height() * singleMedia.width();
					}
				);
			}
			if (env.options.spacing)
				totalThumbsSize += env.options.spacing * (env.currentAlbum.numVisibleMedia() - 1);

			if (totalThumbsSize > env.windowWidth) {
				// the scrollbar is there
				thickness = 15;

			}
		}
		return thickness;
	};

	Utilities.prototype.showWorking = function(myClass) {
		$("#working").addClass(myClass);
		$("#working").show();
	};

	Utilities.prototype.hideWorking = function(myClass) {
		$("#working").removeClass(myClass);
		if ($("#working")[0].classList.length == 1)
			$("#working").hide();
	};

	Utilities.setSelectButtonPosition = function(containerHeight, containerWidth) {
		// calculate and set the select buttons position
		if ($(".select-box").attr("src") === undefined)
			return false;

		var mediaElement = $(".media-box#center .media-box-inner #media-center");
		if (mediaElement[0] === undefined)
			return;
		var titleHeight, thumbsHeight;
		if ($(".media-box#center .title").is(":visible"))
			titleHeight = $(".media-box#center .title").outerHeight();
		else
			titleHeight = 0;
		if ($("#thumbs").is(":visible"))
			thumbsHeight = $("#album-view").outerHeight();
		else
			thumbsHeight = 0;
		var distanceFromImageBorder = 15;
		containerHeight = env.windowHeight - titleHeight - thumbsHeight;
		containerWidth = env.windowWidth;

		let left;
		let imageRatio = env.currentMedia.width() / env.currentMedia.height();
		if (env.slideshowId && env.currentMedia.needsAutoRotation()) {
			$("#select-box").css("bottom", "");
			let windowRatio = env.windowWidth / env.windowHeight;
			let top;
			let rotatedImageRatio = 1 / imageRatio;
			$("#select-box").addClass("rotate-90");
			let scaledHeight = Utilities.scaledHeight($("#media-center"));
			let scaledWidth = Utilities.scaledWidth($("#media-center"));
			if (windowRatio > rotatedImageRatio) {
				if (Utilities.scaledWidth > containerHeight) {
					left = Math.round(env.windowWidth / 2 - env.windowHeight * rotatedImageRatio / 2) + distanceFromImageBorder;
					top = distanceFromImageBorder;
				} else {
					left = Math.round(env.windowWidth / 2 - scaledHeight / 2) + distanceFromImageBorder;
					top = Math.round(env.windowHeight / 2 - scaledWidth / 2) + distanceFromImageBorder;
				}
			} else {
				if (scaledHeight > containerWidth) {
					left = distanceFromImageBorder;
					top = Math.round(env.windowHeight / 2 - env.windowWidth / rotatedImageRatio / 2) + distanceFromImageBorder;
				} else {
					left = Math.round(env.windowWidth / 2 - scaledHeight / 2) + distanceFromImageBorder;
					top = Math.round(env.windowHeight / 2 - scaledWidth / 2) + distanceFromImageBorder;
				}
			}
			$("#select-box").css("top", top.toString() + "px");
			if (env.currentMedia.isVideo())
				// avoid intersecting with video controls
				left += 50 * Utilities.getScale($("#media-center"));
		} else {
			$("#select-box").css("top", "");
			$("#select-box").removeClass("rotate-90");
			let containerRatio = containerWidth / containerHeight;
			let bottom;
			if (env.currentMedia.isAudio()) {
				left = Math.round((containerWidth -  $("#media-center").width()) / 2);
				bottom = distanceFromImageBorder + thumbsHeight;
			} else if (containerRatio > imageRatio) {
				if (env.currentMedia.height() > containerHeight) {
					left = Math.round(containerWidth / 2 - containerHeight * imageRatio / 2) + distanceFromImageBorder;
					bottom = distanceFromImageBorder + thumbsHeight;
				} else {
					left = Math.round(containerWidth / 2 - env.currentMedia.width() / 2) + distanceFromImageBorder;
					bottom = Math.round(containerHeight / 2 - env.currentMedia.height() / 2) + distanceFromImageBorder + thumbsHeight;
				}
			} else {
				if (env.currentMedia.width() > containerWidth) {
					left = distanceFromImageBorder;
					bottom = Math.round(containerHeight / 2 - containerWidth / imageRatio / 2) + distanceFromImageBorder + thumbsHeight;
				} else {
					left = Math.round(containerWidth / 2 - env.currentMedia.width() / 2) + distanceFromImageBorder;
					bottom = Math.round(containerHeight / 2 - env.currentMedia.height() / 2) + distanceFromImageBorder + thumbsHeight;
				}
			}
			if (env.currentMedia.isVideo())
				// avoid intersecting with video controls
				bottom += 50 * Utilities.getScale($("#media-center"));
			$("#select-box").css("bottom", bottom.toString() + "px");
		}
		$("#select-box").css("left", left.toString() + "px");

		return true;
	};

	Utilities.correctElementPositions = function() {
		function MoveMediaBarAboveBottomSocial() {
			if (
				env.currentMedia !== null &&
				Utilities.bottomSocialButtons() &&
				Utilities.areColliding($(".media-box#center .media-bar"), $("#social > div"))
			) {
				// move the media bar above the social buttons
				$(".media-box#center .media-bar").css("bottom", ($("#social > div").outerHeight()) + "px");
			}
		}

		function separateLateralSocialAndPrev() {
			if (
				env.currentMedia !== null &&
				! env.currentAlbumIsAlbumWithOneMedia &&
				Utilities.lateralSocialButtons() &&
				Utilities.areColliding($("#social > div"), $("#prev"))
			) {
				if (parseFloat($("#prev").css("bottom")) > $("#social > div").outerHeight()) {
					// move social buttons below prev button
					$("#social > div")
						.removeClass("ssk-center")
						.css("top", (parseFloat($("#prev").css("top")) + $("#prev").outerHeight()) + "px");
				} else {
					// move social buttons to the right of prev button
					$("#social > div").css("left", ($("#prev").outerWidth(true)) + "px");
				}
			}
		}

		function moveBottomSocialAboveBottomThumbs() {
			if (! Utilities.lateralSocialButtons()) {
				if (
					env.currentMedia !== null &&
					! env.currentAlbumIsAlbumWithOneMedia
				) {
					$("#social > div").css("bottom", parseFloat($("#media-view").css("bottom")) + "px");
				} else {
					$("#social > div").css("bottom", 0);
				}
			}
		}

		function moveSelectBoxAboveBottomSocial() {
			// move the select box above the social buttons and the media bar
			if (
				env.currentMedia !== null &&
				Utilities.bottomSocialButtons() &&
				Utilities.areColliding($("#select-box"), $("#social > div"))
			) {
				$("#select-box").css("bottom", ($("#social > div").outerHeight() + 10) + "px");
			}
		}

		function moveSelectBoxAboveMediaBar() {
			if (
				env.currentMedia !== null &&
				Utilities.areColliding($("#select-box"), $(".media-box#center .media-bar .links"))
			) {
				$("#select-box").css(
					"bottom",
					(env.windowHeight - $(".media-box#center .media-bar .links").offset().top + 10).toString() + "px"
				);
			}
		}

		function moveSelectBoxAtTheRightOfPrev() {
			// move the select box at the right of the prev button and lateral social buttons
			if (
				env.currentMedia !== null &&
				! env.currentAlbumIsAlbumWithOneMedia &&
				Utilities.areColliding($("#select-box"), $("#prev"))
			) {
				if (env.slideshowId && env.currentMedia.needsAutoRotation())
					$("#select-box").css("top", ($("#prev").outerWidth(true) + 20) + "px");
				else
					$("#select-box").css("left", ($("#prev").outerWidth(true) + 20) + "px");
			}
		}

		function moveSelectBoxAtTheRightOfPinch() {
			// move the select box at the right of the pinch buttons
			if (
				env.currentMedia !== null &&
				Utilities.areColliding($("#select-box"), $("#pinch-container"))
			) {
				if (env.slideshowId && env.currentMedia.needsAutoRotation())
					$("#select-box").css("top", (parseFloat($("#pinch-container").css("top")) + $("#pinch-container").outerWidth(true) + 10) + "px");
				else
					$("#select-box").css("left", (parseFloat($("#pinch-container").css("left")) + $("#pinch-container").outerWidth(true) + 10) + "px");
			}
		}

		function moveUpButtonAtTheRightOfPinch() {
			// move the select box at the right of the pinch buttons
			if (
				env.currentMedia !== null &&
				Utilities.areColliding($("#up-button"), $("#pinch-container"))
			) {
				if (env.slideshowId && env.currentMedia.needsAutoRotation())
					$("#up-button").css("top", (parseFloat($("#pinch-container").css("top")) + $("#pinch-container").outerWidth(true) + 10) + "px");
				else
					$("#up-button").css("left", (parseFloat($("#pinch-container").css("left")) + $("#pinch-container").outerWidth(true) + 10) + "px");
			}
		}

		function moveSelectBoxAtTheRightOfLateralSocial() {
			if (
				env.currentMedia !== null &&
				Utilities.lateralSocialButtons() &&
				Utilities.areColliding($("#select-box"), $("#social > div"))
			) {
				$("#select-box").css("left", ($("#social > div").outerWidth(true) + 20) + "px");
			}
		}

		function moveSelectBoxAboveSlideshow() {
			if (
				env.currentMedia !== null &&
				! env.currentAlbumIsAlbumWithOneMedia &&
				env.slideshowId &&
				Utilities.areColliding($("#select-box"), $("#slideshow-buttons"))
			) {
				if (env.currentMedia.needsAutoRotation())
					$("#select-box").css(
						"left",
						($("#slideshow-buttons").outerHeight(true) + parseInt($("#slideshow-buttons").css("left")) + 20) + "px"
					);
				else
					$("#select-box").css(
						"bottom",
						($("#slideshow-buttons").outerHeight(true) + parseInt($("#slideshow-buttons").css("bottom")) + 20) + "px"
					);
			}
		}

		function moveMapAtTheLeftOfNext() {
			// correct map button position
			if (
				env.currentMedia !== null &&
				! env.currentAlbumIsAlbumWithOneMedia &&
				Utilities.areColliding($("#map-button-container"), $("#next"))
			) {
				if (env.slideshowId && env.currentMedia.needsAutoRotation())
					$("#map-button-container").css("bottom", ($("#next").outerWidth(true) + 5) + "px");
				else
					$("#map-button-container").css("right", ($("#next").outerWidth(true) + 5) + "px");
			}
		}

		function movePinchAtTheRightOfPrev() {
			// correct pinch buttons position
			if (
				env.currentMedia !== null &&
				! env.currentAlbumIsAlbumWithOneMedia &&
				Utilities.areColliding($("#pinch-container"), $("#prev"))
			) {
				if (env.slideshowId && env.currentMedia.needsAutoRotation())
					$("#pinch-container").css("right", ($("#prev").outerWidth(true) + 20) + "px");
				else
					$("#pinch-container").css("left", ($("#prev").outerWidth(true) + 20) + "px");
			}
		}

		function moveMapBelowMenuButtons() {
			// correct pinch buttons position
			if (
				env.currentMedia !== null &&
				Utilities.areColliding($("#map-button-container"), $("#right-and-search-menu"))
			) {
				$("#map-button-container").css("top", ($("#right-and-search-menu").outerHeight(true) + 20) + "px");
			}
		}

		function moveDescriptionAtTheLeftOfNext() {
			// correct description/tags box position
			// always moves at the left of next, even if not colliding
			if (
				env.currentMedia !== null &&
				! env.currentAlbumIsAlbumWithOneMedia
			) {
				$("#description-wrapper").css("right", ($("#next").outerWidth(true) + 10) + "px");
			}
		}

		function moveDescriptionAboveBottomSocial() {
			if (
				Utilities.bottomSocialButtons() &&
				Utilities.areColliding($("#description-wrapper"), $("#social > div"))
			) {
				// move the descriptiont/tags box above the social buttons
				$("#description-wrapper").css("bottom", ($("#social > div").outerHeight() + 10) + "px");
			}
		}

		function MoveDescriptionAboveMediaBar() {
			let wasVisible = true;
			if (! $(".media-box#center .media-bar .links").is(":visible")) {
				wasVisible = false;
				$(".media-box#center .media-bar .links").show();
			}
			if (
				env.currentMedia !== null &&
				Utilities.areColliding($("#description-wrapper"), $(".media-box#center .media-bar .links"))
			) {
				// move the descriptiont/tags box above the media bar
				let thumbsHeight = 0;
				if ($("#thumbs").is(":visible"))
					thumbsHeight = $("#thumbs").outerHeight();
				$("#description-wrapper").css(
					"bottom",
					(env.windowHeight - $(".media-box#center .media-bar .links").offset().top + 10).toString() + "px"
				);
			}
			if (! wasVisible)
				$(".media-box#center .media-bar .links").hide();
		}

		function moveDescriptionAtTheLeftOfPinch() {
			if (
				env.currentMedia !== null &&
				Utilities.areColliding($("#description-wrapper"), $("#pinch-container"))
			) {
				// move the descriptiont/tags box to the left of the pinch buttons
				$("#description-wrapper").css(
					"right",
					(parseFloat($("#pinch-container").css("right")) + $("#pinch-container").outerWidth(true) + 10) + "px"
				);
			}
		}

		$("#social > div").removeClass("ssk-bottom").addClass("ssk-center");
		$("#social > div").css("left", "").css("top", "");

		var mediaBarHeigth = parseFloat($(".media-box#center .media-bar").outerHeight());
		if (! mediaBarHeigth)
			mediaBarHeigth = 30;
		$(".media-box#center .media-bar").css("bottom", "");

		moveBottomSocialAboveBottomThumbs();
		MoveMediaBarAboveBottomSocial();
		separateLateralSocialAndPrev();
		moveSelectBoxAboveBottomSocial();
		moveSelectBoxAboveMediaBar();
		moveSelectBoxAtTheRightOfPrev();
		moveSelectBoxAtTheRightOfLateralSocial();
		moveSelectBoxAboveSlideshow();
		moveMapBelowMenuButtons();
		moveMapAtTheLeftOfNext();
		movePinchAtTheRightOfPrev();
		moveSelectBoxAtTheRightOfPinch();
		moveUpButtonAtTheRightOfPinch();
		moveDescriptionAtTheLeftOfNext();
		moveDescriptionAtTheLeftOfPinch();
		moveDescriptionAboveBottomSocial();
		MoveDescriptionAboveMediaBar();
		moveDescriptionAtTheLeftOfNext();
		moveDescriptionAtTheLeftOfPinch();
	};

	Utilities.prototype.sumNumsProtectedMediaOfArray = function(arrayOfAlbumsOrSubalbunms) {
		var result = new NumsProtected({}), i, codesComplexcombination, albumOrSubalbum;

		for (i = 0; i < arrayOfAlbumsOrSubalbunms.length; i ++) {
			albumOrSubalbum = arrayOfAlbumsOrSubalbunms[i];
			for (codesComplexcombination in albumOrSubalbum.numsProtectedMediaInSubTree) {
				if (albumOrSubalbum.numsProtectedMediaInSubTree.hasOwnProperty(codesComplexcombination) && codesComplexcombination !== ",") {
					if (! result.hasOwnProperty(codesComplexcombination))
						result[codesComplexcombination] = new ImagesAudiosVideos();
					result[codesComplexcombination].sum(albumOrSubalbum.numsProtectedMediaInSubTree[codesComplexcombination]);
				}
			}
		}

		return result;
	};


	Utilities.areColliding = function(jQueryObject1, jQueryObject2) {
		if (! jQueryObject1.is(":visible") || ! jQueryObject2.is(":visible"))
			return false;
		var offset1 = jQueryObject1.offset();
		var top1 = offset1.top;
		var left1 = offset1.left;
		var height1 = jQueryObject1.outerHeight(true);
		var width1 = jQueryObject1.outerWidth(true);
		var bottom1 = offset1.top + height1;
		var right1 = offset1.left + width1;

		// Div 2 data
		var offset2 = jQueryObject2.offset();
		var top2 = offset2.top;
		var left2 = offset2.left;
		var height2 = jQueryObject2.outerHeight(true);
		var width2 = jQueryObject2.outerWidth(true);
		var bottom2 = offset2.top + height2;
		var right2 = offset2.left + width2;

		return right2 >= left1 && right1 >= left2 && bottom2 >= top1 && bottom1 >= top2;
	};

	Utilities.degreesToRadians = function(degrees) {
		var pi = Math.PI;
		return degrees * (pi/180);
	};

	Utilities.prototype.escapeSingleQuotes = function(text) {
		return text.replace(/'/g, "\\'");
	};

	Utilities.xDistanceBetweenCoordinatePoints = function(point1, point2) {
		return Math.max(
			Utilities.distanceBetweenCoordinatePoints({lng: point1.lng, lat: point1.lat}, {lng: point2.lng, lat: point1.lat}),
			Utilities.distanceBetweenCoordinatePoints({lng: point1.lng, lat: point2.lat}, {lng: point2.lng, lat: point2.lat})
		);
	};

	Utilities.yDistanceBetweenCoordinatePoints = function(point1, point2) {
		return Utilities.distanceBetweenCoordinatePoints({lng: point1.lng, lat: point1.lat}, {lng: point1.lng, lat: point2.lat});
	};

	Utilities.distanceBetweenCoordinatePoints = function(point1, point2) {
		// converted from Geonames.py
		// Calculate the great circle distance in meters between two points on the earth (specified in decimal degrees)

		// convert decimal degrees to radians
		var r_lon1 = Utilities.degreesToRadians(point1.lng);
		var r_lat1 = Utilities.degreesToRadians(point1.lat);
		var r_lon2 = Utilities.degreesToRadians(point2.lng);
		var r_lat2 = Utilities.degreesToRadians(point2.lat);
		// haversine formula
		var d_r_lon = r_lon2 - r_lon1;
		var d_r_lat = r_lat2 - r_lat1;
		var a = Math.pow(Math.sin(d_r_lat / 2), 2) + Math.cos(r_lat1) * Math.cos(r_lat2) * Math.pow(Math.sin(d_r_lon / 2), 2);
		var c = 2 * Math.asin(Math.sqrt(a));
		var earth_radius = 6371000;  // radius of the earth in m
		var dist = earth_radius * c;
		return dist;
	};

	Utilities.lateralSocialButtons = function() {
		return $(".ssk-group").css("display") === "block" && ! env.slideshowId;
	};

	Utilities.bottomSocialButtons = function() {
		return $(".ssk-group").css("display") === "flex" && ! env.slideshowId;
	};

	Utilities.setLinksVisibility = function() {
		if (env.isAnyMobile) {
			$(".media-box .links").css("display", "inline").css("opacity", 0.5).stop().fadeTo("slow", 0.25);
		} else {
			$("#media-view").off();
			$("#media-view").off('mouseover').on(
				'mouseover',
				function() {
					$(".media-box .links").stop().fadeTo("slow", 0.50).css("display", "inline");
				}
			);
			$("#media-view").off('mouseout').on(
				'mouseout',
				function() {
					$(".media-box .links").stop().fadeOut("slow");
				}
			);
		}
	};

	Utilities.setPrevNextVisibility = function() {
		if (env.currentMedia === null || env.currentAlbumIsAlbumWithOneMedia)
			$("#next, #prev").hide();
		else
			$("#next, #prev").show();
		if (env.isAnyMobile) {
			$("#next, #prev").css("display", "inline").css("opacity", 0.5);
		} else {
			$("#next, #prev").off('mouseenter mouseleave');
			$("#next, #prev").off('mouseenter').on(
				'mouseenter',
				function() {
					$(this).stop().fadeTo("fast", 1);
				}
			);

			$("#next, #prev").off('mouseleave').on(
				'mouseleave',
				function() {
					$(this).stop().fadeTo("fast", 0.4);
				}
			);
		}
	};

	Utilities.addDescriptionOpacity = function() {
		$("#description-wrapper").css("opacity", "0.3");
		$("#album-and-media-container").css("opacity", "1");
	};

	Utilities.prototype.removeDescriptionOpacity = function() {
		$("#description-wrapper").css("opacity", "1");
		$("#album-and-media-container").css("opacity", "0.3");
	};

	Utilities.prototype.hideId = function(id) {
		$(id).hide();
	};

	Utilities.formatDescription = function(text) {
		// Replace CRLF by <p> and remove all useless <br>.
		text = text.replace(/<(\/?\w+)>\s*\n\s*<(\/?\w+)>/g, "<$1><$2>");
		text = text.replace(/\n/g, "</p><p>");
		if (text.substring(0, 3) !== "<p ")
			text = "<p>" + text + "</p>";
		return text;
	};

	Utilities.adaptSubalbumCaptionHeight = function() {
		// check for overflow in album-caption class in order to adapt album caption height to the string length
		// when diving into search subalbum, the whole album path is showed and it can be lengthy

		function adapt() {
			objects.forEach(
				function(object) {
					var difference = maxHeight - parseFloat(object.css("height"));
					object.parent().css("height", (object.parent().height() + difference) + "px");
					object.css("height", maxHeight + "px");
				}
			);
		}

		function resetHeight(object) {
			object.css("height", initialHeight);
			object.parent().css("height", initialParentHeight);
		}

		var maxHeight, objects, top;
		var selector = ".album-caption";
		if (Utilities.geotaggedContentIsHidden())
			selector = "#subalbums a:not(.all-gps) .album-caption";
		var length = $(selector).length;
		var initialHeight = $(selector).css("height");
		var initialParentHeight = $(selector).parent().css("height");

		var thereWasAVerticalScrollBar = Utilities.thereIsAVerticalScrollBar();

		// execute twice, because the first run can activate the scroll bar and thus mess things
		for (var i = 0; i < 2; i ++) {
			if (i === 1) {
				if (thereWasAVerticalScrollBar || ! Utilities.thereIsAVerticalScrollBar())
					break;
			}

			top = false;
			maxHeight = 0;
			objects = [];
			$(selector).each(
				function(index) {
					var newTop = $(this).parent().offset().top;
					if (top !== false && newTop != top) {
						adapt();

						maxHeight = 0;
						objects = [];

						// newTop value must be recalculated
						newTop = $(this).parent().offset().top;
					}

					if (i === 1)
						resetHeight($(this));

					top = newTop;
					objects.push($(this));
					var thisHeight = 0;
					$(this).children().each(
						function() {
							thisHeight += $(this).outerHeight(true);
							// thisHeight += $(this)[0].scrollHeight;
						}
					);
					maxHeight = (thisHeight > maxHeight) ? thisHeight : maxHeight;

					// one more adaptation is needed for the last line
					if (index === length - 1)
						adapt();
				}
			);
		}

		Utilities.scrollAlbumViewToHighlightedSubalbum($("#subalbums .highlighted-object"));
	};

	Utilities.adaptMediaCaptionHeight = function(inPopup = false) {
		// check for overflow in media-caption class in order to adapt media caption height to the string length

		function adapt() {
			objects.forEach(
				function(object) {
					object.css("height", maxHeight + "px");
				}
			);
		}

		function resetHeight(object) {
			object.css("height", initialHeight);
		}

		var maxHeight, objects, top;
		var baseSelector = "#thumbs a";
		if (Utilities.geotaggedContentIsHidden())
			baseSelector = "#thumbs a:not(.gps)";
		if (inPopup)
			baseSelector = "#popup-images-wrapper";
		var selector = baseSelector + " .media-caption";
		var initialHeight = $(selector).css("height");
		var length = $(selector).length;
		var thereWasAVerticalScrollBar = Utilities.thereIsAVerticalScrollBar();

		// execute twice, because the first run can activate the scroll bar and thus mess things
		for (var i = 0; i < 2; i ++) {
			if (i === 1) {
				if (thereWasAVerticalScrollBar || ! Utilities.thereIsAVerticalScrollBar())
					break;
			}

			top = false;
			maxHeight = 0;
			objects = [];
			$(selector).css("height", 0);
			$(selector).each(
				function(index) {
					var newTop = $(this).offset().top;
					if (top !== false && newTop != top) {
						adapt();

						maxHeight = 0;
						objects = [];

						// newTop value must be recalculated
						newTop = $(this).offset().top;
					}

					if (i === 1)
						resetHeight($(this));

					top = newTop;
					objects.push($(this));
					var thisHeight = 0;
					$(this).children().each(
						function() {
							thisHeight += $(this).outerHeight(true);
						}
					);
					maxHeight = (thisHeight > maxHeight) ? thisHeight : maxHeight;

					// one ore adaptation is needed for the last line
					if (index === length - 1)
						adapt();
				}
			);
		}

		Utilities.scrollAlbumViewToHighlightedMedia($("#thumbs .highlighted-object"));
		if (inPopup)
			Utilities.scrollPopupToHighlightedThumb($("#popup-images-wrapper .highlighted-object"));
	};

	Utilities.thereIsAVerticalScrollBar = function() {
		var root = document.compatMode === 'BackCompat' ? document.body : document.documentElement;
		return root.scrollHeight > root.clientHeight;
	};

	Utilities.hasProperty = function(object, property) {
		if (! object.hasOwnProperty(property))
			return false;
		else
			// this[property] is array or string
			return object[property].length > 0;
	};

	Utilities.hasSomeDescription = function(albumOrSingleMediaOrMetadata, property = null) {
		var myObject;
		if (albumOrSingleMediaOrMetadata instanceof SingleMedia)
			myObject = albumOrSingleMediaOrMetadata.metadata;
		else
			myObject = albumOrSingleMediaOrMetadata;

		if (property)
			return Utilities.hasProperty(myObject, property);
		else
			return Utilities.hasProperty(myObject, "title") || Utilities.hasProperty(myObject, "description") || Utilities.hasProperty(myObject, "tags");
	};

	Utilities.prototype.setDescription = function(object) {
		var selector;
		if (object instanceof Album)
			selector = "#album-description-wrapper";
		else
			selector = "#single-media-description-wrapper";

		var hasTitle = Utilities.hasProperty(object, "title");
		var hasDescription = Utilities.hasProperty(object, "description");
		var hasTags = Utilities.hasProperty(object, "tags");

		if (
			! Utilities.hasSomeDescription(object) || (
				(
					! (hasTitle || hasDescription) || env.options.hide_descriptions
				) && (
					! hasTags || env.options.hide_tags
				)
			)
		) {
			$(selector).addClass("hidden");
		} else {
			$(selector).removeClass("hidden");

			// $(selector + " .description").css("max-height", (env.windowHeight / 2) + "px");

			if (! hasTitle && ! hasDescription) {
				$(selector + " .description-title").html("");
				$(selector + " .description-text").html("");
			} else {
				// $(selector + " .description").show();
				if (! hasTitle) {
					$(selector + " .description-title").hide();
					$(selector + " .description-title").html("");
				} else {
					$(selector + " .description-title").show();
					$(selector + " .description-title").html(Utilities.formatDescription(object.title));
				}

				if (! hasDescription) {
					$(selector + " .description-text").hide();
					$(selector + " .description-text").html("");
				} else {
					$(selector + " .description-text").show();
					$(selector + " .description-text").html(Utilities.formatDescription(object.description));
					$(selector + " .description-text p").addClass("description-p");
				}
			}

			if (! hasTags) {
				$(selector + " .description-tags").hide();
				$(selector + " .description-tags").html("");
			} else {
				let textualTags = Utilities._t("#tags") + ": <span class='tag'>" + object.tags.map(tag => Utilities.addTagLink(tag)).join("</span>, <span class='tag'>") + "</span>";
				$(selector + " .description-tags").show();
				$(selector + " .description-tags").html(textualTags);
			}
		}
	};

	Utilities.setDescriptionOptions = function() {
		var forceShowTags = false, hasDescription, hasTags ;

		if ($(".description-tags").html().indexOf(env.markTagBegin) > -1)
			forceShowTags = true;

		var albumHasSomeDescription = env.currentAlbum !== null && env.currentAlbum.hasSomeDescription();
		var singleMediaHasSomeDescription = env.currentMedia !== null && env.currentMedia.hasSomeDescription();

		var showAlbumDescription = albumHasSomeDescription && (forceShowTags || ! env.options.hide_descriptions || ! env.options.hide_tags);
		var showSingleMediaDescription = singleMediaHasSomeDescription && (forceShowTags || ! env.options.hide_descriptions || ! env.options.hide_tags);

		if (! showAlbumDescription && ! showSingleMediaDescription) {
			$("#description-wrapper").addClass("hidden-by-option");
		} else {
			$("#description-wrapper").removeClass("hidden-by-option");

			$("#album-description-wrapper").addClass("hidden-by-option");
			$("#single-media-description-wrapper").addClass("hidden-by-option");
			if (showAlbumDescription) {
				$("#album-description-wrapper").removeClass("hidden-by-option");
			}
			if (showSingleMediaDescription) {
				$("#single-media-description-wrapper").removeClass("hidden-by-option");
			}

			if (env.options.hide_descriptions)
				$(".description").addClass("hidden-by-option");
			else
				$(".description").removeClass("hidden-by-option");

			if (! forceShowTags && env.options.hide_tags)
				$(".description-tags").addClass("hidden-by-option");
			else
				$(".description-tags").removeClass("hidden-by-option");

			$("#description-wrapper").css("right", "");
			var thumbsHeight = 0;
			if (env.currentMedia !== null && $("#thumbs").is(":visible"))
				thumbsHeight = env.options.media_thumb_size + 20;
			$("#description-wrapper").css("bottom", thumbsHeight + 20);

			// $(".description-tags").css("right", $("#description-hide-show").outerWidth(true).toString() + "px");

			var maxHeight = Math.min(env.windowHeight / 4, 500);
			if (env.isAnyMobile)
				maxHeight = Math.min(env.windowHeight / 4, 400);

			var maxWidth = Math.min(env.windowWidth / 2, 500);
			if (env.isAnyMobile)
				maxWidth = Math.min(env.windowWidth / 2, 400);

			$("#description-wrapper").css("width", "");
			$("#description-wrapper").css("height", "");
			$("#description-wrapper").css("max-width", maxWidth.toString() + "px");
			for (var object of [env.currentMedia !== null ? env.currentMedia.metadata : null, env.currentAlbum]) {
				if (object === null)
					continue;
				let id;
				if (object instanceof Album)
					id = "#album-description-wrapper ";
				else
					id = "#single-media-description-wrapper ";
				hasDescription = Utilities.hasProperty(object, "title") || Utilities.hasProperty(object, "description");
				hasTags = Utilities.hasProperty(object, "tags");
				$(id + ".description").css("max-height", "");
				$(id + ".description-text").css("max-height", "");
				$(id + ".description-tags").css("max-height", "");
				$(id + ".description-tags").css("position", "");

				$(id + ".description-text").css("margin-bottom", "");
				$(id + ".description-title").css("margin-bottom", "");
				$(id + ".description-text").css("height", "");
				$(id + ".description").css("border", "");
				$(id + ".description-tags").css("position", "");
				$(id + ".description-tags").css("margin-left", "");
				// var bottomSpace = $("#description-hide-show").outerHeight();
				// if ($(".description-tags").is(":visible") && ! env.options.hide_tags && hasTags)
				// 	bottomSpace = Math.max(bottomSpace, $(".description-tags").outerHeight());
				if ($(id + ".description-text").is(":visible") && $(id + ".description-text").height() > 0) {
					// $(".description-text").css("margin-bottom", bottomSpace.toString() + "px");
				} else if ($(id + ".description-title").is(":visible") && $(id + ".description-title").height() > 0) {
					// $(".description-title").css("margin-bottom", bottomSpace.toString() + "px");
				} else {
					// $(".description").css("border", "0");
					$(id + ".description-tags").css("position", "relative");
					// $(".description-tags").css("margin-left", ($("#description-hide-show").outerWidth(true)) + "px");
				}

				// $(".description-tags").css("max-width", (maxWidth - 20).toString() + "px");
			}

			while (
				Math.max(
					$("#single-media-description-wrapper .description").outerWidth(true),
					$("#single-media-description-wrapper .description-tags").outerWidth(true),
					$("#album-description-wrapper .description").outerWidth(true),
					$("#album-description-wrapper .description-tags").outerWidth(true)
				) > $("#description-wrapper").innerWidth() &&
				$("#description-wrapper").width() < maxWidth
			) {
				$("#description-wrapper").css("width", ($("#description-wrapper").width() + 5) + "px");
			}

			// set visibility of the (possibly two) boxes
			if (albumHasSomeDescription && singleMediaHasSomeDescription) {
				// two boxes
				$("#description-show").hide();

				if (
					! $("#description-wrapper").hasClass("two") &&
					! $("#description-wrapper").hasClass("one") &&
					! $("#description-wrapper").hasClass("no")
				) {
					// first album/media, or comes from one box, box visible
					$("#description-wrapper").addClass("one");
					$("#description-wrapper").removeClass("yes");
				}

				if ($("#description-wrapper").hasClass("two")) {
					$("#description-hide").show();
					$("#description-show-1").hide();
					$("#description-show-2").hide();
					$(".description-wrapper").removeClass("hidden-by-button");
				} else if ($("#description-wrapper").hasClass("one")) {
					$("#description-hide").hide();
					$("#description-show-1").hide();
					$("#description-show-2").show();
					$("#album-description-wrapper").addClass("hidden-by-button");
					$("#single-media-description-wrapper").removeClass("hidden-by-button");
				} else if ($("#description-wrapper").hasClass("no")) {
					$("#description-hide").hide();
					$("#description-show-1").show();
					$("#description-show-2").hide();
					$(".description-wrapper").addClass("hidden-by-button");
				}
			} else {
				// one box
				$("#description-show-1").hide();
				$("#description-show-2").hide();

				if (
					! $("#description-wrapper").hasClass("yes") &&
					! $("#description-wrapper").hasClass("no")
				) {
					// first album/media, or comes from one box, box visible
					$("#description-wrapper").addClass("yes");
					$("#description-wrapper").removeClass("one");
					$("#description-wrapper").removeClass("two");
				}

				if ($("#description-wrapper").hasClass("yes")) {
					$("#description-hide").show();
					$("#description-show").hide();
					$(".description-wrapper").removeClass("hidden-by-button");
				} else if ($("#description-wrapper").hasClass("no")) {
					$("#description-hide").hide();
					$("#description-show").show();
					$(".description-wrapper").addClass("hidden-by-button");
				}
			}

			if ($(".description-text").is(":visible") && ! env.options.hide_descriptions && hasDescription) {
				$(".description-text").css("max-height", maxHeight.toString() + "px");
			} else if ($(".description-tags").is(":visible") && ! env.options.hide_tags && hasTags) {
				$(".description-tags").css("max-height", maxHeight.toString() + "px");
			} else {
				$(".description").css("max-height", maxHeight.toString() + "px");
			}

			$("#description-hide-show").off("click").on(
				"click",
				function() {
					$("#description-hide-show").css("position", "");
					if (albumHasSomeDescription && singleMediaHasSomeDescription) {
						// two boxes

						if ($("#description-wrapper").hasClass("two")) {
							$("#description-wrapper").removeClass("two");
							$("#description-wrapper").addClass("no");
							Utilities.addDescriptionOpacity();
						} else if ($("#description-wrapper").hasClass("one")) {
							$("#description-wrapper").removeClass("one");
							$("#description-wrapper").addClass("two");
						} else if ($("#description-wrapper").hasClass("no")) {
							$("#description-wrapper").removeClass("no");
							$("#description-wrapper").addClass("one");
						}
					} else {
						// one box

						if ($("#description-wrapper").hasClass("yes")) {
							$("#description-wrapper").removeClass("yes");
							$("#description-wrapper").addClass("no");
							Utilities.addDescriptionOpacity();
						} else if ($("#description-wrapper").hasClass("no")) {
							$("#description-wrapper").removeClass("no");
							$("#description-wrapper").addClass("yes");
						}
					}
					Utilities.setFinalOptions(env.currentMedia, false); // OK: on click
				}
			);
		}
	};

	Utilities.prototype.mediaBoxGenerator = function(id) {
		if (env.fullSizeMediaBoxContainerContent.indexOf("width") === -1) {
			// add the css width and height property at 0, so that the width is correct immediately
			env.fullSizeMediaBoxContainerContent = $(env.fullSizeMediaBoxContainerContent)
				.css("width", 0)
				.css("height", 0)
				[0].outerHTML;
		}

		if (id === 'left')
			$("#media-box-container").prepend(env.fullSizeMediaBoxContainerContent.replace('id="center"', 'id="left"'));
		else if (id === 'right')
			$("#media-box-container").append(env.fullSizeMediaBoxContainerContent.replace('id="center"', 'id="right"'));
		$(".media-box#" + id + " .metadata").css("display", $(".media-box#center .metadata").css("display"));
		$(".media-box#" + id + " .media-bar").css("display", $(".media-box#center .media-bar").css("display"));
	};

	Utilities.prototype.showAuthForm = function(event, maybeProtectedContent = false) {
		$.triggerEvent("readyToLoadOtherJsFilesEvent");
		$("#album-view, #media-view, #my-modal, #no-results").css("opacity", "0.2");
		$("#loading").hide();

		MenuFunctions.closeMenu(false);

		$("#no-results").hide();
		$("#auth-text").stop().fadeIn(
			1000,
			function() {
				$("#password").trigger("focus");
				$('#auth-close').off("click").on(
					"click",
					function() {
						$("#auth-text").hide();
						$("#auth-form").show();
						$("#password-request-form").hide();
						$("#album-view, #media-view, #my-modal").css("opacity", "");
						if (env.currentAlbum === null) {
							env.fromEscKey = true;
							$(window).hashchange();
						} else if (maybeProtectedContent) {
							window.location.href = Utilities.upHash();
						}

					}
				);
			}
		);
	};

	Utilities.prototype.showPasswordRequestForm = function(event) {
		$("#auth-form").hide();
		$("#password-request-form").show();
		$("#please-fill").hide();
		$("#identity").attr("title", Utilities._t("#identity-explication"));
	};

	Utilities.encodeAllNonAlfaNum = function(string) {
		return string.replace(
			/[^a-zA-Z0-9]/g,
			function(c) {
				return '%' + c.charCodeAt(0).toString(16);
			}
		);
	};

	Utilities.addGeoContentInfoToSelector = function(clickedSelector, info) {
		return "#" + info + clickedSelector.substring(1);
	};

	Utilities.separateSelectorWithGeoContentInfo = function(selectorWithGeoContentInfo) {
		return ["#" + selectorWithGeoContentInfo.substring(2), parseInt(selectorWithGeoContentInfo.substr(1, 1))];
	};

	Utilities.hasGeoContentInfo = function (selectorWithGeoContentInfo) {
		return (
			selectorWithGeoContentInfo !== undefined &&
			env.selectCollectiveCommands.includes(Utilities.separateSelectorWithGeoContentInfo(selectorWithGeoContentInfo)[0])
		);
	};

	Utilities.encodeSelectionClickHistory = function(selectionClickHistory) {
		// remove the album part of the media selectors, if it's the same as the cache base
		let mediaBeginnings = [
			env.mediaSelectBoxIdBeginning,
			env.mediaMapSelectBoxIdBeginning
		];
		let modifiedSelectionClickHistory = [];
		selectionClickHistory.forEach(
			selectionClickHistoryElement => {
				const albumCacheBase = Object.keys(selectionClickHistoryElement)[0];
				const clickedSelectors = Object.values(selectionClickHistoryElement)[0];

				let newSelectionClickHistoryElement = {};
				newSelectionClickHistoryElement[albumCacheBase] = [];
				clickedSelectors.forEach(
					clickedSelector => {
						// clickedSelector has geotagged content showing info for collective commands
						let originalClickedSelector = clickedSelector;
						if (Utilities.hasGeoContentInfo(clickedSelector))
							clickedSelector = Utilities.separateSelectorWithGeoContentInfo(clickedSelector)[0];

						for (let beginning of mediaBeginnings) {
							let splittedClickedSelector = clickedSelector.substr(1 + beginning.length).split(env.options.cache_folder_separator);
							if (
								! env.selectCollectiveCommands.includes(clickedSelector) &&
								clickedSelector.startsWith("#" + beginning) &&
								splittedClickedSelector.slice(
									0, splittedClickedSelector.length - 1
								).join(env.options.cache_folder_separator) === albumCacheBase
							) {
								newSelectionClickHistoryElement[albumCacheBase].push(
									"#" + beginning + splittedClickedSelector.pop()
								);
								break;
							} else if (beginning === mediaBeginnings[1]) {
								newSelectionClickHistoryElement[albumCacheBase].push(originalClickedSelector);
							}
						}
					}
				);
				modifiedSelectionClickHistory.push(newSelectionClickHistoryElement);
			}
		);


		// menu commands are converted to their one-letter abbreviation
		// double quotes are removed, actually they aren't needed
		// "#" are removed, too
		const regex = new RegExp(
			"(?<=\"#(0|1))(" + Object.keys(env.selectIdsConvertionTable).join("|") + ")(?=\")", "g"
		);
		return Utilities.encodeAllNonAlfaNum(
			JSON.stringify(modifiedSelectionClickHistory)
			.replace(regex, (match) => env.selectIdsConvertionTable[match])
			.replace(/"/g, "")
			.replace(/#/g, "")
		);
	};

	Utilities.decodeSelectionClickHistory = function(encodedSelectionClickHistory) {
		// "#" are restored
		// double quotes are restored, too
		// one-letter abbreviation of menu commands are converted to their long form
		const regex = new RegExp(
			"(?<=\"#(0|1))(" + Object.keys(env.selectIdsReverseConvertionTable).join("|") + ")(?=\")", "g"
			// "(\"(0|1)" + Object.keys(env.selectIdsReverseConvertionTable).join("\"|\"") + "\")", "g"
		);
		const modifiedSelectionClickHistory = JSON.parse(
			Utilities.decodeAllNonAlfaNum(encodedSelectionClickHistory)
			.replace(/:\[/g, ":[#").replace(/\,(?!\{)/g, ",#")
			.replace(/([^[\]{}:,]+)/g, '"$1"')
			.replace(regex, (match) => env.selectIdsReverseConvertionTable[match])
		);

		// restore the album part of the media selectors, it was removed
		const mediaBeginnings = [
			env.mediaSelectBoxIdBeginning,
			env.mediaMapSelectBoxIdBeginning
		];
		let selectionClickHistory = [];

		modifiedSelectionClickHistory.forEach(
			selectionClickHistoryElement => {
				const albumCacheBase = Object.keys(selectionClickHistoryElement)[0];
				const clickedSelectors = Object.values(selectionClickHistoryElement)[0];

				let newSelectionClickHistoryElement = {};
				newSelectionClickHistoryElement[albumCacheBase] = [];
				clickedSelectors.forEach(
					clickedSelector => {
						// clickedSelector has geotagged content showing info for collective commands
						let originalClickedSelector = clickedSelector;
						if (Utilities.hasGeoContentInfo(clickedSelector))
							clickedSelector = Utilities.separateSelectorWithGeoContentInfo(clickedSelector)[0];

						for (let beginning of mediaBeginnings) {
							const singleMediaCacheBase = clickedSelector.substr(1 + beginning.length);
							if (
								! Object.keys(env.selectIdsConvertionTable).includes(clickedSelector.substr(1)) &&
								clickedSelector.startsWith("#" + beginning)
							) {
								newSelectionClickHistoryElement[albumCacheBase].push(
									["#" + beginning + albumCacheBase, singleMediaCacheBase].join(env.options.cache_folder_separator)
								);
								break;
							} else if (beginning === mediaBeginnings[1]) {
								newSelectionClickHistoryElement[albumCacheBase].push(originalClickedSelector);
							}
						}
					}
				);
				selectionClickHistory.push(newSelectionClickHistoryElement);
			}
		);

		return selectionClickHistory;
	};

	Utilities.encodeSelectionCacheBase = function(selectionClickHistory) {
		return [
			env.options.by_selection_string,
			Utilities.encodeSelectionClickHistory(selectionClickHistory)
		].join(env.options.cache_folder_separator);
	};

	Utilities.prototype.decodeSelectionCacheBase = function(albumCacheBase) {
		let splittedAlbumCacheBase = albumCacheBase.split(env.options.cache_folder_separator);
		return Utilities.decodeSelectionClickHistory(splittedAlbumCacheBase[1]);
	};

	Utilities.prototype.isInsideSelectedAlbums = function(albumOrSubalbum, selectionAlbum = env.selectionAlbum) {
		if (
			selectionAlbum.subalbums.some(
				selectedAlbum =>
					albumOrSubalbum.cacheBase.startsWith(selectedAlbum.cacheBase) &&
					albumOrSubalbum.cacheBase !== selectedAlbum.cacheBase
			)
		) {
			return true;
		} else {
			return false;
		}
	};

	Utilities.upHash = function(hashToGoUp = window.location.hash) {
		var resultCacheBase;
		var albumCacheBase, mediaCacheBase, collectedAlbumCacheBase, collectionCacheBase;
		var components = PhotoFloat.returnDecodedHash(hashToGoUp);
		albumCacheBase = components.albumCacheBase;
		mediaCacheBase = components.mediaCacheBase;
		collectedAlbumCacheBase = components.collectedAlbumCacheBase;
		collectionCacheBase = components.collectionCacheBase;

		if (mediaCacheBase === null || env.currentAlbum && env.currentAlbumIsAlbumWithOneMedia) {
			// hash of an album: go up in the album tree
			if (collectionCacheBase !== null) {
				if (albumCacheBase === collectedAlbumCacheBase)
					resultCacheBase = collectionCacheBase;
				else {
					// we must go up in the sub folder
					albumCacheBase = albumCacheBase.split(env.options.cache_folder_separator).slice(0, -1).join(env.options.cache_folder_separator);
					resultCacheBase = Utilities.pathJoin([
						albumCacheBase,
						collectedAlbumCacheBase,
						collectionCacheBase
					]);
				}
			} else {
				if (albumCacheBase === env.options.folders_string) {
					// stay there
					resultCacheBase = albumCacheBase;
				} else if ([env.options.by_date_string, env.options.by_gps_string].indexOf(albumCacheBase) !== -1) {
					// go to folders root
					resultCacheBase = env.options.folders_string;
				} else if (Utilities.isSearchCacheBase(albumCacheBase)) {
					let searchObject = PhotoFloat.decodeSearchCacheBase(albumCacheBase);
					// the return folder must be extracted from the album hash
					resultCacheBase = searchObject.searchedCacheBase;
				} else if (Utilities.isMapCacheBase(albumCacheBase)) {
					// the return folder must be extracted from the album hash
					let mapObject = PhotoFloat.decodeMapCacheBase(albumCacheBase);
					resultCacheBase = mapObject.clickAlbumCacheBase;
					if (
						mapObject.whatToGenerateTheMapFor.length > 1 && (
							env.currentAlbum.media.findIndex(
								singleMedia => singleMedia.cacheBase === mapObject.whatToGenerateTheMapFor
							) > -1
						)
					)
						resultCacheBase = [resultCacheBase, mapObject.clickAlbumCacheBase].join("/");
				} else if (Utilities.isSelectionCacheBase(albumCacheBase)) {
					resultCacheBase = env.options.folders_string;
				} else {
					let album = env.cache.getAlbum(albumCacheBase);
					if (Utilities.isSelectionCacheBase(albumCacheBase) && ! album) {
						resultCacheBase = env.options.folders_string;
					} else if (Utilities.isSelectionCacheBase(albumCacheBase) && album.numVisibleMedia() > 1) {
						// if all the media belong to the same album => the parent
						// other ways => the common root of the selected media
						let minimumLength = 100000;
						let parts = [];
						for (let iMedia = 0; iMedia < album.media.length; iMedia ++) {
							let splittedSelectedMediaCacheBase = album.media[iMedia].foldersCacheBase.split(env.options.cache_folder_separator);
							if (splittedSelectedMediaCacheBase.length < minimumLength)
								minimumLength = splittedSelectedMediaCacheBase.length;
						}
						for (let iPart = 0; iPart < minimumLength; iPart ++)
							parts[iPart] = [];
						for (let iMedia = 0; iMedia < album.media.length; iMedia ++) {
							let splittedSelectedMediaCacheBase = album.media[iMedia].foldersCacheBase.split(env.options.cache_folder_separator);
							for (let iPart = 0; iPart < minimumLength; iPart ++) {
								if (! iPart)
									parts[iPart][iMedia] = splittedSelectedMediaCacheBase[iPart];
								else
									parts[iPart][iMedia] = parts[iPart - 1][iMedia] + env.options.cache_folder_separator + splittedSelectedMediaCacheBase[iPart];
							}
						}
						resultCacheBase = '';
						for (let iPart = 0; iPart < minimumLength; iPart ++) {
							if (parts[iPart].some((val, i, arr) => val !== arr[0])) {
								break;
							} else {
								resultCacheBase = parts[iPart][0];
							}
						}
					} else if (Utilities.isSelectionCacheBase(albumCacheBase) && album.numVisibleMedia() === 1) {
						resultCacheBase = album.media[0].foldersCacheBase;
					} else {
						// we must go up in the sub folders tree
						resultCacheBase = albumCacheBase.split(env.options.cache_folder_separator).slice(0, -1).join(env.options.cache_folder_separator);
					}
				}
			}
		} else {
			// hash of a media: remove the media
			if (collectionCacheBase !== null || Utilities.isFolderCacheBase(albumCacheBase)) {
				// media in found album or in one of its subalbum
				// or
				// media in folder hash:
				// remove the trailing media
				resultCacheBase = Utilities.pathJoin(hashToGoUp.split("/").slice(1, -1));
			} else {
				// all the other cases
				// remove the trailing media and the folder it's inside
				resultCacheBase = Utilities.pathJoin(hashToGoUp.split("/").slice(1, -2));
			}
		}

		return env.hashBeginning + resultCacheBase;
	};


	/* Error displays */
	Utilities.prototype.errorThenGoUp = function(error) {
		if (error === 403) {
			$("#auth-text").stop().fadeIn(1000);
			$("#password").trigger("focus");
		} else {
			// Jason's code only had the following line
			//$("#error-text").stop().fadeIn(2500);

			var rootHash = env.hashBeginning + env.options.folders_string;

			$("#album-view").fadeOut(200);
			$("#media-view").fadeOut(200);

			$("#loading").hide();
			if (window.location.hash === rootHash) {
				$("#error-text-folder").stop();
				$("#error-root-folder").stop().fadeIn(2000);
				$("#powered-by").show();
			} else {
				$("#error-text-folder").stop().fadeIn(
					200,
					function() {
						window.location.href = Utilities.upHash();
					}
				);
				$("#error-text-folder, #error-overlay, #auth-text").fadeOut(3500);
				$("#album-view").stop().fadeOut(100).fadeIn(3500);
				$("#media-view").stop().fadeOut(100).fadeIn(3500);
			}
		}
		// $("#error-overlay").fadeTo(500, 0.8);
		$("body, html").css("overflow", "hidden");
	};

	Utilities.convertMd5ToCode = function(md5) {
		var index = env.guessedPasswordsMd5.indexOf(md5);
		return env.guessedPasswordCodes[index];
	};

	Utilities.prototype.noOp = function() {
		// console.trace();
	};

	Utilities.prototype.convertCodesListToMd5sList = function(codesList) {
		var i, index, md5sList = [];
		for (i = 0; i < codesList.length; i ++) {
			if (! codesList[i]) {
				md5sList.push('');
			} else {
				index = env.guessedPasswordCodes.indexOf(codesList[i]);
				if (index != -1)
					md5sList.push(env.guessedPasswordsMd5[index]);
			}
		}
		return md5sList;
	};

	Utilities.prototype.convertCodesComplexCombinationToCodesSimpleCombination = function(codesComplexCombination) {
		let [albumCodesComplexCombinationList, mediaCodesComplexCombinationList] = PhotoFloat.convertComplexCombinationsIntoLists(codesComplexCombination);
		if (albumCodesComplexCombinationList.length && mediaCodesComplexCombinationList.length)
			return [albumCodesComplexCombinationList[0], mediaCodesComplexCombinationList[0]].join (',');
		else if (albumCodesComplexCombinationList.length && ! mediaCodesComplexCombinationList.length)
			return [albumCodesComplexCombinationList[0], ''].join (',');
		else if (! albumCodesComplexCombinationList.length && mediaCodesComplexCombinationList.length)
			return ['', mediaCodesComplexCombinationList[0]].join (',');
		else
			return '';
	};

	Utilities.prototype.videoOK = function() {
		if (! Modernizr.video || ! Modernizr.video.h264)
			return false;
		else
			return true;
	};

	Utilities.prototype.addVideoUnsupportedMarker = function(id) {
		if (! Modernizr.video) {
			$(".media-box#" + id + " .media-box-inner").html('<div class="video-unsupported-html5"></div>');
			return false;
		}
		else if (! Modernizr.video.h264) {
			$(".media-box#" + id + " .media-box-inner").html('<div class="video-unsupported-h264"></div>');
			return false;
		} else
			return true;
	};

	Utilities.prototype.undie = function() {
		$(".error, #error-overlay, #auth-text", ".search-failed").fadeOut(500);
		$("body, html").css("overflow", "auto");
	};

	Utilities.prototype.humanFileSize = function(fileSize) {
		// from https://stackoverflow.com/questions/10420352/converting-file-size-in-bytes-to-human-readable-string
		if (! fileSize)
			return 0;
		var i = Math.floor(Math.log(fileSize) / Math.log(1024));
		return env.numberFormat.format((fileSize / Math.pow(1024, i)).toFixed(2) * 1) + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
	};

	Utilities.prototype.getAlbumNameFromCacheBase = function(cacheBase) {
		return new Promise(
			function(resolve_getAlbumNameFromAlbumHash) {
				let getAlbumPromise = PhotoFloat.getAlbum(cacheBase, Utilities.noOp, {getMedia: false, getPositions: false});
				getAlbumPromise.then(
					function(theAlbum) {
						var path;
						var splittedPath = theAlbum.path.split('/');
						if (splittedPath[0] === env.options.folders_string) {
							splittedPath[0] = "";
						} else if (splittedPath[0] === env.options.by_date_string) {
							splittedPath[0] = "(" + Utilities._t("#by-date") + ")";
						} else if (splittedPath[0] === env.options.by_gps_string) {
							splittedPath = theAlbum.ancestorsNames;
							splittedPath[0] = "(" + Utilities._t("#by-gps") + ")";
						} else if (splittedPath[0] === env.options.by_map_string) {
							splittedPath = ["(" + Utilities._t("#by-map") + ")"];
						} else if (splittedPath[0] === env.options.by_selection_string) {
							splittedPath = ["(" + Utilities._t("#by-selection") + ")"];
						}
						path = splittedPath.join('/');

						resolve_getAlbumNameFromAlbumHash(path);
					},
					function() {
						console.trace();
					}
				);
			}
		);
	};

	Utilities.prototype.itsTheSameSet = function(array1, array2) {
		var set1 = new Set(array1);
		var set2 = new Set(array2);
		return set1.size === set2.size && (set1.size === new Set([...set1, ...set2]).size);
	};

	Utilities.prototype.highlightSearchedWords = function(whenTyping = false) {
		var isSearch = Utilities.isSearchHash();
		if (isSearch || whenTyping) {
			// highlight the searched text

			let searchWords;
			if (whenTyping) {
				let searchString = $("#search-field").val().trim();
				if (env.search.tagsOnly) {
					searchWords = [searchString];
				} else {
					searchWords = searchString.split(' ');
				}
			} else {
				searchWords = env.currentAlbum.search.words;
			}

			let selector = ".title .media-name, .description, .description-tags .tag";

			let baseSelector = "";
			if (Utilities.isPopup())
				baseSelector = "#popup-images-wrapper";
			else if (env.currentMedia === null)
				baseSelector = "#album-and-media-container";

			let mediaNameSelector = ".media-name";
			let albumNameSelector = ".album-name";
			if ($("#subalbums .first-line, #thumbs .first-line").length) {
				mediaNameSelector += " .first-line";
				albumNameSelector += " .first-line";
			}

			if (baseSelector.length) {
				selector += ", " +
					baseSelector + " " + mediaNameSelector + ", " +
					baseSelector + " " + albumNameSelector + ", " +
					baseSelector + " .media-description, " +
					baseSelector + " .album-description, " +
					baseSelector + " .album-tags, " +
					baseSelector + " .media-tags";
			}

			var punctuationArray = ":;.,-–—‒_(){}[]¡!`´·#|@~&/*^¨'+=?¿<>\\\"".split("");
			const options = {
				caseSensitive: env.search.caseSensitive,
				diacritics: ! env.search.accentSensitive,
				separateWordSearch: whenTyping ? true : env.search.anyWord,
				accuracy: env.search.insideWords || whenTyping ? "partially" : {
					value: "exactly",
					limiters: punctuationArray
				},
				ignorePunctuation: punctuationArray,
				filter: function(node, term, totalCounter, counter) {
					return env.search.numbers || term.match(new RegExp(".*\p{N}+.*")) === null;
				},
				done: function() {
					// make the descritpion and the tags visible if highlighted
					for (let selector of ["#subalbums", "#thumbs"]) {
						let adapt = false;
						$(selector + " .description.ellipsis").each(
							function() {
								if ($(this).html().indexOf(env.markTagBegin) !== -1) {
									$(this).css("text-overflow", "unset").css("overflow", "visible").css("white-space", "unset");
									adapt = true;
								}
							}
						);
						$(selector + " .album-tags, " + selector + " .media-tags").each(
							function() {
								if ($(this).html().indexOf(env.markTagBegin) !== -1) {
									$(this).removeClass("hidden-by-option");
									adapt = true;
								}
							}
						);
						if (adapt) {
							if (selector === "#subalbums") {
								Utilities.adaptSubalbumCaptionHeight(); // highlighting searched words
							} else {
								Utilities.adaptMediaCaptionHeight();
								if (Utilities.isPopup())
									Utilities.adaptMediaCaptionHeight(true);
							}
						}
					}

					// bottom right tags will be made visible if highlighted in Utilities.setDescriptionOptions()

					// make visible the file name if it's highlighted
					let html = $("#media-name-second-part").html();
					if (html && html.indexOf("<mark data-markjs=") !== -1) {
						$(".with-second-part").addClass("hovered");
					}
				}
			};
			$.executeAfterEvent(
				"otherJsFilesLoadedEvent",
				function() {
					$(selector).unmark(
						{
							done: function() {
								$(selector).mark(
									searchWords,
									options
								);
							}
						}
					);
				}
			);
		}
	};

	Utilities.prototype.horizontalDistance = function(object1, object2) {
		var leftOffset1 = object1.offset().left;
		var leftOffset2 = object2.offset().left;
		var rightOffset1 = leftOffset1 + object1.outerWidth();
		var rightOffset2 = leftOffset2 + object2.outerWidth();
		return ((rightOffset2 + leftOffset2) / 2 - (rightOffset1 + leftOffset1) / 2);
	};

	Utilities.prototype.verticalDistance = function(object1, object2) {
		var img1 = object1.children().first().children("img");
		var img2 = object2.children().first().children("img");
		var topOffset1 = img1.offset().top;
		var topOffset2 = img2.offset().top;
		var bottomOffset1 = topOffset1 + img1.outerHeight();
		var bottomOffset2 = topOffset2 + img2.outerHeight();
		return ((topOffset2 + bottomOffset2) / 2 - (topOffset1 + bottomOffset1) / 2);
	};

	Utilities.prototype.tenYears = function() {
		// returns the expire interval for the cookies, in seconds
		return 10 * 365 * 24 * 60 * 60;
	};

	Utilities.prototype.settingsChanged = function() {
		return (
			! (
				env.albumSort === "name" && env.options.default_album_name_sort ||
				env.albumSort === "exifDateMax" && ! env.options.default_album_name_sort
			) ||
			env.albumReverseSort !== env.options.default_album_reverse_sort ||
			! (
				env.mediaSort === "name" && env.options.default_media_name_sort ||
				env.mediaSort === "exifDateMax" && ! env.options.default_media_name_sort
			) ||
			env.mediaReverseSort !== env.options.default_media_reverse_sort ||
			env.defaultOptions.show_album_media_count !== env.options.show_album_media_count ||
			env.defaultOptions.hide_title !== env.options.hide_title ||
			$("#album-and-media-container").hasClass("show-title") ||
			env.defaultOptions.albums_slide_style !== env.options.albums_slide_style ||
			! env.defaultOptions.only_square_thumbnails &&
			env.defaultOptions.album_thumb_type !== env.options.album_thumb_type ||
			env.defaultOptions.show_album_names_below_thumbs !== env.options.show_album_names_below_thumbs ||
			! env.defaultOptions.only_square_thumbnails &&
			env.defaultOptions.media_thumb_type !== env.options.media_thumb_type ||
			env.defaultOptions.show_media_names_below_thumbs !== env.options.show_media_names_below_thumbs ||
			env.defaultOptions.hide_descriptions !== env.options.hide_descriptions ||
			env.defaultOptions.hide_tags !== env.options.hide_tags ||
			env.defaultOptions.thumb_spacing !== env.options.spacing ||
			env.defaultOptions.hide_bottom_thumbnails !== env.options.hide_bottom_thumbnails ||
			env.defaultOptions.flat_downloads !== env.options.flat_downloads ||
			env.defaultOptions.download_images !== env.options.download_images ||
			env.defaultOptions.download_images_size_index !== env.options.download_images_size_index ||
			env.defaultOptions.download_images_format !== env.options.download_images_format ||
			env.defaultOptions.download_audios !== env.options.download_audios ||
			env.defaultOptions.download_audios_size !== env.options.download_audios_size ||
			env.defaultOptions.download_videos !== env.options.download_videos ||
			env.defaultOptions.download_videos_size !== env.options.download_videos_size ||
			env.defaultOptions.download_subalbums !== env.options.download_subalbums ||
			env.defaultOptions.download_selection !== env.options.download_selection ||
			env.defaultOptions.save_data !== env.options.save_data ||
			env.search.insideWords ||
			env.search.anyWord ||
			env.search.caseSensitive ||
			env.search.accentSensitive ||
			env.search.numbers ||
			env.search.tagsOnly ||
			! env.search.currentAlbumOnly ||
			env.options.show_big_virtual_folders ||
			env.slideshowInterval !== env.slideshowIntervalDefault
		);
	};

	Utilities.prototype.hideContextualHelp = function() {
		$("#contextual-help").stop().fadeTo(
			200,
			0,
			function() {
				$("#contextual-help").removeClass("visible");
				$("#contextual-help").hide();
			}
		);
		$("#album-and-media-container").stop().fadeTo(200, 1);
		if ($("#my-modal").hasClass("fadedOut")) {
			$("#my-modal").removeClass("fadedOut").stop().fadeTo(200, 1);
		}

	};

	Utilities.prototype.showContextualHelp = function() {
		if ($("#auth-text").is(":visible") || $("#contextual-help").hasClass("visible"))
			return;
		$("#album-and-media-container").stop().fadeTo(500, 0.1);
		if ($("#my-modal").is(":visible")) {
			$("#my-modal").addClass("fadedOut").stop().fadeTo(500, 0.1);
		}
		// hide everything except scope "any"
		$("#contextual-help .shortcuts tr td.scope:not(.any)").parent().hide();
		// show what is pertinent
		if ($("#right-and-search-menu .menu").hasClass("expanded")) {
			// is any menu
			$(
				"#contextual-help .shortcuts tr td.scope.menu, " +
				"#contextual-help .shortcuts tr td.scope.expandable-menu, " +
				"#contextual-help .shortcuts tr td.scope.menu-command"
			).parent().show();
		} else if (env.currentMedia !== null && ! Utilities.isMap() && ! Utilities.isPopup()) {
			// is a single media
			$(
				"#contextual-help .shortcuts tr td.scope.any-but-menu, " +
				"#contextual-help .shortcuts tr td.scope.album-and-single-media, " +
				"#contextual-help .shortcuts tr td.scope.single-media, " +
				"#contextual-help .shortcuts tr td.scope.root-albums-and-single-media, " +
				"#contextual-help .shortcuts tr td.scope.enlarged-image, " +
				"#contextual-help .shortcuts tr td.scope.video"
			).parent().show();
		} else if (! Utilities.isMap()) {
			// is an album or a popup
			$(
				"#contextual-help .shortcuts tr td.scope.any-but-menu, " +
				"#contextual-help .shortcuts tr td.scope.album, " +
				"#contextual-help .shortcuts tr td.scope.album-and-single-media"
			).parent().show();
			if (env.currentAlbum && env.currentAlbum.isAnyRoot() && ! Utilities.isPopup()) {
				// is a root album
				$(
					"#contextual-help .shortcuts tr td.scope.any-but-menu, " +
					"#contextual-help .shortcuts tr td.scope.root-albums-and-single-media"
				).parent().show();
			}
		}
		$("#contextual-help").stop().fadeTo(
			10,
			0.1,
			function() {
				$("#contextual-help").addClass("visible");
				$("#contextual-help").css("width", "unset");
				var currentWidth = parseInt($("#contextual-help").css("width"));
				var tableCurrentWidth = parseInt($("#contextual-help table.shortcuts").css("width"));
				$("#contextual-help").css("width", Math.min(currentWidth, tableCurrentWidth + 100).toString() + "px");
				$("#contextual-help").stop().fadeTo(500, 1);
			}
		);
	};


	/* make static methods callable as member functions */
	Utilities.prototype.isFolderCacheBase = Utilities.isFolderCacheBase;
	Utilities.prototype.pathJoin = Utilities.pathJoin;
	Utilities.prototype.setLinksVisibility = Utilities.setLinksVisibility;
	Utilities.prototype.setPrevNextVisibility = Utilities.setPrevNextVisibility;
	Utilities.prototype.currentSizeAndIndex = Utilities.currentSizeAndIndex;
	Utilities.prototype.distanceBetweenCoordinatePoints = Utilities.distanceBetweenCoordinatePoints;
	Utilities.prototype.xDistanceBetweenCoordinatePoints = Utilities.xDistanceBetweenCoordinatePoints;
	Utilities.prototype.yDistanceBetweenCoordinatePoints = Utilities.yDistanceBetweenCoordinatePoints;
	Utilities.prototype.degreesToRadians = Utilities.degreesToRadians;
	Utilities.prototype.isByDateCacheBase = Utilities.isByDateCacheBase;
	Utilities.prototype.isByGpsCacheBase = Utilities.isByGpsCacheBase;
	Utilities.prototype.isSearchCacheBase = Utilities.isSearchCacheBase;
	Utilities.prototype.isSelectionCacheBase = Utilities.isSelectionCacheBase;
	Utilities.prototype.isMapCacheBase = Utilities.isMapCacheBase;
	Utilities.prototype.convertMd5ToCode = Utilities.convertMd5ToCode;
	Utilities.prototype._t = Utilities._t;
	Utilities.prototype._s = Utilities._s;
	Utilities.prototype.upHash = Utilities.upHash;
	Utilities.prototype.transformAltPlaceName = Utilities.transformAltPlaceName;
	Utilities.prototype.normalizeAccordingToOptions = Utilities.normalizeAccordingToOptions;
	Utilities.prototype.removeAccents = Utilities.removeAccents;
	Utilities.prototype.addTagLink = Utilities.addTagLink;
	Utilities.prototype.isShiftOrControl = Utilities.isShiftOrControl;
	Utilities.prototype.isPopup = Utilities.isPopup;
	Utilities.prototype.setTitleOptions = Utilities.setTitleOptions;
	Utilities.prototype.setMediaOptions = Utilities.setMediaOptions;
	Utilities.prototype.setFinalOptions = Utilities.setFinalOptions;
	Utilities.prototype.isMap = Utilities.isMap;
	Utilities.prototype.removeHighligths = Utilities.removeHighligths;
	Utilities.prototype.isSearchHash = Utilities.isSearchHash;
	Utilities.prototype.adaptSubalbumCaptionHeight = Utilities.adaptSubalbumCaptionHeight;
	Utilities.prototype.adaptMediaCaptionHeight = Utilities.adaptMediaCaptionHeight;
	Utilities.prototype.addDescriptionOpacity = Utilities.addDescriptionOpacity;
	Utilities.prototype.geotaggedContentIsHidden = Utilities.geotaggedContentIsHidden;
	Utilities.prototype.hasSomeDescription = Utilities.hasSomeDescription;
	Utilities.prototype.objectIsASubalbum = Utilities.objectIsASubalbum;
	Utilities.prototype.addMediaLazyLoader = Utilities.addMediaLazyLoader;
	Utilities.prototype.isPhp = Utilities.isPhp;
	Utilities.prototype.correctElementPositions = Utilities.correctElementPositions;
	Utilities.prototype.setMapButtonPosition = Utilities.setMapButtonPosition;
	Utilities.prototype.setUpButtonVisibility = Utilities.setUpButtonVisibility;
	Utilities.prototype.aSubalbumIsHighlighted = Utilities.aSubalbumIsHighlighted;
	Utilities.prototype.aSingleMediaIsHighlighted = Utilities.aSingleMediaIsHighlighted;
	Utilities.prototype.highlighObject = Utilities.highlighObject;
	Utilities.prototype.highlightedObject = Utilities.highlightedObject;
	Utilities.prototype.addHighlightToMediaOrSubalbum = Utilities.addHighlightToMediaOrSubalbum;
	Utilities.prototype.scrollAlbumViewToHighlightedSubalbum = Utilities.scrollAlbumViewToHighlightedSubalbum;
	Utilities.prototype.scrollAlbumViewToHighlightedMedia = Utilities.scrollAlbumViewToHighlightedMedia;
	Utilities.prototype.scrollPopupToHighlightedThumb = Utilities.scrollPopupToHighlightedThumb;
	Utilities.prototype.encodeHash = Utilities.encodeHash;
	Utilities.prototype.conditionalLoadMatomoTracking = Utilities.conditionalLoadMatomoTracking;
	Utilities.prototype.conditionalLoadOthersJsFiles = Utilities.conditionalLoadOthersJsFiles;
	Utilities.prototype.decodeAllNonAlfaNum = Utilities.decodeAllNonAlfaNum;
	Utilities.prototype.decodeMapClickHistory = Utilities.decodeMapClickHistory;
	Utilities.prototype.roundDecimals = Utilities.roundDecimals;
	Utilities.prototype.setDescriptionOptions = Utilities.setDescriptionOptions;
	Utilities.prototype.thereIsAVerticalScrollBar = Utilities.thereIsAVerticalScrollBar;
	Utilities.prototype.windowVerticalScrollbarWidth = Utilities.windowVerticalScrollbarWidth;
	Utilities.prototype.absolutelyNothingIsSelected = Utilities.absolutelyNothingIsSelected;
	Utilities.prototype.encodeSelectionCacheBase = Utilities.encodeSelectionCacheBase;
	Utilities.prototype.encodeAllNonAlfaNum = Utilities.encodeAllNonAlfaNum;
	Utilities.prototype.addGeoContentInfoToSelector = Utilities.addGeoContentInfoToSelector;
	Utilities.prototype.separateSelectorWithGeoContentInfo = Utilities.separateSelectorWithGeoContentInfo;
	Utilities.prototype.hasGeoContentInfo = Utilities.hasGeoContentInfo;
	Utilities.prototype.modifyCheckedState = Utilities.modifyCheckedState;
	Utilities.prototype.initializeSelectionAlbum = Utilities.initializeSelectionAlbum;
	Utilities.prototype.isAnyRootCacheBase = Utilities.isAnyRootCacheBase;
	Utilities.prototype.somethingIsInMapAlbum = Utilities.somethingIsInMapAlbum;

	window.Utilities = Utilities;
}());
