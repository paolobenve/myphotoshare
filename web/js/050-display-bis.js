$(document).ready(function() {
	var phFl = new PhotoFloat();
	var util = new Utilities();
	var pS;
	// $(document).ready(
	// 	function() {
	$.executeAfterEvent(
		"pinchSwipeFunctionsLoadedEvent",
		function() {
			pS = new PinchSwipe();
			// $(document).trigger("pinchSwipeOkInMenuFunctions");
		}
	);
	var menuF = new MenuFunctions();
	var tF = new TopFunctions();
	var mapF;
	$.executeAfterEvent(
		"mapFunctionsLoadedEvent",
		function() {
			mapF = new MapFunctions();
		}
	);

	/* Event listeners */

	$(document).off("keydown").on(
		"keydown",
		function(e) {
			function generatesortingMessageIdsArray(mode, beginWithReverse = false) {
				// this function is used to make the sort change with the shortcut
				// while in the menu all the sorting are activable, with the shortcut only the main ones are
				let sortingMessageIds = [];
				if (mode === "album") {
					if (env.options.expose_image_dates) {
						if (beginWithReverse) {
							sortingMessageIds.push("by-exif-date-max-reverse");
							sortingMessageIds.push("by-exif-date-max");
						} else {
							sortingMessageIds.push("by-exif-date-max");
							sortingMessageIds.push("by-exif-date-max-reverse");
						}
						beginWithReverse = ! beginWithReverse;
					}

					if (env.options.expose_original_media) {
						if (beginWithReverse) {
							sortingMessageIds.push("by-file-date-max-reverse");
							sortingMessageIds.push("by-file-date-max");
						} else {
							sortingMessageIds.push("by-file-date-max");
							sortingMessageIds.push("by-file-date-max-reverse");
						}
						beginWithReverse = ! beginWithReverse;
					}
				} else {
					if (env.options.expose_image_dates) {
						if (beginWithReverse) {
							sortingMessageIds.push("by-exif-date-reverse");
							sortingMessageIds.push("by-exif-date");
						} else {
							sortingMessageIds.push("by-exif-date");
							sortingMessageIds.push("by-exif-date-reverse");
						}
						beginWithReverse = ! beginWithReverse;
					}
					if (env.options.expose_original_media) {
						if (beginWithReverse) {
							sortingMessageIds.push("by-file-date-reverse");
							sortingMessageIds.push("by-file-date");
						} else {
							sortingMessageIds.push("by-file-date");
							sortingMessageIds.push("by-file-date-reverse");
						}
						beginWithReverse = ! beginWithReverse;
					}
				}

				if (beginWithReverse) {
					sortingMessageIds.push("by-name-reverse");
					sortingMessageIds.push("by-name");
				} else {
					sortingMessageIds.push("by-name");
					sortingMessageIds.push("by-name-reverse");
				}
				beginWithReverse = ! beginWithReverse;

				if (mode === "album") {
					if (beginWithReverse) {
						sortingMessageIds.push("by-pixel-size-max-reverse");
						sortingMessageIds.push("by-pixel-size-max");
					} else {
						sortingMessageIds.push("by-pixel-size-max");
						sortingMessageIds.push("by-pixel-size-max-reverse");
					}
				} else {
					if (beginWithReverse) {
						sortingMessageIds.push("by-pixel-size-reverse");
						sortingMessageIds.push("by-pixel-size");
					} else {
						sortingMessageIds.push("by-pixel-size");
						sortingMessageIds.push("by-pixel-size-reverse");
					}
				}
				beginWithReverse = ! beginWithReverse;

				return sortingMessageIds;
			}
			// end of nested function

			if (e.key === undefined || env.currentAlbum === null) {
				return true;
			}

			if ($(".search-failed:visible").length) {
				$("#loading").html(util._t("#loading"));
				$(".search-failed:visible").fadeOut(300);
				return false;
			}

			var isMap = util.isMap();
			var isPopup = util.isPopup();
			var isAuth = $("#auth-text").is(":visible");

			let upLink = util.upHash();
			let numVisibleSubalbums = env.currentAlbum.visibleSubalbums().length;
			let numVisibleMedia = env.currentAlbum.numVisibleMedia();
			let isABigTransversalAlbum = env.currentAlbum.isABigTransversalAlbum();

			if (e.key === "Escape") {
				// warning: modern browsers will always exit fullscreen when pressing esc

				if (isAuth) {
					$("#auth-close")[0].click();
					return false;
				} else if ($("#how-to-download-selection").is(":visible")) {
					$("#how-to-download-selection-close")[0].click();
					return false;
				} else if ($("#contextual-help").hasClass("visible")) {
					util.hideContextualHelp();
					return false;
				} else if ($("#you-can-suggest-single-media-position").is(":visible")) {
					$("#you-can-suggest-single-media-position").hide();
					return false;
				} else if ($("#tiny-url").is(":visible")) {
					$("#tiny-url").fadeOut();
					$("#album-and-media-container").stop().fadeTo(400, 1);
					return false;
				} else if ($("#right-and-search-menu").hasClass("expanded")) {
					menuF.closeMenu();
					menuF.showHideDownloadSelectionInfo();
					return false;
				} else if (env.currentMedia !== null && env.currentMedia.isAudio() && $("audio#media-center").length && ! $("audio#media-center")[0].paused) {
					// stop the audio, otherwise it keeps playing
					$("audio#media-center")[0].pause();
					return false;
				} else if (env.currentMedia !== null && env.currentMedia.isVideo() && $("video#media-center").length && ! $("video#media-center")[0].paused) {
					// stop the video, otherwise it keeps playing
					$("video#media-center")[0].pause();
					return false;
				} else if (util.isShiftOrControl()) {
					// the +/- popup is there: close it
					$(".shift-or-control .leaflet-popup-close-button")[0].click();
					return false;
				} else if (isPopup) {
					// the popup is there: close it
					env.highlightedObjectId = null;
					$(".media-popup .leaflet-popup-close-button")[0].click();
					// env.mapAlbum = util.initializeMapAlbum();
					menuF.updateMenu(env.currentAlbum);
					return false;
				} else if (isMap) {
					// we are in a map: close it
					$(".modal-close")[0].click();
					env.popupRefreshType = "previousAlbum";
					env.mapRefreshType = null;
					// the menu must be updated here in order to have the browsing mode shortcuts workng
					menuF.updateMenu();
					return false;
				} else if (env.currentMedia && (env.currentZoom > env.initialZoom || $(".media-box#center .title").hasClass("hidden-by-pinch"))) {
					pS.pinchOut(null, null);
					return false;
				} else if (! Modernizr.fullscreen && env.fullScreenStatus) {
					// actually execution comes here only when the fullscreen mode is not the browser one
					// if the browser manages fullscreen mode, the callback of the toggleFullscreen function is executed instead
					util.toggleFullscreen(e);
					return false;
				} else if (upLink) {
					$("#up-button").trigger("click");
					return false;
				}
			} else if (! isAuth) {
				if (
					e.key.toLowerCase() === util._s("#protected-content-unveil-shortcut") &&
					env.currentAlbum !== null &&
					env.currentAlbum.hasVeiledProtectedContent()
				) {
					util.showAuthForm();
					return false;
				}

				// "e" opens the menu, and closes it if focus is not in input field
				if (
					! e.shiftKey &&  ! e.ctrlKey &&  ! e.altKey &&
					e.key.toLowerCase() === util._s(".menu-icon-title-shortcut") && (
						! $("#right-and-search-menu").hasClass("expanded") ||
						$("#search-menu").hasClass("hidden-by-menu-selection")
					)
				) {
					menuF.toggleMenu();
					menuF.showHideDownloadSelectionInfo();
					return false;
				}

				if ($("#right-and-search-menu").hasClass("expanded")) {
					if (
						(
							! $("#search-field").is(":focus") || e.key === "Tab"
						) &&
						! e.ctrlKey && ! e.altKey
					) {
						let highlightedItemObject = menuF.highlightedItemObject();
						if (e.key === "Enter" || e.key === " ") {
							if (highlightedItemObject.hasClass("first-level") && highlightedItemObject.hasClass("expandable")) {
								env.isMenuAction = true;
								highlightedItemObject.children().trigger("click");
							} else if (highlightedItemObject.attr("id") === "search-menu") {
								$("#search-button").trigger("click");
							} else {
								highlightedItemObject.trigger("click");
							}
							return false;
						} else if (e.key === "ArrowDown" || e.key === "ArrowUp" || e.key === "Tab") {
							let nextItemFunction;
							if (e.key === "ArrowUp" && ! e.shiftKey || e.key === "ArrowDown" && e.shiftKey || e.key === "Tab" && e.shiftKey)
								nextItemFunction = menuF.prevItemForHighlighting;
							else
								nextItemFunction = menuF.nextItemForHighlighting;
							let nextItem = nextItemFunction(highlightedItemObject);
							menuF.addHighlightToItem(nextItem);
							$("#search-field").blur();
							menuF.showHideDownloadSelectionInfo();
							return false;
						} else if (e.key === "ArrowLeft" || e.key === "ArrowRight") {
							$("#right-and-search-menu li.first-level.hidden-by-menu-selection.was-highlighted, #search-menu.hidden-by-menu-selection.was-highlighted").addClass("highlighted-menu").removeClass("was-highlighted");
							$("#right-and-search-menu li.first-level:not(.hidden-by-menu-selection).highlighted-menu, #search-menu:not(.hidden-by-menu-selection).highlighted-menu").removeClass("highlighted-menu").addClass("was-highlighted");
							$("#right-and-search-menu li.first-level, #search-menu").toggleClass("hidden-by-menu-selection");
							$("#menu-icon, #search-icon, #right-menu, #search-menu").toggleClass("expanded");
							menuF.highlightMenu();
							menuF.focusSearchField();
							menuF.showHideDownloadSelectionInfo();
							return false;
						}
					}

					if (! $("#search-menu").hasClass("hidden-by-menu-selection") && ! $("#search-field").is(":focus")) {
						// focus the search field, so that the typed text is added
						$("#search-field").trigger("focus");
					}
				}

				if (! isMap) {
					if (e.key === "Enter" && $("#tiny-url").is(":visible")) {
						$("#tiny-url-button").trigger("click");
						return false;
					}

					if (
						env.currentAlbum !== null &&
						numVisibleMedia > 1 &&
						! $("#search-menu").hasClass("expanded") &&
						e.key.toLowerCase() === util._s(".slideshow-icon-title-shortcut")
					) {
						if (! env.slideshowId)
							$("#slideshow-icon").trigger("click");
						else
							util.toggleFullscreen(e);
						return false;
					}

					if (
						! $("#search-menu").hasClass("expanded") &&
						! e.shiftKey &&  ! e.ctrlKey &&  ! e.altKey &&
						e.key.toLowerCase() === util._s(".info-icon-shortcut")
					) {
						if ($("#contextual-help").is(":visible"))
							util.hideContextualHelp();
						else
							util.showContextualHelp();
						return false;
					} else if ($("#right-and-search-menu").hasClass("expanded")) {

						if (
							$(".download-option-images").hasClass("highlighted-menu") &&
							["+", "-"].indexOf(e.key) !== -1
						) {
							let reverse = false;
							if (e.key === "-")
								reverse = true;
							tF.changeImagesDownloadSizeAndFormat(e, reverse);
						}

						if (
							$(".download-option-audios").hasClass("highlighted-menu") &&
							["+", "-"].indexOf(e.key) !== -1
						) {
							tF.changeAudiosDownloadSize(e);
						}

						if (
							$(".download-option-videos").hasClass("highlighted-menu") &&
							["+", "-"].indexOf(e.key) !== -1
						) {
							tF.changeVideosDownloadSize(e);
						}
					} else {
						// no class "expanded" in $("#right-and-search-menu")
		 				if (! e.altKey && e.key === " ") {
							if (env.currentMedia === null || env.currentAlbumIsAlbumWithOneMedia) {
								let highlightedObject = util.highlightedObject();
								util.selectBoxObject(highlightedObject).trigger("click");
								return false;
							} else if (e.ctrlKey){
								$("#select-box").trigger("click");
								return false;
							}
						}
						if (! e.ctrlKey && ! e.altKey) {
							let highlightedObject = util.highlightedObject();
							// let isTransversalAlbum = env.currentAlbum.isTransversal();
								// isTransversalAlbum &&
								// ! env.options.show_big_virtual_folders && (
								// 	numVisibleMedia > env.options.big_virtual_folders_threshold
								// );
							if (env.currentMedia === null && e.key === "Enter") {
								highlightedObject.trigger("click");
								return false;
							} else if (
								env.currentMedia === null &&
								! e.shiftKey && ! e.ctrlKey && (
									e.key === "ArrowLeft" || e.key === "ArrowRight" || e.key === "ArrowDown" || e.key === "ArrowUp"
								) && (
									numVisibleSubalbums ||
									! isABigTransversalAlbum && numVisibleMedia
								)
							) {
								let nextObjectFunction;
								if (e.key === "ArrowLeft" || e.key === "ArrowUp")
									nextObjectFunction = util.prevObjectForHighlighting;
								else
									nextObjectFunction = util.nextObjectForHighlighting;

								let nextObject;
								if (e.key === "ArrowLeft" || e.key === "ArrowRight") {
									nextObject = nextObjectFunction(highlightedObject);
								} else {
									// e.key is "ArrowDown" or "ArrowUp"
									let currentObject = highlightedObject;
									let arrayDistances = [], objectsInLine = [];
									// first, we must reach the next line
									while (true) {
										nextObject = nextObjectFunction(currentObject);
										if (nextObject.html() === highlightedObject.html()) {
											// we have returned to the original object
											return false;
										} else if (util.verticalDistance(highlightedObject, nextObject) !== 0) {
											// we aren't on the original line any more!
											break;
										}
										// we are still on the original line
										currentObject = nextObject;
									}
									// we have reached the next line
									// now we have to reach the same column

									currentObject = nextObject;
									let firstObjectInLine = currentObject;
									while (true) {
										arrayDistances.push(Math.abs(util.horizontalDistance(highlightedObject, currentObject)));
										objectsInLine.push(nextObject);
										nextObject = nextObjectFunction(currentObject);
										if (util.verticalDistance(firstObjectInLine, nextObject) !== 0) {
											// we aren't on the following line any more!
											break;
										}
										// we are still on the following line
										currentObject = nextObject;
									}
									// choose the object which have the minimum horizontal distance
									if (! objectsInLine.length)
										return false;
									let minimumDistanceIndex = arrayDistances.indexOf(Math.min(... arrayDistances));
									nextObject = objectsInLine[minimumDistanceIndex];
								}

								util.addHighlightToMediaOrSubalbum(nextObject);
								if (nextObject.hasClass("thumb-and-caption-container")) {
									if (isPopup)
										util.scrollPopupToHighlightedThumb(nextObject);
									else
										util.scrollAlbumViewToHighlightedMedia(nextObject);
								} else {
									util.scrollAlbumViewToHighlightedSubalbum(nextObject);
								}
								return false;
							} else if (e.key === util._s(".hide-everytyhing-shortcut")) {
								e.preventDefault();
								tF.toggleTitleAndBottomThumbnailsAndDescriptionsAndTags(e);
								return false;
							} else if (e.key === "ArrowRight" && (env.currentZoom !== env.initialZoom || env.prevMedia) && env.currentMedia !== null) {
								if (env.currentZoom === env.initialZoom) {
									$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-pinch");
									$("#next")[0].click();
								} else {
									// drag
									if (! e.shiftKey)
										pS.drag(env.windowWidth / 10, {x: -1, y: 0});
									else
										// faster
										pS.drag(env.windowWidth / 3, {x: -1, y: 0});
								}
								return false;
							} else if (e.key === " " && ! e.shiftKey && env.currentMedia !== null && env.currentMedia.isAudio()) {
								if (! $("audio#media-center").is(":focus")) {
									if ($("audio#media-center")[0].paused)
										// play the audio
										$("audio#media-center")[0].play();
									else
										// stop the audio
										$("audio#media-center")[0].pause();
								}
								return false;
							} else if (e.key === " " && ! e.shiftKey && env.currentMedia !== null && env.currentMedia.isVideo()) {
								if (! $("video#media-center").is(":focus")) {
									if ($("video#media-center")[0].paused)
										// play the video
										$("video#media-center")[0].play();
									else
										// stop the video
										$("video#media-center")[0].pause();
								}
								return false;
							} else if (env.slideshowId && e.key === " ") {
								if ($("#started-slideshow-pause").is(":visible"))
									$("#started-slideshow-pause")[0].click();
								else
									$("#started-slideshow-play")[0].click();
								return false;
							} else if (
								(
									e.key.toLowerCase() === util._s(".next-media-title-shortcut") ||
									e.key === "Backspace" && e.shiftKey ||
									(
										(e.key === "Enter" && ! $("#tiny-url").is(":visible")) ||
										e.key === " "
									) && ! e.shiftKey
								) &&
								env.nextMedia && env.currentMedia !== null
							) {
								$("#next")[0].click();
								return false;
							} else if (
								(e.key.toLowerCase() === util._s(".prev-media-title-shortcut") || e.key === "Backspace" && ! e.shiftKey || (e.key === "Enter" || e.key === " ") && e.shiftKey) &&
								env.prevMedia && env.currentMedia !== null
							) {
								$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-pinch");
								$("#prev")[0].click();
								return false;
							} else if (e.key === "ArrowLeft" && (env.currentZoom !== env.initialZoom || env.prevMedia) && env.currentMedia !== null) {
								if (env.currentZoom === env.initialZoom) {
									$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-pinch");
									$("#prev")[0].click();
								} else {
									// drag
									if (! e.shiftKey)
										pS.drag(env.windowWidth / 10, {x: 1, y: 0});
									else
										// faster
										pS.drag(env.windowWidth / 3, {x: 1, y: 0});
								}
								return false;
							} else if ((e.key === "ArrowUp" || e.key === "PageUp")) {
								let canGoUp = upLink && upLink !== window.location.hash;
								if (canGoUp && e.shiftKey && env.currentMedia === null) {
									$("#loading").show();
									pS.swipeDown(upLink);
									return false;
								} else if (env.currentMedia !== null) {
									if (env.currentZoom === env.initialZoom) {
										if (canGoUp && e.shiftKey) {
											pS.swipeDown(upLink);
											return false;
										}
									} else {
										// drag
										if (! e.shiftKey)
											pS.drag(env.windowHeight / 10, {x: 0, y: 1});
										else
											// faster
											pS.drag(env.windowHeight / 3, {x: 0, y: 1});
										return false;
									}
								}
							} else if (e.key === "ArrowDown" || e.key === "PageDown") {
							 	if (e.shiftKey && env.mediaLink && env.currentMedia === null) {
									pS.swipeUp(env.mediaLink);
									return false;
								} else if (env.currentMedia !== null) {
									if (env.currentZoom === env.initialZoom) {
										if (e.shiftKey) {
											pS.swipeDown(upLink);
											return false;
										}
									} else {
										if (! e.shiftKey)
											pS.drag(env.windowHeight / 10, {x: 0, y: -1});
										else
											// faster
											pS.drag(env.windowHeight / 3, {x: 0, y: -1});
										return false;
									}
								}
							} else if (e.key.toLowerCase() === util._s(".download-link-shortcut")) {
								if (env.currentMedia !== null)
									$(".download-single-media")[0].click();
								return false;
							} else if (e.key.toLowerCase() === util._s(".enter-fullscreen-shortcut") && ! isPopup) {
								util.toggleFullscreen(e);
								return false;
							} else if (e.key.toLowerCase() === util._s(".metadata-hide-shortcut") && env.currentMedia !== null) {
								util.toggleMetadata();
								return false;
							} else if (e.key.toLowerCase() === util._s(".original-link-shortcut") && env.currentMedia !== null) {
								$("#center .original-link")[0].click();
								return false;
							} else if (["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"].indexOf(e.key) > -1) {
								if (env.currentMedia !== null) {
									let number = parseInt(e.key);
									if (number === 0) {
										pS.pinchOut(null, env.initialZoom);
									} else if (number > env.currentZoom) {
										pS.pinchIn(null, number);
									} else {
										pS.pinchOut(null, number);
									}
									return false;
								}
							} else if (e.key === "+") {
								if (env.currentMedia !== null && env.currentMedia.isImage()) {
									pS.pinchIn(null);
									return false;
								}
							} else if (e.key === "-") {
								if (env.currentMedia !== null && env.currentMedia.isImage()) {
									pS.pinchOut(null, null);
									return false;
								}
							} else if (
								e.key.toLowerCase() === util._s(".map-link-shortcut") &&
							 	! isPopup &&
								! util.geotaggedContentIsHidden() &&
								(
									env.currentMedia !== null && (
										env.currentMedia.hasGpsData() || util.isPhp() && env.options.user_may_suggest_location && env.options.request_password_email
									) ||
									env.currentMedia === null && ! env.options.save_data && env.currentAlbum.positionsAndMediaInTree.length
								)
							) {
								if ($(".map-popup-trigger-double")[0] !== undefined)
									$(".map-popup-trigger-double")[0].click();
								else
									$(".map-popup-trigger")[0].click();
								return false;
							}

							if (e.key.toLowerCase() === util._s(".select.everything-here-shortcut")) {
								if (! e.shiftKey) {
									// select everything
									$("#everything-here.select:not(.hidden):not(.selected)").trigger("click");
								} else if (e.shiftKey) {
									// unselect everything
									$("#nothing-here.select").trigger("click");
								}
								return false;
							}
							if (
								env.currentMedia === null && (
									["[", "]"].indexOf(e.key) !== -1 && ! isPopup && numVisibleSubalbums > 1 ||
									["{", "}"].indexOf(e.key) !== -1 && (
										! isPopup && (numVisibleMedia > 1 || isABigTransversalAlbum) ||
										isPopup &&  env.mapAlbum.media.length > 1
									)
								) && env.currentMedia === null
							) {
								// media and subalbums sort switcher

								let mode, beginWithReverse;
								let sortingMessageIds;
								let currentSortingIndex;

								if (["[", "]"].includes(e.key)) {
									mode = "album";
									beginWithReverse = env.beginAlbumSortingWithReverse;
								} else {
									mode = "media";
									beginWithReverse = env.beginMediaSortingWithReverse;
								}

								sortingMessageIds = generatesortingMessageIdsArray(mode, beginWithReverse);
								let sortingMessagesLength = sortingMessageIds.length;
								let wasReverse, isReverse;

								let reverseMenuItemIsSelected = $(".sort." + mode + "-sort.reverse").hasClass("selected");
								for (let i = 0; i < sortingMessagesLength; i ++) {
									let sortingMessageId = sortingMessageIds[i];

									let sortingClassWithoutReverse = sortingMessageId;
									wasReverse = sortingMessageId.includes("-reverse");
									if (wasReverse)
										sortingClassWithoutReverse = sortingMessageId.substring(0, sortingMessageId.indexOf("-reverse"));

									if (
										$(".sort." + mode + "-sort." + sortingClassWithoutReverse).hasClass("selected") && (
											wasReverse && reverseMenuItemIsSelected ||
											! wasReverse && ! reverseMenuItemIsSelected
										)
									) {
										currentSortingIndex = i;
										break;
									}
								}

								$(".sort-message").stop().hide().css("opacity", "");
								let currentSortingMessageHasReverse = sortingMessageIds[currentSortingIndex].includes("-reverse");
								let currentSortingClass = sortingMessageIds[currentSortingIndex];
								let currentSortingClassWithoutReverse = currentSortingClass;
								if (currentSortingClass.includes("-reverse"))
									currentSortingClassWithoutReverse = currentSortingClass.substring(0, currentSortingClass.indexOf("-reverse"));

								let newSortingIndex, newSortingClass, newSortingClassWithoutReverse;
								let startIndex = currentSortingIndex;

								if (["[", "{"].indexOf(e.key) !== -1) {
									while (true) {
										newSortingIndex = (startIndex + sortingMessagesLength - 1) % sortingMessagesLength;
										newSortingClass = sortingMessageIds[newSortingIndex];
										isReverse = newSortingClass.includes("-reverse");
										newSortingClassWithoutReverse = newSortingClass;
										if (isReverse)
											newSortingClassWithoutReverse = newSortingClass.substring(0, newSortingClass.indexOf("-reverse"));
										if (! $(".sort." + mode + "-sort." + newSortingClass).hasClass("hidden")) {
											if (
												newSortingIndex > currentSortingIndex && (
													wasReverse && ! isReverse || ! wasReverse && isReverse
												)
											) {
												if (mode === "album") {
													env.beginAlbumSortingWithReverse = ! env.beginAlbumSortingWithReverse;
													beginWithReverse = env.beginAlbumSortingWithReverse;
												} else {
													env.beginMediaSortingWithReverse = ! env.beginMediaSortingWithReverse;
													beginWithReverse = env.beginMediaSortingWithReverse;
												}
												sortingMessageIds = generatesortingMessageIdsArray(mode, beginWithReverse);
												newSortingClass = sortingMessageIds[newSortingIndex];
											}

											break;
										} else {
											startIndex = newSortingIndex;
										}
									}
								} else {
									while (true) {
										newSortingIndex = (startIndex + 1) % sortingMessagesLength;
										newSortingClass = sortingMessageIds[newSortingIndex];
										isReverse = newSortingClass.includes("-reverse");
										newSortingClassWithoutReverse = newSortingClass;
										if (isReverse)
											newSortingClassWithoutReverse = newSortingClass.substring(0, newSortingClass.indexOf("-reverse"));
										if (! $(".sort." + mode + "-sort." + newSortingClassWithoutReverse).hasClass("hidden")) {
											if (
												newSortingIndex < currentSortingIndex && (
													wasReverse && ! isReverse || ! wasReverse && isReverse
												)
											) {
												if (mode === "album") {
													env.beginAlbumSortingWithReverse = ! env.beginAlbumSortingWithReverse;
													beginWithReverse = env.beginAlbumSortingWithReverse;
												} else {
													env.beginMediaSortingWithReverse = ! env.beginMediaSortingWithReverse;
													beginWithReverse = env.beginMediaSortingWithReverse;
												}
												sortingMessageIds = generatesortingMessageIdsArray(mode, beginWithReverse);
												newSortingClass = sortingMessageIds[newSortingIndex];
											}
											break;
										} else {
											startIndex = newSortingIndex;
										}
									}
								}

								let sortingModeMessageId = sortingMessageIds[newSortingIndex] + "-" + mode + "-sorting";
								$("#" + sortingModeMessageId).html(
									util._t("#" + mode + "-sorting") + " " + util._t("#" + sortingMessageIds[newSortingIndex] + "-sorting")
								).show().fadeOut(5000);

								let newSortingMessageHasReverse = sortingMessageIds[newSortingIndex].includes("-reverse");
								if (currentSortingMessageHasReverse !== newSortingMessageHasReverse) {
									$(".sort." + mode + "-sort.reverse")[0].click();
								}
								if (currentSortingClassWithoutReverse !== newSortingClassWithoutReverse) {
									$(".sort." + mode + "-sort." + newSortingClassWithoutReverse)[0].click();
								}

								return false;
							}
						}

						if (
							! env.slideshowId &&
							env.currentAlbum !== null && (
								env.currentAlbum.isAnyRootOrCollection() ||
								env.currentMedia !== null || env.currentAlbumIsAlbumWithOneMedia
							)
						) {
							// browsing mode switchers
							let nextBrowsingModeRequested = (e.key === ">");
							let prevBrowsingModeRequested = (e.key === "<");

							var filter = ".radio:not(.hidden):not(.selected)";
							if (nextBrowsingModeRequested) {
								let nextBrowsingModeObject = $(".browsing-mode-switcher.selected").nextAll(filter).first();
								if (nextBrowsingModeObject[0] === undefined)
									nextBrowsingModeObject = $(".browsing-mode-switcher.selected").siblings(filter).first();
								if (nextBrowsingModeObject[0] !== undefined) {
									$(".browsing-mode-switcher").removeClass("selected");
									nextBrowsingModeObject.addClass("selected");
									nextBrowsingModeObject[0].click();
									return false;
								}
							} else if (prevBrowsingModeRequested) {
								let prevBrowsingModeObject = $(".browsing-mode-switcher.selected").prevAll(filter).first();
								if (prevBrowsingModeObject[0] === undefined)
									prevBrowsingModeObject = $(".browsing-mode-switcher.selected").siblings(filter).last();
								if (prevBrowsingModeObject[0] !== undefined) {
									$(".browsing-mode-switcher").removeClass("selected");
									prevBrowsingModeObject.addClass("selected");
									prevBrowsingModeObject[0].click();
									return false;
								}
							}
						}
					}
				}
			}

			return true;
		}
	);

	$("#right-and-search-menu").show();

	if (! util.isPhp()) {
		$(".ssk-show-url").hide();
	}

	util.setLinksVisibility();

	let nextTitle  = util._t(".next-media-title");
	let prevTitle  = util._t(".prev-media-title");
	if (! env.isAnyMobile) {
		nextTitle  += " [" + util._s(".next-media-title-shortcut") + "]";
		prevTitle  += " [" + util._s(".prev-media-title-shortcut") + "]";
	}
	$("#next").attr("title", nextTitle).attr("alt", ">");
	$("#prev").attr("title", prevTitle).attr("alt", "<");
	$("#pinch-in").attr("title", util._t("#pinch-in-title")).attr("alt", "+");
	$("#pinch-out").attr("title", util._t("#pinch-out-title")).attr("alt", "-");
	if (env.isAnyMobile) {
		$("#pinch-in").css("width", "30px").css("height", "30px");
		$("#pinch-out").css("width", "30px").css("height", "30px");
	}

	// search
	$("#search-button").off("click").on("click", function() {
		let bySearchViewCacheBase;

		// build the search album part of the hash
		let wordsStringOriginal, wordsString;
		if (env.search.tagsOnly) {
			wordsStringOriginal = util.encodeNonLetters($("#search-field").val()).normalize().replace(/  /g, " ").trim();
		} else {
			wordsStringOriginal = $("#search-field").val().normalize().replace(/[^\p{L}\p{N}]/ug, " ").replace(/  /g, " ").trim();
		}
		if (! env.search.numbers) {
			wordsStringOriginal = wordsStringOriginal.replace(/[\p{N}]/ug, " ").replace(/  /g, " ").trim();
		}
		wordsString = encodeURIComponent(wordsStringOriginal.replace(/ /g, "_"));
		// TO DO: non-alphabetic words have to be filtered out
		if (wordsString) {
			env.search.wordsString = wordsString;
			env.search.words = wordsStringOriginal.split(" ");

			env.search.searchedCacheBase = env.hashComponents.albumCacheBase;
			if (env.search.currentAlbumOnly) {
				env.search.saved_cache_base_to_search_in = env.search.searchedCacheBase;
			} else {
				env.search.searchedCacheBase = env.options.folders_string;
			}

			if (env.search.currentAlbumOnly && util.isSearchCacheBase(env.hashComponents.albumCacheBase)) {
				// save the current hash in order to come back there when exiting from search
				let previousSearchComponents = PhotoFloat.decodeSearchCacheBase(env.hashComponents.albumCacheBase);
				if (util.itsTheSameSet(wordsStringOriginal.split(" "), previousSearchComponents.words)) {
					// it's a new search with the same words and different options: mantain the same folder to search
					env.search.searchedCacheBase = previousSearchComponents.searchedCacheBase;
				}
			}

			bySearchViewCacheBase = phFl.encodeSearchCacheBase(env.search);
			// the following function call is needed in order to calculare more properties
			env.search = phFl.decodeSearchCacheBase(bySearchViewCacheBase);

			if (util.isPopup()) {
				// refine the original popup content!

				env.mapAlbum.search = env.search;

				// remove the stopwords from the search terms
				let stopWordsPromise = phFl.getStopWords();
				stopWordsPromise.then(
					function () {
						env.mapAlbum.removeStopWordsFromSearchWords();
						let filteredMedia = env.mapAlbum.filterMediaInPopupAccordingToSearch();
						if (env.mapAlbum.search.currentAlbumOnly)
							$("#popup-images-wrapper .thumb-and-caption-container").removeClass("filtered-out");
						$("#popup-images-wrapper .thumb-and-caption-container").each(
							(index, singleMediaElement) => {
								let singleMediaObjectId = $(
									singleMediaElement
								).children(".thumb-container").children("a").attr("id").substr(
									env.mediaMapSelectBoxIdBeginning.length
								);
								if (
									filteredMedia.every(
										filteredSingleMedia => (
										 singleMediaObjectId !== [
												filteredSingleMedia.foldersCacheBase,
												filteredSingleMedia.cacheBase
											].join(env.options.cache_folder_separator)
										)
									)
								) {
									$(singleMediaElement).addClass("filtered-out");
								}
							}
						);
						// lazy loader has to be applied again, otherwise images are not loaded. Why?
						util.addMediaLazyLoader(false);

						// the count in the popup title has to be updated
						env.mapAlbum.buildPopupHeader();

						env.isFilteredPopup = false;
						// 			}
						// 		);
						// 	}
						// );
					},
					function() {
						console.trace();
					}
				);
			} else {
				let bySearchViewHash = env.hashBeginning + bySearchViewCacheBase;
				if (bySearchViewHash !== window.location.hash) {
					$("#loading").show();
					$("#loading").html(util._t("#loading-search", wordsStringOriginal));
					window.location.hash = bySearchViewHash;
				}
			}
		}

		menuF.highlightMenu();
		return false;
	});


	/* Entry point for most events */

	$("#search-field").keypress(
		function(ev) {
			// $("#search-menu ul").removeClass("hidden");
			if (ev.which === 13 || ev.keyCode === 13) {
				//Enter key pressed, trigger search button click event
				$("#search-button").trigger("click");
				menuF.focusSearchField();
				$("#search-field").blur();
				return false;
			}
		}
	);

	$("input[id='search-field']").on(
		"input",
		function() {
			if ($("input[id='search-field']").val() || ! util.isSearchHash())
				util.highlightSearchedWords(true);
			else if (util.isSearchHash())
				util.highlightSearchedWords();
		}
	);

	$("input[id='search-field']").on(
		"click",
		function() {
			return false;
		}
	);


	$("li#inside-words").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleInsideWordsSearch();
			return false;
		}
	);
	$("li#any-word").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleAnyWordSearch();
			return false;
		}
	);
	$("li#case-sensitive").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleCaseSensitiveSearch();
			return false;
		}
	);
	$("li#accent-sensitive").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleAccentSensitiveSearch();
			return false;
		}
	);
	$("li#search-numbers").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleNumbersSearch();
			return false;
		}
	);
	$("li#tags-only").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleTagsOnlySearch();
			return false;
		}
	);
	$("li#album-search").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleCurrentAbumSearch();
			return false;
		}
	);

	$(".download-album.command").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.command").hasClass("active")) {
				var thisAlbum;
				if (menuF.mustDownloadSelectionAlbum())
					thisAlbum = env.selectionAlbum;
				else if (util.isPopup())
					thisAlbum = env.mapAlbum;
				else
					thisAlbum = env.currentAlbum;

				thisAlbum.downloadAlbum();
				return false;
			}
		}
	);

	$(".download-option-flat").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleFlatDownloads(ev);
		}
	);

	$(".download-option-images").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleImagesDownload(ev);
		}
	);

	$(".download-option-images .download-option-change").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this).parent());
			tF.changeImagesDownloadSizeAndFormat(ev, ev.shiftKey);
		}
	);

	$(".download-option-images .download-option-change").off("contextmenu").on(
		"contextmenu",
		function(ev) {
			if (! ev.ctrlKey && ! ev.altKey) {
				ev.stopPropagation();
				menuF.addHighlightToItem($(this).parent());
				tF.changeImagesDownloadSizeAndFormat(ev, ! ev.shiftKey);
				return false;
			}
		}
	);

	$(".download-option-audios").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleAudiosDownload(ev);
		}
	);

	$(".download-option-audios .download-option-change").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this).parent());
			tF.changeAudiosDownloadSize(ev, ev.shiftKey);
		}
	);

	$(".download-option-audios .download-option-change").off("contextmenu").on(
		"contextmenu",
		function(ev) {
			if (! ev.ctrlKey && ! ev.altKey) {
				ev.stopPropagation();
				menuF.addHighlightToItem($(this).parent());
				tF.changeAudiosDownloadSize(ev, ! ev.shiftKey);
				return false;
			}
		}
	);

	$(".download-option-videos").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleVideosDownload(ev);
		}
	);

	$(".download-option-videos .download-option-change").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this).parent());
			tF.changeVideosDownloadSize(ev, ev.shiftKey);
		}
	);

	$(".download-option-videos .download-option-change").off("contextmenu").on(
		"contextmenu",
		function(ev) {
			if (! ev.ctrlKey && ! ev.altKey) {
				ev.stopPropagation();
				menuF.addHighlightToItem($(this).parent());
				tF.changeVideosDownloadSize(ev, ! ev.shiftKey);
				return false;
			}
		}
	);

	$(".download-option-subalbums").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleSubalbumsDownload(ev);
		}
	);

	$(".download-option-selection").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleSelectionDownload(ev);
		}
	);

	$(".download-single-media").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));

			let fileName = $(this).attr("file");
			if (fileName) {
				let link = document.createElement("a");
				link.setAttribute("href", fileName);
				link.setAttribute("download", fileName);
				link.click();
				link.remove();
				return false;
			} else {
				$(".download-album.command").trigger("click");
			}
		}
	);

	$(".download-album.selection.active").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			menuF.showHideDownloadSelectionInfo();
			return false;
		}
	);

	// binds the click events to the sort buttons

	$("#right-and-search-menu li").off("contextmenu").on(
		"contextmenu",
		function(ev) {
			ev.stopPropagation();
		}
	);

	$("#right-and-search-menu li.show-title").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleTitle(ev);
			util.setUpButtonVisibility();
		}
	);
	$("#right-and-search-menu li.show-descriptions").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleDescriptions(ev);
		}
	);
	$("#right-and-search-menu li.show-tags").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleTags(ev);
		}
	);
	$("#right-and-search-menu li.show-bottom-thumbnails").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleBottomThumbnails(ev);
		}
	);
	$("#right-and-search-menu li.slide").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleSlideMode(ev);
		}
	);
	$("#right-and-search-menu li.spaced").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleSpacing(ev);
		}
	);
	$("#right-and-search-menu li.show-album-names").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleAlbumNames(ev);
		}
	);
	$("#right-and-search-menu li.show-media-counts").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleMediaCount(ev);
		}
	);
	$("#right-and-search-menu li.show-media-names").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleMediaNames(ev);
		}
	);
	$("#right-and-search-menu li.square-album-thumbnails").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleAlbumsSquare(ev);
		}
	);
	$("#right-and-search-menu li.square-media-thumbnails").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleMediaSquare(ev);
		}
	);
	$("#restore").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.restoreSettings(ev);
		}
	);
	$("#show-big-albums").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			util.toggleBigAlbumsShow(ev);
		}
	);
	$("#save-data").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleSaveData(ev);
			env.isASaveDataChange = true;
			// recreate the page, so that the ui elements related to positions are shown or hidden
			$(window).hashchange();
		}
	);
	$("#search-icon").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			menuF.toggleSearchMenu(ev);
		}
	);

	$("#slideshow-icon").attr("title", util._t(".slideshow-icon-title"));

	$("#up-button").on(
		"click",
		function() {
			if (
				env.currentAlbum.cacheBase === env.options.folders_string && util.isSearchHash() ||
				env.currentAlbum.cacheBase !== env.options.folders_string ||
				env.currentMedia &&
				! env.currentAlbumIsAlbumWithOneMedia
			) {
				env.fromEscKey = true;
				$("#loading").show();
				pS.swipeDown(util.upHash());
				return false;
			}

			if ($("#no-results").is(":visible")) {
				window.location.href = util.upHash();
				return false;
			}
		}
	);

	if (env.isAnyMobile) {
		$("#right-and-search-menu .info-icon").hide();
	} else {
		$(".info-icon").off("mouseenter").on(
			"mouseenter",
			function() {
				util.showContextualHelp();
			}
		).off("mouseleave").on(
			"mouseleave",
			function() {
				util.hideContextualHelp();
			}
		);
	}

	$("#menu-icon").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			menuF.toggleRightMenu(ev);
		}
	);

	$(
		function() {
			$("#description-wrapper").off("mouseenter mouseleave").on(
				"mouseenter",
				util.removeDescriptionOpacity
			).on(
				"mouseleave",
				util.addDescriptionOpacity
			);
		}
	);

	pS.addAlbumGesturesDetection();

	$("#auth-form").off("submit").on(
		"submit",
		function() {
			// This function checks the password looking for a file with the encrypted password name in the passwords subdir
			// the code in the found password file is inserted into env.guessedPasswordsMd5, and at the hash change the content unveiled by that password will be shown

			var passwordObject = $("#password");
			var encryptedPassword = md5(passwordObject.val());
			passwordObject.val("");

			var promise = phFl.getJsonFile(util.pathJoin([env.options.passwords_subdir, encryptedPassword]));
			promise.then(
				function(jsonCode) {
					passwordObject.css("background-color", "rgb(200, 200, 200)");
					var passwordCode = jsonCode.passwordCode;

					if (env.guessedPasswordCodes.length && env.guessedPasswordCodes.includes(passwordCode)) {
						passwordObject.css("background-color", "red");
						passwordObject.on(
							"input",
							function() {
								passwordObject.css("background-color", "");
								passwordObject.off("input");
							}
						);
					} else {
						env.guessedPasswordCodes.push(passwordCode);
						env.guessedPasswordsMd5.push(encryptedPassword);

						$("#loading").show();

						if (util.isMap() || util.isPopup()) {
							// the map must be generated again including the points that only carry protected content
							env.mapRefreshType = "refresh";
							if (env.previousAlbum === null)
								env.previousAlbum = env.mapAlbum;

							if (util.isPopup()) {
								env.popupRefreshType = "mapAlbum";
								if (util.isShiftOrControl())
									$(".shift-or-control .leaflet-popup-close-button")[0].click();
								$(".media-popup .leaflet-popup-close-button")[0].click();
							} else {
								env.popupRefreshType = null;
							}
							// close the map
							$(".modal-close")[0].click();
						}

						env.isFromAuthForm = true;
						menuF.updateMenu();
						$(window).hashchange();
					}
				},
				function() {
					passwordObject.css("background-color", "red");
					passwordObject.on(
						"input",
						function() {
							passwordObject.css("background-color", "");
							passwordObject.off("input");
						}
					);
				}
			);

			return false;
		}
	);

	$(window).on(
		"click",
		function() {
			if ($("#right-and-search-menu").hasClass("expanded") && ! env.isMenuAction) {
				menuF.closeMenu();
				return false;
			} else {
				env.isMenuAction = false;
				return true;
			}
		}
	);


});
//# sourceURL=050-display-bis.js
