(function() {

	var util = new Utilities();

	Subalbums.prototype.intersectionForSearches = function(other) {
		util.mediaOrSubalbumsIntersectionForSearches(this, other);
	};

	Subalbums.prototype.unionForSearches = function(other) {
		util.mediaOrSubalbumsUnionForSearches(this, other);
	};

	Subalbums.prototype.filterSubalbumsAgainstSearchedAlbum = function(searchObject) {
		var removedSubalbums = [];

		if (! searchObject.currentAlbumOnly)
			return removedSubalbums;

		for (let indexSubalbum = this.length - 1; indexSubalbum >= 0 ; indexSubalbum --) {
			let ithSubalbum = this[indexSubalbum];
			let searchedAlbum = env.cache.getAlbum(searchObject.searchedCacheBase);
			if (
				searchedAlbum.isFolder() && (
					ithSubalbum.cacheBase.indexOf(searchObject.searchedCacheBase) !== 0
					// ithSubalbum.cacheBase.split(
					// 	env.options.cache_folder_separator
					// ).length - searchObject.searchedCacheBase.split(
					// 	env.options.cache_folder_separator
					// ).length === 1
				) || (
					searchedAlbum.isSearch() || searchedAlbum.isSelection()
				) && searchedAlbum.subalbums.every(
					searchedSubalbum => searchedSubalbum.cacheBase !== ithSubalbum.cacheBase
				) || searchedAlbum.isMap()
			) {
				this.splice(indexSubalbum, 1);
				removedSubalbums.push(ithSubalbum);
			}
		}

		return removedSubalbums;
	};

	Subalbums.prototype.filterSubalbumsAgainstOneWord = function(searchObject, normalizedWord) {
		var removedSubalbums = [];
		var normalizedWords, normalizedTags;
		for (let indexSubalbums = this.length - 1; indexSubalbums >= 0; indexSubalbums --) {
			let ithSubalbum = this[indexSubalbums];

			normalizedWords = util.normalizeAccordingToOptions(ithSubalbum.words);
			if (ithSubalbum.hasOwnProperty("tags") && searchObject.tagsOnly)
				normalizedTags = util.normalizeAccordingToOptions(ithSubalbum.tags);

			if (! searchObject.insideWords) {
				// whole word
				if (
					! searchObject.tagsOnly &&
					normalizedWords.includes(normalizedWord) ||
					searchObject.tagsOnly &&
					ithSubalbum.hasOwnProperty("tags") &&
					normalizedTags.includes(normalizedWord)
				) {
					// ok, do not remove the subalbum
				} else {
					// remove the subalbum
					this.splice(indexSubalbums, 1);
					removedSubalbums.push(ithSubalbum);
				}
			} else {
				// inside words
				if (
					! searchObject.tagsOnly &&
					normalizedWords.some(element => element.includes(normalizedWord)) ||
					searchObject.tagsOnly &&
					ithSubalbum.hasOwnProperty("tags") &&
					normalizedTags.some(element => element.includes(normalizedWord))
				) {
					// ok, do not remove the subalbum
				} else {
					// remove the subalbum
					this.splice(indexSubalbums, 1);
					removedSubalbums.push(ithSubalbum);
				}
			}
		}

		return removedSubalbums;
	};

	Subalbums.prototype.filterSubalbumsAgainstEveryWord = function(searchObject, lastIndex) {
		var removedSubalbums = [];
		var normalizedWords, normalizedTags;
		for (let indexSubalbums = this.length - 1; indexSubalbums >= 0 ; indexSubalbums --) {
			let ithSubalbum = this[indexSubalbums];
			if (! searchObject.insideWords) {
				// whole word
				normalizedWords = util.normalizeAccordingToOptions(ithSubalbum.words);
				if (ithSubalbum.hasOwnProperty("tags") && searchObject.tagsOnly)
					normalizedTags = util.normalizeAccordingToOptions(ithSubalbum.tags);

				if (
					! searchObject.tagsOnly &&
					searchObject.wordsFromUserNormalizedAccordingToOptions.every(
						(searchWord, index) => index <= lastIndex || normalizedWords.indexOf(searchWord) > -1
					) ||
					searchObject.tagsOnly &&
					ithSubalbum.hasOwnProperty("tags") &&
					searchObject.wordsFromUserNormalizedAccordingToOptions.every(
						(searchWord, index) => index <= lastIndex || normalizedTags.indexOf(searchWord) > -1
					)
				) {
					// ok, do not remove the subalbum
				} else {
					// remove the subalbum
					this.splice(indexSubalbums, 1);
					removedSubalbums.push(ithSubalbum);
				}
			} else {
				// inside words
				for (let indexWordsLeft = lastIndex + 1; indexWordsLeft < searchObject.wordsFromUserNormalizedAccordingToOptions.length; indexWordsLeft ++) {
					normalizedWords = util.normalizeAccordingToOptions(ithSubalbum.words);
					if (ithSubalbum.hasOwnProperty("tags") && searchObject.tagsOnly)
						normalizedTags = util.normalizeAccordingToOptions(ithSubalbum.tags);

					if (
						! searchObject.tagsOnly &&
						normalizedWords.some(normalizedWord => normalizedWord.includes(searchObject.wordsFromUserNormalizedAccordingToOptions[indexWordsLeft])) ||
						searchObject.tagsOnly &&
						ithSubalbum.hasOwnProperty("tags") &&
						normalizedTags.some(normalizedWord => normalizedWord.includes(searchObject.wordsFromUserNormalizedAccordingToOptions[indexWordsLeft]))
				) {
					// ok, do not remove the subalbum
				} else {
						// remove the subalbum
						this.splice(indexSubalbums, 1);
						removedSubalbums.push(ithSubalbum);
						break;
					}
				}
			}
		}

		return removedSubalbums;
	};

	SingleMediaInPositions.prototype.isEqual = function(otherMedia) {
		return this.foldersCacheBase === otherMedia.foldersCacheBase && this.cacheBase === otherMedia.cacheBase;
	};

	PositionAndMedia.prototype.matchPosition = function(positionAndMedia2) {
		return (JSON.stringify([this.lat, this.lng]) === JSON.stringify([positionAndMedia2.lat, positionAndMedia2.lng]));
	};

	NumsProtected.prototype.sum = function(numsProtectedSize2) {
		var keys = util.arrayUnion(Object.keys(this), Object.keys(numsProtectedSize2));
		for (var i = 0; i < keys.length; i++) {
			if (this[keys[i]] !== undefined && numsProtectedSize2[keys[i]] !== undefined) {
				this[keys[i]] = new ImagesAudiosVideos(
					{
						images: this[keys[i]].images + numsProtectedSize2[keys[i]].images,
						audios: this[keys[i]].audios + numsProtectedSize2[keys[i]].audios,
						videos: this[keys[i]].videos + numsProtectedSize2[keys[i]].videos
					}
				);
			} else if (this[keys[i]] === undefined) {
				this[keys[i]] = numsProtectedSize2[keys[i]];
			}
		}
	};

	NumsProtected.prototype.subtract = function(numsProtectedSize2) {
		var keys = util.arrayUnion(Object.keys(this), Object.keys(numsProtectedSize2));
		for (var i = 0; i < keys.length; i++) {
			if (this[keys[i]] !== undefined && numsProtectedSize2[keys[i]] !== undefined) {
				this[keys[i]] = new ImagesAudiosVideos(
					{
						images: this[keys[i]].images - numsProtectedSize2[keys[i]].images,
						audios: this[keys[i]].audios - numsProtectedSize2[keys[i]].audios,
						videos: this[keys[i]].videos - numsProtectedSize2[keys[i]].videos
					}
				);
			} else if (this[keys[i]] === undefined) {
				// execution shouldn't arrive here
				console.trace();
			}
		}
	};

	NumsProtected.prototype.sumUpNumsProtectedMedia = function() {
		var result = new ImagesAudiosVideos(), codesComplexcombination;
		for (codesComplexcombination in this) {
			if (this.hasOwnProperty(codesComplexcombination) && codesComplexcombination !== ",") {
				result.sum(this[codesComplexcombination]);
			}
		}
		return result.images + result.audios + result.videos;
	};

	ImagesAudiosVideos.prototype.imagesAudiosVideosTotal = function() {
		return this.images + this.audios + this.videos;
	};

	ImagesAudiosVideos.prototype.filterAccordingToTypeOfDownloadOptions = function() {
		// this function filters according to options:
		// - download_images
		// - download_audios
		// - download_videos
		return new ImagesAudiosVideos(
			{
				images: (env.options.download_images ? this.images : 0),
				audios: (env.options.download_audios ? this.audios : 0),
				videos: (env.options.download_videos ? this.videos : 0)
			}
		);
	};

	ImagesAudiosVideos.prototype.sum = function(other) {
		this.images = this.images + other.images;
		this.audios = this.audios + other.audios;
		this.videos = this.videos + other.videos;
	};

	ImagesAudiosVideos.prototype.subtract = function(other) {
		this.images = this.images - other.images;
		this.audios = this.audios - other.audios;
		this.videos = this.videos - other.videos;
	};

	Sizes.prototype.sum = function(sizes2) {
		var sizes1 = this;
		Object.keys(sizes1).forEach(
			format => {
				if (format === "original") {
					sizes1.original.sum(sizes2.original);
				} else {
					Object.keys(sizes1[format]).forEach(
						size => {
							size = Number(size);
							if (sizes2[format].hasOwnProperty(size))
								sizes1[format][size].sum(sizes2[format][size]);
						}
					);
				}
			}
		);
	};

	Sizes.prototype.subtract = function(sizes2) {
		var sizes1 = this;
		Object.keys(sizes1).forEach(
			format => {
				if (format === "original") {
					sizes1.original.subtract(sizes2.original);
				} else {
					Object.keys(sizes1[format]).forEach(
						size => {
							size = Number(size);
							if (sizes2[format].hasOwnProperty(size))
								sizes1[format][size].subtract(sizes2[format][size]);
						}
					);
				}
			}
		);
	};

	Cache.prototype.putAlbum = function(album) {
		var done = false, level, cacheLevelsLength = this.js_cache_levels.length, firstCacheBase;
		// check if the album is already in cache (it could be there with another media number)
		// if it is there, remove it
		if (this.albums.index.hasOwnProperty(album.cacheBase)) {
			level = this.albums.index[album.cacheBase];
			delete this.albums[level][album.cacheBase];
			delete this.albums[level].queue[album.cacheBase];
			delete this.albums.index[album.cacheBase];
		}

		if (album.hasOwnProperty("media")) {
			for (level = 0; level < cacheLevelsLength; level ++) {
				if (album.numsMedia.imagesAudiosVideosTotal() >= this.js_cache_levels[level].mediaThreshold) {
					if (! this.albums.hasOwnProperty(level)) {
						this.albums[level] = [];
						this.albums[level].queue = [];
					}
					if (this.albums[level].queue.length >= this.js_cache_levels[level].max) {
						// remove the first element
						firstCacheBase = this.albums[level].queue[0];
						this.albums[level].queue.shift();
						delete this.albums.index[firstCacheBase];
						delete this.albums[level][firstCacheBase];
					}
					this.albums.index[album.cacheBase] = level;
					this.albums[level].queue.push(album.cacheBase);
					this.albums[level][album.cacheBase] = album;
					done = true;
					break;
				}
			}
		}
		if (! done) {
			if (! this.albums.hasOwnProperty(cacheLevelsLength)) {
				this.albums[cacheLevelsLength] = [];
				this.albums[cacheLevelsLength].queue = [];
			}
			this.albums.index[album.cacheBase] = cacheLevelsLength;
			this.albums[cacheLevelsLength].queue.push(album.cacheBase);
			this.albums[cacheLevelsLength][album.cacheBase] = album;
		}
	};

	Cache.prototype.getAlbum = function(cacheBase) {
		if (this.albums.index.hasOwnProperty(cacheBase)) {
			var cacheLevel = this.albums.index[cacheBase];
			var cachedAlbum = this.albums[cacheLevel][cacheBase];
			return cachedAlbum;
		} else
			return false;
	};

	Cache.prototype.getSingleMedia = function(singleMedia) {
		var foldersCacheBase = singleMedia.foldersCacheBase;
		var cacheBase = singleMedia.cacheBase;

		if (! this.media.hasOwnProperty(foldersCacheBase)) {
			this.media[foldersCacheBase] = {};
		}
		if (! this.media[foldersCacheBase].hasOwnProperty(cacheBase)) {
			this.media[foldersCacheBase][cacheBase] = singleMedia;
			return false;
		} else {
			return this.media[foldersCacheBase][cacheBase];
		}
	};

	Cache.prototype.getMedia = function(foldersCacheBase, cacheBase) {
		return this.media[foldersCacheBase][cacheBase];
	};

	if (! Array.prototype.findLastIndex) {
		Array.prototype.findLastIndex = function (predicate) {
			const reversedArr = [...this].reverse();
			const foundIndex = reversedArr.findIndex(predicate);
			return foundIndex === -1 ? -1 : this.length - foundIndex - 1;
		};
	}

	if (!String.prototype.padStart) {
		String.prototype.padStart = function (targetLength, padString = ' ') {
			let str = String(this);
			padString = String(padString);

			if (str.length >= targetLength) {
				return str;
			}

			const padLength = targetLength - str.length;
			const repeatedPad = padString.repeat(Math.ceil(padLength / padString.length));
			return repeatedPad.slice(0, padLength) + str;
		};
	}

	if (!Array.prototype.includes) {
		Array.prototype.includes = function(searchElement, fromIndex) {
			if (this == null) {
				throw new TypeError('this is null or not defined');
			}

			const O = Object(this);
			const len = O.length >>> 0;
			if (len === 0) {
				return false;
			}

			let n = Math.max(0, fromIndex | 0);

			while (n < len) {
				if (n in O && O[n] === searchElement) {
					return true;
				}
				n ++;
			}

			return false;
		};
	}

	if (! Array.prototype.at) {
		Array.prototype.at = function(index) {
			// Normalize negative index
			index = Math.trunc(index);
			if (index < 0) {
				index = this.length + index;
			}
			// Handle out-of-bounds index
			if (index < 0 || index >= this.length) {
				return undefined;
			}
			return this[index];
		};
	}

	if (! Object.values) {
		Object.values = function(obj) {
			return Object.keys(obj).map(key => obj[key]);
		};
	}

	if (!Object.entries) {
		Object.entries = function(obj) {
			return Object.keys(obj).map(key => [key, obj[key]]);
		};
	}
}());
