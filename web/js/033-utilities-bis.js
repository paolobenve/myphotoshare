(function() {
	Utilities.prototype.openInNewTab = function(hash) {
		// albums is a list of objects {albumName: album}
		let albums = [];
		if (Utilities.somethingIsInMapAlbum()) {
			albums.push(env.mapAlbum);
		}
		if (Utilities.absolutelySomethingIsSelected()) {
			albums.push(env.selectionAlbum);
		}
		if (Utilities.absolutelySomethingIsSearched()) {
			albums.push(env.searchAlbum);
		}
		Utilities.openNewTabWithPost(hash, albums);
		return false;
	};

	Utilities.openNewTabWithPost = function(hash, albums) {
		let typeOfPackedAlbum, stringifiedPackedAlbum, albumName;

		let newFormObject = $(
			"<form>",
			{
				action: hash,
				target: "_blank",
				method: "post"
			}
		);

		for (let iAlbum = 0; iAlbum < albums.length; iAlbum ++) {
			let ithAlbum = albums[iAlbum];
			let packedAlbum = lzwCompress.pack(ithAlbum.toJson());
			// var packedAlbum = lzwCompress.pack(JSON.decycle(albums[albumName]));
			if (Array.isArray(packedAlbum)) {
				stringifiedPackedAlbum = packedAlbum.join();
				typeOfPackedAlbum = "array";
			} else {
				typeOfPackedAlbum = "string";
				stringifiedPackedAlbum = packedAlbum;
			}
			if (ithAlbum.isSelection())
				albumName = "selectionAlbum";
			else if (ithAlbum.isSearch())
				albumName = "searchAlbum";
			else
				albumName = "mapAlbum";

			let iString = iAlbum.toString();
			newFormObject.append(
				$(
					"<input>",
					{
						name: "albumName_" + iString,
						value: albumName,
						type: "hidden"
					}
				)
			).append(
				$(
					"<input>",
					{
						name: "stringifiedPackedAlbum_" + iString,
						value: stringifiedPackedAlbum,
						type: "hidden"
					}
				)
			).append(
				$(
					"<input>",
					{
						name: "typeOfPackedAlbum_" + iString,
						value: typeOfPackedAlbum,
						type: "hidden"
					}
				)
			);
		}

		if (Object.keys(env.map).length) {
			let envMapForForm = JSON.parse(JSON.stringify(env.map));
			delete envMapForForm.clickHistory;
			newFormObject.append(
				$(
					"<input>",
					{
						name: "mapClick",
						value: JSON.stringify(envMapForForm),
						type: "hidden"
					}
				)
			);
		}

		if (env.map.clickHistory)
			newFormObject.append(
				$(
					"<input>",
					{
						name: "mapClickHistory",
						value: MapFunctions.encodeMapClickHistory(env.map.clickHistory),
						type: "hidden"
					}
				)
			);

		// if (env.selectionClickHistory)
		// 	newFormObject.append(
		// 		$(
		// 			"<input>",
		// 			{
		// 				name: "selectionClickHistory",
		// 				value: Utilities.encodeSelectionClickHistory(env.selectionClickHistory),
		// 				type: "hidden"
		// 			}
		// 		)
		// 	);

		let currentAlbumIs = "";
		if (env.hasOwnProperty("mapAlbum") && env.currentAlbum === env.mapAlbum)
			currentAlbumIs = "mapAlbum";
		else if (env.hasOwnProperty("selectionAlbum") && env.currentAlbum === env.selectionAlbum)
			currentAlbumIs = "selectionAlbum";
		else if (env.hasOwnProperty("searchAlbum") && env.currentAlbum === env.searchAlbum)
			currentAlbumIs = "searchAlbum";
		if (currentAlbumIs)
			newFormObject.append(
				$(
					"<input>",
					{
						name: "currentAlbumIs",
						value: currentAlbumIs,
						type: "hidden"
					}
				)
			);

		if (env.guessedPasswordsMd5.length) {
			newFormObject.append(
				$(
					"<input>",
					{
						name: "guessedPasswordsMd5",
						value: env.guessedPasswordsMd5.join('-'),
						type: "hidden"
					}
				)
			).append(
				$(
					"<input>",
					{
						name: "guessedPasswordCodes",
						value: env.guessedPasswordCodes.join('-'),
						type: "hidden"
					}
				)
			);
		}
		newFormObject.hide().appendTo("body").trigger("submit").remove();
	};

	Utilities.encodeMapCacheBase = function(mapObject) {
		return [
			env.options.by_map_string,
			[
				mapObject.clickAlbumCacheBase ? Utilities.encodeAllNonAlfaNum(mapObject.clickAlbumCacheBase) : "",
				mapObject.whatToGenerateTheMapFor ? Utilities.encodeAllNonAlfaNum(mapObject.whatToGenerateTheMapFor) : "",
				MapFunctions.encodeMapClickHistory(mapObject.clickHistory)
			].join(env.options.search_options_separator)
		].join(env.options.cache_folder_separator);
	};

	Utilities.prototype.initializeMapAlbum = function() {
		var rootMapAlbum = env.cache.getAlbum(env.options.by_map_string);

		// initializes the map album
		let mapObject = {
			clickAlbumCacheBase: env.map.clickAlbumCacheBase,
			whatToGenerateTheMapFor: env.map.whatToGenerateTheMapFor,
			clickHistory: []
		};
		var newMapAlbum = new MapAlbum(Utilities.encodeMapCacheBase(mapObject));

		newMapAlbum.path = newMapAlbum.cacheBase.replace(new RegExp(env.options.cache_folder_separator, 'g'), "/");
		newMapAlbum.name = "(" + Utilities._t("#by-map") + ")";

		newMapAlbum.map = mapObject;

		newMapAlbum.ancestorsCacheBase = rootMapAlbum.ancestorsCacheBase.slice();
		newMapAlbum.ancestorsCacheBase.push(newMapAlbum.cacheBase);
		newMapAlbum.ancestorsNames = [env.options.by_map_string, newMapAlbum.cacheBase];

		newMapAlbum.includedFilesByCodesSimpleCombination = new IncludedFiles({",": {}});

		env.cache.putAlbum(newMapAlbum);

		rootMapAlbum.numsMediaInSubTree.sum(newMapAlbum.numsMediaInSubTree);
		rootMapAlbum.subalbums.push(newMapAlbum);
		if (env.options.expose_image_positions) {
			rootMapAlbum.positionsAndMediaInTree.mergePositionsAndMedia(newMapAlbum.positionsAndMediaInTree);
			rootMapAlbum.numPositionsInTree = rootMapAlbum.positionsAndMediaInTree.length;
		}

		return newMapAlbum;
	};

	Utilities.prototype.toggleMetadataFromMouse = function(ev) {
		if (ev.button === 0 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			ev.stopPropagation();
			Utilities.toggleMetadata();
			return false;
		}
	};

	Utilities.toggleMetadata = function() {
		if ($(".media-box#center .metadata").css("display") === "none") {
			$(".media-box .links").css("display", "inline").stop().fadeTo("slow", 0.5);
			$(".media-box .metadata-show").hide();
			$(".media-box .metadata-hide").show();
			$(".media-box .metadata")
				.stop()
				.css("height", 0)
				.css("padding-top", 0)
				.css("padding-bottom", 0)
				.show()
				.stop()
				.animate(
					{
						height: $("#center .metadata > table").height(),
						paddingTop: 3,
						paddingBottom: 3
					},
					"slow"
				);
		} else {
			if (env.isAnyMobile)
				$(".media-box .links").stop().fadeTo("slow", 0.25);
			else
				$(".media-box .links").stop().fadeOut("slow");
			$(".media-box .metadata-show").show();
			$(".media-box .metadata-hide").hide();
			$(".media-box .metadata")
				.stop()
				.animate(
					{
						height: 0,
						paddingTop: 0,
						paddingBottom: 0
					},
					"slow",
					function() {
						$(this).hide();
					}
				);
		}
	};

	Utilities.toggleFullscreen = function(e, enterSlideshowFromFullscreen = false) {
		function afterToggling(isFullscreen) {
			if (isFullscreen || isFullscreen === undefined && env.fullScreenStatus) {
				// we have entered fullscreen
				$(".enter-fullscreen").hide();
				$(".exit-fullscreen").show();
				env.fullScreenStatus = true;

				if (env.slideshowId && ! env.slideshowCurrentMediaWasNullBefore) {
					env.currentMedia.addRotation($("#media-center"));
					if (env.prevMedia)
						env.prevMedia.addRotation($("#media-left"));
					if (env.nextMedia)
						env.nextMedia.addRotation($("#media-right"));
				}
				Utilities.setFinalOptions(env.currentMedia, false); // OK: full screen toggle

				// if (env.currentMedia && env.currentMedia.isAudio() && $("audio#media-center")[0].paused)
				// 	$("audio#media-center")[0].play();
				// if (env.currentMedia && env.currentMedia.isVideo() && $("video#media-center")[0].paused)
				// 	$("video#media-center")[0].play();
			} else {
				// we have exited fullscreen

				// the image is pinched in, go back to full screen and pinch the image out
				if (
					env.currentMedia !== null && (
						env.currentZoom > env.initialZoom || $(".media-box#center .title").hasClass("hidden-by-pinch")
					)
				) {
					PinchSwipe.pinchOut(null, null);
					Utilities.toggleFullscreen(e);
					return false;
				}

				$(".enter-fullscreen").show();
				$(".exit-fullscreen").hide();
				env.fullScreenStatus = false;

				if (env.slideshowId) {
					$("#slideshow-buttons").stop().fadeTo(1000, 0);
					$("#slideshow-buttons").off("mouseenter");
					$("#slideshow-buttons").off("mouseleave");
					clearInterval(env.slideshowId);
					env.slideshowId = 0;
					$("#fullscreen-wrapper").removeClass("paused");

					if (env.currentMedia) {
						env.currentMedia.removeRotation($("#media-center"));
						if (env.prevMedia)
							env.prevMedia.removeRotation($("#media-left"));
						if (env.nextMedia)
							env.nextMedia.removeRotation($("#media-right"));
					}
					Utilities.setFinalOptions(env.currentMedia, false); // OK: full screen toggle

					if (env.slideshowCurrentMediaWasNullBefore) {
						env.slideshowCurrentMediaWasNullBefore = false;
						PinchSwipe.swipeDown(Utilities.upHash());
					}
				}
				if (env.currentMedia !== null && env.currentMedia.isAudio() && ! $("audio#media-center")[0].paused)
					$("audio#media-center")[0].pause();
				if (env.currentMedia !== null && env.currentMedia.isVideo() && ! $("video#media-center")[0].paused)
					$("video#media-center")[0].pause();
				// $("#up-button").show();
			}
			$("#loading").hide();
			Utilities.setUpButtonVisibility();

			if (env.currentMedia !== null) {
				env.isFullScreenToggling = true;
				Utilities.resizeSingleMediaWithPrevAndNext(env.currentMedia, env.currentAlbum);
			}
		}
		// end of auxiliary function

		if (Modernizr.fullscreen && ! enterSlideshowFromFullscreen) {
			e.preventDefault();

			$("#fullscreen-wrapper").fullScreen(
				{
					callback: function(isFullscreen) {
						afterToggling(isFullscreen);
					}
				}
			);
		} else {
			enterSlideshowFromFullscreen = false;
			afterToggling();
		}
	};


	Utilities.prototype.toggleFullscreenFromMouse = function(ev) {
		if (ev.button === 0 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			Utilities.toggleFullscreen(ev);
			return false;
		}
	};

	Utilities.resizeSingleMediaWithPrevAndNext = function(self, album) {
		let previousWindowWidth = env.windowWidth;
		let previousWindowHeight = env.windowHeight;
		env.windowWidth = $(window).innerWidth();
		env.windowHeight = $(window).innerHeight();
		if (env.windowWidth === previousWindowWidth && env.windowHeight === previousWindowHeight && ! env.isFullScreenToggling)
			// avoid considering a resize when the mobile browser shows/hides the location bar
			return;

		env.fullSizeMediaBoxContainerContent =
			$(env.fullSizeMediaBoxContainerContent).css("width", env.windowWidth)[0].outerHTML;

		env.isFullScreenToggling = false;

		$("#loading").show();
		Utilities.setTitleOptions();

		if (album.numVisibleMedia() && ! $("#thumbs").children().length)
			album.showMedia();

		let isMap = Utilities.isMap();
		let isPopup = Utilities.isPopup();
		let event = {data: {}};
		event.data.resize = true;
		event.data.id = "center";

		// prev and next tree in the DOM must be given the correct sizes
		$(".media-box#left, .media-box#right").css("width", env.windowWidth);
		$(".media-box#left, .media-box#right").css("height", env.windowHeight);

		var scalePromise = self.scale(event);
		scalePromise.then(
			function() {
				Utilities.setFinalOptions(self, true); // OK: scale promise
			}
		);

		if (album.visibleNumsMedia().imagesAudiosVideosTotal() > 1) {
			if (env.prevMedia) {
				event.data.id = "left";
				env.prevMedia.scale(event);
			}

			if (env.nextMedia) {
				event.data.id = "right";
				env.nextMedia.scale(event);
			}
		}

		if (isMap || isPopup) {
			MapFunctions.resizeMap();

			if (isPopup) {
				if (Utilities.isShiftOrControl())
					$(".shift-or-control .leaflet-popup-close-button")[0].click();
				env.mapAlbum.prepareAndDoPopupUpdate();
				Utilities.adaptMediaCaptionHeight(true);
			}
		}

		// MenuFunctions.updateMenu();
	};

	Utilities.prototype.nextObjectForHighlighting = function(object) {
		var isPopup = Utilities.isPopup();
		var nextObject;

		if (isPopup) {
			nextObject = object.next();
			if (! nextObject.length)
				 nextObject = object.parent().children().first();
		} else {
			let numVisibleSubalbums = env.currentAlbum.visibleSubalbums().length;
			let numVisibleMedia = env.currentAlbum.numVisibleMedia();
			let filter = "*";
			let mediaFilter = "*";
			let subalbumsFilter = "*";
			if (Utilities.geotaggedContentIsHidden()) {
				mediaFilter = ":not(.gps)";
				subalbumsFilter = ":not(.all-gps)";
				filter = mediaFilter;
				if (Utilities.objectIsASubalbum(object))
					filter = subalbumsFilter;
			}
			let nextObjectParent = object.parent().nextAll(filter).first();
			if (nextObjectParent.length) {
				nextObject = nextObjectParent.children();
			} else {
				if (
					Utilities.objectIsASingleMedia(object) && numVisibleMedia === 1 && ! numVisibleSubalbums ||
					Utilities.objectIsASubalbum(object) && numVisibleSubalbums === 1 && ! numVisibleMedia
				)
					return object;
				else if (Utilities.objectIsASingleMedia(object) && numVisibleSubalbums)
					nextObject = $(".album-button-and-caption").parent().filter(subalbumsFilter).first().children();
				else if (Utilities.objectIsASingleMedia(object) && ! numVisibleSubalbums)
					nextObject = $(".thumb-and-caption-container").parent().prevAll(mediaFilter).last().children();
				else if (Utilities.objectIsASubalbum(object) && numVisibleMedia)
					nextObject = $(".thumb-and-caption-container").parent().filter(mediaFilter).first().children();
				else if (Utilities.objectIsASubalbum(object) && ! numVisibleMedia)
					nextObject = $(".album-button-and-caption").parent().prevAll(subalbumsFilter).last().children();
			}
		}

		return nextObject;
	};

	Utilities.prototype.prevObjectForHighlighting = function(object) {
		var isPopup = Utilities.isPopup();
		var prevObject;

		if (isPopup) {
			prevObject = object.prev();
			if (! prevObject.length)
				 prevObject = object.parent().children().last();
		} else {
			let numVisibleSubalbums = env.currentAlbum.visibleSubalbums().length;
			let numVisibleMedia = env.currentAlbum.numVisibleMedia();
			let filter = "*";
			let mediaFilter = "*";
			let subalbumsFilter = "*";
			if (Utilities.geotaggedContentIsHidden()) {
				mediaFilter = ":not(.gps)";
				subalbumsFilter = ":not(.all-gps)";
				filter = mediaFilter;
				if (Utilities.objectIsASubalbum(object))
					filter = subalbumsFilter;
			}
			let prevObjectParent = object.parent().prevAll(filter).first();
			if (prevObjectParent.length) {
				prevObject = prevObjectParent.children();
			} else {
				if (
					Utilities.objectIsASingleMedia(object) && numVisibleMedia === 1 && ! numVisibleSubalbums ||
					Utilities.objectIsASubalbum(object) && numVisibleSubalbums === 1 && ! numVisibleMedia
				)
					return object;
				else if (Utilities.objectIsASingleMedia(object) && numVisibleSubalbums)
					prevObject = $(".album-button-and-caption").parent().filter(subalbumsFilter).last().children();
				else if (Utilities.objectIsASingleMedia(object) && ! numVisibleSubalbums)
					prevObject = $(".thumb-and-caption-container").parent().nextAll(mediaFilter).last().children();
				else if (Utilities.objectIsASubalbum(object) && numVisibleMedia)
					prevObject = $(".thumb-and-caption-container").parent().filter(mediaFilter).last().children();
				else if (Utilities.objectIsASubalbum(object) && ! numVisibleMedia)
					prevObject = $(".album-button-and-caption").parent().nextAll(subalbumsFilter).last().children();
			}
		}

		return prevObject;
	};

	Utilities.prototype.toggleBigAlbumsShow = function(ev) {
		if ((ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			if ($("#message-too-many-media").is(":visible")) {
				$("#message-too-many-media").css("display", "");
			}
			$("#loading").show();
			env.options.show_big_virtual_folders = ! env.options.show_big_virtual_folders;
			if (env.options.show_big_virtual_folders)
				$("#show-hide-them:hover").css("color", "").css("cursor", "");
			else
				$("#show-hide-them:hover").css("color", "inherit").css("cursor", "auto");
			MenuFunctions.setBooleanCookie("showBigVirtualFolders", env.options.show_big_virtual_folders);
			MenuFunctions.updateMenu();
			env.currentAlbum.showMedia();
			$("#loading").hide();
		}
		return false;
	};

	Utilities.prototype.toggleInsideWordsSearch = function() {
		env.search.insideWords = ! env.search.insideWords;
		MenuFunctions.setBooleanCookie("searchInsideWords", env.search.insideWords);
		MenuFunctions.updateMenu();
		if ($("#search-field").val().trim())
			$('#search-button').trigger("click");
	};

	Utilities.prototype.toggleAnyWordSearch = function() {
		env.search.anyWord = ! env.search.anyWord;
		MenuFunctions.setBooleanCookie("searchAnyWord", env.search.anyWord);
		MenuFunctions.updateMenu();
		if (env.search.words.length < 2)
			return;
		if ($("#search-field").val().trim())
			$('#search-button').trigger("click");
	};

	Utilities.prototype.toggleCaseSensitiveSearch = function() {
		env.search.caseSensitive = ! env.search.caseSensitive;
		MenuFunctions.setBooleanCookie("searchCaseSensitive", env.search.caseSensitive);
		MenuFunctions.updateMenu();
		if ($("#search-field").val().trim())
			$('#search-button').trigger("click");
	};

	Utilities.prototype.toggleAccentSensitiveSearch = function() {
		env.search.accentSensitive = ! env.search.accentSensitive;
		MenuFunctions.setBooleanCookie("searchAccentSensitive", env.search.accentSensitive);
		MenuFunctions.updateMenu();
		if ($("#search-field").val().trim())
			$('#search-button').trigger("click");
	};

	Utilities.prototype.toggleNumbersSearch = function() {
		env.search.numbers = ! env.search.numbers;
		MenuFunctions.setBooleanCookie("searchNumbers", env.search.numbers);
		MenuFunctions.updateMenu();
		if ($("#search-field").val().trim())
			$('#search-button').trigger("click");
	};

	Utilities.prototype.toggleTagsOnlySearch = function() {
		env.search.tagsOnly = ! env.search.tagsOnly;
		MenuFunctions.setBooleanCookie("searchTagsOnly", env.search.tagsOnly);
		MenuFunctions.updateMenu();
		if ($("#search-field").val().trim())
			$('#search-button').trigger("click");
	};

	Utilities.prototype.toggleCurrentAbumSearch = function() {
		env.search.currentAlbumOnly = ! env.search.currentAlbumOnly;
		MenuFunctions.setBooleanCookie("searchCurrentAlbum", env.search.currentAlbumOnly);
		MenuFunctions.updateMenu();
		if ($("#search-field").val().trim())
			$('#search-button').trigger("click");
	};


	Utilities.prototype.toggleMetadata = Utilities.toggleMetadata;
	Utilities.prototype.toggleFullscreen = Utilities.toggleFullscreen;
	Utilities.prototype.resizeSingleMediaWithPrevAndNext = Utilities.resizeSingleMediaWithPrevAndNext;
	Utilities.prototype.encodeMapCacheBase = Utilities.encodeMapCacheBase;
}());
//# sourceURL=033-utilities-bis.js
