(function() {
	var util = new Utilities();

	/* constructor */
	function PhotoFloat() {
	}

	PhotoFloat.getStopWords = function() {
		if (! env.search.insideWords && env.options.use_stop_words) {
			// before getting the file check whether it's in the cache
			if (env.cache.hasOwnProperty("stopWords") && env.cache.stopWords.length) {
				return Promise.resolve();
			} else {
				return new Promise(
					function(resolve_getStopWords) {
						env.cache.stopWords = [];
						// get the file
						// var stopWordsFile = util.pathJoin(["cache", "stopwords.json"]);
						let promise = PhotoFloat.getJsonFile("stopwords.json");
						promise.then(
							function(stopWords) {
								env.cache.stopWords = stopWords.stopWords;
								resolve_getStopWords();
							},
							function(jqXHR) {
								// TO DO something different should be made here?
								util.errorThenGoUp(jqXHR.status);
							}
						);
					}
				);
			}
		} else {
			// stop words aren't used
			env.cache.stopWords = [];
			return Promise.resolve();
		}
	};

	PhotoFloat.getJsonFile = function(jsonRelativeFileName) {
		if (env.cache.inexistentFiles.indexOf(jsonRelativeFileName) !== -1) {
			return Promise.reject();
		} else {
			return new Promise(
				function(resolve_getJsonFile, reject_getJsonFile) {
					$.ajax(
						{
							url: util.pathJoin(["cache", jsonRelativeFileName]),
							type: "GET",
							dataType: "json",
							success: function(albumOrPositionsOrMedia) {
								resolve_getJsonFile(albumOrPositionsOrMedia);
							},
							error: function(jqXHR, error, errorThrown) {
								env.cache.inexistentFiles.push(jsonRelativeFileName);
								reject_getJsonFile();
							}
						}
					);
				}
			);
		}
	};

	PhotoFloat.getMediaAndPositions = function(cacheBase, {mustGetMedia, mustGetPositions}) {

		var mediaPromise = getMedia();
		var positionsPromise = getPositions();

		return new Promise(
			function(resolve_AddPositionsAndMedia) {
				Promise.all([mediaPromise, positionsPromise]).then(
					function([mediaGot, positionsGot]) {
						resolve_AddPositionsAndMedia([mediaGot, positionsGot]);
					}
				);
			}
		);
		// end of function getMediaAndPositions body

		// auxiliary functions
		function getMedia() {
			return new Promise(
				function(resolve_getMedia, reject_getMedia) {
					var mediaJsonFile;
					// are media still missing?
					if (mustGetMedia) {
						mediaJsonFile = cacheBase + ".media.json";
						let promise = PhotoFloat.getJsonFile(util.addBySomethingSubdir(mediaJsonFile));
						promise.then(
							function(object) {
								var media = new Media (object);
								resolve_getMedia(media);
							},
							function() {
								resolve_getMedia(false);
							}
						);
					} else {
						resolve_getMedia(false);
					}
				}
			);
		}

		function getPositions() {
			return new Promise(
				function(resolve_getPositions, reject_getPositions) {
					var positionJsonFile;
					// are positions still missing?
					if (mustGetPositions) {
						positionJsonFile = cacheBase + ".positions.json";
						let promise = PhotoFloat.getJsonFile(util.addBySomethingSubdir(positionJsonFile));
						promise.then(
							function(object) {
								var positions = new PositionsAndMedia(object);
								resolve_getPositions(positions);
							},
							function() {
								resolve_getPositions(false);
							}
						);
					} else {
						resolve_getPositions(false);
					}
				}
			);
		}
	};

	PhotoFloat.prototype.convertComplexCombinationsIntoLists = function(codesComplexCombination) {
		var albumCombinationList, mediaCombinationList;
		var albumCombination = codesComplexCombination.split(",")[0];
		if (albumCombination === "")
			albumCombinationList = [];
		else
			albumCombinationList = albumCombination.split("-");
		var mediaCombination = codesComplexCombination.split(",")[1];
		if (mediaCombination === "")
			mediaCombinationList = [];
		else
			mediaCombinationList = mediaCombination.split("-");
		return [albumCombinationList, mediaCombinationList];
	};

	PhotoFloat.isProtectedCacheBase = function(cacheBase) {
		return cacheBase.startsWith(env.options.protected_directories_prefix);
	};

	PhotoFloat.getAlbumFromSingleUnprotectedCacheBaseWithExternalMediaAndPositions = function(unprotectedCacheBase, {getMedia, getPositions}) {
		// this function is called when the album isn't in the cache
		// a brand new album is created

		return new Promise(
			function(resolve_getSingleUnprotectedCacheBase, reject_getSingleUnprotectedCacheBase) {
				let jsonFile = unprotectedCacheBase + ".json";
				let promise = PhotoFloat.getJsonFile(util.addBySomethingSubdir(jsonFile));
				promise.then(
					function unprotectedFileExists(object) {
						let album = new Album(object);
						if (album.hasOwnProperty("media"))
							album.numsMedia = album.media.imagesAudiosVideosCount();
						album.includedFilesByCodesSimpleCombination = new IncludedFiles({",": {}});
						if (album.hasOwnProperty("media"))
							album.includedFilesByCodesSimpleCombination[","].mediaGot = true;
						if (album.hasOwnProperty("positionsAndMediaInTree"))
							album.includedFilesByCodesSimpleCombination[","].positionsGot = true;
						let mustGetMedia = (
							getMedia &&
							! album.includedFilesByCodesSimpleCombination[","].hasOwnProperty("mediaGot") &&
							! album.isABigTransversalAlbum()
						);
						let mustGetPositions = getPositions && ! album.includedFilesByCodesSimpleCombination[","].hasOwnProperty("positionsGot");
						let promise = PhotoFloat.getMediaAndPositions(album.cacheBase, {mustGetMedia: mustGetMedia, mustGetPositions: mustGetPositions});
						promise.then(
							function([mediaGot, positionsGot]) {
								if (mediaGot) {
									album.media = mediaGot;
									album.includedFilesByCodesSimpleCombination[","].mediaGot = true;
								}

								if (positionsGot) {
									album.positionsAndMediaInTree = positionsGot;
									album.numPositionsInTree = album.positionsAndMediaInTree.length;
									album.includedFilesByCodesSimpleCombination[","].positionsGot = true;
								}

								resolve_getSingleUnprotectedCacheBase(album);
							},
							function() {
								console.trace();
							}
						);
					},
					function unprotectedFileDoesntExist() {
						// execution arrives here if the json file doesn't exist
						var emptyAlbum = new Album(unprotectedCacheBase);
						emptyAlbum.empty = true;
						emptyAlbum.includedFilesByCodesSimpleCombination = new IncludedFiles({",": false});

						reject_getSingleUnprotectedCacheBase(emptyAlbum);
					}
				);
			}
		);
	};

	PhotoFloat.prototype.protectedDirectoriesToGet = function() {
		var iAlbumPassword, iMediaPassword, albumGuessedPassword, mediaGuessedPassword, protectedDirectory;
		var result = [];

		for (iAlbumPassword = 0; iAlbumPassword < env.guessedPasswordsMd5.length; iAlbumPassword ++) {
			albumGuessedPassword = env.guessedPasswordsMd5[iAlbumPassword];
			protectedDirectory = env.options.protected_directories_prefix + albumGuessedPassword + ",";
			result.push(protectedDirectory);
		}
		for (iMediaPassword = 0; iMediaPassword < env.guessedPasswordsMd5.length; iMediaPassword ++) {
			mediaGuessedPassword = env.guessedPasswordsMd5[iMediaPassword];
			protectedDirectory = env.options.protected_directories_prefix + "," + mediaGuessedPassword;
			result.push(protectedDirectory);
		}
		for (iAlbumPassword = 0; iAlbumPassword < env.guessedPasswordsMd5.length; iAlbumPassword ++) {
			albumGuessedPassword = env.guessedPasswordsMd5[iAlbumPassword];
			for (iMediaPassword = 0; iMediaPassword < env.guessedPasswordsMd5.length; iMediaPassword ++) {
				mediaGuessedPassword = env.guessedPasswordsMd5[iMediaPassword];
				protectedDirectory = env.options.protected_directories_prefix + albumGuessedPassword + "," + mediaGuessedPassword;
				result.push(protectedDirectory);
			}
		}
		return result;
	};



	PhotoFloat.prototype.mergeProtectedSubalbum = function(subalbum, protectedSubalbum, protectedDirectory, codesSimpleCombination, number) {
		if (protectedSubalbum.hasOwnProperty("randomMedia")) {
			protectedSubalbum.randomMedia.forEach(
				randomSingleMedia => {
					if (! randomSingleMedia.cacheSubdir.startsWith(env.options.protected_directories_prefix))
						randomSingleMedia.cacheSubdir = [protectedDirectory, randomSingleMedia.cacheSubdir].join("/");
				}
			);
		}
		if (subalbum.hasOwnProperty("randomMedia") && protectedSubalbum.hasOwnProperty("randomMedia"))
			subalbum.randomMedia = subalbum.randomMedia.concat(protectedSubalbum.randomMedia);
		else if (protectedSubalbum.hasOwnProperty("randomMedia"))
			subalbum.randomMedia = protectedSubalbum.randomMedia;

		subalbum.numsMediaInSubTree.sum(protectedSubalbum.numsMediaInSubTree);
		subalbum.sizesOfSubTree.sum(protectedSubalbum.sizesOfSubTree);
		subalbum.sizesOfAlbum.sum(protectedSubalbum.sizesOfAlbum);
		subalbum.numPositionsInTree += protectedSubalbum.numPositionsInTree;

		if (env.options.expose_image_positions) {
			subalbum.nonGeotagged.numsMedia.sum(protectedSubalbum.nonGeotagged.numsMedia);
			subalbum.nonGeotagged.numsMediaInSubTree.sum(protectedSubalbum.nonGeotagged.numsMediaInSubTree);
			subalbum.nonGeotagged.sizesOfSubTree.sum(protectedSubalbum.nonGeotagged.sizesOfSubTree);
			subalbum.nonGeotagged.sizesOfAlbum.sum(protectedSubalbum.nonGeotagged.sizesOfAlbum);
		}

		if (subalbum instanceof Album) {
			subalbum.initializeIncludedFilesByCodesSimpleCombinationProperty(codesSimpleCombination, number);
			subalbum.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.countsGot = true;
		}
	};

	PhotoFloat.getAlbum = function(
		albumOrCacheBase,
		getAlbum_error,
		{getMedia = false, getPositions = false, mainSearchCall = false},
		numsProtectedMediaInSubTree
	) {
		// getAlbum_error is a function, and is executed when the album cannot be retrieved:
		// either because it doesn't exist or is a protected one
		var wasInCache = false;

		return new Promise(
			function(resolve_getAlbum, reject_getAlbum) {
				var cacheBase, album;
				if (typeof albumOrCacheBase === "string") {
					cacheBase = albumOrCacheBase;
					album = env.cache.getAlbum(cacheBase);
				} else {
					album = albumOrCacheBase;
					cacheBase = album.cacheBase;
				}

				if (album && ! album.isEmpty()) {
					// the album is already in the cache, possibly missing media y/or positions
					wasInCache = true;
					if (! album.hasOwnProperty("numsProtectedMediaInSubTree") && numsProtectedMediaInSubTree !== undefined) {
						// we arrive here when getAlbum is called by Subalbum.toAlbum():
						// the numsProtectedMediaInSubTree property was built in the subalbum, and now it's passed to the album
						album.numsProtectedMediaInSubTree = numsProtectedMediaInSubTree;
					}

					// add media and positions
					var mediaAndPositionsPromise;
					if (
						// map albums and search albums already have all the media and positions
						album.isCollection() ||
						// the album hasn't unprotected content
						album.includedFilesByCodesSimpleCombination[","] === false
					) {
						// not adding media/positions  => go immediately to mediaAndPositionsPromise.then
						mediaAndPositionsPromise = Promise.resolve([false, false]);
					} else {
						var mustGetMedia = getMedia && (
							! album.hasOwnProperty("media") ||
							album.includedFilesByCodesSimpleCombination[","] !== false && ! album.includedFilesByCodesSimpleCombination[","].hasOwnProperty("mediaGot") &&
							! album.isABigTransversalAlbum()
						);
						var mustGetPositions = getPositions && (
							! album.hasOwnProperty("positionsAndMediaInTree") ||
							album.includedFilesByCodesSimpleCombination[","] !== false && ! album.includedFilesByCodesSimpleCombination[","].hasOwnProperty("positionsGot")
						);
						// this call to getMediaAndPositions adds positions and media of the unprotected album only
						mediaAndPositionsPromise = PhotoFloat.getMediaAndPositions(album.cacheBase, {mustGetMedia: mustGetMedia, mustGetPositions: mustGetPositions});
					}
					mediaAndPositionsPromise.then(
						function mediaAndPositionsGot([mediaGot, positionsGot]) {
							if (mediaGot) {
								if (! album.hasOwnProperty("media") || ! album.media.length)
									album.media = mediaGot;
								else
									// TO DO: verify that concat is enough
									album.media = album.media.concat(mediaGot);
								album.includedFilesByCodesSimpleCombination[","].mediaGot = true;
							}

							if (positionsGot) {
								if (! album.hasOwnProperty("positionsAndMediaInTree") || ! album.positionsAndMediaInTree.length)
									album.positionsAndMediaInTree = positionsGot;
								else
									album.positionsAndMediaInTree.mergePositionsAndMedia(positionsGot);
								album.numPositionsInTree = album.positionsAndMediaInTree.length;
								album.includedFilesByCodesSimpleCombination[","].positionsGot = true;
							}

							if (! album.hasOwnProperty("numsProtectedMediaInSubTree") || album.hasUnloadedProtectedContent(getMedia, getPositions)) {
								var protectedContentPromise = album.addProtectedContent(getMedia, getPositions);
								protectedContentPromise.then(
									function() {
										thingsToBeDoneBeforeResolvingGetAlbum(album);
										resolve_getAlbum(album);
									},
									function() {
										if (getAlbum_error)
											getAlbum_error(album);
									}
								);
							} else {
								thingsToBeDoneBeforeResolvingGetAlbum(album);
								resolve_getAlbum(album);
							}
						},
						function() {
							console.trace();
						}
					);
				} else if (util.isSelectionCacheBase(cacheBase)) {
					// it's a selection album and the album is not in cache, possibly because we started from a link which was shared
					// We must play the selection click history, and then show the selection album
					$("#loading").show();
					$.executeAfterEvent(
						"otherJsFilesLoadedEvent",
						function() {
							env.isPlayingSelectionClickHistory = true;
							let selectionAlbum = util.initializeSelectionAlbum();
							selectionAlbum.changeCacheBase(cacheBase);
							selectionAlbum.selectionClickHistory = util.decodeSelectionCacheBase(cacheBase);

							let playPromise = selectionAlbum.playSelectionClickHistory();
							playPromise.then(
								function() {
									env.isPlayingSelectionClickHistory = false;
									thingsToBeDoneBeforeResolvingGetAlbum(selectionAlbum);
									resolve_getAlbum(selectionAlbum);
									$("#loading").hide();
								}
							);
						}
					);
					util.loadOtherJsCssFiles();
				} else if (util.isSearchCacheBase(cacheBase)) {
					// search albums are not on server
					// if the album hasn't been passed as argument and isn't in cache => it could have been passed with POST and be put in postData["packedAlbum"]
					let searchAlbum = util.initializeSearchAlbumBegin(cacheBase);
					searchAlbum.generateAncestorsCacheBase();
					searchAlbum.search = PhotoFloat.decodeSearchCacheBase(cacheBase);

					if (mainSearchCall) {
						let originalWords = (
							searchAlbum.search.tagsOnly ?
							searchAlbum.search.words[0] :
							searchAlbum.search.words.join(" ")
						);
						originalWords = decodeURIComponent(originalWords);
						$("#search-field").attr("value", originalWords);
						$("#loading").html(util._t("#loading-search", originalWords));
						env.searchAlbum = searchAlbum;
					}

					// possibly we need the stop words, because if some searched word is a stop word it must be removed from the search
					let stopWordsPromise = PhotoFloat.getStopWords();
					stopWordsPromise.then(
						function () {
							function rootSearchAlbumNonExistent() {
								reject_getAlbum("#nothing-to-search");
							}

							// be sure to have in cache the search root album before getting the search words ones
							var rootSearchAlbumPromise = PhotoFloat.getAlbum(env.options.by_search_string, rootSearchAlbumNonExistent, {getMedia: false, getPositions: false});
							rootSearchAlbumPromise.then(
								function(bySearchRootAlbum) {
									function wrongCacheBaseToSearchIn() {
										// the cache base of the album to search in produced error => use the root folder
										// TO DO: maybe the up folder may be tried...
										searchAlbum.search.searchedCacheBase = env.options.folders_string;
										let newSearchCacheBase = PhotoFloat.encodeSearchCacheBase(searchAlbum.search);
										// getAlbum has the noOp as error function because the root album is surely good
										let newAlbumPromise = PhotoFloat.getAlbum(newSearchCacheBase, util.noOp, {getMedia: getMedia, getPositions: getPositions});
										newAlbumPromise.then(
											function(newAlbum) {
												resolve_getAlbum(newAlbum);
											}
										);
									}

									// encoded hashes must be preserved, in order to make tag only search work when a dash is inside the tag
									let splittedCacheBase = cacheBase.split("%2D");
									// splittedCacheBase = splittedCacheBase.map(hash => decodeURI(hash));
									cacheBase = splittedCacheBase.join("%2D");

									searchAlbum.removeStopWordsFromSearchWords();
									// the case that no word has remained after stop words have been removed will be managed in prepareForShowing()

									// get the album where the search has to be performed
									// surely it will be neeeded later
									let albumToSearchInPromise;
									let albumToSearchIn = env.cache.getAlbum(searchAlbum.search.searchedCacheBase);
									if (! albumToSearchIn)
										albumToSearchInPromise = PhotoFloat.getAlbum(searchAlbum.search.searchedCacheBase, wrongCacheBaseToSearchIn, {getMedia: true, getPositions: ! env.options.save_data});
									else
										albumToSearchInPromise = Promise.resolve(albumToSearchIn);
									albumToSearchInPromise.then(
										function(albumToSearchIn) {
											if (
												albumToSearchIn.isSearch() &&
												! albumToSearchIn.visibleSubalbums().length && ! albumToSearchIn.numVisibleMedia().length
											) {
												// the album to search in is a search album with no results or too many results
												// change it to the root album
												searchAlbum.search.searchedCacheBase = env.options.folders_string;
												let newSearchHash = util.encodeHash(
													PhotoFloat.encodeSearchCacheBase(searchAlbum.search),
													env.hashComponents.singleMediaCacheBase,
													env.hashComponents.singleMediaFoldersCacheBase,
													env.hashComponents.collectedAlbumCacheBase,
													env.hashComponents.collectionCacheBase
												);
												window.location.hash = env.hashBeginning + newSearchHash;
											} else {
												// albumToSearchIn is not used directly; now it's in cache, it will be got from there
												let searchPromise = PhotoFloat.getSearchAlbum(searchAlbum, getAlbum_error); // here getAlbum_error is reject_parseHash
												searchPromise.then(
													function(searchAlbum) {
														thingsToBeDoneBeforeResolvingGetAlbum(searchAlbum);
														resolve_getAlbum(searchAlbum);
													},
													function() {
														console.trace();
													}
												);
											}
										},
										function albumToSearchInWrong() {
											// nothing to do
										}
									);
								}
							);
						},
						function() {
							console.trace();
						}
					);
				} else if (util.isMapCacheBase(cacheBase)) {
					$.executeAfterEvent(
						"otherJsFilesLoadedEvent",
						function() {
							env.isPlayingMapClickHistoryInGetAlbum = true;
							let mapAlbum = util.initializeMapAlbum();

							mapAlbum.map = PhotoFloat.decodeMapCacheBase(cacheBase);
							mapAlbum.changeCacheBase(cacheBase);

							$("#my-modal").addClass("almost-hidden");

							// if (mapAlbum.map.clickAlbumCacheBase.indexOf("%") > -1)
							// 	mapAlbum.map.clickAlbumCacheBase = util.decodeAllNonAlfaNum(mapAlbum.map.clickAlbumCacheBase);

							let baseAlbumPromise = PhotoFloat.getAlbum(
								mapAlbum.map.clickAlbumCacheBase,
								getAlbum_error,
								{getMedia: true, getPositions: ! env.options.save_data}
							);
							baseAlbumPromise.then(
								function (clickAlbum) {
									let mapPromise = new Promise(
										function(resolve_mapPromise, reject_mapPromise) {
											let ev = {
												data: {
													singleMedia: null
												}
											};
											const from = "";

											if (mapAlbum.map.whatToGenerateTheMapFor === "e") {
												mapAlbum.generateMap(clickAlbum.positionsAndMediaInTree, ev, from);

												resolve_mapPromise();
											} else if (mapAlbum.map.whatToGenerateTheMapFor === "m") {
												clickAlbum.generatePositionsAndMediaInMedia();
												mapAlbum.generateMap(clickAlbum.positionsAndMediaInMedia, ev, from);

												resolve_mapPromise();
											} else {
												let mediaIndex = clickAlbum.media.findIndex(
													ithMedia => ithMedia.cacheBase === mapAlbum.map.whatToGenerateTheMapFor
												);
												let subalbumIndex = clickAlbum.subalbums.findIndex(
													subalbum => subalbum.cacheBase === [
														mapAlbum.map.clickAlbumCacheBase,
														mapAlbum.map.whatToGenerateTheMapFor
													].join(env.options.cache_folder_separator)
												);

												if (mediaIndex > -1) {
													// a single media map button has been clicked
													let singleMedia = clickAlbum.media[mediaIndex];
													ev = {
														data: {
															singleMedia: singleMedia
														}
													};
													mapAlbum.generateMap(
														new PositionsAndMedia(
															[singleMedia.generatePositionAndMedia()]
														),
														ev,
														from
													);
													resolve_mapPromise();
												} else if (subalbumIndex > -1) {
													// a subalbum map button has been clicked
													let toAlbumPromise = clickAlbum.subalbums[subalbumIndex].toAlbum();
													toAlbumPromise.then(
														function(ithAlbum) {
															clickAlbum.subalbums[subalbumIndex] = ithAlbum;
															mapAlbum.generateMap(ithAlbum.positionsAndMediaInTree, ev, from);

															resolve_mapPromise();
														}
													);
												} else {
													// neither a valid single media nor a valid subalbum map button has been clicked
													getAlbum_error();
												}
											}
										}
									);
									mapPromise.then(
										function() {
											mapAlbum.changeCacheBase(util.encodeMapCacheBase(mapAlbum.map));
											mapAlbum.addAndRemovePositionsPlayingClickHistory();
											let endPreparingPromise = mapAlbum.endPreparingMapAlbum();
											endPreparingPromise.then(
												function() {
													if ($(".modal-close").length)
														$('.modal-close')[0].click();
													$("#my-modal").removeClass("almost-hidden");

													env.isPlayingMapClickHistoryInGetAlbum = false;
													thingsToBeDoneBeforeResolvingGetAlbum(mapAlbum);
													resolve_getAlbum(mapAlbum);
												}
											);
										}
									);
								}
							);
						},
						function () {
							console.trace();
						}
					);
					util.loadOtherJsCssFiles();
				} else {
					// the album is not in the cache, and it's not a collection (search, map, selection)
					// get it brand new
					var unprotectedCacheBasePromise = PhotoFloat.getAlbumFromSingleUnprotectedCacheBaseWithExternalMediaAndPositions(cacheBase, {getMedia: getMedia, getPositions: getPositions});
					unprotectedCacheBasePromise.then(
						function unprotectedAlbumGot(album) {
							if (! album.hasOwnProperty("numsProtectedMediaInSubTree") || album.hasUnloadedProtectedContent(getMedia, getPositions)) {
								var protectedContentPromise = album.addProtectedContent(getMedia, getPositions);
								protectedContentPromise.then(
									function() {
										thingsToBeDoneBeforeResolvingGetAlbum(album);
										resolve_getAlbum(album);
									},
									function() {
										console.trace();
									}
								);
							} else {
								thingsToBeDoneBeforeResolvingGetAlbum(album);
								resolve_getAlbum(album);
							}
						},
						function unprotectedAlbumUnexisting(emptyAlbum) {
							// look for a protected album: something must be there
							if (typeof numsProtectedMediaInSubTree !== "undefined") {
								emptyAlbum.numsProtectedMediaInSubTree = numsProtectedMediaInSubTree;
								numsProtectedMediaInSubTreePropertyIsThere();
							} else {
								// since the album hasn't unprotected content and no protected cache base has been processed yet, the numsProtectedMediaInSubTree property isn't there
								// It must be loaded in order to to know the complex combinations
								// The function getNumsProtectedMediaInSubTreeProperty will do that looking for and loading a protected album
								var numsProtectedMediaPromise = emptyAlbum.getNumsProtectedMediaInSubTreeProperty();
								numsProtectedMediaPromise.then(
									function() {
										numsProtectedMediaInSubTreePropertyIsThere();
									},
									function() {
										// neither the unprotected nor any protected album exists => nonexistent album
										if (getAlbum_error)
											getAlbum_error(emptyAlbum);
									}
								);
							}
							// end of unprotectedAlbumUnexisting function body

							function numsProtectedMediaInSubTreePropertyIsThere() {
								var protectedContentPromise = emptyAlbum.addProtectedContent(getMedia, getPositions);
								protectedContentPromise.then(
									function unprotectedAlbumUnexistingProtectedAlbumExisting() {
										thingsToBeDoneBeforeResolvingGetAlbum(emptyAlbum);
										resolve_getAlbum(emptyAlbum);
									},
									function unprotectedAlbumUnexistingProtectedAlbumUnexisting() {
										// neither the unprotected nor any protected album exists => nonexistent album
										if (getAlbum_error)
											getAlbum_error(emptyAlbum);
									}
								);
							}
						}
					);
				}
			}
		);
		// end of getalbum function

		function thingsToBeDoneBeforeResolvingGetAlbum(theAlbum) {
			var i;
			if (theAlbum.cacheBase === env.options.by_search_string) {
				// root of search albums: build the word list
				for (i = 0; i < theAlbum.subalbums.length; ++i) {
					if (env.searchWordsFromJsonFile.indexOf(theAlbum.subalbums[i].unicodeWords) === -1) {
						env.searchWordsFromJsonFile.push(theAlbum.subalbums[i].unicodeWords);
						env.searchAlbumCacheBasesFromJsonFile.push(theAlbum.subalbums[i].cacheBase);
						env.searchAlbumSubalbumsFromJsonFile.push(theAlbum.subalbums[i]);
					}
				}
			} else if (! theAlbum.isSearch()) {
				theAlbum.removeUnnecessaryPropertiesAndAddParentToMedia();
			}

			if (! theAlbum.isMap())
				theAlbum.generateAncestorsCacheBase();
			if (! wasInCache)
				env.cache.putAlbum(theAlbum);
		}
		//////// end of thingsToBeDoneBeforeResolvingGetAlbum function

	};

	PhotoFloat.returnDecodedHash = function(hash) {
		// decodes the hash and returns its components

		var cacheBaseParts, cacheBasePartsCount;
		var albumCacheBase;
		var mediaCacheBase = null, mediaFolderCacheBase = null;
		var collectionCacheBase = null, collectedAlbumCacheBase = null;

		var cacheBase = PhotoFloat.convertHashToCacheBase(hash);

		if (! cacheBase.length) {
			albumCacheBase = env.options.folders_string;
		} else {
			// split on the slash and count the number of parts
			cacheBaseParts = cacheBase.split("/");
			cacheBasePartsCount = cacheBaseParts.length;

			if (cacheBasePartsCount === 1) {
				// folders or gps or date hash: album only
				albumCacheBase = cacheBase;
			} else if (cacheBasePartsCount === 2) {
				// media in folders album: album, media
				albumCacheBase = cacheBaseParts[0];
				mediaCacheBase = cacheBaseParts[1];
			} else if (cacheBasePartsCount === 3) {
				// gps or date or search or selection hash: album, album where the image is, media
				// subfolder of search hash: album, search subalbum, search album
				if (
					util.isSearchCacheBase(cacheBaseParts[2]) ||
					util.isSelectionCacheBase(cacheBaseParts[2]) ||
					util.isMapCacheBase(cacheBaseParts[2])) {
					albumCacheBase = cacheBaseParts[0];
					collectedAlbumCacheBase = cacheBaseParts[1];
					collectionCacheBase = cacheBaseParts[2];
				} else {
					albumCacheBase = cacheBaseParts[0];
					mediaFolderCacheBase = cacheBaseParts[1];
					mediaCacheBase = cacheBaseParts[2];
				}
			} else if (cacheBasePartsCount === 4) {
				albumCacheBase = cacheBaseParts[0];
				collectedAlbumCacheBase = cacheBaseParts[1];
				collectionCacheBase = cacheBaseParts[2];
				mediaCacheBase = cacheBaseParts[3];
			}
		}

		return {
			albumCacheBase: albumCacheBase,
			mediaCacheBase: mediaCacheBase,
			mediaFolderCacheBase: mediaFolderCacheBase,
			collectedAlbumCacheBase: collectedAlbumCacheBase,
			collectionCacheBase: collectionCacheBase
		};
	};

	PhotoFloat.prototype.decodeHash = function() {
		env.hashComponents = PhotoFloat.returnDecodedHash(window.location.hash);

		// env.albumCacheBase = components.albumCacheBase;
		// env.mediaCacheBase = components.mediaCacheBase;
		// env.mediaFolderCacheBase = components.mediaFolderCacheBase;
		// env.collectedAlbumCacheBase = components.collectedAlbumCacheBase;
		// env.collectionCacheBase = components.collectionCacheBase;
	};


	PhotoFloat.decodeMapCacheBase = function(albumCacheBase) {
		let splittedAlbumCacheBase = albumCacheBase.split(
			env.options.cache_folder_separator
		)[1].split(env.options.search_options_separator);
		return {
			clickAlbumCacheBase: util.decodeAllNonAlfaNum(splittedAlbumCacheBase[0]),
			whatToGenerateTheMapFor: util.decodeAllNonAlfaNum(splittedAlbumCacheBase[1]),
			clickHistory: util.decodeMapClickHistory(splittedAlbumCacheBase[2])
		};
	};

	PhotoFloat.encodeSearchCacheBase = function(searchObject) {
		let searchCacheBase = env.options.by_search_string + env.options.cache_folder_separator;
		if (searchObject.insideWords)
			searchCacheBase += "i" + env.options.search_options_separator;
		if (searchObject.anyWord)
			searchCacheBase += "n" + env.options.search_options_separator;
		if (searchObject.caseSensitive)
			searchCacheBase += "c" + env.options.search_options_separator;
		if (searchObject.accentSensitive)
			searchCacheBase += "a" + env.options.search_options_separator;
		if (searchObject.numbers)
			searchCacheBase += "b" + env.options.search_options_separator;
		if (searchObject.tagsOnly)
			searchCacheBase += "t" + env.options.search_options_separator;
		if (searchObject.currentAlbumOnly)
			searchCacheBase += "o" + env.options.search_options_separator;
		searchCacheBase += searchObject.wordsString + env.options.search_options_separator;
		searchCacheBase += util.encodeAllNonAlfaNum(searchObject.searchedCacheBase);
		return searchCacheBase;
	};

	PhotoFloat.decodeSearchCacheBase = function(albumCacheBase) {
		var searchOptions, searchObject = {};

		var splittedAlbumCacheBase = albumCacheBase.split(env.options.cache_folder_separator);
		var wordsWithOptionsString = splittedAlbumCacheBase[1];
		var wordsAndOptions = wordsWithOptionsString.split(env.options.search_options_separator);
		var wordsString = wordsAndOptions[wordsAndOptions.length - 2];
		var wordsStringOriginal = wordsString.replace(/_/g, " ");
		// the normalized words are needed in order to compare with the search cache json files names, which are normalized
		var wordsStringNormalizedAccordingToOptions = util.normalizeAccordingToOptions(wordsStringOriginal);
		var wordsStringNormalized = wordsStringOriginal.toLowerCase();
		wordsStringNormalized = util.removeAccents(wordsStringNormalized);
		if (wordsAndOptions.length > 2) {
			searchOptions = wordsAndOptions.slice(0, -2);
			searchObject.insideWords = searchOptions.includes("i");
			searchObject.anyWord = searchOptions.includes("n");
			searchObject.caseSensitive = searchOptions.includes("c");
			searchObject.accentSensitive = searchOptions.includes("a");
			searchObject.numbers = searchOptions.includes("b");
			searchObject.tagsOnly = searchOptions.includes("t");
			searchObject.currentAlbumOnly = searchOptions.includes("o");
		}

		wordsString = util.normalizeAccordingToOptions(wordsString);
		if (searchObject.tagsOnly) {
			searchObject.wordsString = wordsString;
			searchObject.wordsFromUser = [decodeURIComponent(wordsString).replace(/_/g, " ")];
			searchObject.wordsFromUserNormalized = [decodeURIComponent(wordsStringNormalized)];
			searchObject.wordsFromUserNormalizedAccordingToOptions = [decodeURIComponent(wordsStringNormalizedAccordingToOptions)];
			searchObject.words = [decodeURIComponent(wordsStringOriginal).replace(/_/g, " ")];
		} else {
			searchObject.wordsString = wordsString;
			searchObject.wordsFromUser = wordsString.split("_");
			searchObject.wordsFromUserNormalized = wordsStringNormalized.split(" ");
			searchObject.wordsFromUserNormalizedAccordingToOptions = wordsStringNormalizedAccordingToOptions.split(" ");
			searchObject.words = wordsStringOriginal.split(" ");
		}

		searchObject.searchedCacheBase = util.decodeAllNonAlfaNum(wordsAndOptions.at(-1));

		return searchObject;
	};


	PhotoFloat.prototype.parseHashAndReturnAlbumAndMediaIndex = function() {
		return new Promise(
			function(resolve_parseHash, reject_parseHash) {
				$("#message-too-many-media").css("display", "");
				$(".search-failed").hide();
				$("#album-view, #subalbums, #thumbs").removeClass("hidden");

				let promise = PhotoFloat.getAlbum(
					env.hashComponents.albumCacheBase,
					reject_parseHash,
					{getMedia: true, getPositions: ! env.options.save_data, mainSearchCall: true}
				);
				promise.then(
					function(album) {
						let indexMedia = album.getMediaIndex(env.hashComponents.mediaFolderCacheBase, env.hashComponents.mediaCacheBase);
						if (indexMedia === null)
							// getMediaIndex arguments don't match any media in the album,
							// in getMediaIndex either the authentication dialog has been shown or the hash has been changed to the album
							indexMedia = -1;

						resolve_parseHash([album, indexMedia]);
					},
					function() {
						console.trace();
					}
				);
			}
		);
	};

	PhotoFloat.prototype.noResults = function(album, selector = "#no-results") {
		// no media found or other search fail, show the message
		env.currentAlbum = album;
		$.triggerEvent("readyToLoadOtherJsFilesEvent");
		$.executeAfterEvents(
			["otherJsFilesLoadedEvent", "imagesInPageLoadedEvent"],
			function() {
				MenuFunctions.openSearchMenu(album);
				$("#subalbums, #thumbs, #media-view").addClass("hidden-by-no-results");
				$("#loading").hide();
				$(".search-failed").hide();
				$(selector).stop().fadeIn(
					1000,
					function() {
						$(window).on(
							"click",
							function() {
								$(window).off("click");
								$("#loading").html(util._t("#loading"));
								$(selector).fadeOut(300);
							}
						);
					}
				);
			}
		);
	};

	PhotoFloat.getSearchAlbum = function(searchAlbum, reject_parseHash) {
		return new Promise(
			function(resolve_getSearchAlbum, reject_getSearchAlbum) {
				function subalbumsAbsentOrGot(searchAlbum) {
					var indexMedia, indexSubalbums;
					$(".search-failed").hide();

					for (indexMedia = 0; indexMedia < searchAlbum.media.length; indexMedia ++) {
						let ithMedia = searchAlbum.media[indexMedia];
						// add the parent to the media
						ithMedia.addParent(searchAlbum);
						let ithMediaArray = new Media([ithMedia]);
						searchAlbum.numsMediaInSubTree.sum(ithMediaArray.imagesAudiosVideosCount());
						if (ithMedia.hasGpsData()) {
							// add the media position
							searchAlbum.positionsAndMediaInTree.addSingleMedia(ithMedia);
							searchAlbum.numPositionsInTree = searchAlbum.positionsAndMediaInTree.length;
						} else if (env.options.expose_image_positions) {
							searchAlbum.nonGeotagged.numsMediaInSubTree.sum(ithMediaArray.imagesAudiosVideosCount());
							searchAlbum.nonGeotagged.sizesOfSubTree.sum(ithMedia.fileSizes);
							searchAlbum.nonGeotagged.sizesOfAlbum.sum(ithMedia.fileSizes);
						}
						searchAlbum.sizesOfSubTree.sum(ithMedia.fileSizes);
						searchAlbum.sizesOfAlbum.sum(ithMedia.fileSizes);
					}

					if (searchAlbum.subalbums.length) {
						let subalbumPromises = [];
						for (indexSubalbums = 0; indexSubalbums < searchAlbum.subalbums.length; indexSubalbums ++) {
							let thisSubalbum = searchAlbum.subalbums[indexSubalbums];
							let thisIndex = indexSubalbums;
							// update the media counts
							searchAlbum.numsMediaInSubTree.sum(thisSubalbum.numsMediaInSubTree);
							if (env.options.expose_image_positions)
								searchAlbum.nonGeotagged.numsMediaInSubTree.sum(thisSubalbum.nonGeotagged.numsMediaInSubTree);
							// update the size totals
							searchAlbum.sizesOfSubTree.sum(thisSubalbum.sizesOfSubTree);
							searchAlbum.sizesOfAlbum.sum(thisSubalbum.sizesOfAlbum);
							if (env.options.expose_image_positions) {
								searchAlbum.nonGeotagged.sizesOfSubTree.sum(thisSubalbum.nonGeotagged.sizesOfSubTree);
								searchAlbum.nonGeotagged.sizesOfAlbum.sum(thisSubalbum.nonGeotagged.sizesOfAlbum);
							}

							// add the points from the subalbums

							// the subalbum could still have no positionsAndMediaInTree array, get it
							if (! thisSubalbum.hasOwnProperty("positionsAndMediaInTree")) {
								let subalbumPromise = new Promise(
									function(resolve_subalbumPromise) {
										let promise = PhotoFloat.getAlbum(thisSubalbum.cacheBase, null, {getMedia: false, getPositions: ! env.options.save_data});
										promise.then(
											function(thisAlbum) {
												searchAlbum.subalbums[thisIndex] = thisAlbum;
												if (! env.options.save_data) {
													searchAlbum.positionsAndMediaInTree.mergePositionsAndMedia(thisAlbum.positionsAndMediaInTree);
													searchAlbum.numPositionsInTree = searchAlbum.positionsAndMediaInTree.length;
												}
												resolve_subalbumPromise();
											},
											function() {
												console.trace();
											}
										);
									}
								);
								subalbumPromises.push(subalbumPromise);
							}
						}
						Promise.all(subalbumPromises).then(
							function() {
								searchAlbum.endPreparingAlbum();

								resolve_getSearchAlbum(searchAlbum);
							}
						);
					} else {
						searchAlbum.endPreparingAlbum();

						resolve_getSearchAlbum(searchAlbum);
					}
				}

				function searchEnded(resetMediaAndSubalbums = false) {
					if (resetMediaAndSubalbums) {
						searchAlbum.initializeSearchAlbumEnd();
						searchAlbum.media = new Media([]);
						searchAlbum.subalbums = new Subalbums([]);
					}
					delete searchAlbum.mediaSort;
					delete searchAlbum.mediaReverseSort;
					delete searchAlbum.albumSort;
					delete searchAlbum.albumReverseSort;

					if (searchAlbum.media.length || searchAlbum.subalbums.length) {
						searchAlbum.sortAlbumsMedia();
					}

					subalbumsAbsentOrGot(searchAlbum);
				}
				// end of nested functions


				var albumCacheBases = [], wordSubalbums = new Subalbums([]);
				var lastIndex, i, j, wordCacheBases, numSearchAlbumsReady = 0, numSubAlbumsToGet = 0;
				var searchResultsMedia = [];
				var searchResultsSubalbums = [];

				// searchAlbum.ancestorsCacheBase = bySearchRootAlbum.ancestorsCacheBase.slice();
				if (searchAlbum.search.wordsFromUser.length && ! searchAlbum.search.anyWord)
					// when serching all the words, getting the first album is enough, media that do not match the other words will be escluded later
					lastIndex = 0;
				else
					lastIndex = searchAlbum.search.wordsFromUser.length - 1;
				if (searchAlbum.search.insideWords) {
					// we must determine the albums that could match the words given by the user, word by word
					for (i = 0; i <= lastIndex; i ++) {
						wordCacheBases = [];
						for (j = 0; j < env.searchWordsFromJsonFile.length; j ++) {
							if (
								env.searchWordsFromJsonFile[j].some(
									function(word) {
										return word.includes(searchAlbum.search.wordsFromUserNormalized[i]);
									}
								)
							) {
								wordCacheBases.push(env.searchAlbumCacheBasesFromJsonFile[j]);
								wordSubalbums.push(env.searchAlbumSubalbumsFromJsonFile[j]);
								numSubAlbumsToGet ++;
							}
						}
						if (wordCacheBases.length)
							albumCacheBases.push(wordCacheBases);
						else
							albumCacheBases.push([]);
					}
				} else {
					// whole words
					for (i = 0; i <= lastIndex; i ++) {
						if (
							env.searchWordsFromJsonFile.some(
								function(words, index) {
									if (words.includes(searchAlbum.search.wordsFromUserNormalized[i].replace(/([^\p{L}\p{N}])/ug, " "))) {
										albumCacheBases.push([env.searchAlbumCacheBasesFromJsonFile[index]]);
										wordSubalbums.push(env.searchAlbumSubalbumsFromJsonFile[index]);
										return true;
									}
									return false;
								}
							)
						) {
							numSubAlbumsToGet ++;
						} else {
							albumCacheBases.push([]);
						}
					}
				}

				let resetMediaAndSubalbums = true;
				if (numSubAlbumsToGet > env.options.max_search_album_number) {
					// proprerly terminate too many results
					env.searchTooWide = true;
					searchEnded(resetMediaAndSubalbums);
					// nothing more to do in this function
				} else {
					$(".search-failed").hide();
					searchAlbum.initializeSearchAlbumEnd();
					searchAlbum.numsProtectedMediaInSubTree = util.sumNumsProtectedMediaOfArray(wordSubalbums);

					if (lastIndex < 0) {
						// proprerly terminate if no search words
						searchEnded(resetMediaAndSubalbums);
						// next for cycle isn't executed because albumCacheBases[indexWords].length is 0
					}

					for (let indexWords = 0; indexWords <= lastIndex; indexWords ++) {
						if (! albumCacheBases[indexWords].length) {
							// proprerly terminate if no result
							searchEnded(resetMediaAndSubalbums);
							// next for cycle isn't executed because albumCacheBases[indexWords].length is 0
						}

						for (let indexAlbums = 0; indexAlbums < albumCacheBases[indexWords].length; indexAlbums ++) {
							let thisIndexWords = indexWords, thisIndexAlbums = indexAlbums;
							let promise = PhotoFloat.getAlbum(
								albumCacheBases[thisIndexWords][thisIndexAlbums],
								reject_parseHash,
								{getMedia: true, getPositions: ! env.options.save_data}
							);
							promise.then(
								function(wordAlbum) {
									env.cache.putAlbum(wordAlbum);

									let wordAlbumClone = wordAlbum.clone();
									let resultMedia = wordAlbumClone.media;
									let resultSubalbums = wordAlbumClone.subalbums;

									// found media and subalbums still has to be filtered according to search criteria
									resultMedia.filterMediaAgainstSearchedAlbum(searchAlbum);
									resultMedia.filterMediaAgainstOneWord(searchAlbum.search, searchAlbum.search.wordsFromUserNormalizedAccordingToOptions[thisIndexWords]);

									resultSubalbums.filterSubalbumsAgainstSearchedAlbum(searchAlbum.search);
									resultSubalbums.filterSubalbumsAgainstOneWord(
										searchAlbum.search,
										searchAlbum.search.wordsFromUserNormalizedAccordingToOptions[thisIndexWords]
									);

									if (! (thisIndexWords in searchResultsMedia)) {
										searchResultsMedia[thisIndexWords] = resultMedia;
										searchResultsSubalbums[thisIndexWords] = resultSubalbums;
									} else {
										searchResultsMedia[thisIndexWords].unionForSearches(resultMedia);
										searchResultsSubalbums[thisIndexWords].unionForSearches(resultSubalbums);
									}

									// the following instruction makes me see that numSearchAlbumsReady never reaches numSubAlbumsToGet when numSubAlbumsToGet is > 1000,
									// numSearchAlbumsReady remains < 1000

									numSearchAlbumsReady ++;
									if (numSearchAlbumsReady >= numSubAlbumsToGet) {
										// all the word albums have been fetched and worked out, we can merge the results
										searchAlbum.media = new Media(searchResultsMedia[0]);
										searchAlbum.subalbums = new Subalbums(searchResultsSubalbums[0]);
										for (let indexWords1 = 1; indexWords1 <= lastIndex; indexWords1 ++) {
											if (indexWords1 in searchResultsMedia) {
												if (searchAlbum.search.anyWord)
													searchAlbum.media.unionForSearches(searchResultsMedia[indexWords1]);
												else
													searchAlbum.media.intersectionForSearches(searchResultsMedia[indexWords1]);
											}
											if (indexWords1 in searchResultsSubalbums) {
												if (searchAlbum.search.anyWord)
													searchAlbum.subalbums.unionForSearches(searchResultsSubalbums[indexWords1]);
												else
													searchAlbum.subalbums.intersectionForSearches(searchResultsSubalbums[indexWords1]);
											}
										}

										if (lastIndex != searchAlbum.search.wordsFromUser.length - 1) {
											// we still have to filter out the media and subalbums that do not match the words after the first
											// we are in all words search mode

											searchAlbum.media.filterMediaAgainstEveryWord(searchAlbum.search, lastIndex);
											searchAlbum.subalbums.filterSubalbumsAgainstEveryWord(searchAlbum.search, lastIndex);
										}

										// search albums need to conform to default behaviour of albums:
										// json files must have subalbums and media sorted according to options
										let promises = [];
										if (searchAlbum.media.length) {
											// collect the cache bases to get, so that the album for a cachebase is fetched only once
											let cacheBasesAndMediaToGet = [];
											searchAlbum.media.forEach(
												function(singleMedia) {
													let cacheBase = singleMedia.foldersCacheBase;
													// let searchStartCacheBase = albumCacheBase.split(env.options.cache_folder_separator).slice(2).join(env.options.cache_folder_separator);
													if (util.isByDateCacheBase(searchAlbum.search.searchedCacheBase) && singleMedia.hasOwnProperty("dayAlbumCacheBase"))
														cacheBase = singleMedia.dayAlbumCacheBase;
													else if (util.isByGpsCacheBase(searchAlbum.search.searchedCacheBase) && singleMedia.hasGpsData())
														cacheBase = singleMedia.gpsAlbumCacheBase;
													let matchingIndex = cacheBasesAndMediaToGet.findIndex(cacheBaseAndMedia => cacheBaseAndMedia.cacheBase === cacheBase);
													if (matchingIndex !== -1)
														cacheBasesAndMediaToGet[matchingIndex].media.push(singleMedia);
													else
														cacheBasesAndMediaToGet.push({cacheBase: cacheBase, media: [singleMedia]});
												}
											);
											cacheBasesAndMediaToGet.forEach(
												function getCacheBase(cacheBaseAndMedia) {
													let cacheBasePromise = new Promise(
														function(resolve_getAlbum) {
															let parentAlbumPromise = PhotoFloat.getAlbum(cacheBaseAndMedia.cacheBase, null, {getMedia: false, getPositions: false});
															parentAlbumPromise.then(
																function albumGot(parentAlbum) {
																	cacheBaseAndMedia.media.forEach(
																		singleMedia => {
																			singleMedia.generateCaptionsForSearch(parentAlbum);
																		}
																	);
																	resolve_getAlbum();
																}
															);
														}
													);
													promises.push(cacheBasePromise);
												}
											);
										}

										if (searchAlbum.subalbums.length) {
											// search albums need to conform to default behaviour of albums: json files have subalbums and media sorted according to options
											searchAlbum.subalbums.forEach(
												function(subalbum, iSubalbum) {
													let promise = new Promise(
														function(resolve_subalbum) {
															var convertAlbumPromise = subalbum.toAlbum(null, {getMedia: false, getPositions: false});
															convertAlbumPromise.then(
																function(subalbum) {
																	searchAlbum.subalbums[iSubalbum] = subalbum;
																	searchAlbum.subalbums[iSubalbum].generateCaptionsForSearch();
																	resolve_subalbum();
																},
																function() {
																	console.trace();
																}
															);
														}
													);
													promises.push(promise);
												}
											);
										}

										Promise.all(promises).then(
											function() {
												searchEnded();
											}
										);
									}
								},
								function() {
									console.trace();
								}
							);
						}
					}
				}
			}
		);
	};


	PhotoFloat.convertCacheBaseToId = function(cacheBase) {
		var codedHash, i, chr;

		if (cacheBase.length === 0)
			return 0;
		else if (cacheBase.indexOf(".") === -1)
			return cacheBase;
		else {
			for (i = 0; i < cacheBase.length; i++) {
				chr = cacheBase.charCodeAt(i);
				codedHash = ((codedHash << 5) - codedHash) + chr;
				codedHash |= 0; // Convert to 32bit integer
			}
			return cacheBase.replace(/\./g, "_") + "_" + codedHash;
		}
	};

	PhotoFloat.convertHashToCacheBase = function(hash) {
		while (hash.length) {
			if (hash.charAt(0) === "#")
				hash = hash.substring(1);
			else if (hash.charAt(0) === "!")
				hash = hash.substring(1);
			else if (hash.charAt(0) === "/")
				hash = hash.substring(1);
			else if (hash.substring(0, 3) === "%21")
				hash = hash.substring(3);
			else if (hash.charAt(hash.length - 1) === "/")
				hash = hash.substring(0, hash.length - 1);
			else
				break;
		}

		if (! hash)
			hash = env.options.folders_string;
		return hash;
	};

	PhotoFloat.prototype.getOptions = function() {
		function setEnvLanguage() {
			var userLanguages = navigator.language || navigator.userLanguage || navigator.browserLanguage || navigator.systemLanguage || "en-US";
			var userLanguage = userLanguages.split("-")[0];
			if (userLanguage && env.translations[userLanguage] !== undefined)
				env.language = userLanguage;
			else if (env.options.language && env.translations[env.options.language] !== undefined)
				env.language = env.options.language;
			else
				env.language = "en";

			env.numberFormat = Intl.NumberFormat(userLanguages);

		}

		// beginning of getOptions body
		if (Object.keys(env.options).length > 0) {
			if (! util.isSearchHash()) {
				// reset the return link from search
				env.search.searchedCacheBase = PhotoFloat.convertHashToCacheBase(env.hashComponents.albumCacheBase);
			}
			return Promise.resolve();
		} else {
			return new Promise(
				function(resolve_getOptions, reject_getOptions) {
					let promise = PhotoFloat.getJsonFile("options.json");
					promise.then(
						function(data) {
							// for map zoom levels, see http://wiki.openstreetmap.org/wiki/Zoom_levels

							for (let key in data) {
								if (data.hasOwnProperty(key))
									env.options[key] = data[key];
							}

							// temporarily add default values for options not included yet in options.json
							if (env.options.expose_original_media === undefined) {
								env.options.expose_original_media = false;
								env.options.expose_full_size_media_in_cache = true;
								env.options.expose_image_metadata = true;
								env.options.expose_image_dates = true;
								env.options.expose_image_positions = true;
							}

							if (env.options.download_images === undefined)
								env.options.download_images = true;
							if (env.options.download_images_size_index === undefined) {
								if (env.options.expose_original_media)
									env.options.download_images_size_index = "original";
								else if (env.options.expose_full_size_media_in_cache)
									env.options.download_images_size_index = "fullSize";
								else
									env.options.download_images_size_index = 0;
							}
							if (env.options.download_images_format === undefined)
								env.options.download_images_format = "jpg";

							if (env.options.download_audios === undefined)
								env.options.download_audios = true;
							if (env.options.download_audios_size === undefined) {
								if (env.options.expose_original_media)
									env.options.download_audios_size = "original";
								else if (env.options.expose_full_size_media_in_cache)
									env.options.download_audios_size = "fullSize";
								else
									env.options.download_audios_size = "transcoded";
							}

							if (env.options.download_videos === undefined)
								env.options.download_videos = true;
							if (env.options.download_videos_size === undefined) {
								if (env.options.expose_original_media)
									env.options.download_videos_size = "original";
								else if (env.options.expose_full_size_media_in_cache)
									env.options.download_videos_size = "fullSize";
								else
									env.options.download_videos_size = "transcoded";
							}

							if (env.options.download_subalbums === undefined)
								env.options.download_subalbums = true;

							if (env.options.download_selection === undefined)
								env.options.download_selection = false;

							if (env.options.save_data)
								// do not optimize image formats
								env.devicePixelRatio = 1;

							// save the options in order to restore them if requested
							env.defaultOptions = util.cloneObject(env.options);

							setEnvLanguage();
							util.translate();

							// override according to user selections

							var albumSortCookie = MenuFunctions.getCookie("albumSortRequested");
							env.albumSort = env.options.default_album_name_sort ? "name" : "exifDateMax";
							if (albumSortCookie !== null)
								env.albumSort = albumSortCookie;

							var albumReverseSortCookie = MenuFunctions.getBooleanCookie("albumReverseSortRequested");
							env.albumReverseSort = env.options.default_album_reverse_sort;
							if (albumReverseSortCookie !== null)
								env.albumReverseSort = albumReverseSortCookie;

							var mediaSortCookie = MenuFunctions.getCookie("mediaSortRequested");
							env.mediaSort = env.options.default_media_name_sort ? "name" : "exifDateMax";
							if (mediaSortCookie !== null)
								env.mediaSort = mediaSortCookie;

							var mediaReverseSortCookie = MenuFunctions.getBooleanCookie("mediaReverseSortRequested");
							env.mediaReverseSort = env.options.default_media_reverse_sort;
							if (mediaReverseSortCookie !== null)
								env.mediaReverseSort = mediaReverseSortCookie;

							var titleCookie = MenuFunctions.getBooleanCookie("hideTitle");
							if (titleCookie !== null)
								env.options.hide_title = titleCookie;

							var descriptionsCookie = MenuFunctions.getBooleanCookie("hideDescriptions");
							if (descriptionsCookie !== null)
								env.options.hide_descriptions = descriptionsCookie;

							var tagsCookie = MenuFunctions.getBooleanCookie("hideTags");
							if (tagsCookie !== null)
								env.options.hide_tags = tagsCookie;

							var bottomThumbnailsCookie = MenuFunctions.getBooleanCookie("hideBottomThumbnails");
							if (bottomThumbnailsCookie !== null)
								env.options.hide_bottom_thumbnails = bottomThumbnailsCookie;

							var saveDataCookie = MenuFunctions.getBooleanCookie("saveData");
							if (saveDataCookie !== null)
								env.options.save_data = saveDataCookie;

							var slideCookie = MenuFunctions.getBooleanCookie("albumsSlideStyle");
							if (slideCookie !== null)
								env.options.albums_slide_style = slideCookie;

							if (env.options.thumb_spacing)
								env.options.spacingSavedValue = env.options.thumb_spacing;
							else
								env.options.spacingSavedValue = env.options.media_thumb_size * 0.03;

							var spacingCookie = MenuFunctions.getNumberCookie("spacing");
							if (spacingCookie !== null) {
								env.options.spacing = spacingCookie;
							} else {
								env.options.spacing = env.options.thumb_spacing;
							}

							var showAlbumNamesCookie = MenuFunctions.getBooleanCookie("showAlbumNamesBelowThumbs");
							if (showAlbumNamesCookie !== null)
								env.options.show_album_names_below_thumbs = showAlbumNamesCookie;

							var showMediaCountCookie = MenuFunctions.getBooleanCookie("showAlbumMediaCount");
							if (showMediaCountCookie !== null)
								env.options.show_album_media_count = showMediaCountCookie;

							var showMediaNamesCookie = MenuFunctions.getBooleanCookie("showMediaNamesBelowThumbs");
							if (showMediaNamesCookie !== null)
								env.options.show_media_names_below_thumbs = showMediaNamesCookie;

							if (! env.options.only_square_thumbnails) {
								var squareAlbumsCookie = MenuFunctions.getCookie("albumThumbType");
								if (squareAlbumsCookie !== null)
									env.options.album_thumb_type = squareAlbumsCookie;

								var squareMediaCookie = MenuFunctions.getCookie("mediaThumbType");
								if (squareMediaCookie !== null)
									env.options.media_thumb_type = squareMediaCookie;
							}

							var flatDownloadsCookie = MenuFunctions.getBooleanCookie("flatDownloads");
							if (flatDownloadsCookie !== null)
								env.options.flat_downloads = flatDownloadsCookie;

							var imagesDownloadCookie = MenuFunctions.getBooleanCookie("imagesDownload");
							if (imagesDownloadCookie !== null)
								env.options.download_images = imagesDownloadCookie;

							var imagesDownloadSizeIndexCookie = MenuFunctions.getCookie("imagesDownloadSizeIndex");
							if (imagesDownloadSizeIndexCookie !== null) {
								if (["original", "fullSize"].indexOf(imagesDownloadSizeIndexCookie) !== -1)
									env.options.download_images_size_index = imagesDownloadSizeIndexCookie;
								else
									env.options.download_images_size_index = Number(imagesDownloadSizeIndexCookie);
							}

							var imagesDownloadFormatCookie = MenuFunctions.getCookie("imagesDownloadFormat");
							if (imagesDownloadFormatCookie !== null)
								env.options.download_images_format = imagesDownloadFormatCookie;

							var audiosDownloadCookie = MenuFunctions.getBooleanCookie("audiosDownload");
							if (audiosDownloadCookie !== null)
								env.options.download_audios = audiosDownloadCookie;

							var audiosDownloadSizeCookie = MenuFunctions.getCookie("audiosDownloadSize");
							if (audiosDownloadSizeCookie !== null)
								env.options.download_audios_size = audiosDownloadSizeCookie;

							var videosDownloadCookie = MenuFunctions.getBooleanCookie("videosDownload");
							if (videosDownloadCookie !== null)
								env.options.download_videos = videosDownloadCookie;

							var videosDownloadSizeCookie = MenuFunctions.getCookie("videosDownloadSize");
							if (videosDownloadSizeCookie !== null)
								env.options.download_videos_size = videosDownloadSizeCookie;

							var subalbumsDownloadCookie = MenuFunctions.getBooleanCookie("subalbumsDownload");
							if (subalbumsDownloadCookie !== null)
								env.options.download_subalbums = subalbumsDownloadCookie;

							var selectionDownloadCookie = MenuFunctions.getBooleanCookie("selectionDownload");
							if (selectionDownloadCookie !== null)
								env.options.download_selection = selectionDownloadCookie;

							env.search.insideWords = false;
							var searchInsideWordsCookie = MenuFunctions.getBooleanCookie("searchInsideWords");
							if (searchInsideWordsCookie !== null)
								env.search.insideWords = searchInsideWordsCookie;

							env.search.anyWord = false;
							var searchAnyWordCookie = MenuFunctions.getBooleanCookie("searchAnyWord");
							if (searchAnyWordCookie !== null)
								env.search.anyWord = searchAnyWordCookie;

							env.search.caseSensitive = false;
							var searchCaseSensitiveCookie = MenuFunctions.getBooleanCookie("searchCaseSensitive");
							if (searchCaseSensitiveCookie !== null)
								env.search.caseSensitive = searchCaseSensitiveCookie;

							env.search.accentSensitive = false;
							var searchAccentSensitiveCookie = MenuFunctions.getBooleanCookie("searchAccentSensitive");
							if (searchAccentSensitiveCookie !== null)
								env.search.accentSensitive = searchAccentSensitiveCookie;

							env.search.numbers = false;
							var searchNumbersCookie = MenuFunctions.getBooleanCookie("searchNumbers");
							if (searchNumbersCookie !== null)
								env.search.numbers = searchNumbersCookie;

							env.search.tagsOnly = false;
							var searchTagsOnlyCookie = MenuFunctions.getBooleanCookie("searchTagsOnly");
							if (searchTagsOnlyCookie !== null)
								env.search.tagsOnly = searchTagsOnlyCookie;

							env.search.currentAlbumOnly = true;
							var searchCurrentAlbumCookie = MenuFunctions.getBooleanCookie("searchCurrentAlbum");
							if (searchCurrentAlbumCookie !== null)
								env.search.currentAlbumOnly = searchCurrentAlbumCookie;

							env.options.show_big_virtual_folders = false;
							var showBigVirtualFoldersCookie = MenuFunctions.getBooleanCookie("showBigVirtualFolders");
							if (showBigVirtualFoldersCookie !== null)
								env.options.show_big_virtual_folders = showBigVirtualFoldersCookie;

							if (! env.search.hasOwnProperty("searchedCacheBase") || ! env.search.searchedCacheBase)
								env.search.searchedCacheBase = env.options.folders_string;
							if (! env.search.hasOwnProperty("saved_cache_base_to_search_in") || ! env.search.saved_cache_base_to_search_in)
								env.search.saved_cache_base_to_search_in = env.options.folders_string;

							env.slideAlbumButtonBorder = 1;
							env.slideBorder = 3;
							env.slideMarginFactor = 0.05;

							env.options.foldersStringWithTrailingSeparator = env.options.folders_string + env.options.cache_folder_separator;
							env.options.byDateStringWithTrailingSeparator = env.options.by_date_string + env.options.cache_folder_separator;
							env.options.byGpsStringWithTrailingSeparator = env.options.by_gps_string + env.options.cache_folder_separator;
							env.options.bySearchStringWithTrailingSeparator = env.options.by_search_string + env.options.cache_folder_separator;
							env.options.bySelectionStringWithTrailingSeparator = env.options.by_selection_string + env.options.cache_folder_separator;
							env.options.byMapStringWithTrailingSeparator = env.options.by_map_string + env.options.cache_folder_separator;

							if (util.isPhp() && env.options.request_password_email) {
								$("#request-password").off("click").on("click", util.showPasswordRequestForm);
								$("#password-request-form").off("submit").on(
									"submit",
									function() {
										let name = $("#form-name").val().trim();
										let email = $("#form-email").val().trim();
										let identity = $("#form-identity").val().trim();

										if (name && email && identity) {
											// alert(location.href.substr(0, - location.hash) + "?name=" + encodeURI($("#form-name").val()) + "&email=" + encodeURI($("#form-email").val()) + "&identity=" + encodeURI($("#form-identity").val()) + location.hash);
											var newHref = // location.href.substr(0, - location.hash) +
											 					"?siteurl=" + encodeURIComponent(location.href) +
																"&name=" + encodeURIComponent(name) +
																"&email=" + encodeURIComponent(email) +
																"&identity=" + encodeURIComponent(identity) +
																location.hash;
											$("#auth-text").stop().fadeOut(1000);
											$("#sending-email").stop().fadeIn(
												1000,
												function() {
													MenuFunctions.sendEmail(newHref, "#sending-email");
												}
											);
										} else {
											$("#please-fill").css("display", "table");
											$("#please-fill").fadeOut(5000);
										}
										return false;
									}
								);
							} else {
								$("#request-password").hide();
							}

							$("#padlock img").attr("alt", util._t("#padlock-img-alt-text"));

							// decide what format to use for cache images
							env.options.format = "jpg";
							for (let i = 0; i < env.options.cache_images_formats.length; i ++) {
								let format = env.options.cache_images_formats[i];
								if ($("html").hasClass(format) || ! $("html").hasClass("not" + format)) {
									env.options.format = format;
									break;
								}
							}

							// WARNING: do not initialize the search root album, the app must read it from its json file!
							util.initializeOrGetMapRootAlbum();
							util.initializeSelectionRootAlbum();

							env.searchAlbum = new Album();
							env.searchAlbum.includedFilesByCodesSimpleCombination = new IncludedFiles();
							// $.executeAfterEvent(
							// 	"otherJsFilesLoadedEvent",
							// 	function() {
							// 		env.mapAlbum = util.initializeMapAlbum();
							// 		env.selectionAlbum = util.initializeSelectionAlbum();
							// 	}
							// );

							if (env.options.version !== undefined)
								$("#powered-by").attr("title", util._t("#software-version") + ": " + env.options.version + " - " + util._t("#json-version") + ": " + env.options.json_version.toString());
							// }

							$.triggerEvent("optionsLoadedEvent");

							resolve_getOptions();
						},
						function(jqXHR, textStatus, errorThrown) {
							if (errorThrown === "Not Found") {
								reject_getOptions();
							}
						}
					);
				}
			);
		}
	};


	/* make static methods callable as member functions */
	PhotoFloat.prototype.getAlbum = PhotoFloat.getAlbum;
	PhotoFloat.prototype.convertHashToCacheBase = PhotoFloat.convertHashToCacheBase;
	PhotoFloat.prototype.getStopWords = PhotoFloat.getStopWords;
	PhotoFloat.prototype.hasProtectedContent = PhotoFloat.hasProtectedContent;
	PhotoFloat.prototype.getJsonFile = PhotoFloat.getJsonFile;
	PhotoFloat.prototype.getMediaAndPositions = PhotoFloat.getMediaAndPositions;
	PhotoFloat.prototype.returnDecodedHash = PhotoFloat.returnDecodedHash;
	PhotoFloat.prototype.decodeMapCacheBase = PhotoFloat.decodeMapCacheBase;
	PhotoFloat.prototype.convertCacheBaseToId = PhotoFloat.convertCacheBaseToId;
	PhotoFloat.prototype.decodeSearchCacheBase = PhotoFloat.decodeSearchCacheBase;
	PhotoFloat.prototype.encodeSearchCacheBase = PhotoFloat.encodeSearchCacheBase;

	/* expose class globally */
	window.PhotoFloat = PhotoFloat;
}());
