(function() {

	var phFl = new PhotoFloat();
	var util = new Utilities();
	var menuF = new MenuFunctions();
	var pS;
	// $(document).ready(
	// 	function() {
	$.executeAfterEvent(
		"pinchSwipeFunctionsLoadedEvent",
		function() {
			pS = new PinchSwipe();
			$.triggerEvent("pinchSwipeOkInSingleMediaMethodsEvent");
		}
	);
	// 	}
	// );
	var tF = new TopFunctions();

	SingleMedia.prototype.addParent = function(album) {
		// add parent album
		if (! this.hasOwnProperty("parentAlbum"))
			this.parentAlbum = album;
	};

	SingleMedia.prototype.clone = function() {
		return new SingleMedia(util.cloneObject(this));
	};

	SingleMedia.prototype.cloneAndDeleteParent = function() {
		let clonedSingleMedia = this.clone();
		delete clonedSingleMedia.parentAlbum;
		return clonedSingleMedia;
	};

	SingleMedia.prototype.transformForPositions = function() {
		return new SingleMediaInPositions(
			{
				name: this.albumMediaPath(),
				// name: util.pathJoin([this.albumName, this.name]),
				cacheBase: this.cacheBase,
				foldersCacheBase: this.foldersCacheBase
			}
		);
	};

	SingleMedia.prototype.generatePositionAndMedia = function() {
		return new PositionAndMedia(
			{
				'lat' : parseFloat(this.metadata.latitude),
				'lng': parseFloat(this.metadata.longitude),
				'mediaList': [this.transformForPositions()]
			}
		);
	};

	SingleMedia.prototype.isEqual = function(otherMedia) {
		return otherMedia !== null && this.foldersCacheBase === otherMedia.foldersCacheBase && this.cacheBase === otherMedia.cacheBase;
	};

	SingleMedia.prototype.hasGpsData = function() {
		return env.options.expose_image_positions && this.metadata.latitude !== undefined && this.metadata.longitude !== undefined;
	};

	SingleMedia.prototype.swipeRight = function() {
		var self = this;
		if (env.currentMedia && env.currentMedia.isAudio() && $("audio#media-center").length && ! $("audio#media-center")[0].paused)
			// stop the audio, otherwise it keeps playing
			$("audio#media-center")[0].pause();
		if (env.currentMedia && env.currentMedia.isVideo() && $("video#media-center").length && ! $("video#media-center")[0].paused)
			// stop the video, otherwise it keeps playing
			$("video#media-center")[0].pause();
		$("#media-box-container").off('webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd').on(
			'webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd',
			function() {
				$("#media-box-container").off('webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd');
				$("#media-box-container").css("transition-duration", "0s");

				// remove right image and move html code to left side
				$(".media-box#right").remove();
				$(".media-box#center").attr('id', 'right');
				$(".media-box#left").attr('id', 'center');
				$("#media-center").attr('id', 'media-right');
				$("#media-left").attr('id', 'media-center');
				util.mediaBoxGenerator('left');
				$(".media-box#left").css("width", $(".media-box#center").attr('width')).css("height", $(".media-box#center").attr('height'));

				window.location.href = util.encodeHash(env.currentAlbum.cacheBase, self.cacheBase, self.foldersCacheBase, env.hashComponents.collectedAlbumCacheBase, env.hashComponents.collectionCacheBase);
			}
		);

		$("#pinch-container").addClass("hidden");
		pS.swipeMedia(0);
	};

	SingleMedia.prototype.swipeLeft = function() {
		var self = this;
		if (env.currentMedia && env.currentMedia.isAudio() && $("audio#media-center").length && ! $("audio#media-center")[0].paused)
			// stop the audio, otherwise it keeps playing
			$("audio#media-center")[0].pause();
		if (env.currentMedia && env.currentMedia.isVideo() && $("video#media-center").length && ! $("video#media-center")[0].paused)
			// stop the video, otherwise it keeps playing
			$("video#media-center")[0].pause();
		$("#media-box-container").off('webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd').on(
			'webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd',
			function() {
				$("#media-box-container").off('webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd');
				$("#media-box-container").css("transition-duration", "0s");

				// remove left image and move html code to right side
				$(".media-box#left").remove();
				$(".media-box#center").attr('id', 'left');
				$(".media-box#right").attr('id', 'center');
				$("#media-center").attr('id', 'media-left');
				$("#media-right").attr('id', 'media-center');
				util.mediaBoxGenerator('right');
				$(".media-box#right").css("width", $(".media-box#center").attr('width')).css("height", $(".media-box#center").attr('height'));
				// this translation avoid the flickering when inserting the right image into the DOM
				$("#media-box-container").css("transform", "translate(-" + env.windowWidth + "px, 0px)");

				window.location.href = util.encodeHash(env.currentAlbum.cacheBase, self.cacheBase, self.foldersCacheBase, env.hashComponents.collectedAlbumCacheBase, env.hashComponents.collectionCacheBase);
			}
		);

		$("#pinch-container").addClass("hidden");
		pS.swipeMedia(env.windowWidth * 2);
	};

	SingleMedia.prototype.isInsideAlbum = function(album) {
			// check whether the media is inside the given album
		return (
			util.isMapCacheBase(album.cacheBase) &&
			this.isInMapAlbum(album) ||
			util.isSearchCacheBase(album.cacheBase) &&
			this.isInFoundMedia(album) ||
			util.isSelectionCacheBase(album.cacheBase) &&
			this.isInsideSelection(album) ||
			util.isAnyRootCacheBase(album.cacheBase) && ! util.isCollectionCacheBase(album.cacheBase) || (
				this.foldersCacheBase.startsWith(album.cacheBase) ||
				this.hasOwnProperty("dayAlbumCacheBase") && this.dayAlbumCacheBase.startsWith(album.cacheBase) ||
				this.hasOwnProperty("gpsAlbumCacheBase") && this.gpsAlbumCacheBase.startsWith(album.cacheBase)
			)
		);
	};

	SingleMedia.prototype.isInsideAlbumToSearchIn = function(searchAlbum) {
			// check whether the media is inside the album defined by options.searchedCacheBase
		let searchObject = searchAlbum.search;
		let albumToSearchIn = env.cache.getAlbum(searchObject.searchedCacheBase);
		return this.isInsideAlbum(albumToSearchIn);
	};

	SingleMedia.prototype.isInMapAlbum = function(mapAlbum = env.mapAlbum) {
		if (! util.somethingIsInMapAlbum(mapAlbum))
			return false;

		var index = mapAlbum.media.findIndex(aSingleMedia => aSingleMedia.isEqual(this));
		if (index > -1)
			return true;
		else
			return false;
	};

	SingleMedia.prototype.isSearched = function() {
		if (! util.somethingIsSearched())
			return false;

		var index = env.searchAlbum.media.findIndex(aSingleMedia => aSingleMedia.isEqual(this));
		if (index > -1)
			return true;
		else
			return false;
	};

	SingleMedia.prototype.isInFoundSubalbums = function(album) {
		if (! util.somethingIsSearched(album))
			return false;

		let subalbum;
		subalbum = album.subalbums.find(aSubalbum => this.foldersCacheBase.startsWith(aSubalbum.cacheBase));
		if (typeof subalbum !== "undefined")
			return subalbum;
		else
			return false;
	};

	SingleMedia.prototype.isInFoundMedia = function(album) {
		// if (! util.somethingIsSearched(album))
		// 	return false;

		let singleMedia = album.media.find(aSingleMedia => this.isEqual(aSingleMedia));
		if (typeof singleMedia !== "undefined")
			return singleMedia;
		else
			return false;
	};

	SingleMedia.prototype.isSelected = function(selectionAlbum = env.selectionAlbum) {
		if (Utilities.absolutelyNothingIsSelected(selectionAlbum))
			return false;

		var index = selectionAlbum.media.findIndex(aSingleMedia => aSingleMedia.isEqual(this));
		if (index > -1)
			return true;
		else
			return false;
	};

	SingleMedia.prototype.isInsideSelection = function(selectionAlbum = env.selectionAlbum) {
		var self = this;
		if (
			self.isInsideSelectedAlbums(selectionAlbum) || selectionAlbum.media.find(singleMedia => singleMedia.isEqual(self)) !== undefined
		) {
			return true;
		} else {
			return false;
		}
	};

	SingleMedia.prototype.isInsideSelectedAlbums = function(selectionAlbum = env.selectionAlbum) {
		var self = this;
		if (
			typeof selectionAlbum !== "undefined" &&
			selectionAlbum.subalbums.some(
				selectedAlbum =>
					self.foldersCacheBase.startsWith(selectedAlbum.cacheBase) ||
					self.hasOwnProperty("dayAlbumCacheBase") && self.dayAlbumCacheBase.startsWith(selectedAlbum.cacheBase) ||
					self.hasOwnProperty("gpsAlbumCacheBase") && self.gpsAlbumCacheBase.startsWith(selectedAlbum.cacheBase)
			)
		) {
			return true;
		} else {
			return false;
		}
	};

	SingleMedia.prototype.chooseReducedImage = function(containerObject, fullScreenStatus) {
		var chosenMedia, reducedWidth, reducedHeight;
		var mediaWidth = this.width(), mediaHeight = this.height();
		var mediaSize = Math.max(mediaWidth, mediaHeight);
		var mediaRatio = mediaWidth / mediaHeight, containerRatio;
		var choosenSize;

		if (this.needsAutoRotation()) {
			[mediaWidth, mediaHeight] = [mediaHeight, mediaWidth];
		}

		chosenMedia = this.fullSizeMediaPath();
		if (chosenMedia === this.albumMediaPath() || chosenMedia === this.fullSizeCopyMediaPath())
			choosenSize = 0;
		else
			choosenSize = env.options.reduced_sizes[0];

		if (containerObject === null) {
			// try with what is more probable to be the container
			if (fullScreenStatus)
				containerObject = $(window);
			else {
				containerObject = $(".media-box#center .media-box-inner");
			}
		}

		var containerWidth = containerObject.width();
		var containerHeight = containerObject.height();
		containerRatio = containerObject.width() / containerObject.height();

		if (
			mediaRatio >= containerRatio && mediaWidth <= containerWidth * env.devicePixelRatio ||
			mediaRatio < containerRatio && mediaHeight <= containerHeight * env.devicePixelRatio
		) {
			// the original media is smaller than the container, use it
		} else {
			for (var i = 0; i < env.options.reduced_sizes.length; i++) {
				if (env.options.reduced_sizes[i] < mediaSize) {
					if (mediaWidth > mediaHeight) {
						reducedWidth = env.options.reduced_sizes[i];
						reducedHeight = env.options.reduced_sizes[i] * mediaHeight / mediaWidth;
					} else {
						reducedHeight = env.options.reduced_sizes[i];
						reducedWidth = env.options.reduced_sizes[i] * mediaWidth / mediaHeight;
					}

					if (
						mediaRatio > containerRatio && reducedWidth < containerWidth * env.devicePixelRatio ||
						mediaRatio < containerRatio && reducedHeight < containerHeight * env.devicePixelRatio
					)
						break;
				}
				chosenMedia = this.reducedMediaPath(env.options.reduced_sizes[i]);
				choosenSize = env.options.reduced_sizes[i];
			}
		}
		return [chosenMedia, choosenSize];
	};

	SingleMedia.prototype.chooseMediaReduction = function(id, fullScreenStatus) {
		// chooses the proper reduction to use depending on the container size
		var containerObject, mediaSrc;
		var mediaSize = Math.max(this.width(), this.height());

		if (this.isVideo() || this.isAudio()) {
			mediaSrc = this.reducedMediaPath();
		} else if (this.isImage()) {
			if (fullScreenStatus && Modernizr.fullscreen)
				containerObject = $(window);
			else
				containerObject = $(".media-box#" + id + " .media-box-inner");

			[mediaSrc, mediaSize] = this.chooseReducedImage(containerObject, fullScreenStatus);
		}

		return [mediaSrc, mediaSize];
	};

	SingleMedia.prototype.needsAutoRotation = function() {
		if (this.isAudio())
			return false;
		var containerWidth = env.windowWidth;
		var containerHeight = env.windowHeight;

		return (
			env.slideshowId > 0 &&
			env.slideshowAutoRotation && (
				containerWidth < containerHeight && this.width() > this.height() ||
				containerWidth > containerHeight && this.width() < this.height()
			)
		);
	};

	SingleMedia.prototype.toggleSingleMediaSelection = function(selectionAlbum) {
		let self = this;
		$.executeAfterEvent(
			"otherJsFilesLoadedEvent",
			function() {
				if (self.isSelected(selectionAlbum)) {
					self.removeFromSelection(selectionAlbum);
				} else {
					self.addToSelection(selectionAlbum);
				}
			}
		);
	};

	SingleMedia.prototype.width = function() {
		return (
			this.isAudio() ? (
				env.options.reduced_size_for_sharing ? env.options.reduced_size_for_sharing : env.options.album_thumb_size
			) : this.metadata.size[0]
		);
	};

	SingleMedia.prototype.height = function() {
		return this.isAudio() ? (
			env.options.reduced_size_for_sharing ? env.options.reduced_size_for_sharing : env.options.album_thumb_size
		) : this.metadata.size[1];
	};

	SingleMedia.prototype.createMediaHtml = function(album, id, fullScreenStatus) {
		// creates a media element that can be inserted in DOM (e.g. with append/prepend methods)

		// the actual sizes of the image
		var mediaSrc, mediaElement, containerObject;
		var mediaWidthOriginal = this.width();
		var mediaHeightOriginal = this.height();
		var mediaWidth = mediaWidthOriginal;
		var mediaHeight = mediaHeightOriginal;
		var singleMediaName, singleMediaTitle;
		var attrWidth = mediaWidth, attrHeight = mediaHeight;
		var mediaSize = 0;

		if (fullScreenStatus && Modernizr.fullscreen)
			containerObject = $(window);
		else
			containerObject = $(".media-box#" + id + " .media-box-inner");

		if (this.isImage()) {
			[mediaSrc, mediaSize] = this.chooseReducedImage(containerObject, fullScreenStatus);
			mediaElement = $('<img/>');
			if (env.options.expose_image_dates && env.currentAlbum.isFolder()) {
				mediaElement.attr("title", this.exifDate);
			} else {
				[singleMediaName, singleMediaTitle] = this.nameAndTitleForShowing(true);
				mediaElement.attr("title", util.pathJoin([this.albumName, singleMediaName]));
			}
		} else if (this.isAudio()) {
			mediaSrc = this.reducedMediaPath();
			mediaElement = $('<audio/>', {controls: true });
		} else if (this.isVideo()) {
			mediaSrc = this.reducedMediaPath();
			mediaElement = $('<video/>', {controls: true });
		}

		if (mediaSize) {
			// correct phisical width and height according to reduction sizes
			if (mediaWidth > mediaHeight) {
				attrWidth = mediaSize;
				attrHeight = Math.round(mediaSize * mediaHeightOriginal / mediaWidthOriginal);
			} else {
				attrHeight = mediaSize;
				attrWidth = Math.round(mediaSize * mediaWidthOriginal / mediaHeightOriginal);
			}
		}

		if (this.needsAutoRotation()) {
			[mediaWidth, mediaHeight] = [mediaHeight, mediaWidth];
		}

		mediaElement
			.attr("id", "media-" + id)
			.attr("alt", singleMediaTitle);
		if (! this.isAudio())
			mediaElement
				.attr("width", attrWidth)
				.attr("height", attrHeight)
				.attr("ratio", mediaWidth / mediaHeight);
		if (
			! env.options.save_data ||
			id === "center" ||
			util.isLoaded(mediaSrc) ||
			id === "right"
		) {
			mediaElement.attr("src", encodeURI(mediaSrc));
		} else {
			mediaElement.attr("data-src", encodeURI(mediaSrc));
			mediaElement.attr("src", "img/image-placeholder.jpg");
		}

		return mediaElement[0].outerHTML;
	};

	SingleMedia.prototype.createMediaLinkTag = function(mediaSrc) {
		// creates a link tag to be inserted in <head>

		if (this.isAudio()) {
			return '<link rel="audio_src" href="' + encodeURI(mediaSrc) + '" />';
		} else if (this.isVideo()) {
			return '<link rel="video_src" href="' + encodeURI(mediaSrc) + '" />';
		} else if (this.isImage()) {
			return '<link rel="image_src" href="' + encodeURI(mediaSrc) + '" />';
		}
	};

	SingleMedia.prototype.chooseTriggerEvent = function() {
		// choose the event that must trigger the scale function

		if (this.isImage()) {
			return "load";
		} else if (this.isAudio() || this.isVideo()) {
			return "loadstart";
		}
	};

	SingleMedia.prototype.isImage = function() {
		return this.mimeType.startsWith("image/");
	};

	SingleMedia.prototype.isAudio = function() {
		return this.mimeType.startsWith("audio/");
	};

	SingleMedia.prototype.isVideo = function() {
		return this.mimeType.startsWith("video/");
	};

	SingleMedia.prototype.fullSizeMediaPath = function(format = null) {
		if (! format) {
			if (this.isImage())
				format = env.options.format;
			else if (this.isAudio())
				format = "mp3";
			else if (this.isVideo())
				format = "mp4";
		}
		if (
			this.fileSizes.hasOwnProperty("original") &&
			this.fileSizes.original.imagesAudiosVideosTotal() &&
			! env.options.browser_unsupported_mime_types.includes(this.mimeType)
		)
			return this.albumMediaPath();
		else if (
			this.fileSizes.hasOwnProperty(format) &&
			this.fileSizes[format].hasOwnProperty(0) &&
			this.fileSizes[format][0].imagesAudiosVideosTotal()
		)
			return this.fullSizeCopyMediaPath(format);
		else if (
			this.fileSizes.jpg.hasOwnProperty(0) &&
			this.fileSizes.jpg[0].imagesAudiosVideosTotal()
		)
			return this.fullSizeCopyMediaPath("jpg");
		else
			return this.reducedMediaPath(env.options.reduced_sizes[0], format);

		// if (
		// 	env.options.expose_full_size_media_in_cache ||
		// 	env.options.expose_original_media && (
		// 		(
		// 			this.mimeType === "image/webp" && ! $("html").hasClass("webp") ||
		// 			this.mimeType === "image/avif" && ! $("html").hasClass("avif") ||
		// 			this.mimeType === "image/jxl" && ! $("html").hasClass("jxl") ||
		// 			env.options.browser_unsupported_mime_types.includes(this.mimeType)
		// 		) && (
		// 			! ["image/webp", "image/avif", "image/jxl"].includes(this.mimeType) ||
		// 			env.options.browser_unsupported_video_codecs.includes(this.codec) ||
		// 			! env.options.expose_image_metadata ||
		// 			! env.options.expose_image_dates ||
		// 			! env.options.expose_image_positions && this.hasGpsData()
		// 		)
		// 	) || (
		// 		env.options.expose_full_size_media_in_cache ||
		// 		env.options.expose_original_media
		// 	) && (
		// 		format === "jpg" && this.mimeType !== "image/jpeg" ||
		// 		format === "mp3" && this.mimeType !== "audio/mp3" ||
		// 		format === "mp4" && this.mimeType !== "video/mp4"
		// 	)
		// ) {
		// 	return this.fullSizeCopyMediaPath(format);
		// } else if (env.options.expose_original_media) {
		// 	return this.albumMediaPath();
		// } else {
		// 	return this.reducedMediaPath(env.options.reduced_sizes[0], format);
		// }
	};

	SingleMedia.prototype.albumMediaPath = function() {
		return util.pathJoin([this.albumName, this.name]);
	};

	SingleMedia.prototype.fullSizeCopyMediaPath = function(format = null) {
		var foldersCacheBase = this.foldersCacheBase;
		foldersCacheBase = foldersCacheBase.substring(env.options.folders_string.length + 1);
		if (foldersCacheBase)
			foldersCacheBase += env.options.cache_folder_separator;
		return util.pathJoin(
			[
				"cache",
				this.cacheSubdir,
				(
					foldersCacheBase + this.cacheBase + env.options.cache_folder_separator + "fs." + (
						this.isImage() ?
						(format !== null ? format : env.options.format) :
						this.isAudio() ?
						"mp3" :
						"mp4" // this.isVideo() is true
					)
				)
			]
		);
	};

	SingleMedia.prototype.thumbnailPath = function(size, type) {
		var suffix = env.options.cache_folder_separator, cacheBase, rootString = "root-";

		var mediaWidth = this.width();
		var mediaHeight = this.height();

		var actualSize = size;
		if (env.devicePixelRatio > 1) {
			actualSize = parseInt(Math.round(actualSize * env.options.mobile_thumbnail_factor));
		}
		if (
			type === "singleMedia" &&
			! env.options.only_square_thumbnails &&
			env.options.media_thumb_type.indexOf("fixed_height") > -1 &&
			mediaWidth > mediaHeight
		)
			actualSize = parseInt(Math.round(actualSize * mediaWidth / mediaHeight));

		suffix += actualSize.toString();
		if (
			env.options.only_square_thumbnails ||
			mediaWidth === mediaHeight ||
			type === "subalbum" && env.options.album_thumb_type.indexOf("square") > -1 ||
			type === "singleMedia" && env.options.media_thumb_type.indexOf("square") > -1
		)
			suffix += "sq";
		suffix += "." + env.options.format;

		cacheBase = this.foldersCacheBase + env.options.cache_folder_separator + this.cacheBase + suffix;
		if (cacheBase.startsWith(rootString))
			cacheBase = cacheBase.substring(rootString.length);
		else {
			if (util.isFolderCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.foldersStringWithTrailingSeparator.length);
			else if (util.isByDateCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.byDateStringWithTrailingSeparator.length);
			else if (util.isByGpsCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.byGpsStringWithTrailingSeparator.length);
			else if (util.isSearchCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.bySearchStringWithTrailingSeparator.length);
			else if (util.isSelectionCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.bySelectionStringWithTrailingSeparator.length);
			else if (util.isMapCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.byMapStringWithTrailingSeparator.length);
		}
		if (this.cacheSubdir)
			return util.pathJoin(["cache", this.cacheSubdir, cacheBase]);
		else
			return util.pathJoin(["cache", cacheBase]);
	};

	SingleMedia.prototype.reducedMediaPath = function(size = null, format = null) {
		if (format === null)
			format = env.options.format;
		// this function is called with null size for audios and videos
		var suffix = env.options.cache_folder_separator, cacheBase, rootString = "root-";
		if (this.isImage()) {
			suffix += size.toString();
			if (this.metadata.size[0] === this.metadata.size[1])
				suffix += "sq";
			suffix += "." + format;
		} else if (this.isAudio()) {
			suffix += "transcoded.mp3";
		} else if (this.isVideo()) {
			suffix += "transcoded.mp4";
		}

		cacheBase = this.foldersCacheBase + env.options.cache_folder_separator + this.cacheBase + suffix;
		if (cacheBase.startsWith(rootString))
			cacheBase = cacheBase.substring(rootString.length);
		else {
			if (util.isFolderCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.foldersStringWithTrailingSeparator.length);
			else if (util.isByDateCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.byDateStringWithTrailingSeparator.length);
			else if (util.isByGpsCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.byGpsStringWithTrailingSeparator.length);
			else if (util.isSearchCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.bySearchStringWithTrailingSeparator.length);
			else if (util.isSelectionCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.bySelectionStringWithTrailingSeparator.length);
			else if (util.isMapCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.byMapStringWithTrailingSeparator.length);
		}
		if (this.cacheSubdir)
			return util.pathJoin(["cache", this.cacheSubdir, cacheBase]);
		else
			return util.pathJoin(["cache", cacheBase]);
	};

	SingleMedia.prototype.sharingMediaPath = function(square = false) {
		var suffix = env.options.cache_folder_separator, cacheBase, rootString = "root-";
		if (env.options.reduced_size_for_sharing)
			suffix += env.options.reduced_size_for_sharing.toString();
		else
			suffix += env.options.album_thumb_size.toString();
		if (square || this.width === this.height)
			suffix += "sq";
		suffix += "." + "jpg";

		cacheBase = this.foldersCacheBase + env.options.cache_folder_separator + this.cacheBase + suffix;
		if (cacheBase.startsWith(rootString))
			cacheBase = cacheBase.substring(rootString.length);
		else {
			if (util.isFolderCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.foldersStringWithTrailingSeparator.length);
			else if (util.isByDateCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.byDateStringWithTrailingSeparator.length);
			else if (util.isByGpsCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.byGpsStringWithTrailingSeparator.length);
			else if (util.isSearchCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.bySearchStringWithTrailingSeparator.length);
			else if (util.isSelectionCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.bySelectionStringWithTrailingSeparator.length);
			else if (util.isMapCacheBase(cacheBase))
				cacheBase = cacheBase.substring(env.options.byMapStringWithTrailingSeparator.length);
		}
		if (this.cacheSubdir)
			return util.pathJoin(["cache", this.cacheSubdir, cacheBase]);
		else
			return util.pathJoin(["cache", cacheBase]);
	};

	SingleMedia.prototype.scale = function(event) {
		// this function works on the img tag identified by event.data.id
		// it adjusts width, height and position so that it fits in its parent (<div class="media-box-inner">, or the whole window)
		// and centers vertically
		var self = this;
		return new Promise(
			function(resolve_scale) {
				var mediaElement, containerObject;
				var containerHeight = $(window).innerHeight(), containerWidth = $(window).innerWidth(), containerRatio;

				if (self.isAudio())
					resolve_scale([containerHeight, containerWidth]);

				var mediaWidthOriginal = self.width();
				var mediaHeightOriginal = self.height();
				var mediaWidth = mediaWidthOriginal;
				var mediaHeight = mediaHeightOriginal;
				var attrWidth = mediaWidthOriginal;
				var attrHeight = mediaHeightOriginal;
				var id = event.data.id;
				var heightForMedia, heightForMediaAndTitle, titleHeight;

				util.setTitleOptions(event.data.id);
				util.setMediaOptions();

				if ($("#thumbs").is(":visible")) {
					// the last 6 is the top border, which is yellow for the current thumbnail when in media view
					let height = env.options.media_thumb_size + util.horizontalScrollBarThickness($("#thumbs")[0]) + 6;
					$("#thumbs").css("height", height.toString() + "px");
				}

				env.windowWidth = $(window).innerWidth();
				heightForMediaAndTitle = util.mediaBoxContainerHeight();

				// widths must be set before calculating title height
				$("#media-box-container").css("width", env.windowWidth * 3).css("transform", "translate(-" + env.windowWidth + "px, 0px)");

				$(".media-box#" + id).css("width", env.windowWidth);
				$(".media-box#" + id + " .media-box-inner").css("width", env.windowWidth);
				$(".media-box#" + id).show();
				if ($(".media-box#" + id + " .title").is(":visible"))
				// if ($(".media-box#" + id + " .title").css("display") !== "none")
					titleHeight = $(".media-box#" + id + " .title").outerHeight();
				else
					titleHeight = 0;

				heightForMedia = heightForMediaAndTitle - titleHeight;
				$("#media-box-container").css("height", heightForMediaAndTitle);
				$(".media-box#" + id).css("height", heightForMediaAndTitle);
				$(".media-box#" + id + " .media-box-inner").css("height", heightForMedia);
				$(".media-box#" + id).show();

				if (self.isImage())
					mediaElement = $(".media-box#" + id + " .media-box-inner img");
				else if (self.isAudio())
					mediaElement = $(".media-box#" + id + " .media-box-inner audio");
				else if (self.isVideo())
					mediaElement = $(".media-box#" + id + " .media-box-inner video");

				if (self.needsAutoRotation()) {
					[mediaWidth, mediaHeight] = [mediaHeight, mediaWidth];
				}

				if (env.fullScreenStatus && Modernizr.fullscreen)
					containerObject = $(window);
				else {
					containerObject = $(".media-box#" + id + " .media-box-inner");
				}

				containerHeight = heightForMedia;
				containerRatio = containerWidth / containerHeight;

				$.executeAfterEvent(
					"pinchSwipeOkInSingleMediaMethodsEvent",
					function() {
						if (self.isImage()) {
							let [imageSrc, mediaSize] = self.chooseReducedImage(containerObject, env.fullScreenStatus);
							let previousSrc = mediaElement.attr("src");

							// chooseReducedImage() sets mediaSize to 0 if it returns the original media
							if (mediaSize) {
								if (mediaWidth > mediaHeight) {
									attrWidth = mediaSize;
									attrHeight = Math.round(mediaHeight / mediaWidth * attrWidth);
								} else {
									attrHeight = mediaSize;
									attrWidth = Math.round(mediaWidth / mediaHeight * attrHeight);
								}
							}

							if (
								encodeURI(imageSrc) !== previousSrc &&
								env.currentZoom === env.initialZoom ||
								self.needsAutoRotation() && ! self.isRotated(mediaElement) ||
								! self.needsAutoRotation() && self.isRotated(mediaElement)
							) {
								// resizing had the effect that a different reduction has been choosed

								$("link[rel=image_src]").remove();
								$('link[rel="video_src"]').remove();
								$("head").append("<link rel='image_src' href='" + encodeURI(imageSrc) + "' />");
								mediaElement.attr("width", attrWidth).attr("height", attrHeight);
								if (
									! env.options.save_data ||
									id === "center" ||
									util.isLoaded(imageSrc) ||
									// added next line in order not the show the placeholder when beginning the slideshow
									id === "right"
								) {
									mediaElement.attr("src", encodeURI(imageSrc));
									if (mediaElement.attr("data-src"))
										mediaElement.removeAttr("data-src");
								} else {
									mediaElement.attr("data-src", encodeURI(imageSrc));
									mediaElement.attr("src", "img/image-placeholder.jpg");
								}
							}
						}

						mediaElement.show();

						if (id === "center") {
							// util.setFinalOptions(self, env.currentZoom === env.initialZoom && ! event.data.pinch);

							if ($("#thumbs").is(":visible")) {
								util.scrollBottomMediaToHighlightedThumb();
							}
						}
						resolve_scale([containerHeight, containerWidth]);

						$("#loading").hide();
					}
				);
			}
		);
	};

	SingleMedia.prototype.generateCaptionsForCollections = function(album) {
		var raquo = " <span class='gray'>&raquo;</span> ";
		var folderArray = album.cacheBase.split(env.options.cache_folder_separator);

		var secondLine = '';
		if (album.isByDate()) {
			secondLine += "<span class='gray'>(";
			if (folderArray.length === 2) {
				secondLine += util._t("#in-year-album") + " ";
			} else if (folderArray.length === 3) {
				secondLine += util._t("#in-month-album") + " ";
			} else if (folderArray.length === 4) {
				secondLine += util._t("#in-day-album") + " ";
			} else {
				secondLine += util._t("#in-by-date-album-with-link", env.hashBeginning + env.options.by_date_string);
			}
			secondLine += "</span>";
			if (folderArray.length > 3)
				secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[3] + "'>" + util.dateElementForFolderName(folderArray, 3) + "</a>" + " ";
			if (folderArray.length > 2)
				secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[2] + "'>" + util.dateElementForFolderName(folderArray, 2) + "</a>" + " ";
			if (folderArray.length > 1)
				secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[1] + "'>" + util.dateElementForFolderName(folderArray, 1) + "</a>";
			secondLine += "<span class='gray'>)</span>";
		} else if (album.isByGps()) {
			for (let iCacheBase = 1; iCacheBase < album.ancestorsCacheBase.length; iCacheBase ++) {
				let albumName;
				if (album.ancestorsNames[iCacheBase] === "")
					albumName = util._t('.not-specified');
				else
					albumName = util.transformAltPlaceName(album.ancestorsNames[iCacheBase]);
				if (iCacheBase === 1)
					secondLine += "<span class='gray'>(" + util._t("#in-by-gps-album") + "</span> ";
				// let marker = "<marker>" + iCacheBase + "</marker>";
				// secondLine += marker;
				secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + albumName + "</a>";
				if (iCacheBase < album.ancestorsCacheBase.length - 1)
					secondLine += raquo;
				if (iCacheBase === album.ancestorsCacheBase.length - 1)
					secondLine += "<span class='gray'>)</span>";

				// secondLine = secondLine.replace(marker, "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + albumName + "</a>");
			}
			if (! secondLine)
				secondLine = "<span class='gray'>(" + util._t("#in-by-gps-album-with-link", env.hashBeginning + env.options.by_gps_string) + ")</span>";
		} else {
			if (album.ancestorsCacheBase.length === 1) {
				secondLine += "<span class='gray'>(" + util._t("#in-root-album-with-link", env.hashBeginning + env.options.folders_string) + ")</span>";
			}

			for (let iCacheBase = 1; iCacheBase < album.ancestorsCacheBase.length; iCacheBase ++) {
				if (iCacheBase === 0 && album.ancestorsCacheBase.length === 2) {
					secondLine += "<span class='gray'>(" + util._t("#regular-album") + ")</span>";
				} else {
					if (iCacheBase === 1)
						secondLine += "<span class='gray'>(" + util._t("#in-regular-album") + "</span> ";
					// let marker = "<marker>" + iCacheBase + "</marker>";
					// secondLine += marker;
					secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + album.ancestorsNames[iCacheBase] + "</a>";
					if (iCacheBase < album.ancestorsCacheBase.length - 1)
						secondLine += raquo;
					if (iCacheBase === album.ancestorsCacheBase.length - 1)
						secondLine += "<span class='gray'>)</span>";
					// secondLine = secondLine.replace(marker, "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + album.ancestorsNames[iCacheBase] + "</a>");
				}
			}
		}

		var [nameForShowing, titleForShowing] = this.nameAndTitleForShowing(true, true);
		var captionsForCollection = util.addSpanToFirstAndSecondLine(nameForShowing, secondLine);
		var captionForCollectionSorting = nameForShowing + env.options.cache_folder_separator + util.convertByDateAncestorNames(album.ancestorsNames).slice(1).reverse().join(env.options.cache_folder_separator).replace(/^0+/, '');
		return [captionsForCollection, captionForCollectionSorting, titleForShowing];
	};

	SingleMedia.prototype.generateCaptionsForPopup = function(album) {
		if (album.isSelection())
			[this.captionsForPopup, this.captionForPopupSorting] = [this.captionsForSelection, this.captionForSelectionSorting];
		else if (album.isSearch())
			[this.captionsForPopup, this.captionForPopupSorting] = [this.captionsForSearch, this.captionForSearchSorting];
		else
			[this.captionsForPopup, this.captionForPopupSorting, this.titleForShowing] = this.generateCaptionsForCollections(album);
	};

	SingleMedia.prototype.generateCaptionsForSelection = function(album) {
		if (album.isSearch())
			[this.captionsForSelection, this.captionForSelectionSorting] = [this.captionsForSearch, this.captionForSearchSorting];
		else if (album.isMap())
			[this.captionsForSelection, this.captionForSelectionSorting] = [this.captionsForPopup, this.captionForPopupSorting];
		else
			[this.captionsForSelection, this.captionForSelectionSorting, this.titleForShowing] = this.generateCaptionsForCollections(album);
	};

	SingleMedia.prototype.generateCaptionsForSearch = function(album) {
		if (album.isSelection())
			[this.captionsForSearch, this.captionForSearchSorting] = [this.captionsForSelection, this.captionForSelectionSorting];
		else if (album.isMap())
			[this.captionsForSearch, this.captionForSearchSorting] = [this.captionsForPopup, this.captionForPopupSorting];
		else
			[this.captionsForSearch, this.captionForSearchSorting, this.titleForShowing] = this.generateCaptionsForCollections(album);
	};

	SingleMedia.prototype.nameAndTitleForShowing = function(html = false, br = false) {
		var mediaName = '';
		var mediaTitle = '';
		if (this.metadata.hasOwnProperty("title") && this.metadata.title && this.metadata.title !== this.name) {
			mediaName = this.metadata.title;
			mediaTitle = this.name;
			if (! br) {
				// remove the tags fronm the title
				mediaName = mediaName.replace(/<[^>]*>?/gm, ' ');
			}

		} else {
			mediaName = this.name;
		}
		return [mediaName, mediaTitle];
	};

	SingleMedia.prototype.hasProperty = function(property) {
		return util.hasProperty(this.metadata, property);
	};

	SingleMedia.prototype.hasSomeDescription = function(property = null) {
		return util.hasSomeDescription(this, property);
	};

	SingleMedia.prototype.setDescription = function() {
		util.setDescription(this.metadata);
	};

	SingleMedia.prototype.chooseMediaThumbnail = function(thumbnailSize) {
		return this.thumbnailPath(thumbnailSize, "singleMedia");
	};

	SingleMedia.prototype.chooseSubalbumThumbnail	= function(thumbnailSize) {
		return this.thumbnailPath(thumbnailSize, "subalbum");
	};

	SingleMedia.prototype.isRotated = function(mediaElement) {
		var object = pS.getTransformParameters(mediaElement);
		return object.rotate === 90;
	};

	SingleMedia.prototype.mediaSelectBoxSelector = function(forPopup = false) {
		let firstPartOfSelector = env.mediaSelectBoxIdBeginning;
		if (util.isPopup() || forPopup)
			firstPartOfSelector = env.mediaMapSelectBoxIdBeginning;
		return "#" + firstPartOfSelector + phFl.convertCacheBaseToId(this.foldersCacheBase) + "-" + phFl.convertCacheBaseToId(this.cacheBase);
	};

	SingleMedia.prototype.show = function(album, id) {
		function loadNextPrevMedia(self, containerHeight, containerWidth) {
			$.executeAfterEvent(
				"pinchSwipeOkInSingleMediaMethodsEvent",
				function() {
					$("#pinch-in").off("click").on("click", pS.pinchIn);
					$("#pinch-out").off("click").on("click", pS.pinchOut);
				}
			);
			if (
				self.hasGpsData() || (
					util.isPhp() && env.options.user_may_suggest_location && env.options.request_password_email
				)
			) {
				$("#map-button-container").removeClass("hidden");
				$("#map-button-container").off("click").on(
					"click",
					{singleMedia: self, album: env.currentAlbum},
					function(ev, from) {
						// do not remove the from parameter, it is valored when the click is activated via the trigger() jquery function
						ev.stopPropagation();

						env.mapAlbum = util.initializeMapAlbum();
						// if (! env.map.clickHistory) {
						env.mapAlbum.map.clickAlbumCacheBase = phFl.convertHashToCacheBase(window.location.hash);
						env.mapAlbum.map.clickedSelector = "#map-button-container";
						env.mapAlbum.map.whatToGenerateTheMapFor = ev.data.singleMedia.cacheBase;
						// }
						env.mapAlbum.generateMapFromSingleMedia(ev.data.singleMedia, ev, from);
					}
				);
			} else {
				$("#map-button-container").addClass("hidden");
				$("#map-button-container").off("click");
			}

			$.executeAfterEvent(
				"imagesInPageLoadedEvent",
				function() {
					util.modifyCheckedState("#" + env.singleMediaSelectBoxId, self.isSelected());
				}
			);
			$("#" + env.singleMediaSelectBoxId).off("click").on(
				"click",
				{singleMedia: self},
				function(ev) {
					ev.stopPropagation();
					ev.preventDefault();
					let singleMediaSelector = "#" + env.singleMediaSelectBoxId;
					let otherSelector = ev.data.singleMedia.mediaSelectBoxSelector();

					if (Utilities.absolutelyNothingIsSelected())
						env.selectionAlbum = util.initializeSelectionAlbum();

					env.selectionAlbum.addToSelectionClickHistory(env.currentAlbum, otherSelector);

					if (ev.data.singleMedia.isSelected()) {
						// remove from selection
						ev.data.singleMedia.removeFromSelection();

						if (album.isSelection()) {
							if (util.absolutelyNothingIsSelected()) {
								env.selectionAlbum = util.initializeSelectionAlbum();
							} else {
								album.updateLocationHash(false, true);
							}
							window.location.hash = util.upHash();
						}
					} else {
						// add to selection
						ev.data.singleMedia.generateCaptionsForSelection(env.currentAlbum);
						ev.data.singleMedia.addToSelection(env.selectionAlbum);

						delete env.selectionAlbum.mediaSort;
						delete env.selectionAlbum.mediaReverseSort;
						env.selectionAlbum.sortAlbumsMedia();
					}

					if (! album.isSelection() || ! util.absolutelyNothingIsSelected()) {
						// update the selector
						util.modifyCheckedState(singleMediaSelector, ev.data.singleMedia.isSelected());
						if ($(otherSelector + " img").is(":visible")) {
							util.modifyCheckedState(otherSelector, ev.data.singleMedia.isSelected());
						}
					}
				}
			);

			if (! self.isAudio()) {
				$.executeAfterEvent(
					"pinchSwipeOkInSingleMediaMethodsEvent",
					pS.addMediaGesturesDetection
				);
			}

			if (! album.isSelection() || ! util.absolutelyNothingIsSelected()) {
				if (album.numVisibleMedia() > 1) {
					if (env.prevMedia)
						env.prevMedia.show(album, "left");
					if (env.nextMedia)
						env.nextMedia.show(album, "right");
				}

				$(window).off("resize").on(
					"resize",
					function() {
						if (env.slideshowId !== 1)
							// the resizing is because a slideshow
							util.resizeSingleMediaWithPrevAndNext(self, album);
					}
				);
			}
		}
		// end of loadNextPrevMedia auxiliary function

		//////////////////////////////////
		// beginning of SingleMedia show method body
		//////////////////////////////////
		var text, mediaSelector;
		var exposureTime, heightForMedia, heightForMediaAndTitle;
		var whatMedia;

		$("#downloading-media").hide();
		$(".media-box-inner").show();

		env.firstEscKey = true;

		if (id === "center") {
			env.mediaLink = util.encodeHash(env.currentAlbum.cacheBase, this.cacheBase, this.foldersCacheBase, env.hashComponents.collectedAlbumCacheBase, env.hashComponents.collectionCacheBase);
			$(".media-bar").show();
			whatMedia = this;
			util.setPrevNextVisibility();
		} else if (id === "left") {
			whatMedia = env.prevMedia;
		} else if (id === "right") {
			whatMedia = env.nextMedia;
		}

		if (id === "center") {
			$("#media-view").removeClass("hidden");
			$("#album-and-media-container").addClass("single-media");
			env.windowWidth = $(window).innerWidth();
			if (! env.currentAlbumIsAlbumWithOneMedia && ! env.options.hide_bottom_thumbnails) {
				if (
					$("#thumbs").html() === "" ||
					env.albumOfPreviousState && (
						env.isFromAuthForm ||
						env.albumOfPreviousState !== env.currentAlbum
					)
				) {
					env.currentAlbum.showMedia();
				} else {
					util.scrollBottomMediaToHighlightedThumb();
				}
			}

			env.isFromAuthForm = false;
			$("#powered-by").hide();

			$("#album-view").off('mousewheel').on('mousewheel', tF.scrollBottomThumbs);
		}

		var setTitlePromise = tF.setTitle(id, whatMedia, this);
		setTitlePromise.then(
			function titleSet(self) {
				$("#subalbums").addClass("hidden");

				heightForMediaAndTitle = util.mediaBoxContainerHeight();

				var titleHeight = 0;
				if ($(".media-box#" + id + " .title").is(":visible"))
					titleHeight = $(".media-box#" + id + " .title").outerHeight();

				heightForMedia = heightForMediaAndTitle - titleHeight;

				if (id === "center") {
					$("#media-box-container").css("width", env.windowWidth * 3).css("height", heightForMediaAndTitle);
					$("#media-box-container").css("transform", "translate(-" + env.windowWidth + "px, 0px)");
					$(".media-box").css("width", env.windowWidth).css("height", heightForMediaAndTitle);
					$(".media-box .media-box-inner").css("width", env.windowWidth).css("height", heightForMedia);
					$(".media-box").show();

					env.currentAlbum.media[env.currentMediaIndex].byDateName =
						util.pathJoin([env.currentAlbum.media[env.currentMediaIndex].dayAlbum, env.currentAlbum.media[env.currentMediaIndex].name]);
					if (env.currentAlbum.media[env.currentMediaIndex].hasOwnProperty("gpsAlbum"))
						env.currentAlbum.media[env.currentMediaIndex].byGpsName =
							util.pathJoin([env.currentAlbum.media[env.currentMediaIndex].gpsAlbum, env.currentAlbum.media[env.currentMediaIndex].name]);

					let visibleMedia = env.currentAlbum.visibleMedia();

					if (! env.currentAlbumIsAlbumWithOneMedia) {
						let currentVisibleMediaIndex = visibleMedia.findIndex(
							singleMedia => singleMedia.isEqual(env.currentAlbum.media[env.currentMediaIndex])
						);
						// find previous media
						let previousVisibleMediaIndex = currentVisibleMediaIndex -1;
						if (previousVisibleMediaIndex < 0)
							previousVisibleMediaIndex = visibleMedia.length - 1;
						env.prevMedia = visibleMedia[previousVisibleMediaIndex];
						env.prevMedia.byDateName = util.pathJoin([env.prevMedia.dayAlbum, env.prevMedia.name]);
						if (env.prevMedia.hasOwnProperty("gpsAlbum"))
							env.prevMedia.byGpsName = util.pathJoin([env.prevMedia.gpsAlbum, env.prevMedia.name]);

						// find next media
						let nextVisibleMediaIndex = currentVisibleMediaIndex + 1;
						if (nextVisibleMediaIndex > visibleMedia.length - 1)
							nextVisibleMediaIndex = 0;
						env.nextMedia = visibleMedia[nextVisibleMediaIndex];
						env.nextMedia.byDateName = util.pathJoin([env.nextMedia.dayAlbum, env.nextMedia.name]);
						if (env.nextMedia.hasOwnProperty("gpsAlbum"))
							env.nextMedia.byGpsName = util.pathJoin([env.nextMedia.gpsAlbum, env.nextMedia.name]);
					}
				}

				var mediaBoxInnerObject = $(".media-box#" + id + " .media-box-inner");
				// empty the img container: another image will be put in there

				if (self.isVideo() && ! util.videoOK()) {
					mediaBoxInnerObject.empty();
					util.addVideoUnsupportedMarker(id);
					if (id === "center")
						loadNextPrevMedia(self);
				} else {
					let newMediaObject;
					if (self.isImage()) {
						mediaSelector = ".media-box#" + id + " .media-box-inner img";
						newMediaObject = $("<img>");
					} else if (self.isAudio()) {
						mediaSelector = ".media-box#" + id + " .media-box-inner audio";
						newMediaObject = $("<audio>");
					} else if (self.isVideo()) {
						mediaSelector = ".media-box#" + id + " .media-box-inner video";
						newMediaObject = $("<video>");
					}

					let mediaSrc, mediaSize, mediaHtml;
					if (self.isImage() || self.isVideo() || self.isAudio()) {
						// TO DO: is the following line correct for videos?
						[mediaSrc, mediaSize] = self.chooseMediaReduction(id, env.fullScreenStatus);
					}
					mediaHtml = self.createMediaHtml(album, id, env.fullScreenStatus);

					let loadEvent = self.chooseTriggerEvent();

					if ($(mediaBoxInnerObject.html()).attr("src") !== $(mediaHtml).attr("src")) {
						// only replace the media-box-inner content if it's not yet there
						mediaBoxInnerObject.empty();
						mediaBoxInnerObject.show().append(mediaHtml);

						if (id === "center" && ! ($("link[rel=image_src]").length || $("link[rel=video_src]").length)) {
							// $("link[rel=image_src]").remove();
							// $('link[rel=video_src]').remove();
							$("head").append(self.createMediaLinkTag(mediaSrc));
						}
					}

					if (id === "center") {
						mediaBoxInnerObject.css("opacity", 1);
						self.setDescription();
					}

					// we use a trick in order to manage the loading of the image/video, from https://www.seancdavis.com/blog/wait-until-all-images-loaded/
					// the trick is to bind the event to a generic element not in the DOM, and to set its source after the onload event is bound
					newMediaObject.off(loadEvent).on(
						loadEvent,
						{
							id: id,
							resize: false,
						},
						function (event) {
							// $(mediaSelector).off(loadEvent);
							let scalePromise = self.scale(event);
							scalePromise.then(
								function([containerHeight, containerWidth]) {
									if (! self.isAudio())
										self.addRotation($(mediaSelector));
									if (id === "center") {
										$.executeAfterEvents(
											["socialShareKitLoadedEvent", "pinchSwipeOkInSingleMediaMethodsEvent", "imagesInPageLoadedEvent"],
											function() {
												util.setFinalOptions(self, false); // OK: scale promise (?)
											}
										);

										loadNextPrevMedia(self, containerHeight, containerWidth);
									}
								}
							);
						}
					);

					if (
						! env.options.save_data ||
						id === "center" ||
						util.isLoaded($(mediaSelector).attr("src")) ||
						id === "right"
					) {
						newMediaObject.attr("src", $(mediaSelector).attr("src"));
						util.setLoaded($(mediaSelector).attr("src"));
					} else {
						newMediaObject.attr("data-src", $(mediaSelector).attr("data-src"));
						newMediaObject.attr("src", $(mediaSelector).attr("src"));
						// newMediaObject.addClass("lazyload-next-prev");
						$(
							function() {
								newMediaObject.Lazy(
									{
										threshold: 0,
										appendScroll: $("#media-box-container"),
										afterLoad: function(element) {
											util.setLoaded(newMediaObject.attr("src"));
										}
									}
								);
							}
						);
					}
					if (id === "center") {
						if (! env.options.persistent_metadata) {
							$(".media-box .metadata").hide();
							$(".media-box .metadata-show").show();
							$(".media-box .metadata-hide").hide();
						}
					}
				}

				if (id === "center") {
					mediaBoxInnerObject.off('contextmenu click mousewheel');
					$(".media-box#center .media-box-inner .media-bar").off();
					$("#next").off();
					$("#prev").off();

					if (self.isImage()) {
						$.executeAfterEvent(
							"pinchSwipeOkInSingleMediaMethodsEvent",
							function() {
								mediaBoxInnerObject.off("mousewheel").on("mousewheel", pS.swipeOnWheel);
							}
						);
					}

					$(".media-box#center .media-box-inner .media-bar").off("click").on(
						"click",
						function(ev) {
							ev.stopPropagation();
						}
					).off("contextmenu").on(
						"contextmenu",
						function(ev) {
							ev.stopPropagation();
						}
					);

					if (env.currentAlbumIsAlbumWithOneMedia) {
						mediaBoxInnerObject.css('cursor', 'default');
					} else {
						$.executeAfterEvents(
							["pinchSwipeOkInSingleMediaMethodsEvent", "imagesInPageLoadedEvent"],
							function() {
								$("#next").show();
								$("#prev").show();

								$("audio#media-center").off("contextmenu");
								if (env.currentMedia.isAudio()) {
									$("audio#media-center").on(
										"contextmenu",
										function(ev) {
											if (! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey)
												ev.preventDefault();
											ev.stopPropagation();
											if ($("audio#media-center").length && ! $("audio#media-center")[0].paused)
												$("audio#media-center")[0].pause();
										}
									);
								}
								mediaBoxInnerObject.css('cursor', '').off("contextmenu").on(
									"contextmenu",
									function(ev) {
										ev.stopPropagation();
										if (
											env.prevMedia &&
											! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey &&
											env.currentZoom === env.initialZoom
										) {
											ev.preventDefault();
											env.prevMedia.swipeRight();
											return false;
										}
										// contextMenu = true;
										return true;
									}
								);
								// util.setFinalOptions(self, false);

								$("#prev").off("click").on("click", function(ev) {
									if (
										env.prevMedia &&
										ev.button === 0 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
									) {
										$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-pinch");
										if (env.currentMedia.isImage())
											pS.pinchOut(null, env.initialZoom);
										env.prevMedia.swipeRight();
										return false;
									}
									return true;
								});
								$("#next").off("click").on(
									"click",
									function(ev) {
										if (
											env.nextMedia &&
											ev.button === 0 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
										) {
											$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-pinch");
											if (env.currentMedia.isImage())
												pS.pinchOut(null, env.initialZoom);
											env.nextMedia.swipeLeft();
											return false;
										}
										return true;
									}
								);
							}
						);
					}

					if (env.options.expose_original_media || env.options.expose_full_size_media_in_cache) {
						$(".media-box#" + id + " .original-link").parent().show();
						let fullSizeMediaPath = encodeURI(self.fullSizeMediaPath());
						$(".download-single-media").attr("file", fullSizeMediaPath);

						$(".media-box#" + id + " .original-link").attr("target", "_blank").attr("href", fullSizeMediaPath);
					} else {
						$(".media-box#" + id + " .original-link").parent().hide();
					}
					if (self.hasGpsData()) {
						$(".media-box#" + id + " .map-link").off("click").on(
							"click",
							function() {
								$(".map-popup-trigger")[0].click();
							}
						);
						$(".media-box#" + id + " .map-link").parent().show();
					} else {
						$(".media-box#" + id + " .map-link").removeAttr("href");
						// $(".media-box#" + id + " .map-link").removeAttr("href").css("cursor", "pointer");
						$(".media-box#" + id + " .map-link").parent().hide();
					}

					$(".media-box#center .metadata-show, .media-box#center .metadata-hide, .media-box#center .metadata").off("click").on(
						"click",
						function(ev) {
							util.toggleMetadataFromMouse(ev);
						}
					);
					$(".media-box#center .enter-fullscreen, .media-box#center .exit-fullscreen").off("click").on(
						"click",
						function(ev) {
							util.toggleFullscreenFromMouse(ev);
						}
					);

					// set social buttons events
					let triggerEvent = env.currentMedia.chooseTriggerEvent();
					$("#media-center").off(triggerEvent).on(
						triggerEvent,
						function() {
							util.conditionalLoadMatomoTracking();
							util.conditionalLoadOthersJsFiles();
						}
					);
				}

				$(".media-box#" + id + " .metadata tr.gps").off("click");
				text = "<table>";
				// Here we keep only the technical metadata
				if (self.metadata.hasOwnProperty("title") && self.metadata.title && self.metadata.title !== self.name)
					text += "<tr><td class='metadata-data-name'></td><td>" + self.name + "</td></tr>";
				if (self.exifDate !== undefined)
					text += "<tr><td class='metadata-data-date'></td><td>" + self.exifDate + "</td></tr>";
				if (env.options.expose_original_media) {
					text += "<tr><td class='metadata-data-file-size'></td><td>" + util.humanFileSize(self.fileSizes.original.imagesAudiosVideosTotal()) + "</td></tr>";
				}
				if (self.metadata.size !== undefined)
					text += "<tr><td class='metadata-data-size'></td><td>" + self.width() + " x " + self.height() + "</td></tr>";
				if (self.metadata.make !== undefined)
					text += "<tr><td class='metadata-data-make'></td><td>" + self.metadata.make + "</td></tr>";
				if (self.metadata.model !== undefined)
					text += "<tr><td class='metadata-data-model'></td><td>" + self.metadata.model + "</td></tr>";
				if (self.metadata.aperture !== undefined)
					text += "<tr><td class='metadata-data-aperture'></td><td> f/" + self.metadata.aperture + "</td></tr>";
				if (self.metadata.focalLength !== undefined)
					text += "<tr><td class='metadata-data-focalLength'></td><td>" + self.metadata.focalLength + "</td></tr>";
				if (self.metadata.subjectDistanceRange !== undefined)
					text += "<tr><td class='metadata-data-subjectDistanceRange'></td><td>" + self.metadata.subjectDistanceRange + "</td></tr>";
				if (self.metadata.iso !== undefined)
					text += "<tr><td class='metadata-data-iso'></td><td>" + self.metadata.iso + "</td></tr>";
				if (self.metadata.sceneCaptureType !== undefined)
					text += "<tr><td class='metadata-data-sceneCaptureType'></td><td>" + self.metadata.sceneCaptureType + "</td></tr>";
				if (self.metadata.exposureTime !== undefined) {
					text += "<tr><td class='metadata-data-exposureTime'></td><td>" + self.metadata.exposureTime + " sec</td></tr>";
				}
				if (self.metadata.exposureProgram !== undefined)
					text += "<tr><td class='metadata-data-exposureProgram'></td><td>" + self.metadata.exposureProgram + "</td></tr>";
				if (self.metadata.exposureCompensation !== undefined)
					text += "<tr><td class='metadata-data-exposureCompensation'></td><td>" + self.metadata.exposureCompensation + "</td></tr>";
				if (self.metadata.spectralSensitivity !== undefined)
					text += "<tr><td class='metadata-data-spectralSensitivity'></td><td>" + self.metadata.spectralSensitivity + "</td></tr>";
				if (self.metadata.sensingMethod !== undefined)
					text += "<tr><td class='metadata-data-sensingMethod'></td><td>" + self.metadata.sensingMethod + "</td></tr>";
				if (self.metadata.lightSource !== undefined)
					text += "<tr><td class='metadata-data-lightSource'></td><td>" + self.metadata.lightSource + "</td></tr>";
				if (self.metadata.flash !== undefined)
					text += "<tr><td class='metadata-data-flash'></td><td>" + self.metadata.flash + "</td></tr>";
				if (self.metadata.orientationText !== undefined)
					text += "<tr><td class='metadata-data-orientation'></td><td>" + self.metadata.orientationText + "</td></tr>";
				if (self.metadata.rotation !== undefined)
					text += "<tr><td class='metadata-data-orientation'></td><td>" + self.metadata.rotation + "º</td></tr>";
				if (self.metadata.duration !== undefined)
					text += "<tr><td class='metadata-data-duration'></td><td>" + self.metadata.duration + "</td></tr>";
				if (self.codec !== undefined)
					text += "<tr><td class='metadata-data-codec'></td><td>" + self.codec + "</td></tr>";
				if (self.metadata.latitude !== undefined)
					text += "<tr class='map-link-from-gps gps'><td class='metadata-data-latitude'></td><td>" + self.metadata.latitudeMS + " </td></tr>";
				if (self.metadata.longitude !== undefined)
					text += "<tr class='map-link-from-gps gps'><td class='metadata-data-longitude'></td><td>" + self.metadata.longitudeMS + " </td></tr>";
				if (self.metadata.altitude !== undefined)
					text += "<tr class='map-link-from-gps gps'><td class='metadata-data-altitude'></td><td>" + self.metadata.altitude + " m</td></tr>";
				text += "</table>";
				$(".media-box#" + id + " .metadata").html(text);
				var linkTitle = util._t('#show-map');
				if (! env.isAnyMobile)
					linkTitle += " [" + util._s(".map-link-shortcut") + "]";
				$(".media-box#" + id + " .metadata tr.gps").attr("title", linkTitle).off("click").on(
					"click",
					function() {
						$(".map-popup-trigger")[0].click();
					}
				);

				if (id === "center") {
					if (env.currentAlbum.hasSomeDescription())
						env.currentAlbum.setDescription();
					if (env.currentMedia.hasSomeDescription())
						env.currentMedia.setDescription();


					// util.setFinalOptions(env.currentMedia, false);

					menuF.updateMenu();

					// // load tracking code here in case thumbs are not shown
					// util.conditionalLoadMatomoTracking();
					// util.conditionalLoadOthersJsFiles();
				}

				util.translate();

				$("#subalbums").addClass("hidden");
				util.highlightSearchedWords();
			}
		);
	};

	SingleMedia.prototype.addRotation = function(mediaElement) {
		if (this.needsAutoRotation()) {
			var object = pS.getTransformParameters(mediaElement);
			var parameter = pS.prepareTransformParameter(
				90,
				0,
				0,
				object.scale * (
					object.rotate === 0 ?
					pS.scaleFactorForRotation(this, mediaElement) / object.scale :
					1
				)
			);
			mediaElement.css(env.transformParameterName, parameter);
			if (env.currentMedia.isImage()) {
				env.unroundedCurrentZoom = pS.unroundedScreenZoom();
				env.currentZoom = util.roundDecimals(env.unroundedCurrentZoom);
				env.unroundedInitialZoominitialZoom = env.unroundedCurrentZoom;
				env.initialZoom = env.currentZoom;
			}
			util.setFinalOptions(env.currentMedia, false); // OK: add rotation
		}
	};

}());
