(function() {

	var phFl = new PhotoFloat();
	var util = new Utilities();
	var menuF = new MenuFunctions();

	/* constructor */
	function TopFunctions() {
	}

	TopFunctions.setTitle = function(id, singleMedia, self) {
		return new Promise(
			function (resolve_setTitle) {
				function searchCacheBaseIsCurrentAlbumOnly(collectionCacheBase) {
					if (collectionCacheBase) {
						let searchObject = phFl.decodeSearchCacheBase(collectionCacheBase);
						return searchObject.currentAlbumOnly;
					} else {
						return false;
					}
				}

				function getSearchFolderCacheBase(collectionCacheBase) {
					if (collectionCacheBase) {
						let searchObject = phFl.decodeSearchCacheBase(collectionCacheBase);
						return searchObject.searchedCacheBase;
					} else {
						return false;
					}
				}

				function getMapFolderCacheBase(collectionCacheBase) {
					let mapObject = phFl.decodeMapCacheBase(collectionCacheBase);
					searchOrSelectionFolderCacheBase = mapObject.clickAlbumCacheBase;
				}

				function getSelectionFolderCacheBase(searchOrSelectionOrMapCacheBase) {
					return new Promise(
						function(resolve_getSelectionFolderCacheBase) {
							let getAlbumPromise = phFl.getAlbum(searchOrSelectionOrMapCacheBase, Utilities.noOp, {getMedia: true, getPositions: false});
							getAlbumPromise.then(
								function(selectionAlbum) {
									let selectionCacheBases = selectionAlbum.subalbums.map(
										subalbum => subalbum.cacheBase.split(env.options.cache_folder_separator).slice(0, -1).join(env.options.cache_folder_separator)
									).concat(
										selectionAlbum.media.map(
											singleMedia => singleMedia.foldersCacheBase
										)
									);
									let selectionFolderCacheBase = minimumCommonBeginning(selectionCacheBases);
									if (selectionFolderCacheBase.at(-1) === env.options.cache_folder_separator)
										selectionFolderCacheBase = selectionFolderCacheBase.substring(
											0,
											selectionFolderCacheBase.length - 1
										);

									resolve_getSelectionFolderCacheBase(selectionFolderCacheBase);
								}
							);
						}
					);
				}

				function countsDetails(images, audios, videos, media, mode) {
					let imagesCount, audiosCount, videosCount, mediaCount, arrayTotals = [];
					let count = "";

					if (images[mode]) {
						imagesCount = env.numberFormat.format(images[mode]) + "&nbsp;" + util._t(".title-images");
						arrayTotals.push(imagesCount);
					}
					if (audios[mode]) {
						audiosCount = env.numberFormat.format(audios[mode]) + "&nbsp;" + util._t(".title-audios");
						arrayTotals.push(audiosCount);
					}
					if (videos[mode]) {
						videosCount = env.numberFormat.format(videos[mode]) + "&nbsp;" + util._t(".title-videos");
						arrayTotals.push(videosCount);
					}
					mediaCount = env.numberFormat.format(media[mode]);

					if (arrayTotals.length > 1) {
						count += arrayTotals.join(" + ") + " = " + mediaCount;
					} else if (images[mode]) {
						count += imagesCount;
					} else if (audios[mode]){
						count += audiosCount;
					} else if (videos[mode]){
						count += videosCount;
					}

					return count;
				}

				function minimumCommonBeginning(array) {
					if (array.length === 0) {
						return "";
					}

					let minLength = Math.min(...array.map((str) => str.length));
					let commonBeginning = "";

					for (let i = 0; i < minLength; i++) {
						let char = array[0][i]; // Get the character at index i from the first string

						if (array.every((str) => str[i] === char)) {
							commonBeginning += char;
						} else {
							break;
						}
					}

					return commonBeginning;
				}

				var title, titleCount = {}, documentTitle, i, isFolderTitle, isDateTitle, isGpsTitle, isSearchTitle, isInsideCollectionTitle, isSearchCurrentAlbumOnly, isSelectionTitle, isMapTitle;
				var titleAnchorClasses, searchOrSelectionFolderCacheBase, searchOrSelectionOrMapCacheBase;
				const raquo = "&raquo;";
				const raquoForTitle = " \u00ab ";
				// gpsLevelNumber is the number of levels for the by gps tree
				// current levels are country, region, place => 3
				var gpsLevelNumber = 3;
				var gpsName = '';
				var setDocumentTitle = (id === "center" || id === "album");
				var titleComponents = [];
				var cacheBasesForTitleComponents = [];
				var titlesForTitleComponents = [];
				var classesForTitleComponents = [];

				// do not remove the following function, without this call the browsing mode switcher shortcuts won't work
				menuF.updateMenu();

				if (id === "album") {
					$(".media-box#" + id + " .title").addClass("hidden");
					$("#album-view .title").removeClass("hidden");
				} else {
					$(".media-box#" + id + " .title").removeClass("hidden");
					$("#album-view .title").addClass("hidden");
					$("#album-view .title-string").html("");
				}

				isFolderTitle = (env.currentAlbum.ancestorsCacheBase[0] === env.options.folders_string);
				isDateTitle = (env.currentAlbum.ancestorsCacheBase[0] === env.options.by_date_string);
				isGpsTitle = (env.currentAlbum.ancestorsCacheBase[0] === env.options.by_gps_string);
				isSearchTitle = (env.currentAlbum.ancestorsCacheBase[0] === env.options.by_search_string);
				isSelectionTitle = (env.currentAlbum.ancestorsCacheBase[0] === env.options.by_selection_string);
				isMapTitle = (env.hashComponents.albumCacheBase.startsWith(env.options.by_map_string));
				isInsideCollectionTitle = false;
				isSearchCurrentAlbumOnly = false;

				// let getFolderCacheBasePromise = new Promise(
				// 	function(resolve_getFolderCacheBasePromise) {
				let searchOrSelectionFolderCacheBasePromise;
				if (isFolderTitle && env.hashComponents.collectionCacheBase) {
					isInsideCollectionTitle = true;
					searchOrSelectionOrMapCacheBase = env.hashComponents.collectionCacheBase;
					if (
						util.isSearchCacheBase(searchOrSelectionOrMapCacheBase) &&
						searchCacheBaseIsCurrentAlbumOnly(searchOrSelectionOrMapCacheBase)
					) {
						isSearchCurrentAlbumOnly = true;
						searchOrSelectionFolderCacheBase = getSearchFolderCacheBase(searchOrSelectionOrMapCacheBase);
						searchOrSelectionFolderCacheBasePromise = Promise.resolve(searchOrSelectionFolderCacheBase);
					}
					if (util.isSelectionCacheBase(searchOrSelectionOrMapCacheBase)) {
						isSearchCurrentAlbumOnly = true;
						searchOrSelectionFolderCacheBasePromise = getSelectionFolderCacheBase(searchOrSelectionOrMapCacheBase);
					}
					if (util.isMapCacheBase(searchOrSelectionOrMapCacheBase)) {
						isSearchCurrentAlbumOnly = true;
						searchOrSelectionFolderCacheBase = getMapFolderCacheBase(searchOrSelectionOrMapCacheBase);
						searchOrSelectionFolderCacheBasePromise = Promise.resolve(searchOrSelectionFolderCacheBase);
					}
				} else if (isSearchTitle) {
					searchOrSelectionOrMapCacheBase = env.hashComponents.albumCacheBase;
					if (searchCacheBaseIsCurrentAlbumOnly(searchOrSelectionOrMapCacheBase)) {
						isSearchCurrentAlbumOnly = true;
						searchOrSelectionFolderCacheBase = getSearchFolderCacheBase(searchOrSelectionOrMapCacheBase);
						searchOrSelectionFolderCacheBasePromise = Promise.resolve(searchOrSelectionFolderCacheBase);
					} else {
						searchOrSelectionFolderCacheBasePromise = Promise.resolve(searchOrSelectionFolderCacheBase);
					}
				} else if (isMapTitle) {
					let mapObject = phFl.decodeMapCacheBase(env.hashComponents.albumCacheBase);
					searchOrSelectionOrMapCacheBase = env.hashComponents.albumCacheBase;
					isSearchCurrentAlbumOnly = true;
					searchOrSelectionFolderCacheBase = mapObject.clickAlbumCacheBase;
					searchOrSelectionFolderCacheBasePromise = Promise.resolve(searchOrSelectionFolderCacheBase);
				} else if (isSelectionTitle) {
					searchOrSelectionOrMapCacheBase = env.hashComponents.albumCacheBase;
					isSearchCurrentAlbumOnly = true;
					searchOrSelectionFolderCacheBasePromise = getSelectionFolderCacheBase(searchOrSelectionOrMapCacheBase);
				} else {
					searchOrSelectionFolderCacheBasePromise = Promise.resolve(searchOrSelectionFolderCacheBase);
				}

				searchOrSelectionFolderCacheBasePromise.then(
					function(searchOrSelectionFolderCacheBase) {
						isSearchCurrentAlbumOnly = (
							isSearchCurrentAlbumOnly &&
							searchOrSelectionFolderCacheBase !== util.isAnyRootCacheBase(searchOrSelectionFolderCacheBase)
						);

						titleAnchorClasses = 'title-anchor';
						// if (env.isAnyMobile)
						// 	titleAnchorClasses += ' mobile';

						var mediaTotalInAlbum, imagesTotalInAlbum, audiosTotalInAlbum, videosTotalInAlbum;
						var mediaTotalInSubTree, imagesTotalInSubTree, audiosTotalInSubTree, videosTotalInSubTree;
						var mediaTotalInSubAlbums, imagesTotalInSubAlbums, audiosTotalInSubAlbums, videosTotalInSubAlbums;
						var numSubalbums = {
							all: env.currentAlbum.subalbums.length
						};
						if (env.options.expose_image_positions)
							numSubalbums.nonGps = env.currentAlbum.subalbums.filter(
								ithSubalbum => ithSubalbum.nonGeotagged.numsMediaInSubTree.imagesAudiosVideosTotal()
							).length;

						if (singleMedia === null) {
							mediaTotalInAlbum = {
								all: env.currentAlbum.numsMedia.imagesAudiosVideosTotal()
							};
							if (env.options.expose_image_positions)
								mediaTotalInAlbum.nonGps = env.currentAlbum.nonGeotagged.numsMedia.imagesAudiosVideosTotal();

							imagesTotalInAlbum = {
								all: env.currentAlbum.numsMedia.images
							};
							if (env.options.expose_image_positions)
								imagesTotalInAlbum.nonGps = env.currentAlbum.nonGeotagged.numsMedia.images;

							audiosTotalInAlbum = {
								all: env.currentAlbum.numsMedia.audios
							};
							if (env.options.expose_image_positions)
								audiosTotalInAlbum.nonGps = env.currentAlbum.nonGeotagged.numsMedia.audios;

							videosTotalInAlbum = {
								all: env.currentAlbum.numsMedia.videos
							};
							if (env.options.expose_image_positions)
								videosTotalInAlbum.nonGps = env.currentAlbum.nonGeotagged.numsMedia.videos;


							mediaTotalInSubTree = {
								all: env.currentAlbum.numsMediaInSubTree.imagesAudiosVideosTotal()
							};
							if (env.options.expose_image_positions)
								mediaTotalInSubTree.nonGps = env.currentAlbum.nonGeotagged.numsMediaInSubTree.imagesAudiosVideosTotal();

							imagesTotalInSubTree = {
								all: env.currentAlbum.numsMediaInSubTree.images
							};
							if (env.options.expose_image_positions)
								imagesTotalInSubTree.nonGps = env.currentAlbum.nonGeotagged.numsMediaInSubTree.images;

							audiosTotalInSubTree = {
								all: env.currentAlbum.numsMediaInSubTree.audios
							};
							if (env.options.expose_image_positions)
								audiosTotalInSubTree.nonGps = env.currentAlbum.nonGeotagged.numsMediaInSubTree.audios;

							videosTotalInSubTree = {
								all: env.currentAlbum.numsMediaInSubTree.videos
							};
							if (env.options.expose_image_positions)
								videosTotalInSubTree.nonGps = env.currentAlbum.nonGeotagged.numsMediaInSubTree.videos;


							mediaTotalInSubAlbums = {
								all: mediaTotalInSubTree.all - mediaTotalInAlbum.all
							};
							if (env.options.expose_image_positions)
								mediaTotalInSubAlbums.nonGps = mediaTotalInSubTree.nonGps - mediaTotalInAlbum.nonGps;

							imagesTotalInSubAlbums = {
								all: imagesTotalInSubTree.all - imagesTotalInAlbum.all
							};
							if (env.options.expose_image_positions)
								imagesTotalInSubAlbums.nonGps = imagesTotalInSubTree.nonGps - imagesTotalInAlbum.nonGps;

							audiosTotalInSubAlbums = {
								all: audiosTotalInSubTree.all - audiosTotalInAlbum.all,
							};
							if (env.options.expose_image_positions)
								audiosTotalInSubAlbums.nonGps = audiosTotalInSubTree.nonGps - audiosTotalInAlbum.nonGps;

							videosTotalInSubAlbums = {
								all: videosTotalInSubTree.all - videosTotalInAlbum.all,
							};
							if (env.options.expose_image_positions)
								videosTotalInSubAlbums.nonGps = videosTotalInSubTree.nonGps - videosTotalInAlbum.nonGps;
						}

						// the first component of the title is always the root album
						if (env.options.page_title[env.language] !== "")
							titleComponents[0] = env.options.page_title[env.language];
						else
							titleComponents[0] = util._t(".title-string");
						cacheBasesForTitleComponents[0] = env.options.folders_string;
						classesForTitleComponents[0] = [""];

						let promises = [];

						if (isSearchTitle || isMapTitle || isSelectionTitle || isInsideCollectionTitle) {
							if (isSearchCurrentAlbumOnly) {
								// put the components of the album searched in
								let splittedCacheBaseSearchedIn = searchOrSelectionFolderCacheBase.split(env.options.cache_folder_separator);
								let cacheBasesToAdd = splittedCacheBaseSearchedIn.map((x, i) => splittedCacheBaseSearchedIn.slice(0, i + 1).join(env.options.cache_folder_separator));
								if (
									splittedCacheBaseSearchedIn[0] === env.options.folders_string ||
									splittedCacheBaseSearchedIn[0] === env.options.by_selection_string ||
									splittedCacheBaseSearchedIn[0] === env.options.by_search_string ||
									splittedCacheBaseSearchedIn[0] === env.options.by_map_string
								) {
									splittedCacheBaseSearchedIn.shift();
									cacheBasesToAdd.shift();
								}
								let classesToAdd = splittedCacheBaseSearchedIn.map((x, i) => ["pre-cache-base-" + id + "-" + i]);
								let indexForAdding1 = titleComponents.length;

								// the value added to titleComponents will be substituted soon; they are put now in order to let the array grow
								titleComponents = titleComponents.concat(splittedCacheBaseSearchedIn);
								cacheBasesForTitleComponents = cacheBasesForTitleComponents.concat(cacheBasesToAdd);
								classesForTitleComponents = classesForTitleComponents.concat(classesToAdd);

								// substitute each album cache base with the right name
								cacheBasesToAdd.forEach(
									function(cacheBase, i) {
										let thisI = i;
										let ithPromise = new Promise(
											function(resolve_ithPromise) {
												let cacheBasePromise = phFl.getAlbum(cacheBase, null, {getMedia: false, getPositions: false});
												cacheBasePromise.then(
													function(theAlbum) {
														let nameForShowing = theAlbum.nameForShowing();
														if (nameForShowing)
															titleComponents[indexForAdding1 + thisI] = nameForShowing;
														else if (util.isSelectionCacheBase(cacheBase))
															titleComponents[indexForAdding1 + thisI] = "(" + util._t("#by-selection") + ")";
														else if (util.isMapCacheBase(cacheBase))
														// else if (util.isMapCacheBase(cacheBase) && ! env.map.clickHistory)
															titleComponents[indexForAdding1 + thisI] = "(" + util._t("#by-map") + ")";

														resolve_ithPromise();
													}
												);
											}
										);
										promises.push(ithPromise);
									}
								);
							}

							// put the search cacheBase
							if (searchOrSelectionOrMapCacheBase) {
								if (util.isSearchCacheBase(searchOrSelectionOrMapCacheBase)) {
									classesForTitleComponents.push(["search-link"]);
									cacheBasesForTitleComponents.push(searchOrSelectionOrMapCacheBase);
									titleComponents.push("(" + util._t("#by-search") + ")");
								} else if (util.isSelectionCacheBase(searchOrSelectionOrMapCacheBase)) {
									classesForTitleComponents.push(["selection-link"]);
									cacheBasesForTitleComponents.push(searchOrSelectionOrMapCacheBase);
									titleComponents.push("(" + util._t("#by-selection") + ")");
								} else if (util.isMapCacheBase(searchOrSelectionOrMapCacheBase)) {
								// } else if (util.isMapCacheBase(searchOrSelectionOrMapCacheBase) && ! env.map.clickHistory) {
									classesForTitleComponents.push(["map-link"]);
									cacheBasesForTitleComponents.push(searchOrSelectionOrMapCacheBase);
									titleComponents.push("(" + util._t("#by-map") + ")");
								}
							}

							if (isInsideCollectionTitle) {
								// put the components of the found album and (if any) its subalbums
								let splittedAlbumCacheBase = env.hashComponents.albumCacheBase.split(env.options.cache_folder_separator);
								let splittedFoundAlbumCacheBase, cacheBasesForSplittedAlbumCacheBase;
								if (isSearchCurrentAlbumOnly) {
									splittedFoundAlbumCacheBase = env.hashComponents.collectedAlbumCacheBase.split(env.options.cache_folder_separator);
									cacheBasesForSplittedAlbumCacheBase = splittedAlbumCacheBase.map((x, i) => splittedAlbumCacheBase.slice(0, i + 1).join(env.options.cache_folder_separator)).slice(splittedFoundAlbumCacheBase.length - 1);
								} else {
									cacheBasesForSplittedAlbumCacheBase = splittedAlbumCacheBase.map((x, i) => splittedAlbumCacheBase.slice(0, i + 1).join(env.options.cache_folder_separator)).slice(1);
								}

								let cacheBasesToAdd = cacheBasesForSplittedAlbumCacheBase.slice();
								let classesToAdd = cacheBasesForSplittedAlbumCacheBase.map((x, i) => ["post-cache-base-" + id + "-" + i]);

								let indexForAdding2 = titleComponents.length;

								titleComponents = titleComponents.concat(cacheBasesForSplittedAlbumCacheBase);
								cacheBasesForTitleComponents = cacheBasesForTitleComponents.concat(cacheBasesToAdd);
								classesForTitleComponents = classesForTitleComponents.concat(classesToAdd);
								cacheBasesToAdd.forEach(
									function(cacheBase, i) {
										let thisI = i;
										let ithPromise = new Promise(
											function(resolve_ithPromise) {
												let cacheBasePromise = phFl.getAlbum(cacheBase, null, {getMedia: false, getPositions: false});
												cacheBasePromise.then(
													function(theAlbum) {
														let name = theAlbum.nameForShowing();
														let fake_name, subalbumPosition;
														if (theAlbum.hasOwnProperty("captionsForSearch"))
															[fake_name, subalbumPosition] = theAlbum.captionsForSearch;
														else if (theAlbum.hasOwnProperty("captionsForSelection"))
															[fake_name, subalbumPosition] = theAlbum.captionsForSelection;
														if (thisI === 0) {
															name =
																"<span class='with-second-part'>" +
																	"<span id='album-name-first-part'>" + name + "</span> " +
																	"<span id='album-name-second-part'>" + subalbumPosition + "</span>" +
																"</span> ";
														}
														if (name)
															titleComponents[indexForAdding2 + thisI] = name;
														resolve_ithPromise();
													}
												);
											}
										);
										promises.push(ithPromise);
									}
								);
							}

							// the counts for inside a search are generated further
							for (const mode in numSubalbums) {
								if (numSubalbums.hasOwnProperty(mode)) {
									if (
										(
											isSearchTitle ||
											isSelectionTitle ||
											isMapTitle
										) &&
										singleMedia === null &&
										(mediaTotalInAlbum[mode] || numSubalbums[mode])
									) {
										if (isSearchTitle)
											titleCount[mode] = "<span class='title-count'>(" + util._t(".title-found") + " ";
										else
											// map title
											titleCount[mode] = "<span class='title-count'>(" + util._t(".title-selected") + " ";

										if (mediaTotalInAlbum[mode]) {
											titleCount[mode] += countsDetails(imagesTotalInAlbum, audiosTotalInAlbum, videosTotalInAlbum, mediaTotalInAlbum, mode);
										}

										if (mediaTotalInAlbum[mode] && numSubalbums[mode])
											titleCount[mode] += ", " + util._t(".title-and") + " ";

										if (numSubalbums[mode]) {
											titleCount[mode] += env.numberFormat.format(numSubalbums[mode]) + " " + util._t(".title-albums");
										}

										if (env.search.hasOwnProperty("removedStopWords") && env.search.removedStopWords.length) {
											// say that some search word hasn't been used
											titleCount[mode] += " - " + env.search.removedStopWords.length + " " + util._t("#removed-stopwords") + ": ";
											for (i = 0; i < env.search.removedStopWords.length; i ++) {
												if (i)
													titleCount[mode] += ", ";
												titleCount[mode] += env.search.removedStopWords[i];
											}
										}

										titleCount[mode] += ")</span>";
									}
								}
							}
						} else {
							// here: ! (isSearchTitle || isInsideCollectionTitle)
							let titleComponentsToAdd;
							if (env.currentAlbum.hasOwnProperty("ancestorsTitles") && env.currentAlbum.hasOwnProperty("ancestorsNames")) {
								titleComponentsToAdd = env.currentAlbum.ancestorsNames.map(
									(ithComponent, i) => {
										if (
											env.currentAlbum.ancestorsTitles[i] &&
											env.currentAlbum.ancestorsNames[i] &&
											env.currentAlbum.ancestorsTitles[i] !== env.currentAlbum.ancestorsNames[i]
										)
											return env.currentAlbum.ancestorsTitles[i] + " <span class='real-name'>(" + env.currentAlbum.ancestorsNames[i] + ")";
										else if (env.currentAlbum.ancestorsTitles[i])
											return env.currentAlbum.ancestorsTitles[i];
										else
											return env.currentAlbum.ancestorsNames[i];
									}
								);
							} else if (env.currentAlbum.hasOwnProperty("ancestorsNames")) {
								titleComponentsToAdd = env.currentAlbum.ancestorsNames.slice();
							} else {
								titleComponentsToAdd = env.currentAlbum.path.split("/");
							}

							let cacheBasesToAdd = env.currentAlbum.ancestorsCacheBase.slice();
							let classesToAdd = env.currentAlbum.ancestorsCacheBase.map(x => [""]);

							if (cacheBasesToAdd[0] === env.options.folders_string) {
								titleComponentsToAdd = titleComponentsToAdd.slice(1);
								cacheBasesToAdd = cacheBasesToAdd.slice(1);
								classesToAdd = classesToAdd.slice(1);
							}

							titleComponents = titleComponents.concat(titleComponentsToAdd);
							cacheBasesForTitleComponents = cacheBasesForTitleComponents.concat(cacheBasesToAdd);
							classesForTitleComponents = classesForTitleComponents.concat(classesToAdd);

							if (isDateTitle) {
								titleComponents[1] = "(" + util._t("#by-date") + ")";

								if (titleComponents.length > 2) {
									titleComponents[2] = parseInt(titleComponents[2]).toString();
									if (titleComponents.length > 3) {
										titleComponents[3] = util._t("#month-" + titleComponents[3]);
										if (titleComponents.length > 4) {
											titleComponents[4] = parseInt(titleComponents[4]).toString();
										}
									}
								}

								if (singleMedia === null) {
									for (const mode in numSubalbums) {
										if (numSubalbums.hasOwnProperty(mode)) {
											titleCount[mode] = "<span class='title-count'>(";

											titleCount[mode] += countsDetails(imagesTotalInAlbum, audiosTotalInAlbum, videosTotalInAlbum, mediaTotalInAlbum, mode);

											if (titleComponents.length >= 5)
												titleCount[mode] += " " + util._t(".title-in-day-album");
											else if (titleComponents.length >= 3)
												titleCount[mode] += " " + util._t(".title-in-date-album");
											titleCount[mode] += ")</span>";
										}
									}
								}
							} else if (isGpsTitle) {
								titleComponents[1] = "(" + util._t("#by-gps") + ")";

								for (i = 2; i < titleComponents.length; i ++) {
									if (i === titleComponents.length - 1) {
										const brFalse = false;
										gpsName = util.transformAltPlaceName(titleComponents[i], brFalse);
									} else {
										gpsName = titleComponents[i];
									}

									if (gpsName === '')
										gpsName = util._t('.not-specified');

									// let aObject = $("<a></a>");
									// aObject.attr("title", util._t("#place-icon-title") + gpsName + util._t("#place-icon-title-end"));
									// titlesForTitleComponents[i] = aObject.attr("title");

									titleComponents[i] = gpsName;
								}

								if (singleMedia === null) {
									for (const mode in numSubalbums) {
										if (numSubalbums.hasOwnProperty(mode)) {
											titleCount[mode] = "<span class='title-count'>(";

											titleCount[mode] += countsDetails(imagesTotalInAlbum, audiosTotalInAlbum, videosTotalInAlbum, mediaTotalInAlbum, mode);

											if (titleComponents.length >= gpsLevelNumber + 2)
												titleCount[mode] += " " + util._t(".title-in-gps-album");
											else if (titleComponents.length >= 3)
												titleCount[mode] += " " + util._t(".title-in-gpss-album");
											titleCount[mode] += ")</span>";
										}
									}
								}
							} else if (isSelectionTitle) {
								titleComponents.pop();
								cacheBasesForTitleComponents.pop();

								titleComponents[1] = "(" + util._t("#by-selection") + ")";

								for (const mode in numSubalbums) {
									if (
										numSubalbums.hasOwnProperty(mode) &&
										singleMedia === null &&
										(mediaTotalInAlbum[mode] || numSubalbums[mode])
									) {
										titleCount[mode] = "<span class='title-count'>(";
										if (numSubalbums[mode]) {
											titleCount[mode] += env.numberFormat.format(numSubalbums[mode]);
											titleCount[mode] += " " + util._t(".title-albums");
										}

										if (mediaTotalInAlbum[mode] && numSubalbums[mode])
											titleCount[mode] += " " + util._t(".title-and") + " ";

										if (mediaTotalInAlbum[mode]) {
											titleCount[mode] += countsDetails(imagesTotalInAlbum, audiosTotalInAlbum, videosTotalInAlbum, mediaTotalInAlbum, mode);
										}

										titleCount[mode] += ")</span>";
									}
								}
							} else if (isMapTitle) {
								titleComponents.pop();
								cacheBasesForTitleComponents.pop();

								titleComponents[1] = "(" + util._t("#by-map") + ")";

								for (const mode in numSubalbums) {
									if (
										numSubalbums.hasOwnProperty(mode) &&
										titleComponents.length > 2 &&
										singleMedia === null &&
										mediaTotalInAlbum[mode]
									) {
										titleCount[mode] = "<span class='title-count'>(";
										titleCount[mode] += countsDetails(imagesTotalInAlbum, audiosTotalInAlbum, videosTotalInAlbum, mediaTotalInAlbum, mode);
										titleCount[mode] += ")</span>";
									}
								}
							} else {
								// folders title

								// counts are added further
								// nothing to do
							}
						}

						if (singleMedia === null && (isInsideCollectionTitle || isFolderTitle)) {
							let firstTime = true;
							let previousTotalCount = -1;
							for (const mode in numSubalbums) {
								if (numSubalbums.hasOwnProperty(mode)) {
									let totalCount = numSubalbums[mode] + mediaTotalInAlbum[mode] + mediaTotalInSubAlbums[mode];
									if (firstTime && previousTotalCount > 0 && totalCount > 0)
										titleCount[mode] += " ";
									firstTime = false;
									if (totalCount > 0) {
										titleCount[mode] = "<span class='title-count'>(";
										// // if (numSubalbums[mode]) {
										// // 	titleCount[mode] += env.numberFormat.format(numSubalbums[mode]) + " " + util._t(".subalbum");
										// // }
										//
										// if ((mediaTotalInAlbum[mode] || mediaTotalInSubAlbums[mode]) && numSubalbums[mode])
										// 	titleCount[mode] += "; ";

										if (mediaTotalInAlbum[mode]) {
											titleCount[mode] += countsDetails(imagesTotalInAlbum, audiosTotalInAlbum, videosTotalInAlbum, mediaTotalInAlbum, mode);

											if (mediaTotalInSubAlbums[mode])
												titleCount[mode] += "; ";
										}

										if (mediaTotalInSubAlbums[mode]) {
											titleCount[mode] += util._t(".title-in-subalbums", numSubalbums[mode]) + " ";
											titleCount[mode] += countsDetails(imagesTotalInSubAlbums, audiosTotalInSubAlbums, videosTotalInSubAlbums, mediaTotalInSubAlbums, mode);
										}
										if (mediaTotalInAlbum[mode] && mediaTotalInSubAlbums[mode]) {
											titleCount[mode] += "; " + util._t(".title-total") + " ";
											titleCount[mode] += countsDetails(imagesTotalInSubTree, audiosTotalInSubTree, videosTotalInSubTree, mediaTotalInSubTree, mode);
										}
										titleCount[mode] += ")</span>";
									}
								}
							}
						}

						// if (addSearchMarker) {
						// 	let numElements = searchOrSelectionFolderCacheBase.split(env.options.cache_folder_separator).length;
						// 	titleComponents.splice(numElements, 0, " (" + util._t("#by-search") + ")");
						// 	cacheBasesForTitleComponents.splice(numElements, 0, env.options.by_search_string);
						// }

						if (env.options.expose_image_positions && ! env.options.save_data)
							promises.push(env.currentAlbum.generatePositionsAndMediaInMediaAndSubalbums());

						Promise.all(promises).then(
							function() {
								let documentTitleComponents = titleComponents.map(component => util.stripHtmlAndReplaceEntities(component));

								if (singleMedia !== null) {
									let singleMediaNameHtml;
									let [singleMediaName, singleMediaTitle] = singleMedia.nameAndTitleForShowing(true);
									let defined = false;

									if (isSearchTitle || isSelectionTitle || isMapTitle && ! env.map.clickHistory) {
										let name, mediaNamePosition;
										// let albumForCaption;

										// if (env.currentAlbum.isByGps())
										// 	albumForCaption = env.cache.getAlbum(singleMedia.gpsAlbumCacheBase);
										// else if (env.currentAlbum.isByDate())
										// 	albumForCaption = env.cache.getAlbum(singleMedia.dayAlbumCacheBase);
										// else if (env.currentAlbum.isFolder() || env.currentAlbum.isSelection() || env.currentAlbum.isSearch())
										// 	albumForCaption = env.cache.getAlbum(singleMedia.foldersCacheBase);

										if (isSearchTitle && singleMedia.captionsForSearch) {
											// if (! singleMedia.hasOwnProperty("captionsForSearch"))
											// 	singleMedia.generateCaptionsForSearch(albumForCaption);
											[name, mediaNamePosition] = singleMedia.captionsForSearch;
											defined = true;
										} else if (isSelectionTitle && singleMedia.hasOwnProperty("captionsForSelection")) {
											// if (! singleMedia.hasOwnProperty("captionsForSelection"))
											// 	singleMedia.generateCaptionsForSelection(albumForCaption);
											[name, mediaNamePosition] = singleMedia.captionsForSelection;
											defined = true;
										} else if (isMapTitle &&singleMedia.captionsForPopup) {
											// if (! singleMedia.hasOwnProperty("captionsForPopup"))
											// 	singleMedia.generateCaptionsForPopup(albumForCaption);
											[name, mediaNamePosition] = singleMedia.captionsForPopup;
											defined = true;
										}

										if (defined && ! mediaNamePosition && singleMedia.titleForShowing) {
											mediaNamePosition = singleMedia.titleForShowing;
											singleMediaNameHtml =
												"<span class='media-name with-second-part'>" +
												"<span id='media-name-first-part'>" + name + "</span> " +
												"<span id='media-name-second-part'>(" + mediaNamePosition + ")</span>" +
												"</span> ";
										} else if (name) {
											singleMediaNameHtml = "<span class='media-name'>" + name + "</span>";
										}
									}

									if (! defined && singleMediaTitle) {
										singleMediaNameHtml =
											"<span class='media-name with-second-part'>" +
											"<span id='media-name-first-part'>" + singleMediaName + "</span> " +
											"<span id='media-name-second-part'>(" + singleMediaTitle + ")</span>" +
											"</span> ";
									} else {
										singleMediaNameHtml = "<span class='media-name'>" + singleMediaName + "</span>";
									}

									titleComponents.push(singleMediaNameHtml);
									classesForTitleComponents.push([""]);
									titlesForTitleComponents.push([singleMediaTitle]);

									documentTitleComponents.push(util.stripHtmlAndReplaceEntities(singleMediaName));
								}

								title = titleComponents.map(
									(component, i) => {
										let titleElement;
										let aTagBegin = "";
										let aTagEnd = "";
										if (cacheBasesForTitleComponents[i] !== undefined && cacheBasesForTitleComponents[i] !== "") {
											aTagBegin = "<a class='" + titleAnchorClasses + "' href='" + env.hashBeginning + cacheBasesForTitleComponents[i] + "'>";
											aTagEnd = "</a>";
										}
										let aObject;
										if (component.indexOf("<a href=") !== -1) {
											let firstClosingAngularBracketPosition = component.indexOf(">");
											let secondOpeningAngularBracketPosition = component.indexOf(" <", 2);
											aObject = $(
												component.substring(0, firstClosingAngularBracketPosition + 1) + // <span class='with-second-part'>
												aTagBegin +
												component.substring(firstClosingAngularBracketPosition + 1, secondOpeningAngularBracketPosition) + // the album name
												aTagEnd +
												component.substring(secondOpeningAngularBracketPosition)
											);
										} else {
											aObject = $(aTagBegin + component + aTagEnd);
										}
										if (classesForTitleComponents[i] !== "")
											classesForTitleComponents[i].forEach(singleClass => aObject.addClass(singleClass));
										if (titlesForTitleComponents[i] !== "")
											aObject.attr("title", titlesForTitleComponents[i]);
										titleElement = aObject.wrapAll('<div>').parent().html();
										// } else {
										// 	titleElement = "<span class='title-no-anchor'>" + component + "</span>";
										// }
										return titleElement;
									}
								).join(raquo);
								// title = "<span class='title-elements'>" + title + "</span>";

								documentTitle = documentTitleComponents.reverse().join(raquoForTitle);

								if (singleMedia === null && env.currentAlbum.numPositionsInTree && ! env.options.save_data) {
									let markers = "";

									let showSingleMarker = (env.currentAlbum.numPositionsInMedia > 0 && env.currentAlbum.numPositionsInTree !== env.currentAlbum.numPositionsInSubalbums);
									let showDoubleMarker = (env.currentAlbum.numPositionsInSubalbums > 0);

									let imgTitle1, imgTitle2;
									if (showSingleMarker && ! showDoubleMarker || ! showSingleMarker && showDoubleMarker) {
										imgTitle1 = util._t("#show-markers-on-map");
										imgTitle2 = imgTitle1;
									} else if (showSingleMarker && showDoubleMarker){
										imgTitle1 = util._t("#show-album-markers-on-map");
										imgTitle2 = util._t("#show-tree-markers-on-map");
									}
									let imgAlt = util._t(".marker");
									let imgHtml =
										"<img " +
											"class='title-img gps' " +
										">";
									let imgObject = $(imgHtml);
									imgObject.attr("alt", imgAlt);

									if (showSingleMarker) {
										let imgObjectSingle = imgObject.clone();
										if (! env.isAnyMobile && ! showDoubleMarker)
											imgTitle1 += " [" + util._s(".map-link-shortcut") + "]";
										imgObjectSingle.attr("title", imgTitle1);
										imgObjectSingle.attr("src", "img/ic_place_white_24dp_2x.png");
										imgObjectSingle.attr("width", "30");
										imgObjectSingle.attr("height", "42");
										imgObjectSingle.addClass("map-popup-trigger");
										markers += imgObjectSingle.wrapAll('<span>').parent().html();
									}
									if (showDoubleMarker) {
										let imgObjectDouble = imgObject.clone();
										if (! env.isAnyMobile)
											imgTitle2 += " [" + util._s(".map-link-shortcut") + "]";
										imgObjectDouble.attr("title", imgTitle2);
										imgObjectDouble.attr("src", "img/ic_place_white_24dp_2x_double.png");
										imgObjectDouble.attr("width", "38");
										imgObjectDouble.attr("height", "44");
										imgObjectDouble.addClass("map-popup-trigger-double");
										markers += imgObjectDouble.wrapAll('<span>').parent().html();
									}

									title += markers;
								}

								if (env.currentMedia === null) {
									title += "<span class='hidden-geotagged-media'>[" + util._t(".hidden-geotagged-media") + "]</span> ";
								}

								title = "<span class='title-main'>" + title + "</span>";

								if (Object.keys(titleCount).length > 0) {
									for (const mode in titleCount) {
										if (titleCount.hasOwnProperty(mode)) {
											let modeClass = mode;
											if (mode === "nonGps")
												modeClass = "non-gps";
											title += $(titleCount[mode]).addClass(modeClass).prop("outerHTML");
										}
									}
								}

								let selector;
								if (id === "album")
									selector = "#album-view .title-string";
								else
									selector = ".media-box#" + id + " .title-string";
								$(selector).html(title);

								// add the middle click event
								if (util.isPhp()) {
									// execution enters here if we are using index.php
									$(selector + " .title-anchor").each(
										function() {
											var components = phFl.returnDecodedHash($(this).attr("href"));
											var albumCacheBase = components.albumCacheBase;
											$(this).off("auxclick").on(
												"auxclick",
												{albumHash: env.hashBeginning + albumCacheBase},
												function (ev) {
													if (ev.which === 2) {
														util.openInNewTab(ev.data.albumHash);
														return false;
													}
												}
											);
										}
									);
								}

								$(".hidden-geotagged-media").attr("title", util._t("#hide-geotagged-media"));

								if (setDocumentTitle) {
									document.title = documentTitle;
								}
								// TopFunctions.trackMatomo(id);

								if (id === "album") {
								// if (id === "album" || id === "center") {
									util.setTitleOptions();
									// activate the map popup trigger in the title or in the single media
									$(".map-popup-trigger").off("click").on(
										"click",
										{singleMedia: env.currentMedia},
										function(ev, from) {
											// do not remove the from parameter, it is valored when the click is activated via the trigger() jquery function

											env.mapAlbum = util.initializeMapAlbum();
											// if (! env.map.clickHistory) {
											env.mapAlbum.map.clickAlbumCacheBase = env.currentAlbum.cacheBase;
											env.mapAlbum.map.clickedSelector = ".map-popup-trigger";
											env.mapAlbum.map.whatToGenerateTheMapFor = "m"; // means "media in the album, without the subalbums"
											// }
											env.mapAlbum.generateMapFromTitleWithoutSubalbums(env.currentAlbum, ev, from);

											// focus the map, so that the arrows and +/- work
											$("#mapdiv").trigger("focus");
										}
									);

									$(".map-popup-trigger-double").off("click").on(
										"click",
										{singleMedia: env.currentMedia},
										function(ev, from) {
											// do not remove the from parameter, it is valored when the click is activated via the trigger() jquery function

											env.mapAlbum = util.initializeMapAlbum();
											// if (! env.map.clickHistory) {
											env.mapAlbum.map.clickAlbumCacheBase = env.currentAlbum.cacheBase;
											env.mapAlbum.map.clickedSelector = ".map-popup-trigger-double";
											env.mapAlbum.map.whatToGenerateTheMapFor = "e"; // means "everything: media in the album and in the subalbums"
											// }
											env.mapAlbum.generateMapFromTitle(env.currentAlbum, ev, from);

											// focus the map, so that the arrows and +/- work
											$("#mapdiv").trigger("focus");
										}
									);

									if (
										env.previousAlbum &&
										env.previousAlbum.isMap() && (
											env.previousMedia === null ||
											env.previousAlbumIsAlbumWithOneMedia
										) &&
										env.previousAlbum.map.clickAlbumCacheBase === phFl.convertHashToCacheBase(window.location.hash) &&
										[".map-popup-trigger", ".map-popup-trigger-double", "#map-button-container"].indexOf(env.previousAlbum.map.clickedSelector) !== -1 &&
										env.fromEscKey ||
										env.mapRefreshType
									) {
										env.fromEscKey = false;
										$(env.previousAlbum.map.clickedSelector).trigger("click", ["fromTrigger"]);
									} else if (
										env.map.clickHistory && (
											util.isMapCacheBase(phFl.convertHashToCacheBase(window.location.hash)) &&
											env.map.clickAlbumCacheBase === phFl.decodeMapCacheBase(phFl.convertHashToCacheBase(window.location.hash)).clickAlbumCacheBase ||
											env.map.clickAlbumCacheBase === phFl.convertHashToCacheBase(window.location.hash)
										) &&
										[".map-popup-trigger", ".map-popup-trigger-double", "#map-button-container"].indexOf(env.map.clickedSelector) !== -1
										// [".map-popup-trigger", ".map-popup-trigger-double"].indexOf(env.map.clickedSelector) !== -1 &&
										// env.fromEscKey ||
										// env.mapRefreshType
									) {
										env.fromEscKey = false;
										$.executeAfterEvents(
											["otherJsFilesLoadedEvent", "pruneclusterLoadedEvent", "mapFunctionsLoadedEvent"],
											function() {
												$(env.map.clickedSelector).trigger("click", ["fromTrigger"]);
											}
										);
									}

									if (env.currentMedia === null) {
										util.addClickToHiddenGeotaggedMediaPhrase();
									}
								}

								resolve_setTitle(self);
							}
						);
					}
				);
				// 	}
				// );

			}
		);
	};

	TopFunctions.trackMatomo = function() {
		// trigger matomo tracking. It's here because it needs document.title
		if (env.options.matomo_server && env.options.matomo_id) {
			$.triggerEvent("loadMatomoEvent");

			$.executeAfterEvent(
				"matomoLoadedEvent",
				function() {
					var _paq = window._paq = window._paq || [];
					_paq.push(['setCustomUrl', '/' + window.location.hash.substr(1)]);
					let titleText, splittedTitle;
					splittedTitle = document.title.split("«");
					splittedTitle = splittedTitle.map(text => text.trim());
					titleText = splittedTitle.join(" « ");

					_paq.push(['setDocumentTitle', titleText]);
					_paq.push(['trackPageView']);
				}
			);
		}

		$.triggerEvent("lazyBegunEvent");
	};

	TopFunctions.prototype.scrollBottomThumbs = function(e, delta) {
		this.scrollLeft -= (delta * 80);
		e.preventDefault();
	};

	TopFunctions.scrollAlbum = function(e, delta) {
		this.scrollTop -= (delta * 80);
		e.preventDefault();
	};

	TopFunctions.prototype.setTitle = TopFunctions.setTitle;

	window.TopFunctions = TopFunctions;
}());
