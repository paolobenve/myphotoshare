(function() {

	var util = new Utilities();

	Album.prototype.collectMediaInTree = function(geotaggedContentIsHidden) {
		var self = this;
		return new Promise(
			function (resolve_collect) {
				var visibleMediaInAlbum = [];
				visibleMediaInAlbum = self.visibleMedia(geotaggedContentIsHidden);
				var subalbumsPromises = [];
				self.visibleSubalbums(geotaggedContentIsHidden).forEach(
					function(subalbum) {
						var subalbumPromise = new Promise(
							function(resolve_subalbumPromise) {
								var toAlbumPromise = subalbum.toAlbum(null, {getMedia: true, getPositions: ! env.options.save_data});
								toAlbumPromise.then(
									function(album) {
										var collectPromise = album.collectMediaInTree(geotaggedContentIsHidden);
										collectPromise.then(
											function(mediaInSubalbum) {
												resolve_subalbumPromise(mediaInSubalbum);
											}
										);
									}
								);
							}
						);
						subalbumsPromises.push(subalbumPromise);
					}
				);
				Promise.all(subalbumsPromises).then(
					function(arrayMedia) {
						arrayMedia.forEach(
							function(media) {
								media.forEach(
									singleMedia => {
										if (visibleMediaInAlbum.indexOf(singleMedia) === -1)
											visibleMediaInAlbum.push(singleMedia);
									}
								);
							}
						);
						resolve_collect(visibleMediaInAlbum);
					}
				);
			}
		);
	};

	Album.prototype.downloadAlbum = function() {
		// adapted from https://gist.github.com/c4software/981661f1f826ad34c2a5dc11070add0f
		//
		// this function must be converted to streams, example at https://jimmywarting.github.io/StreamSaver.js/examples/saving-multiple-files.html

		// $("#downloading-media").html(util._t("#downloading-media"));
		// $("#downloading-media").append(env.br + "<span id='file-name-for-adding-to-zip'></span>");
		var self = this;

		var zip = new JSZip();
		var zipFilename;
		// var basePath = self.path;
		
		zipFilename = env.options.page_title[env.language];
		if (self.isSearch()) {
			zipFilename += "." + util._t("#by-search") + " '" + $("#search-field").val() + "'";
		} else if (self.isSelection()) {
			zipFilename += "." + util._t("#by-selection");
		} else if (self.isByDate()) {
			let textComponents = self.path.split("/").splice(1);
			if (textComponents.length > 1)
				textComponents[1] = util._t("#month-" + textComponents[1]);
			if (textComponents.length)
				zipFilename += "." + textComponents.join("-");
		} else if (self.isByGps()) {
			zipFilename += "." + self.ancestorsNames.splice(1).join("-");
		} else if (self.isMap()) {
			zipFilename += "." + util._t("#from-map");
		} else if (self.cacheBase !== env.options.folders_string) {
			zipFilename += "." + self.nameForShowing(null);
		}

		zipFilename += ".zip";

		var includedMedia = [];
		var stop = false;
		var downloading = false;
		var partialSize;
		var fileNames = [];
		var totalSize = 0;
		$("#downloading-and-zipping").show();
		$("#downloading-and-zipping").children().hide();
		$('#downloading-media-close').show();

		$('#downloading-media-close').off("click").on(
			"click",
			function() {
				if (! downloading && ! stop && ! $("#preparing-zip-closing").is(":visible"))
					$("#preparing-zip-closing").show();
				else if (downloading || stop && $("#preparing-zip-closing").is(":visible"))
					$("#downloading-and-zipping").fadeOut(1000);

				stop = true;
			}
		);

		$("#downloading-media").show();

		let ancestorsNamesList = self.ancestorsNames.slice(1);
		if (ancestorsNamesList.length > 2) {
			ancestorsNamesList[2] = util.transformAltPlaceName(ancestorsNamesList[2]);
		}
		let albumPath = ancestorsNamesList.join("/");
		let albumPathLength = albumPath.length;

		var buildMediaListPromise = buildMediaList(self, albumPath);
		buildMediaListPromise.then(
			function() {
				// includedMedia has been filled with the media

				downloading = true;

				var getMediaBinaryContentPromise = getMediaBinaryContent();
				getMediaBinaryContentPromise.then(
					// the complete zip can be generated...
					function generateAndSaveZip() {
						if (stop)
							return;

						$("#downloading-media").hide();
						$("#preparing-zip-text").show();
						$("#file-name-for-adding-to-zip").html("").show();
						zip.generateAsync(
							{type: "blob"},
							function onUpdate(meta) {
								if (meta.currentFile)
									$("#file-name-for-adding-to-zip").html(
										meta.currentFile +
										env.br +
										meta.percent.toFixed(1) + "%"
									);
							}
						).then(
							function(content) {
								if (! stop) {
									// ... and saved
									saveAs(content, zipFilename);
								}
								$("#downloading-and-zipping").fadeOut(2000);
							}
						);
					}
				);
			}
		);
		// end of function body

		function buildMediaList(album, subalbum = "") {
			return new Promise(
				function(resolve_buildMediaList) {
					$("#file-name-for-adding-to-zip").show();
					var albumPromises = [];
					partialSize = 0;

					if (! album.isTransversal() || album.ancestorsNames.length >= 4) {
						let visibleMedia = album.visibleMedia();
						for (let iVisibleMedia = 0; iVisibleMedia < visibleMedia.length; iVisibleMedia ++) {
							if (stop)
								break;
							let ithMedia = visibleMedia[iVisibleMedia];
							if (
								ithMedia.isImage() && ! env.options.download_images ||
								ithMedia.isAudio() && ! env.options.download_audios ||
								ithMedia.isVideo() && ! env.options.download_videos ||
								includedMedia.some(singleMedia => singleMedia.isEqual(ithMedia))
							)
								continue;

							ithMedia.downloadData = {};
							if (env.options.flat_downloads)
								ithMedia.downloadData.subalbum = "";
							else if (true || self.isCollection())
								ithMedia.downloadData.subalbum = ithMedia.parentAlbum.ancestorsNames.slice(1).join("/");
							else
								ithMedia.downloadData.subalbum = subalbum.substring(albumPathLength + 1);

							includedMedia.push(ithMedia);
							let sizeToAdd;
							if (
								ithMedia.isImage() && env.options.download_images_size_index === "original" ||
								ithMedia.isAudio() && env.options.download_audios_size === "original" ||
								ithMedia.isVideo() && env.options.download_videos_size === "original"
							) {
								sizeToAdd = ithMedia.fileSizes.original.imagesAudiosVideosTotal();
							} else if (
								ithMedia.isImage() && env.options.download_images_size_index === "fullSize" ||
								ithMedia.isAudio() && env.options.download_audios_size === "fullSize" ||
								ithMedia.isVideo() && env.options.download_videos_size === "fullSize"
							) {
							sizeToAdd = ithMedia.fileSizes[env.options.download_images_format][0].imagesAudiosVideosTotal();
							} else {
								if (ithMedia.isImage())
									sizeToAdd = ithMedia.fileSizes[env.options.download_images_format][
										env.options.reduced_sizes[env.options.download_images_size_index]
									].imagesAudiosVideosTotal();
								else
									sizeToAdd = ithMedia.fileSizes[env.options.download_images_format][env.options.reduced_sizes[0]].imagesAudiosVideosTotal();
							}
							totalSize += sizeToAdd;
						}
					}

					if (env.options.download_subalbums && ! stop) {
						// sort subalbums: regular albums, then by date ones, then by gps ones, then searches, then maps
						let regulars = [], byDate = [], byGps = [], searches = [], fromMap = [], selections = [];
						let sortedSubalbums = [];
						let visibleSubalbums = album.visibleSubalbums();
						for (let iVisibleSubalbum = 0; iVisibleSubalbum < visibleSubalbums.length; iVisibleSubalbum ++) {
							let ithSubalbum = visibleSubalbums[iVisibleSubalbum];

							if (ithSubalbum.isFolder())
								regulars.push(ithSubalbum);
							if (ithSubalbum.isByDate())
								byDate.push(ithSubalbum);
							if (ithSubalbum.isByGps())
								byGps.push(ithSubalbum);
							if (ithSubalbum.isSearch())
								searches.push(ithSubalbum);
							if (ithSubalbum.isMap())
								fromMap.push(ithSubalbum);
							if (ithSubalbum.isSelection())
								selections.push(ithSubalbum);
						}
						sortedSubalbums.push(... regulars);
						sortedSubalbums.push(... byDate);
						sortedSubalbums.push(... byGps);
						sortedSubalbums.push(... searches);
						sortedSubalbums.push(... fromMap);
						sortedSubalbums.push(... selections);

						for (let i = 0; i < sortedSubalbums.length; i ++) {
							if (stop)
								break;
							// let iVisibleSubalbum = visibleSubalbums.findIndex(subalbum => sortedSubalbums[i].isEqual(subalbum));
							let ithSubalbum = sortedSubalbums[i];
							let subalbumPromise = new Promise(
								function(resolveSubalbumPromise) {
									let convertSubalbumPromise = ithSubalbum.toAlbum(null, {getMedia: true, getPositions: false});
									convertSubalbumPromise.then(
										function(ithAlbum) {
											album.subalbums[album.subalbums.findIndex(subalbum => subalbum.isEqual(ithSubalbum))] = ithAlbum;
											let ancestorsNamesList = ithAlbum.ancestorsNames.slice(1);
											if (ancestorsNamesList.length > 2) {
												ancestorsNamesList[2] = util.transformAltPlaceName(ancestorsNamesList[2]);
											}
											let albumPath = ancestorsNamesList.join("/");
											let buildMediaListPromise = buildMediaList(ithAlbum, albumPath);
											buildMediaListPromise.then(
												function() {
													resolveSubalbumPromise();
												}
											);
										}
									);
								}
							);
							albumPromises.push(subalbumPromise);
						}
					}

					Promise.all(albumPromises).then(
						function() {
							if (! stop)
								resolve_buildMediaList();
						}
					);
				}
			);
		}

		function getSingleMediaBinaryContent(iVisibleMedia, howMany) {
			var ithMedia = includedMedia[iVisibleMedia];

			return new Promise(
				function(resolve_getSingleMediaBinaryContent) {
					if (stop)
						resolve_getSingleMediaBinaryContent();

					let url;
					let format;
					if (ithMedia.isImage())
						format = env.options.download_images_format;
					else if (ithMedia.isAudio())
						format = "mp3";
					else if (ithMedia.isVideo())
						format = "mp4";

					if (
						ithMedia.isImage() && env.options.download_images_size_index === "original" ||
						ithMedia.isAudio() && env.options.download_audios_size === "original" ||
						ithMedia.isVideo() && env.options.download_videos_size === "original"
					) {
						url = encodeURI(ithMedia.albumMediaPath());
					} else if (
						ithMedia.isImage() && env.options.download_images_size_index === "fullSize" ||
						ithMedia.isAudio() && env.options.download_audios_size === "fullSize" ||
						ithMedia.isVideo() && env.options.download_videos_size === "fullSize"
					) {
						url = encodeURI(ithMedia.fullSizeMediaPath(format));
					} else {
						if (ithMedia.isImage) {
							url = encodeURI(
								ithMedia.reducedMediaPath(
									env.options.reduced_sizes[env.options.download_images_size_index],
									format
								)
							);
						} else {
							url = encodeURI(
								ithMedia.reducedMediaPath(
									env.options.reduced_sizes[0],
									format
								)
							);
						}
					}
					let name = ithMedia.name;
					if (
						ithMedia.isImage() &&
						name.substr(- format.length - 1).toLowerCase() !== "." + format &&
						! (format == "jpg "&& name.substr(-5).toLowerCase() !== ".jpeg") ||
						ithMedia.isAudio() &&
						name.substr(- format.length - 1).toLowerCase() !== "." + format ||
						ithMedia.isVideo() &&
						name.substr(- format.length - 1).toLowerCase() !== "." + format
					)
						name = name + "." + format;
					// load a file and add it to the zip file
					JSZipUtils.getBinaryContent(
						url,
						function (err, data) {
							if (stop)
								return;
							if (err) {
								throw err; // or handle the error
							}
							let fileName = name;
							if (ithMedia.downloadData.subalbum)
								fileName = util.pathJoin([ithMedia.downloadData.subalbum, fileName]);

							if (env.options.flat_downloads) {
								// ensure that each file name is unique

								const match = fileName.match(/ (\()\d+(\))$/);
								const squareParenthesis = (match && match[1] === "[" && match[2] === "]");
								let openParenthesis = "[";
								let closeParenthesis = "]";
								if (squareParenthesis) {
									openParenthesis = "(";
									closeParenthesis = ")";
								}

								let splittedName = fileName.split(".");
								const extension = splittedName.pop();
								const baseName = splittedName.join(".");
								let suffixIndex = 1;
								while (fileNames.includes(fileName)) {
									fileName = [baseName + " " + openParenthesis + suffixIndex.toString() + closeParenthesis, extension].join(".");
									suffixIndex ++;
								}
							}
							fileNames.push(fileName);

							let ithSize;
							if (
								ithMedia.isImage() && env.options.download_images_size_index === "original" ||
								ithMedia.isAudio() && env.options.download_audios_size === "original" ||
								ithMedia.isVideo() && env.options.download_videos_size === "original"
							) {
								ithSize = ithMedia.fileSizes.original.imagesAudiosVideosTotal();
							} else if (
								ithMedia.isImage() && env.options.download_images_size_index === "fullSize" ||
								ithMedia.isAudio() && env.options.download_audios_size === "fullSize" ||
								ithMedia.isVideo() && env.options.download_videos_size === "fullSize"
							) {
								ithSize = ithMedia.fileSizes[env.options.download_images_format][0].imagesAudiosVideosTotal();
							} else {
								if (ithMedia.isImage())
									ithSize = ithMedia.fileSizes[env.options.download_images_format][
										env.options.reduced_sizes[env.options.download_images_size_index]
									].imagesAudiosVideosTotal();
								else
									ithSize = ithMedia.fileSizes[env.options.download_images_format][env.options.reduced_sizes[0]].imagesAudiosVideosTotal();
							}
							partialSize += ithSize;

							$("#file-name-for-adding-to-zip").html(
								fileName +
								env.br +
								util.humanFileSize(ithSize) +
								env.br +
								(partialSize / totalSize * 100).toFixed(1) + " %"
							);
							zip.file(fileName, data, {binary:true});
							if (iVisibleMedia + howMany >= includedMedia.length) {
								resolve_getSingleMediaBinaryContent();
							} else {
								let newPromise = getSingleMediaBinaryContent(iVisibleMedia + howMany, howMany);
								newPromise.then(
									function() {
										resolve_getSingleMediaBinaryContent();
									}
								);
							}
						}
					);
				}
			);
		}

		function getMediaBinaryContent() {
			return new Promise(
				function(resolve_getMediaBinaryContent) {
					$("#file-name-for-adding-to-zip").show();
					partialSize = 0;
					var howMany = Math.max(Math.ceil(includedMedia.length / 10), 4);
					var promises = [];
					for (let iVisibleMedia = 0; iVisibleMedia < Math.min(howMany, includedMedia.length); iVisibleMedia ++) {
						let ithMediaPromise = getSingleMediaBinaryContent(iVisibleMedia, howMany);
						promises.push(ithMediaPromise);
					}
					Promise.all(promises).then(
						function() {
							resolve_getMediaBinaryContent();
						}
					);
				}
			);
		}
	};

	Album.prototype.clickSelectEverythingHere = function(selectionAlbum, geotaggedContentIsHidden) {
		let self = this;
		geotaggedContentIsHidden = geotaggedContentIsHidden ? 1 : 0;
		return new Promise(
			function(resolve_everythingPromise) {
				if (
					(
						! env.options.selectable_albums || util.isPopup() || self.everySubalbumIsSelected(selectionAlbum, geotaggedContentIsHidden)
					) &&
					self.everyMediaIsSelected(selectionAlbum, geotaggedContentIsHidden)
				) {
					self.removeAllMediaFromSelection(selectionAlbum, geotaggedContentIsHidden);
					if (env.options.selectable_albums && ! util.isPopup()) {
						let promise = self.removeAllSubalbumsFromSelection(selectionAlbum, geotaggedContentIsHidden);
						promise.then(
							function() {
								resolve_everythingPromise();
							}
						);
					} else {
						resolve_everythingPromise();
					}
				} else {
					self.addAllMediaToSelection(selectionAlbum, geotaggedContentIsHidden);
					if (env.options.selectable_albums) {
						let promise = self.addAllSubalbumsToSelection(selectionAlbum, geotaggedContentIsHidden);
						promise.then(
							function() {
								delete selectionAlbum.mediaSort;
								delete selectionAlbum.mediaReverseSort;
								selectionAlbum.sortAlbumsMedia();

								resolve_everythingPromise();
							}
						);
					} else {
						delete selectionAlbum.mediaSort;
						delete selectionAlbum.mediaReverseSort;
						selectionAlbum.sortAlbumsMedia();

						resolve_everythingPromise();
					}
				}
			}
		);
	};

	Album.prototype.clickSelectEverythingIndividual = function(selectionAlbum, geotaggedContentIsHidden) {
		let self = this;
		geotaggedContentIsHidden = geotaggedContentIsHidden ? 1 : 0;
		return new Promise(
			function (resolve_everythingIndividualPromise) {
				let everythingIndividualPromise = self.recursivelyAllMediaAreSelected(selectionAlbum, geotaggedContentIsHidden);
				everythingIndividualPromise.then(
					function isTrue() {
						let firstPromise = self.recursivelyRemoveMediaFromSelection(selectionAlbum, geotaggedContentIsHidden);
						firstPromise.then(
							function recursivelyAllMediaRemoved() {
								resolve_everythingIndividualPromise();
							}
						);
					},
					function isFalse() {
						let firstPromise = self.recursivelySelectAllMedia(selectionAlbum, geotaggedContentIsHidden);
						firstPromise.then(
							function recursivelyAllMediaSelected() {
								delete selectionAlbum.mediaSort;
								delete selectionAlbum.mediaReverseSort;
								selectionAlbum.sortAlbumsMedia();

								resolve_everythingIndividualPromise();
							}
						);
					}
				);
			}
		);
	};

	Album.prototype.clickSelectMediaHere = function(selectionAlbum, geotaggedContentIsHidden) {
		geotaggedContentIsHidden = geotaggedContentIsHidden ? 1 : 0;

		if (this.everyMediaIsSelected(selectionAlbum, geotaggedContentIsHidden)) {
			this.removeAllMediaFromSelection(selectionAlbum, geotaggedContentIsHidden);
		} else {
			this.addAllMediaToSelection(selectionAlbum, geotaggedContentIsHidden);

			delete selectionAlbum.mediaSort;
			delete selectionAlbum.mediaReverseSort;
			selectionAlbum.sortAlbumsMedia();
		}
	};

	Album.prototype.clickSelectAlbumsHere = function(selectionAlbum, geotaggedContentIsHidden) {
		geotaggedContentIsHidden = geotaggedContentIsHidden ? 1 : 0;
		let self = this;
		return new Promise(
			function(resolve_clickSelectAlbums) {
				if (self.everySubalbumIsSelected(selectionAlbum, geotaggedContentIsHidden)) {
					let promise = self.removeAllSubalbumsFromSelection(selectionAlbum, geotaggedContentIsHidden);
					promise.then(
						function allSubalbumsRemoved() {
							resolve_clickSelectAlbums();
						}
					);
				} else {
					var promise = self.addAllSubalbumsToSelection(selectionAlbum, geotaggedContentIsHidden);
					promise.then(
						function allSubalbumsAdded() {
							delete selectionAlbum.mediaSort;
							delete selectionAlbum.mediaReverseSort;
							selectionAlbum.sortAlbumsMedia();

							resolve_clickSelectAlbums();
						}
					);
				}
			}
		);
	};

	Album.prototype.clickSelectGlobalResetHere = function(selectionAlbum, geotaggedContentIsHidden) {
		let self = this;
		return new Promise(
			function(resolve_clickSelectGlobalResetHere) {
				let promises = [];
				let mediaPromise = self.recursivelyRemoveMediaFromSelection(selectionAlbum, geotaggedContentIsHidden);
				promises.push(mediaPromise);

				if (env.options.selectable_albums) {
					let subalbumsPromise = self.removeAllSubalbumsFromSelection(selectionAlbum, geotaggedContentIsHidden);
					promises.push(subalbumsPromise);
				}

				Promise.all(promises).then(
					function() {
						resolve_clickSelectGlobalResetHere();
					}
				);
			}
		);
	};

	Album.prototype.clickSelectNothingHere = function(selectionAlbum, geotaggedContentIsHidden) {
		let self = this;
		geotaggedContentIsHidden = geotaggedContentIsHidden ? 1 : 0;
		return new Promise(
			function(resolve_clickSelectNothing) {
				self.removeAllMediaFromSelection(selectionAlbum, geotaggedContentIsHidden);
				if (env.options.selectable_albums) {
					let subalbumsPromise = self.removeAllSubalbumsFromSelection(selectionAlbum, geotaggedContentIsHidden);
					subalbumsPromise.then(
						function allSubalbumsRemoved() {
							resolve_clickSelectNothing();
						}
					);
				} else {
					resolve_clickSelectNothing();
				}
			}
		);
	};

	Album.prototype.clickSelectNoAlbumsHere = function(selectionAlbum, geotaggedContentIsHidden) {
		let self = this;
		geotaggedContentIsHidden = geotaggedContentIsHidden ? 1 : 0;

		return new Promise(
			function(resolve_clickSelectNoAlbums) {
				let subalbumsPromise = self.removeAllSubalbumsFromSelection(selectionAlbum, geotaggedContentIsHidden);
				subalbumsPromise.then(
					function allSubalbumsRemoved() {
						resolve_clickSelectNoAlbums();
					}
				);
			}
		);
	};

	Album.prototype.clickSelectNoMediaHere = function(selectionAlbum, geotaggedContentIsHidden) {
		geotaggedContentIsHidden = geotaggedContentIsHidden ? 1 : 0;
		this.removeAllMediaFromSelection(selectionAlbum, geotaggedContentIsHidden);
	};


	Album.prototype.noSubalbumIsSelected = function(selectionAlbum, geotaggedContentIsHidden) {
		if (util.absolutelyNothingIsSelected(selectionAlbum))
			return true;

		return ! this.visibleSubalbums(geotaggedContentIsHidden).some(subalbum => subalbum.isSelected(selectionAlbum));
	};

	// Album.prototype.someSubalbumIsSelected = function(selectionAlbum, geotaggedContentIsHidden) {
	// 	return ! this.noSubalbumIsSelected(selectionAlbum, geotaggedContentIsHidden);
	// };

	Album.prototype.noMediaIsSelected = function(selectionAlbum, geotaggedContentIsHidden) {
		if (util.absolutelyNothingIsSelected(selectionAlbum))
			return true;

		return ! this.visibleMedia(geotaggedContentIsHidden).some(singleMedia => singleMedia.isSelected(selectionAlbum));
	};

	// Album.prototype.someMediaIsSelected = function(selectionAlbum, geotaggedContentIsHidden) {
	// 	return ! this.noMediaIsSelected(selectionAlbum, geotaggedContentIsHidden);
	// };

	Album.prototype.everyMediaIsSelected = function(selectionAlbum, geotaggedContentIsHidden) {
		if (util.absolutelyNothingIsSelected(selectionAlbum))
			return false;

		return this.visibleMedia(geotaggedContentIsHidden).every(singleMedia => singleMedia.isSelected(selectionAlbum));
	};

	Album.prototype.everySubalbumIsSelected = function(selectionAlbum, geotaggedContentIsHidden) {
		if (util.absolutelyNothingIsSelected(selectionAlbum))
			return false;

		return this.visibleSubalbums(geotaggedContentIsHidden).every(subalbum => subalbum.isSelected(selectionAlbum));
	};

	//SelectionAlbum.prototype.recursivelySelectAllMedia = function(baseAlbum, geotaggedContentIsHidden) {
	Album.prototype.recursivelyapplyAlbumMethodToAllMedia = function(albumMethod, selectionAlbum, geotaggedContentIsHidden) {
		var self = this;
		return new Promise(
			function (resolve_promise) {
				albumMethod.apply(self, [selectionAlbum, geotaggedContentIsHidden]);
				// self.addAllMediaToSelection(selectionAlbum, geotaggedContentIsHidden);
				let promises = [];
				let visibleSubalbums = self.visibleSubalbums(geotaggedContentIsHidden);
				for (let i = 0; i < visibleSubalbums.length; i ++) {
					let iVisibleSubalbum = i;
					let ithSubalbum = visibleSubalbums[iVisibleSubalbum];
					let ithPromise = new Promise(
						function(resolve_ithPromise) {
							let convertSubalbumPromise = ithSubalbum.toAlbum(null, {getMedia: true, getPositions: false});
							convertSubalbumPromise.then(
								function(ithAlbum) {
									self.subalbums[
										self.subalbums.findIndex(subalbum => subalbum.isEqual(visibleSubalbums[iVisibleSubalbum]))
									] = ithAlbum;
									let promise = ithAlbum.recursivelyapplyAlbumMethodToAllMedia(
										albumMethod,
										selectionAlbum,
										geotaggedContentIsHidden
									);
									promise.then(
										function() {
											resolve_ithPromise();
										}
									);
								}
							);
						}
					);
					promises.push(ithPromise);
				}
				Promise.all(promises).then(
					function() {
						resolve_promise();
					}
				);
			}
		);
	};

	Album.prototype.recursivelySelectAllMedia = function(selectionAlbum, geotaggedContentIsHidden) {
		var self = this;
		return new Promise(
			function (resolve_promise) {
				let promise = self.recursivelyapplyAlbumMethodToAllMedia(
					Album.prototype.addAllMediaToSelection,
					selectionAlbum,
					geotaggedContentIsHidden
				);
				promise.then(
					function() {
						delete selectionAlbum.mediaSort;
						delete selectionAlbum.mediaReverseSort;
						selectionAlbum.sortAlbumsMedia();

						resolve_promise();
					}
				);
			}
		);
	};

	Album.prototype.recursivelyRemoveMediaFromSelection = function(selectionAlbum, geotaggedContentIsHidden) {
		var self = this;
		return new Promise(
			function (resolve_promise) {
				let promise = self.recursivelyapplyAlbumMethodToAllMedia(
					Album.prototype.removeAllMediaFromSelection,
					selectionAlbum,
					geotaggedContentIsHidden
				);
				promise.then(
					function() {
						delete selectionAlbum.mediaSort;
						delete selectionAlbum.mediaReverseSort;
						selectionAlbum.sortAlbumsMedia();

						resolve_promise();
					}
				);
			}
		);
	};

	Album.prototype.recursivelyAllMediaAreSelected = function(selectionAlbum, geotaggedContentIsHidden) {
		var self = this;
		return new Promise(
			function (resolve_promise, reject_promise) {
				if (! self.everyMediaIsSelected(selectionAlbum, geotaggedContentIsHidden)) {
					reject_promise();
				} else {
					let promises = [];
					let visibleSubalbums = self.visibleSubalbums(geotaggedContentIsHidden);
					for (let i = 0; i < visibleSubalbums.length; i ++) {
						let iVisibleSubalbum = i;
						let ithSubalbum = visibleSubalbums[iVisibleSubalbum];
						let ithPromise = new Promise(
							function(resolve_ithPromise, reject_ithPromise) {
								let convertSubalbumPromise = ithSubalbum.toAlbum(null, {getMedia: true, getPositions: false});
								convertSubalbumPromise.then(
									function(ithAlbum) {
										self.subalbums[
											self.subalbums.findIndex(subalbum => subalbum.isEqual(visibleSubalbums[iVisibleSubalbum]))
										] = ithAlbum;
										let promise = ithAlbum.recursivelyAllMediaAreSelected(selectionAlbum, geotaggedContentIsHidden);
										promise.then(
											function() {
												resolve_ithPromise();
											},
											function() {
												reject_ithPromise();
											}
										);
									}
								);
							}
						);
						promises.push(ithPromise);
					}
					Promise.all(promises).then(
						function() {
							resolve_promise(true);
						},
						function() {
							reject_promise(false);
						}
					);
				}
			}
		);
	};

	Album.prototype.callSingleMediaMethodOnAllMedia = function(singleMediaMethod, selectionAlbum, geotaggedContentIsHidden) {
		let visibleMedia = this.visibleMedia(geotaggedContentIsHidden);
		for (let indexVisibleMedia = visibleMedia.length - 1; indexVisibleMedia >= 0; indexVisibleMedia --) {
			singleMediaMethod.call(visibleMedia[indexVisibleMedia], selectionAlbum, geotaggedContentIsHidden);
		}
	};

	Album.prototype.addAllMediaToSelection = function(selectionAlbum, geotaggedContentIsHidden) {
		this.callSingleMediaMethodOnAllMedia(SingleMedia.prototype.addToSelection, selectionAlbum, geotaggedContentIsHidden);
	};

	Album.prototype.removeAllMediaFromSelection = function(selectionAlbum, geotaggedContentIsHidden) {
		this.callSingleMediaMethodOnAllMedia(SingleMedia.prototype.removeFromSelection, selectionAlbum, geotaggedContentIsHidden);
	};

	Album.prototype.callSubalbumMethodOnAllSubalbums = function(subalbumMethod, selectionAlbum, geotaggedContentIsHidden) {
		let visibleSubalbums = this.visibleSubalbums(geotaggedContentIsHidden);
		let self = this;
		return new Promise(
			function(resolve_executeMethodOnAllSubalbums) {
				let promises = [];
				for (let indexVisibleSubalbum = visibleSubalbums.length - 1; indexVisibleSubalbum >= 0; indexVisibleSubalbum --) {
					let methodPromise = subalbumMethod.call(
						self,
						visibleSubalbums[indexVisibleSubalbum],
						selectionAlbum
					);
					promises.push(methodPromise);
				}
				Promise.all(promises).then(
					function() {
						resolve_executeMethodOnAllSubalbums();
					}
				);
			}
		);
	};

	Album.prototype.addAllSubalbumsToSelection = function(selectionAlbum, geotaggedContentIsHidden) {
		let self = this;
		return new Promise(
			function(resolve_addAllSubalbumsToSelection) {
				let promise = self.callSubalbumMethodOnAllSubalbums(
					Album.prototype.addSubalbumToSelection,
					selectionAlbum,
					geotaggedContentIsHidden
				);
				promise.then(
					function() {
						resolve_addAllSubalbumsToSelection();
					}
				);
			}
		);
	};

	Album.prototype.removeAllSubalbumsFromSelection = function(selectionAlbum, geotaggedContentIsHidden) {
		let self = this;
		return new Promise(
			function(resolve_removeAllSubalbumsFromSelection) {
				let promise = self.callSubalbumMethodOnAllSubalbums(
					Album.prototype.removeSubalbumFromSelection,
					selectionAlbum,
					geotaggedContentIsHidden
				);
				promise.then(
					function() {
						resolve_removeAllSubalbumsFromSelection();
					}
				);
			}
		);
	};

	Album.prototype.isInsideSelectedAlbums = function(selectionAlbum) {
		return util.isInsideSelectedAlbums(this, selectionAlbum);
	};

	Album.prototype.addSubalbumToSelection = function(ithSubalbum, selectionAlbum) {
		var self = this;
		return new Promise(
			function(resolve_addSubalbum) {
				///// sub-function //////////
				function continue_addSubalbumToSelection(ithAlbum) {
					selectionAlbum.subalbums.push(ithAlbum);

					delete selectionAlbum.albumSort;
					delete selectionAlbum.albumReverseSort;
					selectionAlbum.sortAlbumsMedia();

					self.invalidatePositionsAndMediaInAlbumAndSubalbums();

					resolve_addSubalbum();
				}
				///// end of sub-function //////////

				if (ithSubalbum.isSelected(selectionAlbum)) {
					resolve_addSubalbum();
				} else {
					let convertSubalbumPromise = ithSubalbum.toAlbum(null, {getMedia: true, getPositions: ! env.options.save_data});
					convertSubalbumPromise.then(
						function(ithAlbum) {
							self.subalbums[self.subalbums.findIndex(subalbum => subalbum.isEqual(ithSubalbum))] = ithAlbum;

							var albumIsInsideSelectedAlbums = ithAlbum.isInsideSelectedAlbums(selectionAlbum);
							if (albumIsInsideSelectedAlbums) {
								continue_addSubalbumToSelection(ithAlbum);
							} else {
								var collectPromise = ithAlbum.collectMediaInTree();
								collectPromise.then(
									function(mediaInAlbumTree) {
										var mediaInAlbumNotSelectedNorInsideSelectedAlbums = [];
										var mediaInAlbumNotSelectedNorInsideSelectedAlbumsNorGeotagged = [];
										mediaInAlbumTree.forEach(
											function(singleMedia) {
												if (! singleMedia.isInsideSelectedAlbums(selectionAlbum) && ! singleMedia.isSelected(selectionAlbum)) {
													mediaInAlbumNotSelectedNorInsideSelectedAlbums.push(singleMedia);
													if (! singleMedia.hasGpsData())
														mediaInAlbumNotSelectedNorInsideSelectedAlbumsNorGeotagged.push(singleMedia);
												}
											}
										);

										mediaInAlbumNotSelectedNorInsideSelectedAlbums.forEach(
											singleMedia => {
												if (singleMedia.hasGpsData()) {
													selectionAlbum.positionsAndMediaInTree.addSingleMedia(singleMedia);
												}
												selectionAlbum.sizesOfSubTree.sum(singleMedia.fileSizes);
											}
										);
										if (env.options.expose_image_positions)
											mediaInAlbumNotSelectedNorInsideSelectedAlbumsNorGeotagged.forEach(
												singleMedia => {
													selectionAlbum.nonGeotagged.sizesOfSubTree.sum(singleMedia.fileSizes);
												}
											);
										selectionAlbum.numPositionsInTree = selectionAlbum.positionsAndMediaInTree.length;

										selectionAlbum.numsMediaInSubTree.sum(new Media(mediaInAlbumNotSelectedNorInsideSelectedAlbums).imagesAudiosVideosCount());
										if (env.options.expose_image_positions)
											selectionAlbum.nonGeotagged.numsMediaInSubTree.sum(
												new Media(mediaInAlbumNotSelectedNorInsideSelectedAlbumsNorGeotagged).imagesAudiosVideosCount()
											);

										continue_addSubalbumToSelection(ithAlbum);
									}
								);
							}
						}
					);
				}
			}
		);
	};

	Album.prototype.removeSubalbumFromSelection = function(clickedSubalbum, selectionAlbum) {
		if (! $.getTriggersStatus().otherJsFilesLoadedEvent)
			return;

		var self = this;
		let iClickedSubalbum = self.subalbums.findIndex(
			ithSubalbum => ithSubalbum.isEqual(clickedSubalbum)
		);

		return new Promise(
			function(resolve_removeSubalbum) {
				if (! clickedSubalbum.isSelected(selectionAlbum)) {
					resolve_removeSubalbum();
				} else {
					let convertSubalbumPromise = clickedSubalbum.toAlbum(null, {getMedia: true, getPositions: ! env.options.save_data});
					convertSubalbumPromise.then(
						function(clickedAlbum) {
							self.subalbums[iClickedSubalbum] = clickedAlbum;

							var selectedMediaInsideSelectedAlbums = [];
							selectionAlbum.media.forEach(
								function(singleMedia) {
									if (singleMedia.isInsideSelectedAlbums(selectionAlbum))
										selectedMediaInsideSelectedAlbums.push(singleMedia);
								}
							);

							var subalbumIsInsideSelectedAlbums = clickedAlbum.isInsideSelectedAlbums(selectionAlbum);

							var indexInSelection = selectionAlbum.subalbums.findIndex(selectedSubalbum => selectedSubalbum.isEqual(clickedAlbum));
							selectionAlbum.subalbums.splice(indexInSelection, 1);

							if (
								selectionAlbum.hasOwnProperty("positionsAndMediaInTree") && selectionAlbum.positionsAndMediaInTree.length &&
								clickedAlbum.hasOwnProperty("positionsAndMediaInTree") && clickedAlbum.positionsAndMediaInTree.length
							) {
								selectionAlbum.positionsAndMediaInTree.removePositionsAndMedia(clickedAlbum.positionsAndMediaInTree);
								selectionAlbum.numPositionsInTree = selectionAlbum.positionsAndMediaInTree.length;
							}

							if (! subalbumIsInsideSelectedAlbums) {
								selectionAlbum.numsMediaInSubTree.subtract(clickedAlbum.numsMediaInSubTree);
								selectionAlbum.sizesOfSubTree.subtract(clickedAlbum.sizesOfSubTree);
								if (env.options.expose_image_positions) {
									selectionAlbum.nonGeotagged.numsMediaInSubTree.subtract(clickedAlbum.nonGeotagged.numsMediaInSubTree);
									selectionAlbum.nonGeotagged.sizesOfSubTree.subtract(clickedAlbum.nonGeotagged.sizesOfSubTree);
								}
							}

							delete selectionAlbum.albumSort;
							delete selectionAlbum.albumReverseSort;
							selectionAlbum.sortAlbumsMedia();

							self.invalidatePositionsAndMediaInAlbumAndSubalbums();

							resolve_removeSubalbum();
						}
					);
				}
			}
		);
	};
}());
//# sourceURL=041-album-methods-bis.js
