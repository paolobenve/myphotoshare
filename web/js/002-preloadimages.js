(function($) {
	var cache = [];
	$.preloadImages = function() {
		var args_len = arguments.length;
		for (var i = args_len; i--;) {
			var cacheImage = document.createElement('img');
			cacheImage.src = arguments[i];
			cache.push(cacheImage);
		}
	};

	var triggers = {};

	$.resetEvent = function(eventName) {
		// $(document).off(eventName);
		triggers[eventName] = false;
	};

	$.triggerEvent = function(eventName) {
		if (! triggers[eventName]) {
			$(document).trigger(eventName);
			triggers[eventName] = true;
		}
	};

	$.getTriggersStatus = function() {
		return triggers;
	};

	$.executeAfterEvents = function(eventNamesArray, f) {
		if (eventNamesArray.length === 1)
			$.executeAfterEvent(eventNamesArray[0], f);
		else
			$.executeAfterEvent(
				eventNamesArray[0],
				function() {
					$.executeAfterEvents(eventNamesArray.slice(1), f);
				}
			);
	};

	$.executeAfterEvent = function(eventName, f) {
		if (! triggers.hasOwnProperty(eventName))
			triggers[eventName] = false;

		if (! triggers[eventName]) {
			$(document).on(
				eventName,
				function() {
					triggers[eventName] = true;
					f();
				}
			);
		} else {
			f();
		}
	};
})(jQuery);
