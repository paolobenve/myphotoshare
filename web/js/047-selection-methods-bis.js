(function() {

	var util = new Utilities();
	var phFl = new PhotoFloat();
	var menuF = new MenuFunctions();

	SelectionAlbum.prototype.playSelectionClickHistory = function(cacheBaseIndex = 0) {
		let self = this;
		return new Promise(
			function(resolve_playSelectionClickHistory) {
				var selectionClickHistoryElement = self.selectionClickHistory[cacheBaseIndex];
				var oneCacheBasePromise = new Promise(
					function(resolve_oneCacheBasePromise) {
						let ithAlbumCacheBase = Object.keys(selectionClickHistoryElement)[0];
						let getAlbumPromise = phFl.getAlbum(ithAlbumCacheBase, menuF.errorOpeningUnprotectedJsonFiles, {getMedia: true, getPositions: false});
						getAlbumPromise.then(
							function(albumForSelecting) {
								// begin of nested function
								function performClicks(indexSelector = 0) {
									function recurseOrResolveUpperPromise() {
										if (indexSelector === clickedSelectors.length - 1)
											resolve_oneCacheBasePromise();
										else
											performClicks(indexSelector + 1);
									}

									let clickedSelector = clickedSelectors[indexSelector];
									// clickedSelector has geotagged content showing info for collective commands
									let hideGeotaggedContent = null;
									if (util.hasGeoContentInfo(clickedSelector))
										[clickedSelector, hideGeotaggedContent] = util.separateSelectorWithGeoContentInfo(clickedSelector);

									if (
										[
											env.albumSelectBoxIdBeginning,
											env.mediaSelectBoxIdBeginning,
											env.mediaMapSelectBoxIdBeginning
										].some(
											id => (
												! Object.keys(env.selectIdsConvertionTable).includes(clickedSelector.substr(1)) &&
												clickedSelector.startsWith("#" + id)
											)
										)
									) {
										let albumCacheBase;
										if (clickedSelector.startsWith("#" + env.albumSelectBoxIdBeginning)) {
											albumCacheBase = clickedSelector.substr(1 + env.albumSelectBoxIdBeginning.length);
											let subalbumForSelecting = albumForSelecting.subalbums.find(
												subalbum => subalbum.albumSelectBoxSelector() === clickedSelector
											);
											let togglePromise = subalbumForSelecting.toggleSubalbumSelection(albumForSelecting, self);
											togglePromise.then(recurseOrResolveUpperPromise);
										} else if (clickedSelector.startsWith("#" + env.mediaSelectBoxIdBeginning)) {
											let splittedId = clickedSelector.substr(1 + env.mediaSelectBoxIdBeginning.length).split(env.options.cache_folder_separator);
											splittedId.pop();
											albumCacheBase = splittedId.join(env.options.cache_folder_separator);
											let singleMediaForSelecting = albumForSelecting.media.find(
												singleMedia => singleMedia.mediaSelectBoxSelector() === clickedSelector
											);
											singleMediaForSelecting.toggleSingleMediaSelection(self);
											recurseOrResolveUpperPromise();
										} else {
											let splittedId = clickedSelector.substr(1 + env.mediaMapSelectBoxIdBeginning.length).split(env.options.cache_folder_separator);
											splittedId.pop();
											albumCacheBase = splittedId.join(env.options.cache_folder_separator);
											let mediaForSelecting = albumForSelecting.media.find(
												singleMedia => singleMedia.mediaSelectBoxSelector(true) === clickedSelector
											);
											mediaForSelecting.toggleSingleMediaSelection(self);
											recurseOrResolveUpperPromise();
										}
									} else if (clickedSelector === "#everything-here") {
										let selectEverythingPromise = albumForSelecting.clickSelectEverythingHere(self, hideGeotaggedContent);
										selectEverythingPromise.then(
											function() {
												recurseOrResolveUpperPromise();
											}
										);
									} else if (clickedSelector === "#every-media-individual") {
										let selectEverythingIndividualPromise = albumForSelecting.clickSelectEverythingIndividual(self, hideGeotaggedContent);
										selectEverythingIndividualPromise.then(
											function() {
												recurseOrResolveUpperPromise();
											}
										);
									} else if (clickedSelector === "#media-here") {
										albumForSelecting.clickSelectMediaHere(self);
										recurseOrResolveUpperPromise();
									} else if (clickedSelector === "#albums-here") {
										let selectAlbumsPromise = albumForSelecting.clickSelectAlbumsHere(self, hideGeotaggedContent);
										selectAlbumsPromise.then(
											function() {
												recurseOrResolveUpperPromise();
											}
										);
									// no need to intercept "#global-reset", because that action always resets the selection click history
									} else if (clickedSelector === "#nothing-here") {
										let selectNothingPromise = albumForSelecting.clickSelectNothingHere(self, hideGeotaggedContent);
										selectNothingPromise.then(
											function() {
												recurseOrResolveUpperPromise();
											}
										);
									} else if (clickedSelector === "#no-albums") {
										let selectNoAlbumsPromise = albumForSelecting.clickSelectNoAlbumsHere(self, hideGeotaggedContent);
										selectNoAlbumsPromise.then(
											function() {
												recurseOrResolveUpperPromise();
											}
										);
									} else if (clickedSelector === "#no-media") {
										albumForSelecting.clickSelectNoMediaHere(self, hideGeotaggedContent);
										recurseOrResolveUpperPromise();
									}
								}
								// end of nested function

								let clickedSelectors = Object.values(selectionClickHistoryElement)[0];
								performClicks();
							}
						);
					}
				);

				oneCacheBasePromise.then(
					function() {
						if (cacheBaseIndex < self.selectionClickHistory.length - 1) {
							let newPlayPromise = self.playSelectionClickHistory(cacheBaseIndex + 1);
							newPlayPromise.then(
								function() {
									resolve_playSelectionClickHistory();
								}
							);
						} else {
							// cacheBaseIndex is selectionClickHistory.length -1
							resolve_playSelectionClickHistory();
						}
					},
					function(album) {
						console.trace();
					}
				);
			}
		);
	};

	SelectionAlbum.prototype.addToSelectionClickHistory = function(baseAlbum, clickedSelector, geotaggedContentIsHidden) {
		// this method is called on the selection album
		function cacheBase(clickHistoryElement) {
			return Object.keys(clickHistoryElement)[0];
		}

		function clickedSelectors(clickHistoryElement) {
			return Object.values(clickHistoryElement)[0];
		}

		let mediaBeginnings = [
			env.mediaSelectBoxIdBeginning,
			env.mediaMapSelectBoxIdBeginning
		];

		geotaggedContentIsHidden = geotaggedContentIsHidden ? 1 : 0;

		let albumCacheBase = baseAlbum.cacheBase;
		if (! util.isFolderCacheBase(albumCacheBase)) {
			// for media and subalbums clicked selectors, use the album cache base getting it from the selector itself
			for (const beginning of [... mediaBeginnings, env.albumSelectBoxIdBeginning]) {
				if (
					! env.selectCollectiveCommands.includes(clickedSelector) &&
					clickedSelector.startsWith("#" + beginning)
				) {
					let reducedClickedSelector = clickedSelector.substr(1 + beginning.length);
					let splitted = reducedClickedSelector.split(env.options.cache_folder_separator);
					albumCacheBase = splitted.slice(0, splitted.length - 1).join(env.options.cache_folder_separator);
					break;
				}
			}
		}

		if (
			mediaBeginnings.some(
				beginning => (
					! env.selectCollectiveCommands.includes(clickedSelector) &&
					clickedSelector.startsWith("#" + beginning)
				)
			)
		) {
			// clicks on media can be gathered, with the condition that no collective click or album click
			// is in the middle

			// so we try to gather with the last same cache base clicks,
			// but only after the last click on an album click or with a collective command

			// what is the last click history element which has a clicked selector
			// which is either collective (i.e. from menu) or an album one?
			let lastCollectiveOrAlbumClickHistoryIndex = this.selectionClickHistory.findLastIndex(
				selectionClickHistoryElement => (
					mediaBeginnings.every(
						beginning => (
							clickedSelectors(selectionClickHistoryElement).every(
								thisClickedSelector => {
									if (util.hasGeoContentInfo(thisClickedSelector))
										thisClickedSelector = util.separateSelectorWithGeoContentInfo(thisClickedSelector)[0];
									return (
										! (
											! env.selectCollectiveCommands.includes(thisClickedSelector) &&
											thisClickedSelector.startsWith("#" + beginning)
										)
									);
								}
							)
						)
					)
				)
			);
			let lastCollectiveOrAlbumClickedSelector = -1;
			if (lastCollectiveOrAlbumClickHistoryIndex > -1) {
				// what is the last clicked selector which has a clicked selector
				// which is either collective (i.e. from menu) or an album one?
				lastCollectiveOrAlbumClickedSelector = clickedSelectors(
						this.selectionClickHistory[lastCollectiveOrAlbumClickHistoryIndex]
				).findLastIndex(
					thisClickedSelector => mediaBeginnings.every(
						beginning => {
							if (util.hasGeoContentInfo(thisClickedSelector))
								thisClickedSelector = util.separateSelectorWithGeoContentInfo(thisClickedSelector)[0];
							return ! (
								! env.selectCollectiveCommands.includes(thisClickedSelector) &&
								thisClickedSelector.startsWith("#" + beginning)
							);
						}
					)
				);
			}

			// which is the last click history element with the same cache base of the album where the click was issued
			// and that is from lastCollectiveOrAlbumClickHistoryIndex onward?
			let lastSameCacheBaseClickHistoryIndex = this.selectionClickHistory.findIndex(
				(selectionClickHistoryElement, index) => (
					index >= lastCollectiveOrAlbumClickedSelector &&
					cacheBase(selectionClickHistoryElement) === albumCacheBase
				)
			);

			if (lastSameCacheBaseClickHistoryIndex > -1) {
				let clickHistoryElement = this.selectionClickHistory[lastSameCacheBaseClickHistoryIndex];

				// the clicked selector is there?
				let clickedSelectorIndex = clickedSelectors(clickHistoryElement).findIndex(
					thisClickedSelector => {
						if (util.hasGeoContentInfo(thisClickedSelector))
							thisClickedSelector = util.separateSelectorWithGeoContentInfo(thisClickedSelector)[0];
						return thisClickedSelector === clickedSelector;
					}
				);
				if (clickedSelectorIndex > -1) {
					// the clicked selector is already there selecting that media, a new click on it means removing it
					if (clickHistoryElement[albumCacheBase].length === 1)
						this.selectionClickHistory.splice(lastSameCacheBaseClickHistoryIndex, 1);
					else
						clickHistoryElement[albumCacheBase].splice(clickedSelectorIndex, 1);
				} else {
					// the clicked selector is not there, add it
					clickHistoryElement[albumCacheBase].push(clickedSelector);
					// this.selectionClickHistory[lastSameCacheBaseClickHistoryIndex].push(clickedSelector);
				}
			} else {
				// the album where the click was issued was not there, add it
				let clickHistoryElement = {};
				clickHistoryElement[albumCacheBase] = [clickedSelector];
				this.selectionClickHistory.push(clickHistoryElement);
			}
		} else {
			// an album was clicked, or a collective command was issued
			// For collective command, the state of "hide geotagged content" must be taken into account
			// If the last album which the click was issued in is the same, add the new click to it,
			// otherways make a new element in the history
			let lastClickHistoryElement = this.selectionClickHistory.at(-1);
			let clickedSelectorIsAlbum = (
				! env.selectCollectiveCommands.includes(clickedSelector) &&
				clickedSelector.startsWith("#" + env.albumSelectBoxIdBeginning)
			);

			if (
				this.selectionClickHistory.length &&
				cacheBase(lastClickHistoryElement) === albumCacheBase
			) {
				// add to this click history element, or remove it if it was the last one
				let lastSelector = lastClickHistoryElement[albumCacheBase].at(-1);
				let hideGeotaggedContent = null;
				let geotaggedContentIsHidden = util.geotaggedContentIsHidden() ? 1 : 0;
				if (util.hasGeoContentInfo(lastSelector))
					[lastSelector, hideGeotaggedContent] = util.separateSelectorWithGeoContentInfo(lastSelector);
				if (
					lastSelector === clickedSelector && (
						clickedSelectorIsAlbum ||
						hideGeotaggedContent === geotaggedContentIsHidden
					)
				)
					lastClickHistoryElement[albumCacheBase].pop();
				else
					lastClickHistoryElement[albumCacheBase].push(
						// add the geotagged content hidden info at the beginning of the selector
						clickedSelectorIsAlbum ? clickedSelector : util.addGeoContentInfoToSelector(clickedSelector, geotaggedContentIsHidden)
					);
			} else {
				// make a new element
				let clickHistoryElement = {};
				clickHistoryElement[albumCacheBase] = [
					clickedSelectorIsAlbum ? clickedSelector : util.addGeoContentInfoToSelector(clickedSelector, geotaggedContentIsHidden)
				];
				this.selectionClickHistory.push(clickHistoryElement);
			}
		}

		this.updateSelectionCacheBase();
	};

	SelectionAlbum.prototype.updateSelectionCacheBase = function() {
		this.changeCacheBase(util.encodeSelectionCacheBase(this.selectionClickHistory));
	};

	SelectionAlbum.prototype.updateLocationHash = function(updateSubalbums = false, updateMedia = false) {
		if (! env.isPlayingSelectionClickHistory) {
			if (env.currentAlbum.isSelection()) {
				env.isASelectionChange = true;
				// the following hash change is because we are in the selection, and it changed
				// only the hash changes, nothing more
				window.location.hash = util.encodeHash(
					env.currentAlbum.cacheBase,
					env.hashComponents.mediaCacheBase,
					env.hashComponents.mediaFolderCacheBase,
					env.hashComponents.collectedAlbumCacheBase,
					env.hashComponents.collectionCacheBase
				);
				if (updateSubalbums)
					this.showSubalbums();
				if (updateMedia)
					this.showMedia();
				env.isASelectionChange = false;
			}

			env.selectionAlbum.updateSelectionLink();
		}
	};

	SelectionAlbum.prototype.changeCacheBase = function(newCacheBase) {
		util.changeCacheBase(this, newCacheBase);
	};

	SelectionAlbum.prototype.updateSelectionLink = function() {
		$(".selection.link").attr("href", env.hashBeginning + this.cacheBase);
	};
}());
//# sourceURL=047-selection-methods-bis.js
