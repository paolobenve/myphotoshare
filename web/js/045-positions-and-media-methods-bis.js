(function() {

	var mapF;
	$.executeAfterEvent(
		"mapFunctionsLoadedEvent",
		function() {
			mapF = new MapFunctions();
		}
	);

	PositionsAndMedia.prototype.averagePosition = function() {
		var averageLatLng = L.latLng(0, 0);
		var lat, lng, countTotal = 0;
		for (var i = 0; i < this.length; i ++) {
			lat = this[i].lat;
			lng = this[i].lng;
			if (this[i].hasOwnProperty("count")) {
				lat *= this[i].count;
				lng *= this[i].count;
			}
			averageLatLng.lat += lat;
			averageLatLng.lng += lng;
			if (this[i].hasOwnProperty("count"))
				countTotal += this[i].count;
			else
				countTotal += 1;
		}
		averageLatLng.lat /= countTotal;
		averageLatLng.lng /= countTotal;

		return averageLatLng;
	};
}());
//# sourceURL=045-positions-and-media-methods-bis.js
