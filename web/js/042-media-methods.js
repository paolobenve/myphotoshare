(function() {

	var util = new Utilities();

	Media.prototype.clone = function() {
		var isCloning = true;
		return new Media(this, isCloning);
	};

	Media.prototype.getAndPutIntoCache = function() {
		this.forEach(
			function(singleMedia, index, media) {
				var singleMediaFromCache = env.cache.getSingleMedia(singleMedia);
				if (singleMediaFromCache !== false) {
					media[index] = singleMediaFromCache;
				}
			}
		);
	};

	Media.prototype.removeUnnecessaryPropertiesAndAddParent = function(album) {
		var unnecessaryProperties = ['checksum', 'dateTimeDir', 'dateTimeFile'];
		// remove unnecessary properties from each media
		for (let i = this.length - 1; i >= 0; i --) {
			for (let j = 0; j < unnecessaryProperties.length; j ++) {
				if (this[i].hasOwnProperty(unnecessaryProperties[j]))
					delete this[i][unnecessaryProperties[j]];
			}

			this[i].addParent(album);
		}
	};

	Media.prototype.intersectionForSearches = function(other) {
		util.mediaOrSubalbumsIntersectionForSearches(this, other);
	};

	Media.prototype.unionForSearches = function(other) {
		util.mediaOrSubalbumsUnionForSearches(this, other);
	};

	Media.prototype.sortBy = function(fieldArray) {
		util.sortBy(this, fieldArray);
	};

	Media.prototype.sortByName = function() {
		this.sortBy(['name']);
	};

	Media.prototype.sortByExifDate = function () {
		this.sort(
			function(a, b) {
				return a.exifDate < b.exifDate ? -1 : a.exifDate > b.exifDate ? 1 : 0;
			}
		);
	};
	Media.prototype.sortByFileDate = function () {
		this.sort(
			function(a, b) {
				return a.fileDate < b.fileDate ? -1 : a.fileDate > b.fileDate ? 1 : 0;
			}
		);
	};

	Media.prototype.sortByPixelSize = function () {
		this.sort(
			function(a, b) {
				let aMax, bMax;
				if (a.hasOwnProperty("metadata") && a.metadata.hasOwnProperty("size"))
					aMax = Math.max(... a.metadata.size);
				else
					aMax = 0;
				if (b.hasOwnProperty("metadata") && b.metadata.hasOwnProperty("size"))
					bMax = Math.max(... b.metadata.size);
				else
					bMax = 0;
				return aMax < bMax ? -1 : aMax > bMax ? 1 : 0;
			}
		);
	};

	Media.prototype.sortByFileSize = function () {
		this.sort(
			function(a, b) {
				const aSize = a.fileSizes.original.imagesAudiosVideosTotal();
				const bSize = b.fileSizes.original.imagesAudiosVideosTotal();
				return aSize < bSize ? -1 : aSize > bSize ? 1 : 0;
			}
		);
	};

	Media.prototype.sortReverse = function() {
		this.reverse();
	};

	Media.prototype.imagesAudiosVideosCount = function() {
		var result = new ImagesAudiosVideos();
		for (let i = 0; i < this.length; i ++) {
			if (this[i].isImage())
				result.images += 1;
			else if (this[i].isAudio())
				result.audios += 1;
			else if (this[i].isVideo())
				result.videos += 1;
		}
		return result;
	};

	Media.prototype.filterMediaAgainstSearchedAlbum = function(searchAlbum) {
		let removedMedia = [];
		let searchObject = searchAlbum.search;

		if (! searchObject.currentAlbumOnly)
			return removedMedia;

		for (let indexMedia = this.length - 1; indexMedia >= 0 ; indexMedia --) {
			let ithMedia = this[indexMedia];
			if (! ithMedia.isInsideAlbumToSearchIn(searchAlbum)) {
				// remove the media
				this.splice(indexMedia, 1);
				removedMedia.push(ithMedia);
			}
		}

		return removedMedia;
	};

	Media.prototype.filterMediaAgainstOneWord = function(searchObject, normalizedWord) {
		var removedMedia = [];
		var normalizedWords, normalizedTags;
		for (let indexMedia = this.length - 1; indexMedia >= 0 ; indexMedia --) {
			const ithMedia = this[indexMedia];

			normalizedWords = util.normalizeAccordingToOptions(ithMedia.words);
			if (ithMedia.metadata.hasOwnProperty("tags") && searchObject.tagsOnly)
				normalizedTags = util.normalizeAccordingToOptions(ithMedia.metadata.tags);

			if (! searchObject.insideWords) {
				// whole word
				if (
					! searchObject.tagsOnly &&
					normalizedWords.includes(normalizedWord) ||
					searchObject.tagsOnly &&
					ithMedia.metadata.hasOwnProperty("tags") &&
					normalizedTags.includes(normalizedWord)
				) {
					// ok, do not remove the media
				} else {
					// remove the media
					this.splice(indexMedia, 1);
					removedMedia.push(ithMedia);
				}
			} else {
				// inside words
				if (
					! searchObject.tagsOnly &&
					normalizedWords.some(element => element.includes(normalizedWord)) ||
					searchObject.tagsOnly &&
					ithMedia.metadata.hasOwnProperty("tags") &&
					normalizedTags.some(element => element.includes(normalizedWord))
				) {
					// ok, do not remove the media
				} else {
					// remove the media
					this.splice(indexMedia, 1);
					removedMedia.push(ithMedia);
				}
			}
		}

		return removedMedia;
	};

	Media.prototype.filterMediaAgainstEveryWord = function(searchObject, lastIndex = -1) {
		var removedMedia = [];
		var normalizedWords, normalizedTags;

		for (let indexMedia = this.length - 1; indexMedia >= 0 ; indexMedia --) {
			const ithMedia = this[indexMedia];

			normalizedWords = util.normalizeAccordingToOptions(ithMedia.words);
			if (ithMedia.metadata.hasOwnProperty("tags") && searchObject.tagsOnly)
				normalizedTags = util.normalizeAccordingToOptions(ithMedia.metadata.tags);

			if (! searchObject.insideWords) {
				// whole word
				if (
					! searchObject.tagsOnly &&
					searchObject.wordsFromUserNormalizedAccordingToOptions.every(
						(searchWord, index) => index <= lastIndex || normalizedWords.indexOf(searchWord) > -1
					) ||
					searchObject.tagsOnly &&
					ithMedia.metadata.hasOwnProperty("tags") &&
					searchObject.wordsFromUserNormalizedAccordingToOptions.every(
						(searchWord, index) => index <= lastIndex || normalizedTags.indexOf(searchWord) > -1
					)
				) {
					// ok, do not remove the media
				} else {
					// remove the media
					this.splice(indexMedia, 1);
					removedMedia.push(ithMedia);
				}
			} else {
				// inside words
				for (let indexWordsLeft = lastIndex + 1; indexWordsLeft < searchObject.wordsFromUserNormalizedAccordingToOptions.length; indexWordsLeft ++) {
					if (
						! searchObject.tagsOnly &&
						normalizedWords.some(searchWord => searchWord.includes(searchObject.wordsFromUserNormalizedAccordingToOptions[indexWordsLeft])) ||
						searchObject.tagsOnly &&
						ithMedia.metadata.hasOwnProperty("tags") &&
						normalizedTags.some(
							(searchWord, index) => index <= lastIndex && normalizedTags.indexOf(searchWord) > -1
						)
					) {
						// ok, do not remove the media
					} else {
						// remove the media
						this.splice(indexMedia, 1);
						removedMedia.push(ithMedia);
						break;
					}
				}
			}
		}

		return removedMedia;
	};

}());
