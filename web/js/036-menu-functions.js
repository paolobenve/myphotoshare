(function() {

	var phFl = new PhotoFloat();
	var util = new Utilities();
	var pS;
	// $(document).ready(
	// 	function() {
	$.executeAfterEvent(
		"pinchSwipeFunctionsLoadedEvent",
		function() {
			pS = new PinchSwipe();
			// $(document).trigger("pinchSwipeOkInMenuFunctions");
		}
	);
	// 	}
	// );

	/* constructor */
	function MenuFunctions() {
	}

	MenuFunctions.hideDescriptionMenuEntry = function() {
		var isPopup = util.isPopup();
		var isMap = util.isMap();

		var popupHasSomeDescription, albumHasSomeDescription, subalbumsHaveSomeDescription, mediaHaveSomeDescription;
		if (isPopup)
			popupHasSomeDescription =
				env.mapAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("title")) ||
				env.mapAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("description"));
		if (env.currentAlbum !== null) {
			albumHasSomeDescription =
				env.currentAlbum.hasSomeDescription("title") ||
				env.currentAlbum.hasSomeDescription("description");
			subalbumsHaveSomeDescription =
				env.currentAlbum.visibleSubalbums().length > 0 && (
					env.currentAlbum.subalbums.some(subalbum => subalbum.hasSomeDescription("title")) ||
					env.currentAlbum.subalbums.some(subalbum => subalbum.hasSomeDescription("description"))
				);
			mediaHaveSomeDescription =
				env.currentAlbum.numVisibleMedia() > 0 && (
					env.currentAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("title")) ||
					env.currentAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("description"))
				);
		}
		var singleMediaHasSomeDescription;
		if (env.currentMedia !== null)
			singleMediaHasSomeDescription =
				env.currentMedia.hasSomeDescription("title") ||
				env.currentMedia.hasSomeDescription("description");

		return (
			isMap ||
			isPopup && ! popupHasSomeDescription ||
			env.currentMedia === null && ! albumHasSomeDescription && ! subalbumsHaveSomeDescription && ! mediaHaveSomeDescription ||
			env.currentMedia !== null && ! (singleMediaHasSomeDescription || ! env.currentMedia.hasSomeDescription("tags") && albumHasSomeDescription)
		);
	};

	MenuFunctions.hideTagsMenuEntry = function() {
		var isPopup = util.isPopup();
		var isMap = util.isMap();

		var popupHasSomeTag, albumHasSomeTag, subalbumsHaveSomeTag, mediaHaveSomeTag;
		if (isPopup)
			popupHasSomeTag =
				env.mapAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("tags"));
		if (env.currentAlbum !== null) {
			albumHasSomeTag =
				env.currentAlbum.hasSomeDescription("tags");
			subalbumsHaveSomeTag =
				env.currentAlbum.visibleSubalbums().length > 0 && (
					env.currentAlbum.subalbums.some(subalbum => subalbum.hasSomeDescription("tags"))
				);
			mediaHaveSomeTag =
				env.currentAlbum.numVisibleMedia() > 0 && (
					env.currentAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("tags"))
				);
		}
		var singleMediaHasSomeTag;
		if (env.currentMedia !== null)
			singleMediaHasSomeTag = env.currentMedia.hasSomeDescription("tags");

		return (
			isMap ||
			isPopup && ! popupHasSomeTag ||
			env.currentMedia === null && ! albumHasSomeTag && ! subalbumsHaveSomeTag && ! mediaHaveSomeTag ||
			env.currentMedia !== null && ! singleMediaHasSomeTag && ! albumHasSomeTag
		);
	};

	MenuFunctions.mustDownloadSelectionAlbum = function() {
		return (
			env.options.download_selection &&
			env.selectionAlbum &&
			env.selectionAlbum.numsMediaInSubTree && (
				true
				// env.options.download_subalbums &&
				// env.selectionAlbum.numsVisibleMediaInSubtreeForDownload().imagesAudiosVideosTotal() ||
				// ! env.options.download_subalbums &&
				// env.selectionAlbum.numsVisibleMediaInAlbumForDownload().imagesAudiosVideosTotal()
			)
		);
	};

	MenuFunctions.prototype.errorOpeningUnprotectedJsonFiles = function(album) {
		function openAuthDialog(album) {
			$("#loading").hide();
			// the tow following function calls are needed
			// in order to set the click event on the padlock
			util.loadOtherJsCssFiles();
			MenuFunctions.updateMenu(album);

			$.executeAfterEvent(
				"otherJsFilesLoadedEvent",
				function() {
					$("#protected-content-unveil")[0].click();
				}
			);
		}

		function checkHigherAncestor() {
			if (album.isSearch())
				MenuFunctions.openSearchMenu(album);

			let upHash = util.upHash(higherHash);
			if (! higherHash.length || upHash === higherHash) {
				// the top album has been reached and no unprotected nor protected content has been found
				if (album.isEmpty || album.hasVeiledProtectedContent())
					openAuthDialog();
			} else {
				higherHash = upHash;
				let cacheBase = higherHash.substring(env.hashBeginning.length);
				let getAlbumPromise = phFl.getAlbum(cacheBase, checkHigherAncestor, {getMedia: false, getPositions: false});
				getAlbumPromise.then(
					function(upAlbum) {
						if (upAlbum.hasVeiledProtectedContent() && ! env.fromEscKey) {
						// if (upAlbum.hasVeiledProtectedContent() && ! env.fromEscKey) {
							openAuthDialog(upAlbum);
						} else {
							util.errorThenGoUp();
						}
					}
				);
			}
		}
		// end of auxiliary function

		// neither the unprotected nor the protected album exist
		// the user could have opened a protected album link: the password can be asked, but only if some ancestor album has protected content
		let higherHash = location.hash;
		checkHigherAncestor();
	};

	MenuFunctions.updateMenu = function(thisAlbum) {

		if (! thisAlbum) {
			thisAlbum = env.currentAlbum;
			if (util.isPopup())
				thisAlbum = env.mapAlbum;
		}

		if (env.currentAlbum === null && thisAlbum === null) {
			return;
		}

		////////////////// PROTECTED CONTENT //////////////////////////////

		// the padlock visibility is determined always on current album or thisAlbum, never on popup
		let selector = ".protection";
		let albumForUnveiling = env.currentAlbum;
		if (albumForUnveiling === null)
			albumForUnveiling = thisAlbum;
		if (albumForUnveiling === undefined || albumForUnveiling.hasVeiledProtectedContent()) {
			$(selector).removeClass("hidden");
			$(selector).off("click").on(
				"click",
				function() {
					util.showAuthForm();
					MenuFunctions.closeMenu();
				}
			);
		} else {
			$(selector).addClass("hidden");
		}

		///////////////////////////////////////////////////////////////////
		////////////////////// OTHER MENU ELEMENTS ////////////////////////

		$.executeAfterEvent(
			"otherJsFilesLoadedEvent",
			function() {
				let isPopup = util.isPopup();
				let isMap = ($('#mapdiv').html() ? true : false) && ! isPopup;
				let isMapOrPopup = isMap || isPopup;

				let thisAlbumIsAlbumWithOneMedia = thisAlbum.isAlbumWithOneMedia();
				let isSingleMedia = (env.currentMedia !== null || thisAlbumIsAlbumWithOneMedia);
				let isAnyRootOrCollection = thisAlbum.isAnyRootOrCollection();

				let numVisibleSubalbums = thisAlbum.visibleSubalbums().length;
				let numVisibleMedia = thisAlbum.numVisibleMedia();
				let geotaggedContentIsHidden = util.geotaggedContentIsHidden();

				let nothingIsSelected = util.nothingIsSelected();
				let everySubalbumIsSelected = false;
				if (! isPopup && env.hasOwnProperty("selectionAlbum"))
					everySubalbumIsSelected = thisAlbum.everySubalbumIsSelected(env.selectionAlbum);
				let noSubalbumIsSelected = true;
				if (env.options.selectable_albums && env.hasOwnProperty("selectionAlbum"))
					noSubalbumIsSelected = thisAlbum.noSubalbumIsSelected(env.selectionAlbum);
				let everyMediaIsSelected = false;
				if (env.hasOwnProperty("selectionAlbum")) {
					if (! isPopup)
						everyMediaIsSelected = thisAlbum.everyMediaIsSelected(env.selectionAlbum);
					else
						everyMediaIsSelected = env.mapAlbum.everyMediaIsSelected(env.selectionAlbum);
				}
				let noMediaIsSelected = true;
				if (env.hasOwnProperty("selectionAlbum")) {
					if (! isPopup)
						noMediaIsSelected = thisAlbum.noMediaIsSelected(env.selectionAlbum);
					else
						noMediaIsSelected = env.mapAlbum.noMediaIsSelected(env.selectionAlbum);
				}
				let isABigTransversalAlbum = thisAlbum.isABigTransversalAlbum();
				let hasGpsData, thisMedia;

				if (isSingleMedia) {
					if (env.currentMedia)
						thisMedia = env.currentMedia;
					else
						thisMedia = thisAlbum.visibleMedia()[0];
					hasGpsData = thisMedia.hasGpsData();
				} else if (isAnyRootOrCollection) {
					hasGpsData = (thisAlbum.hasOwnProperty("numPositionsInTree") && thisAlbum.numPositionsInTree > 0);
				} else {
					hasGpsData = false;
				}

				// add the correct classes to the menu buttons

				////////////////// BROWSING MODES //////////////////////////////

				$(".browsing-mode-switcher").off("click");

				if (
					isMapOrPopup ||
					thisAlbum === null ||
					! isSingleMedia && ! isAnyRootOrCollection
				) {
					$(".browsing-mode-switcher").addClass("hidden");
				} else {
					$(".browsing-mode-switcher").removeClass("hidden").removeClass("selected");
					$(".first-level.browsing-mode-switcher li").addClass("active");

					if (! env.options.expose_image_dates) {
						$("#by-date-view").addClass("hidden");
					}

					if (! hasGpsData || ! env.options.expose_image_positions || env.options.save_data) {
						$("#by-gps-view").addClass("hidden");
					}

					if (
						nothingIsSelected || ! (
							isSingleMedia && thisMedia.isSelected() ||
							isAnyRootOrCollection
						)
					) {
						$("#by-selection-view").addClass("hidden");
					}

					if (
						! util.somethingIsInMapAlbum() || ! (
							isSingleMedia && thisMedia.isInMapAlbum() ||
							isAnyRootOrCollection
						)
					) {
						$("#by-map-view").addClass("hidden");
					}

					if (
						! (
							isAnyRootOrCollection && util.somethingIsSearched() ||
							isSingleMedia && (
								// util.somethingIsSearched() ||
								// env.hashComponents.collectionCacheBase && util.isSearchCacheBase(env.hashComponents.collectionCacheBase)
								thisAlbum.isSearch() ||
								thisMedia.isSearched() ||
								env.hashComponents.collectionCacheBase && util.isSearchCacheBase(env.hashComponents.collectionCacheBase) ||
								thisMedia.isInFoundSubalbums() !== false
							)
						)
					) {
						$("#by-search-view").addClass("hidden");
					}

					if (thisAlbum.isFolder() && ! (env.hashComponents.collectionCacheBase && util.isSearchCacheBase(env.hashComponents.collectionCacheBase))) {
						// folder album: change to by date or by gps view
						$("#folders-view").addClass("selected").removeClass("active");
					} else if (thisAlbum.isByDate()) {
						$("#by-date-view").addClass("selected").removeClass("active");
					} else if (thisAlbum.isByGps()) {
						$("#by-gps-view").addClass("selected").removeClass("active");
					} else if (thisAlbum.isMap()) {
						$("#by-map-view").removeClass("hidden").addClass("selected").removeClass("active");
					} else if (
						thisAlbum.isSearch() ||
						env.hashComponents.collectionCacheBase && util.isSearchCacheBase(env.hashComponents.collectionCacheBase)
					) {
						$("#by-search-view").removeClass("hidden").addClass("selected").removeClass("active");
					} else if (thisAlbum.isSelection()) {
						$("#by-selection-view").removeClass("hidden").addClass("selected").removeClass("active");
					}
				}

				// bind the click events

				$("#folders-view").off("click").on(
					"click",
					function changeToFoldersView(ev) {
						ev.stopPropagation();
						MenuFunctions.addHighlightToItem($(this));
						TopFunctions.showBrowsingModeMessage(ev, "#folders-browsing");

						if (isSingleMedia) {
							$(".title").removeClass("hidden-by-pinch");
							$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-pinch");
							window.location.href =
								env.hashBeginning + util.pathJoin([thisMedia.foldersCacheBase, thisMedia.cacheBase]);
						} else if (isAnyRootOrCollection) {
							window.location.href = env.hashBeginning + encodeURIComponent(env.options.folders_string);
						}

						return false;
					}
				);

				$("#by-date-view").off("click").on(
					"click",
					function changeToByDateView(ev) {
						ev.stopPropagation();
						MenuFunctions.addHighlightToItem($(this));
						TopFunctions.showBrowsingModeMessage(ev, "#by-date-browsing");

						if (isSingleMedia) {
							$(".title").removeClass("hidden-by-pinch");
							$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-pinch");
							window.location.href =
								env.hashBeginning + util.pathJoin([thisMedia.dayAlbumCacheBase, thisMedia.foldersCacheBase, thisMedia.cacheBase]);
						} else if (isAnyRootOrCollection) {
							window.location.href = env.hashBeginning + encodeURIComponent(env.options.by_date_string);
						}

						return false;
					}
				);

				$("#by-gps-view").off("click").on(
					"click",
					function changeToByGpsView(ev) {
						ev.stopPropagation();
						MenuFunctions.addHighlightToItem($(this));
						TopFunctions.showBrowsingModeMessage(ev, "#by-gps-browsing");

						if (isSingleMedia) {
							$(".title").removeClass("hidden-by-pinch");
							$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-pinch");
							window.location.href =
								env.hashBeginning + util.pathJoin([thisMedia.gpsAlbumCacheBase, thisMedia.foldersCacheBase, thisMedia.cacheBase]);
						} else if (isAnyRootOrCollection) {
							window.location.href = env.hashBeginning + encodeURIComponent(env.options.by_gps_string);
						}

						return false;
					}
				);

				$("#by-map-view").off("click").on(
					"click",
					function changeToByMapView(ev) {
						ev.stopPropagation();
						MenuFunctions.addHighlightToItem($(this));
						TopFunctions.showBrowsingModeMessage(ev, "#by-map-browsing");
						if (isSingleMedia) {
							$(".title").removeClass("hidden-by-pinch");
							$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-pinch");
							window.location.href = util.encodeHash(env.mapAlbum.cacheBase, thisMedia.cacheBase, thisMedia.foldersCacheBase);
						} else if (isAnyRootOrCollection) {
							window.location.href = util.encodeHash(env.mapAlbum.cacheBase, null);
						}

						return false;
					}
				);

				$("#by-search-view").off("click").on(
					"click",
					function changeToBySearchView(ev) {
						ev.stopPropagation();
						MenuFunctions.addHighlightToItem($(this));
						TopFunctions.showBrowsingModeMessage(ev, "#by-search-browsing");
						if (isSingleMedia) {
							$(".title").removeClass("hidden-by-pinch");
							$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-pinch");
							// if (thisMedia.hasOwnProperty("searchHashes") && thisMedia.searchHashes.length)
							var foundAlbum = thisMedia.isInFoundSubalbums();
							if (foundAlbum !== false) {
								window.location.href = util.encodeHash(thisMedia.foldersCacheBase, thisMedia.cacheBase, thisMedia.foldersCacheBase, foundAlbum.cacheBase, env.searchAlbum.cacheBase);
							} else {
								window.location.href = util.encodeHash(env.searchAlbum.cacheBase, thisMedia.cacheBase, thisMedia.foldersCacheBase);
							}
						} else if (isAnyRootOrCollection) {
							window.location.href = util.encodeHash(env.searchAlbum.cacheBase, null);
						}

						return false;
					}
				);

				// WARNING: the ":not(.hidden)" is missing intentionally, in order to permit to trigger a click even if the menu item isn't shown
				$("#by-selection-view").off("click").on(
					"click",
					function(ev) {
						ev.stopPropagation();
						MenuFunctions.addHighlightToItem($(this));
						util.changeToBySelectionView(ev, thisMedia);

						return false;
					}
				);

				////////////////// SEARCH //////////////////////////////

				if ($("#right-and-search-menu .menu").hasClass("expanded")) {
					if (isMap)
						$("#search-menu").addClass("hidden-by-map");
					else
						$("#search-menu").removeClass("hidden-by-map");

					if (
						thisAlbum !== null
					) {
						if (isPopup)
							$("#search-field, #search-button").attr("title", util._t("#refine-popup-content"));
						else
							$("#search-field, #search-button").attr("title", util._t("#real-search"));

						if (env.search.insideWords)
							$("li#inside-words").addClass("selected");
						else
							$("li#inside-words").removeClass("selected");
						if (env.search.anyWord)
							$("li#any-word").addClass("selected");
						else
							$("li#any-word").removeClass("selected");
						if (env.search.caseSensitive)
							$("li#case-sensitive").addClass("selected");
						else
							$("li#case-sensitive").removeClass("selected");
						if (env.search.accentSensitive)
							$("li#accent-sensitive").addClass("selected");
						else
							$("li#accent-sensitive").removeClass("selected");
						if (env.search.numbers)
							$("li#search-numbers").addClass("selected");
						else
							$("li#search-numbers").removeClass("selected");
						if (env.search.tagsOnly)
							$("li#tags-only").addClass("selected");
						else
							$("li#tags-only").removeClass("selected");
						if (env.search.searchedCacheBase === env.options.folders_string) {
							// TO DO: actually code could enter here for any root album, i.e. for by date, ecc. too
							$("#album-search").attr('title', util._t("#current-album-is") + ' ""');
						} else {
							let albumNamePromise = util.getAlbumNameFromCacheBase(env.search.searchedCacheBase);
							albumNamePromise.then(
								function(path) {
									$("#album-search").attr('title', util._t("#current-album-is") + ' "' + path + '"');
								}
							);
						}

						if (env.search.currentAlbumOnly)
							$("li#album-search").addClass("selected");
						else
							$("li#album-search").removeClass("selected");
						// }
					}
				}

				////////////////// UI //////////////////////////////

				if ($("#right-and-search-menu .menu").hasClass("expanded")) {
					if (isMap) {
						$("#right-and-search-menu li.ui").addClass("hidden");
					} else {
						$("#right-and-search-menu li.ui").removeClass("hidden");

						if (isMapOrPopup) {
							$("#right-and-search-menu li.show-title").addClass("hidden");
						} else {
							$("#right-and-search-menu li.show-title").removeClass("hidden");
							if (
								env.options.hide_title ||
								$("#album-and-media-container").hasClass("single-media") &&
								$("#album-and-media-container #media-view #center .title").css("display") === "none"
							)
								$("#right-and-search-menu li.show-title").removeClass("selected");
							else
								$("#right-and-search-menu li.show-title").addClass("selected");
						}

						if (MenuFunctions.hideDescriptionMenuEntry(thisAlbum)) {
							$("#right-and-search-menu li.show-descriptions").addClass("hidden");
						} else {
							$("#right-and-search-menu li.show-descriptions").removeClass("hidden");
							if (env.options.hide_descriptions)
								$("#right-and-search-menu li.show-descriptions").removeClass("selected");
							else
								$("#right-and-search-menu li.show-descriptions").addClass("selected");
						}

						if (MenuFunctions.hideTagsMenuEntry()) {
							$("#right-and-search-menu li.show-tags").addClass("hidden");
						} else {
							$("#right-and-search-menu li.show-tags").removeClass("hidden");
							if (env.options.hide_tags)
								$("#right-and-search-menu li.show-tags").removeClass("selected");
							else
								$("#right-and-search-menu li.show-tags").addClass("selected");
						}

						if (
							isMapOrPopup ||
							env.currentMedia !== null ||
							thisAlbumIsAlbumWithOneMedia ||
							thisAlbum !== null && numVisibleSubalbums === 0 && env.options.hide_title
						) {
							$("#right-and-search-menu li.show-media-counts").addClass("hidden");
						} else {
							$("#right-and-search-menu li.show-media-counts").removeClass("hidden");
							if (env.options.show_album_media_count)
								$("#right-and-search-menu li.show-media-counts").addClass("selected");
							else
								$("#right-and-search-menu li.show-media-counts").removeClass("selected");
						}


						if (isMap || isPopup && env.mapAlbum.media.length <= 1 || (! $("#thumbs").is(":visible") && ! $("#subalbums").is(":visible")))
							$("#right-and-search-menu li.spaced").addClass("hidden");
						else
							$("#right-and-search-menu li.spaced").removeClass("hidden");
						if (env.options.spacing)
							$("#right-and-search-menu li.spaced").addClass("selected");
						else
							$("#right-and-search-menu li.spaced").removeClass("selected");

						if (
							isMapOrPopup ||
							env.currentMedia !== null ||
							thisAlbumIsAlbumWithOneMedia ||
							env.options.only_square_thumbnails ||
							thisAlbum !== null && numVisibleSubalbums === 0
						) {
							$("#right-and-search-menu li.square-album-thumbnails").addClass("hidden");
						} else {
							$("#right-and-search-menu li.square-album-thumbnails").removeClass("hidden");
							if (env.options.album_thumb_type.indexOf("square") > -1)
								$("#right-and-search-menu li.square-album-thumbnails").addClass("selected");
							else
								$("#right-and-search-menu li.square-album-thumbnails").removeClass("selected");
						}

						if (
							isMapOrPopup ||
							env.currentMedia !== null ||
							thisAlbumIsAlbumWithOneMedia ||
							thisAlbum !== null && numVisibleSubalbums === 0
						) {
							$("#right-and-search-menu li.slide").addClass("hidden");
						} else {
							$("#right-and-search-menu li.slide").removeClass("hidden");
							if (env.options.albums_slide_style)
								$("#right-and-search-menu li.slide").addClass("selected");
							else
								$("#right-and-search-menu li.slide").removeClass("selected");
						}

						if (
							isMapOrPopup ||
							env.currentMedia !== null ||
							thisAlbumIsAlbumWithOneMedia ||
							thisAlbum !== null && (numVisibleSubalbums === 0 || ! thisAlbum.isFolder())
						) {
							$("#right-and-search-menu li.show-album-names").addClass("hidden");
						} else {
							$("#right-and-search-menu li.show-album-names").removeClass("hidden");
							if (env.options.show_album_names_below_thumbs)
								$("#right-and-search-menu li.show-album-names").addClass("selected");
							else
								$("#right-and-search-menu li.show-album-names").removeClass("selected");
						}

						if (
							isMap ||
							env.options.only_square_thumbnails ||
							! $("#thumbs").is(":visible")
						)
							$("#right-and-search-menu li.square-media-thumbnails").addClass("hidden");
						else
							$("#right-and-search-menu li.square-media-thumbnails").removeClass("hidden");
						if (env.options.media_thumb_type.indexOf("square") > -1)
						 	$("#right-and-search-menu li.square-media-thumbnails").addClass("selected");
						else
							$("#right-and-search-menu li.square-media-thumbnails").removeClass("selected");

						if (
							isMap ||
							! isMapOrPopup && (
								env.currentMedia !== null ||
								thisAlbumIsAlbumWithOneMedia ||
								thisAlbum !== null && (
									numVisibleMedia === 0 ||
									isABigTransversalAlbum
								)
							)
						) {
							$("#right-and-search-menu li.show-media-names").addClass("hidden");
						} else {
							$("#right-and-search-menu li.show-media-names").removeClass("hidden");
							if (env.options.show_media_names_below_thumbs)
								$("#right-and-search-menu li.show-media-names").addClass("selected");
							else
								$("#right-and-search-menu li.show-media-names").removeClass("selected");
						}

						if (
							isMapOrPopup ||
							env.currentMedia === null || thisAlbumIsAlbumWithOneMedia
						) {
							$("#right-and-search-menu li.show-bottom-thumbnails").addClass("hidden");
						} else {
							$("#right-and-search-menu li.show-bottom-thumbnails").removeClass("hidden");

							if (env.options.hide_bottom_thumbnails)
								$("#right-and-search-menu li.show-bottom-thumbnails").removeClass("selected");
							else
								$("#right-and-search-menu li.show-bottom-thumbnails").addClass("selected");
						}
					}
				}

				////////////////// SORT //////////////////////////////

				if (
					isMap ||
					! isPopup && (
						env.currentMedia !== null || (numVisibleMedia <= 1 || isABigTransversalAlbum) && numVisibleSubalbums <= 1
					) ||
					isPopup && env.mapAlbum.media.length <= 1
				) {
					// showing a media or a map or a popup on the map, nothing to sort
					$("#right-and-search-menu li.sort").addClass("hidden");
				} else if (thisAlbum !== null) {
					if (
						! isPopup && (numVisibleMedia <= 1 || isABigTransversalAlbum) ||
						isPopup && env.mapAlbum.media.length <= 1
					) {
						// no media or one media
						$("#right-and-search-menu li.media-sort").addClass("hidden");
					} else {
						$("#right-and-search-menu li.media-sort").removeClass("hidden");
					}

					if (isPopup || numVisibleSubalbums <= 1) {
						// no subalbums or one subalbum
						$("#right-and-search-menu li.album-sort").addClass("hidden");
					} else {
						$("#right-and-search-menu li.album-sort").removeClass("hidden");
					}

					var modes = ["album", "media"];
					for (var i in modes) {
						if (modes.hasOwnProperty(i)) {
							let albumOrMedia = modes[i];

							$("#right-and-search-menu li." + albumOrMedia + "-sort").removeClass("selected");
							$("#right-and-search-menu li." + albumOrMedia + "-sort").addClass("active");

							$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date").addClass("hidden");
							$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date-max").addClass("hidden");
							$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date-min").addClass("hidden");
							$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date").addClass("hidden");
							$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date-max").addClass("hidden");
							$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date-min").addClass("hidden");
							$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size").addClass("hidden");
							$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size-min").addClass("hidden");
							$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size-max").addClass("hidden");
							$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-size").addClass("hidden");

							if (env.options.expose_image_dates) {
								if (albumOrMedia === "album") {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date-max").removeClass("hidden");
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date-min").removeClass("hidden");
								} else {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date").removeClass("hidden");
								}

								if (
									albumOrMedia === "album" &&
									thisAlbum[albumOrMedia + "Sort"] === "exifDate"
								)
									thisAlbum[albumOrMedia + "Sort"] = "exifDateMax";

								if (
									albumOrMedia === "media" && (
										thisAlbum[albumOrMedia + "Sort"] === "exifDateMax" ||
										thisAlbum[albumOrMedia + "Sort"] === "exifDateMin"
									)
								)
									thisAlbum[albumOrMedia + "Sort"] = "exifDate";

								if (thisAlbum[albumOrMedia + "Sort"] === "exifDate") {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date").addClass("selected");
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date").removeClass("active");
								} else if (thisAlbum[albumOrMedia + "Sort"] === "exifDateMax") {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date-max").addClass("selected");
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date-max").removeClass("active");
								} else if (thisAlbum[albumOrMedia + "Sort"] === "exifDateMin") {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date-min").addClass("selected");
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-exif-date-min").removeClass("active");
								}
							}

							if (env.options.expose_original_media) {
								if (albumOrMedia === "album") {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date-max").removeClass("hidden");
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date-min").removeClass("hidden");
								} else {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date").removeClass("hidden");
								}

								if (
									albumOrMedia === "album" &&
									thisAlbum[albumOrMedia + "Sort"] === "fileDate"
								)
									thisAlbum[albumOrMedia + "Sort"] = "fileDateMax";

								if (
									albumOrMedia === "media" && (
										thisAlbum[albumOrMedia + "Sort"] === "fileDateMax" ||
										thisAlbum[albumOrMedia + "Sort"] === "fileDateMin"
									)
								)
									thisAlbum[albumOrMedia + "Sort"] = "fileDate";

								if (thisAlbum[albumOrMedia + "Sort"] === "fileDate") {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date").addClass("selected");
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date").removeClass("active");
								} else {
									if (thisAlbum[albumOrMedia + "Sort"] === "fileDateMax") {
										$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date-max").addClass("selected");
										$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date-max").removeClass("active");
									} else if (thisAlbum[albumOrMedia + "Sort"] === "fileDateMin") {
										$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date-min").addClass("selected");
										$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-date-min").removeClass("active");
									}
								}
							}

							if (thisAlbum[albumOrMedia + "Sort"] === "name") {
								$("#right-and-search-menu li." + albumOrMedia + "-sort.by-name").addClass("selected");
								$("#right-and-search-menu li." + albumOrMedia + "-sort.by-name").removeClass("active");
							}

							if (env.options.expose_original_media || env.options.expose_full_size_media_in_cache) {
								if (albumOrMedia === "album") {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size-max").removeClass("hidden");
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size-min").removeClass("hidden");
								} else {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size").removeClass("hidden");
								}

								if (
									albumOrMedia === "album" &&
									thisAlbum[albumOrMedia + "Sort"] === "pixelSize"
								)
									thisAlbum[albumOrMedia + "Sort"] = "pixelSizeMax";

								if (
									albumOrMedia === "media" && (
										thisAlbum[albumOrMedia + "Sort"] === "pixelSizeMax" ||
										thisAlbum[albumOrMedia + "Sort"] === "pixelSizeMin"
									)
								)
									thisAlbum[albumOrMedia + "Sort"] = "pixelSize";

								if (thisAlbum[albumOrMedia + "Sort"] === "pixelSize") {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size").addClass("selected");
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size").removeClass("active");
								} else {
									if (thisAlbum[albumOrMedia + "Sort"] === "pixelSizeMax") {
										$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size-max").addClass("selected");
										$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size-max").removeClass("active");
									} else if (thisAlbum[albumOrMedia + "Sort"] === "pixelSizeMin") {
										$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size-min").addClass("selected");
										$("#right-and-search-menu li." + albumOrMedia + "-sort.by-pixel-size-min").removeClass("active");
									}
								}
							}

							if (env.options.expose_original_media) {
								$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-size").removeClass("hidden");
								if (thisAlbum[albumOrMedia + "Sort"] === "fileSize") {
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-size").addClass("selected");
									$("#right-and-search-menu li." + albumOrMedia + "-sort.by-file-size").removeClass("active");
								}
							}

							if (
								thisAlbum[albumOrMedia + "ReverseSort"]
							) {
								$("#right-and-search-menu li." + albumOrMedia + "-sort.reverse").addClass("selected");
							} else {
								$("#right-and-search-menu li." + albumOrMedia + "-sort.reverse").removeClass("selected");
							}
						}

					}
				}

				////////////////// SELECTION //////////////////////////////

				if (isMap || thisAlbum.isSearch() && ! numVisibleMedia && ! numVisibleSubalbums) {
					$("li.first-level.select").addClass("hidden");
				} else {
					$("li.first-level.select").removeClass("hidden");
					$("li.first-level.select li.select").removeClass("hidden");
					if (! env.options.selectable_albums) {
						$("#albums-here.select").addClass("hidden");
						$("#no-albums.select").addClass("hidden");
						$("#media-here.select").addClass("hidden");
						$("#no-media.select").addClass("hidden");
					}
					$("li.first-level.select li.select").removeClass("selected");

					if (thisAlbum.isSelection()) {
						$("#global-reset-here.select").addClass("hidden");
						$("#go-to-selected.select").addClass("hidden");
					} else {
						let goToSelectedText = util._t("#go-to-selected.select");
						goToSelectedText += " (";
						if (env.hasOwnProperty("selectionAlbum")) {
							if (env.selectionAlbum.visibleSubalbums().length)
								goToSelectedText += env.selectionAlbum.visibleSubalbums().length + " " + util._t(".title-albums");
							if (env.selectionAlbum.visibleSubalbums().length && env.selectionAlbum.numVisibleMedia())
								goToSelectedText += ", ";
							if (env.selectionAlbum.numVisibleMedia())
								goToSelectedText += env.selectionAlbum.numVisibleMedia() + " " + util._t(".title-media");
						}
						goToSelectedText += ")";
						$("#go-to-selected.select").html(goToSelectedText);
					}
					if (nothingIsSelected  || env.options.save_data) {
						$("#global-reset.select").addClass("hidden");
						$("#global-reset-here.select").addClass("hidden");
					}
					if (thisAlbum.isAnyRoot()) {
						$("#global-reset-here.select").addClass("hidden");
					}
					if (nothingIsSelected) {
						$("#go-to-selected.select").addClass("hidden");
						$("#no-albums.select").addClass("hidden");
						$("#no-media.select").addClass("hidden");
					}
					if (noSubalbumIsSelected && (noMediaIsSelected || isABigTransversalAlbum))
						$("#nothing-here.select").addClass("hidden");
					if (! numVisibleSubalbums || noSubalbumIsSelected || everySubalbumIsSelected || noMediaIsSelected) {
						$("#no-albums.select").addClass("hidden");
					}
					if (! numVisibleMedia || noMediaIsSelected || everyMediaIsSelected || noSubalbumIsSelected) {
						$("#no-media.select").addClass("hidden");
					}

					if (! isPopup) {
						if (! numVisibleMedia || (! numVisibleSubalbums || ! env.options.selectable_albums))
							$("#media-here.select, #albums-here.select, #no-media.select, #no-albums.select").addClass("hidden");
						if (! numVisibleMedia && (! numVisibleSubalbums || ! env.options.selectable_albums))
							$("#everything-here.select, #every-media-individual.select").addClass("hidden");
					} else if (isPopup) {
						$("#albums-here.select, #no-albums.select").addClass("hidden");
						$("#media-here.select, #no-media.select").addClass("hidden");
					}

					if (env.options.selectable_albums && everySubalbumIsSelected) {
						$("#albums-here.select").addClass("selected");
					}

					if (everyMediaIsSelected) {
						$("#media-here.select").addClass("selected");
					}

					if ((isPopup || ! env.options.selectable_albums || everySubalbumIsSelected) && everyMediaIsSelected) {
						$("#everything-here.select").addClass("selected");
					}

					if (isABigTransversalAlbum) {
						$("#media.select").addClass("hidden");
						// $("#everything-here.select, #media-here.select").addClass("hidden");
					}

					if (! numVisibleSubalbums || isPopup || env.options.save_data) {
						$("#every-media-individual.select").addClass("hidden");
					} else if ($("#right-and-search-menu .menu").hasClass("expanded")) {
						if (env.hasOwnProperty("selectionAlbum")) {
							let everythingIndividualPromise = thisAlbum.recursivelyAllMediaAreSelected(env.selectionAlbum);
							everythingIndividualPromise.then(
								function isTrue() {
									$("#every-media-individual.select").addClass("selected");
								},
								function isFalse() {
									// do nothing
								}
							);
						}
					}


					$("#everything-here.select:not(.shortcut-help)").off("click").on(
						"click",
						function() {
							MenuFunctions.addHighlightToItem($(this));
							let albumToUse;
							if (isPopup)
								albumToUse = env.mapAlbum;
							else
								albumToUse = thisAlbum;
							util.showWorking("select-everything-here");

							if (util.absolutelyNothingIsSelected())
								env.selectionAlbum = util.initializeSelectionAlbum();

							env.selectionAlbum.addToSelectionClickHistory(albumToUse, "#everything-here", geotaggedContentIsHidden);
							let everythingPromise = albumToUse.clickSelectEverythingHere(env.selectionAlbum, geotaggedContentIsHidden);
							everythingPromise.then(
								function() {
									util.modifyCheckedStates(
										(
											isPopup ?
											"#popup-images-wrapper .select-box" :
											"#album-view .select-box"
										),
										(
											albumToUse.media.length && albumToUse.media[0].isSelected() ||
											albumToUse.subalbums.length && albumToUse.subalbums[0].isSelected()
										)
									);
									if (env.currentMedia)
										util.modifyCheckedState("#" + env.singleMediaSelectBoxId, env.currentMedia.isSelected());

									if (albumToUse.isSelection()) {
										if (util.absolutelyNothingIsSelected()) {
											env.selectionAlbum = util.initializeSelectionAlbum();
											window.location.hash = util.upHash();
										} else {
											albumToUse.updateLocationHash(true, true);
										}
									}

									util.hideWorking("select-everything-here");
									MenuFunctions.updateMenu();
								}
							);

							return false;
						}
					);

					$("#every-media-individual.select").off("click").on(
						"click",
						function() {
							MenuFunctions.addHighlightToItem($(this));
							util.showWorking("select-every-media-individual");

							if (util.absolutelyNothingIsSelected())
								env.selectionAlbum = util.initializeSelectionAlbum();

							env.selectionAlbum.addToSelectionClickHistory(thisAlbum, "#every-media-individual", geotaggedContentIsHidden);
							let everythingIndividualPromise = thisAlbum.clickSelectEverythingIndividual(env.selectionAlbum, geotaggedContentIsHidden);
							everythingIndividualPromise.then(
								function() {
									util.hideWorking("select-every-media-individual");
									if (thisAlbum.noMediaIsSelected(env.selectionAlbum)) {
										util.modifyCheckedStates("#thumbs .select-box", false);
										if (env.currentMedia)
											util.modifyCheckedState("#" + env.singleMediaSelectBoxId, false);

										$("#removed-individually").stop().fadeIn(
											1000,
											function() {
												if (thisAlbum.isSelection()) {
													if (util.absolutelyNothingIsSelected()) {
														env.selectionAlbum = util.initializeSelectionAlbum();
														window.location.hash = util.upHash();
													} else {
														thisAlbum.updateLocationHash(false, true);
													}
												}
												MenuFunctions.updateMenu();
												$("#removed-individually").stop().fadeOut(3000);
											}
										);
									} else {
										util.modifyCheckedStates("#thumbs .select-box", true);
										if (env.currentMedia)
											util.modifyCheckedState("#" + env.singleMediaSelectBoxId, true);

										$("#added-individually").stop().fadeIn(
											1000,
											function() {
												if (thisAlbum.isSelection()) {
													thisAlbum.updateLocationHash(false, true);
												}
												MenuFunctions.updateMenu();
												$("#added-individually").stop().fadeOut(3000);
											}
										);
									}
								}
							);

							return false;
						}
					);

					$("#media-here.select").off("click").on(
						"click",
						function() {
							MenuFunctions.addHighlightToItem($(this));
							var albumToUse;
							if (isPopup)
								albumToUse = env.mapAlbum;
							else
								albumToUse = thisAlbum;

							if (util.absolutelyNothingIsSelected())
								env.selectionAlbum = util.initializeSelectionAlbum();

							env.selectionAlbum.addToSelectionClickHistory(albumToUse, "#media-here", geotaggedContentIsHidden);
							albumToUse.clickSelectMediaHere(env.selectionAlbum, geotaggedContentIsHidden);

							if (albumToUse.isSelection()) {
								if (util.absolutelyNothingIsSelected()) {
									env.selectionAlbum = util.initializeSelectionAlbum();
									window.location.hash = util.upHash();
								} else {
									albumToUse.updateLocationHash(false, true);
								}
							}

							util.modifyCheckedStates(
								"#thumbs .select-box",
								albumToUse.media.length && albumToUse.media[0].isSelected()
							);
							if (env.currentMedia)
								util.modifyCheckedState("#" + env.singleMediaSelectBoxId, env.currentMedia.isSelected());

							MenuFunctions.updateMenu();

							return false;
						}
					);

					$("#albums-here.select").off("click").on(
						"click",
						function() {
							MenuFunctions.addHighlightToItem($(this));
							util.showWorking("select-albums");

							if (util.absolutelyNothingIsSelected())
								env.selectionAlbum = util.initializeSelectionAlbum();

							env.selectionAlbum.addToSelectionClickHistory(thisAlbum, "#albums-here", geotaggedContentIsHidden);
							let albumsPromise = thisAlbum.clickSelectAlbumsHere(env.selectionAlbum, geotaggedContentIsHidden);
							albumsPromise.then(
								function() {
									if (thisAlbum.isSelection()) {
										if (util.absolutelyNothingIsSelected()) {
											env.selectionAlbum = util.initializeSelectionAlbum();
											window.location.hash = util.upHash();
										} else {
											thisAlbum.updateLocationHash(true, false);
										}
									}

									util.modifyCheckedStates(
										"#subalbums .select-box",
										thisAlbum.subalbums.length && thisAlbum.subalbums[0].isSelected()
									);

									MenuFunctions.updateMenu();
									util.hideWorking("select-albums");
								}
							);
							return false;
						}
					);

					$("#global-reset.select").off("click").on(
						"click",
						function() {
							MenuFunctions.addHighlightToItem($(this));

							env.selectionAlbum = util.initializeSelectionAlbum();

							if (thisAlbum.isSelection()) {
								window.location.hash = util.upHash();
							} else {
								util.modifyCheckedStates("#album-view .select-box", false);
								if (env.currentMedia)
									util.modifyCheckedState("#" + env.singleMediaSelectBoxId, false);
							}

							MenuFunctions.updateMenu();

							return false;
						}
					);

					$("#global-reset-here.select").off("click").on(
						"click",
						function() {
							MenuFunctions.addHighlightToItem($(this));
							util.showWorking("select-global-reset-here");

							if (util.absolutelyNothingIsSelected())
								env.selectionAlbum = util.initializeSelectionAlbum();

							env.selectionAlbum.addToSelectionClickHistory(thisAlbum, "#global-reset-here", geotaggedContentIsHidden);
							let globalResetPromise = thisAlbum.clickSelectGlobalResetHere(env.selectionAlbum, geotaggedContentIsHidden);
							globalResetPromise.then(
								function() {
									util.hideWorking("select-global-reset-here");
									util.modifyCheckedStates("#album-view .select-box", false);
									if (env.currentMedia)
										util.modifyCheckedState("#" + env.singleMediaSelectBoxId, false);

									$("#made-global-reset-here").stop().fadeIn(
										1000,
										function() {
											if (thisAlbum.isSelection()) {
												env.selectionAlbum = util.initializeSelectionAlbum();
												window.location.hash = util.upHash();
											}

											MenuFunctions.updateMenu();
											$("#made-global-reset-here").stop().fadeOut(3000);
										}
									);
								}
							);
							return false;
						}
					);

					$("#nothing-here.select").off("click").on(
						"click",
						function() {
							MenuFunctions.addHighlightToItem($(this));
							var albumToUse;
							if (isPopup)
								albumToUse = env.mapAlbum;
							else
								albumToUse = thisAlbum;
							util.showWorking("select-nothing-here");

							if (util.absolutelyNothingIsSelected())
								env.selectionAlbum = util.initializeSelectionAlbum();

							env.selectionAlbum.addToSelectionClickHistory(albumToUse, "#nothing-here", geotaggedContentIsHidden);
							let nothingPromise = albumToUse.clickSelectNothingHere(env.selectionAlbum, geotaggedContentIsHidden);
							nothingPromise.then(
								function() {
									if (thisAlbum.isSelection()) {
										if (util.absolutelyNothingIsSelected()) {
											env.selectionAlbum = util.initializeSelectionAlbum();
											window.location.hash = util.upHash();
										} else {
											thisAlbum.updateLocationHash(true, true);
										}
									}

									util.modifyCheckedStates("#album-view .select-box", false);
									if (env.currentMedia)
										util.modifyCheckedState("#" + env.singleMediaSelectBoxId, false);

									MenuFunctions.updateMenu();
									util.hideWorking("select-nothing-here");
								}
							);
							return false;
						}
					);

					$("#no-albums.select").off("click").on(
						"click",
						function() {
							MenuFunctions.addHighlightToItem($(this));
							util.showWorking("select-no-albums");

							if (util.absolutelyNothingIsSelected())
								env.selectionAlbum = util.initializeSelectionAlbum();

							env.selectionAlbum.addToSelectionClickHistory(thisAlbum, "#no-albums", geotaggedContentIsHidden);
							let noAlbumsPromise = thisAlbum.clickSelectNoAlbumsHere(env.selectionAlbum, geotaggedContentIsHidden);
							noAlbumsPromise.then(
								function() {
									if (thisAlbum.isSelection()) {
										if (util.absolutelyNothingIsSelected()) {
											env.selectionAlbum = util.initializeSelectionAlbum();
											window.location.hash = util.upHash();
										} else {
											thisAlbum.updateLocationHash(true, false);
										}
									}

									util.modifyCheckedStates("#subalbums .select-box", false);

									MenuFunctions.updateMenu();
									util.hideWorking("select-no-albums");
								}
							);
							return false;
						}
					);

					$("#no-media.select").off("click").on(
						"click",
						function() {
							MenuFunctions.addHighlightToItem($(this));
							var albumToUse;
							if (isPopup)
								albumToUse = env.mapAlbum;
							else
								albumToUse = thisAlbum;

							if (util.absolutelyNothingIsSelected())
								env.selectionAlbum = util.initializeSelectionAlbum();

							env.selectionAlbum.addToSelectionClickHistory(albumToUse, "#no-media", geotaggedContentIsHidden);
							albumToUse.clickSelectNoMediaHere(env.selectionAlbum, geotaggedContentIsHidden);

							if (albumToUse.isSelection()) {
								if (util.absolutelyNothingIsSelected()) {
									env.selectionAlbum = util.initializeSelectionAlbum();
									window.location.hash = util.upHash();
								} else {
									albumToUse.updateLocationHash(false, true);
								}
							}

							util.modifyCheckedStates("#thumbs .select-box", false);
							if (env.currentMedia)
								util.modifyCheckedState("#" + env.singleMediaSelectBoxId, false);

							MenuFunctions.updateMenu();
							return false;
						}
					);

					$("#go-to-selected.select:not(.hidden)").off("click").on(
						"click",
						function(ev) {
							MenuFunctions.addHighlightToItem($(this));
							util.changeToBySelectionView(ev);
							return false;
						}
					);
				}

				////////////////// DOWNLOAD //////////////////////////////

				if (isMap) {
					$(".download-album").addClass("hidden");
				} else {
					$(".download-album").removeClass("hidden");

					let albumForDownload = thisAlbum;
					let useSelection = false;

					if (MenuFunctions.mustDownloadSelectionAlbum()) {
						albumForDownload = env.selectionAlbum;
						useSelection = true;
					}

					let albumForDownloadisABigTransversalAlbum = albumForDownload.isABigTransversalAlbum();
					let albumForDownloadIsAlbumWithOneMedia = albumForDownload.isAlbumWithOneMedia();
					let numVisibleMediaForDownload = albumForDownload.numsVisibleMediaInAlbumForDownload().imagesAudiosVideosTotal();
					let numVisibleSubalbumsForDownload = (
						env.options.download_subalbums ?
						albumForDownload.visibleSubalbums().length :
						0
					);

					if (
						isMap ||
						! useSelection && isPopup ||
						! isMapOrPopup && (
							! useSelection && (
								env.currentMedia !== null ||
								albumForDownloadIsAlbumWithOneMedia
							) ||
							albumForDownload !== null && (
								! env.options.download_subalbums ||
								albumForDownload.numsVisibleMediaInSubtreeForDownload().imagesAudiosVideosTotal() ===
								albumForDownload.numsVisibleMediaInAlbumForDownload().imagesAudiosVideosTotal() ||
								albumForDownloadisABigTransversalAlbum
							)
						)
					) {
						$(".download-option-flat").addClass("hidden");
					} else {
						$(".download-option-flat").removeClass("hidden");
						if (env.options.flat_downloads)
							$(".download-option-flat").addClass("selected");
						else
							$(".download-option-flat").removeClass("selected");
					}

					if (
						isMap || (
							! isMapOrPopup && ! useSelection && (
								env.currentMedia !== null ||
								albumForDownloadIsAlbumWithOneMedia
							) ||
							albumForDownload !== null && (
								// albumForDownload.numsVisibleMediaInSubtreeForDownload().images === 0 ||
								albumForDownloadisABigTransversalAlbum
							)
						)
					) {
						$(".download-option-images").addClass("hidden");
					} else {
						$(".download-option-images").removeClass("hidden");
						if (env.options.download_images)
							$(".download-option-images").addClass("selected");
						else
							$(".download-option-images").removeClass("selected");

						if (
							env.options.reduced_sizes.length &&
							(
								env.options.expose_original_media ||
								env.options.expose_full_size_media_in_cache
							)
						) {
							$(".download-option-images .download-option-change").removeClass("hidden");
							if (
								env.options.expose_original_media &&
								env.options.download_images_size_index === "original"
							) {
								$(".download-option-images .download-option-original").removeClass("hidden");
								$(".download-option-images .download-option-full").addClass("hidden");
								$(".download-option-images .download-option-sized").addClass("hidden");
								$(".download-option-images .download-option-format").addClass("hidden");
							} else if (
								env.options.expose_full_size_media_in_cache &&
								env.options.download_images_size_index === "fullSize"
							) {
								$(".download-option-images .download-option-original").addClass("hidden");
								$(".download-option-images .download-option-full").removeClass("hidden");
								$(".download-option-images .download-option-sized").addClass("hidden");
								$(".download-option-images .download-option-format").html(env.options.download_images_format);
								$(".download-option-images .download-option-format").removeClass("hidden");
							} else {
								$(".download-option-images .download-option-original").addClass("hidden");
								$(".download-option-images .download-option-full").addClass("hidden");
								$(".download-option-images .download-option-sized").removeClass("hidden");
								$(".download-option-images .download-option-sized").html(
									env.options.reduced_sizes[env.options.download_images_size_index] + "px"
								);
								$(".download-option-images .download-option-format").html(env.options.download_images_format);
								$(".download-option-images .download-option-format").removeClass("hidden");
							}
						} else {
							$(".download-option-images .download-option-change").addClass("hidden");
						}
					}

					if (
						isMap || (
							! isMapOrPopup && ! useSelection && (
								env.currentMedia !== null ||
								albumForDownloadIsAlbumWithOneMedia
							) ||
							albumForDownload !== null && (
								// albumForDownload.numsVisibleMediaInSubtreeForDownload().audios === 0 ||
								albumForDownloadisABigTransversalAlbum
							)
						)
					) {
						$(".download-option-audios").addClass("hidden");
					} else {
						$(".download-option-audios").removeClass("hidden");
						if (env.options.download_audios)
							$(".download-option-audios").addClass("selected");
						else
							$(".download-option-audios").removeClass("selected");

						if (
							env.options.expose_original_media ||
							env.options.expose_full_size_media_in_cache
						) {
							$(".download-option-audios .download-option-change").removeClass("hidden");
							if (env.options.download_audios_size === "original") {
								$(".download-option-audios .download-option-original").removeClass("hidden");
								$(".download-option-audios .download-option-full").addClass("hidden");
								$(".download-option-audios .download-option-sized").addClass("hidden");
							} else if (env.options.download_audios_size === "fullSize") {
								$(".download-option-audios .download-option-full").removeClass("hidden");
								$(".download-option-audios .download-option-original").addClass("hidden");
								$(".download-option-audios .download-option-sized").addClass("hidden");
							} else if (env.options.download_audios_size === "transcoded") {
								$(".download-option-audios .download-option-sized").removeClass("hidden");
								$(".download-option-audios .download-option-original").addClass("hidden");
								$(".download-option-audios .download-option-full").addClass("hidden");
							}
						} else {
							$(".download-option-audios .download-option-change").addClass("hidden");
						}
					}

					if (
						isMap || (
							! isMapOrPopup && ! useSelection && (
								env.currentMedia !== null ||
								albumForDownloadIsAlbumWithOneMedia
							) ||
							albumForDownload !== null && (
								// albumForDownload.numsVisibleMediaInSubtreeForDownload().videos === 0 ||
								albumForDownloadisABigTransversalAlbum
							)
						)
					) {
						$(".download-option-videos").addClass("hidden");
					} else {
						$(".download-option-videos").removeClass("hidden");
						if (env.options.download_videos)
							$(".download-option-videos").addClass("selected");
						else
							$(".download-option-videos").removeClass("selected");

						if (
							env.options.expose_original_media ||
							env.options.expose_full_size_media_in_cache
						) {
							$(".download-option-videos .download-option-change").removeClass("hidden");
							if (env.options.download_videos_size === "original") {
								$(".download-option-videos .download-option-original").removeClass("hidden");
								$(".download-option-videos .download-option-full").addClass("hidden");
								$(".download-option-videos .download-option-sized").addClass("hidden");
							} else if (env.options.download_videos_size === "fullSize") {
								$(".download-option-videos .download-option-full").removeClass("hidden");
								$(".download-option-videos .download-option-original").addClass("hidden");
								$(".download-option-videos .download-option-sized").addClass("hidden");
							} else if (env.options.download_videos_size === "transcoded") {
								$(".download-option-videos .download-option-sized").removeClass("hidden");
								$(".download-option-videos .download-option-original").addClass("hidden");
								$(".download-option-videos .download-option-full").addClass("hidden");
							}
						} else {
							$(".download-option-videos .download-option-change").addClass("hidden");
						}
					}

					if (
						isMap ||
						isPopup ||
						! isMapOrPopup && ! useSelection && (
							env.currentMedia !== null ||
							albumForDownloadIsAlbumWithOneMedia
						) ||
						albumForDownload !== null && (
							numVisibleSubalbums && ! numVisibleMediaForDownload ||
							! numVisibleSubalbums && numVisibleMediaForDownload ||
							albumForDownloadisABigTransversalAlbum
						)
					) {
						$(".download-option-subalbums").addClass("hidden");
					} else {
						$(".download-option-subalbums").removeClass("hidden");
						if (env.options.download_subalbums)
							$(".download-option-subalbums").addClass("selected");
						else
							$(".download-option-subalbums").removeClass("selected");
					}

					if (
						thisAlbum.isSelection() ||
						! env.selectionAlbum ||
						! env.selectionAlbum.numVisibleMedia() ||
						isMap ||
						isPopup && (
							env.options.download_selection_here
						) && env.mapAlbum.visibleMedia().every(
							singleMedia => ! singleMedia.isSelected()
						) ||
						! isMapOrPopup &&
						env.options.download_selection_here && ! albumForDownload.numVisibleSubalbums() &&
						albumForDownload.visibleMedia().every(
							singleMedia => ! singleMedia.isSelected()
						)
					) {
						$(".download-option-selection").addClass("hidden");
					} else {
						$(".download-option-selection").removeClass("hidden");
						if (env.options.download_selection)
							$(".download-option-selection").addClass("selected");
						else
							$(".download-option-selection").removeClass("selected");
					}

					$(".download-single-media").addClass("hidden").addClass("active");
					$(".download-album ul li.download-album").addClass("hidden").removeClass("red");
					$(".download-album ul li.download-album:not(.selection)").addClass("active");
					$(".download-album ul li.download-album.sized").addClass("hidden");

					if (
						env.hasOwnProperty("selectionAlbum") &&
						env.selectionAlbum.visibleNumsMediaInSubTree().imagesAudiosVideosTotal() &&
						! albumForDownload.isSelection()
					) {
						$(".download-album.selection").removeClass("hidden");
						$(".download-album.selection").attr("title", util._t(".how-to-download-selection").replace(/<br \/>/gm, ' '));
					}

					if (albumForDownload.isSearch() && ! numVisibleMedia && ! numVisibleSubalbums) {
						// download menu item remains hidden
					} else if (env.currentMedia !== null || albumForDownloadIsAlbumWithOneMedia) {
						let currentMedia = null;
						if (useSelection) {
							currentMedia = albumForDownload.media[0];
							if (
								currentMedia.isImage() && ! env.options.download_images ||
								currentMedia.isAudio() && ! env.options.download_audios ||
								currentMedia.isVideo() && ! env.options.download_videos
							)
								currentMedia = null;
						} else if (util.isPopup()) {
							currentMedia = env.mapAlbum.media[0];
						} else {
							currentMedia = env.currentMedia;
						}

						$(".download-album.expandable, .download-album.caption").removeClass("hidden");
						// let fullSizeMediaPath;
						// if (albumForDownloadIsAlbumWithOneMedia)
						// 	fullSizeMediaPath =
						// 		encodeURI(albumForDownload.visibleMedia()[0].fullSizeMediaPath());
						// else
						// 	fullSizeMediaPath = encodeURI(currentMedia.fullSizeMediaPath());
						// $(".download-single-media").attr("file", fullSizeMediaPath);
						let pixelSize = "";
						let downloadSize = 0;
						if (currentMedia) {
							$(".download-single-media").removeClass("hidden");
							if (currentMedia.fileSizes.hasOwnProperty("original"))
								downloadSize = currentMedia.fileSizes.original.imagesAudiosVideosTotal();
							else if (currentMedia.fileSizes[env.options.format].hasOwnProperty(0))
								downloadSize = currentMedia.fileSizes[env.options.format][0].imagesAudiosVideosTotal();
							if (! env.options.expose_original_media && ! env.options.expose_full_size_media_in_cache && env.options.reduced_sizes.length) {
								pixelSize = ", " + env.numberFormat.format(env.options.reduced_sizes[0]) + "px";
								downloadSize = currentMedia.fileSizes[env.options.format][env.options.reduced_sizes[0]].imagesAudiosVideosTotal();
							}
							let downloadBeginning;
							if (currentMedia.isImage())
								downloadBeginning  = util._t(".download-image");
							else if (currentMedia.isAudio())
								downloadBeginning  = util._t(".download-audio");
							else if (currentMedia.isVideo())
								downloadBeginning  = util._t(".download-video");

							$(".download-single-media").html(
								downloadBeginning + pixelSize + ": " +
								util.humanFileSize(downloadSize) +
								" [" + env.shortcuts[env.language][".download-link-shortcut"] + "]"
							);
						}

					} else if (albumForDownload !== null && $("#right-and-search-menu .menu").hasClass("expanded")) {
						$(".download-album.expandable, .download-album.caption").removeClass("hidden");

						let showDownloadEverything = false;

						// let albumForDownload;
						// if (isPopup)
						// 	albumForDownload = env.mapAlbum;
						// else if (
						// 	env.options.download_selection &&
						// 	env.selectionAlbum &&
						// 	env.selectionAlbum.numsVisibleMediaInSubtree().imagesAudiosVideosTotal()
						// )
						// 	albumForDownload = env.selectionAlbum;
						// else
						// 	albumForDownload = downloadAlbum;

						let albumOrNonGeotagged = albumForDownload;
						if (geotaggedContentIsHidden)
							albumOrNonGeotagged = albumForDownload.nonGeotagged;

						// for (let everythingOrMediaOnly of  ["everything", "media-only"]) {
						let everythingOrMediaOnly = "everything";
						if (! env.options.download_subalbums && albumForDownload.subalbums.length && albumForDownload.numsMedia.imagesAudiosVideosTotal())
							everythingOrMediaOnly = "media-only";
						let numContent, numImages, numAudios, numVideos, downloadSize, what;
						let albumForDownloadNumsMedia, albumForDownloadSizesOfAlbum;

						if (env.options.download_subalbums) {
							albumForDownloadNumsMedia = albumForDownload.visibleNumsMediaInSubTree();
							albumForDownloadSizesOfAlbum = albumOrNonGeotagged.sizesOfSubTree;
						} else {
							albumForDownloadNumsMedia = albumForDownload.visibleNumsMedia();
							albumForDownloadSizesOfAlbum = albumOrNonGeotagged.sizesOfAlbum;
						}

						numImages = albumForDownloadNumsMedia.images;
						numAudios = albumForDownloadNumsMedia.audios;
						numVideos = albumForDownloadNumsMedia.videos;

						let numMediaInThisTree = albumForDownloadNumsMedia.imagesAudiosVideosTotal();
						let numMediaInThisAlbum = albumForDownload.numVisibleMedia();
						if (! env.options.download_subalbums) {
							numMediaInThisTree = numMediaInThisAlbum;
						}

						// for (let content of ["all", "images", "audios", "videos"]) {
						let allContent = (
							(numImages ? env.options.download_images : true) &&
							(numAudios ? env.options.download_audios : true) &&
							(numVideos ? env.options.download_videos : true)
						);

						what = "";
						numContent = 0;
						downloadSize = 0;
						for (let content of ["images", "audios", "videos"]) {
							if (env.options["download_" + content]) {
								what += ", " + util._t(".title-" + content);
								numContent += albumForDownloadNumsMedia[content];

								// TO DO: I must take into account that downloaded media are always jpg
								if (env.options["download_" + content]) {
									if (
										content === "images" && env.options.download_images_size_index === "original" ||
										content === "audios" && env.options.download_audios_size === "original" ||
										content === "videos" && env.options.download_videos_size === "original"
									) {
										downloadSize += albumForDownloadSizesOfAlbum.original[content];
									} else if (
										content === "images" && env.options.download_images_size_index === "fullSize" ||
										content === "audios" && env.options.download_audios_size === "fullSize" ||
										content === "videos" && env.options.download_videos_size === "fullSize"
									) {
										downloadSize += albumForDownloadSizesOfAlbum[env.options.download_images_format][0][content];
									} else {
										if (content === "images")
											downloadSize += albumForDownloadSizesOfAlbum[env.options.download_images_format][
												env.options.reduced_sizes[env.options.download_images_size_index]
											][content];
										else if (content === "audios" || content === "videos")
											downloadSize += albumForDownloadSizesOfAlbum[env.options.download_images_format][
												env.options.reduced_sizes[0]
											][content];
									}
								}
							}
						}

						if (allContent)
							what = " " + util._t(".title-media");

						if (numContent) {
							$(".download-album.command").removeClass("hidden");
							// reset the html
							$(".download-album.command").html(util._t(".download-album.simple"));

							$(".download-album.command").append(
								" (" + env.numberFormat.format(numContent) + "): " +
								(downloadSize ? util.humanFileSize(downloadSize) : "")
							);
							if (downloadSize < env.bigZipSize) {
								// maximum allowable size is 500MB (see https://github.com/eligrey/FileSaver.js/#supported-browsers)
								// actually it can be less (Chrome on Android)
								// It may happen that the files are collected but nothing is saved
								$(".download-album.command").attr("title", "");
							} else if (downloadSize < env.maximumZipSize) {
								$(".download-album.command")
									.addClass("red").attr("title", util._t("#download-difficult"));
							} else {
								$(".download-album.command")
									.addClass("red").removeClass("active").attr("title", util._t("#cant-download"));
							}

							if (! downloadSize) {
								$(".download-album.command").removeClass("active");
							}

							if (everythingOrMediaOnly === "everything" && allContent)
								showDownloadEverything = true;
						}
					}
				}

				MenuFunctions.adaptDownloadOptionChangeElement();

				////////////////// NON-GEOTAGGED ONLY MODE //////////////////////////////

				if ($("#right-and-search-menu .menu").hasClass("expanded")) {
					if (env.options.expose_image_positions) {
						let mediaCount = thisAlbum.numsMediaInSubTree.imagesAudiosVideosTotal();
						let nonGeotaggedMediaCount = thisAlbum.nonGeotagged.numsMediaInSubTree.imagesAudiosVideosTotal();
						if (
							isMap || isPopup ||
							! nonGeotaggedMediaCount || nonGeotaggedMediaCount === mediaCount
						) {
							$(".non-geotagged-only").addClass("hidden");
						} else {
							$(".non-geotagged-only").removeClass("hidden");
						}

						$("#hide-geotagged-media").off("click").on(
							"click",
							function() {
								// css manages the showing/hiding of media/subalbums, the title change and the subalbums caption change
								// this function performs additional operation: highlighting the correct object and showing the correct image in subalbums

								var geotaggedContentWasHidden = util.geotaggedContentIsHidden();
								// toggle the class that hides/shows changes through css
								$("#fullscreen-wrapper").toggleClass("hide-geotagged");
								if (! geotaggedContentWasHidden)
									$("#hide-geotagged-media").addClass("selected");
								else
									$("#hide-geotagged-media").removeClass("selected");

								// activate lazy loader
								if (util.isPopup())
									$('#popup-images-wrapper').trigger("scroll");
								else if (env.currentMedia !== null)
									$("#album-view").trigger("scroll");
								else
									$(window).trigger("scroll");

								util.checkAlbumWithOneMedia();

								// highlight the menu item
								MenuFunctions.addHighlightToItem($(this));
								MenuFunctions.updateMenu();

								if (! geotaggedContentWasHidden)
									util.addClickToHiddenGeotaggedMediaPhrase();

								if (! geotaggedContentWasHidden && env.currentMedia !== null) {
									// single media view, we must hide the geotagged content
									if (env.currentMedia.hasGpsData()) {
										// I cannot keep showing the current media which is geotagged, show the album
										env.fromEscKey = true;
										$("#loading").show();
										pS.swipeDown(util.upHash());
									} else {
										// reload, so that if previous or next media is geotagged, new media will be loaded
										$(window).hashchange();
									}
								} else {
									let currentObject;
									if (thisAlbum.isAlbumWithOneMedia()) {
										currentObject = $("#thumbs").children(":not(.gps)").children()[0];
									} else {
										currentObject = util.highlightedObject();
									}
									let currentObjectIsASubalbums = util.aSubalbumIsHighlighted();
									let newObject = currentObject;
									if (! geotaggedContentWasHidden) {
										// we have to correct subalbum or media highlighting, too
										if (
											currentObjectIsASubalbums && currentObject.parent().hasClass("all-gps") ||
											! currentObjectIsASubalbums && currentObject.parent().hasClass("gps")
										) {
											newObject = util.nextObjectForHighlighting(currentObject);
											if (
												currentObjectIsASubalbums && ! util.aSubalbumIsHighlighted() ||
												! currentObjectIsASubalbums && util.aSubalbumIsHighlighted()
											) {
												newObject = util.prevObjectForHighlighting(newObject);
											}
										}
									}

									util.addHighlightToMediaOrSubalbum(newObject);

									// adapt subalbums and media caption height
									util.adaptSubalbumCaptionHeight(); // hiding/showing geotagged content
									util.adaptMediaCaptionHeight();

									if (currentObjectIsASubalbums)
										util.scrollAlbumViewToHighlightedSubalbum(newObject);
									else
										util.scrollAlbumViewToHighlightedMedia(newObject);

									if (! geotaggedContentWasHidden && $("#subalbums").is(":visible")) {
										// since we have hidden the geotagged content, we have to check and possibly correct the subalbum image
										$("#subalbums > a:not(.all-gps) img.thumbnail").each(
											function() {
												var [randomSingleMedia, randomMediaAlbumCacheBase] = util.getMediaFromSubalbumObject($(this));
												if (! randomSingleMedia.hasGpsData()) {
													return;
												} else  {
													thisAlbum.pickRandomMediaAndInsertIt(
														thisAlbum.visibleSubalbums().findIndex(
															subalbum => randomMediaAlbumCacheBase.startsWith(subalbum.cacheBase)
														)
													);
												}
											}
										);
									} else {
										// we are showing the geotagged content, a reload is needed
										env.isRevertingFromHidingGeotaggedMedia = true;
										$(window).hashchange();
									}
								}
								MenuFunctions.closeMenu();
							}
						);
					} else {
						$("#hide-geotagged-media").hide();
					}
				}

				////////////////// BIG ALBUMS //////////////////////////////

				if (
					isMapOrPopup ||
					thisAlbum === null ||
					thisAlbum.isABigTransversalAlbum()
				) {
					$("#right-and-search-menu .big-albums").addClass("hidden");
				} else {
					$("#right-and-search-menu .big-albums").removeClass("hidden");
					if (env.options.show_big_virtual_folders)
					 	$("#right-and-search-menu .big-albums").addClass("selected");
					else
						$("#right-and-search-menu .big-albums").removeClass("selected");
				}

				////////////////// SAVE DATA MODE //////////////////////////////

				if (env.options.save_data)
					$("#save-data").addClass("selected");
				else
					$("#save-data").removeClass("selected");


				////////////////// RESTORE SETTINGS //////////////////////////////

				if (! util.settingsChanged())
					$("#restore").addClass("hidden");
				else
					$("#restore").removeClass("hidden");


				////////////////// be sure that the highlighted menu entry is visible //////////////////////////////

				if ($(".first-level ul li.highlighted-menu.hidden").length) {
					// the highlingting is inside a closed first-level menu entry, move it to the parent
					$(".first-level ul li.highlighted-menu").parent().parent().addClass("highlighted-menu");
					$(".first-level ul li.highlighted-menu").removeClass("highlighted-menu");
				}

				util.setMapButtonPosition();
				util.correctElementPositions();

				////////////////// ACCORDION EFFECT //////////////////////////////

				// accordion effect on right menu
				$("#right-and-search-menu li.expandable .caption").off("click").on(
					"click",
					function(ev) {
						ev.stopPropagation();
						if ($(this).hasClass("caption"))
							MenuFunctions.addHighlightToItem($(this).parent());
						else
							MenuFunctions.addHighlightToItem($(this));
						var wasExpanded = $(this).parent().hasClass("expanded");
						$("#right-and-search-menu li.expandable").removeClass("expanded");
						$("#right-and-search-menu li.first-level ul").addClass("hidden");
						if (! wasExpanded) {
							$(this).nextAll().first().removeClass("hidden");
							$(this).parent().addClass("expanded");
						} else if ($(".first-level ul li.highlighted-menu.hidden").length) {
							// the highlingting is inside a closed first-level menu entry, move it to the parent
							$(".first-level ul li.highlighted-menu").parent().parent().addClass("highlighted-menu");
							$(".first-level ul li.highlighted-menu").removeClass("highlighted-menu");
						}

						util.setMapButtonPosition();
						// util.correctElementPositions();

						MenuFunctions.adaptDownloadOptionChangeElement();
						$(document).ready(function() {
						});

					}
				);
			}
		);
	};

	MenuFunctions.adaptDownloadOptionChangeElement = function() {
		// This function sets equal width to images/audios/videos download option

		$(".download-option-change").css("width", "auto");
		let maxWidth = Math.max(
			... $(".download-option-change").map(
				function() {
					return parseInt($(this).css("width"));
				}
			).get()
		);
		if (maxWidth)
			$(".download-option-change").css("width", (maxWidth + 10) + "px");
	};

	MenuFunctions.removeHighligthsToItems = function() {
		if (! $("#search-menu").hasClass("hidden-by-menu-selection"))
			$("#search-menu.highlighted-menu, #search-menu .highlighted-menu").removeClass("highlighted-menu");
		else
			$("#right-and-search-menu .first-level.highlighted-menu, #right-and-search-menu .first-level .highlighted-menu").removeClass("highlighted-menu");
	};

	MenuFunctions.prototype.prevItemForHighlighting = function(object) {
		var isSearchMother = object.hasClass("search");
		var isFirstLevel = object.hasClass("first-level");
		if (isSearchMother) {
			return object.children("ul").children(".active:not(.hidden)").last();
		} else if (isFirstLevel) {
			let prevFirstLevelObject = object.prevAll(".first-level.active:not(.hidden)").first();
			if (! prevFirstLevelObject.length)
				prevFirstLevelObject = object.nextAll(".first-level.active:not(.hidden)").last();

			if (
				prevFirstLevelObject.hasClass("expandable") &&
				prevFirstLevelObject.hasClass("expanded")  &&
				prevFirstLevelObject.children("ul").children(".active:not(.hidden)").length
			) {
				// go to the last child of the previous first level menu item
				return prevFirstLevelObject.children("ul").children(".active:not(.hidden)").last();
			} else {
				// go to the previous first-level menu entry
				return prevFirstLevelObject;
			}
		} else if (! object.prevAll(".active:not(.hidden)").length) {
			// go to its first-level menu entry
			return object.parent().parent();
		} else {
			// go to the previous second-level menu entry
			return object.prevAll(".active:not(.hidden)").first();
		}
	};

	MenuFunctions.prototype.nextItemForHighlighting = function(object) {
		var isSearchMother = object.hasClass("search");
		var isFirstLevel = isSearchMother || object.hasClass("first-level");
		if (isFirstLevel) {
			if (
				! isSearchMother &&
				! object.hasClass("expandable") ||
				object.hasClass("expandable") && ! object.hasClass("expanded") ||
				object.hasClass("expandable") && object.hasClass("expanded") && ! object.children("ul").children(".active:not(.hidden)").length
			) {
				// go to the next first-level menu entry
				let nextFirstLevelObject = object.nextAll(".first-level.active:not(.hidden)").first();
				if (nextFirstLevelObject.length)
					return nextFirstLevelObject;
				else
					return object.siblings(".first-level:not(.hidden)").first();
			} else if (isSearchMother || object.hasClass("expandable") && object.hasClass("expanded")) {
				// go to its first child
				return object.children("ul").children(".active:not(.hidden)").first();
			}
		}

		var isSearchChild = object.parent().parent().hasClass("search");
		if (isSearchChild && ! object.nextAll(".active:not(.hidden)").length) {
			return object.parent().parent();
		} else if (! isSearchChild && ! object.nextAll(".active:not(.hidden)").length) {
			// go to the next first-level menu entry
			let nextFirstLevelObject = object.parent().parent().nextAll(".first-level.active:not(.hidden)").first();
			if (nextFirstLevelObject.length)
				return nextFirstLevelObject;
			else
				return object.parent().parent().siblings(".first-level.active:not(.hidden)").first();
		} else {
			return object.nextAll(".active:not(.hidden)").first();
		}
	};

	MenuFunctions.openSearchMenu = function(album) {
		$("#right-and-search-menu").addClass("expanded");
		$("#search-icon, #search-menu").addClass("expanded");
		$("#menu-icon, #right-menu").removeClass("expanded");

		$("#search-menu").removeClass("hidden-by-menu-selection");
		$(".first-level").addClass("hidden-by-menu-selection");

		MenuFunctions.highlightMenu();

		$("#album-and-media-container:not(.single-media) #album-view").css("opacity", "0.5");
		$(".media-popup .leaflet-popup-content-wrapper").css("background-color", "darkgray");

		MenuFunctions.updateMenu(album);
	};

	MenuFunctions.prototype.toggleSearchMenu = function(ev, album) {
		if (! $("#right-and-search-menu").hasClass("expanded") || ! $("#search-menu").hasClass("expanded"))
			MenuFunctions.openSearchMenu(album);
		else
			MenuFunctions.closeMenu();
	};

	MenuFunctions.openRightMenu = function(album) {
		$("#right-and-search-menu").addClass("expanded");
		$("#search-icon, #search-menu").removeClass("expanded");
		$("#menu-icon, #right-menu").addClass("expanded");

		$("#search-menu").addClass("hidden-by-menu-selection");
		$(".first-level").removeClass("hidden-by-menu-selection");

		MenuFunctions.highlightMenu();

		$("#album-and-media-container:not(.single-media) #album-view").css("opacity", "0.3");
		$(".media-popup .leaflet-popup-content-wrapper").css("background-color", "darkgray");
		MenuFunctions.updateMenu(album);
	};

	MenuFunctions.prototype.toggleRightMenu = function(ev, album) {
		if (! $("#right-and-search-menu").hasClass("expanded") || ! $("#right-menu").hasClass("expanded"))
			MenuFunctions.openRightMenu(album);
		else
			MenuFunctions.closeMenu();
	};

	MenuFunctions.openMenu = function(album) {
		if ($(".first-level").hasClass("hidden-by-menu-selection")) {
			MenuFunctions.openSearchMenu();
		} else {
			MenuFunctions.openRightMenu();
		}
	};

	MenuFunctions.closeMenu = function(removeOpacity=true) {
		$("#right-and-search-menu").removeClass("expanded");
		$("#search-icon, #search-menu, #menu-icon, #right-menu").removeClass("expanded");
		$("#search-menu").addClass("hidden-by-menu-selection");

		if (removeOpacity) {
			$("#album-view").css("opacity", "");
			$(".media-popup .leaflet-popup-content-wrapper").css("background-color", "");
		}

		util.setMapButtonPosition();
		// util.correctElementPositions();
	};

	MenuFunctions.prototype.toggleMenu = function(ev, album) {
		if (! $("#right-and-search-menu").hasClass("expanded"))
			MenuFunctions.openMenu(album);
		else
			MenuFunctions.closeMenu();
	};

	MenuFunctions.prototype.focusSearchField = function() {
		if (! env.isAnyMobile) {
			$("#search-button").trigger("focus");
		} else {
			$("#search-button").blur();
		}
		if (
			! $("#search-menu .highlighted-menu").length &&
			! $("#search-menu .was-highlighted").length
		)
			$("#search-menu:not(.hidden-by-menu-selection)").addClass("highlighted-menu");
		else if (
			! $("#search-menu .highlighted-menu").length &&
			$("#search-menu .was-highlighted").length
		)
			$("#search-menu:not(.hidden-by-menu-selection) .was-highlighted").removeClass("was-highlighted").addClass("highlighted-menu");
	};

	MenuFunctions.highlightMenu = function() {
		if (
			! $("#search-menu.highlighted-menu").length &&
			! $("#search-menu .highlighted-menu").length &&
			! $("#search-menu.was-highlighted").length &&
			! $("#search-menu .was-highlighted").length
		)
			$("#search-menu:not(.hidden-by-menu-selection)").addClass("highlighted-menu");
		else if(
			! $("#search-menu.highlighted-menu").length &&
			! $("#search-menu .highlighted-menu").length && (
				$("#search-menu.was-highlighted").length ||
				$("#search-menu .was-highlighted").length
			)
		)
			$("#search-menu:not(.hidden-by-menu-selection).was-highlighted").removeClass("was-highlighted").addClass("highlighted-menu");

		if (
			! $("#right-and-search-menu li.first-level.highlighted-menu").length &&
			! $("#right-and-search-menu li.first-level .highlighted-menu").length &&
			! $("#right-and-search-menu li.first-level.was-highlighted").length &&
			! $("#right-and-search-menu li.first-level .was-highlighted").length
		)
			$("#right-and-search-menu li.first-level:not(.hidden-by-menu-selection)").first().addClass("highlighted-menu");
		else if(
			! $("#right-and-search-menu li.first-level.highlighted-menu").length &&
			! $("#right-and-search-menu li.first-level .highlighted-menu").length && (
				$("#right-and-search-menu li.first-level.was-highlighted").length ||
				$("#right-and-search-menu li.first-level .was-highlighted").length
			)
		)
			$("#right-and-search-menu li.first-level:not(.hidden-by-menu-selection).was-highlighted").removeClass("was-highlighted").addClass("highlighted-menu");
	};

	MenuFunctions.addHighlightToItem = function(object) {
		if(object.attr("id") !== "menu-icon" && object.attr("id") !== "search-icon")
			MenuFunctions.removeHighligthsToItems();
		object.addClass("highlighted-menu");
	};

	MenuFunctions.prototype.highlightedItemObject = function() {
		if (! $("#search-menu").hasClass("hidden-by-menu-selection"))
			return $("#search-menu.highlighted-menu, #search-menu .highlighted-menu");
		else
			return $("#right-and-search-menu .first-level.highlighted-menu, #right-and-search-menu .first-level .highlighted-menu");
	};

	MenuFunctions.getBooleanCookie = function(key) {
		var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
		if (! keyValue)
			return null;
		else if (keyValue[2] === "1")
			return true;
		else
			return false;
	};

	MenuFunctions.setBooleanCookie = function(key, value) {
		if (value)
			value = 1;
		else
			value = 0;
		MenuFunctions.setCookie(key, value);
		return true;
	};

	MenuFunctions.getCookie = function(key) {
		var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
		if (! keyValue)
			return null;
		else
			return keyValue[2];
	};

	MenuFunctions.getNumberCookie = function(key) {
		var keyValue = MenuFunctions.getCookie(key);
		if (keyValue === null)
			return null;
		else
			return parseFloat(keyValue);
	};

	MenuFunctions.setCookie = function(key, value) {
		var expires = new Date();
		expires.setTime(expires.getTime() + util.tenYears() * 1000);
		document.cookie = key + '=' + value + ';expires=' + expires.toUTCString() + ';samesite=lax';
		return true;
	};

	MenuFunctions.prototype.sendEmail = function(href, selectorToFadeOut) {
		// location.href = href;
		var request = new XMLHttpRequest();
		request.onreadystatechange = function() {
			var self = this;
			if (self.readyState === 4) {
				$(selectorToFadeOut).fadeOut(100);
				if (self.status >= 200 && self.status < 400) {
					if (self.responseText.indexOf("email sent!") !== -1) {
						// the email has been sent
						$("#email-sent").stop().fadeIn(
							500,
							function() {
								$("#email-sent").fadeOut(3000);
							}
						);
					} else {
						// the email hasn't been sent
						$("#email-not-sent").stop().fadeIn(
							500,
							function() {
								$("#email-not-sent").append("<br /><span class='little'>"+ self.responseText + "</span>");
								$("#email-not-sent").off("click").on(
									"click",
									function() {
										$("#email-not-sent").fadeOut(500);
									}
								);
							}
						);
					}
				}
			}
		};
		request.open("GET", href, true);
		// request.open("GET", window.location.origin + window.location.pathname + href, true);
		// request.setRequestHeader('Content-Type', 'application/json');
		request.send();
	};

	MenuFunctions.prototype.showHideDownloadSelectionInfo = function() {
		if (
			$(".download-album.selection").hasClass("highlighted-menu") &&
			$("#right-and-search-menu").hasClass("expanded") &&
			$("#right-menu").hasClass("expanded")
		) {
			$("#how-to-download-selection").show();
			$('#how-to-download-selection-close').off("click").on(
				"click",
				function() {
					$("#how-to-download-selection").hide();
				}
			);
		} else {
			if ($('#how-to-download-selection-close').is(":visible"))
				$('#how-to-download-selection-close')[0].click();
		}
	};

	MenuFunctions.prototype.getBooleanCookie = MenuFunctions.getBooleanCookie;
	MenuFunctions.prototype.setBooleanCookie = MenuFunctions.setBooleanCookie;
	MenuFunctions.prototype.setCookie = MenuFunctions.setCookie;
	MenuFunctions.prototype.updateMenu = MenuFunctions.updateMenu;
	MenuFunctions.prototype.openSearchMenu = MenuFunctions.openSearchMenu;
	MenuFunctions.prototype.closeMenu = MenuFunctions.closeMenu;
	MenuFunctions.prototype.openRightMenu = MenuFunctions.openRightMenu;
	MenuFunctions.prototype.highlightMenu = MenuFunctions.highlightMenu;
	MenuFunctions.prototype.addHighlightToItem = MenuFunctions.addHighlightToItem;
	MenuFunctions.prototype.getCookie = MenuFunctions.getCookie;
	MenuFunctions.prototype.mustDownloadSelectionAlbum = MenuFunctions.mustDownloadSelectionAlbum;

	window.MenuFunctions = MenuFunctions;
}());
