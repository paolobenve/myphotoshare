(function() {

	var phFl = new PhotoFloat();
	var util = new Utilities();
	var menuF = new MenuFunctions();
	var tF = new TopFunctions();
	var mapF;
	$.executeAfterEvent(
		"mapFunctionsLoadedEvent",
		function() {
			mapF = new MapFunctions();
		}
	);

	Album.prototype.endPreparingAlbum = function() {
		// this function is called after a search album or a map album is prepared

		// add the various counts
		this.numsMedia = this.media.imagesAudiosVideosCount();

		if (env.options.expose_image_positions) {
			this.nonGeotagged.numsMedia = this.media.filter(
				singleMedia => ! singleMedia.hasGpsData()
			).imagesAudiosVideosCount();

			this.numPositionsInTree = this.positionsAndMediaInTree.length;
		}

		// save in the cache array
		if (! env.cache.getAlbum(this.cacheBase))
			env.cache.putAlbum(this);

		this.sortAlbumsMedia();

		$("#loading").hide();

	};

	Album.prototype.albumSelectBoxSelector = function() {
		return util.albumSelectBoxSelector(this.cacheBase);
	};

	Album.prototype.generatePositionsAndMediaInMedia = function() {
		if (this.hasOwnProperty("positionsAndMediaInMedia"))
			return;

		if (this.subalbums.length) {
			this.positionsAndMediaInMedia = new PositionsAndMedia([]);
			var self = this;
			this.media.forEach(
				function(singleMedia) {
					if (singleMedia.hasGpsData())
						self.positionsAndMediaInMedia.addPositionAndMedia(singleMedia.generatePositionAndMedia(self));
				}
			);
		} else {
			this.positionsAndMediaInMedia = new PositionsAndMedia(this.positionsAndMediaInTree);
		}
	};

	Album.prototype.isEmpty = function() {
		if (this.empty !== undefined && this.empty)
			return true;

		if (this.isMap())
			return ! this.positionsAndMediaInTree.length;

		if (this.isSearch() || this.isSelection())
			return ! this.media.length && ! this.subalbums.length;

		return false;
	};

	Album.prototype.hasGpsData = function() {
		return env.options.expose_image_positions && this.nonGeotagged.numsMediaInSubTree.imagesAudiosVideosTotal() === 0;
	};

	Album.prototype.toAlbum = function(error, {getMedia = false, getPositions = false}) {
		var self = this;
		return new Promise(
			function(resolve_convertIntoAlbum) {
				let promise = phFl.getAlbum(self.cacheBase, error, {getMedia: getMedia, getPositions: getPositions});
				promise.then(
					function(convertedSubalbum) {
						resolve_convertIntoAlbum(convertedSubalbum);
					}
				);
			}
		);
	};

	Album.prototype.clone = function() {
		var isCloning = true;
		return new Album(this, isCloning);
	};

	Album.prototype.toSubalbum = function() {
		var isCloning = true;
		return new Subalbum(this, isCloning);
	};

	Album.prototype.toJson = function() {
		var albumProperties = [
			"albumSort",
			"albumReverseSort",
			"ancestorsCacheBase",
			"ancestorsNames",
			"ancestorsTitles",
			"cacheBase",
			"cacheSubdir",
			"compositeImageSize",
			"includedFilesByCodesSimpleCombination",
			"description",
			"jsonVersion",
			"media",
			"mediaSort",
			"mediaReverseSort",
			"name",
			"numsMedia",
			"numsMediaInSubTree",
			"numsProtectedMediaInSubTree",
			"path",
			"physicalPath",
			"captionsForSelection",
			"captionForSelectionSorting",
			"captionsForSearch",
			"captionForSearchSorting",
			"sizesOfAlbum",
			"sizesOfSubTree",
			"subalbums",
			"tags",
			"title"
		];
		if (env.options.expose_image_dates) {
			albumProperties.push("exifDateMax");
			albumProperties.push("exifDateMin");
		}
		if (env.options.expose_image_positions) {
			albumProperties.push("nonGeotagged");
		}
		albumProperties.push("numPositionsInMedia");
		albumProperties.push("numPositionsInSubalbums");
		albumProperties.push("numPositionsInTree");
		albumProperties.push("positionsAndMediaInMedia");
		albumProperties.push("positionsAndMediaInSubalbums");
		albumProperties.push("positionsAndMediaInTree");
		var clonedAlbum = this.clone();
		Object.keys(this).forEach(
			function(key) {
				if (albumProperties.indexOf(key) === -1) {
					delete clonedAlbum[key];
				}
			}
		);
		return JSON.stringify(clonedAlbum);
	};

	Album.prototype.removeUnnecessaryPropertiesAndAddParentToMedia = function() {
		// remove unnecessary properties from album
		var unnecessaryProperties = ["albumIniMTime", "passwordMarkerMTime"];
		for (let j = 0; j < unnecessaryProperties.length; j ++) {
			if (this.hasOwnProperty(unnecessaryProperties[j]))
				delete this[unnecessaryProperties[j]];
		}
		if (this.hasOwnProperty("media"))
			this.media.removeUnnecessaryPropertiesAndAddParent(this);
	};

	Album.prototype.hasPositionsInMedia = function() {
		var result =
			env.options.expose_image_positions &&
			this.hasOwnProperty("media") &&
			this.media.length &&
			this.media.some(singleMedia => singleMedia.hasGpsData());
		return result;
	};

	Album.prototype.hasValidPositionsAndMediaInMediaAndSubalbums = function() {
		return this.hasOwnProperty("positionsAndMediaInMedia");
	};

	Album.prototype.invalidatePositionsAndMediaInAlbumAndSubalbums = function() {
		if (this.hasOwnProperty("positionsAndMediaInMedia"))
			delete this.positionsAndMediaInMedia;
	};

	Album.prototype.isEqual = function(otherAlbum) {
		return otherAlbum !== null && this.cacheBase === otherAlbum.cacheBase;
	};

	Album.prototype.initializeSearchAlbumEnd = function() {
	// 	var rootSearchAlbum = env.cache.getAlbum(env.options.by_search_string);
	// 	if (! rootSearchAlbum) {
	// 		util.initializeSearchRootAlbum();
	// 		rootSearchAlbum = env.cache.getAlbum(env.options.by_search_string);
	// 	}
	//
	// 	// rootSearchAlbum.numsMediaInSubTree.sum(this.numsMediaInSubTree);
	// 	// if (rootSearchAlbum.hasOwnProperty("positionsAndMediaInTree") && this.hasOwnProperty("positionsAndMediaInTree")) {
	// 	// 	rootSearchAlbum.positionsAndMediaInTree.mergePositionsAndMedia(this.positionsAndMediaInTree);
	// 	// }
	// 	// if (this.hasOwnProperty("numPositionsInTree"))
	// 	// 	rootSearchAlbum.numPositionsInTree += this.numPositionsInTree;
	//
	// 	this.ancestorsCacheBase = [env.options.by_search_string, this.cacheBase];
	// 	this.name = "(" + Utilities._t("#by-search") + ")";
	};

	Album.prototype.guessedPasswordCodes = function() {
		return util.arrayIntersect(this.passwordCodes(), env.guessedPasswordCodes);
	};

	Album.prototype.passwordCodes = function() {
		var self;
		if (this.isSearch())
			self = env.cache.getAlbum(env.options.by_search_string);
		else
			self = this;

		var codes = [];
		if (self.hasOwnProperty("numsProtectedMediaInSubTree")) {
			Object.keys(self.numsProtectedMediaInSubTree).forEach(
				function(codesComplexCombination) {
					if (codesComplexCombination === ",")
						return;
					let combinations = codesComplexCombination.replace(",", "-").split("-");
					var indexOfVoidString = combinations.indexOf("");
					if (indexOfVoidString !== -1)
						combinations.splice(indexOfVoidString, 1);

					combinations.forEach(
						function(combination) {
							var codesFromCombination = combination.split("-");
							if (typeof codesFromCombination === "string")
								codesFromCombination = [codesFromCombination];
							codesFromCombination.forEach(
								function(code) {
									if (! codes.includes(code))
										codes.push(code);
								}
							);
						}
					);
				}
			);
		}
		return codes;
	};

	Album.prototype.guessedCodesSimpleCombinations = function() {
		var guessed = [];
		this.codesSimpleCombinations().forEach(
			codesSimpleCombination => {
				var albumCombination = codesSimpleCombination.split(",")[0];
				var mediaCombination = codesSimpleCombination.split(",")[1];
				if (
					(albumCombination === "" || env.guessedPasswordCodes.indexOf(albumCombination) !== -1) &&
					(mediaCombination === "" || env.guessedPasswordCodes.indexOf(mediaCombination) !== -1)
				)
					guessed.push(codesSimpleCombination);
			}
		);
		return guessed;
	};

	Album.prototype.codesSimpleCombinations = function() {
		var self;
		if (this.isSearch())
			self = env.cache.getAlbum(env.options.by_search_string);
		else
			self = this;

		var codesSimpleCombinations = [];
		Object.keys(self.numsProtectedMediaInSubTree).forEach(
			function(codesComplexCombination) {
				if (codesComplexCombination === ",")
					return;
				var albumCombinations = codesComplexCombination.split(",")[0].split("-");
				if (albumCombinations.length && typeof albumCombinations === "string")
					albumCombinations = [albumCombinations];
				var mediaCombinations = codesComplexCombination.split(",")[1].split("-");
				if (mediaCombinations.length && typeof mediaCombinations === "string")
					mediaCombinations = [mediaCombinations];
				if (albumCombinations.length) {
					albumCombinations.forEach(
						albumCode => {
							var combination = albumCode + ",";
							if (mediaCombinations.length) {
								mediaCombinations.forEach(
									mediaCode => {
										combination = albumCode + "," + mediaCode;
										if (codesSimpleCombinations.indexOf(combination) === -1)
											codesSimpleCombinations.push(combination);
									}
								);
							} else {
								if (codesSimpleCombinations.indexOf(combination) === -1)
									codesSimpleCombinations.push(combination);
							}
						}
					);
				} else {
					mediaCombinations.forEach(
						mediaCode => {
							var combination = "," + mediaCode;
							if (codesSimpleCombinations.indexOf(combination) === -1)
								codesSimpleCombinations.push(combination);
						}
					);
				}
			}
		);
		return codesSimpleCombinations;
	};

	Album.prototype.sortByPath = function() {
		if (this.subalbums.length) {
			if (this.isSelection()) {
				util.sortBy(this.subalbums, ["captionForSelectionSorting"]);
				// this.subalbums = util.sortBy(this.subalbums, ["altName", "name", "path"]);
			} else if (this.isSearch()) {
				util.sortBy(this.subalbums, ["captionForSelectionSorting"]);
				// this.subalbums = util.sortBy(this.subalbums, ["altName", "name", "path"]);
			} else if (this.isByGps()) {
				if (this.subalbums[0].hasOwnProperty("altName"))
					util.sortBy(this.subalbums, ["altName"]);
				else
					util.sortBy(this.subalbums, ["name"]);
			} else {
				util.sortBy(this.subalbums, ["path"]);
			}
		}
	};

	Album.prototype.isAnyRoot = function() {
		return util.isAnyRootCacheBase(this.cacheBase);
	};

	Album.prototype.isAnyRootOrCollection = function() {
		return util.isAnyRootOrCollectionCacheBase(this.cacheBase);
	};

	Album.prototype.isFolder = function() {
		return util.isFolderCacheBase(this.cacheBase);
	};

	Album.prototype.isByDate = function() {
		return util.isByDateCacheBase(this.cacheBase);
	};

	Album.prototype.isByGps = function() {
		return util.isByGpsCacheBase(this.cacheBase);
	};

	Album.prototype.isSearch = function() {
		return util.isSearchCacheBase(this.cacheBase);
	};

	Album.prototype.isSelection = function() {
		return util.isSelectionCacheBase(this.cacheBase);
	};

	Album.prototype.isMap = function() {
		return util.isMapCacheBase(this.cacheBase);
	};

	Album.prototype.isTransversal =  function() {
		return this.isByDate() || this.isByGps();
	};

	Album.prototype.isVirtual =  function() {
		return this.isSelection() || this.isMap();
	};

	Album.prototype.isCollection =  function() {
		return this.isSearch() || this.isVirtual();
	};

	Album.prototype.isGenerated =  function() {
		return this.isTransversal() || this.isCollection();
	};

	Album.prototype.isSelected = function(selectionAlbum = env.selectionAlbum) {
		return util.albumIsSelected(this, selectionAlbum);
	};

	Album.prototype.isAlbumWithOneMedia = function() {
		return (
			this !== null &&
			this.visibleNumsMedia().imagesAudiosVideosTotal() === 1 &&
			this.visibleNumsMediaInSubTree().imagesAudiosVideosTotal() === 1
		);
	};

	Album.prototype.generateCaptionsForCollections = function() {
		var raquo = " <span class='gray'>&raquo;</span> ";
		var folderArray = this.cacheBase.split(env.options.cache_folder_separator);

		var firstLine;
		if (this.isByDate()) {
			firstLine = util.dateElementForFolderName(folderArray, folderArray.length - 1);
		} else if (this.isByGps()) {
			if (this.name === '')
				firstLine = util._t('.not-specified');
			else if (this.hasOwnProperty('altName'))
				firstLine = util.transformAltPlaceName(this.altName);
			else
				firstLine = this.nameForShowing(null, true, true);
		} else {
			firstLine = this.nameForShowing(null, true, true);
		}

		var secondLine = '';
		if (this.isByDate()) {
			secondLine += "<span class='gray'>(";
			if (folderArray.length === 2) {
				secondLine += util._t("#year-album");
			} else if (folderArray.length === 3) {
				secondLine += util._t("#month-album") + " ";
			} else if (folderArray.length === 4) {
				secondLine += util._t("#day-album") + ", ";
			}
			secondLine += "</span>";
			if (folderArray.length > 2) {
				if (folderArray.length === 4)
					secondLine += util.dateElementForFolderName(folderArray, 2) + " ";
				secondLine += util.dateElementForFolderName(folderArray, 1);
			}
			secondLine += "<span class='gray'>)</span>";
		} else if (this.isByGps()) {
			for (let iCacheBase = 1; iCacheBase < this.ancestorsCacheBase.length - 1; iCacheBase ++) {
				let albumName;
				if (this.ancestorsNames[iCacheBase] === '')
					albumName = util._t('.not-specified');
				else
					albumName = this.ancestorsNames[iCacheBase];
				if (iCacheBase === 1)
					secondLine = "<span class='gray'>(" + util._t("#by-gps-album-in") + "</span> ";
				// let marker = "<marker>" + iCacheBase + "</marker>";
				// secondLine += marker;
				secondLine += "<a href='" + env.hashBeginning + this.ancestorsCacheBase[iCacheBase] + "'>" + albumName + "</a>";
				if (iCacheBase < this.ancestorsCacheBase.length - 2)
					secondLine += raquo;
				if (iCacheBase === this.ancestorsCacheBase.length - 2)
					secondLine += "<span class='gray'>)</span>";

				// secondLine = secondLine.replace(marker, "<a href='" + env.hashBeginning + this.ancestorsCacheBase[iCacheBase] + "'>" + albumName + "</a>");
			}
			if (! secondLine)
				secondLine = "<span class='gray'>(" + util._t("#by-gps-album") + ")</span>";
		} else if (this.cacheBase === env.options.folders_string) {
			secondLine += "<span class='gray'>(" + util._t("#root-album") + ")</span>";
		} else {
			for (let iCacheBase = 0; iCacheBase < this.ancestorsCacheBase.length - 1; iCacheBase ++) {
				if (iCacheBase === 0 && this.ancestorsCacheBase.length === 2) {
					secondLine = "<span class='gray'>(" + util._t("#regular-album") + ")</span> ";
				} else {
					if (iCacheBase === 1)
						secondLine = "<span class='gray'>(" + util._t("#regular-album-in") + "</span> ";
					// let marker = "<marker>" + iCacheBase + "</marker>";
					// secondLine += marker;
					secondLine += "<a href='" + env.hashBeginning + this.ancestorsCacheBase[iCacheBase] + "'>" + this.ancestorsNames[iCacheBase] + "</a>";
					if (iCacheBase < this.ancestorsCacheBase.length - 2)
						secondLine += raquo;
					if (iCacheBase === this.ancestorsCacheBase.length - 2)
						secondLine += "<span class='gray'>)</span>";
					// secondLine = secondLine.replace(marker, "<a href='" + env.hashBeginning + this.ancestorsCacheBase[iCacheBase] + "'>" + this.ancestorsNames[iCacheBase] + "</a>");
				}
			}
		}
		var captionsForCollection = util.addSpanToFirstAndSecondLine(firstLine, secondLine);
		var captionForCollectionSorting = util.convertByDateAncestorNames(this.ancestorsNames).slice(1).reverse().join(env.options.cache_folder_separator).replace(/^0+/, '');
		return [captionsForCollection, captionForCollectionSorting];
	};

	Album.prototype.generateCaptionsForSelection = function() {
		if (this.isSearch())
			[this.captionsForSelection, this.captionForSelectionSorting] = [this.captionsForSearch, this.captionForSearchSorting];
		else if (this.isMap())
			[this.captionsForSelection, this.captionForSelectionSorting] = [this.captionsForPopup, this.captionForPopupSorting];
		else
			[this.captionsForSelection, this.captionForSelectionSorting] = this.generateCaptionsForCollections();
	};

	Album.prototype.generateCaptionsForSearch = function() {
		if (this.isSelection())
			[this.captionsForSearch, this.captionForSearchSorting] = [this.captionsForSelection, this.captionForSelectionSorting];
		else if (this.isMap())
			[this.captionsForSearch, this.captionForSearchSorting] = [this.captionsForPopup, this.captionForPopupSorting];
		else
			[this.captionsForSearch, this.captionForSearchSorting] = this.generateCaptionsForCollections(this);
	};

	Album.prototype.nameForShowing = function(parentAlbum, html = false, br = false) {
		return util.nameForShowing(this, parentAlbum, html, br);
	};

	Album.prototype.folderMapTitle = function(subalbum, folderName) {
		var folderMapTitle;
		if (this.isSelection() && subalbum.isByDate()) {
			let reducedFolderName = folderName.substring(0, folderName.indexOf(env.br));
			folderMapTitle = util._t("#place-icon-title") + reducedFolderName;
		} else if (this.isSelection() && subalbum.isByGps()) {
			if (subalbum.name === "")
				folderMapTitle = util._t(".not-specified");
			else if (subalbum.hasOwnProperty("altName"))
				folderMapTitle = util.transformAltPlaceName(subalbum.altName);
			else
				folderMapTitle = subalbum.nameForShowing(this);
			folderMapTitle = util._t("#place-icon-title") + folderMapTitle;
		} else {
			folderMapTitle = util._t("#place-icon-title") + folderName;
		}
		return folderMapTitle;
	};

	Album.prototype.hasProperty = function(property) {
		return util.hasProperty(this, property);
	};

	Album.prototype.hasSomeDescription = function(property = null) {
		return util.hasSomeDescription(this, property);
	};

	Album.prototype.setDescription = function() {
		util.setDescription(this);
	};

	Album.prototype.generatePositionsAndMediaInMediaAndSubalbums = function() {
		var self = this;
		return new Promise(
			function(resolve_generatePositionsAndMediaInMediaAndSubalbums) {
				let numSubalbums = self.subalbums.length;
				if (
					self.hasValidPositionsAndMediaInMediaAndSubalbums() ||
					(
						! self.numsMedia.imagesAudiosVideosTotal() || ! self.hasPositionsInMedia()
					) && ! numSubalbums
				) {
					resolve_generatePositionsAndMediaInMediaAndSubalbums();
				} else {
					let hasPositionsInMedia = (self.hasPositionsInMedia() && (! self.isTransversal() || numSubalbums === 0));
					if (! hasPositionsInMedia) {
						// no media or date/gps album with subalbums
						self.positionsAndMediaInMedia = new PositionsAndMedia([]);
						self.numPositionsInMedia = 0;
						self.positionsAndMediaInSubalbums = new PositionsAndMedia(self.positionsAndMediaInTree);
						self.numPositionsInSubalbums = self.positionsAndMediaInSubalbums.length;
						resolve_generatePositionsAndMediaInMediaAndSubalbums();
					} else if (numSubalbums === 0) {
						// no subalbums
						self.generatePositionsAndMediaInMedia();
						self.numPositionsInMedia = self.positionsAndMediaInMedia.length;
						self.positionsAndMediaInSubalbums = new PositionsAndMedia([]);
						self.numPositionsInSubalbums = 0;
						resolve_generatePositionsAndMediaInMediaAndSubalbums();
					} else {
						// we have media and subalbum, but we don't know if positionsAndMediaInTree property is there
						let promise = phFl.getAlbum(self, null, {getMedia: true, getPositions: ! env.options.save_data});
						promise.then(
							function(album) {
								self = album;
								self.generatePositionsAndMediaInMedia();
								self.numPositionsInMedia = self.positionsAndMediaInMedia.length;
								self.positionsAndMediaInSubalbums = new PositionsAndMedia(self.positionsAndMediaInTree);
								self.positionsAndMediaInSubalbums.removePositionsAndMedia(self.positionsAndMediaInMedia);
								self.numPositionsInSubalbums = self.positionsAndMediaInSubalbums.length;
								resolve_generatePositionsAndMediaInMediaAndSubalbums();
							}
						);
					}
				}
			}
		);
	};

	Album.prototype.setSubalbumsSortDataAndAdaptCaption = function() {
		if (this.visibleSubalbums().length) {
			const selectors = ".album-sort-data";
			let promises = [];
			if (env.albumSort === "name" && env.options.show_album_names_below_thumbs) {
				$(selectors).hide().html("");
			} else {
				$(selectors).show().each(
					function() {
						const selector = this;
						// const album = env.cache.getAlbum($(this).parent().parent().parent().attr("id"));
						const promise = phFl.getAlbum(
							$(selector).parent().parent().parent().attr("id"),
							null,
							{getMedia: true, getPositions: ! env.options.save_data}
						);
						promise.then(
							function(album) {
								let sortData = "";

								if (env.albumSort === "exifDateMax")
									sortData = album.exifDateMax;
								if (env.albumSort === "exifDateMin")
									sortData = album.exifDateMin;
								else if (env.albumSort === "fileDateMax")
									sortData = album.fileDateMax;
								else if (env.albumSort === "fileDateMin")
									sortData = album.fileDateMin;
								else if (env.albumSort === "name")
									sortData = album.name;
								else if (env.albumSort === "pixelSizeMax")
									sortData = album.pixelSizeMax;
								else if (env.albumSort === "pixelSizeMin")
									sortData = album.pixelSizeMin;
								else if (env.albumSort === "fileSize")
									sortData = util.humanFileSize(album.sizesOfSubTree.original.imagesAudiosVideosTotal());

								if (sortData)
									$(selector).html("["  + sortData + "]");
								else
									$(selector).html("&nbsp;");
							},
							function() {
								console.trace();
							}
						);
						promises.push(promise);
					}
				);
			}
			Promise.all(promises).then(
				function() {
					$.executeAfterEvent(
						"otherJsFilesLoadedEvent",
						function() {
							let thereWasAVerticalScrollBar = util.thereIsAVerticalScrollBar();
							util.adaptSubalbumCaptionHeight(); // in setSubalbumsSortDataAndAdaptCaption

							if (! thereWasAVerticalScrollBar && util.thereIsAVerticalScrollBar()) {
								// adapting media height have made the scrollbar appear, possibly messing the subalbums -> re-adapt
								util.adaptMediaCaptionHeight(); // in setSubalbumsSortDataAndAdaptCaption
							}
						}
					);
				}
			);
		}
	};

	Album.prototype.setMediaSortDataAndAdaptCaption = function(inPopup = false) {
		if (this.numVisibleMedia()) {
			let selectors = "#thumbs .media-sort-data";
			let albumToUse = env.currentAlbum;
			if (inPopup) {
				selectors = "#popup-images-wrapper .media-sort-data";
				albumToUse = env.mapAlbum;
			}

			if (albumToUse.isAlbumWithOneMedia())
				return;

			if (env.mediaSort === "name" && env.options.show_media_names_below_thumbs) {
				$(selectors).hide().html("");
			} else {
				$(selectors).show().each(
					function() {
						const selector = this;
						let complexCacheBase = $(selector).parent().parent().parent().parent().attr("id");
						if (inPopup)
							complexCacheBase = $(selector).parent().parent().parent().attr("id");
						const cacheBase = complexCacheBase.substr(complexCacheBase.indexOf("--") + 2);
						const singleMedia = albumToUse.media.find(element => element.cacheBase === cacheBase);
						let sortData = "";

						if (env.mediaSort === "exifDate")
							sortData = singleMedia.exifDate;
						else if (env.mediaSort === "fileDate")
							sortData = singleMedia.fileDate;
						else if (env.mediaSort === "name")
							sortData = singleMedia.name;
						else if (env.mediaSort === "pixelSize")
							sortData = singleMedia.isAudio() ? 0 : Math.max(... singleMedia.metadata.size);
						else if (env.mediaSort === "fileSize")
							sortData = util.humanFileSize(singleMedia.fileSizes.original.imagesAudiosVideosTotal());

							if (sortData)
								$(selector).html("["  + sortData + "]");
							else
								$(selector).html("&nbsp;");
					}
				);
			}

			if (! env.options.show_media_names_below_thumbs && env.mediaSort !== "name")
				util.showMediaNameInAudio();
			else
				util.hideMediaNameInAudio();

			$.executeAfterEvent(
				"cssFontsLoadedEvent",
				function adaptAndUpdate() {
					let thereWasAVerticalScrollBar;
					if (! inPopup)
					// 	env.mapAlbum.updatePopup();
					// else
						thereWasAVerticalScrollBar = util.thereIsAVerticalScrollBar();

					util.adaptMediaCaptionHeight(inPopup);

					if (! inPopup) {
						if (! thereWasAVerticalScrollBar && util.thereIsAVerticalScrollBar()) {
							// adapting media height have made the scrollbar appear, possibly messing the subalbums -> re-adapt
							util.adaptSubalbumCaptionHeight(); // in setMediaSortDataAndAdaptCaption
						}
					}
				}
			);
		}
	};

	Album.prototype.sortAlbumsMedia = function() {
		// this function applies the sorting on the media and subalbum lists
		// and sets the album properties that attest the lists status

		// album properties reflect the current sorting of album and media objects
		// json files have subalbums and media sorted by date not reversed

		if (this.subalbums.length) {
			if (this.needAlbumSort(env.albumSort)) {
				if (env.albumSort === "name")
					this.sortByPath();
				else if (env.albumSort === "exifDateMax")
					util.sortByExifDateMax(this.subalbums);
				else if (env.albumSort === "exifDateMin")
					util.sortByExifDateMin(this.subalbums);
				else if (env.albumSort === "fileDateMax")
					util.sortByFileDateMax(this.subalbums);
				else if (env.albumSort === "fileDateMin")
					util.sortByFileDateMin(this.subalbums);
				else if (env.albumSort === "pixelSizeMax")
					util.sortByPixelSizeMax(this.subalbums);
				else if (env.albumSort === "pixelSizeMin")
					util.sortByPixelSizeMin(this.subalbums);
				else if (env.albumSort === "fileSize")
					util.sortByFileSize(this.subalbums);
				this.albumSort = env.albumSort;
				this.albumReverseSort = false;
			}
			if (
				this.needAlbumReverseSort("exifDateMax") ||
				this.needAlbumReverseSort("exifDateMin") ||
				this.needAlbumReverseSort("fileDateMax") ||
				this.needAlbumReverseSort("fileDateMin") ||
				this.needAlbumReverseSort("name") ||
				this.needAlbumReverseSort("pixelSizeMax") ||
				this.needAlbumReverseSort("pixelSizeMin") ||
				this.needAlbumReverseSort("fileSize")
			) {
				this.subalbums.reverse();
				this.albumReverseSort = ! this.albumReverseSort;
			}
		}

		if (this.hasOwnProperty("media") && this.media.length) {
			if (this.needMediaSort(env.mediaSort)) {
				if (env.mediaSort === "name")
					this.media.sortByName();
				else if (env.mediaSort === "exifDate")
					this.media.sortByExifDate();
				else if (env.mediaSort === "fileDate")
					this.media.sortByFileDate();
				else if (env.mediaSort === "pixelSize")
					this.media.sortByPixelSize();
				else if (env.mediaSort === "fileSize")
					this.media.sortByFileSize();
				this.mediaSort = env.mediaSort;
				this.mediaReverseSort = false;
			}
			if (
				this.needMediaReverseSort("exifDate") ||
				this.needMediaReverseSort("fileDate") ||
				this.needMediaReverseSort("name") ||
				this.needMediaReverseSort("pixelSize") ||
				this.needMediaReverseSort("fileSize")
			) {
				this.media.sortReverse();
				this.mediaReverseSort = ! this.mediaReverseSort;
			}

			// calculate the new index
			if (env.currentMedia !== null && (env.currentAlbum === null || this.isEqual(env.currentAlbum))) {
				env.currentMediaIndex = this.media.findIndex(
					function(thisMedia) {
						var matches = thisMedia.isEqual(env.currentMedia);
						return matches;
					}
				);
			}
		}
	};

	Album.prototype.initializeIncludedFilesByCodesSimpleCombinationProperty = function(codesSimpleCombination, number) {
		if (typeof codesSimpleCombination !== "undefined") {
			if (! this.includedFilesByCodesSimpleCombination.hasOwnProperty(codesSimpleCombination))
				this.includedFilesByCodesSimpleCombination[codesSimpleCombination] = {};
			if (typeof number !== "undefined" && codesSimpleCombination !== ",") {
				if (! this.includedFilesByCodesSimpleCombination[codesSimpleCombination].hasOwnProperty(number)) {
					this.includedFilesByCodesSimpleCombination[codesSimpleCombination][number] = {};
					this.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album = {};
				}
			}
		}
	};

	Album.prototype.addContentWithExternalMediaAndPositionsFromProtectedCacheBase = function(protectedCacheBase, {getMedia, getPositions}) {
		// this function gets a single protected json file

		var self = this;
		var splittedProtectedCacheBase = protectedCacheBase.split(".");
		var number = parseInt(splittedProtectedCacheBase.at(-1));
		var codesSimpleCombination = util.convertProtectedCacheBaseToCodesSimpleCombination(protectedCacheBase);
		self.initializeIncludedFilesByCodesSimpleCombinationProperty(codesSimpleCombination, number);

		return new Promise(
			function(resolve_getSingleProtectedCacheBase, reject_getSingleProtectedCacheBase) {
				// let's check whether the protected cache base has been already loaded
				if (self.includedFilesByCodesSimpleCombination[codesSimpleCombination] === false) {
				// if (self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number] === false) {
					// the protected cache base doesn't exist
					reject_getSingleProtectedCacheBase();
				} else if (self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.hasOwnProperty("protectedAlbumGot")) {
					// the protected cache base has been already fetched
					// possibly the external media and positions are still missing
					let addMediaAndPositionsPromise = addExternalMediaAndPositionsFromProtectedAlbum();
					addMediaAndPositionsPromise.then(
						function externalMediaAndPositionsAdded() {
							resolve_getSingleProtectedCacheBase();
						}
					);
				} else {
					// the protected cache base hasn't been fetched yet
					var protectedJsonFile = protectedCacheBase + ".json";

					var promise = phFl.getJsonFile(protectedJsonFile);
					promise.then(
						function protectedFileExists(object) {
							var protectedAlbum = new Album(object);
							let protectedDirectory = protectedCacheBase.split("/")[0];
							if (! self.hasOwnProperty("numsProtectedMediaInSubTree") || self.empty)
								// this is needed when addContentWithExternalMediaAndPositionsFromProtectedCacheBase() is called by getNumsProtectedMediaInSubTreeProperty()
								self.numsProtectedMediaInSubTree = protectedAlbum.numsProtectedMediaInSubTree;

							if (! self.hasOwnProperty("name"))
								self.name = protectedAlbum.name;
							if (! self.hasOwnProperty("altName") && protectedAlbum.hasOwnProperty("altName"))
								self.altName = protectedAlbum.altName;
							if (! self.hasOwnProperty("ancestorsNames") && protectedAlbum.hasOwnProperty("ancestorsNames"))
								self.ancestorsNames = protectedAlbum.ancestorsNames;
							if (! self.hasOwnProperty("ancestorsTitles") && protectedAlbum.hasOwnProperty("ancestorsTitles"))
								self.ancestorsTitles = protectedAlbum.ancestorsTitles;
							if (! self.hasOwnProperty("ancestorsCenters") && protectedAlbum.hasOwnProperty("ancestorsCenters"))
								self.ancestorsCenters = protectedAlbum.ancestorsCenters;
							if (! self.hasOwnProperty("title") && protectedAlbum.hasOwnProperty("title"))
								self.title = protectedAlbum.title;
							if (! self.hasOwnProperty("description") && protectedAlbum.hasOwnProperty("description"))
								self.description = protectedAlbum.description;
							if (! self.hasOwnProperty("tags") && protectedAlbum.hasOwnProperty("tags"))
								self.tags = protectedAlbum.tags;

							self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].codesComplexCombination = protectedAlbum.codesComplexCombination;

							if (! protectedAlbum.hasOwnProperty("numsMedia"))
								protectedAlbum.numsMedia = protectedAlbum.media.imagesAudiosVideosCount();

							if (! self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.hasOwnProperty("protectedAlbumGot")) {
								if (protectedAlbum.subalbums.length) {
									self.mergeProtectedSubalbums(protectedAlbum, protectedDirectory, codesSimpleCombination, number);
								}

								self.numsMedia.sum(protectedAlbum.numsMedia);
								if (! self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.hasOwnProperty("countsGot")) {
									self.numsMediaInSubTree.sum(protectedAlbum.numsMediaInSubTree);
									self.sizesOfSubTree.sum(protectedAlbum.sizesOfSubTree);
									self.sizesOfAlbum.sum(protectedAlbum.sizesOfAlbum);

									self.numPositionsInTree += protectedAlbum.numPositionsInTree;
									if (env.options.expose_image_positions) {
										self.nonGeotagged.numsMedia.sum(protectedAlbum.nonGeotagged.numsMedia);
										self.nonGeotagged.numsMediaInSubTree.sum(protectedAlbum.nonGeotagged.numsMediaInSubTree);
										self.nonGeotagged.sizesOfSubTree.sum(protectedAlbum.nonGeotagged.sizesOfSubTree);
										self.nonGeotagged.sizesOfAlbum.sum(protectedAlbum.nonGeotagged.sizesOfAlbum);
									}
								}
								if (! self.hasOwnProperty("path"))
									self.path = protectedAlbum.path;
								self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.protectedAlbumGot = true;

								if (protectedAlbum.hasOwnProperty("media")) {
									protectedAlbum.media.forEach(
										singleMedia => {
											singleMedia.protected = true;
											if (! singleMedia.cacheSubdir.startsWith(env.options.protected_directories_prefix))
												singleMedia.cacheSubdir = [protectedDirectory, singleMedia.cacheSubdir].join("/");
										}
									);
									if (! self.hasOwnProperty("media"))
										self.media = protectedAlbum.media;
									else
										protectedAlbum.media.forEach(
											singleProtectedMedia => {
												if (
													self.media.every(
														singleMedia => ! singleProtectedMedia.isEqual(singleMedia)
													)
												) {
													self.media.push(singleProtectedMedia);
												}
											}
										);
									self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.mediaGot = true;
								}

								if (protectedAlbum.hasOwnProperty("positionsAndMediaInTree")) {
									if (! self.hasOwnProperty("positionsAndMediaInTree"))
										self.positionsAndMediaInTree = protectedAlbum.positionsAndMediaInTree;
									else
										self.positionsAndMediaInTree.mergePositionsAndMedia(protectedAlbum.positionsAndMediaInTree);
									self.numPositionsInTree = self.positionsAndMediaInTree.length;
									self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.positionsGot = true;
								}
								self.invalidatePositionsAndMediaInAlbumAndSubalbums();
								self.generatePositionsAndMediaInMediaAndSubalbums();

								if (protectedAlbum.hasOwnProperty("exifDateMax")) {
									if (! self.hasOwnProperty("exifDateMax") || protectedAlbum.exifDateMax > self.exifDateMax)
										self.exifDateMax = protectedAlbum.exifDateMax;
								}
								if (protectedAlbum.hasOwnProperty("exifDateMin")) {
									if (! self.hasOwnProperty("exifDateMin") || protectedAlbum.exifDateMin < self.exifDateMin)
										self.exifDateMin = protectedAlbum.exifDateMin;
								}

								// add pointers to the same object for the symlinks
								for (let iSymlink = 0; iSymlink < protectedAlbum.symlinkCodesAndNumbers.length; iSymlink ++) {
									let symlinkCodesAndNumbersItem = protectedAlbum.symlinkCodesAndNumbers[iSymlink];
									if (
										! self.includedFilesByCodesSimpleCombination.hasOwnProperty(symlinkCodesAndNumbersItem.codesSimpleCombination) ||
										! Object.keys(self.includedFilesByCodesSimpleCombination[symlinkCodesAndNumbersItem.codesSimpleCombination]).some(
											function(thisNumber) {
												thisNumber = parseInt(thisNumber);
												var result =
													self.includedFilesByCodesSimpleCombination[symlinkCodesAndNumbersItem.codesSimpleCombination][thisNumber].codesComplexCombination ==
														symlinkCodesAndNumbersItem.codesComplexCombination &&
													self.includedFilesByCodesSimpleCombination[symlinkCodesAndNumbersItem.codesSimpleCombination][thisNumber].hasOwnProperty("protectedAlbumGot");
												return result;
											}
										)
									) {
										// actually add the pointer
										if (! self.includedFilesByCodesSimpleCombination.hasOwnProperty(symlinkCodesAndNumbersItem.codesSimpleCombination))
											self.includedFilesByCodesSimpleCombination[symlinkCodesAndNumbersItem.codesSimpleCombination] = {};
										if (! self.includedFilesByCodesSimpleCombination[symlinkCodesAndNumbersItem.codesSimpleCombination].hasOwnProperty(symlinkCodesAndNumbersItem.number))
											self.includedFilesByCodesSimpleCombination[symlinkCodesAndNumbersItem.codesSimpleCombination][symlinkCodesAndNumbersItem.number] = {};
										self.includedFilesByCodesSimpleCombination[symlinkCodesAndNumbersItem.codesSimpleCombination][symlinkCodesAndNumbersItem.number].album =
											self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album;
										self.includedFilesByCodesSimpleCombination[symlinkCodesAndNumbersItem.codesSimpleCombination][symlinkCodesAndNumbersItem.number].codesComplexCombination =
											symlinkCodesAndNumbersItem.codesComplexCombination;
									}
								}
							}
							let addMediaAndPositionsPromise = addExternalMediaAndPositionsFromProtectedAlbum();
							addMediaAndPositionsPromise.then(
								function externalMediaAndPositionsAdded() {
									resolve_getSingleProtectedCacheBase();
								},
								function() {
									console.trace();
								}
							);
						},
						function protectedFileDoesntExist() {
							if (codesSimpleCombination !== null)
								// save the info that the protected cache base doesn't exist
								self.includedFilesByCodesSimpleCombination[codesSimpleCombination] = false;

							// do not do anything, i.e. another protected cache base will be processed
							reject_getSingleProtectedCacheBase();
						}
					);
				}
				// end of addContentWithExternalMediaAndPositionsFromProtectedCacheBase function body

				function addExternalMediaAndPositionsFromProtectedAlbum() {
					return new Promise(
						function(resolve_addExternalMediaAndPositionsFromProtectedAlbum) {
							var mustGetMedia = (
								getMedia &&
								! self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.hasOwnProperty("mediaGot") &&
								! self.isABigTransversalAlbum()
							);
							var mustGetPositions = getPositions && ! self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.hasOwnProperty("positionsGot");
							if (! mustGetMedia && ! mustGetPositions) {
								resolve_addExternalMediaAndPositionsFromProtectedAlbum();
							} else {
								var promise = phFl.getMediaAndPositions(protectedCacheBase, {mustGetMedia: mustGetMedia, mustGetPositions: mustGetPositions});
								promise.then(
									function([mediaGot, positionsGot]) {
										if (mediaGot) {
											mediaGot.forEach(singleMedia => {singleMedia.protected = true;});
											if (! self.hasOwnProperty("media"))
												self.media = mediaGot;
											else
												self.media = self.media.concat(mediaGot);
											// self.includedFilesByCodesSimpleCombination[","].mediaGot = true;
											self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.mediaGot = true;
										}

										if (positionsGot) {
											if (! self.hasOwnProperty("positionsAndMediaInTree") || ! self.positionsAndMediaInTree.length)
												self.positionsAndMediaInTree = positionsGot;
											else
												self.positionsAndMediaInTree.mergePositionsAndMedia(positionsGot);
											self.numPositionsInTree = self.positionsAndMediaInTree.length;
											// self.includedFilesByCodesSimpleCombination[","].positionsGot = true;
											self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.positionsGot = true;
										}

										resolve_addExternalMediaAndPositionsFromProtectedAlbum();
									},
									function() {
										console.trace();
									}
								);
							}
						}
					);
				}
			}
		);
	};

	Album.prototype.codesSimpleCombinationsToGet = function(getMedia, getPositions) {
		var iAlbumPassword, iMediaPassword, albumGuessedPassword, mediaGuessedPassword;
		var albumCode, mediaCode;
		var codesComplexCombinationInAlbum;

		var result = [];
		for (iAlbumPassword = 0; iAlbumPassword <= env.guessedPasswordsMd5.length; iAlbumPassword ++) {
			if (iAlbumPassword === env.guessedPasswordsMd5.length) {
				albumCode = "";
			} else {
				albumGuessedPassword = env.guessedPasswordsMd5[iAlbumPassword];
				albumCode = util.convertMd5ToCode(albumGuessedPassword);
			}
			for (iMediaPassword = 0; iMediaPassword <= env.guessedPasswordsMd5.length; iMediaPassword ++) {
				if (iMediaPassword === env.guessedPasswordsMd5.length) {
					mediaCode = "";
				} else {
					mediaGuessedPassword = env.guessedPasswordsMd5[iMediaPassword];
					mediaCode = util.convertMd5ToCode(mediaGuessedPassword);
				}
				if (! albumCode && ! mediaCode)
					continue;
				let codesSimpleCombination = albumCode + "," + mediaCode;

				for (codesComplexCombinationInAlbum in this.numsProtectedMediaInSubTree) {
					if (this.numsProtectedMediaInSubTree.hasOwnProperty(codesComplexCombinationInAlbum) && codesComplexCombinationInAlbum != ",") {
						let [albumCodesComplexCombinationList, mediaCodesComplexCombinationList] = phFl.convertComplexCombinationsIntoLists(codesComplexCombinationInAlbum);

						if (
							(
								! albumCodesComplexCombinationList.length &&
								! albumCode &&
								mediaCodesComplexCombinationList.length &&
								mediaCode &&
								mediaCodesComplexCombinationList.indexOf(mediaCode) != -1
							) || (
								albumCodesComplexCombinationList.length &&
								albumCode &&
								albumCodesComplexCombinationList.indexOf(albumCode) != -1 &&
								! mediaCodesComplexCombinationList.length &&
								! mediaCode
							) || (
								albumCodesComplexCombinationList.length &&
								albumCode &&
								albumCodesComplexCombinationList.indexOf(albumCode) != -1 &&
								mediaCodesComplexCombinationList.length &&
								mediaCode &&
								mediaCodesComplexCombinationList.indexOf(mediaCode) != -1
							)
						) {
							let numProtectedCacheBases = this.getNumProtectedCacheBases(codesComplexCombinationInAlbum);
							if (
								! (codesSimpleCombination in this.includedFilesByCodesSimpleCombination) ||
								Object.values(this.includedFilesByCodesSimpleCombination[codesSimpleCombination]).filter(
									objectWithNumberKey => objectWithNumberKey.album === {countsGot: true}
								).length < numProtectedCacheBases ||
								getMedia && Object.keys(this.includedFilesByCodesSimpleCombination[codesSimpleCombination]).some(
									number => ! this.includedFilesByCodesSimpleCombination[codesSimpleCombination][parseInt(number)].this.hasOwnProperty("mediaGot")
								) ||
								getPositions && Object.keys(this.includedFilesByCodesSimpleCombination[codesSimpleCombination]).some(
									number => ! this.includedFilesByCodesSimpleCombination[codesSimpleCombination][parseInt(number)].this.hasOwnProperty("positionsGot")
								)
							) {
								if (! result.includes(codesSimpleCombination))
									result.push(codesSimpleCombination);
							}
						}
					}
				}
			}
		}
		return result;
	};

	Album.prototype.getNumsProtectedMediaInSubTreeProperty = function() {
		function getNextProtectedDirectory() {
			return new Promise(
				function(resolve_getNextProtectedDirectory, reject_getNextProtectedDirectory) {
					iDirectory ++;
					if (iDirectory >= theProtectedDirectoriesToGet.length) {
						reject_getNextProtectedDirectory();
					} else {
						// since numsProtectedMediaInSubTree isn't in the album,
						// there is no way to know if a protected directory will have the searched content:
						// so we must try until a protected directory has the protected album we need

						let protectedDirectory = theProtectedDirectoriesToGet[iDirectory];
						let protectedCacheBase = util.pathJoin([protectedDirectory, util.addBySomethingSubdir(self.cacheBase) + ".0"]);

						let promise = self.addContentWithExternalMediaAndPositionsFromProtectedCacheBase(protectedCacheBase, {getMedia: false, getPositions: false});
						promise.then(
							function addContentWithExternalMediaAndPositionsFromProtectedCacheBase_resolved() {
								// ok, we got what we were looking for: numsProtectedMediaInSubTree property has been added by addContentWithExternalMediaAndPositionsFromProtectedCacheBase()

								delete self.mediaSort;
								delete self.mediaReverseSort;
								delete self.albumSort;
								delete self.albumReverseSort;
								self.sortAlbumsMedia();

								resolve_getNextProtectedDirectory();
							},
							function addContentWithExternalMediaAndPositionsFromProtectedCacheBase_rejected() {
								var promise = getNextProtectedDirectory();
								promise.then(
									function() {
										resolve_getNextProtectedDirectory();
									},
									function() {
										reject_getNextProtectedDirectory();
									}
								);
							}
						);
					}
				}
			);
		}
		// end of getNextProtectedDirectory function

		var iDirectory = -1;
		var self = this;
		var theProtectedDirectoriesToGet;
		return new Promise(
			function(resolve_getNumsProtectedMediaInSubTreeProperty, reject_getNumsProtectedMediaInSubTreeProperty) {
				theProtectedDirectoriesToGet = phFl.protectedDirectoriesToGet();
				if (! theProtectedDirectoriesToGet.length) {
					reject_getNumsProtectedMediaInSubTreeProperty();
				} else {
					if (self.hasOwnProperty("numsProtectedMediaInSubTree") && ! self.empty) {
						resolve_getNumsProtectedMediaInSubTreeProperty();
					} else {
						var getNumsProtectedMediaInSubTreePropertyPromise = getNextProtectedDirectory();
						getNumsProtectedMediaInSubTreePropertyPromise.then(
							resolve_getNumsProtectedMediaInSubTreeProperty,
							reject_getNumsProtectedMediaInSubTreeProperty
						);
					}
				}
			}
		);
	};

	Album.prototype.mergeProtectedSubalbums = function(protectedAlbum, protectedDirectory, codesSimpleCombination, number) {
		var cacheBases = [];
		this.subalbums.forEach(
			function(subalbum) {
				cacheBases.push(subalbum.cacheBase);
			}
		);
		var self = this;
		// for (i = 0; i < protectedAlbum.subalbums.length; i ++) {
		protectedAlbum.subalbums.forEach(
			function(ithProtectedSubalbum) {
				if (cacheBases.indexOf(ithProtectedSubalbum.cacheBase) === -1) {
					ithProtectedSubalbum.randomMedia.forEach(
						randomSingleMedia => {
							if (! randomSingleMedia.cacheSubdir.startsWith(env.options.protected_directories_prefix))
								randomSingleMedia.cacheSubdir = [protectedDirectory, randomSingleMedia.cacheSubdir].join("/");
						}
					);
					self.subalbums.push(ithProtectedSubalbum);

					if (ithProtectedSubalbum instanceof Album) {
						ithProtectedSubalbum.initializeIncludedFilesByCodesSimpleCombinationProperty(codesSimpleCombination, number);
						ithProtectedSubalbum.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.countsGot = true;
					}
				} else {
					self.subalbums.forEach(
						function(subalbum) {
							if (subalbum.isEqual(ithProtectedSubalbum))
								phFl.mergeProtectedSubalbum(subalbum, ithProtectedSubalbum, protectedDirectory, codesSimpleCombination, number);
						}
					);
				}
			}
		);
	};

	Album.prototype.hasUnloadedProtectedContent = function(mustGetMedia, mustGetPositions) {
		// this function is for detecting if protected content has to be loaded
		// to detect if the auth dialog has to be shown use hasVeiledProtectedContent() instead
		if (this.isVirtual())
			return false;
		if (! env.guessedPasswordCodes.length)
			return false;
		var self = this;
		var guessedSimpleCombinationFromNumsProtected = this.guessedCodesSimpleCombinations();
		var guessedSimpleCombinationFromIncludedFiles = Object.keys(self.includedFilesByCodesSimpleCombination);
		var indexOfComma = guessedSimpleCombinationFromIncludedFiles.indexOf(",");
		if (indexOfComma !== -1)
			guessedSimpleCombinationFromIncludedFiles.splice(indexOfComma, 1);

		var intersection = util.arrayIntersect(guessedSimpleCombinationFromNumsProtected, guessedSimpleCombinationFromIncludedFiles);

		var codesComplexcombinationsGot = [];
		Object.keys(self.includedFilesByCodesSimpleCombination).forEach(
			codesSimpleCombination => {
				if (codesSimpleCombination !== ",") {
					Object.keys(self.includedFilesByCodesSimpleCombination[codesSimpleCombination]).forEach(
						number => {
							let codesComplexCombination = self.includedFilesByCodesSimpleCombination[
								codesSimpleCombination
							][number].codesComplexCombination;
							if (! codesComplexcombinationsGot.includes(codesComplexCombination))
								codesComplexcombinationsGot.push(codesComplexCombination);
						}
					);
				}
			}
		);
		var allCodesSimpleCombinations = Object.keys(self.numsProtectedMediaInSubTree);
		var codesComplexCombinationsNotGot = [];
		for (const element of allCodesSimpleCombinations) {
			if (! codesComplexcombinationsGot.includes(element)) {
				codesComplexCombinationsNotGot.push(element);
			}
		}

		if (intersection.length < guessedSimpleCombinationFromNumsProtected.length) {
			return true;
		} else if (
			intersection.some(
				codesSimpleCombination => codesSimpleCombination.split(",").some(
					code => codesComplexCombinationsNotGot.some(
						codesComplexCombination => codesComplexCombination.includes(code)
					)
				)
			)
		) {
			return true;
		} else if (
			intersection.every(
				codesSimpleCombination => Object.keys(self.includedFilesByCodesSimpleCombination[codesSimpleCombination]).every(
						number =>
							self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.hasOwnProperty("protectedAlbumGot") &&
							(! mustGetMedia || self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.hasOwnProperty("mediaGot")) &&
							(! mustGetPositions || self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.hasOwnProperty("positionsGot"))
				)
			)
		) {
			return false;
		} else {
			return true;
		}
	};


	Album.prototype.addProtectedContent = function(getMedia, getPositions, numsProtectedMediaInSubTree) {
		// this function adds the protected content to the given album
		var self = this;

		return new Promise(
			function(resolve_addProtectedContent, reject_addProtectedContent) {
				if (self.isVirtual()) {
					// map and selection albums do not admit adding protected content
					resolve_addProtectedContent();
				} else {
					var numsPromise;
					if (self.isEmpty() && typeof numsProtectedMediaInSubTree !== "undefined") {
						self.numsProtectedMediaInSubTree = numsProtectedMediaInSubTree;
					}
					// if (self.hasOwnProperty("numsProtectedMediaInSubTree")) {
					if (true || self.hasOwnProperty("numsProtectedMediaInSubTree") && ! self.isEmpty()) {
						numsPromise = continueAddProtectedContent();
						numsPromise.then(
							function() {
								self.invalidatePositionsAndMediaInAlbumAndSubalbums();
								resolve_addProtectedContent();
							},
							function() {
								console.trace();
							}
						);
					} else {
						// the album hasn't unprotected content and no protected cache base has been processed yet:
						// a protected album must be loaded in order to know the complex combinations

						var promise = self.getNumsProtectedMediaInSubTreeProperty();
						promise.then(
							function() {
								self.empty = false;
								numsPromise = continueAddProtectedContent();
								numsPromise.then(
									function() {
										self.invalidatePositionsAndMediaInAlbumAndSubalbums();
										resolve_addProtectedContent();
									},
									function() {
										console.trace();
									}
								);
							},
							function() {
								// numsProtectedMediaInSubTree couldn't be retrieved because no protected album was found
								reject_addProtectedContent();
							}
						);
						// }
					}
				}
			}
		);
		// end of function code


		function continueAddProtectedContent() {
			// this function keeps on the job of addProtectedContent
			// it is called when the numsProtectedMediaInSubTree property is in the album

			return new Promise(
				function(resolve_continueAddProtectedContent) {
					var theCodesSimpleCombinationsToGet = self.codesSimpleCombinationsToGet(getMedia, getPositions);
					if (! theCodesSimpleCombinationsToGet.length) {
						if (! env.cache.getAlbum(self.cacheBase))
							env.cache.putAlbum(self);
						resolve_continueAddProtectedContent();
					} else {
						// prepare and get the protected content from the protected directories
						let protectedPromises = [];
						// let codesSimpleCombinationGot = [];

						// loop on the simple combinations, i.e. on the protected directories
						for (let iSimple = 0; iSimple < theCodesSimpleCombinationsToGet.length; iSimple ++) {
							let codesSimpleCombination = theCodesSimpleCombinationsToGet[iSimple];
							// let codesCombinationsLists = phFl.convertComplexCombinationsIntoLists(codesSimpleCombination);
							let [albumMd5, mediaMd5] = util.convertCodesListToMd5sList(codesSimpleCombination.split(","));
							// codesSimpleCombinationGot.push(codesSimpleCombination);

							let protectedDirectory = env.options.protected_directories_prefix;
							if (albumMd5)
								protectedDirectory += albumMd5;
							protectedDirectory += ",";
							if (mediaMd5)
								protectedDirectory += mediaMd5;

							// if (! self.includedFilesByCodesSimpleCombination.hasOwnProperty(codesSimpleCombination) || ! self.includedFilesByCodesSimpleCombination[codesSimpleCombination]){
							// 	// TO DO: check if it's safe to do this when self.includedFilesByCodesSimpleCombination[codesSimpleCombination] === false
							// 	self.includedFilesByCodesSimpleCombination[codesSimpleCombination] = {};
							// }

							// we can know how many files/symlinks we have to get in the protected directory
							let numProtectedCacheBases = self.getNumProtectedCacheBases(codesSimpleCombination);
							for (let iCacheBase = 0; iCacheBase < numProtectedCacheBases; iCacheBase ++) {
								let number = iCacheBase;
								let protectedCacheBase = util.pathJoin([protectedDirectory, util.addBySomethingSubdir(self.cacheBase) + "." + iCacheBase]);
								self.initializeIncludedFilesByCodesSimpleCombinationProperty(codesSimpleCombination, number);

								let ithPromise = new Promise(
									function(resolve_ithPromise, reject) {
										if (
											self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.hasOwnProperty("protectedAlbumGot") &&
											(! getMedia || self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.hasOwnProperty("mediaGot")) &&
											(! getPositions || self.includedFilesByCodesSimpleCombination[codesSimpleCombination][number].album.hasOwnProperty("positionsGot"))
										) {
											// this cache base has been already loaded and either media/positions are already there or aren't needed now
											resolve_ithPromise();
										} else {
											let promise = self.addContentWithExternalMediaAndPositionsFromProtectedCacheBase(protectedCacheBase, {getMedia: getMedia, getPositions: getPositions});
											promise.then(
												function() {
													if (self.isEmpty())
														self.empty = false;
													resolve_ithPromise();
												},
												function() {
													// the protected cache base doesn't exist, keep on!
													// execution shouldn't arrive here
													resolve_ithPromise();
												}
											);
										}
									}
								);
								protectedPromises.push(ithPromise);
							}
						}

						Promise.all(protectedPromises).then(
							function() {
								// execution arrives here when all the protected json has been loaded and processed

								delete self.mediaSort;
								delete self.mediaReverseSort;
								delete self.albumSort;
								delete self.albumReverseSort;
								self.sortAlbumsMedia();

								resolve_continueAddProtectedContent();
							}
						);
					}
				}
			);
		}
	};

	// Album.prototype.decodeSearchCacheBase = function(albumCacheBase) {
	// 	var components = PhotoFloat.decodeSearchCacheBase(albumCacheBase);
	//
	// 	this.search = {};
	// 	this.search.insideWords = components.search_inside_words;
	// 	this.search.anyWord = components.search_any_word;
	// 	this.search.caseSensitive = components.search_case_sensitive;
	// 	this.search.accentSensitive = components.search_accent_sensitive;
	// 	this.search.numbers = components.search_numbers;
	// 	this.search.tagsOnly = components.search_tags_only;
	// 	this.search.currentAlbumOnly = components.search_current_album;
	// 	this.search.searchedCacheBase = components.searchedCacheBase;
	// 	this.search.words = components.searchWords;
	// 	this.search.wordsFromUser = components.searchWordsFromUser;
	// 	this.search.wordsFromUserNormalized = components.searchWordsFromUserNormalized;
	// 	this.search.wordsFromUserNormalizedAccordingToOptions = components.searchWordsFromUserNormalizedAccordingToOptions;
	// };

	Album.prototype.removeStopWordsFromSearchWords = function() {
		// remove the stop words from the search words lists

		if (! this.hasOwnProperty("search"))
			return;

		this.search.removedStopWords = [];
		var searchWordsFromUserWithoutStopWords = [];
		var searchWordsFromUserWithoutStopWordsNormalized = [];
		var searchWordsFromUserWithoutStopWordsNormalizedAccordingToOptions = [];
		if (! env.cache.stopWords.length) {
			searchWordsFromUserWithoutStopWords = this.search.wordsFromUser;
			searchWordsFromUserWithoutStopWordsNormalized = this.search.wordsFromUserNormalized;
			searchWordsFromUserWithoutStopWordsNormalizedAccordingToOptions = this.search.wordsFromUserNormalizedAccordingToOptions;
		} else {
			for (var i = 0; i < this.search.wordsFromUser.length; i ++) {
				if (env.cache.stopWords.every(stopWord => stopWord !== this.search.wordsFromUserNormalized[i])) {
					searchWordsFromUserWithoutStopWords.push(this.search.wordsFromUser[i]);
					searchWordsFromUserWithoutStopWordsNormalized.push(this.search.wordsFromUserNormalized[i]);
					searchWordsFromUserWithoutStopWordsNormalizedAccordingToOptions.push(this.search.wordsFromUserNormalizedAccordingToOptions[i]);
				} else {
					this.search.removedStopWords.push(this.search.wordsFromUser[i]);
				}
			}
		}

		this.search.wordsFromUser = searchWordsFromUserWithoutStopWords;
		this.search.wordsFromUserNormalized = searchWordsFromUserWithoutStopWordsNormalized;
		this.search.wordsFromUserNormalizedAccordingToOptions = searchWordsFromUserWithoutStopWordsNormalizedAccordingToOptions;
	};

	Album.prototype.getNumProtectedCacheBases = function(codesComplexCombination) {
		var [albumCodesComplexCombinationList, mediaCodesComplexCombinationList] = phFl.convertComplexCombinationsIntoLists(codesComplexCombination);

		var count = 0;
		for (var codesComplexCombinationFromObject in this.numsProtectedMediaInSubTree) {
			if (this.numsProtectedMediaInSubTree.hasOwnProperty(codesComplexCombinationFromObject) && codesComplexCombinationFromObject !== ",") {
				var [albumCodesComplexCombinationListFromObject, mediaCodesComplexCombinationListFromObject] =
					phFl.convertComplexCombinationsIntoLists(codesComplexCombinationFromObject);

				if (
					(
						albumCodesComplexCombinationList.length &&
						albumCodesComplexCombinationListFromObject.length &&
						mediaCodesComplexCombinationList.length &&
						mediaCodesComplexCombinationListFromObject.length &&
						albumCodesComplexCombinationListFromObject.indexOf(albumCodesComplexCombinationList[0]) !== -1 &&
						mediaCodesComplexCombinationListFromObject.indexOf(mediaCodesComplexCombinationList[0]) !== -1
					) || (
						! albumCodesComplexCombinationList.length &&
						! albumCodesComplexCombinationListFromObject.length &&
						mediaCodesComplexCombinationList.length &&
						mediaCodesComplexCombinationListFromObject.length &&
						mediaCodesComplexCombinationListFromObject.indexOf(mediaCodesComplexCombinationList[0]) !== -1
					) || (
						albumCodesComplexCombinationList.length &&
						albumCodesComplexCombinationListFromObject.length &&
						! mediaCodesComplexCombinationList.length &&
						! mediaCodesComplexCombinationListFromObject.length &&
						albumCodesComplexCombinationListFromObject.indexOf(albumCodesComplexCombinationList[0]) !== -1
					)
				)
					count ++;
			}
		}

		return count;
	};

	Album.prototype.hasProtectedContent = function() {
		return this.passwordCodes().length > 0;
	};

	Album.prototype.hasVeiledProtectedContent = function() {
		// this function is for detecting if the auth dialog has to be shown
		// to detect if protected content has to be loaded use hasUnloadedProtectedContent() instead

		function combinationIsVeiled(codesSeparatedByDash) {
			return ! guessedPasswordCodes.length || guessedPasswordCodes.every(
				code => ! codesSeparatedByDash.split("-").includes(code)
			);
		}

		let guessedPasswordCodes = this.guessedPasswordCodes();

		if (this.isVirtual())
			return false;

		let keys = Object.keys(this.numsProtectedMediaInSubTree).filter(
			key => key != ","
		);
		if (! guessedPasswordCodes.length && ! keys.length)
			return false;

		if (! guessedPasswordCodes.length && keys.length)
			return true;

		return keys.some(
			codesCombination => codesCombination.split(",").filter(
				combination => combination !== ""
			).every(
				combination => combinationIsVeiled(combination)
			)
		);
	};

	Album.prototype.generateAncestorsCacheBase = function() {
		if (! this.hasOwnProperty("ancestorsCacheBase")) {
			var i;
			this.ancestorsCacheBase = [];
			var splittedCacheBase = this.cacheBase.split(env.options.cache_folder_separator);
			var length = splittedCacheBase.length;
			this.ancestorsCacheBase[0] = splittedCacheBase[0];
			if (this.isSearch()) {
				this.ancestorsCacheBase[1] = this.cacheBase;
			} else {
				for (i = 1; i < length; i ++) {
					this.ancestorsCacheBase[i] = [this.ancestorsCacheBase[i - 1], splittedCacheBase[i]].join(env.options.cache_folder_separator);
				}
			}
		}

		return;
	};

	Album.prototype.visibleNumsMedia = function() {
		if (util.geotaggedContentIsHidden())
			return this.nonGeotagged.numsMedia;
		else
			return this.numsMedia;
	};

	Album.prototype.visibleNumsMediaInSubTree = function() {
		if (util.geotaggedContentIsHidden())
			return this.nonGeotagged.numsMediaInSubTree;
		else
			return this.numsMediaInSubTree;
	};

	Album.prototype.pickRandomMediaForCompositeImage = function(numMedia) {
		let self = this;
		let arrayAlreadyPickedMedia = [];
		return new Promise(
			function(resolve_pickRandomMediaForCompositeImage, reject_pickRandomMediaForCompositeImage) {
				function pickOneMoreRandomMedia() {
					return new Promise(
						function(resolve_pickOneMoreRandomMedia, reject_pickOneMoreRandomMedia) {
							var randomMediaPromise = self.pickRandomMedia(
								null,
								function error() {
									// executions shoudn't arrive here, if it arrives it's because of some error
									console.trace();
								}
							);
							randomMediaPromise.then(
								function([randomAlbum, indexVisible]) {
									let pickedSingleMedia = randomAlbum.visibleMedia()[indexVisible];
									let identifier;
									if (pickedSingleMedia.isAudio())
										identifier = env.logo;
									else if (numMedia === 1)
										identifier = pickedSingleMedia.sharingMediaPath();
									else
										identifier = pickedSingleMedia.sharingMediaPath(true);
									resolve_pickOneMoreRandomMedia(identifier);
								}
							);
						}
					);
				}

				function recursiveGetRandomElement() {
					let pickOnePromise = pickOneMoreRandomMedia();
					pickOnePromise.then(
						function(identifier) {
							if (! arrayAlreadyPickedMedia.includes(identifier))
								arrayAlreadyPickedMedia.push(identifier);

							if (arrayAlreadyPickedMedia.length < numMedia) {
								recursiveGetRandomElement();
							} else {
								resolve_pickRandomMediaForCompositeImage(arrayAlreadyPickedMedia);
							}
						}
					);
				}
				// end of nested function

				recursiveGetRandomElement();
			}
		);
	};


	Album.prototype.pickRandomMedia = function(iVisibleSubalbum, error) { // from old version
		var self = this;
		var indexVisible;
		var ithSubalbum;
		if (iVisibleSubalbum === null)
			ithSubalbum = self;
		else
			ithSubalbum = self.visibleSubalbums()[iVisibleSubalbum];

		return new Promise(
			function(resolve_pickRandomMedia) {
				var promise = ithSubalbum.toAlbum(error, {getMedia: false, getPositions: false});
				promise.then(
					function beginPick(ithAlbum) {
						if (iVisibleSubalbum !== null)
							self.visibleSubalbums()[iVisibleSubalbum] = ithAlbum;

						indexVisible = Math.floor(Math.random() * ithAlbum.visibleNumsMediaInSubTree().imagesAudiosVideosTotal());
						nextAlbum(ithAlbum, resolve_pickRandomMedia);
					},
					function() {
						console.trace();
					}
				);
			}
		);
		//// end of function pickRandomMedia ////////////////////

		function nextAlbum(ithAlbum, resolve_pickRandomMedia) {
			if (! ithAlbum.visibleNumsMediaInSubTree().imagesAudiosVideosTotal()) {
				error();
				return;
			}

			let nVisibleMediaInAlbum = ithAlbum.visibleNumsMedia().imagesAudiosVideosTotal();

			if (ithAlbum.isTransversal() && ithAlbum.visibleSubalbums().length > 0) {
				// do not get the random media from the year/country nor the month/state albums
				// this way loading of albums is much faster
				nVisibleMediaInAlbum = 0;
			}

			if (indexVisible >= nVisibleMediaInAlbum) {
				indexVisible -= nVisibleMediaInAlbum;
				if (ithAlbum.visibleSubalbums().length) {
					let found = false;
					let visibleSubalbums = ithAlbum.visibleSubalbums();
					for (let j = 0; j < visibleSubalbums.length; j ++) {
						let jthSubalbum = visibleSubalbums[j];
						if (indexVisible >= jthSubalbum.visibleNumsMediaInSubTree().imagesAudiosVideosTotal()) {
							indexVisible -= jthSubalbum.visibleNumsMediaInSubTree().imagesAudiosVideosTotal();
						} else {
							var promise = jthSubalbum.toAlbum(error, {getMedia: false, getPositions: false});
							promise.then(
								function(jthAlbum) {
									nextAlbum(jthAlbum, resolve_pickRandomMedia);
								}
							);
							found = true;
							break;
						}
					}
					if (! found)
						error();
				}
			} else {
				var lastPromise = phFl.getAlbum(ithAlbum, error, {getMedia: true, getPositions: ! env.options.save_data});
				lastPromise.then(
					function(ithAlbumWithMediaAndPositions) {
						resolve_pickRandomMedia([ithAlbumWithMediaAndPositions, indexVisible]);
					},
					function() {
						console.trace();
					}
				);
			}
		}
	};

	Album.prototype.getMediaIndex = function(mediaFolderCacheBase, mediaCacheBase) {
		// returns the index of the media identified by the arguments
		// returns null if no media matches

		var mediaIndex = -1;
		var self = this;
		if (mediaCacheBase !== null) {
			mediaIndex = this.media.findIndex(
				function(thisMedia) {
					var matches =
						thisMedia.cacheBase === mediaCacheBase &&
						(mediaFolderCacheBase === null || thisMedia.foldersCacheBase === mediaFolderCacheBase);
					return matches;
				}
			);
			if (mediaIndex === -1) {
				$("#loading").stop().hide();

				if (this.hasVeiledProtectedContent()) {
					// the media not found could be a protected one, show the authentication dialog, it could be a protected media
					util.showAuthForm(null, true);
				} else {
					// surely the media doesn't exist

					$("#album-view").fadeOut(200).fadeIn(3500);
					$("#media-view").fadeOut(200);
					// $("#album-view").stop().fadeIn(3500);
					$("#error-text-image").stop().fadeIn(200);
					$("#error-text-image, #error-overlay, #auth-text").fadeOut(
						2500,
						function() {
							window.location.href = env.hashBeginning + self.cacheBase;
							// mediaIndex = -1;
							// keepOn();
							$("#media-view").fadeIn(100);
						}
					);
				}
				return null;
			}
		}
		return mediaIndex;
	};

	Album.prototype.isUndefinedOrFalse = function(property) {
		return ! this.hasOwnProperty(property) || ! this[property];
	};

	Album.prototype.isUndefinedOrTrue = function(property) {
		return ! this.hasOwnProperty(property) || this[property];
	};

	Album.prototype.isUndefinedOrDifferent = function(property, sorting) {
		return ! this.hasOwnProperty(property) || this[property] !== sorting;
	};

	// this function refer to the need that the html showed be sorted
	Album.prototype.needAlbumSort = function(sorting) {
		return this.isUndefinedOrDifferent("albumSort", sorting);
	};

	Album.prototype.needAlbumReverseSort = function(sorting) {
		return this.needAlbumSort(sorting) && (
			this.isUndefinedOrTrue("albumReverseSort") && ! env.albumReverseSort ||
			this.isUndefinedOrFalse("albumReverseSort") && env.albumReverseSort
		);
	};

	Album.prototype.needMediaSort = function(sorting) {
		return this.isUndefinedOrDifferent("mediaSort", sorting);
	};

	Album.prototype.needMediaReverseSort = function(sorting) {
		return this.needMediaSort(sorting) && (
			this.isUndefinedOrTrue("mediaReverseSort") && ! env.mediaReverseSort ||
			this.isUndefinedOrFalse("mediaReverseSort") && env.mediaReverseSort
		);
	};


	Album.prototype.prepareForShowing = function(mediaIndex) {
		let self = this;
		env.matomoLoaded = false;
		menuF.showHideDownloadSelectionInfo();

		if (env.currentMedia !== null && env.currentMedia.isAudio() && $("audio").length)
			// stop the audio, otherwise it will keep playing
			$("audio")[0].pause();

		if (env.currentMedia !== null && env.currentMedia.isVideo() && $("video").length)
			// stop the video, otherwise it will keep playing
			$("video")[0].pause();

		if (this.numsMediaInSubTree.imagesAudiosVideosTotal() === 0 && ! this.isSearch()) {
			// the album hasn't any content:
			// either the hash is wrong or it's a protected content album
			// go up
			window.location.href = util.upHash();
			return;
		}

		util.undie();
		$("#loading").hide();

		if (this !== env.currentAlbum) {
			// this if condition is required for when a password is guessed
			env.previousAlbum = env.currentAlbum;
		}
		env.albumOfPreviousState = env.currentAlbum;
		env.currentAlbum = this;

		if (env.currentAlbum.isMap())
			env.mapAlbum = env.currentAlbum;
		else if (env.currentAlbum.isSelection())
			env.selectionAlbum = env.currentAlbum;
		else if (env.currentAlbum.isSearch())
			env.searchAlbum = env.currentAlbum;

		env.previousMedia = env.currentMedia;

		env.currentMedia = null;
		if (mediaIndex !== -1)
			env.currentMedia = env.currentAlbum.media[mediaIndex];
		env.currentMediaIndex = mediaIndex;

		util.checkAlbumWithOneMedia();

		let menuIconTitle = util._t(".menu-icon-title");
		if (! env.isAnyMobile)
			menuIconTitle += ", " + util._t(".menu-icon-title-end");
		$("#menu-icon").attr("title", menuIconTitle);

		let infoIconTitle = util._t(".info-icon-title");
		$(".info-icon").attr("title", infoIconTitle);

		if (env.currentMedia === null)
			env.currentAlbum.sortAlbumsMedia();

		if (env.currentAlbumIsAlbumWithOneMedia) {
			$("#media-view").css("cursor", "default");
			$("#album-and-media-container").addClass("one-media");
			env.nextMedia = null;
			env.prevMedia = null;
		} else {
			$("#album-and-media-container").removeClass("one-media");
			$("#media-view").css("cursor", "ew-resize");
		}

		if (! util.isSearchHash() || env.currentAlbum.visibleSubalbums().length || env.currentAlbum.numVisibleMedia())
			$("#subalbums, #thumbs, #media-view").removeClass("hidden-by-no-results");

		if (env.currentMedia !== null) {
			env.nextMedia = null;
			env.prevMedia = null;
			$("#subalbums").addClass("hidden");
			env.currentMedia.show(env.currentAlbum, "center");
		} else {
			// currentMedia is null
			$("#media-view").addClass("hidden");
			$("#album-and-media-container").removeClass("single-media");
			$("#album-view").removeAttr("height");

			if (env.previousMedia === null) {
				$("#album-view").scrollTop(0);
				// // the following instruction is needed to activate the lazy loader
				// $("#album-view").trigger("scroll");
			}
			$("#album-view").off("mousewheel");
			$("#thumbs").css("height", "");
			$(".thumb-container").removeClass("current-thumb");
			$("#media-view, #album-view").removeClass("no-bottom-space");
			$("#album-view").removeClass("hidden");

			if (! env.currentAlbum.numVisibleMedia())
				$("#thumbs").addClass("hidden");

			if (env.currentAlbum.visibleSubalbums().length)
				$("#subalbums").removeClass("hidden");
			else
				$("#subalbums").addClass("hidden");
			if (! env.isRevertingFromHidingGeotaggedMedia)
				util.removeHighligths();
			$("body").off("mousewheel").on("mousewheel", tF.scrollAlbum);

			util.setMediaOptions();

			env.currentAlbum.setDescription();
			util.setDescriptionOptions();

			let adaptAndScroll = false;
			let thumbObject;
			if ($("#album-view").is(":visible")) {
				if (env.currentAlbum.visibleSubalbums().length) {
					env.currentAlbum.showSubalbums(false);
				} else {
					$("#subalbums").addClass("hidden");
				}

				if (env.currentAlbum.numVisibleMedia()) {
					if (
						env.albumOfPreviousState === null || (
							env.albumOfPreviousState !== env.currentAlbum ||
							env.albumOfPreviousState !== null && env.isFromAuthForm
						) ||
						! $("#thumbs").children().length ||
						env.isRevertingFromHidingGeotaggedMedia
					) {
						env.currentAlbum.showMedia();
					} else {
						util.adaptMediaCaptionHeight();
					}

					// set this flag for performing the actions after the title has been set
					adaptAndScroll = true;
				} else {
					$("#thumbs").addClass("hidden");
				}

				if (util.isSearchHash() && ! env.currentAlbum.visibleSubalbums().length && ! env.currentAlbum.numVisibleMedia()) {
					if (env.searchTooWide) {
						phFl.noResults(env.currentAlbum, "#search-too-wide");
						env.searchTooWide = false;
					} else if (env.currentAlbum.search.wordsFromUser.length === 0) {
						phFl.noResults(env.currentAlbum, "#no-search-string-after-stopwords-removed");
					} else {
						phFl.noResults(env.currentAlbum);
					}
				}

				if (env.isRevertingFromHidingGeotaggedMedia && this.subalbums.length && env.albumInSubalbumDiv !== this)
					env.isRevertingFromHidingGeotaggedMedia = false;

				util.decideWhatToHighlight();


				env.windowWidth = $(window).innerWidth();

				// menuF.updateMenu();
				if (env.currentAlbum.visibleSubalbums().length)
					this.bindSubalbumSortEvents();
				if (env.currentAlbum.numVisibleMedia())
					this.bindMediaSortEvents();

				util.addMediaLazyLoader(true);
			}


			let titlePromise = tF.setTitle("album", null);
			titlePromise.then(
				function titleSet() {
					// // height must be re-adaptated because title has been added
					// util.adaptMediaCaptionHeight();
					util.setUpButtonVisibility();

					if (adaptAndScroll) {
						if (thumbObject !== undefined && thumbObject.length) {
							util.scrollAlbumViewToHighlightedMedia(thumbObject.parent().parent());
						}
					}

					if ($("#album-view").is(":visible")) {
						$(window).off("resize").on(
							"resize",
							function () {
								var isMap = util.isMap();
								var isPopup = util.isPopup();
								var previousWindowWidth = env.windowWidth;
								var previousWindowHeight = env.windowHeight;
								env.windowWidth = $(window).innerWidth();
								env.windowHeight = $(window).innerHeight();
								if (env.windowWidth === previousWindowWidth && (env.isAnyMobile || env.windowHeight === previousWindowHeight))
									// avoid considering a resize when the mobile browser shows/hides the location bar
									return;

								env.fullSizeMediaBoxContainerContent =
									$(env.fullSizeMediaBoxContainerContent).css("width", env.windowWidth)[0].outerHTML;

								$("#loading").show();

								util.setTitleOptions();
								util.setSubalbumsOptions();

								util.adaptSubalbumCaptionHeight(); // resizing window
								util.adaptMediaCaptionHeight(); // resizing window

								if (isMap || isPopup) {
									mapF.resizeMap();

									if ($("#popup-images-wrapper .highlighted-object").length && env.mapAlbum.media.length)
										env.highlightedObjectId = $("#popup-images-wrapper .highlighted-object").attr("id");

									if (isPopup) {
										if (util.isShiftOrControl())
											$(".shift-or-control .leaflet-popup-close-button")[0].click();
										env.mapAlbum.prepareAndDoPopupUpdate();
										util.adaptMediaCaptionHeight(true);
									}
								}

								if (env.currentAlbum.visibleSubalbums().length && util.aSubalbumIsHighlighted())
									util.scrollAlbumViewToHighlightedSubalbum($("#subalbums .highlighted-object"));
								if (env.currentAlbum.numVisibleMedia() && util.aSingleMediaIsHighlighted())
									util.scrollAlbumViewToHighlightedMedia($("#thumbs .highlighted-object"));
								// here the isPopup() function must be used, because the popup may be already closed
								if (util.isPopup() && env.mapAlbum.media.length)
									util.scrollPopupToHighlightedThumb($("#popup-images-wrapper .highlighted-object"));

								$("#loading").hide();

								util.addMediaLazyLoader(false);
								// menuF.updateMenu();

								$.executeAfterEvent(
									"socialShareKitLoadedEvent",
									function() {
										util.correctElementPositions();
									}
								);
							}
						);

						env.isFromAuthForm = false;
					}
				}
			);
			$("#powered-by").show();
		}

		util.showHideSlideshowIcon();
		$.executeAfterEvent(
			"socialShareKitLoadedEvent",
			function() {
				util.setSocialButtons(); // in prepareForShowing

				if (env.currentMedia === null) {
					if (env.currentAlbum.visibleSubalbums().length) {
						self.setSubalbumsSortDataAndAdaptCaption();
					}
					if (env.currentAlbum.numVisibleMedia()) {
						self.setMediaSortDataAndAdaptCaption();
						if (util.isPopup())
							self.setMediaSortDataAndAdaptCaption(true);
					}

					util.correctElementPositions();
				}
			}
		);
	};


	Album.prototype.slideshow = function(ev) {
		function pauseSlideshow(ev) {
			$("#started-slideshow-pause").hide();
			$("#started-slideshow-play").show();
			$("#fullscreen-wrapper").addClass("paused");
			wasPaused = true;
		}

		function resumeSlideshow(ev) {
			if (env.currentMedia.isAudio() && ! $("audio#media-center")[0].paused)
				$("audio#media-center")[0].pause();
			if (env.currentMedia.isVideo() && ! $("video#media-center")[0].paused)
				$("video#media-center")[0].pause();
			$("#started-slideshow-pause").show();
			$("#started-slideshow-play").hide();
			$("#fullscreen-wrapper").removeClass("paused");
		}

		function updateSpeed() {
			var string = "";
			if (! env.isAnyMobile)
				string = util._t("#started-slideshow-interval") + ": ";
			$("#started-slideshow-interval").html(string + env.slideshowInterval);
		}

		$("#started-slideshow").html(util._t("#started-slideshow"));
		$("#started-slideshow").fadeIn(
			400,
			function() {
				if (! env.isAnyMobile)
					$("#started-slideshow").append(
						"<div id='started-slideshow-how-to-stop' class='smaller'>" + util._t("#started-slideshow-how-to-stop") + "</div>"
					);
				$("#started-slideshow").fadeOut(3000);
			}
		);

		env.slideshowCurrentMediaWasNullBefore = false;
		if (env.currentMedia === null) {
			// reach the first image before starting the slideshow
			let highlightedObject = util.highlightedObject();
			while (true) {
				util.addHighlightToMediaOrSubalbum(highlightedObject);
				if (util.objectIsASubalbum(highlightedObject)) {
					util.scrollAlbumViewToHighlightedSubalbum(highlightedObject);
				} else {
					util.scrollAlbumViewToHighlightedMedia(highlightedObject);
					highlightedObject.trigger("click");
					break;
				}
				highlightedObject = util.nextObjectForHighlighting(highlightedObject);
			}
			// we must define the current media
			let currentMediaCacheBase = highlightedObject.parent().attr("href").split("/").pop();
			env.currentMedia = env.currentAlbum.media.find(singleMedia => singleMedia.cacheBase === currentMediaCacheBase);
			env.slideshowCurrentMediaWasNullBefore = true;
		}

		env.slideshowId = 1;
		let enterSlideshowFromFullscreen = false;
		if (env.fullScreenStatus)
			enterSlideshowFromFullscreen = true;
		util.toggleFullscreen(ev, enterSlideshowFromFullscreen);

		$("#slideshow-buttons").stop().fadeTo(
			100,
			0.8,
			function() {
				$("#slideshow-buttons").stop().fadeTo(3000, 0.4);
			}
		);
		$("#slideshow-buttons").off("mouseenter").on(
			"mouseenter",
			function() {
				$("#slideshow-buttons").stop().fadeTo(100, 1);
			}
		);
		$("#slideshow-buttons").off("mouseleave").on(
			"mouseleave",
			function() {
				$("#slideshow-buttons").stop().fadeTo(1000, 0.4);
			}
		);

		var counter = 0;
		var wasPaused = false;
		env.slideshowId = setInterval(
			function() {
				function startAudioVideo() {
					$("#media-center")[0].play();
					$("#media-center").off('ended').on(
						'ended',
						function(){
							var resumeIntervalId = setInterval(
								function() {
									clearInterval(resumeIntervalId);
									resumeSlideshow();
								},
								env.slideshowInterval * 1000 / 2
							);

						}
					);
				}

				if (env.currentMedia !== null && ! $("#fullscreen-wrapper").hasClass("paused")) {
					updateSpeed();
					counter ++;
					if (! wasPaused && counter % 10 === 0 && env.currentMedia.isAudio()) {
						pauseSlideshow();
						$("<audio>").on(
							"loadstart",
							startAudioVideo
						).attr("src", $("#media-center").attr("src"));
					} else if (! wasPaused && counter % 10 === 0 && env.currentMedia.isVideo()) {
						pauseSlideshow();
						$("<video>").on(
							"loadstart",
							startAudioVideo
						).attr("src", $("#media-center").attr("src"));
					} else if (wasPaused || counter > env.slideshowInterval * 10) {
						$("#next")[0].click();
						wasPaused = false;
						counter = 0;
					}
				}
			},
			100
		);

		// the slideshow is ended by the toggleFullscreen() function when the user exits fullscreen
		// or clicking the stop button

		$("#started-slideshow-orientation-auto").off("click").on(
			"click",
			function(ev) {
				env.slideshowAutoRotation = ! env.slideshowAutoRotation;
				$("#started-slideshow-orientation-auto").toggleClass("active");
				if (env.slideshowAutoRotation) {
					env.currentMedia.addRotation($("#media-center"));
					if (env.prevMedia)
						env.prevMedia.addRotation($("#media-left"));
					if (env.nextMedia)
						env.nextMedia.addRotation($("#media-right"));
				} else {
					env.currentMedia.removeRotation($("#media-center"));
					if (env.prevMedia)
						env.prevMedia.removeRotation($("#media-left"));
					if (env.nextMedia)
						env.nextMedia.removeRotation($("#media-right"));
				}
				util.setFinalOptions(env.currentMedia, false); // OK: on click
			}
		);

		$("#started-slideshow-stop").off("click").on(
			"click",
			util.toggleFullscreen
		);

		$("#started-slideshow-pause").off("click").on(
			"click",
			pauseSlideshow
		);

		$("#started-slideshow-play").off("click").on(
			"click",
			resumeSlideshow
		);

		$("#started-slideshow-slow-down").off("click").on(
			"click",
			function(ev) {
				if (env.slideshowInterval > 1) {
					env.slideshowInterval -= 1;
					menuF.setCookie("slideshowInterval", env.slideshowInterval);
					updateSpeed();
					if (env.slideshowInterval === 1)
						$("#started-slideshow-slow-down").addClass("dimmed");
				}
			}
		);

		$("#started-slideshow-speed-up").off("click").on(
			"click",
			function(ev) {
				if (env.slideshowInterval === 1) {
					$("#started-slideshow-slow-down").removeClass("dimmed");
				}
				env.slideshowInterval += 1;
				menuF.setCookie("slideshowInterval", env.slideshowInterval);
				updateSpeed();
			}
		);

		$(".slideshow-button").show();
		$("#started-slideshow-play").hide();
		$("#started-slideshow-speed-up").show();
		$("#started-slideshow-slow-down").show();
		updateSpeed();
	};

	Album.prototype.bindSubalbumSortEvents = function() {
		// binds the click events to the sort buttons

		var self = this;

		$("li.album-sort.by-exif-date-max").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortSubalbumsBy(ev, "exifDateMax");
				self.setSubalbumsSortDataAndAdaptCaption();
			}
		);
		$("li.album-sort.by-exif-date-min").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortSubalbumsBy(ev, "exifDateMin");
				self.setSubalbumsSortDataAndAdaptCaption();
			}
		);
		$("li.album-sort.by-file-date-max").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortSubalbumsBy(ev, "fileDateMax");
				self.setSubalbumsSortDataAndAdaptCaption();
			}
		);
		$("li.album-sort.by-file-date-min").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortSubalbumsBy(ev, "fileDateMin");
				self.setSubalbumsSortDataAndAdaptCaption();
			}
		);
		$("li.album-sort.by-name").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortSubalbumsBy(ev, "name");
				self.setSubalbumsSortDataAndAdaptCaption();
			}
		);
		$("li.album-sort.by-pixel-size-min").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortSubalbumsBy(ev, "pixelSizeMin");
				self.setSubalbumsSortDataAndAdaptCaption();
			}
		);
		$("li.album-sort.by-pixel-size-max").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortSubalbumsBy(ev, "pixelSizeMax");
				self.setSubalbumsSortDataAndAdaptCaption();
			}
		);
		$("li.album-sort.by-file-size").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortSubalbumsBy(ev, "fileSize");
				self.setSubalbumsSortDataAndAdaptCaption();
			}
		);
		$("li.album-sort.reverse").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortSubalbumsReverse(ev);
				self.setSubalbumsSortDataAndAdaptCaption();
			}
		);
	};

	Album.prototype.bindMediaSortEvents = function() {
		// binds the click events to the sort buttons

		var self = this;

		$("li.media-sort.by-exif-date").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortMediaBy(ev, "exifDate");
				self.setMediaSortDataAndAdaptCaption();
				if (util.isPopup())
					self.setMediaSortDataAndAdaptCaption(true);
			}
		);
		$("li.media-sort.by-file-date").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortMediaBy(ev, "fileDate");
				self.setMediaSortDataAndAdaptCaption();
				if (util.isPopup())
					self.setMediaSortDataAndAdaptCaption(true);
			}
		);
		$("li.media-sort.by-name").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortMediaBy(ev, "name");
				self.setMediaSortDataAndAdaptCaption();
				if (util.isPopup())
					self.setMediaSortDataAndAdaptCaption(true);
			}
		);
		$("li.media-sort.by-pixel-size").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortMediaBy(ev, "pixelSize");
				self.setMediaSortDataAndAdaptCaption();
				if (util.isPopup())
					self.setMediaSortDataAndAdaptCaption(true);
			}
		);
		$("li.media-sort.by-file-size").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortMediaBy(ev, "fileSize");
				self.setMediaSortDataAndAdaptCaption();
				if (util.isPopup())
					self.setMediaSortDataAndAdaptCaption(true);
			}
		);

		$("li.media-sort.reverse").off("click").on(
			"click",
			function(ev) {
				menuF.addHighlightToItem($(this));
				self.sortMediaReverse(ev);
				self.setMediaSortDataAndAdaptCaption();
				if (util.isPopup())
					self.setMediaSortDataAndAdaptCaption(true);
			}
		);
	};


	Album.prototype.sortSubalbumsCommons = function() {
		this.sortAlbumsMedia();

		let aSubalbumWasHighlighted = util.aSubalbumIsHighlighted();
		let highlightedObjectSelector = util.highlightedObjectSelector();

		env.currentAlbum.showSubalbums(true);

		if (aSubalbumWasHighlighted) {
			util.highlighObject(highlightedObjectSelector, aSubalbumWasHighlighted);
		}
		menuF.updateMenu(this);
	};

	Album.prototype.sortMediaCommons = function() {
		this.sortAlbumsMedia();

		let aSubalbumWasHighlighted = util.aSubalbumIsHighlighted();
		let highlightedObjectSelector = util.highlightedObjectSelector();

		this.showMedia();

		if (! util.isPopup() && env.currentMedia !== null) {
			util.scrollBottomMediaToHighlightedThumb();
		}

		if (util.isPopup() && env.mapAlbum.media.length || ! aSubalbumWasHighlighted) {
			util.highlighObject(highlightedObjectSelector, aSubalbumWasHighlighted);
		}

		if (util.isPopup() && env.currentAlbum.numsMedia.imagesAudiosVideosTotal()) {
			// the regular album must be sorted, too
			env.currentAlbum.sortAlbumsMedia();
			if (! aSubalbumWasHighlighted) {
				let inThumbsTrue = true;
				highlightedObjectSelector = util.highlightedObjectSelector(inThumbsTrue);

				env.currentAlbum.showMedia();

				if (! aSubalbumWasHighlighted) {
					util.highlighObject(highlightedObjectSelector, aSubalbumWasHighlighted, inThumbsTrue);
				}
			}
		}

		menuF.updateMenu(this);
	};

	Album.prototype.sortSubalbumsBy = function(ev, sorting) {
		ev.stopPropagation();
		if (
			this.isUndefinedOrDifferent("albumSort", sorting) &&
			(ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
		) {
			env.albumSort = sorting;
			menuF.setCookie("albumSortRequested", env.albumSort);

			this.sortSubalbumsCommons();
		}

		return false;
	};

	Album.prototype.sortSubalbumsReverse = function(ev) {
		ev.stopPropagation();
		if (
			(ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
		) {
			env.albumReverseSort = ! env.albumReverseSort;
			menuF.setBooleanCookie("albumReverseSortRequested", env.albumReverseSort);

			this.sortSubalbumsCommons();
		}
		return false;
	};

	Album.prototype.sortMediaBy = function (ev, sorting) {
		ev.stopPropagation();
		if (
			this.isUndefinedOrDifferent("mediaSort", sorting) &&
			(ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
		) {
			env.mediaSort = sorting;
			menuF.setCookie("mediaSortRequested", env.mediaSort);

			this.sortMediaCommons();
		}
		return false;
	};

	Album.prototype.sortMediaReverse = function(ev) {
		ev.stopPropagation();
		if (
			(ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
		) {
			env.mediaReverseSort = ! env.mediaReverseSort;
			menuF.setBooleanCookie("mediaReverseSortRequested", env.mediaReverseSort);

			this.sortMediaCommons();
		}
		return false;
	};


	Album.prototype.visibleMedia = function(geotaggedContentIsHidden) {
		if (this.isABigTransversalAlbum())
			return new Media([]);

		if (
			geotaggedContentIsHidden !== undefined && geotaggedContentIsHidden ||
			geotaggedContentIsHidden === undefined && util.geotaggedContentIsHidden()
		)
			return this.media.filter(singleMedia => ! singleMedia.hasGpsData());
		else
			return this.media;
	};

	Album.prototype.numsVisibleMedia = function(geotaggedContentIsHidden) {
		if (this.isABigTransversalAlbum())
			return new ImagesAudiosVideos();

		if (
			geotaggedContentIsHidden !== undefined && geotaggedContentIsHidden ||
			geotaggedContentIsHidden === undefined && util.geotaggedContentIsHidden()
		)
			return this.nonGeotagged.numsMedia;
		else
			return this.numsMedia;
	};

	Album.prototype.numVisibleMedia = function(geotaggedContentIsHidden) {
		if (this.isABigTransversalAlbum())
			return 0;

		return this.numsVisibleMedia().imagesAudiosVideosTotal();
	};

	Album.prototype.numsVisibleMediaInSubtree = function(geotaggedContentIsHidden) {
		if (
			geotaggedContentIsHidden !== undefined && geotaggedContentIsHidden ||
			geotaggedContentIsHidden === undefined && util.geotaggedContentIsHidden()
		)
			return this.nonGeotagged.numsMediaInSubTree;
		else
			return this.numsMediaInSubTree;
	};

	Album.numsVisibleMediaInAlbumForDownload = function(geotaggedContentIsHidden) {
		let result;
		if (
			geotaggedContentIsHidden !== undefined && geotaggedContentIsHidden ||
			geotaggedContentIsHidden === undefined && util.geotaggedContentIsHidden()
		)
			result = this.nonGeotagged.numsMedia.filterAccordingToTypeOfDownloadOptions();
		else
			result = this.numsMedia.filterAccordingToTypeOfDownloadOptions();
			// result = this.media.imagesAudiosVideosCount().filterAccordingToTypeOfDownloadOptions();
		return result;
	};

	Album.prototype.numsVisibleMediaInSubtreeForDownload = function(geotaggedContentIsHidden) {
		let result;
		if (! env.options.download_subalbums) {
			result = this.numsVisibleMediaInAlbumForDownload();
		} else {
			if (
				geotaggedContentIsHidden !== undefined && geotaggedContentIsHidden ||
				geotaggedContentIsHidden === undefined && util.geotaggedContentIsHidden()
			)
				result = this.nonGeotagged.numsMediaInSubTree.filterAccordingToTypeOfDownloadOptions();
			else
				result = this.numsMediaInSubTree.filterAccordingToTypeOfDownloadOptions();
		}
		return result;
	};

	Album.prototype.numVisibleImagesOrAudiosOrVideosInSubTree = function(geotaggedContentIsHidden, what) {
		if (
			geotaggedContentIsHidden !== undefined && geotaggedContentIsHidden ||
			geotaggedContentIsHidden === undefined && util.geotaggedContentIsHidden()
		)
			return this.nonGeotagged.numsMediaInSubTree[what];
		else
			return this.numsMediaInSubTree[what];
	};

	Album.prototype.numVisibleImagesInSubtree = function(geotaggedContentIsHidden) {
		return this.numVisibleImagesOrAudiosOrVideosInSubTree(geotaggedContentIsHidden, "images");
	};

	Album.prototype.numVisibleAudiosInSubtree = function(geotaggedContentIsHidden) {
		return this.numVisibleImagesOrAudiosOrVideosInSubTree(geotaggedContentIsHidden, "audios");
	};

	Album.prototype.numVisibleVideosInSubtree = function(geotaggedContentIsHidden) {
		return this.numVisibleImagesOrAudiosOrVideosInSubTree(geotaggedContentIsHidden, "videos");
	};

	// Album.prototype.numVisibleMediaInSubtree = function(geotaggedContentIsHidden) {
	// 	if (
	// 		geotaggedContentIsHidden !== undefined && geotaggedContentIsHidden ||
	// 		geotaggedContentIsHidden === undefined && util.geotaggedContentIsHidden()
	// 	)
	// 		return this.nonGeotagged.numsMediaInSubTree.imagesAudiosVideosTotal();
	// 	else
	// 		return this.numsMediaInSubTree.imagesAudiosVideosTotal();
	// };
	//
	Album.prototype.visibleSubalbums = function(geotaggedContentIsHidden) {
		if (
			geotaggedContentIsHidden !== undefined && geotaggedContentIsHidden ||
			geotaggedContentIsHidden === undefined && util.geotaggedContentIsHidden()
		)
			return this.subalbums.filter(subalbum => ! subalbum.hasGpsData());
		else
			return this.subalbums;
	};

	Album.prototype.isABigTransversalAlbum = function() {
		return (
			(
				this.isByDate() &&
				this.path.split("/").length < 4 ||
				this.isByGps() &&
				this.path.split("/").length < 5
			) &&
			! env.options.show_big_virtual_folders &&
			(
				util.geotaggedContentIsHidden() ?
				this.nonGeotagged.numsMedia.imagesAudiosVideosTotal() :
				this.numsMedia.imagesAudiosVideosTotal()
			) >= env.options.big_virtual_folders_threshold
		);
	};


	Album.prototype.showMedia = function(lazyload = true) {
		function generateSingleMediaThumbnail(i) {
			let iMedia = i;
			let ithMedia = self.media[iMedia];
			let thumbHash;
			let width, height;
			let thumbHeight, thumbWidth, calculatedWidth, calculatedHeight;
			let id = phFl.convertCacheBaseToId(ithMedia.cacheBase);
			let isAudio = ithMedia.isAudio();

			if (! isAudio)
				thumbHash = ithMedia.chooseMediaThumbnail(thumbnailSize);

			width = ithMedia.width();
			height = ithMedia.height();

			if (! env.options.only_square_thumbnails && env.options.media_thumb_type.indexOf("fixed_height") > -1) {
				if (height < env.options.media_thumb_size) {
					thumbHeight = height;
					thumbWidth = width;
				} else {
					thumbHeight = env.options.media_thumb_size;
					thumbWidth = Math.round(thumbHeight * width / height);
				}
				calculatedWidth = thumbWidth;
			} else if (env.options.only_square_thumbnails || env.options.media_thumb_type.indexOf("square") > -1) {
				thumbHeight = thumbnailSize;
				thumbWidth = thumbnailSize;
				calculatedWidth = env.options.media_thumb_size;
			}
			calculatedHeight = env.options.media_thumb_size;

			let albumViewPadding = $("#album-view").css("padding");
			if (! albumViewPadding)
				albumViewPadding = 0;
			else
				albumViewPadding = parseInt(albumViewPadding);
			calculatedWidth = Math.round(Math.min(calculatedWidth, env.windowWidth - 2 * albumViewPadding));
			calculatedHeight = Math.round(calculatedWidth / thumbWidth * thumbHeight);

			let mapLinkIcon = "";
			if (
				! inPopup && (
					ithMedia.hasGpsData() ||
					util.isPhp() &&
					env.options.user_may_suggest_location &&
					env.options.request_password_email &&
					! isAudio
				)
			) {
				let imgHtml =
						"<img " +
							"class = 'thumbnail-map-link' ";
				if (ithMedia.hasGpsData()){
					imgHtml +=
							"src='img/ic_place_white_24dp_2x.png' " +
							"width='30' " +
							"height='42'";
				} else {
					imgHtml +=
							"src='img/ic_place_white_24dp_2x_with_plus.png' " +
							"width='48' " +
							"height='42'";
				}
				imgHtml +=
						">";
				mapLinkIcon = "<a id='media-map-link-" + id + "'>" + imgHtml + "</a>";
			}

			let imgHtml =
					"<img " +
						"class='select-box' " +
					">";

			let selectBoxHtml = "<a id='" + ithMedia.mediaSelectBoxSelector().substr(1) + "'>" + imgHtml + "</a>";

			let mediaHash;
			if (env.hashComponents.collectionCacheBase !== undefined && env.hashComponents.collectionCacheBase !== null)
				mediaHash = util.encodeHash(self.cacheBase, ithMedia.cacheBase, ithMedia.foldersCacheBase, env.hashComponents.collectedAlbumCacheBase, env.hashComponents.collectionCacheBase);
			else
				mediaHash = util.encodeHash(self.cacheBase, ithMedia.cacheBase, ithMedia.foldersCacheBase);

			let data = "", popupPrefix = "";
			if (inPopup) {
				data =
					"data='" +
						JSON.stringify(
							{
								width: width,
								height: height,
								albumCacheBase: self.cacheBase,
								mediaHash: mediaHash
							}
						) +
					"' ";
				popupPrefix = "popup-";
			}

			let mediaId = popupPrefix + ithMedia.foldersCacheBase + "--" + ithMedia.cacheBase;

			let dataSrc;
			if (! isAudio) {
				dataSrc = encodeURI(thumbHash);
			} else {
				dataSrc = "img/audio.png";
				thumbWidth = "500";
				thumbHeight = "500";
			}

			imgHtml =
				"<img ";
			if (lazyload) {
				imgHtml +=
					"data-src='" + dataSrc + "' " +
					"src='img/image-placeholder.jpg' ";
			} else {
				imgHtml +=
					"src='" + dataSrc + "' ";
			}
			imgHtml +=
					data +
					"class='thumbnail " + lazyClass + "' " +
					"width='" + thumbWidth + "' " +
					"height='" + thumbHeight + "' " +
					"id='" + mediaId + "' " +
					"style='" +
						 "width: " + calculatedWidth + "px; " +
						 "height: " + calculatedHeight + "px;" +
						 "'" +
				"/>";

			let imageId = ithMedia.foldersCacheBase + "--" + ithMedia.cacheBase;
			if (! inPopup)
				imageId = "album-view-" + imageId;
			else
				imageId = "popup-" + imageId;

			let imageString =
				"<div class='thumb-and-caption-container' style='" +
							"width: " + calculatedWidth + "px;" +
							"'";
			if (inPopup)
				imageString += " id='" + imageId + "'";
			imageString +=
				">" +
					"<div class='thumb-container' " + "style='" +
							// "width: " + calculatedWidth + "px; " +
							"height: " + env.options.media_thumb_size + "px;" +
					"'>" +
						mapLinkIcon +
						selectBoxHtml +
						"<span class='helper'></span>" +
						imgHtml +
					"</div>" +
					"<div class='media-caption'>";
			let name = "", title;
			if (util.isPopup()) {
				if (! ithMedia.hasOwnProperty("captionsForPopup"))
					ithMedia.generateCaptionsForPopup(self);
				if (ithMedia.captionsForPopup[0])
					name = ithMedia.captionsForPopup.join(env.br);
			} else if (self.isSearch()) {
				// if (! ithMedia.hasOwnProperty("captionsForSearch"))
				// 	ithMedia.generateCaptionsForSearch(self);
				if (ithMedia.hasOwnProperty("captionsForSearch") && ithMedia.captionsForSearch[0])
					name = ithMedia.captionsForSearch.join(env.br);
			} else if (self.isSelection()) {
				// if (! ithMedia.hasOwnProperty("captionsForSelection"))
				// 	ithMedia.generateCaptionsForSelection(self);
				if (ithMedia.hasOwnProperty("captionsForSelection") && ithMedia.captionsForSelection[0])
					name = ithMedia.captionsForSelection.join(env.br);
			} else if (self.isMap()) {
				// if (! ithMedia.hasOwnProperty("captionsForPopup"))
				// 	ithMedia.generateCaptionsForPopup(self);
				if (ithMedia.hasOwnProperty("captionsForPopup") && ithMedia.captionsForPopup[0])
					name = ithMedia.captionsForPopup.join(env.br);
			}

			if (! name) {
				[name, title] = ithMedia.nameAndTitleForShowing(true, true);
			}

			let spanHtml =
						"<div class='media-name'>" +
								name +
						"</div>";

			imageString += spanHtml;

			if (ithMedia.metadata.hasOwnProperty("description")) {
				imageString +=
						"<div class='media-description'>" +
							"<div class='description ellipsis'>" + util.stripHtmlAndReplaceEntities(ithMedia.metadata.description) + "</div>" +
						"</div>";
			}
			if (ithMedia.metadata.hasOwnProperty("tags") && ithMedia.metadata.tags.length) {
				imageString +=
						"<div class='media-tags'>" +
							"<span class='tags'>" + util._t("#tags") + ": <span class='tag'>" + ithMedia.metadata.tags.map(tag => util.addTagLink(tag)).join("</span>, <span class='tag'>") + "</span></span>" +
						"</div>";
			}
			imageString +=
						"<div class='media-bottom'>" +
							"<div class='sort-data media-sort-data'></div>" +
						"</div>" +
					"</div>" +
				"</div>";

			if (inPopup) {
				$(thumbsSelector).append(imageString);
			} else {
				let aHtml = "<a href='" + mediaHash + "' id='" + imageId + "'></a>";
				$(thumbsSelector).append(aHtml);
				$(thumbsSelector + " #" + imageId).append(imageString);
			}

			let titleText;
			if (! inPopup && ithMedia.hasGpsData())
				titleText = util._t("#show-on-map");
			else
				titleText = util._t("#suggest-position-on-map");
			$("#" + imageId + " img.thumbnail-map-link").attr("title", titleText).attr("alt", util._t(".marker"));
			$("#" + imageId + " img.select-box").attr("alt", util._t("#selector"));
			let [nameForShowing, titleForShowing] = ithMedia.nameAndTitleForShowing();
			$("#" + imageId + " img.thumbnail").attr(
				"title",
				util.pathJoin([ithMedia.albumName, nameForShowing])
			).attr(
				"alt",
				util.trimExtension(ithMedia.name)
			);
			$("#" + imageId + " .media-caption .media-name").attr("title", titleForShowing);
			if (ithMedia.metadata.hasOwnProperty("description")) {
				$("#" + imageId + " .description.ellipsis").attr("title", util.stripHtmlAndReplaceEntities(ithMedia.metadata.description));
			}

			if (isAudio) {
				$(thumbsSelector + " #" + imageId + " .thumb-container").append(
					"<div class='name-in-audio'>" +
						nameForShowing +
					"</div>"
				);
			}

			if (
				! inPopup && (
					ithMedia.hasGpsData() || (
						util.isPhp() && env.options.user_may_suggest_location && env.options.request_password_email
					)
				)
			) {
				$("#media-map-link-" + id).off("click").on(
					"click",
					{singleMedia: ithMedia, album: self},
					function(ev, from) {
						// do not remove the from parameter, it is valored when the click is activated via the trigger() jquery function
						ev.stopPropagation	();
						env.mapAlbum = util.initializeMapAlbum();
						// if (! env.map.clickHistory) {
							env.mapAlbum.map.clickAlbumCacheBase = env.currentAlbum.cacheBase;
							env.mapAlbum.map.clickedSelector = "#media-map-link-" + id;
							env.mapAlbum.map.whatToGenerateTheMapFor = ithMedia.cacheBase;
						// }
						env.mapAlbum.generateMapFromSingleMedia(ithMedia, ev, from);
					}
				);
			}

			if (ithMedia.hasGpsData())
				$("#" + imageId).addClass("gps");

			if (
				! inPopup &&
				env.previousAlbum !== null &&
				env.previousAlbum.isMap() && (
					env.previousMedia === null ||
					env.previousAlbumIsAlbumWithOneMedia
				) &&
				env.previousAlbum.map.clickAlbumCacheBase === phFl.convertHashToCacheBase(window.location.hash) &&
				env.previousAlbum.map.clickedSelector === "#media-map-link-" + id &&
				env.fromEscKey ||
				env.mapRefreshType === "refresh"
			) {
				env.fromEscKey = false;
				$(env.previousAlbum.map.clickedSelector).trigger("click", ["fromTrigger"]);
			} else if (
				env.map.clickHistory && (
					util.isMapCacheBase(phFl.convertHashToCacheBase(window.location.hash)) &&
					env.map.clickAlbumCacheBase === phFl.decodeMapCacheBase(phFl.convertHashToCacheBase(window.location.hash)).clickAlbumCacheBase ||
					env.map.clickAlbumCacheBase === phFl.convertHashToCacheBase(window.location.hash)
				) &&
				env.map.clickedSelector === "#media-map-link-" + id
				// env.map.clickedSelector === "#media-map-link-" + id &&
				// env.fromEscKey ||
				// env.mapRefreshType === "refresh"
			) {
				env.fromEscKey = false;
				$.executeAfterEvents(
					["otherJsFilesLoadedEvent", "pruneclusterLoadedEvent", "mapFunctionsLoadedEvent"],
					function() {
						$(env.map.clickedSelector).trigger("click", ["fromTrigger"]);
					}
				);
			}

			let ithSelector = ithMedia.mediaSelectBoxSelector();
			util.modifyCheckedState(ithSelector, ithMedia.isSelected());

			$(ithSelector).off("click").on(
				"click",
				{ithMedia: ithMedia, clickedSelector: ithSelector},
				function(ev) {
					ev.stopPropagation();
					ev.preventDefault();
					if (Utilities.absolutelyNothingIsSelected())
						env.selectionAlbum = util.initializeSelectionAlbum();

					env.selectionAlbum.addToSelectionClickHistory(self, ev.data.clickedSelector);
					if (! ev.data.ithMedia.isSelected())
						ev.data.ithMedia.generateCaptionsForSelection(env.currentAlbum);
					ev.data.ithMedia.toggleSingleMediaSelection(env.selectionAlbum);
					util.modifyCheckedState(ithSelector, ev.data.ithMedia.isSelected());
					if (
						env.currentMedia &&
						ev.data.clickedSelector === env.currentMedia.mediaSelectBoxSelector()
					) {
						util.modifyCheckedState("#" + env.singleMediaSelectBoxId, ev.data.ithMedia.isSelected());
					}

					if (env.currentAlbum.isSelection()) {
						if (util.absolutelyNothingIsSelected()) {
							env.selectionAlbum = util.initializeSelectionAlbum();
							window.location.hash = util.upHash();
						} else {
							if (
								! env.currentMedia &&
								! env.currentAlbumIsAlbumWithOneMedia
							) {
								TopFunctions.setTitle("album", null);
							}

							let highlightedObject = util.highlightedObject();
							let prevObject = highlightedObject;
							let highlightedObjectSelector = "#" + $(highlightedObject).children().children().children(".select-box").parent().attr("id");
							if (
								highlightedObjectSelector === ev.data.clickedSelector && (
									! $(highlightedObjectSelector).parent().parent().parent().is(':first-child') ||
									env.currentAlbum.subalbums.length
								)
							) {
								prevObject = util.prevObjectForHighlighting(highlightedObject);
							}

							$(ithSelector).parent().parent().parent().remove();

							env.selectionAlbum.updateLocationHash();

							if (env.currentMedia) {
								if (ev.data.clickedSelector === env.currentMedia.mediaSelectBoxSelector()) {
									window.location.hash = util.upHash();
									return(false);
								}
							}

							$(prevObject).addClass("highlighted-object");

							if (prevObject.hasClass("thumb-and-caption-container")) {
								if (util.isPopup())
									util.scrollPopupToHighlightedThumb(prevObject);
								else
									util.scrollAlbumViewToHighlightedMedia(prevObject);
							} else {
								util.scrollAlbumViewToHighlightedSubalbum(prevObject);
							}
						}
					}

					menuF.updateMenu();
					// env.isASelectionChange = true;
				}
			);

			if (util.isPhp()) {
				// execution enters here if we are using index.php
				let imageId = ithMedia.foldersCacheBase + "--" + ithMedia.cacheBase;
				$("#album-view-" + imageId + ", #popup-" + imageId).off("auxclick").on(
					"auxclick",
					{mediaHash: util.encodeHash(self.cacheBase, ithMedia.cacheBase, ithMedia.foldersCacheBase)},
					function (ev) {
						if (ev.which === 2) {
							util.openInNewTab(ev.data.mediaHash);
							return false;
						}
					}
				);
			}
		}
		// end of auxiliary function generateSingleMediaThumbnail()

		var self = this;
		var inPopup = false;
		// TO DO: verify that the 2nd condition makes sense
		if (util.isPopup() && env.hasOwnProperty("mapAlbum") && this.isEqual(env.mapAlbum))
			inPopup = true;

		var thumbnailSize = env.options.media_thumb_size;
		var lazyClass, thumbsSelector;
		if (inPopup) {
			thumbsSelector = "#popup-images-wrapper";
			lazyClass = "lazyload-popup-media";
		} else {
			thumbsSelector = "#thumbs";
			lazyClass = "lazyload-media";
		}

		let numVisibleMedia = self.numVisibleMedia();
		let isABigTransversalAlbum = this.isABigTransversalAlbum();

		let populateMedia = true;
		if (this.isTransversal())
			populateMedia = ! isABigTransversalAlbum || env.options.show_big_virtual_folders;

		if (isABigTransversalAlbum) {
			let tooManyMediaText, isShowing = false;
			if (env.options.show_big_virtual_folders) {
				tooManyMediaText =
					"<span id='too-many-media'>" + util._t("#too-many-media") + "</span>: " + numVisibleMedia +
					", <span id='too-many-media-limit-is'>" + util._t("#too-many-media-limit-is") + "</span> " + env.options.big_virtual_folders_threshold + "</span>, " +
					"<span id='show-hide-them'>" + util._t("#hide-them") + "</span>";
			} else {
				$("#thumbs").empty();
				tooManyMediaText =
					"<span id='too-many-media'>" + util._t("#too-many-media") + "</span>: " + numVisibleMedia +
					", <span id='too-many-media-limit-is'>" + util._t("#too-many-media-limit-is") + "</span> " + env.options.big_virtual_folders_threshold + "</span>, " +
					"<span id='show-hide-them'>" + util._t("#show-them") + "</span>";
				isShowing = true;
			}
			$("#message-too-many-media").html(tooManyMediaText).show();
			if (! $("#right-and-search-menu").hasClass("expanded")) {
				$("#show-hide-them:hover").css("color", "").css("cursor", "");
			} else {
				$("#show-hide-them:hover").css("color", "inherit").css("cursor", "auto");
			}
			$("#show-hide-them").off("click").on(
				"click",
				function() {
					if (isShowing) {
						$("#loading").fadeIn(
							500,
							function() {
								$("#show-big-albums")[0].click();
							}
						);
					} else {
						$("#show-big-albums")[0].click();
					}
				}
			);
		}

		if (
			populateMedia && (
				! isABigTransversalAlbum ||
				env.options.show_big_virtual_folders
			) && (
				inPopup ||
				$("#thumbs").is(":visible") ||
				env.options.show_big_virtual_folders ||
				env.currentAlbumIsAlbumWithOneMedia
			) ||
			env.isRevertingFromHidingGeotaggedMedia ||
			env.isASelectionChange
		) {
			env.isASelectionChange = false;

			if (! env.currentAlbumIsAlbumWithOneMedia) {
				$(thumbsSelector).empty();
				$(thumbsSelector).removeClass("hidden");

				//
				// media loop
				//
				for (let i = 0; i < this.media.length; ++i) {
					generateSingleMediaThumbnail(i);
				}
			}
		}

		util.setMediaOptions();
		// if (self.numVisibleMedia() && env.currentMedia === null)
		// 	util.adaptMediaCaptionHeight(inPopup);

	 	if ($(thumbsSelector).is(":visible") || util.isPopup()) {
			if ($("#album-and-media-container").hasClass("single-media"))
				util.scrollBottomMediaToHighlightedThumb();
			else if (util.isPopup() && self.media.length) {
				var highlightedObject = $("#" + util.highlightedObject().attr("id"));
				if (! highlightedObject.length) {
					highlightedObject = $("#popup-images-wrapper").children().first();
					util.addHighlightToMediaOrSubalbum(highlightedObject);
				}
				util.scrollPopupToHighlightedThumb(highlightedObject);
			}

			util.addMediaLazyLoader(true);
		}

		// util.setFinalOptions(env.currentMedia, false);

		$.executeAfterEvent(
			"pinchSwipeFunctionsLoadedEvent",
		// $.executeAfterEvents(
		// 	[
		// 		"pinchSwipeFunctionsLoadedEvent",
		// 		"otherJsFilesLoadedEvent",
		// 		"lazyBegunEvent"
		// 	],
			function() {
				util.highlightSearchedWords();
			}
		);

		$("#loading").hide();
	};

	Album.prototype.insertRandomImage = function(randomSubAlbumCacheBase, randomSingleMedia, iVisibleSubalbum) {
		var titleName, randomMediaLink;
		var id = phFl.convertCacheBaseToId(this.visibleSubalbums()[iVisibleSubalbum].cacheBase);
		var isAudio = randomSingleMedia.isAudio();
		var mediaSrc, mediaSrcWidth, mediaSrcHeight;
		if (! isAudio) {
			mediaSrc = randomSingleMedia.chooseSubalbumThumbnail(env.options.album_thumb_size);

			let mediaSrcIsSquare = (mediaSrc.substr(-7, 2) === "sq");
			let mediaSrcSize = mediaSrc.substring(mediaSrc.lastIndexOf(env.options.cache_folder_separator) + 1, mediaSrc.lastIndexOf("."));
			if (mediaSrcIsSquare)
				mediaSrcSize = mediaSrcSize.substr(0, 3);
			mediaSrcHeight = parseInt(mediaSrcSize);
			mediaSrcWidth = parseInt(mediaSrcSize);
			if (! mediaSrcIsSquare) {
				if (randomSingleMedia.width() > randomSingleMedia.height())
					mediaSrcHeight = parseInt(mediaSrcSize / randomSingleMedia.width() * randomSingleMedia.height());
				else
					mediaSrcWidth = parseInt(mediaSrcSize / randomSingleMedia.height() * randomSingleMedia.width());
			}
		}

		$("#downloading-media").hide();

		var collectedAlbumCacheBase, collectionCacheBase;
		var components = phFl.returnDecodedHash(randomSubAlbumCacheBase);
		collectedAlbumCacheBase = components.collectedAlbumCacheBase;
		collectionCacheBase = components.collectionCacheBase;

		if (this.isSearch() || this.isSelection()) {
			let [name, fake_title] = randomSingleMedia.nameAndTitleForShowing();
			titleName = util.pathJoin([randomSingleMedia.albumName, name]);
			randomMediaLink = util.encodeHash(
				randomSingleMedia.foldersCacheBase,
				randomSingleMedia.cacheBase,
				randomSingleMedia.foldersCacheBase,
				randomSubAlbumCacheBase,
				this.cacheBase
			);
		} else if (this.isByDate()) {
			titleName = util.pathJoin([randomSingleMedia.dayAlbum, randomSingleMedia.name]);
			randomMediaLink = util.encodeHash(
				randomSingleMedia.dayAlbumCacheBase,
				randomSingleMedia.cacheBase,
				randomSingleMedia.foldersCacheBase,
				randomSubAlbumCacheBase,
				this.cacheBase
			);
		} else if (this.isByGps()) {
			let humanGeonames = util.pathJoin(
				[
					env.options.by_gps_string,
					randomSingleMedia.geoname.country_name,
					randomSingleMedia.geoname.region_name,
					randomSingleMedia.geoname.alt_place_name
				]
			);
			titleName = util.pathJoin([humanGeonames, randomSingleMedia.name]);
			randomMediaLink = util.encodeHash(
				randomSingleMedia.gpsAlbumCacheBase,
				randomSingleMedia.cacheBase,
				randomSingleMedia.foldersCacheBase,
				randomSubAlbumCacheBase,
				this.cacheBase
			);
		} else {
			let [name, fake_title] = randomSingleMedia.nameAndTitleForShowing();
			titleName = util.pathJoin([randomSingleMedia.albumName, name]);
			randomMediaLink = util.encodeHash(
				randomSingleMedia.foldersCacheBase,
				randomSingleMedia.cacheBase,
				randomSingleMedia.foldersCacheBase,
				env.hashComponents.collectedAlbumCacheBase,
				env.hashComponents.collectionCacheBase
			);
		}

		titleName = titleName.substr(titleName.indexOf("/") + 1);
		var goTo = util._t(".go-to") + " " + titleName;
		$("#" + id + " .album-button a.random-media-link").attr("href", randomMediaLink);
		$("#" + id + " img.album-button-random-media-link").attr("title", goTo).attr("alt", util._t(".arrow"));
		if ($("#" + id + " img.thumbnail").length) {
			// replacing is needed in order to reactivate the lazy loader
			$("#" + id + " img.thumbnail").replaceWith($("#" + id + " img.thumbnail")[0].outerHTML);
			$("#" + id + " img.thumbnail").attr("title", titleName).attr("alt", titleName);
			if (isAudio) {
				$("#" + id + " img.thumbnail").attr("data-src", "img/audio.png");
				$("#" + id + " img.thumbnail").attr("width", "500");
				$("#" + id + " img.thumbnail").attr("height", "500");

				$("#" + id + " img.thumbnail").after(
					"<div class='name-in-audio' style='width: " + (env.options.album_thumb_size - 10 - 10 - 3 - 3) + "px;'>" +
						randomSingleMedia.nameAndTitleForShowing()[0] +
					"</div>"
				);
			} else {
				$("#" + id + " img.thumbnail").attr("data-src", encodeURI(mediaSrc));
				$("#" + id + " img.thumbnail").attr("width", mediaSrcWidth);
				$("#" + id + " img.thumbnail").attr("height", mediaSrcHeight);
			}
		}

		if (util.geotaggedContentIsHidden()) {
			$("#" + id + " img.thumbnail").attr("src", "img/image-placeholder.jpg");
		}

		$(
			function() {
				var threshold = env.options.album_thumb_size;
				if (env.options.save_data)
					threshold = 0;
				$("img.lazyload-album-" + id).Lazy(
					{
						afterLoad: function() {
							util.conditionalLoadMatomoTracking();
							util.conditionalLoadOthersJsFiles();
						},
						chainable: false,
						threshold: threshold,
						// bind: "event",
						scrollDirection: "vertical",
						appendScroll: $(window)
					}
				);
			}
		);
	};


	Album.prototype.pickRandomMediaAndInsertIt = function(iVisibleSubalbum) {
		var self = this;
		var promise = self.pickRandomMedia(
			iVisibleSubalbum,
			function error() {
				// executions shoudn't arrive here, if it arrives it's because of some error
				console.trace();
			}
		);
		promise.then(
			function([randomAlbum, indexVisible]) {
				self.insertRandomImage(randomAlbum.cacheBase, randomAlbum.visibleMedia()[indexVisible], iVisibleSubalbum);
			},
			function(album) {
				console.trace();
			}
		);
	};

	Album.prototype.showSubalbums = function(forcePopulate) {
		function generateSubalbumThumbnail(i) {
			let iSubalbum = i;
			let ithSubalbum = self.subalbums[iSubalbum];
			let id = phFl.convertCacheBaseToId(ithSubalbum.cacheBase);
			let nameHtml = "";

			if (self.isSearch()) {
				// if (! ithSubalbum.hasOwnProperty("captionsForSearch"))
				// 	ithSubalbum.generateCaptionsForSearch();
				if (ithSubalbum.hasOwnProperty("captionsForSearch") && ithSubalbum.captionsForSearch[0])
					nameHtml = ithSubalbum.captionsForSearch.join(env.br);
			} else if (self.isSelection()) {
				// if (! ithSubalbum.hasOwnProperty("captionsForSelection"))
				// 	ithSubalbum.generateCaptionsForSelection();
				if (ithSubalbum.hasOwnProperty("captionsForSelection") && ithSubalbum.captionsForSelection[0])
					nameHtml = ithSubalbum.captionsForSelection.join(env.br);
			}

			if (! nameHtml)
				nameHtml = ithSubalbum.nameForShowing(self, true, true);
			if (! nameHtml)
				nameHtml = "<span class='italic gray'>(" + util._t("#root-album") + ")</span>";

			let captionId = "album-caption-" + id;
			let captionHtml =
				"<div class='album-caption' id='" + captionId + "'>";
			captionHtml +=
					"<div class='album-name'>" + nameHtml + "</div>";

			if (ithSubalbum.hasOwnProperty("description")) {
				captionHtml +=
					"<div class='album-description'>" +
						"<div class='description ellipsis'>" + util.stripHtmlAndReplaceEntities(ithSubalbum.description) + "</div>" +
					"</div>";
			}

			if (ithSubalbum.hasOwnProperty("tags") && ithSubalbum.tags.length) {
				captionHtml +=
					"<div class='album-tags'>" +
						"<span class='tags'>" + util._t("#tags") + ": <span class='tag'>" + ithSubalbum.tags.map(tag => util.addTagLink(tag)).join("</span>, <span class='tag'>") + "</span></span>" +
					"</div>";
			}

			captionHtml +=
					"<div class='album-bottom'>" +
						"<div class='album-caption-count'>";
			captionHtml += util.specifyNumberOfImagesAudiosVideos(
				ithSubalbum.numsMediaInSubTree,
				ithSubalbum.nonGeotagged.numsMediaInSubTree
			);

			captionHtml +=
						"</div>" +
						"<div class='sort-data album-sort-data'></div>" +
					"</div>";

			captionHtml +=
				"</div>";

			let captionObject = $(captionHtml);

			let positionHtml = "";
			let folderMapTitleWithoutHtmlTags;
			if (ithSubalbum.numPositionsInTree && ! env.options.save_data) {
				folderMapTitleWithoutHtmlTags = self.folderMapTitle(ithSubalbum, nameHtml).replace(/<br \/>/gm, " ").replace(/<[^>]*>?/gm, "");
				positionHtml =
					"<a id='subalbum-map-link-" + id + "' >" +
						"<img " +
							"class='thumbnail-map-link gps' " +
							"src='img/ic_place_white_24dp_2x.png' " +
							"width='48' " +
							"height='42'" +
						"/>" +
					"</a>";
			}

			// a dot could be present in a cache base, making $("#" + cacheBase) fail, beware...
			let subfolderHash;
			if (self.isSearch() || self.isSelection() || self.isMap()) {
				subfolderHash = util.encodeHash(ithSubalbum.cacheBase, null, null, ithSubalbum.cacheBase, self.cacheBase);
			} else {
				if (typeof env.hashComponents.collectionCacheBase !== "undefined" && env.hashComponents.collectionCacheBase !== null)
					subfolderHash = util.encodeHash(ithSubalbum.cacheBase, null, null, env.hashComponents.collectedAlbumCacheBase, env.hashComponents.collectionCacheBase);
				else
					subfolderHash = util.encodeHash(ithSubalbum.cacheBase, null);
			}

			let gpsClass = "";
			if (
				// ithSubalbum.numPositionsInTree &&
				env.options.expose_image_positions &&
				! ithSubalbum.nonGeotagged.numsMediaInSubTree.imagesAudiosVideosTotal()
			)
				gpsClass = " class='all-gps'";

			let aHrefHtml = "<a href='" + subfolderHash + "'" + gpsClass + "></a>";
			let aHrefObject = $(aHrefHtml);
			let albumButtonAndCaptionHtml =
				"<div id='" + id + "' class='album-button-and-caption'></div>";
			let albumButtonAndCaptionObject = $(albumButtonAndCaptionHtml);

			let selectBoxHtml = "";
			if (env.options.selectable_albums)
				selectBoxHtml =
					"<a id='" +
						ithSubalbum.albumSelectBoxSelector().substr(1) +
						"'>" +
						"<img " +
							"class='select-box' " +
							"style='display: none;'" +
						">" +
					"</a>";

			let imageObject = $(
				"<div class='album-button'>" +
					selectBoxHtml +
					positionHtml +
					"<a class='random-media-link' href=''>" +
						"<img src='img/link-arrow.png' class='album-button-random-media-link'>" +
					"</a>" +
					"<span class='helper'></span>" +
					"<img src='img/image-placeholder.jpg' class='thumbnail lazyload-album-" + id + "'>" +
				"</div>"
			);
			albumButtonAndCaptionObject.append(imageObject);
			albumButtonAndCaptionObject.append(captionObject);
			aHrefObject.append(albumButtonAndCaptionObject);

			objects[iSubalbum] = {
				aHrefObject: aHrefObject,
				ithSubalbum: ithSubalbum,
				id: id,
				captionId: captionId,
				folderMapTitleWithoutHtmlTags: folderMapTitleWithoutHtmlTags,
				subfolderHash: subfolderHash
				// from: from
			};
		}
		// end of auxiliary function generateSubalbumThumbnail()

		var self = this;

		if (env.fromEscKey && env.firstEscKey) {
			// respect the existing mediaLink (you cannot do it more than once)
			env.firstEscKey = false;
		} else {
			env.firstEscKey = true;
		}

		var geotaggedContentIsHidden = util.geotaggedContentIsHidden();

		// reset mediaLink
		if (self.numVisibleMedia())
			env.mediaLink = util.encodeHash(self.cacheBase, self.media[0].cacheBase, self.media[0].foldersCacheBase, env.hashComponents.collectedAlbumCacheBase, env.hashComponents.collectionCacheBase);
		else
			env.mediaLink = env.hashBeginning + self.cacheBase;

		// insert into DOM
		if (! self.visibleSubalbums().length)
			$("#subalbums").addClass("hidden");
			// $("#subalbums").hide();

		let populateSubalbums = (
			forcePopulate ||
			env.albumInSubalbumDiv === null ||
			self === null ||
			(
				env.albumInSubalbumDiv !== self ||
				env.isFromAuthForm ||
				env.isRevertingFromHidingGeotaggedMedia ||
				env.isASaveDataChange ||
				! env.options.save_data &&
				env.previousAlbum !== null &&
				env.previousAlbum.isMap() &&
				(
					env.previousMedia === null ||
					env.previousAlbumIsAlbumWithOneMedia
				) &&
				env.previousAlbum.hasOwnProperty("map.clickedSelector")
			) &&
			self.visibleSubalbums().length ||
			env.isASelectionChange
		);

		env.isASelectionChange = false;

		// if (env.isRevertingFromHidingGeotaggedMedia)
		// 	env.isRevertingFromHidingGeotaggedMedia = false;

		if (env.isASaveDataChange)
			env.isASaveDataChange = false;

		let objects = [];
		if (populateSubalbums) {
			$("#subalbums").empty();
			// $("#subalbums").insertBefore("#message-too-many-media");

			//
			// subalbums loop
			//
			for (let i = 0; i < self.subalbums.length; i ++) {
				generateSubalbumThumbnail(i);
			}

			// perform the last operations with each subalbum
			for (let i = 0; i < self.subalbums.length; i ++) {
				let iSubalbum = i;
				let aHrefObject = objects[iSubalbum].aHrefObject;
				let ithSubalbum = objects[iSubalbum].ithSubalbum;
				let id = objects[iSubalbum].id;
				let captionId = objects[iSubalbum].captionId;
				let folderMapTitleWithoutHtmlTags = objects[iSubalbum].folderMapTitleWithoutHtmlTags;
				let subfolderHash = objects[iSubalbum].subfolderHash;
				// let from = objects[iSubalbum].from;

				$("#subalbums").append(aHrefObject);

				if (ithSubalbum.numPositionsInTree && ! env.options.save_data) {
					$("#subalbum-map-link-" + id + " img.thumbnail-map-link").attr("title", folderMapTitleWithoutHtmlTags);
					$("#subalbum-map-link-" + id + " img.thumbnail-map-link").attr("alt", util._t(".marker"));
				}

				let albumSelectBoxSelector = ithSubalbum.albumSelectBoxSelector();
				if (env.options.selectable_albums) {
					util.modifyCheckedState(albumSelectBoxSelector, ithSubalbum.isSelected());
					$(albumSelectBoxSelector + " img.select-box").attr("alt", util._t("#selector"));
				}

				if (ithSubalbum.hasOwnProperty("description"))
					$("#" + captionId + " .description").attr("title", util.stripHtmlAndReplaceEntities(ithSubalbum.description));

				if (ithSubalbum.hasOwnProperty("numPositionsInTree") && ! env.options.save_data && ithSubalbum.numPositionsInTree) {
					$("#subalbum-map-link-" + id).off("click").on(
						"click",
						{
							ithSubalbum: ithSubalbum,
							singleMedia: null
						},
						function(ev, from) {
							// do not remove the from parameter, it is valored when the click is activated via the trigger() jquery function
							ev.preventDefault();

							env.mapAlbum = util.initializeMapAlbum();
							// if (! env.mapAlbum.map.clickHistory) {
							env.mapAlbum.map.clickAlbumCacheBase = env.currentAlbum.cacheBase;
							env.mapAlbum.map.clickedSelector = "#subalbum-map-link-" + id;
							let splitted = ev.data.ithSubalbum.cacheBase.split(env.options.cache_folder_separator);
							env.mapAlbum.map.whatToGenerateTheMapFor = splitted.at(-1);
							// }
							env.mapAlbum.generateMapFromSubalbum(ev.data.ithSubalbum, ev, from);
						}
					);
				}

				if (
					! env.options.save_data &&
					env.previousAlbum !== null &&
					env.previousAlbum.isMap() &&
					(
						env.previousMedia === null ||
						env.previousAlbumIsAlbumWithOneMedia
					) &&
					env.previousAlbum.map.clickAlbumCacheBase === phFl.convertHashToCacheBase(window.location.hash) &&
					env.previousAlbum.map.clickedSelector === "#subalbum-map-link-" + id &&
					env.fromEscKey ||
					env.mapRefreshType === "refresh"
				) {
					env.fromEscKey = false;
					$(env.previousAlbum.map.clickedSelector).trigger("click", ["fromTrigger"]);
				} else if (
					env.map.clickHistory && (
						util.isMapCacheBase(phFl.convertHashToCacheBase(window.location.hash)) &&
						env.map.clickAlbumCacheBase === phFl.decodeMapCacheBase(phFl.convertHashToCacheBase(window.location.hash)).clickAlbumCacheBase ||
						env.map.clickAlbumCacheBase === phFl.convertHashToCacheBase(window.location.hash)
					) &&
					env.map.clickedSelector === "#subalbum-map-link-" + id
					// env.map.clickedSelector === "#subalbum-map-link-" + id &&
					// env.fromEscKey ||
					// env.mapRefreshType === "refresh"
				) {
					env.fromEscKey = false;
					$.executeAfterEvents(
						["otherJsFilesLoadedEvent", "pruneclusterLoadedEvent", "mapFunctionsLoadedEvent"],
						function() {
							$(env.map.clickedSelector).trigger("click", ["fromTrigger"]);
						}
					);
				}

				if (util.isPhp()) {
					// execution enters here if we are using index.php
					$("#" + id).off("auxclick").on(
						"auxclick",
						// {subfolderHash: subfolderHash},
						function (ev) {
							if (ev.which === 2) {
								util.openInNewTab(subfolderHash);
								return false;
							}
						}
					);
				}

				if (env.options.selectable_albums) {
					$(albumSelectBoxSelector + " .select-box").show();
					$(albumSelectBoxSelector).off("click").on(
						"click",
						{ithSubalbum: ithSubalbum},
						function(ev) {
							ev.stopPropagation();
							ev.preventDefault();

							if (env.selectingSelectors.indexOf(albumSelectBoxSelector) !== -1)
								return;

							env.selectingSelectors.push(albumSelectBoxSelector);

							if (Utilities.absolutelyNothingIsSelected())
								env.selectionAlbum = util.initializeSelectionAlbum();

							env.selectionAlbum.addToSelectionClickHistory(env.currentAlbum, albumSelectBoxSelector);
							let toAlbumPromise = ev.data.ithSubalbum.toAlbum(util.noOp, {getMedia: false, getPositions: false});
							toAlbumPromise.then(
								function(ithAlbum) {
									if (! ithAlbum.isSelected())
										ithAlbum.generateCaptionsForSelection();

									let subalbumPromise = ev.data.ithSubalbum.toggleSubalbumSelection(env.currentAlbum, env.selectionAlbum);
									subalbumPromise.then(
										function() {
											env.selectingSelectors = env.selectingSelectors.filter(selector => selector !== albumSelectBoxSelector);

											util.modifyCheckedState(albumSelectBoxSelector, ithAlbum.isSelected());

											if (env.currentAlbum.isSelection() && ! ithAlbum.isSelected()) {
												env.selectionAlbum.updateSelectionCacheBase();
												env.albumInSubalbumDiv = null;

												if (util.absolutelyNothingIsSelected()) {
													env.selectionAlbum = util.initializeSelectionAlbum();
													window.location.hash = util.upHash();
												} else {
													let highlightedObject = util.highlightedObject();

													if (highlightedObject.children(".album-button").children(albumSelectBoxSelector).length > 0) {
														// the clicked object is the highlighted one:
														// before removing it, highlight the previous or next object
														let nextObject;
														if (highlightedObject.parent().parent().children().length === 1 && env.currentAlbum.numVisibleMedia()) {
															nextObject = util.nextObjectForHighlighting(highlightedObject);
															util.addHighlightToMediaOrSubalbum(nextObject);
															util.scrollAlbumViewToHighlightedMedia(nextObject);
														} else {
															if (highlightedObject.parent().is(":last-child")) {
																nextObject = util.prevObjectForHighlighting(highlightedObject);
															} else {
																nextObject = util.nextObjectForHighlighting(highlightedObject);
															}
															util.addHighlightToMediaOrSubalbum(nextObject);
															util.scrollAlbumViewToHighlightedSubalbum(nextObject);
														}
													}

													$(albumSelectBoxSelector).parent().parent().parent().remove();
													tF.setTitle("album", null);

													env.currentAlbum.updateLocationHash();

													let aSubalbumWasHighlighted = util.aSubalbumIsHighlighted();
													let highlightedObjectSelector = util.highlightedObjectSelector();
													// env.currentAlbum.showSubalbums(true);
													if (aSubalbumWasHighlighted)
														util.highlighObject(highlightedObjectSelector, aSubalbumWasHighlighted);
													util.adaptSubalbumCaptionHeight(); // in click for selecting subalbums
													env.isASelectionChange = true;
												}
											}

											menuF.updateMenu();
										}
									);
								}
							);
						}
					);
				}

				let iVisibleSubalbum = self.visibleSubalbums().findIndex(subalbum => subalbum.isEqual(self.subalbums[iSubalbum]));
				if (iVisibleSubalbum !== -1) {
					if (
						ithSubalbum.randomMedia === undefined ||
						// ! env.options.save_data ||
						geotaggedContentIsHidden &&
						ithSubalbum.visibleNumsMediaInSubTree().imagesAudiosVideosTotal() &&
						ithSubalbum.randomMedia.every(singleMedia => singleMedia.hasGpsData())
					) {
						self.pickRandomMediaAndInsertIt(iVisibleSubalbum);
					} else {
						let randomSingleMedia;
						while (true) {
							randomSingleMedia = ithSubalbum.randomMedia[parseInt(Math.floor(Math.random() * ithSubalbum.randomMedia.length))];
							if (
								! geotaggedContentIsHidden ||
								ithSubalbum.visibleNumsMediaInSubTree().imagesAudiosVideosTotal() ||
								! randomSingleMedia.hasGpsData()
							)
								break;
						}

						var randomMediaFromCache = env.cache.getSingleMedia(randomSingleMedia);
						if (randomMediaFromCache !== false)
							randomSingleMedia = randomMediaFromCache;
						self.insertRandomImage(randomSingleMedia.foldersCacheBase, randomSingleMedia, iVisibleSubalbum);
					}
				}

				if (util.isPhp()) {
					// execution enters here if we are using index.php
					let subalbumId = phFl.convertCacheBaseToId(ithSubalbum.cacheBase);
					$("#" + subalbumId).parent().off("auxclick").on(
						"auxclick",
						{subalbumHash: subfolderHash},
						function (ev) {
							if (ev.which === 2) {
								util.openInNewTab(ev.data.subalbumHash);
								return false;
							}
						}
					);
				}
			}

			if (populateSubalbums)
				env.albumInSubalbumDiv = self;
			$("#loading").hide();

			if (self.visibleSubalbums().length) {
				$("#album-and-media-container").removeClass("single-media");
				$("#subalbums").removeClass("hidden");
				// $("#subalbums").show();
				$("#album-view").removeAttr("height");
			}

			util.setSubalbumsOptions();

			util.highlightSearchedWords();
		}
	};

	Album.prototype.numsVisibleMediaInAlbumForDownload = Album.numsVisibleMediaInAlbumForDownload;
}());
