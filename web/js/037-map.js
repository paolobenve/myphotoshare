(function() {

	var util = new Utilities();
	var menuF = new MenuFunctions();

	/* constructor */
	function MapFunctions() {
	}

	MapFunctions.prototype.setLastMapCenterAndZoom = function(center, zoom) {
		env.lastMapPositionAndZoom = {center: center, zoom: zoom};
		menuF.setCookie("centerAndZoom", JSON.stringify(env.lastMapPositionAndZoom));
	};

	MapFunctions.prototype.getLastMapCenterAndZoom = function() {
		var centerAndZoomCookie = menuF.getCookie("centerAndZoom");
		if (centerAndZoomCookie)
			env.lastMapPositionAndZoom = JSON.parse(centerAndZoomCookie);
		else
			env.lastMapPositionAndZoom = {
				center: {lat: 0, lng: 0},
				zoom: 10
			};
	};

	MapFunctions.setKeepShowingGeolocationSuggestTextState = function(state) {
		if (typeof env !== "undefined")
			env.keepShowingGeolocationSuggestText = state;
		menuF.setBooleanCookie("showGeolocationSuggestText", state);
	};

	MapFunctions.getKeepShowingGeolocationSuggestTextState = function() {
		var showLocationSuggestTextCookie = menuF.getBooleanCookie("showGeolocationSuggestText");
		if (showLocationSuggestTextCookie !== null) {
			MapFunctions.setKeepShowingGeolocationSuggestTextState(showLocationSuggestTextCookie);
		} else {
			if (typeof env !== "undefined")
				env.keepShowingGeolocationSuggestText = true;
			MapFunctions.setKeepShowingGeolocationSuggestTextState(true);
		}
	};

	MapFunctions.calculatePopupSizes = function() {
		var scrollerSize = util.windowVerticalScrollbarWidth();

		// how much space is available horizontally for the thumbnails?
		env.maxWidthForPopupContent = parseInt($("#mapdiv").width() * 0.85);

		// the space for the images: remove the margin
		env.maxWidthForImagesInPopup = env.maxWidthForPopupContent - 15 - 15 - scrollerSize;

		// vertical popup size
		env.maxHeightForPopupContent = parseInt($("#mapdiv").height() * 0.85);
	};

	MapFunctions.setPopupPosition = function() {
		if (
			env.available_map_popup_positions.every(
				function(orientation) {
					return ! $(".media-popup.leaflet-popup").hasClass(orientation);
				}
			)
		) {
			$(".media-popup.leaflet-popup").addClass(env.options.default_map_popup_position);
		}
	};

	MapFunctions.prototype.panMap = function() {
		// pan the map so that the popup is inside the map
		var popupPosition = env.mymap.latLngToContainerPoint(env.popup.getLatLng());
		var popupWidth = $(".media-popup .leaflet-popup-content-wrapper").width();
		var popupHeight = $(".media-popup .leaflet-popup-content-wrapper").height();
		var mapWidth = $("#mapdiv").width();
		var mapHeight = $("#mapdiv").height();
		var panX = 0, panY = 0;
		if (popupPosition.x + popupWidth > mapWidth) {
			panX = popupWidth - (mapWidth - popupPosition.x);
		} else if (popupPosition.x < 0)
			panX = popupPosition.x - 50;

		if (popupPosition.y + popupHeight > mapHeight) {
			panY = popupHeight - (mapHeight - popupPosition.y) + 50;
		} else if (popupPosition.y < 0)
			panY = popupPosition.y - 20;
		env.mymap.panBy([panX, panY], {animate: false});
	};

	MapFunctions.resizeMap = function() {
		env.windowWidth = $(window).innerWidth();
		env.windowHeight = $(window).innerHeight();

		$("#my-modal.modal").css("display", "block");
		if (env.isAnyMobile) {
			$("#my-modal .modal-content").css("width", (env.windowWidth - 12).toString() + "px");
			$("#my-modal .modal-content").css("height", (env.windowHeight - 12).toString() + "px");
			$("#my-modal .modal-content").css("padding", "5px");
			$("#my-modal.modal").css("top", "0");
			$("#my-modal.modal").css("padding-top", "0");
			$("#my-modal.modal-close").css("top", "22px");
			$("#my-modal.modal-close").css("right", "22px");
		} else {
			$("#my-modal .modal-content").css("width", (env.windowWidth - 55).toString() + "px");
			$("#my-modal .modal-content").css("height", (env.windowHeight - 60).toString() + "px");
			$("#my-modal .modal-content").css("padding", "");
			$("#my-modal.modal").css("top", "");
			$("#my-modal.modal").css("padding-top", "");
			$("#my-modal.modal-close").css("top", "");
			$("#my-modal.modal-close").css("right", "");
		}
	};

	MapFunctions.prototype.addPopupMover = function() {
		// add the popup mover
		$(".popup-mover").remove();
		$(".media-popup .leaflet-popup-close-button").after('<a id="popup-mover" class="popup-mover"></a>');
		// add the corresponding listener
		$(".popup-mover").off("click").on(
			"click",
			function() {
				var currentIndex = env.available_map_popup_positions.findIndex(
					function(orientation) {
						return $(".media-popup.leaflet-popup").hasClass(orientation);
					}
				);
				var nextIndex = currentIndex + 1;
				if (currentIndex === env.available_map_popup_positions.length - 1)
					nextIndex = 0;
				$(".media-popup.leaflet-popup").
					removeClass(env.available_map_popup_positions[currentIndex]).
					addClass(env.available_map_popup_positions[nextIndex]);
				return false;
			}
		);

		$(".media-popup .leaflet-popup-content").css("max-height", parseInt(env.windowHeight * 0.8)).css("max-width", parseInt(env.windowWidth * 0.8));
		MapFunctions.setPopupPosition();
	};

	MapFunctions.prototype.calculatePopupSizes = MapFunctions.calculatePopupSizes;

	window.MapFunctions = MapFunctions;

	L.NumberedDivIcon = L.Icon.extend({
		options: {
			// EDIT THIS TO POINT TO THE FILE AT http://www.charliecroom.com/marker_hole.png (or your own marker)
			iconUrl: 'css/images/marker_hole.png',
			number: '',
			shadowUrl: null,
			iconSize: new L.Point(25, 41),
			iconAnchor: new L.Point(13, 41),
			popupAnchor: new L.Point(0, -33),
			/*
			iconAnchor: (Point)
			popupAnchor: (Point)
			*/
			className: 'leaflet-div-icon'
		},

		createIcon: function () {
			var div = document.createElement('div');
			var img = this._createImg(this.options.iconUrl);
			var numdiv = document.createElement('div');
			numdiv.setAttribute ( "class", "number" );
			numdiv.innerHTML = this.options.number || '';
			div.appendChild ( img );
			div.appendChild ( numdiv );
			this._setIconStyles(div, 'icon');
			return div;
		},

		//you could change this to add a shadow like in the normal marker if you really wanted
		createShadow: function () {
			return null;
		}
	});

	MapFunctions.encodeMapClickHistory = function(mapClickHistory) {
		function convertLatLngToString(latlng) {
			let lat = parseInt(latlng.lat * multiplier).toString() + "N";
			if (latlng.lat < 0)
				lat = Math.abs(parseInt(latlng.lat * multiplier)).toString() + "S";

			let lng = parseInt(latlng.lng * multiplier).toString() + "E";
			if (latlng.lng < 0)
				lng = Math.abs(parseInt(latlng.lng * multiplier)).toString() + "W";

			return {lat: lat, lng: lng};
		}

		// end of nested functions

		let trailerElements = [];
		let multiplier;


		mapClickHistory.forEach(
			function(mapClickHistoryElement) {
				const radiantsOnePixel = Math.abs(2 * Math.PI * Math.cos(mapClickHistoryElement.center.lat) / (Math.pow(2, (mapClickHistoryElement.zoom + 8))));
				const numberOfDecimals = Math.ceil(Math.abs(Math.log10(radiantsOnePixel))) + 1;
				multiplier = Math.pow(10, numberOfDecimals);  // in order to transform decimals into integers

				const center = convertLatLngToString(mapClickHistoryElement.center);
				const latlng = convertLatLngToString(mapClickHistoryElement.latlng);
				trailerElements.push(
					(mapClickHistoryElement.ctrlKey ? "1" : "0") +
					(mapClickHistoryElement.shiftKey ? "1" : "0") +
					mapClickHistoryElement.zoom.toString().padStart(2, '0') +
					numberOfDecimals.toString().padStart(2, '0') +
					center.lat +
					center.lng +
					latlng.lat +
					latlng.lng
				);
			}
		);
		let cacheBaseTrailer = trailerElements.join(",");

		return cacheBaseTrailer;
	};


	MapFunctions.prototype.setKeepShowingGeolocationSuggestTextState = MapFunctions.setKeepShowingGeolocationSuggestTextState;
	MapFunctions.prototype.setPopupPosition = MapFunctions.setPopupPosition;
	MapFunctions.prototype.resizeMap = MapFunctions.resizeMap;

	$.triggerEvent("mapFunctionsLoadedEvent");
}());
//# sourceURL=037-map.js
