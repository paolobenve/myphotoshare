$(document).ready(function() {

	/*
	 * None of the globals here polutes the global scope, as everything
	 * is enclosed in an anonymous function.
	 *
	 */

	var env = new Env();
	window.env = env;

	var phFl = new PhotoFloat();
	var util = new Utilities();
	var menuF = new MenuFunctions();
	// var tF = new TopFunctions();

	if ($(".media-box#center").length) {
		// triplicate the #mediaview content in order to swipe the media
		var titleContent = $("#album-view").clone().children().first();
		$(".media-box#center").prepend(titleContent[0].outerHTML);
		util.mediaBoxGenerator('left');
		util.mediaBoxGenerator('right');
	}

	$(window).hashchange(
		function() {
			if (env.isASelectionChange) {
				env.isASelectionChange = false;
				return;
			}

			$(
				function() {
					// activate lazy loader
					if (util.isPopup())
						$('#popup-images-wrapper').trigger("scroll");
					else if (env.currentMedia !== null)
						$("#album-view").trigger("scroll");
					else
						$(window).trigger("scroll");

				}
			);
			$("#auth-text").hide();
			// $("#thumbs").show();
			$("#subalbums").removeClass("hidden");
			$("#album-view, #media-view, #my-modal").css("opacity", "");

			// $("#album-view").removeClass("hidden");
			$("link[rel=image_src]").remove();
			$("link[rel=audio_src]").remove();
			$("link[rel=video_src]").remove();
			// $("#right-and-search-menu").removeClass("expanded");

			if (util.isMap() || util.isPopup()) {
				// we are in a map: close it
				$('.modal-close')[0].click();
			}

			var optionsPromise = phFl.getOptions();
			optionsPromise.then(
				function() {
					phFl.decodeHash();

					util.translate();
					$("#loading").show();

					if (! util.isSearchHash()) {
						// restore current album search flag to its default value
						env.search.currentAlbumOnly = true;
						menuF.setBooleanCookie("searchCurrentAlbum", env.search.currentAlbumOnly);
					}

					if (util.isPhp() && typeof postData !== "undefined" && postData !== null) {
						util.readPostData();
					}

					// parseHashAndReturnAlbumAndMediaIndex returns an array of 2 elements:
					// - the requested album
					// - the requested media index (if applicable)
					var hashPromise = phFl.parseHashAndReturnAlbumAndMediaIndex();
					hashPromise.then(
						function([album, mediaIndex]) {
							if (env.isABrowsingModeChangeFromMouseClick || env.isASaveDataChange) {
								if (env.isABrowsingModeChangeFromMouseClick)
									env.isABrowsingModeChangeFromMouseClick = false;
								menuF.openRightMenu();
							} else if (album.isSearch() && ! album.numsMediaInSubTree.imagesAudiosVideosTotal()) {
								menuF.openSearchMenu(album);
							} else {
								menuF.closeMenu();
							}
							album.prepareForShowing(mediaIndex);
						},
						function(album) {
							menuF.errorOpeningUnprotectedJsonFiles(album);
						}
					);
				},
				function() {
					$("#album-view").fadeOut(200);
					$("#media-view").fadeOut(200);
					$("#album-view").stop().fadeIn(3500);
					$("#media-view").stop().fadeIn(3500);
					$("#error-options-file").stop().fadeIn(200);
					$("#error-options-file, #error-overlay, #auth-text").fadeOut(2500);
				}
			);
		}
	);

	// Execution starts here
	// An array of promises is used in order to prepare for new formats (avif, jxl)
	// they aren't supported yet because Pillow doesn't support them
	var formatPromises = [];
	var webpPromise = new Promise(
		function(resolve_webpPromise) {
			Modernizr.on(
				"webp",
				function(webpSupported) {
					if (webpSupported) {
						// format supported
						$("html").addClass("webp");
					} else {
						// format not supported
						$("html").addClass("not-webp");
					}
					resolve_webpPromise();
				}
			);
		}
	);
	formatPromises.push(webpPromise);

	Promise.all(formatPromises).then(
		function() {
			$(window).hashchange();
		}
	);

	function lazyLoadImagesInPage() {
		$("img.lazyload-page#up-button").Lazy(
			{
				afterLoad: function() {
					util.setUpButtonVisibility();
				}
			}
		);

		var count = 0;
		var selector = "img.lazyload-page#prev, img.lazyload-page#next";
		// with audios, loading select-box like the other doesn't arrive to trigger to afterLoad function; why?!?
		if (env.currentMedia && env.currentMedia.isAudio())
			$("img.lazyload-page#select-box").Lazy();
		else
			selector += ", img.lazyload-page#select-box, img.lazyload-page#pinch-in, img.lazyload-page#pinch-out";
		if (util.isPhp())
			selector += ", img.lazyload-page#show-url";
		var numSelectors = selector.split(",").length;
		$(selector).Lazy(
			{
				afterLoad: function() {
					count ++;
					if (count === numSelectors) {
						$(document).ready(
							function() {
								$.triggerEvent("imagesInPageLoadedEvent");
							}
						);
					}
				}
			}
		);
		$("img.lazyload-page:not(#prev):not(#next):not(#pinch-in):not(#pinch-out):not(#show-url):not(#select-box)").Lazy();
	}

	$.executeAfterEvent(
		"readyToLoadOtherJsFilesEvent",
		function() {
			lazyLoadImagesInPage();
			$.executeAfterEvent(
				"imagesInPageLoadedEvent",
				function() {
					util.loadOtherJsCssFiles();
				}
			);
		}
	);

	$.executeAfterEvent(
		"optionsLoadedEvent",
		function() {
			if (env.options.matomo_server && env.options.matomo_id) {
				// matomo analytics: load the script
				$.executeAfterEvent(
					"loadMatomoEvent",
					function() {
						$(document).ready(
							function() {
								var _paq = window._paq = window._paq || [];
								// _paq.push(['trackPageView']);
								_paq.push(['enableLinkTracking']);
								(function() {
									var u=env.options.matomo_server;
									_paq.push(['setTrackerUrl', u+'matomo.php']);
									_paq.push(['setSiteId', env.options.matomo_id.toString()]);
									var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
									g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
								})();
								$.triggerEvent("matomoLoadedEvent");
							}
						);
					}
				);
			}

			if (env.options.google_analytics_id) {
				// google analytics
				$.executeAfterEvent(
					"lazyBegunEvent",
					function() {
						$(document).ready(
							function() {
								// from https://git.zx2c4.com/PhotoFloat/tree/web/js/999-googletracker.js
								window._gaq = window._gaq || [];
								window._gaq.push(['_setAccount', env.options.google_analytics_id]);
								var ga = document.createElement('script');
								ga.type = 'text/javascript';
								ga.async = true;
								ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
								var s = document.getElementsByTagName('script')[0];
								s.parentNode.insertBefore(ga, s);
								$(document).ready(function() {
									$(window).hashchange(function() {
										window._gaq = window._gaq || [];
										window._gaq.push(['_trackPageview']);
										window._gaq.push(['_trackPageview', phFl.convertHashToCacheBase(location.hash)]);
									});
								});
							}
						);
					}
				);
			}
		}
	);
});
