(function() {
	var util = new Utilities();
	var menuF = new MenuFunctions();
	var mapF;
	$.executeAfterEvent(
		"mapFunctionsLoadedEvent",
		function() {
			mapF = new MapFunctions();
		}
	);

	TopFunctions.showBrowsingModeMessage = function(ev, selector) {
		$(".browsing-mode-message").stop().hide().css("opacity", "");
		$(selector).show();
		$(selector).fadeOut(
			2500,
			function(){
				util.hideId(selector);
			}
		);
		if (ev.originalEvent !== undefined && ev.originalEvent.x !== 0 && ev.originalEvent.y !== 0)
			env.isABrowsingModeChangeFromMouseClick = true;
	};

	TopFunctions.prototype.toggleTitleAndBottomThumbnailsAndDescriptionsAndTags = function(ev) {
		// if ([1, 9].indexOf(ev.which) !== -1 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
		var howMany = 0;
		if (env.options.hide_title)
			howMany ++;
		if (env.options.hide_bottom_thumbnails)
			howMany ++;
		if (env.options.hide_descriptions)
			howMany ++;
		if (env.options.hide_tags)
			howMany ++;

		var previousTitleVisibility = $("#album-view .title").is(":visible");
		var previousBottomThumbnailsVisibility = $("#album-and-media-container.single-media #thumbs").is(":visible");
		if (env.currentMedia !== null) {
			previousTitleVisibility = $(".media-box#center .title").is(":visible");
		}
		if (howMany > 2) {
			env.options.hide_title = false;
			env.options.hide_bottom_thumbnails = false;
			env.options.hide_descriptions = false;
			env.options.hide_tags = false;
		} else {
			env.options.hide_title = true;
			env.options.hide_bottom_thumbnails = true;
			env.options.hide_descriptions = true;
			env.options.hide_tags = true;
		}
		menuF.setBooleanCookie("hideTitle", env.options.hide_title);
		menuF.setBooleanCookie("hideBottomThumbnails", env.options.hide_bottom_thumbnails);
		menuF.setBooleanCookie("hideDescriptions", env.options.hide_descriptions);
		menuF.setBooleanCookie("hideTags", env.options.hide_tags);
		menuF.updateMenu();

		if (util.isPopup()) {
			env.mapAlbum.updatePopup();
		}

		util.setTitleOptions();
		util.setMediaOptions();
		if (env.currentMedia === null) {
			util.setSubalbumsOptions();
			if (env.currentAlbum.visibleSubalbums().length)
				util.adaptSubalbumCaptionHeight(); // in toggleTitleAndBottomThumbnailsAndDescriptionsAndTags
		}

		var currentTitleVisibility = $("#album-view .title").is(":visible");
		var currentBottomThumbnailsVisibility = $("#album-and-media-container.single-media #thumbs").is(":visible");
		if (env.currentMedia !== null) {
			currentTitleVisibility = $(".media-box#center .title").is(":visible");
		}

		if (env.currentMedia !== null) {
			if (currentTitleVisibility !== previousTitleVisibility || currentBottomThumbnailsVisibility !== previousBottomThumbnailsVisibility) {
				let event = {data: {}};
				event.data.resize = true;
				event.data.id = "center";
				let scalePromise = env.currentMedia.scale(event);
				scalePromise.then(
					function() {
						util.setFinalOptions(env.currentMedia, false); // OK: scale promise
					}
				);
				if (env.nextMedia) {
					event.data.id = "right";
					env.nextMedia.scale(event);
				}
				if (env.prevMedia) {
					event.data.id = "left";
					env.prevMedia.scale(event);
				}
			}
		}

		return false;
	};

	TopFunctions.toggleTitle = function(ev) {
		// next line: why [1, 9].indexOf(ev.which) !== -1 ?!?!?
		// if ([1, 9].indexOf(ev.which) !== -1 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
		if ((ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			if (
				env.isAnyMobile &&
				env.currentMedia !== null &&
				! env.options.hide_title && (
					$("#album-and-media-container #media-view #center .title").css("display") === "none" ||
					$("#album-and-media-container.single-media").hasClass("show-title")
				)
			) {
				// the visibility has to be controlled with the added class
				$("#album-and-media-container.single-media").toggleClass("show-title");
			} else {
				if (
					env.isAnyMobile &&
					env.currentMedia !== null &&
					env.options.hide_title &&
					$("#album-and-media-container #media-view #center .title").css("display") === "none"
				)
					$("#album-and-media-container.single-media").addClass("show-title");
				env.options.hide_title = ! env.options.hide_title;
				menuF.setBooleanCookie("hideTitle", env.options.hide_title);
			}
			if (env.currentMedia !== null) {
				let event = {data: {}};
				event.data.resize = true;
				event.data.id = "center";
				let scalePromise = env.currentMedia.scale(event);
				scalePromise.then(
					function() {
						util.setFinalOptions(env.currentMedia, false); // OK: toggle title
					}
				);
				if (env.nextMedia !== null) {
					event.data.id = "right";
					env.nextMedia.scale(event);
				}
				if (env.prevMedia !== null) {
					event.data.id = "left";
					env.prevMedia.scale(event);
				}
			}
			util.setTitleOptions();
			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.toggleBottomThumbnails = function(ev) {
		if (ev.which === undefined || [1, 9].indexOf(ev.which) !== -1 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			env.options.hide_bottom_thumbnails = ! env.options.hide_bottom_thumbnails;
			menuF.setBooleanCookie("hideBottomThumbnails", env.options.hide_bottom_thumbnails);
			menuF.updateMenu();
			if (env.options.hide_bottom_thumbnails) {
				$("#album-and-media-container.single-media #thumbs").addClass("hidden-by-option");
			} else {
				$("#album-and-media-container.single-media #thumbs").removeClass("hidden-by-option");
			}
			if (! $("#album-and-media-container").hasClass("single-media")) {
				$("#album-and-media-container").addClass("single-media");
				env.currentAlbum.showMedia();
			}
			if (env.currentMedia !== null) {
				let event = {data: {}};
				event.data.resize = true;
				event.data.id = "center";
				let scalePromise = env.currentMedia.scale(event);
				scalePromise.then(
					function() {
						util.setFinalOptions(env.currentMedia, false); // OK: toggle bottom thumbnails
						menuF.updateMenu();
					}
				);
				if (env.nextMedia !== null) {
					event.data.id = "right";
					env.nextMedia.scale(event);
				}
				if (env.prevMedia !== null) {
					event.data.id = "left";
					env.prevMedia.scale(event);
				}
			}
		}
		return false;
	};

	TopFunctions.toggleSaveData = function(ev) {
		if (ev.which === undefined || [1, 9].indexOf(ev.which) !== -1 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			env.options.save_data = ! env.options.save_data;
			menuF.setBooleanCookie("saveData", env.options.save_data);

			if (env.options.save_data)
				// do not optimize image formats
				env.devicePixelRatio = 1;
			else
				env.devicePixelRatio =  window.devicePixelRatio || 1;

			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.toggleDescriptions = function(ev) {
		if (ev.which === undefined || [1, 9].indexOf(ev.which) !== -1 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			env.options.hide_descriptions = ! env.options.hide_descriptions;
			menuF.setBooleanCookie("hideDescriptions", env.options.hide_descriptions);

			if (util.isPopup() || env.currentMedia === null && env.currentAlbum.numVisibleMedia()) {
				util.setMediaOptions();
			}
			util.adaptMediaCaptionHeight();
			if (! util.isPopup() && env.currentMedia === null && env.currentAlbum.visibleSubalbums().length) {
				util.setSubalbumsOptions();
			}
			util.setFinalOptions(env.currentMedia, false); // OK: toggle descriptions

			menuF.updateMenu();
			if (util.isPopup()) {
				env.mapAlbum.updatePopup();
				util.adaptMediaCaptionHeight(true);
			}

			if (env.currentAlbum.visibleSubalbums().length)
				util.adaptSubalbumCaptionHeight(); // in toggleDescriptions
		}
		return false;
	};

	TopFunctions.toggleTags = function(ev) {
		if (ev.which === undefined || [1, 9].indexOf(ev.which) !== -1 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			env.options.hide_tags = ! env.options.hide_tags;
			menuF.setBooleanCookie("hideTags", env.options.hide_tags);

			if (util.isPopup() || env.currentMedia === null && env.currentAlbum.numVisibleMedia()) {
				util.setMediaOptions();
			}
			util.adaptMediaCaptionHeight();
			if (! util.isPopup() && env.currentMedia === null && env.currentAlbum.visibleSubalbums().length) {
				util.setSubalbumsOptions();
			}
			util.setFinalOptions(env.currentMedia, false); // OK: toggle tags

			menuF.updateMenu();
			if (util.isPopup()) {
				env.mapAlbum.updatePopup();
				util.adaptMediaCaptionHeight(true);
			}

			if (env.currentAlbum.visibleSubalbums().length)
				util.adaptSubalbumCaptionHeight(); // in toggleTags
		}
		return false;
	};

	TopFunctions.toggleSlideMode = function(ev) {
		if ((ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			env.options.albums_slide_style = ! env.options.albums_slide_style;
			menuF.setBooleanCookie("albumsSlideStyle", env.options.albums_slide_style);


			util.setSubalbumsOptions();
			// if (env.currentAlbum.visibleSubalbums().length)
			util.adaptSubalbumCaptionHeight(); // in toggleSlideMode

			let aSubalbumWasHighlighted = util.aSubalbumIsHighlighted();
			let highlightedObjectSelector = util.highlightedObjectSelector();
			util.highlighObject(highlightedObjectSelector, aSubalbumWasHighlighted);

			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.toggleSpacing = function(ev) {
		ev.stopPropagation();
		if ((ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			if (env.options.spacing)
				env.options.spacing = 0;
			else
				env.options.spacing = env.options.spacingSavedValue;
			menuF.setCookie("spacing", env.options.spacing);

			if (env.currentAlbum.visibleSubalbums().length) {
				util.setSubalbumsOptions();
				util.adaptSubalbumCaptionHeight(); // in toggleSpacing
			}
			if (env.currentAlbum.numVisibleMedia()) {
				util.setMediaOptions();
				util.adaptMediaCaptionHeight();
			}

			if (util.isPopup())
				env.mapAlbum.updatePopup();

			let aSubalbumWasHighlighted = util.aSubalbumIsHighlighted();
			let highlightedObjectSelector = util.highlightedObjectSelector();
			util.highlighObject(highlightedObjectSelector, aSubalbumWasHighlighted);

			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.toggleAlbumNames = function(ev) {
		if ((ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			env.options.show_album_names_below_thumbs = ! env.options.show_album_names_below_thumbs;
			menuF.setBooleanCookie("showAlbumNamesBelowThumbs", env.options.show_album_names_below_thumbs);
			menuF.updateMenu();
			util.setSubalbumsOptions();
			env.currentAlbum.setSubalbumsSortDataAndAdaptCaption();
		}
		return false;
	};

	TopFunctions.toggleMediaCount = function(ev) {
		ev.stopPropagation();
		if (
			(ev.button === 0 || ev.button === undefined) &&
			! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
		) {
			env.options.show_album_media_count = ! env.options.show_album_media_count;
			menuF.setBooleanCookie("showAlbumMediaCount", env.options.show_album_media_count);
			menuF.updateMenu();
			util.setTitleOptions();
			util.setSubalbumsOptions();
			if (env.currentAlbum.visibleSubalbums().length) {
				util.adaptSubalbumCaptionHeight(); // in toggleMediaCount
			}
		}
		return false;
	};

	TopFunctions.toggleMediaNames = function(ev) {
		if (
			(ev.button === 0 || ev.button === undefined) &&
			! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
		) {
			env.options.show_media_names_below_thumbs = ! env.options.show_media_names_below_thumbs;
			menuF.setBooleanCookie("showMediaNamesBelowThumbs", env.options.show_media_names_below_thumbs);
			menuF.updateMenu();
			util.setMediaOptions();
			if (env.currentAlbum.numVisibleMedia()) {
				env.currentAlbum.setMediaSortDataAndAdaptCaption();
				if (util.isPopup())
					env.currentAlbum.setMediaSortDataAndAdaptCaption(true);
			}

			if (util.isPopup()) {
				env.mapAlbum.updatePopup();
				env.currentAlbum.setMediaSortDataAndAdaptCaption();
				if (util.isPopup())
					env.currentAlbum.setMediaSortDataAndAdaptCaption(true);
			}
		}
		return false;
	};

	TopFunctions.toggleAlbumsSquare = function(ev) {
		if (
			(ev.button === 0 || ev.button === undefined) &&
			! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
		) {
			env.options.album_thumb_type = (
				env.options.album_thumb_type.indexOf("square") > -1 ?
				"album_fit" :
				"album_square"
			);
			menuF.setCookie("albumThumbType", env.options.album_thumb_type);

			util.setSubalbumsOptions();
			util.adaptSubalbumCaptionHeight(); // in toggleAlbumsSquare

			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.toggleMediaSquare = function(ev) {
		if (
			(ev.button === 0 || ev.button === undefined) &&
			! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
		) {
			env.options.media_thumb_type = (
				env.options.media_thumb_type.indexOf("square") > -1 ?
				"media_fixed_height" :
				"media_square"
			);
			menuF.setCookie("mediaThumbType", env.options.media_thumb_type);
			menuF.updateMenu();
			if (env.currentAlbum.numVisibleMedia()) {
				let highlightedSingleMediaId = $(
					"#thumbs .highlighted-object img.thumbnail"
				).attr("id");
				env.currentAlbum.showMedia();
				if (env.currentMedia !== null) {
					util.scrollBottomMediaToHighlightedThumb();
				} else {
					util.addHighlightToMediaOrSubalbum(
						$("#" + highlightedSingleMediaId
					).parent().parent());
					util.scrollAlbumViewToHighlightedMedia($(
						"#" + highlightedSingleMediaId
					).parent().parent());
				}
			}

			if (util.isPopup()) {
				env.mapAlbum.updatePopup();
				let highlightedSingleMediaInPopupId = $(
					"#popup-images-wrapper .highlighted-object img.thumbnail"
				).attr("id");
				env.mapAlbum.showMedia();
				$("#popup-images-wrapper .highlighted-object").removeClass("highlighted-object");
				util.addHighlightToMediaOrSubalbum($("#" + highlightedSingleMediaInPopupId));
				util.scrollPopupToHighlightedThumb($("#" + highlightedSingleMediaInPopupId));
			}
		}
		return false;
	};

	TopFunctions.toggleFlatDownloads = function(ev) {
		if (
			(ev.button === 0 || ev.button === undefined) &&
			! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
		) {
			env.options.flat_downloads = ! env.options.flat_downloads;
			menuF.setBooleanCookie("flatDownloads", env.options.flat_downloads);
			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.toggleImagesDownload = function(ev) {
		if (
			(ev.button === 0 || ev.button === undefined) &&
			! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey
		) {
			env.options.download_images = ! env.options.download_images;
			menuF.setBooleanCookie("imagesDownload", env.options.download_images);
			if (! env.options.download_audios && ! env.options.download_videos) {
				env.options.download_videos = true;
				menuF.setBooleanCookie("videosDownload", env.options.download_videos);
			}
			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.changeImagesDownloadSizeAndFormat = function(ev, reverse=false) {
		if (! ev.ctrlKey && ! ev.altKey) {
			let modes = [];
			if (env.options.expose_original_media)
				modes.push("original");
			if (env.options.expose_full_size_media_in_cache)
				modes.push("fullSize");
			if (env.options.reduced_sizes.length)
				for (let i = 0; i < env.options.reduced_sizes.length; i ++)
					modes.push(i);

			let cacheImageFormatsBeginningWithJpg = ["jpg"];
			for (let format of env.options.cache_images_formats)
				if (cacheImageFormatsBeginningWithJpg.indexOf(format) === -1)
					cacheImageFormatsBeginningWithJpg.push(format);

			let modeIndex = modes.indexOf(env.options.download_images_size_index);
			let formatIndex = cacheImageFormatsBeginningWithJpg.indexOf(
				env.options.download_images_format
			);
			if (! reverse) {
				if (
					modeIndex === modes.length -1 &&
					formatIndex === cacheImageFormatsBeginningWithJpg.length - 1
				) {
					env.options.download_images_size_index = modes[0];
					env.options.download_images_format = cacheImageFormatsBeginningWithJpg[0];
				} else if (
					env.options.download_images_size_index === "original" ||
					formatIndex === cacheImageFormatsBeginningWithJpg.length - 1
				) {
					env.options.download_images_size_index = modes[modeIndex + 1];
					env.options.download_images_format = cacheImageFormatsBeginningWithJpg[0];
				} else {
					env.options.download_images_format = cacheImageFormatsBeginningWithJpg[
						formatIndex + 1
					];
				}
			} else {
				// reverse is true
				if (
					modeIndex === 0 && (
						env.options.download_images_size_index === "original" || formatIndex === 0
					)
				) {
					env.options.download_images_size_index = modes[modes.length - 1];
					env.options.download_images_format = cacheImageFormatsBeginningWithJpg[
						cacheImageFormatsBeginningWithJpg.length - 1
					];
				} else if (formatIndex === 0) {
					env.options.download_images_size_index = modes[modeIndex - 1];
					env.options.download_images_format = cacheImageFormatsBeginningWithJpg[
						cacheImageFormatsBeginningWithJpg.length - 1
					];
				} else {
					env.options.download_images_format = cacheImageFormatsBeginningWithJpg[
						formatIndex - 1
					];
				}
			}

			if (! env.options.download_images) {
				env.options.download_images = ! env.options.download_images;
				menuF.setCookie("imagesDownload", env.options.download_images);
			}

			menuF.setCookie("imagesDownloadSizeIndex", env.options.download_images_size_index);
			menuF.setCookie("imagesDownloadFormat", env.options.download_images_format);
			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.toggleAudiosDownload = function(ev) {
		if ((ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			env.options.download_audios = ! env.options.download_audios;
			menuF.setBooleanCookie("audiosDownload", env.options.download_audios);
			if (! env.options.download_images && ! env.options.download_videos) {
				env.options.download_images = true;
				menuF.setBooleanCookie("imagesDownload", env.options.download_images);
			}
			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.changeAudiosDownloadSize = function(ev, reverse=false) {
		if (! ev.ctrlKey && ! ev.altKey) {
			let modes = [];
			if (env.options.expose_original_media)
				modes.push("original");
			if (env.options.expose_full_size_media_in_cache)
				modes.push("fullSize");
			modes.push("transcoded");

			let modeIndex = modes.indexOf(env.options.download_audios_size);
			if (! reverse) {
				if (modeIndex === modes.length - 1)
					env.options.download_audios_size = modes[0];
				else
					env.options.download_audios_size = modes[modeIndex + 1];
			} else {
				// reverse is true
				if (modeIndex === 0)
					env.options.download_audios_size = modes[modes.length - 1];
				else
					env.options.download_audios_size = modes[modeIndex - 1];
			}

			if (! env.options.download_audios) {
				env.options.download_audios = ! env.options.download_audios;
				menuF.setCookie("audiosDownload", env.options.download_audios);
			}

			menuF.setCookie("audiosDownloadSize", env.options.download_audios_size);
			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.toggleVideosDownload = function(ev) {
		if ((ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			env.options.download_videos = ! env.options.download_videos;
			menuF.setBooleanCookie("videosDownload", env.options.download_videos);
			if (! env.options.download_images && ! env.options.download_audios) {
				env.options.download_images = true;
				menuF.setBooleanCookie("imagesDownload", env.options.download_images);
			}
			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.changeVideosDownloadSize = function(ev, reverse=false) {
		if (! ev.ctrlKey && ! ev.altKey) {
			let modes = [];
			if (env.options.expose_original_media)
				modes.push("original");
			if (env.options.expose_full_size_media_in_cache)
				modes.push("fullSize");
			modes.push("transcoded");

			let modeIndex = modes.indexOf(env.options.download_videos_size);
			if (! reverse) {
				if (modeIndex === modes.length - 1)
					env.options.download_videos_size = modes[0];
				else
					env.options.download_videos_size = modes[modeIndex + 1];
			} else {
				// reverse is true
				if (modeIndex === 0)
					env.options.download_videos_size = modes[modes.length - 1];
				else
					env.options.download_videos_size = modes[modeIndex - 1];
			}

			if (! env.options.download_videos) {
				env.options.download_videos = ! env.options.download_videos;
				menuF.setCookie("videosDownload", env.options.download_videos);
			}

			menuF.setCookie("videosDownloadSize", env.options.download_videos_size);
			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.toggleSubalbumsDownload = function(ev) {
		if ((ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			env.options.download_subalbums = ! env.options.download_subalbums;
			menuF.setBooleanCookie("subalbumsDownload", env.options.download_subalbums);
			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.toggleSelectionDownload = function(ev) {
		if ((ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			env.options.download_selection = ! env.options.download_selection;
			menuF.setBooleanCookie("selectionDownload", env.options.download_selection);
			menuF.updateMenu();
		}
		return false;
	};

	TopFunctions.prototype.restoreSettings = function(ev) {
		if (
			! (
				env.albumSort === "name" && env.options.default_album_name_sort ||
				env.albumSort === "exifDateMax" && ! env.options.default_album_name_sort
			)
		) {
			env.albumSort = env.options.default_album_name_sort ? "name" : "exifDateMax";
			menuF.setCookie("albumSortRequested", env.albumSort);
			env.currentAlbum.sortAlbumsMedia();
		}
		if (env.albumReverseSort !== env.options.default_album_reverse_sort) {
			env.albumReverseSort = env.options.default_album_reverse_sort;
			menuF.setBooleanCookie("albumReverseSortRequested", env.options.default_album_reverse_sort);
			env.currentAlbum.sortAlbumsMedia();
		}
		if (
			! (
				env.mediaSort === "name" && env.options.default_media_name_sort ||
				env.mediaSort === "exifDateMax" && ! env.options.default_media_name_sort
			)
		) {
			env.mediaSort = env.options.default_media_name_sort ? "name" : "exifDateMax";
			menuF.setCookie("mediaSortRequested", env.mediaSort);
			env.currentAlbum.sortAlbumsMedia();
			if (util.isPopup())
				env.mapAlbum.sortAlbumsMedia();
		}
		if (env.mediaReverseSort !== env.options.default_media_reverse_sort) {
			env.mediaReverseSort = env.options.default_media_reverse_sort;
			menuF.setBooleanCookie("mediaReverseSortRequested", env.options.default_media_reverse_sort);
			env.currentAlbum.sortAlbumsMedia();
			if (util.isPopup())
				env.mapAlbum.sortAlbumsMedia();
		}

		if (env.defaultOptions.show_album_media_count !== env.options.show_album_media_count) {
			TopFunctions.toggleMediaCount(ev);
		}
		if (env.defaultOptions.hide_title !== env.options.hide_title) {
			TopFunctions.toggleTitle(ev);
		}
		if ($("#album-and-media-container").hasClass("show-title")) {
			$("#album-and-media-container").removeClass("show-title");
		}

		if (env.defaultOptions.albums_slide_style !== env.options.albums_slide_style) {
			TopFunctions.toggleSlideMode(ev);
		}
		if (! env.defaultOptions.only_square_thumbnails && env.defaultOptions.album_thumb_type !== env.options.album_thumb_type) {
			TopFunctions.toggleAlbumsSquare(ev);
		}
		if (env.defaultOptions.show_album_names_below_thumbs !== env.options.show_album_names_below_thumbs) {
			TopFunctions.toggleAlbumNames(ev);
		}

		if (! env.defaultOptions.only_square_thumbnails && env.defaultOptions.media_thumb_type !== env.options.media_thumb_type) {
			TopFunctions.toggleMediaSquare(ev);
		}
		if (env.defaultOptions.show_media_names_below_thumbs !== env.options.show_media_names_below_thumbs) {
			TopFunctions.toggleMediaNames(ev);
		}

		if (env.defaultOptions.hide_descriptions !== env.options.hide_descriptions) {
			TopFunctions.toggleDescriptions(ev);
		}
		if (env.defaultOptions.hide_tags !== env.options.hide_tags) {
			TopFunctions.toggleTags(ev);
		}
		if (env.defaultOptions.thumb_spacing !== env.options.spacing) {
			TopFunctions.toggleSpacing(ev);
		}
		if (env.defaultOptions.hide_bottom_thumbnails !== env.options.hide_bottom_thumbnails) {
			TopFunctions.toggleBottomThumbnails(ev);
		}

		if (env.defaultOptions.flat_downloads !== env.options.flat_downloads) {
			TopFunctions.toggleFlatDownloads(ev);
		}

		if (env.defaultOptions.download_images !== env.options.download_images) {
			TopFunctions.toggleImagesDownload(ev);
		}

		while (
			env.defaultOptions.download_images_size_index !== env.options.download_images_size_index &&
			env.defaultOptions.download_images_format !== env.options.download_images_format
		) {
			TopFunctions.changeImagesDownloadSizeAndFormat(ev);
		}

		if (env.defaultOptions.download_audios !== env.options.download_audios) {
			TopFunctions.toggleAudiosDownload(ev);
		}

		while (env.defaultOptions.download_audios_size !== env.options.download_audios_size) {
			TopFunctions.changeAudiosDownloadSize(ev);
		}

		if (env.defaultOptions.download_videos !== env.options.download_videos) {
			TopFunctions.toggleVideosDownload(ev);
		}

		while (env.defaultOptions.download_videos_size !== env.options.download_videos_size) {
			TopFunctions.changeVideosDownloadSize(ev);
		}

		if (env.defaultOptions.download_subalbums !== env.options.download_subalbums) {
			TopFunctions.toggleSubalbumsDownload(ev);
		}

		if (env.defaultOptions.download_selection !== env.options.download_selection) {
			TopFunctions.toggleSelectionDownload(ev);
		}

		if (env.defaultOptions.save_data !== env.options.save_data) {
			TopFunctions.toggleSaveData(ev);
		}

		if (env.search.insideWords) {
			util.toggleInsideWordsSearch();
		}
		if (env.search.anyWord) {
			util.toggleAnyWordSearch();
		}
		if (env.search.caseSensitive) {
			util.toggleCaseSensitiveSearch();
		}
		if (env.search.accentSensitive) {
			util.toggleAccentSensitiveSearch();
		}
		if (env.search.numbers) {
			util.toggleNumbersSearch();
		}
		if (env.search.tagsOnly) {
			util.toggleTagsOnlySearch();
		}
		if (! env.search.currentAlbumOnly) {
			util.toggleCurrentAbumSearch();
		}

		if (env.options.show_big_virtual_folders) {
			util.toggleBigAlbumsShow(ev);
		}

		if (env.keepShowingGeolocationSuggestText === undefined || ! env.keepShowingGeolocationSuggestText) {
			mapF.setKeepShowingGeolocationSuggestTextState(true);
		}

		if (env.slideshowInterval !== env.slideshowIntervalDefault) {
			env.slideshowInterval = env.slideshowIntervalDefault;
			menuF.setCookie("slideshowInterval", env.slideshowInterval);
		}

		menuF.updateMenu();
		$("body").css("background-color", env.options.background_color);
		util.setFinalOptions(env.currentMedia, false); // OK: restore settings

		if (env.currentMedia === null && env.currentAlbum.visibleSubalbums().length && $("#subalbums").is(":visible")) {
			env.currentAlbum.setSubalbumsSortDataAndAdaptCaption();
		}
		if (env.currentMedia === null && env.currentAlbum.numVisibleMedia() && $("#thumbs").is(":visible")) {
			env.currentAlbum.setMediaSortDataAndAdaptCaption();
			if (util.isPopup())
				env.currentAlbum.setMediaSortDataAndAdaptCaption(true);
		}

		let highlightedItemObject = menuF.highlightedItemObject();
		let nextItem = menuF.nextItemForHighlighting(highlightedItemObject);
		menuF.addHighlightToItem(nextItem);

		$("#settings-restored").stop().fadeIn(
			200,
			function() {
				$("#settings-restored").fadeOut(2500);
			}
		);

		$(window).hashchange();

		return false;
	};

	TopFunctions.prototype.showBrowsingModeMessage = TopFunctions.showBrowsingModeMessage;
	TopFunctions.prototype.toggleMediaCount = TopFunctions.toggleMediaCount;
	TopFunctions.prototype.toggleMediaNames = TopFunctions.toggleMediaNames;
	TopFunctions.prototype.toggleTitle = TopFunctions.toggleTitle;
	TopFunctions.prototype.toggleSlideMode = TopFunctions.toggleSlideMode;
	TopFunctions.prototype.toggleAlbumsSquare = TopFunctions.toggleAlbumsSquare;
	TopFunctions.prototype.toggleAlbumNames = TopFunctions.toggleAlbumNames;
	TopFunctions.prototype.toggleMediaSquare = TopFunctions.toggleMediaSquare;
	TopFunctions.prototype.toggleDescriptions = TopFunctions.toggleDescriptions;
	TopFunctions.prototype.toggleTags = TopFunctions.toggleTags;
	TopFunctions.prototype.toggleSpacing = TopFunctions.toggleSpacing;
	TopFunctions.prototype.toggleBottomThumbnails = TopFunctions.toggleBottomThumbnails;
	TopFunctions.prototype.toggleFlatDownloads = TopFunctions.toggleFlatDownloads;
	TopFunctions.prototype.toggleImagesDownload = TopFunctions.toggleImagesDownload;
	TopFunctions.prototype.changeImagesDownloadSizeAndFormat = TopFunctions.changeImagesDownloadSizeAndFormat;
	TopFunctions.prototype.toggleAudiosDownload = TopFunctions.toggleAudiosDownload;
	TopFunctions.prototype.changeAudiosDownloadSize = TopFunctions.changeAudiosDownloadSize;
	TopFunctions.prototype.toggleVideosDownload = TopFunctions.toggleVideosDownload;
	TopFunctions.prototype.changeVideosDownloadSize = TopFunctions.changeVideosDownloadSize;
	TopFunctions.prototype.toggleSubalbumsDownload = TopFunctions.toggleSubalbumsDownload;
	TopFunctions.prototype.toggleSelectionDownload = TopFunctions.toggleSelectionDownload;
	TopFunctions.prototype.toggleSaveData = TopFunctions.toggleSaveData;
}());
//# sourceURL=038-top-functions-bis.js
