(function() {

	var util = new Utilities();
	var pS;
	$.executeAfterEvent(
		"pinchSwipeFunctionsLoadedEvent",
		function() {
			pS = new PinchSwipe();
			$.triggerEvent("pinchSwipeOkInSingleMediaMethodsBisEvent");
		}
	);

	SingleMedia.prototype.addToSelection = function(selectionAlbum = env.selectionAlbum) {
		if (! this.isSelected(selectionAlbum)) {
			// this.parentAlbum = selectionAlbum;
			selectionAlbum.media.push(this);

			if (this.hasGpsData()) {
				// add the media position
				selectionAlbum.positionsAndMediaInTree.addSingleMedia(this);
				selectionAlbum.numPositionsInTree = selectionAlbum.positionsAndMediaInTree.length;
			}
			var singleMediaArrayCounts = new Media([this]).imagesAudiosVideosCount();
			selectionAlbum.numsMedia.sum(singleMediaArrayCounts);
			selectionAlbum.sizesOfAlbum.sum(this.fileSizes);
			if (env.options.expose_image_positions && ! this.hasGpsData()) {
				selectionAlbum.nonGeotagged.numsMedia.sum(singleMediaArrayCounts);
				selectionAlbum.nonGeotagged.sizesOfAlbum.sum(this.fileSizes);
			}
			if (! this.isInsideSelectedAlbums()) {
				selectionAlbum.numsMediaInSubTree.sum(singleMediaArrayCounts);
				selectionAlbum.sizesOfSubTree.sum(this.fileSizes);
				if (env.options.expose_image_positions && ! this.hasGpsData()) {
					selectionAlbum.nonGeotagged.numsMediaInSubTree.sum(singleMediaArrayCounts);
					selectionAlbum.nonGeotagged.sizesOfSubTree.sum(this.fileSizes);
				}
			}

			this.parentAlbum.invalidatePositionsAndMediaInAlbumAndSubalbums();
		}
	};

	SingleMedia.prototype.removeFromSelection = function(selectionAlbum = env.selectionAlbum) {
		if (this.isSelected(selectionAlbum)) {
			var index = selectionAlbum.media.findIndex(x => x.isEqual(this));

			selectionAlbum.media.splice(index, 1);

			if (this.hasGpsData()) {
				selectionAlbum.positionsAndMediaInTree.removeSingleMedia(this);
				selectionAlbum.numPositionsInTree = selectionAlbum.positionsAndMediaInTree.length;
			}
			var singleMediaArrayCounts = new Media([this]).imagesAudiosVideosCount();
			selectionAlbum.numsMedia.subtract(singleMediaArrayCounts);
			selectionAlbum.sizesOfAlbum.subtract(this.fileSizes);
			if (env.options.expose_image_positions && ! this.hasGpsData()) {
				selectionAlbum.nonGeotagged.numsMedia.subtract(singleMediaArrayCounts);
				selectionAlbum.nonGeotagged.sizesOfAlbum.subtract(this.fileSizes);
			}
			if (! this.isInsideSelectedAlbums()) {
				selectionAlbum.numsMediaInSubTree.subtract(singleMediaArrayCounts);
				selectionAlbum.sizesOfSubTree.subtract(this.fileSizes);
				if (env.options.expose_image_positions && ! this.hasGpsData()) {
					selectionAlbum.nonGeotagged.numsMediaInSubTree.subtract(singleMediaArrayCounts);
					selectionAlbum.nonGeotagged.sizesOfSubTree.subtract(this.fileSizes);
				}
			}
		}
	};

	SingleMedia.prototype.removeRotation = function(mediaElement) {
		// this function resets the zoom
		if (env.currentMedia.isImage()) {
			var zoomRatio = 1;
			if (env.currentZoom > env.initialZoom)
				zoomRatio = env.currentZoom / env.unroundedInitialZoom;
		}

		var parameter = pS.prepareTransformParameter(0, 0, 0, 1);
		mediaElement.css(env.transformParameterName, parameter);
		if (env.currentMedia.isImage()) {
			env.unroundedCurrentZoom = pS.unroundedScreenZoom();
			env.currentZoom = util.roundDecimals(env.unroundedCurrentZoom);
			env.initialZoom = env.currentZoom;
		}
		util.setFinalOptions(env.currentMedia, false); // OK: remove rotation
	};

}());
//# sourceURL=043-single-media-methods-bis.js
