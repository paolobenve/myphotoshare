(function() {

	var util = new Utilities();
	var phFl = new PhotoFloat();
	var menuF = new MenuFunctions();
	var mapF;
	$.executeAfterEvent(
		"mapFunctionsLoadedEvent",
		function() {
			mapF = new MapFunctions();
		}
	);

	MapAlbum.prototype.addAndRemovePositionsPlayingClickHistory = function() {
		// this function adds to or remove from the map album the positions enclosed in the cluster
		// the other values in the map albums still remain to be calculated
		let self = this;
		self.map.clickHistory.forEach(
			mapClickHistoryElement => {
				// var oneClickPromise = new Promise(
				// 	function(resolve_oneClickPromise) {
				env.mymap.setView(mapClickHistoryElement.center, mapClickHistoryElement.zoom, {animate: false});
				// sometimes on mobile after resizing the cluster may be out of the map bounds -> move the cluster to the center of the map
				if (! env.mymap.getBounds().contains(mapClickHistoryElement.latlng))
					env.mymap.setView(mapClickHistoryElement.latlng, mapClickHistoryElement.zoom, {animate: false});
				let ev = {
					latlng: mapClickHistoryElement.latlng,
					originalEvent: {
						shiftKey: mapClickHistoryElement.shiftKey,
						ctrlKey: mapClickHistoryElement.ctrlKey,
					}
				};
				self.addOrRemovePositionsThroughClick(ev);
				self.possiblyResetPopup(ev, false);
			}
		);
	};


	MapAlbum.prototype.showAddSubtractPopup = function(evt) {
		function updateMapAlbumAndPopup(ev) {
			self.addOrRemovePositionsThroughClick(ev);
			let endPreparingPromise = self.endPreparingMapAlbum();
			endPreparingPromise.then(
				function() {
					self.prepareAndDoPopupUpdate();
				}
			);
		}

		let self = this;

		let [
			currentCluster,
			,
			clusterPositionsAlreadyInPopup,
			clusterPositionsNotYetInPopup
		] = self.calculationsWithClusters(evt);

		// if (evt.fromAddOrSubtract === undefined) {
		if ($('.shift-or-control').length)
			$('.shift-or-control .leaflet-popup-close-button')[0].click();

		let addDimmed = "", subtractDimmed = "";
		if (this.isEmpty() || ! clusterPositionsAlreadyInPopup.length) {
			// shiftKey = true;
			// ctrlKey = false;
			subtractDimmed = " dimmed";
		} else if (! clusterPositionsNotYetInPopup.length) {
			// shiftKey = false;
			// ctrlKey = true;
			addDimmed = " dimmed";
		}

		// show a popup with + and - in order to tell the app if we must add (shift) or remove (control)
		L.popup({className: "shift-or-control"})
			.setLatLng(currentCluster.averagePosition)
			.setContent('<div class="cluster-add' + addDimmed + '">+</div><div class="cluster-subtract' + subtractDimmed + '">-</div>')
			.addTo(env.mymap)
			.openOn(env.mymap);

		$("#loading").hide();

		if (! addDimmed) {
			$(".cluster-add").off("click").on(
				"click",
				function(ev) {
					$(".shift-or-control .leaflet-popup-close-button")[0].click();
					ev.shiftKey = true;
					ev.ctrlKey = false;
					ev.latlng = evt.latlng;
					ev.fromAddOrSubtract = true;
					updateMapAlbumAndPopup(ev);
				}
			);
		}

		if (! subtractDimmed) {
			$(".cluster-subtract").off("click").on(
				"click",
				function(ev) {
					$(".shift-or-control .leaflet-popup-close-button")[0].click();
					ev.shiftKey = false;
					ev.ctrlKey = true;
					ev.latlng = evt.latlng;
					ev.fromAddOrSubtract = true;
					updateMapAlbumAndPopup(ev);
				}
			);
		}
	};

	MapAlbum.prototype.changeCacheBase = function(newCacheBase) {
		util.changeCacheBase(this, newCacheBase);
	};


	MapAlbum.prototype.generateMap = function(positionsAndMedia, ev, from, autoMode = false) {
		function updateMapAlbumAndContinue(ev) {
			$("#loading").show();
			self.addOrRemovePositionsThroughClick(ev);
			let endPreparingPromise = self.endPreparingMapAlbum();
			endPreparingPromise.then(
				function() {
					if (! autoMode) {
						$("#loading").hide();
						self.possiblyResetPopup(ev);
						if (self.media.length)
							self.prepareAndDoPopupUpdate();
					}
				}
			);
		}

		function setMapSize(positionsAndMedia) {
			mapF.resizeMap();

			$('.map-container').show();
			$(".map-container").css("max-height", $(window).height() - 54).css("max-width", $(window).width() - 54).css("right", "44px").css("top", "24px");
			$(".map-container").css("display", "grid");

			if (positionsAndMedia) {
				// calculate the center
				var center;
				var lastCenterAndZoom;
				if (positionsAndMedia.length) {
					center = positionsAndMedia.averagePosition();
				} else {
					mapF.getLastMapCenterAndZoom();
					lastCenterAndZoom = env.lastMapPositionAndZoom;
					if (typeof lastCenterAndZoom.center === "object")
						center = lastCenterAndZoom.center;
					else
						center = {lat: 0, lng: 0};
				}

				// var thumbAndCaptionHeight = 0;

				// default zoom is used for single media or media list with one point
				var maxXDistance = env.options.photo_map_size;
				var maxYDistance = env.options.photo_map_size;
				if (positionsAndMedia.length > 1) {
					// calculate the maximum distance from the center
					// it's needed in order to calculate the zoom level
					maxXDistance = 0;
					maxYDistance = 0;
					for (let i = 0; i < positionsAndMedia.length; ++i) {
						maxXDistance = Math.max(maxXDistance, Math.abs(util.xDistanceBetweenCoordinatePoints(center, positionsAndMedia[i])));
						maxYDistance = Math.max(maxYDistance, Math.abs(util.yDistanceBetweenCoordinatePoints(center, positionsAndMedia[i])));
					}
				}
				// calculate the zoom level needed in order to have all the points inside the map
				// see https://wiki.openstreetmap.org/wiki/Zoom_levels
				var earthCircumference = 40075016;
				var xZoom = Math.min(env.maxOSMZoom, parseInt(Math.log2((env.windowWidth / 2 * 0.9) * earthCircumference * Math.cos(util.degreesToRadians(center.lat)) / 256 / maxXDistance)));
				var yZoom = Math.min(env.maxOSMZoom, parseInt(Math.log2((env.windowHeight / 2 * 0.9) * earthCircumference * Math.cos(util.degreesToRadians(center.lat)) / 256 / maxYDistance)));
				var zoom;
				if (typeof lastCenterAndZoom === "object")
					zoom = lastCenterAndZoom.zoom;
				else
					zoom = Math.min(xZoom, yZoom);

				if (
					! positionsAndMedia.length && ! (
						util.isPhp() && env.options.user_may_suggest_location && env.options.request_password_email
					)
				)
					zoom = 3;

				if (env.mapIsInitialized)
					env.mymap.remove();

				env.mymap = L.map('mapdiv', {'closePopupOnClick': false}).setView([center.lat, center.lng], zoom);
				$(".map-container > div").css("min-height", (env.windowHeight -50).toString() + "px");
				env.mapIsInitialized = true;

				env.mymap.off("moveend").on(
					"moveend",
					function () {
						mapF.setLastMapCenterAndZoom(env.mymap.getCenter(), env.mymap.getZoom());
					}
				);

				env.mymap.off("zoomend").on(
					"zoomend",
					function () {
						mapF.setLastMapCenterAndZoom(env.mymap.getCenter(), env.mymap.getZoom());
					}
				);
			}
		}

		// end of nested function

		let self = this;

		env.titleWrapper =
			"<div id='popup-image-count' style='max-width: " + env.maxWidthForPopupContent + "px;'>" +
			"</div>" +
			"<div id='popup-images-wrapper'>" +
			"</div>";

		setMapSize(positionsAndMedia);

		if (positionsAndMedia) {
			$("#loading").hide();

			var markers = [];
			// initialize the markers clusters
			env.pruneCluster = new PruneClusterForLeaflet(150, 70);
			PruneCluster.Cluster.ENABLE_MARKERS_LIST = true;

			// modify the marker and the prunecluster so that the click can be managed in order to show the image popup
			env.pruneCluster.BuildLeafletMarker	 = function (marker, position) {
				var m = new L.Marker(position);
				this.PrepareLeafletMarker(m, marker.data, marker.category);
				m.off("click").on(
					"click",
					function(ev) {
						updateMapAlbumAndContinue(ev);
					}
				);
				m.off("contextmenu").on(
					"contextmenu",
					function(ev) {
						if (! ev.originalEvent.shiftKey && ! ev.originalEvent.ctrlKey) {
							ev.originalEvent.preventDefault();
							self.showAddSubtractPopup(ev);
							return false;
						}
					}
				);
				return m;
			};

			env.pruneCluster.BuildLeafletCluster = function (cluster, position) {
				var m = new L.Marker(position, {
					icon: env.pruneCluster.BuildLeafletClusterIcon(cluster)
				});
				m._leafletClusterBounds = cluster.bounds;
				m.off("click").on(
					"click",
					function(ev) {
						updateMapAlbumAndContinue(ev);
					}
				);
				m.off("contextmenu").on(
					"contextmenu",
					function(ev) {
						if (! ev.originalEvent.shiftKey && ! ev.originalEvent.ctrlKey) {
							ev.originalEvent.preventDefault();
							self.showAddSubtractPopup(ev);
							return false;
						}
					}
				);
				return m;
			};

			// modify the cluster marker so that it shows the number of photos rather than the number of clusters
			env.pruneCluster.BuildLeafletClusterIcon = function (cluster) {
				var c = 'prunecluster prunecluster-';
				var iconSize = 38;
				var maxPopulation = env.pruneCluster.Cluster.GetPopulation();
				var markers = cluster.GetClusterMarkers();
				var population = 0;
				// count the number of photos in a cluster
				for(let i = 0; i < markers.length; i ++) {
					population += markers[i].data.mediaList.length;
				}

				if (population < Math.max(10, maxPopulation * 0.01)) {
					c += 'small';
				}
				else if (population < Math.max(100, maxPopulation * 0.05)) {
					c += 'medium';
					iconSize = 40;
				}
				else {
					c += 'large';
					iconSize = 44;
				}
				return new L.DivIcon({
					html: "<div><span>" + population + "</span></div>",
					className: c,
					iconSize: L.point(iconSize, iconSize)
				});
			};

			var isSuggestLocation = false;

			if (! env.isPlayingMapClickHistoryInGetAlbum) {
				L.tileLayer(
					'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
					// Use the following line instead of the former in order to bypass the browser tile cache
					// 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{randint}',
					{
						randint: () => Math.floor( Math.random() * 200000 ) + 1,
						attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors',
						maxZoom: 21,
						maxNativeZoom: env.maxOSMZoom,
						subdomains: 'abc',
						id: 'mapbox.streets'
					}
				).addTo(env.mymap);

				L.control.scale().addTo(env.mymap);

				if (Modernizr.geolocation && window.location.protocol === "https:") {
					L.Control.myLocationButton = L.Control.extend({
						onAdd: function(map) {
							var img = L.DomUtil.create('img');

							img.src = 'img/current-location.png';
							img.style.width = '50px';

							return img;
						},

						onRemove: function(map) {
							// Nothing to do here
						}
					});

					L.control.myLocationButton = function(opts) {
						return new L.Control.myLocationButton(opts);
					};

					L.control.myLocationButton(
						{
							position: 'bottomright'
						}
					).addTo(env.mymap);

					$(".leaflet-bottom.leaflet-right").attr("title", util._t("#click-for-your-position"));
					$(".leaflet-bottom.leaflet-right").off("click").on(
						"click",
						function(ev) {
							ev.stopPropagation();
							ev.preventDefault();
							$("#error-getting-current-location").stop().hide();
							navigator.geolocation.getCurrentPosition(
								function geolocationSuccess(position) {
									env.mymap.panTo(new L.LatLng(position.coords.latitude, position.coords.longitude));
								},
								function geolocationError(err) {
									ev.stopPropagation();
									$("#error-getting-current-location").empty();
									$("#error-getting-current-location").html(
										util._t("#error-getting-current-location") + " <span id='error-getting-current-location-question-mark'>?</span>" +
										env.br +
										"<div id='error-getting-current-location-little'>" + "(" + err.message + ")</div>"
									);
									$("#error-getting-current-location").stop().fadeIn(
										1000,
										function() {
											$("#error-getting-current-location").stop().fadeOut(5000);
										}
									);
									$("#error-getting-current-location-question-mark, #error-getting-current-location").off().on(
										"click",
										function(ev) {
											ev.stopPropagation();
											$("#error-getting-current-location").stop().fadeIn(100);
											$("#error-getting-current-location-question-mark, #error-getting-current-location").off("click").on(
												"click",
												function() {
													$("#error-getting-current-location").hide();
												}
											);
											$("#error-getting-current-location-little").show();
										}
									);
								},
								{
									timeout: 1000
								}
							);
						}
					);
				}

				if (
					ev.data.singleMedia !== null && ! ev.data.singleMedia.hasGpsData() &&
					util.isPhp() && env.options.user_may_suggest_location && env.options.request_password_email
				) {
					isSuggestLocation = true;
					$("#mapdiv").css("cursor", "default");

					// show the central marker, in order to permit the user to suggest by email the geolocation of current media
					if (env.lastMapPositionAndZoom.center !== false)
						env.mymap.setView(env.lastMapPositionAndZoom.center, env.lastMapPositionAndZoom.zoom, {animate: false});
					$(".map-marker-centered").show();
					$(".map-marker-centered-send-suggestion").show();
					$(".map-marker-centered-send-suggestion").attr("title", util._t("#click-to-suggest-position-on-map"));
					if (env.keepShowingGeolocationSuggestText) {
						$("#you-can-suggest-single-media-position").stop().fadeIn(500);
						$("#you-can-suggest-single-media-position").stop().fadeIn(500);
						if ($("#you-can-suggest-single-media-position").children().length === 2) {
							$("#you-can-suggest-single-media-position").append('<div class="keep-showing suggest-button">' + util._t("#keep-showing") + '</div>');
							$("#you-can-suggest-single-media-position").append('<div class="stop-showing suggest-button">' + util._t("#stop-showing") + '</div>');
							$(".stop-showing").on(
								"click",
								function() {
									env.keepShowingGeolocationSuggestText = false;
									menuF.setBooleanCookie("showGeolocationSuggestText", false);
									$("#you-can-suggest-single-media-position").fadeOut(1000);
								}
							);
							$(".keep-showing").on(
								"click",
								function() {
									$("#you-can-suggest-single-media-position").fadeOut(1000);
								}
							);
						}
					}

					$(".map-marker-centered-send-suggestion").off("click").on(
						"click",
						function() {
							var center = env.lastMapPositionAndZoom.center;
							var parameters = [
								"siteurl=" + encodeURIComponent(location.href),
								"photo=" + encodeURIComponent(ev.data.singleMedia.albumMediaPath()),
								"lat=" + encodeURIComponent(center.lat),
								"lng=" + encodeURIComponent(center.lng)
							];
							if (env.options.debug_js)
								// the following line is needed in order to bypass the browser (?) cache;
								// without the random number the php code isn't executed
								parameters.push("&random=" + Math.floor(Math.random() * 10000000));
							var popupUrl = "?" + parameters.join("&");
							$("#sending-single-media-position").stop().fadeIn(
								1000,
								function() {
									menuF.sendEmail(popupUrl, "#sending-single-media-position");
								}
							);
							mapF.setLastMapCenterAndZoom(center, env.mymap.getZoom());
						}
					);
				}
			}

			$('.modal-close').off("click").on(
				"click",
				function() {
					if (! env.isPlayingMapClickHistoryInGetAlbum) {
						mapF.setLastMapCenterAndZoom(env.mymap.getCenter(), env.mymap.getZoom());
						$(".map-marker-centered").hide();
						$(".map-marker-centered-send-suggestion").hide();
						$("#you-can-suggest-single-media-position").hide();
					}
					$("#my-modal.modal").css("display", "none");
					// env.popupRefreshType = "previousAlbum";
					$('#mapdiv').empty();
					if (! env.isPlayingMapClickHistoryInGetAlbum) {
						util.showHideSlideshowIcon();
						if (env.currentAlbum.numVisibleMedia())
							env.currentAlbum.bindMediaSortEvents();
						util.setSocialButtons(); // when closing map
						util.setUpButtonVisibility();
						menuF.updateMenu();
					}
				}
			);

			util.setUpButtonVisibility();

			if (! isSuggestLocation) {
				var cacheBases;
				for (var iPoint = 0; iPoint < positionsAndMedia.length; iPoint ++) {
					cacheBases = '';
					for(var iImage = 0; iImage < positionsAndMedia[iPoint].mediaList.length; iImage ++) {
						// we must get the media corresponding to the name in the point
						if (cacheBases)
							cacheBases += env.br;
						cacheBases += positionsAndMedia[iPoint].mediaList[iImage].cacheBase;
					}

					markers[iPoint] = new PruneCluster.Marker(
						positionsAndMedia[iPoint].lat,
						positionsAndMedia[iPoint].lng,
						{
							icon:	new L.NumberedDivIcon({number: positionsAndMedia[iPoint].mediaList.length})
						}
					);
					env.pruneCluster.RegisterMarker(markers[iPoint]);
					markers[iPoint].data.tooltip = cacheBases;
					markers[iPoint].data.mediaList = positionsAndMedia[iPoint].mediaList;
					markers[iPoint].weight = positionsAndMedia[iPoint].mediaList.length;
				}

				env.mymap.addLayer(env.pruneCluster);

				/**
				* Add a click handler to the map to render the popup.
				*/
				env.mymap.off("click").on(
					"click",
					function(ev) {
						updateMapAlbumAndContinue(ev);
					}
				);
				env.mymap.off("contextmenu").on(
					"contextmenu",
					function(ev) {
						if (! ev.originalEvent.shiftKey && ! ev.originalEvent.ctrlKey) {
							ev.originalEvent.preventDefault();
							self.showAddSubtractPopup(ev);
							return false;
						}
					}
				);

				if (from !== undefined && ! env.isPlayingMapClickHistoryInGetAlbum) {
					if (env.popupRefreshType === "previousAlbum" && ! self.map.clickHistory) {
						self.endPreparingAlbum();
						self.prepareAndDoPopupUpdate();
					} else {
						if (! self.map.clickHistory && env.popupRefreshType === "mapAlbum") {
							env.oldMapAlbum = self;
						}

						// mapAlbum = util.initializeMapAlbum();
						self.addAndRemovePositionsPlayingClickHistory();
						let endPreparingPromise = self.endPreparingMapAlbum();
						endPreparingPromise.then(
							function() {
								if (env.isFilteredPopup)
									self.filterPopupAccordingToSearch();

								$("#popup-image-count").trigger("click");
							}
						);
					}
				}
			}
		}

		if (! env.isPlayingMapClickHistoryInGetAlbum) {
			util.showHideSlideshowIcon();
			util.setSocialButtons(); // in generateMap
			menuF.updateMenu();
		}
	};

	MapAlbum.prototype.possiblyResetPopup = function(evt) {
		var shiftKey, ctrlKey;
		if (evt.shiftKey !== undefined) {
			shiftKey = evt.shiftKey;
			ctrlKey = evt.ctrlKey;
		} else {
			shiftKey = evt.originalEvent.shiftKey;
			ctrlKey = evt.originalEvent.ctrlKey;
		}

		// reset the thumbnails if not shift- nor ctrl-clicking
		if (! shiftKey && ! ctrlKey) {
			$("#popup-images-wrapper").html("");
			// $(".leaflet-popup-content").html("");
		}

		// close if no media left
		if (! this.media.length) {
			$("#loading").hide();
			if (util.isShiftOrControl())
				$(".shift-or-control .leaflet-popup-close-button")[0].click();
			if ($(".media-popup .leaflet-popup-close-button").length)
				$(".media-popup .leaflet-popup-close-button")[0].click();
		}
	};

	MapAlbum.prototype.calculationsWithClusters = function(evt) {
		// this function prepares some value which is needed by various functions

		let clusters = env.pruneCluster.Cluster._clusters;
		// decide what point is to be used: the nearest to the clicked position
		let minimumDistance = false, newMinimumDistance, distance, index;
		for (let i = 0; i < clusters.length; i ++) {
			distance = Math.abs(
				util.distanceBetweenCoordinatePoints(
					{lng: evt.latlng.lng, lat: evt.latlng.lat},
					{lng: clusters[i].averagePosition.lng, lat: clusters[i].averagePosition.lat}
				)
			);
			// console.log(i, distance);
			if (minimumDistance === false) {
				minimumDistance = distance;
				index = i;
			} else {
				newMinimumDistance = Math.min(minimumDistance, distance);
				if (newMinimumDistance != minimumDistance) {
					minimumDistance = newMinimumDistance;
					index = i;
				}
			}
		}
		let currentCluster = clusters[index];
		currentCluster.data.mediaList = [];

		// build the cluster's media name list
		let positionsAndMediaInCluster = new PositionsAndMedia([]);
		for (let i = 0; i < currentCluster._clusterMarkers.length; i ++) {
			currentCluster.data.mediaList = currentCluster.data.mediaList.concat(currentCluster._clusterMarkers[i].data.mediaList);
			positionsAndMediaInCluster.push(new PositionAndMedia(
					{
						lat: currentCluster._clusterMarkers[i].position.lat,
						lng: currentCluster._clusterMarkers[i].position.lng,
						mediaList: currentCluster._clusterMarkers[i].data.mediaList,
						count: currentCluster._clusterMarkers[i].data.mediaList.length
					}
				)
			);
		}

		let clusterPositionsAlreadyInPopup = new PositionsAndMedia([]);
		let clusterPositionsNotYetInPopup = new PositionsAndMedia([]);
		if (! this.isEmpty()) {
			for (let indexPositions = 0; indexPositions < positionsAndMediaInCluster.length; indexPositions ++) {
				let positionsAndMediaElement = positionsAndMediaInCluster[indexPositions];
				if (this.positionsAndMediaInTree.findIndex(element => positionsAndMediaElement.matchPosition(element)) !== -1)
					// the position was present
					clusterPositionsAlreadyInPopup.push(positionsAndMediaElement);
				else
					// the position was not present
					clusterPositionsNotYetInPopup.push(positionsAndMediaElement);
			}
		} else {
			clusterPositionsNotYetInPopup = positionsAndMediaInCluster;
		}

		var shiftKey, ctrlKey;
		if (evt.shiftKey !== undefined) {
			shiftKey = evt.shiftKey;
			ctrlKey = evt.ctrlKey;
		} else {
			shiftKey = evt.originalEvent.shiftKey;
			ctrlKey = evt.originalEvent.ctrlKey;
		}

		let mapClickHistoryElement = {
				latlng: currentCluster.averagePosition,
				shiftKey: shiftKey,
				ctrlKey: ctrlKey,
				zoom: env.mymap.getZoom(),
				center: {lat: env.mymap.getCenter().lat, lng: env.mymap.getCenter().lng}
		};

		return [
			currentCluster,
			positionsAndMediaInCluster,
			clusterPositionsAlreadyInPopup,
			clusterPositionsNotYetInPopup,
			shiftKey,
			ctrlKey,
			mapClickHistoryElement
		];
	};

	MapAlbum.prototype.addOrRemovePositionsThroughClick = function(evt) {
		// this function adds to or remove from the map album
		// the positions enclosed in the cluster specified inside the event argument
		// the other values in the map album, including the media, still remain to be added

		let self = this;
		let currentCluster, positionsAndMediaInCluster, clusterPositionsAlreadyInPopup, clusterPositionsNotYetInPopup;
		let shiftKey, ctrlKey, mapClickHistoryElement;

		if (evt !== null && evt.latlng !== undefined) {
			// $("#loading").show();

			[
				currentCluster,
				positionsAndMediaInCluster,
				clusterPositionsAlreadyInPopup,
				clusterPositionsNotYetInPopup,
				shiftKey,
				ctrlKey,
				mapClickHistoryElement
			] = self.calculationsWithClusters(evt);
		}

		if (ctrlKey) {
			if (! self.isEmpty()) {
				// control click: remove the points

				if (! env.isPlayingMapClickHistoryInGetAlbum) {
					self.map.clickHistory.push(mapClickHistoryElement);
					self.changeCacheBase(util.encodeMapCacheBase(self.map));
				}

				var matchingIndex;
				for (let indexPositions = 0; indexPositions < clusterPositionsAlreadyInPopup.length; indexPositions ++) {
					let positionsAndMediaElement = clusterPositionsAlreadyInPopup[indexPositions];
					matchingIndex = self.positionsAndMediaInTree.findIndex((element) => positionsAndMediaElement.matchPosition(element));
					// remove the position from the positions list
					self.positionsAndMediaInTree.splice(matchingIndex, 1);

					// // ...and the corresponding photos from the media list
					// for (let iMediaPosition = 0; iMediaPosition < positionsAndMediaElement.mediaList.length; iMediaPosition ++) {
					// 	mediaListElement = positionsAndMediaElement.mediaList[iMediaPosition];
					// 	matchingIndex = self.media.findIndex(singleMedia => singleMedia.isEqual(mediaListElement));
					// 	self.media.splice(matchingIndex, 1);
					// }
				}
				// self.numPositionsInTree = self.positionsAndMediaInTree.length;
			}
		} else {
			// not control click: add (with shift) or replace (without shift) the positions
			// imageLoadPromise = new Promise(
			// 	function(resolve_imageLoad) {
			var indexPositions;

			if (self.isEmpty() || ! shiftKey) {
				// normal click or shift click without previous content

				if (! env.isPlayingMapClickHistoryInGetAlbum) {
					if (! self.isEmpty()) {
						self.media = new Media([]);
						self.positionsAndMediaInTree = new PositionAndMedia([]);
					}
					self.map.clickHistory = [mapClickHistoryElement];
					self.changeCacheBase(util.encodeMapCacheBase(self.map));
				}
				// self.media = new Media([]);
				self.positionsAndMediaInTree = positionsAndMediaInCluster;
			} else if (clusterPositionsNotYetInPopup.length){
				// shift-click with previous content
				if (! env.isPlayingMapClickHistoryInGetAlbum) {
					self.map.clickHistory.push(mapClickHistoryElement);
					self.changeCacheBase(util.encodeMapCacheBase(self.map));
				}

				for (indexPositions = 0; indexPositions < clusterPositionsNotYetInPopup.length; indexPositions ++)
					self.positionsAndMediaInTree.push(clusterPositionsNotYetInPopup[indexPositions]);
			}
		}
	};

	MapAlbum.prototype.endPreparingMapAlbum = function() {
		let self = this;
		return new Promise(
			function(resolve_endPreparingMapAlbum, reject_endPreparingMapAlbum) {
				let mediaPromise = self.setMediaFromPositions();
				mediaPromise.then(
					function() {
						self.numsMedia = self.media.imagesAudiosVideosCount();
						self.numsMediaInSubTree = new ImagesAudiosVideos(self.numsMedia);
						self.numPositionsInTree = self.positionsAndMediaInTree.length;
						self.numsProtectedMediaInSubTree = new NumsProtected({",": self.numsMedia});
						delete self.mediaSort;
						delete self.mediaReverseSort;
						// now sort them according to options
						self.sortAlbumsMedia();

						// update the map root album in cache
						var rootMapAlbum = env.cache.getAlbum(env.options.by_map_string);
						if (! rootMapAlbum)
							rootMapAlbum = util.initializeOrGetMapRootAlbum();
						if (env.oldMapAlbum) {
							rootMapAlbum.numsMediaInSubTree.subtract(env.oldMapAlbum.numsMediaInSubTree);
							rootMapAlbum.subalbums = new Subalbums([]);
							rootMapAlbum.positionsAndMediaInTree.removePositionsAndMedia(self.positionsAndMediaInTree);
						}
						rootMapAlbum.numsMediaInSubTree.sum(self.numsMediaInSubTree);
						rootMapAlbum.subalbums.push(self);
						rootMapAlbum.positionsAndMediaInTree.mergePositionsAndMedia(self.positionsAndMediaInTree);
						rootMapAlbum.numPositionsInTree += self.positionsAndMediaInTree.length;

						resolve_endPreparingMapAlbum();
						// do not call bindSortEvents method on self: bindings have already been set for currentAlbum
					}
				);
			}
		);
	};

	MapAlbum.prototype.prepareAndDoPopupUpdate = function() {
		if (env.isPlayingMapClickHistoryInGetAlbum)
			env.currentAlbum = this;

		mapF.calculatePopupSizes();
		let popupLatLng;
		// let popupContent = "";

		if ($(".leaflet-popup-content").html()) {
		// if (env.popup) {
			// popupContent = $(".leaflet-popup-content").html();
			popupLatLng = env.popup.getLatLng();
			env.popup.remove();
			$(".leaflet-popup").remove();
		}
		$(".modal-close").hide();
		env.popup = L.popup(
			{
				maxWidth: env.maxWidthForPopupContent,
				maxHeight: env.maxHeightForPopupContent,
				autoPan: false,
				className: "media-popup"
			}
		);

		if (this.numPositionsInTree)
			env.popup.setLatLng(this.positionsAndMediaInTree.averagePosition());
		else
			env.popup.setLatLng(popupLatLng);

		env.popup.setContent(env.titleWrapper).openOn(env.mymap);
		// if (popupContent)
		// 	$(".leaflet-popup-content").html(popupContent);
		// else
		this.showMedia();

		this.updatePopup();
		if (! env.isPlayingMapClickHistoryInGetAlbum) {
			util.setSocialButtons(); // in prepareAndDoPopupUpdate
			this.setMediaSortDataAndAdaptCaption(true);
			if ($("#" + env.highlightedObjectId).length) {
				util.scrollPopupToHighlightedThumb($("#" + env.highlightedObjectId));
			}
		}

		$(".media-popup .leaflet-popup-close-button").off("click").on(
			"click",
			function() {
				$(".media-popup.leaflet-popup").html("");
				env.oldMapAlbum = null;
				if (! env.isResizing)
					env.isFilteredPopup = false;
				$(".modal-close").show();
				// focus the map, so that the arrows and +/- work
				$("#mapdiv").trigger("focus");
				$("#social").hide();
				menuF.updateMenu();
			}
		);

		mapF.addPopupMover();

		$(document).ready(
			function() {
				mapF.panMap();
			}
		);

		$("#loading").hide();
		menuF.updateMenu();
	};

	MapAlbum.prototype.setMediaFromPositions = function() {
		var self = this;
		return new Promise(
			function(resolve_setMediaFromPositions) {
				// function getMarkerClass(positionAndCount) {
				// 	var imgClass =
				// 		"popup-img-" +
				// 		(positionAndCount.lat / 1000).toString().replace(".", "") +
				// 		"-" +
				// 		(positionAndCount.lng / 1000).toString().replace(".", "");
				// 	return imgClass;
				// }
				// // end of nested function

				var mediaListElement, indexPositions, indexImage, photoIndex, mediaIndex;
				var photosByAlbum = {}, positionsAndMediaElement;

				// in order to add the html code for the images to a string,
				// we group the photos by album: this way we rationalize the process of getting them
				for (indexPositions = 0; indexPositions < self.positionsAndMediaInTree.length; indexPositions ++) {
					positionsAndMediaElement = self.positionsAndMediaInTree[indexPositions];
					// let markerClass = getMarkerClass(positionsAndMediaElement);
					for (indexImage = 0; indexImage < positionsAndMediaElement.mediaList.length; indexImage ++) {
						mediaListElement = positionsAndMediaElement.mediaList[indexImage];
						if (! photosByAlbum.hasOwnProperty(mediaListElement.foldersCacheBase)) {
							photosByAlbum[mediaListElement.foldersCacheBase] = [];
						}
						photosByAlbum[mediaListElement.foldersCacheBase].push(mediaListElement);
						// 	{
						// 		element: mediaListElement,
						// 		markerClass: markerClass
						// 	}
						// );
					}
				}

				// ok, now we can interate over the object we created and add the media to the map album
				self.media = new Media([]);
				self.numsMediaInSubTree = new ImagesAudiosVideos();
				// self.numPositionsInTree = 0;
				// self.numsProtectedMediaInSubTree = new NumsProtected();
				self.sizesOfAlbum = new Sizes();
				self.sizesOfSubTree = new Sizes();
				var cacheBasesPromises = [];
				for (var foldersCacheBase in photosByAlbum) {
					if (photosByAlbum.hasOwnProperty(foldersCacheBase)) {
						let cacheBasePromise = new Promise(
							function(resolve_cacheBasePromise) {
								let photosInAlbum = photosByAlbum[foldersCacheBase];
								var getAlbumPromise = phFl.getAlbum(foldersCacheBase, util.errorThenGoUp, {getMedia: true, getPositions: ! env.options.save_data});
								getAlbumPromise.then(
									function(theAlbum) {
										for (mediaIndex = 0; mediaIndex < theAlbum.numsMedia.imagesAudiosVideosTotal(); mediaIndex ++) {
											for (photoIndex = 0; photoIndex < photosInAlbum.length; photoIndex ++) {
												if (theAlbum.media[mediaIndex].cacheBase === photosInAlbum[photoIndex].cacheBase) {
													theAlbum.media[mediaIndex].generateCaptionsForPopup(theAlbum);
													self.media.push(theAlbum.media[mediaIndex]);
													self.numsMediaInSubTree.sum(new Media([theAlbum.media[mediaIndex]]).imagesAudiosVideosCount());
													self.sizesOfAlbum.sum(theAlbum.media[mediaIndex].fileSizes);
													self.sizesOfSubTree.sum(theAlbum.media[mediaIndex].fileSizes);
												}
											}
										}
										resolve_cacheBasePromise();
									},
									function() {
										console.trace();
									}
								);
							}
						);
						cacheBasesPromises.push(cacheBasePromise);
					}
				}
				Promise.all(cacheBasesPromises).then(
					function() {
						// self.numsMedia = self.media.imagesAudiosVideosCount();
						// if (env.options.expose_image_positions) {
						// 	self.positionsAndMediaInTree.mergePositionsAndMedia(self.positionsAndMediaInTree);
						// 	self.numPositionsInTree = self.positionsAndMediaInTree.length;
						// }
						resolve_setMediaFromPositions();
					}
				);
			}
		);
	};

	MapAlbum.prototype.generateMapFromSubalbum = function(subalbum, ev, from) {
		let self = this;
		var subalbumPromise = subalbum.toAlbum(util.errorThenGoUp, {getMedia: false, getPositions: ! env.options.save_data});
		let evData = ev.data;
		subalbumPromise.then(
			function(album) {
				// var album = env.currentAlbum.subalbums[iSubalbum];
				if (album.positionsAndMediaInTree.length) {
					ev.stopPropagation();
					ev.preventDefault();
					ev.data = evData;
					self.generateMap(album.positionsAndMediaInTree, ev, from);
				} else {
					$("#warning-no-geolocated-media").stop().fadeIn(200).fadeOut(3000);
				}
			},
			function() {
				console.trace();
			}
		);
	};

	MapAlbum.prototype.generateMapFromTitle = function(albumOrMedia, ev, from) {
		if (albumOrMedia.constructor.name == "SingleMedia") {
			this.generateMapFromSingleMedia(albumOrMedia, ev, from);
		} else if (albumOrMedia.positionsAndMediaInTree.length) {
			this.generateMap(albumOrMedia.positionsAndMediaInTree, ev, from);
		}
	};

	MapAlbum.prototype.generateMapFromTitleWithoutSubalbums = function(album, ev, from) {
		if (album.positionsAndMediaInMedia === undefined)
			album.generatePositionsAndMediaInMedia();
		if (album.positionsAndMediaInMedia.length)
			this.generateMap(album.positionsAndMediaInMedia, ev, from);
	};

	MapAlbum.prototype.generateMapFromSingleMedia = function(singleMedia, ev, from) {
		let pointList;
		ev.preventDefault();
		if (singleMedia) {
			if (singleMedia.hasGpsData()) {
				pointList = new PositionsAndMedia([singleMedia.generatePositionAndMedia()]);
			} else if (util.isPhp() && env.options.user_may_suggest_location && env.options.request_password_email) {
				pointList = new PositionsAndMedia([]);
			}
			this.generateMap(pointList, ev, from);
		}
	};

	MapAlbum.prototype.refreshPopup = function() {
		// reopens the (already closed) popup, possibly in order to sort differently the media inside it,
		// or because of some option change
		if (env.isFilteredPopup)
			this.filterMediaInPopupAccordingToSearch();

		this.endPreparingMapAlbum();
		this.prepareAndDoPopupUpdate();
	};

	MapAlbum.prototype.updatePopup = function() {
		util.setMediaOptions();
		env.popup.setContent($(".media-popup .leaflet-popup-content").html());
		mapF.calculatePopupSizes();
		$(".media-popup .leaflet-popup-content").css("max-width", env.maxWidthForPopupContent + "px");
		$(".media-popup .leaflet-popup-content").css("min-width", env.options.media_thumb_size + "px");

		$("#popup-image-count").css("max-width", env.maxWidthForPopupContent + "px");
		if (this.numPositionsInTree)
			env.popup.setLatLng(this.positionsAndMediaInTree.averagePosition());
		this.buildPopupHeader();

		$("#popup-images-wrapper").css("max-height", "");
		mapF.setPopupPosition();
		util.addMediaLazyLoader(false);
		$(document).ready(
			function() {
				$("#popup-images-wrapper").css(
					"max-height",
					(
						$(".media-popup .leaflet-popup-content").outerHeight() - $(".media-popup #popup-image-count").outerHeight()
					) + "px"
				);
			}
		);

		if (this.media.length) {
			var highlightedObject = $("#" + util.highlightedObject().attr("id"));
			if (util.isPopup()) {
				highlightedObject = $("#popup-images-wrapper").children().first();
				util.scrollPopupToHighlightedThumb(highlightedObject);
			}

			util.highlightSearchedWords();

			this.bindMediaSortEvents();
		}

		// menuF.updateMenu();
	};


	MapAlbum.prototype.filterMediaInPopupAccordingToSearch = function() {
		// filter according to the search

		// var removedMedia = [];
		let filteredMedia = new Media(this.media);
		if (env.search.anyWord) {
			// at least one word
			filteredMedia.filterMediaAgainstSearchedAlbum(env.mapAlbum);
			let mediaResult = new Media([]);
			env.search.wordsFromUserNormalizedAccordingToOptions.forEach(
				function(searchWord) {
					filteredMedia.filterMediaAgainstOneWord(env.search, searchWord);
					mediaResult = util.arrayUnion(mediaResult, filteredMedia, function(a, b) {return a.isEqual(b);});
				}
			);
			return mediaResult;
		} else {
			// every word
			filteredMedia.filterMediaAgainstEveryWord(env.search);
			return filteredMedia;
		}

		// removedMedia.forEach(
		// 	ithMedia => {
		// 		this.positionsAndMediaInTree.removeSingleMedia(ithMedia);
		// 	}
		// );
		//
		// let endPreparingPromise = this.endPreparingMapAlbum();
		// endPreparingPromise.then(
		// 	function() {
		// 		self.prepareAndDoPopupUpdate();
		// 		if (! env.search.insideWords && env.search.removedStopWords.length) {
		// 			// say that some search word hasn't been used
		// 			let stopWordsFound = " - <span class='italic'>" + env.search.removedStopWords.length + " " + util._t("#removed-stopwords") + ": ";
		// 			for (let i = 0; i < env.search.removedStopWords.length; i ++) {
		// 				if (i)
		// 					stopWordsFound += ", ";
		// 				stopWordsFound += env.search.removedStopWords[i];
		// 			}
		// 			stopWordsFound += "</span>";
		// 			$("#popup-image-count").append(stopWordsFound);
		// 		}
		// 	}
		// );
	};

	MapAlbum.prototype.buildPopupHeader = function() {
		let self = this;
		let filteredOutLength = $("#popup-images-wrapper .thumb-and-caption-container.filtered-out").length;
		if (self.numsMedia.imagesAudiosVideosTotal() - filteredOutLength)
			$("#popup-image-count").html(
				"<span id='popup-image-count-number'>" + (
				self.numsMedia.imagesAudiosVideosTotal() - filteredOutLength
			).toString() +
				"</span> " + util._t("#images")
			);
		else
			$("#popup-image-count").html(util._t("#no-images"));
		$("#popup-image-count").css("max-width", env.maxWidthForPopupContent);

		// add the click event for showing the photos in the popup as an album
		$("#popup-image-count").off("click").on(
			"click",
			function() {
				env.highlightedObjectId = null;
				if (util.isShiftOrControl())
					$(".shift-or-control .leaflet-popup-close-button")[0].click();
				if ($(".media-popup .leaflet-popup-close-button").length)
					$(".media-popup .leaflet-popup-close-button")[0].click();
				if ($(".modal-close").length)
					$('.modal-close')[0].click();
				env.popupRefreshType = "previousAlbum";
				env.mapRefreshType = null;

				// self.endPreparingMapAlbum();
				$("#album-view").addClass("hidden");
				$("#loading").show();

				let albumCacheBase = self.cacheBase;
				if (filteredOutLength) {
					let searchObject = env.mapAlbum.search;
					searchObject.searchedCacheBase = env.mapAlbum.cacheBase;
					albumCacheBase = phFl.encodeSearchCacheBase(searchObject);
				}
				window.location.href = env.hashBeginning + albumCacheBase;
			}
		);
	};
}());
//# sourceURL=048-map-methods-bis.js
