(function() {

	var util = new Utilities();
	var phFl = new PhotoFloat();

	Subalbum.prototype.visibleNumsMediaInSubTree = function() {
		if (util.geotaggedContentIsHidden())
			return this.nonGeotagged.numsMediaInSubTree;
		else
			return this.numsMediaInSubTree;
	};

	Subalbum.prototype.hasGpsData = function() {
		return env.options.expose_image_positions && this.nonGeotagged.numsMediaInSubTree.imagesAudiosVideosTotal() === 0;
	};

	Subalbum.prototype.isEqual = function(otherSubalbum) {
		return otherSubalbum !== null && this.cacheBase === otherSubalbum.cacheBase;
	};

	Subalbum.prototype.toAlbum = function(error, {getMedia = false, getPositions = false}) {
		var self = this;
		return new Promise(
			function(resolve_convertIntoAlbum) {
				let promise;
				if (self.hasOwnProperty("numsProtectedMediaInSubTree"))
					promise = phFl.getAlbum(self.cacheBase, error, {getMedia: getMedia, getPositions: getPositions}, self.numsProtectedMediaInSubTree);
				else
					promise = phFl.getAlbum(self.cacheBase, error, {getMedia: getMedia, getPositions: getPositions});
				promise.then(
					function(convertedSubalbum) {
						let properties = [
							"randomMedia",
							"captionsForSelection",
							"captionForSelectionSorting",
							"captionsForSearch",
							"captionForSearchSorting",
							"unicodeWords",
							"words",
							"tags"
						];
						properties.forEach(
							function(property) {
								if (self.hasOwnProperty(property)) {
									// transfer subalbums properties to the album
									convertedSubalbum[property] = self[property];
								}
							}
						);
						resolve_convertIntoAlbum(convertedSubalbum);
					}
				);
			}
		);
	};

	Subalbum.prototype.isFolder = function() {
		return util.isFolderCacheBase(this.cacheBase);
	};

	Subalbum.prototype.isByDate = function() {
		return util.isByDateCacheBase(this.cacheBase);
	};

	Subalbum.prototype.isByGps = function() {
		return util.isByGpsCacheBase(this.cacheBase);
	};

	Subalbum.prototype.isSearch = function() {
		return util.isSearchCacheBase(this.cacheBase);
	};

	Subalbum.prototype.isSelection = function() {
		return util.isSelectionCacheBase(this.cacheBase);
	};

	Subalbum.prototype.isMap = function() {
		return util.isMapCacheBase(this.cacheBase);
	};

	Subalbum.prototype.isTransversal =  function() {
		return this.isByDate() || this.isByGps();
	};

	Subalbum.prototype.isGenerated =  function() {
		return this.isTransversal() || this.isCollection();
	};

	Subalbum.prototype.isInFoundSubalbums = function(album) {
		// if (! util.somethingIsSearched())
		// 	return false;
		// else {
		var foundAlbum = album.subalbums.find(
			aSubalbum => this.cacheBase.startsWith(aSubalbum.cacheBase)
		);
		if (typeof foundAlbum !== "undefined")
			return foundAlbum;
		else
			return false;
		// }
	};

	Subalbum.prototype.albumSelectBoxSelector = function() {
		return util.albumSelectBoxSelector(this.cacheBase);
	};

	Subalbum.prototype.isSelected = function(selectionAlbum = env.selectionAlbum) {
		return util.albumIsSelected(this, selectionAlbum);
	};

	// Subalbum.prototype.isInsideAlbumToSearchIn = function(album) {
	// 	return(
	// 		// check whether the subalbum is inside the current album tree
	// 		util.isSearchCacheBase(album.search.searchedCacheBase) &&
	// 		this.isInFoundSubalbums(env.cache.getAlbum(album.search.searchedCacheBase)) ||
	// 		util.isSelectionCacheBase(album.search.searchedCacheBase) &&
	// 		this.isInsideSelectedAlbums() ||
	// 		util.isAnyRootCacheBase(album.search.searchedCacheBase) || (
	// 			this.cacheBase.startsWith(album.search.searchedCacheBase) &&
	// 			this.cacheBase !== album.search.searchedCacheBase
	// 		)
	// 	);
	// };

	Subalbum.prototype.isInsideSelectedAlbums = function(selectionAlbum = env.selectionAlbum) {
		util.isInsideSelectedAlbums(selectionAlbum, this);
	};

	Subalbum.prototype.nameForShowing = function(parentAlbum, html = false, br = false) {
		return util.nameForShowing(this, parentAlbum, html, br);
	};

	Subalbum.prototype.hasProperty = function(property) {
		return util.hasProperty(this, property);
	};

	Subalbum.prototype.hasSomeDescription = function(property = null) {
		return util.hasSomeDescription(this, property);
	};

	Subalbum.prototype.toggleSubalbumSelection = function(baseAlbum, selectionAlbum) {
		let self = this;
		return new Promise(
			function(resolve_toggleSubalbumSelection) {
				$.executeAfterEvent(
					"otherJsFilesLoadedEvent",
					function() {
						if (self.isSelected(selectionAlbum)) {
							let removeSubalbumPromise = baseAlbum.removeSubalbumFromSelection(self, selectionAlbum);
							removeSubalbumPromise.then(
								function subalbumRemoved() {
									resolve_toggleSubalbumSelection();
								}
							);
						} else {
							let addSubalbumPromise = baseAlbum.addSubalbumToSelection(self, selectionAlbum);
							addSubalbumPromise.then(
								function subalbumAdded() {
									delete selectionAlbum.mediaSort;
									delete selectionAlbum.mediaReverseSort;
									selectionAlbum.sortAlbumsMedia();

									resolve_toggleSubalbumSelection();
								}
							);
						}
					}
				);
			}
		);
	};
}());
