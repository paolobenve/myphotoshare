<?php
	$jsonString = file_get_contents("cache/options.json");
	if (! $jsonString) {
		echo "Missing options.json file in cache dir. Either:" .
			"<ul>" .
				"<li>Your albums are not indexed yet:" .
					"<ul>" .
						"<li>maybe the scanner hasn't completed its run: be patient until it finishes indexing the media</li>" .
						"<li>if an error has occurred in the scanner, <a href='https://gitlab.com/paolobenve/myphotoshare/-/issues'>please report the issue</a> so that it can be investigated</li>" .
					"</ul>" .
				"</li>" .
				"<li>Your web site configuration is wrong:" .
					"<ul>" .
						"<li>double check that the directories you set in the web site configuration are the same that you set in the scanner</li>" .
					"</ul>" .
				"</li>" .
			"</ul>" .
			"If you are not the site owner, please report the issue to him/her";
		exit;
	}
	$options = json_decode($jsonString, true);

	// var_dump($options);

	if (! array_key_exists("page_title", $options))
		$options["page_title"] = "My photos";

	if (gettype($options["page_title"]) == "string") {
		$options["page_title"] = [
			"en" => $options["page_title"],
			"it" => $options["page_title"],
			"es" => $options["page_title"],
			"fr" => $options["page_title"],
			"sk" => $options["page_title"]
		];
	}

	if (! array_key_exists("debug_css", $options))
		$options["debug_css"] = "false";
	if (! array_key_exists("debug_js", $options))
		$options["debug_js"] = "false";
	if (! array_key_exists("use_system_js_libraries", $options))
		$options["use_system_js_libraries"] = "false";
	if (! array_key_exists("save_data", $options))
		$options["save_data"] = "false";
	if (! array_key_exists("tiny_urls_file", $options))
		$options["tiny_urls_file"] = "_tiny_urls.json";
	if (! array_key_exists("user_may_suggest_location", $options))
		$options["user_may_suggest_location"] = "true";
	if (! array_key_exists("language", $options) || ! array_key_exists($options["language"], $options["page_title"]))
		$options["language"] = "en";
	if (! array_key_exists("use_internal_modernizr", $options))
		$options["use_internal_modernizr"] = "true";
	// echo "--------------------\n";
	// var_dump($options);

	// Check if an option is true or 1
	function is_option_set($option_name) {
		global $options;
		return strcasecmp($options[$option_name], "true") == 0 || $options[$option_name] == "1";
	}

	// Check if an option as a list contains a given value
	function has_option_value($option_name, $option_value) {
		global $options;
		return stripos($options[$option_name], $option_value) !== false;
	}

	function removeParamFromUrl($url, $param) {
		// from https://stackoverflow.com/questions/4937478/strip-off-specific-parameter-from-urls-querystring
		$url = preg_replace("/(&|\?)" . preg_quote($param) . "=[^&]*$/", "", $url);
		$url = preg_replace("/(&|\?)" . preg_quote($param) . "=[^&]*&/", "$1", $url);
		return $url;
	}

	function removeRParameter($url) {
		$urlWithoutR = removeParamFromUrl($url, "r");
		return $urlWithoutR;
	}

	$compositeImage = "";
	if (! empty($_POST) && ! empty($_POST["longurl"]) || ! empty($_GET) && ! empty($_GET["t"])) {
		$tinyUrlsFile = "cache/" . $options["tiny_urls_file"];
		// Open the file to get existing content
		$tinyUrls = json_decode(file_get_contents($tinyUrlsFile), true);
		if (! $tinyUrls)
			$tinyUrls = [];
		else
			// remove the r (random number) parameter from all the urls
			foreach($tinyUrls as $key => $url) {
				$tinyUrls[$key] = removeRParameter($url);
			}
		// error_log($tinyUrls["1"][1]);

		if (! empty($_POST["longurl"])) {
			// save to disk the long url with its tiny version, and return the tiny version

			// remove the r parameter, see https://www.codexworld.com/how-to/remove-specific-parameter-from-url-query-string-php/
			$longUrl = removeRParameter($_POST["longurl"]);

			// look for the url in the array
			$reducedMd5 = array_search($longUrl, $tinyUrls);

			if ($reducedMd5 !== false) {
				// the url is there => return the tiny url
				echo $reducedMd5;
				exit(0);
			}

			// the url isn't there
			$md5 = md5($longUrl);
			$nDigits = 1;
			while (true) {
				$reducedMd5 = substr($md5, 0, $nDigits);
				if (! array_key_exists($reducedMd5, $tinyUrls) && $reducedMd5 !== "0")
					break;
				$nDigits += 1;
			}

			// error_log(json_encode($_POST));

			if (! empty($_POST["compositeimage"])) {
				$parsedLongUrl = parse_url($longUrl, PHP_URL_QUERY);
				// save the longUrl parameters as values in the $_get associative array
				parse_str($parsedLongUrl, $_get);

				// save the composite image to disk
				// first replace the url-safe characters with the correct ones
				$compositeImage = strtr($_POST["compositeimage"], "._-", "+/=");
				$compositeImageData = explode(';', $compositeImage)[1];
				$compositeImageData = base64_decode(explode(',', $compositeImageData)[1]);
				// calculate the image size
				if (empty($_get["w"])) {
					$imageSize = getimagesizefromstring($compositeImageData);
					$longUrl = str_replace("?url=", "?w=" . $imageSize[0] . "&h=" . $imageSize[1] . "&url=", $longUrl);
					$_get["w"] = $imageSize[0];
					$_get["h"] = $imageSize[1];
				}
				// save the image with the tiny url parameter as name in order to avoid the "file name too long" error
				file_put_contents("cache/" . $options["composite_images_subdir_js"] . "/" . $reducedMd5 . ".jpg", $compositeImageData);

			}
			$tinyUrls[$reducedMd5] = $longUrl;
			// Write the contents back to the file
			file_put_contents($tinyUrlsFile, json_encode($tinyUrls));

			// return the reduced tiny url
			echo $reducedMd5;
			exit(0);
		} else if (! empty($_GET["t"])) {
			if (array_key_exists($_GET["t"], $tinyUrls)) {
				$url = $tinyUrls[$_GET["t"]];
				$longUrl = $url;

				$parsedLongUrl = parse_url($longUrl, PHP_URL_QUERY);
				// save the longUrl parameters as values in the $_get associative array
				parse_str($parsedLongUrl, $_get);
			} else {
				echo "Wrong tiny url";
				exit(1);
			}
		}
	}

	// send the email for requesting the protected content password
	if (
		! empty($_GET) && ! empty($_GET["email"])
		&& array_key_exists("request_password_email", $options)
		&& ! empty($options["request_password_email"])
	) {
		$subject = "Password request";
		$message =
			"This request has been sent from " . $_GET["siteurl"] . "\r\n\r\n" .
			"'" . $_GET["name"] . "' <" . $_GET["email"] . ">  says:" . "\r\n\r\n" .
			$_GET["identity"];
		$from = "'myphotoshare' <" . $options["request_password_email"] . ">";
		$headers =
			"From: " . $from . "\r\n" .
			"Reply-To: '" . $_GET["name"] . "' <" . $_GET["email"] . ">" . "\r\n" .
			"X-Mailer: PHP/" . phpversion();
		$result = mail($options["request_password_email"], $subject, $message, $headers, "-f " . $from);
		if (! $result) {
			echo "mail not sent:" . error_get_last()["message"];
			echo "<br />mail command = mail('" .$options["request_password_email"] . "', '" . $subject . "', '" . $message . "')";
			echo "<br />subject = " . $subject;
			echo "<br />nessage = " . $message;
			exit(1);
		} else {
			echo "email sent!";
			exit(0);
		}
	}

	// send the email for suggesting a photo geolocation
	if (
		! empty($_GET) && ! empty($_GET["photo"])
		&& array_key_exists("request_password_email", $options)
		&& ! empty($options["request_password_email"])
		&& is_option_set("user_may_suggest_location")
	) {
		echo "preparing the email...";
		$subject = "Suggestion for geolocation of " . $_GET["photo"];
		$mediarealpath = realpath($_GET["photo"]);
		if ($mediarealpath === false) {
			echo "<br />mail not sent: PHP's <em>realpath</em> function returned false: missing file '" . $_GET["photo"] . "', or wrong folder permissions";
			exit(1);
		}
		$exiftoolArguments =
			" -GPSLatitude*=" . $_GET["lat"] .
			" -GPSLongitude*=" . $_GET["lng"] .
			" '" .
			str_replace("'", "'\''", $mediarealpath) .    # see https://stackoverflow.com/questions/1250079/how-to-escape-single-quotes-within-single-quoted-strings?r=Saves_UserSavesList
			"'";

		$message =
			"Someone at" .
			"\r\n" .
				"    " . $_GET["siteurl"] .
				"\r\n" .
			"suggested that the media" .
			"\r\n" .
				"    " . $mediarealpath .
				"\r\n" .
			"be geolocated at:" .
			"\r\n" .
				"    lat = " . $_GET["lat"] . "\r\n" .
				"    lng = " . $_GET["lng"] .
				"\r\n" .
				"\r\n" .
			"Check the new position:" .
			"\r\n" .
				"    https://www.openstreetmap.org/#map=10/" . $_GET["lat"] . "/" . $_GET["lng"] .
				"\r\n" .
				"\r\n" .
			"Apply it" .
			"\r\n" .
				"    overwriting the original non-geotagged media:" .
				"\r\n" .
					"        exiftool -overwrite_original" . $exiftoolArguments .
				"\r\n" .
				"\r\n" .
				"    without overwriting the original non-geotagged media:" .
					"\r\n" .
					"        exiftool " . $exiftoolArguments;

		$from = "myphotoshare <" . $options["request_password_email"] . ">";
		$headers =
			"From: " . $from . "\r\n" .
			"X-Mailer: PHP/" . phpversion();
		echo "<br /><br />sending the email with message:<br /><br />";
		echo $message;
		echo "<br /><br />to " . $options["request_password_email"] . "...";
		$result = mail($options["request_password_email"], $subject, $message, $headers, "-f " . $from);
		if (! $result) {
			echo "<br /><br />mail not sent: " . error_get_last()["message"];
			exit(1);
		} else {
			echo "<br /><br />email sent!";
			exit(0);
		}
	}

	if (! empty($_GET) && ! empty($_GET["t"]) || ! empty($_GET["m"])) {
		if (! empty($_GET["m"])) {
			// make redirect work with long urls too
			$_get = $_GET;
		}

		$completeUrl = $_get["url"];
		if (! empty($_get["hash"]))
			$completeUrl .= "#" . $_get["hash"];

		// redirect to the correct page without the tiny url parameter used for sharing
		echo "
			<script>
				document.addEventListener(
					'DOMContentLoaded',
					function(event) {
						window.location.href = '" . $completeUrl . "';
					}
				);
			</script>
		";
	} ?>
<!DOCTYPE html>
<html lang="en">
<!-- FROM PHP -->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, user-scalable=no">
		<meta name="fragment" content="!" />
		<meta name="medium" content="image" />

		<title><?php // if ($options["page_title"][$options["language"]])
			echo $options["page_title"][$options["language"]];
		?></title>

		<link rel="icon" href="favicon.ico" type="image/x-icon"/>

		<?php
		// receive the post data, they contain the compressed stringified map/selection/search albums
		// echo "<pre>";
		// var_dump($_POST);
		// var_dump(json_encode($_POST));
		// echo "</pre>";
		function passPostArrayToJavascript(){
			if (! empty($_POST)) {
				echo "
				<script>
					var postData = " . json_encode($_POST) . "; // alert(postData['packedAlbum']);
					// var popupRefreshType = null;
					// var mapRefreshType = null;
				</script>" . "\n\n";
			}
		}
		passPostArrayToJavascript();
		// no, if postdata isn't undefined, javascript's postData["packedAlbum"] contains the compressed stringified album

		echo "<script>
			function isPhp() {}
		</script>" . "\n\n";

		//~ ini_set("display_errors", 1);
		//~ error_reporting(E_ALL);
		// from http://skills2earn.blogspot.it/2012/01/how-to-check-if-file-exists-on.html , solution # 3
		function url_exist($url) {
			if (@fopen($url,"r"))
				return true;
			else
				return false;
		}

		if (! empty($_get["m"]) || ! empty($_GET) && ! empty($_GET["t"])) {
			if (! empty($_GET) && ! empty($_GET["t"]))
				$compositeImageJs = "cache/" . $options["composite_images_subdir_js"] . "/" . $_GET["t"] . ".jpg";
			if (! empty($_GET) && ! empty($_GET["t"]) && file_exists($compositeImageJs)) {
				// the image name is a tiny one
				$mediaPath = $compositeImageJs;
			} else if (! empty($_get["m"])) {
				$mediaPath = urldecode($_get["m"]);
				// Prevent directory traversal security vulnerability
				if (strpos(realpath($mediaPath), realpath("cache")) !== 0 || ! url_exist(realpath($mediaPath))) {
					// use the MyPhotoShare logo instead
					$mediaPath = "img/myphotoshareLogo.jpg";
				}
			}

			// put the <link rel=".."> tag in <head> for letting facebook/google+ load the media when sharing
			$linkTag = "<link rel='";
			$imageEnd = ".jpg";
			$videoEnd = ".mp4";
			$audioEnd = ".mp3";
			if ($compositeImage || substr($mediaPath, - strlen($imageEnd)) === $imageEnd)
				// image
				$linkTag .= "image_src";
			else if (substr($mediaPath, - strlen($audioEnd)) === $audioEnd)
				// audio
				$linkTag .= "audio_src";
			else if (substr($mediaPath, - strlen($videoEnd)) === $videoEnd)
				// video
				$linkTag .= "video_src";
			if ($compositeImage) {
				$linkTag .= "' href='" . $compositeImage . "'";
			} else {
				$linkTag .= "' href='" . $mediaPath . "'";
			}
			$linkTag .= ">";
			echo "\n$linkTag\n";

			// put the <meta property=".."> tags in <head> for letting facebook/google+/etc load the image/video when sharing

			$title = urldecode($_get["title"]);
			echo "\n" .
					"<meta property='og:title' content='" . $title . "' />" . "\n";

			echo "\n" .
					"<meta property='og:type' content='website' />" . "\n";

			// security: myphotoshare hashes only has letter, numbers, underscores and dashes
			$hash = preg_replace("/[^-_a-z0-9]/i", "", urldecode($_get["hash"]));
			$urlWithoutHash = urldecode($_get["url"]);
			//$urlWithHash = $hash ? $urlWithoutHash . "#" . $hash : $urlWithoutHash;

			$pageUrl = "http" . (($_SERVER["SERVER_PORT"] == 443) ? "s" : "") . "://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
			echo "\n" .
					"<meta property='og:url' content='" . htmlentities($pageUrl) . "' />" . "\n";

			$description = empty($_get["desc"]) ? $title : urldecode($_get["desc"]);
			echo "\n" .
					"<meta name='description' content='" . $description . "' />" . "\n";
			echo "\n" .
					"<meta property='og:description' content='" . $description . "' />" . "\n";

			$image = htmlentities($urlWithoutHash . $mediaPath);
			// error_log($mediaPath);
			// error_log($image);

			echo "\n" .
					"<meta property='og:image' itemprop='image' content='" . $image . "' />" . "\n";

			echo "\n" .
					"<meta property='og:image:type' content='image/jpg' />" . "\n";

			if (! empty($_get["w"]) && ctype_digit($_get["w"]))
				echo "\n" .
					"<meta property='og:image:width' content='" . $_get["w"] . "'>" . "\n";
			if (! empty($_get["h"]) && ctype_digit($_get["h"]))
				echo "\n" .
					"<meta property='og:image:height' content='" . $_get["h"] . "'>" . "\n";
		} ?>
	</head>
	<body>
		<div id="fullscreen-wrapper">
			<div id="nothing-to-search" class="search-failed"></div>
			<div id="no-search-string" class="search-failed"></div>
			<div id="no-search-string-after-stopwords-removed" class="search-failed"></div>
			<div id="no-results" class="search-failed"></div>
			<div id="search-too-wide" class="search-failed"></div>

			<!-- The map -->
			<div id="my-modal" class="modal">
				<!-- Modal content -->
				<div class="modal-content">
					<div id="mapdiv"></div>
					<div class="modal-close close-button"></div>
					<div class="map-marker-centered"></div>
					<div class="map-marker-centered-send-suggestion"></div>
				</div>
			</div>

			<div id="auth-text" style="display: none;">
				<div id="auth-close" class="close-button"></div>
				<form id="auth-form">
					<div id="auth-question"></div>
					<input id="username" type="text" value="username" autocomplete="username" style="display: none;">
					<input id="password" type="password" autocomplete="current-password">
					<input type="submit" value="⏎" class="button"/>
				</form>
				<div>
					<div id="request-password"></div>
					<form id="password-request-form">
						<div id="enter-your-data"></div>
						<div id="please-fill"></div>
						<input type="hidden" name="requestpassword">
						<span id="name-label"></span>
						<input id="form-name" type="text" autocomplete="name" name="name">
						<span id="email-label" class="space-before"></span>
						<input id="form-email" type="text" autocomplete="email" name="email">
						<span id="identity" class="space-before">
							<span id="identity-label"></span>
							<input id="form-identity" type="text" name="identity">
						</span>
						<input type="submit" value="⏎" class="button"/>
					</form>
				</div>
			</div>

			<img id="up-button" width="100" height="49" alt="^" data-src="img/album.png" class="lazyload-page" style="display: none;">
			<div id="album-and-media-container">
				<div class="hidden" id="media-view">
					<div id="media-box-container">
						<div class="media-box" id="center">

							<div class="media-box-inner">
							</div>

							<div class="media-bar" style="display: none;">
								<div class="metadata"></div>
								<div class="links">
									<span class="link-button">
										<a class="metadata-show"></a>
										<a class="metadata-hide"></a>
										<a class="menu-divider">|</a>
									</span>
									<span class="link-button">
										<a class="original-link"></a>
										<a class="menu-divider">|</a>
									</span>
									<span class="link-button">
										<a class="map-link"></a>
										<span class="menu-divider">|</span>
									</span>
									<span class="fullscreen">
										<span class="link-button">
											<a class="enter-fullscreen"></a>
											<a class="exit-fullscreen"></a>
										</span>
									</span>
								</div>
							</div>

						</div>
					</div>

					<img id="prev" width="42" height="88" alt="<" data-src="img/prev.jpg" class="lazyload-page" style="display: none;">
					<img id="next" width="42" height="88" alt=">" data-src="img/next.jpg" class="lazyload-page" style="display: none;">
					<a>
						<img alt="[]" id="select-box" data-src="img/checkbox-unchecked-48px.png" class="select-box lazyload-page" style="display: none;">
					</a>
					<div id="pinch-container" class="hidden" style="display: none;">
						<img data-src="img/pinch-plus.png" alt="+" id="pinch-in" width="25" height="25" class="pinch lazyload-page">
						<img data-src="img/pinch-minus.png" alt="-" id="pinch-out" width="25" height="25" class="pinch disabled lazyload-page">
					</div>
					<div id="map-button-container" class="hidden map-popup-trigger"></div>
				</div>

				<div id="album-view">
					<div class="title">
						<span class="title-string"></span>
					</div>

					<div id="subalbums"></div>
					<div id="message-too-many-media" role="alert"></div>
					<div id="thumbs"></div>
					<div id="powered-by">
						<span id="powered-by-string">Powered by</span>
						<a href="https://gitlab.com/paolobenve/myphotoshare" target="_blank">MyPhotoShare</a>
					</div>
				</div>
			</div>

			<div id="loading" class="messages" style="display: block;">Loading...</div>
			<div id="working" class="messages"></div>
			<div id="downloading-and-zipping" class="messages">
				<div id="downloading-media-close" class="close-button"></div>
				<div id="downloading-media"></div>
				<div id="preparing-zip-text"></div>
				<div id="file-name-for-adding-to-zip"></div>
				<div id="preparing-zip-closing"></div>
			</div>
			<div id="sending-email" class="messages"></div>
			<div id="email-sent" class="messages"></div>
			<div id="email-not-sent" class="messages"></div>
			<div id="you-can-suggest-single-media-position" class="messages"></div>
			<div id="sending-single-media-position" class="messages"></div>
			<div id="tiny-url" class="messages"></div>
			<div id="settings-restored" class="messages"></div>
			<div id="started-slideshow" class="messages"></div>
			<div id="slideshow-buttons" style="display: none;">
				<div id="started-slideshow-interval-wrapper" class="slideshow-button">
					<div id="started-slideshow-interval"></div>
					<div id="started-slideshow-slow-down" class="slideshow-button">-</div>
					<div id="started-slideshow-speed-up" class="slideshow-button">+</div>
				</div>
				<div id="started-slideshow-pause" class="slideshow-button">
					<img data-src="img/pause.png" alt="pause" width="37" height="30" class="lazyload-page">
				</div>
				<div id="started-slideshow-play" class="slideshow-button">
					<img data-src="img/play.png" alt="play" width="37" height="30" class="lazyload-page">
				</div>
				<div id="started-slideshow-stop" class="slideshow-button">
					<img data-src="img/stop.png" alt="stop" width="37" height="30" class="lazyload-page">
				</div>
				<div id="started-slideshow-orientation-auto" class="slideshow-button">
					<img data-src="img/orientation-auto.png" alt="stop" width="35" height="37" class="lazyload-page">
				</div>
			</div>

			<div id="contextual-help" class="messages" style="display: none;">
				<div class="shortcuts-title"></div>
				<div class="shortcuts-body">
					<table class="shortcuts">
						<tr>
							<th class="table-title-scope"></th>
							<th class="table-title-shortcut"></th>
							<th class="table-title-action"></th>
						</tr>
						<tr>
							<td class="scope any"></td>
							<td class="shortcut italic">ESC</td>
							<td class="shortcut-help esc"></td>
						</tr>
						<tr>
							<td class="scope menu"></td>
							<td class="shortcut"><span class="italic">TAB</span>, <span class="arrow-down italic"></span></td>
							<td class="shortcut-help highlight-next-in-menu"></td>
						</tr>
						<tr>
							<td class="scope menu"></td>
							<td class="shortcut"><span class="shift italic"></span>-<span class="italic">TAB</span>, <span class="arrow-up italic"></span></td>
							<td class="shortcut-help highlight-previous-in-menu"></td>
						</tr>
						<tr>
							<td class="scope menu"></td>
							<td class="shortcut"><span class="arrow-left italic"></span>, <span class="arrow-right italic"></span></td>
							<td class="shortcut-help toggle-main-search-menu"></td>
						</tr>
						<tr>
							<td class="scope expandable-menu"></td>
							<td class="shortcut"><span class="enter italic"></span>, <span class="space italic"></span></td>
							<td class="shortcut-help toggle-menu-expansion"></td>
						</tr>
						<tr>
							<td class="scope menu-command"></td>
							<td class="shortcut"><span class="enter italic"></span>, <span class="space italic"></span></td>
							<td class="shortcut-help activate-menu-entry"></td>
						</tr>
						<tr>
							<td class="scope any-but-menu"></td>
							<td class="shortcut enter-fullscreen-shortcut bold"></td>
							<td class="shortcut-help enter-fullscreen"></td>
						</tr>
						<tr>
							<td class="scope any-but-menu"></td>
							<td class="shortcut open-menu-shortcut bold"></td>
							<td class="shortcut-help open-menu"></td>
						</tr>
						<tr>
							<td class="scope any-but-menu"></td>
							<td class="shortcut map-link-shortcut bold"></td>
							<td class="shortcut-help map-link"></td>
						</tr>
						<tr>
							<td class="scope any-but-menu"></td>
							<td class="shortcut hide-everytyhing-shortcut bold"></td>
							<td class="shortcut-help toggle-title"></td>
						</tr>
						<tr>
							<td class="scope any-but-menu"></td>
							<td class="shortcut select everything-here-shortcut bold"></td>
							<td class="shortcut-help select everything-here"></td>
						</tr>
						<tr>
							<td class="scope root-albums-and-single-media"></td>
							<td class="shortcut"><span class="bold">&gt;</span> / <span class="bold">&lt;</span></td>
							<td class="shortcut-help change-browsing-mode"></td>
						</tr>
						<tr>
							<td class="scope album-and-single-media"></td>
							<td class="shortcut slideshow-icon-title-shortcut bold"></td>
							<td class="shortcut-help start-slideshow"></td>
						</tr>
						<tr>
							<td class="scope album"></td>
							<td class="shortcut enter italic"></td>
							<td class="shortcut-help enter-highlighted"></td>
						</tr>
						<tr>
							<td class="scope album"></td>
							<td class="shortcut"><span class="arrow-right italic"></span> / <span class="arrow-left italic"></span></td>
							<td class="shortcut-help move-highlighting"></td>
						</tr>
						<tr>
							<td class="scope album"></td>
							<td class="shortcut"><span class="bold">]</span> / <span class="bold">[</span></td>
							<td class="shortcut-help change-album-sorting"></td>
						</tr>
						<tr>
							<td class="scope album"></td>
							<td class="shortcut"><span class="bold">}</span> / <span class="bold">{</span></td>
							<td class="shortcut-help change-media-sorting"></td>
						</tr>
						<tr>
							<td class="scope album"></td>
							<td class="shortcut">
								<span class="select everything-here-shortcut bold"></span> /
								<span class="shift italic"></span>-<span class="select everything-here-shortcut bold"></span></td>
							<td class="shortcut-help select everything-here"></td>
						</tr>
						<tr>
							<td class="scope album"></td>
							<td class="shortcut space italic"></td>
							<td class="shortcut-help select-highlighted"></td>
						</tr>
						<tr>
							<td class="scope album"></td>
							<td class="shortcut">
								<span class="shift italic"></span>-<span class="arrow-up italic"></span>,
								<span class="shift italic"></span>-<span class="page-up italic"></span>
							</td>
							<td class="shortcut-help album-up"></td>
						</tr>
						<tr>
							<td class="scope album"></td>
							<td class="shortcut">
								<span class="shift italic"></span>-<span class="arrow-down italic"></span>,
								<span class="shift italic"></span>-<span class="page-down italic"></span>
							</td>
							<td class="shortcut-help first-media"></td>
						</tr>
						<tr>
							<td class="scope single-media"></td>
							<td class="shortcut">
								<span class="enter italic"></span>,
								<span class="arrow-right italic"></span>,
								<span class="shift italic"></span>-<span class="back-space italic"></span>,
								<span class="next-media-title-shortcut bold"></span>,
								<span class="shift italic"></span>-<span class="prev-media-title-shortcut bold"></span>
							</td>
							<td class="shortcut-help next-media"></td>
						</tr>
						<tr>
							<td class="scope single-media"></td>
							<td class="shortcut">
								<span class="shift italic"></span>-<span class="enter italic"></span>,
								<span class="arrow-left italic"></span>,
								<span class="shift italic"></span>-<span class="space italic"></span>,
								<span class="back-space italic"></span>,
								<span class="prev-media-title-shortcut bold"></span>,
								<span class="shift italic"></span>-<span class="next-media-title-shortcut bold"></span>
							</td>
							<td class="shortcut-help prev-media"></td>
						</tr>
						<tr>
							<td class="scope video"></td>
							<td class="shortcut">
								<span class="space italic"></span>
							</td>
							<td class="shortcut-help play-stop"></td>
						</tr>
						<tr>
							<td class="scope single-media"></td>
							<td class="shortcut hide-everytyhing-shortcut bold"></td>
							<td class="shortcut-help toggle-title-and-bottom-thumbnails"></td>
						</tr>
						<tr>
							<td class="scope single-media"></td>
							<td class="shortcut metadata-show-shortcut bold"></td>
							<td class="shortcut-help show-meadata"></td>
						</tr>
						<tr>
							<td class="scope single-media"></td>
							<td class="shortcut download-link-shortcut bold"></td>
							<td class="shortcut-help download-link"></td>
						</tr>
						<tr>
							<td class="scope single-media"></td>
							<td class="shortcut original-link-shortcut bold"></td>
							<td class="shortcut-help download-link"></td>
						</tr>
						<tr>
							<td class="scope single-media"></td>
							<td class="shortcut"><span class="bold">+</span> / <span class="bold">-</span></td>
							<td class="shortcut-help zoom-in-out"></td>
						</tr>
						<tr>
							<td class="scope single-media"></td>
							<td class="shortcut">
								<span class="shift italic"></span>-<span class="arrow-up italic"></span>,
								<span class="shift italic"></span>-<span class="page-up italic"></span>,
								<span class="shift italic"></span>-<span class="arrow-down italic"></span>,
								<span class="shift italic"></span>-<span class="page-down italic"></span>
							</td>
							<td class="shortcut-help album-up-from-media"></td>
						</tr>
						<tr>
							<td class="scope single-media"></td>
							<td class="shortcut">
								<span class="control italic"></span>-<span class="space italic"></span>
							</td>
							<td class="shortcut-help toggle-selection"></td>
						</tr>
						<tr>
							<td class="scope single-media"></td>
							<td class="shortcut"><span class="bold">0</span> .. <span class="bold">9</span></td>
							<td class="shortcut-help zoom-level"></td>
						</tr>
						<tr>
							<td class="scope enlarged-image"></td>
							<td class="shortcut">
								<span class="arrows italic"></span>,
								<span class="page-up italic"></span>,
								<span class="page-down italic"></span>
							</td>
							<td class="shortcut-help drag"></td>
						</tr>
						<tr>
							<td class="scope enlarged-image"></td>
							<td class="shortcut">
								<span class="shift italic"></span>-<span class="arrows italic"></span>,
								<span class="shift italic"></span>-<span class="page-up italic"></span>,
								<span class="shift italic"></span>-<span class="page-down italic"></span>
							</td>
							<td class="shortcut-help drag-faster"></td>
						</tr>
					</table>
				</div>
			</div>

			<div id="folders-browsing" class="browsing-mode-message"></div>
			<div id="by-date-browsing" class="browsing-mode-message"></div>
			<div id="by-gps-browsing" class="browsing-mode-message"></div>
			<div id="by-search-browsing" class="browsing-mode-message"></div>
			<div id="by-selection-browsing" class="browsing-mode-message"></div>
			<div id="by-map-browsing" class="browsing-mode-message"></div>

			<div id="added-individually" class="selection-message"></div>
			<div id="removed-individually" class="selection-message"></div>
			<div id="made-global-reset-here" class="selection-message"></div>

			<div id="error-overlay"></div>
			<div id="error-options-file" class="error"></div>
			<div id="error-text-folder" class="error"></div>
			<div id="error-root-folder" class="error"></div>
			<div id="error-text-image" class="error"></div>
			<div id="error-nonexistent-map-album" class="error"></div>
			<div id="error-nonexistent-selection-album" class="error"></div>
			<div id="warning-no-geolocated-media" class="error"></div>
			<div id="error-getting-current-location" class="error"></div>

			<div id="by-exif-date-max-album-sorting" class="sort-message"></div>
			<div id="by-exif-date-max-reverse-album-sorting" class="sort-message"></div>
			<div id="by-exif-date-min-album-sorting" class="sort-message"></div>
			<div id="by-exif-date-min-reverse-album-sorting" class="sort-message"></div>
			<div id="by-file-date-max-album-sorting" class="sort-message"></div>
			<div id="by-file-date-max-reverse-album-sorting" class="sort-message"></div>
			<div id="by-file-date-min-album-sorting" class="sort-message"></div>
			<div id="by-file-date-min-reverse-album-sorting" class="sort-message"></div>
			<div id="by-name-album-sorting" class="sort-message"></div>
			<div id="by-name-reverse-album-sorting" class="sort-message"></div>
			<div id="by-pixel-size-max-album-sorting" class="sort-message"></div>
			<div id="by-pixel-size-max-reverse-album-sorting" class="sort-message"></div>
			<div id="by-pixel-size-min-album-sorting" class="sort-message"></div>
			<div id="by-pixel-size-min-reverse-album-sorting" class="sort-message"></div>
			<div id="by-file-size-album-sorting" class="sort-message"></div>
			<div id="by-file-size-reverse-album-sorting" class="sort-message"></div>
			<div id="by-exif-date-media-sorting" class="sort-message"></div>
			<div id="by-exif-date-reverse-media-sorting" class="sort-message"></div>
			<div id="by-file-date-media-sorting" class="sort-message"></div>
			<div id="by-file-date-reverse-media-sorting" class="sort-message"></div>
			<div id="by-name-media-sorting" class="sort-message"></div>
			<div id="by-name-reverse-media-sorting" class="sort-message"></div>
			<div id="by-pixel-size-media-sorting" class="sort-message"></div>
			<div id="by-pixel-size-reverse-media-sorting" class="sort-message"></div>
			<div id="by-file-size-media-sorting" class="sort-message"></div>
			<div id="by-file-size-reverse-media-sorting" class="sort-message"></div>

			<div id="how-to-download-selection" class="messages">
				<div id="how-to-download-selection-close" class="close-button"></div>
				<div class="how-to-download-selection"></div>
			</div>
		</div>

		<div id="social">
			<div class="ssk-group ssk-rounded ssk-sticky ssk-left ssk-center">
				<a href="" class="ssk ssk-show-url"><img id="show-url" data-src="img/copy.png" width="40" height="40" class="lazyload-page" style="display: none;"></a>
				<a href="" class="ssk ssk-facebook hidden"></a>
				<a href="" class="ssk ssk-whatsapp hidden"></a>
				<a href="" class="ssk ssk-twitter hidden"></a>
				<a href="" class="ssk ssk-google-plus hidden"></a>
				<a href="" class="ssk ssk-linkedin hidden"></a>
				<a href="" class="ssk ssk-pinterest hidden"></a>
				<a href="" class="ssk ssk-tumblr hidden"></a>
				<a href="" class="ssk ssk-vk hidden"></a>
				<a href="" class="ssk ssk-buffer hidden"></a>
				<a href="" class="ssk ssk-email hidden"></a>
			</div>
		</div>

		<div id="right-and-search-menu" style="display: none;">
			<div id="menu-and-padlock">
				<div id="menu-icon">☰</div>
				<img id="search-icon" alt="lens" width="77" height="76" data-src="img/ic_search_black_48dp_2x.png" class="lazyload-page">
				<img id="slideshow-icon" width="40" height="40" alt="slideshow" data-src="img/slideshow.png" class="slideshow-icon lazyload-page">
				<div class="info-icon info-icon-shortcut bold"></div>
				<div id="padlock" class="protection">
					<img width="24" height="25" alt="padlock" data-src="img/padlock.png" class="lazyload-page">
				</div>
			</div>
			<div id="search-menu" class="menu search active hidden-by-menu-selection">
				<form class="caption">
					<input type="search" id="search-field">
					<img id="search-button" alt="lens" data-src="img/ic_search_black_48dp_2x.png" class="lazyload-page">
				</form>
				<ul>
					<li id="inside-words" class="active"></li>
					<li id="any-word" class="active"></li>
					<li id="case-sensitive" class="active"></li>
					<li id="accent-sensitive" class="active"></li>
					<li id="search-numbers" class="active"></li>
					<li id="tags-only" class="active"></li>
					<li id="album-search" class="active"></li>
				</ul>
			</div>

			<ul id="right-menu" class="menu">
				<li class="first-level expandable browsing-mode-switcher active">
					<span class="browsing-mode-switcher caption"></span>
					<ul class="sub-menu hidden">
						<li id="folders-view" class="browsing-mode-switcher radio active"></li>
						<li id="by-date-view" class="browsing-mode-switcher radio active"></li>
						<li id="by-gps-view" class="browsing-mode-switcher radio active"></li>
						<li id="by-map-view" class="browsing-mode-switcher radio active"></li>
						<li id="by-search-view" class="browsing-mode-switcher radio active"></li>
						<li id="by-selection-view" class="browsing-mode-switcher radio active"></li>
					</ul>
				</li>

				<li class="first-level expandable sort album-sort active">
					<span class="sort album-sort caption"></span>
					<ul class="sub-menu hidden">
						<li class="sort album-sort by-exif-date-max radio"></li>
						<li class="sort album-sort by-exif-date-min radio"></li>
						<li class="sort album-sort by-file-date-max radio"></li>
						<li class="sort album-sort by-file-date-min radio"></li>
						<li class="sort album-sort by-name radio"></li>
						<li class="sort album-sort by-pixel-size-max radio"></li>
						<li class="sort album-sort by-pixel-size-min radio"></li>
						<li class="sort album-sort by-file-size radio"></li>
						<li class="sort album-sort reverse active"></li>
					</ul>
				</li>

				<li class="first-level expandable sort media-sort active">
					<span class="sort media-sort caption"></span>
					<ul class="sub-menu hidden">
						<li class="sort media-sort by-exif-date radio"></li>
						<li class="sort media-sort by-file-date radio"></li>
						<li class="sort media-sort by-name radio"></li>
						<li class="sort media-sort by-pixel-size radio"></li>
						<li class="sort media-sort by-file-size radio"></li>
						<li class="sort media-sort reverse active"></li>
					</ul>
				</li>

				<li class="first-level expandable ui active">
					<span class="ui caption"></span>
					<ul class="sub-menu hidden">
						<li class="ui show-title active"></li>
						<li class="ui show-media-counts active"></li>
						<li class="ui slide active"></li>
						<li class="ui square-album-thumbnails active"></li>
						<li class="ui show-album-names active"></li>
						<li class="ui square-media-thumbnails active"></li>
						<li class="ui show-media-names active"></li>
						<li class="ui show-descriptions active"></li>
						<li class="ui show-tags active"></li>
						<li class="ui spaced active"></li>
						<li class="ui show-bottom-thumbnails active"></li>
					</ul>
				</li>

				<li class="first-level expandable select active">
					<span class="select caption"></span>
					<ul class="sub-menu hidden">
						<li id="everything-here" class="select active"></li>
						<li id="every-media-individual" class="select active"></li>
						<li id="albums-here" class="select active"></li>
						<li id="media-here" class="select active"></li>
						<li id="global-reset" class="select active"></li>
						<li id="global-reset-here" class="select active"></li>
						<li id="nothing-here" class="select active"></li>
						<li id="no-albums" class="select active"></li>
						<li id="no-media" class="select active"></li>
						<li id="go-to-selected" class="select active"></li>
					</ul>
				</li>

				<li class="first-level expandable download-album active">
					<span class="download-album caption"></span>
					<ul class="sub-menu hidden">
						<li class="download-album command active"></li>
						<li class="download-single-media command active"></li>
						<li class="download-option-images download-option active">
							<span class="download-option-title"></span>
							<span class="download-option-change">
								<span class="download-option-original"></span>
								<span class="download-option-full"></span>
								<span class="download-option-sized"></span>
								<span class="download-option-format"></span>
								<span class="download-option-cycle">♺</span>
							</span>
						</li>
						<li class="download-option-audios download-option active">
							<span class="download-option-title"></span>
							<span class="download-option-change">
								<span class="download-option-original"></span>
								<span class="download-option-full"></span>
								<span class="download-option-sized"></span>
								<span class="download-option-cycle">♺</span>
							</span>
						</li>
						<li class="download-option-videos download-option active">
							<span class="download-option-title"></span>
							<span class="download-option-change">
								<span class="download-option-original"></span>
								<span class="download-option-full"></span>
								<span class="download-option-sized"></span>
								<span class="download-option-cycle">♺</span>
							</span>
						</li>
						<li class="download-option-subalbums download-option active"></li>
						<li class="download-option-flat download-option active"></li>
						<li class="download-option-selection download-option active"></li>
					</ul>
				</li>

				<li id="show-big-albums" class="first-level big-albums active"></li>

				<li id="hide-geotagged-media" class="first-level non-geotagged-only active"></li>

				<li id="protected-content-unveil" class="first-level protection active"></li>

				<li id="save-data" class="first-level save-data active"></li>

				<li id="restore" class="first-level restore active"></li>
			</ul>
		</div>

		<div id="description-wrapper" class="hidden-by-option">
			<div class="hidden description-wrapper" id="single-media-description-wrapper">
				<div id="single-media-description-wrapper-label" class="description-wrapper-label"></div>
				<div class="description">
					<div class="description-title"></div>
					<div class="description-text"></div>
				</div>
				<div class="description-tags"></div>
			</div>
			<div class="hidden description-wrapper" id="album-description-wrapper">
				<div id="album-description-wrapper-label" class="description-wrapper-label"></div>
				<div class="description">
					<div class="description-title"></div>
					<div class="description-text"></div>
				</div>
				<div class="description-tags"></div>
			</div>
			<div id="description-hide-show">
				<div id="description-hide" class="description-hide-show"></div>
				<div id="description-show" class="description-hide-show"></div>
				<div id="description-show-1" class="description-hide-show"></div>
				<div id="description-show-2" class="description-hide-show"></div>
			</div>
		</div>

		<script>
			var loadJS = function(url) {
				return new Promise(
					function(resolve_loadJS) {
						let scriptTag = document.createElement("script");
						scriptTag.setAttribute("type","text/javascript");
						scriptTag.src = "js/" + url;
						if (scriptTag.addEventListener)
							scriptTag.addEventListener("load", resolve_loadJS, false);
						document.body.appendChild(scriptTag);
					}
				);
			};
		</script>

		<?php
		if (! is_option_set("debug_js")) { ?>
			<script>
				loadJS("scripts.1.min.js");

				<!-- // the loading of the other minized scripts is prepared at the end of 050-display.js -->
			</script>
		<?php	} else {
			// Use system wide jQuery if available
			if (
				is_option_set("use_system_js_libraries") &&
				file_exists("/usr/share/javascript/jquery/jquery.js")
			) { ?>
				<script src="/javascript/jquery/jquery.js"></script>
			<?php	} else { ?>
				<script src="js/000-jquery.js"></script>
			<?php	}

			// jQuery-hashchange should be in Debian! ?>
			<script src="js/001-hashchange.js"></script>

			<script src="js/002-preloadimages.js"></script>

			<?php
			// Use system wide jQuery-mousewheel if available
			if (
				is_option_set("use_system_js_libraries") &&
				file_exists("/usr/share/javascript/jquery-mousewheel/jquery.mousewheel.js")
			) { ?>
				<script src="/javascript/jquery-mousewheel/jquery.mousewheel.js"></script>
			<?php	} else { ?>
				<script src="js/003-mousewheel.js"></script>
			<?php	}

			// Use system wide jQuery-fullscreen if available
			if (
				is_option_set("use_system_js_libraries") &&
				file_exists("/usr/share/javascript/jquery-fullscreen/jquery.fullscreen.js")
			) { ?>
				<script src="/javascript/jquery-fullscreen/jquery.fullscreen.js"></script>
			<?php	} else { ?>
				<script src="js/004-fullscreen.js"></script>
			<?php	}

			// Use system wide modernizr if available
			if (
				! is_option_set("use_internal_modernizr") &&
				is_option_set("use_system_js_libraries") &&
				file_exists("/usr/share/javascript/modernizr/modernizr.min.js")
			) { ?>
				<script src="/javascript/modernizr/modernizr.min.js"></script>
			<?php	} else { ?>
				<script src="js/005-modernizr.js"></script>
			<?php	} ?>

			<?php
			// Use system wide jQuery-lazyload if available
			if (
				is_option_set("use_system_js_libraries") &&
				file_exists("/usr/share/javascript/jquery-lazyload/jquery.lazyload.min.js")
			) { ?>
				<script src="/javascript/jquery-lazyload/jquery.lazyload.min.js"></script>
			<?php	} else { ?>
				<script src="js/007-jquery-lazy.js"></script>
			<?php	} ?>

			<script src="js/016-lzw-compress.js"></script>
			<script src="js/017-imgsupport.js"></script>
			<script src="js/020-translations.js"></script>
			<script src="js/021-classes.js"></script>
			<script src="js/033-utilities.js"></script>
			<script src="js/034-libphotofloat.js"></script>
			<script src="js/036-menu-functions.js"></script>
			<script src="js/038-top-functions.js"></script>
			<script src="js/041-album-methods.js"></script>
			<script src="js/042-media-methods.js"></script>
			<script src="js/043-single-media-methods.js"></script>
			<script src="js/044-subalbum-methods.js"></script>
			<script src="js/045-positions-and-media-methods.js"></script>
			<script src="js/046-other-methods.js"></script>
			<script src="js/050-display.js"></script>

			<!-- // the loading of the other scripts is prepared at the end of 050-display.js -->
		<?php } ?>

		<script>
			var loadCss = function (url) {
				var head = document.getElementsByTagName("head")[0];
				var style = document.createElement("link");
				style.href = "css/" + url;
				style.type = "text/css";
				style.rel = "stylesheet";
				if (url === "001-fonts.css") {
					style.onload = function() {
						$(document).on(
							"animationend",
							function() {
								$.triggerEvent("cssFontsLoadedEvent");
							}
						);
					};
				}
				head.append(style);
			};
		</script>

		<?php	if (! is_option_set("debug_css")) { ?>
			<script>
				loadCss("styles.1.min.css");

				<!-- // the loading of the other minimized css files is prepared at the end of 050-display.js -->
			</script>
		<?php	} else { ?>
			<!-- no debug_css -->
			<script>
				let cssFiles1 = [
					"000-controls.css",
					"002-mobile.css"
				];
				for (let i = 0; i < cssFiles1.length; i ++) {
					loadCss(cssFiles1[i]);
				}

				<!-- // the loading of the other css files is prepared at the end of 050-display.js -->

			</script>
		<?php	} ?>
	</body>
</html>
