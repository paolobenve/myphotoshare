# -*- coding: utf-8 -*-

import os
import subprocess
from Utilities import message, indented_message, next_level, back_level, unlink_file
import Options


class VideoToolWrapper(object):
    def __init__(self):
        self.use_check_output = False
        self.cleanup = True

    def call(self, *args):
        if not Options.ffmpeg_ffprobe_are_there:
            indented_message("Missing ffmepg/ffprobe", "", 3)
            return False
        path = args[-1]
        message(
            "Command:",
            self.command
            + " "
            + " ".join(
                list(
                    map(
                        lambda arg: arg if arg.find(" ") == -1 else "'" + arg + "'",
                        args,
                    )
                )
            ),
            5,
        )
        if self.use_check_output:
            try:
                value = subprocess.check_output((self.command,) + args)
                return value
            except KeyboardInterrupt:
                if self.cleanup:
                    unlink_file(path)
                raise
            except subprocess.CalledProcessError:
                indented_message("Error!", "", 5)
                if self.cleanup:
                    unlink_file(path)
                return False
        else:
            try:
                return_code = subprocess.call((self.command,) + args)
                if return_code > 0:
                    indented_message("Error!", "", 5)
                    return False
                return True
            except KeyboardInterrupt:
                if self.cleanup:
                    unlink_file(path)
                raise
            except subprocess.CalledProcessError:
                indented_message("Error!", "", 5)
                if self.cleanup:
                    unlink_file(path)
                back_level()
                return False


class VideoTranscodeCaller(VideoToolWrapper):
    def __init__(self):
        VideoToolWrapper.__init__(self)
        self.command = "ffmpeg"
        self.use_check_output = False
        self.cleanup = True


class VideoProbeCaller(VideoToolWrapper):
    def __init__(self):
        VideoToolWrapper.__init__(self)
        self.command = "ffprobe"
        self.use_check_output = True
        self.cleanup = False
