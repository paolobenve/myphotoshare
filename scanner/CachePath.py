# -*- coding: utf-8 -*-
# do not remove previous line: it's not a comment!

import re
import os.path
from datetime import datetime
import hashlib
import unicodedata
import unidecode
from pprint import pprint

import Options


def trim_base_custom(path, base):
    if path.startswith(base):
        path = path[len(base) :]
    if path.startswith("/"):
        path = path[1:]
    return path


def remove_album_path(path):
    return trim_base_custom(path, Options.config["album_path"])


def remove_folders_marker(path):
    marker_position = path.find(Options.config["folders_string"])
    if marker_position == 0:
        path = path[len(Options.config["folders_string"]) :]
        if len(path) > 0:
            path = path[1:]
    return path


def remove_non_alphabetic_characters(phrase):
    # normalize unicode, see https://stackoverflow.com/questions/16467479/normalizing-unicode
    phrase = unicodedata.normalize("NFC", phrase)
    # convert non-alphabetic characters to spaces
    new_phrase = ""
    for c in phrase:
        new_phrase += (
            c
            if (c.isalnum() or c in Options.config["unicode_combining_marks"])
            else " "
        )

    # normalize multiple, leading and trailing spaces
    phrase = " ".join(new_phrase.split())

    return phrase


def remove_new_lines_and_tags(phrase):
    # remove tags
    phrase = re.sub("<[^<]+?>", " ", phrase)
    # remove new lines
    phrase = " ".join(phrase.splitlines())

    return phrase


def reduce_spaces(phrase):
    while phrase.find("  ") != -1:
        phrase = phrase.replace("  ", " ")
    return phrase


def remove_all_but_alphanumeric_chars_dashes_slashes(phrase):
    # normalize unicode, see https://stackoverflow.com/questions/16467479/normalizing-unicode
    phrase = unicodedata.normalize("NFC", phrase)
    # convert non-alphabetic characters to spaces
    new_phrase = ""
    for c in phrase:
        new_phrase += (
            c
            if (
                c in ["-", "/"]
                or c.isalpha()
                or c.isdecimal()
                or c in Options.config["unicode_combining_marks"]
            )
            else " "
        )
        # new_phrase += c if (c in ['-', '/', '.'] or c.isalpha() or c.isdecimal() or c in Options.config['unicode_combining_marks']) else " "
    # normalize multiple, leading and trailing spaces
    phrase = " ".join(new_phrase.split())

    return phrase


def remove_digits(phrase):
    # remove digits
    phrase = "".join([" " if c.isdecimal() else c for c in phrase])
    # normalize multiple, leading and trailing spaces
    phrase = " ".join(phrase.split())

    return phrase


def remove_accents(phrase):
    # strip accents (from http://nullege.com/codes/show/src@d@b@dbkit-0.2.2@examples@notary@notary.py/38/unicodedata.combining)
    normalized_phrase = unicodedata.normalize("NFKD", phrase)
    accentless_phrase = ""
    for c in normalized_phrase:
        if c not in Options.config["unicode_combining_marks"]:
            accentless_phrase += c

    return accentless_phrase


def switch_to_lowercase(phrase):
    phrase = phrase.lower()

    return phrase


def transliterate_to_ascii(phrase):
    # convert accented characters to ascii, from https://stackoverflow.com/questions/517923/what-is-the-best-way-to-remove-accents-in-a-python-unicode-string

    # the following line generate a problem with chinese, because unidecode translate every ideogram with a word
    # phrase = unidecode.unidecode(phrase)

    words = phrase_to_words(phrase)
    decoded_words = []
    for word in words:
        # removing spaces is necessary with chinese: every ideogram is rendered with a word
        decoded_words.append(unidecode.unidecode(word).strip().replace(" ", "_"))

    phrase = " ".join(decoded_words)

    return phrase


def phrase_to_words(phrase):
    phrase = reduce_spaces(phrase)
    # splits the phrase into a list
    return list(phrase.split(" "))


def is_same_image_mime_type(single_media, format):
    return (
        single_media.mime_type == "image/jpeg"
        and format == "jpg"
        or single_media.mime_type == "image/png"
        and format == "png"
        or single_media.mime_type == "image/webp"
        and format == "webp"
        or Options.config["avif_supported"]
        and (single_media.mime_type == "image/avif")
        and format == "avif"
        or Options.config["heic_supported"]
        and (single_media.mime_type == "image/heic")
        and format == "heic"
        or Options.config["jxl_supported"]
        and single_media.mime_type == "image/jxl"
        and format == "jxl"
    )


def can_expose_all_metadata(single_media):
    return (
        Options.config["expose_image_metadata"]
        and Options.config["expose_image_dates"]
        and (Options.config["expose_image_positions"] or not single_media.has_gps_data)
    )


def original_media_is_enough(single_media):
    result = single_media.mime_type not in Options.config[
        "browser_unsupported_mime_types"
    ] and (
        single_media.is_image
        or (
            single_media.is_video
            and single_media.codec
            not in Options.config["browser_unsupported_video_codecs"]
        )
        or (
            single_media.is_audio
            and single_media.codec
            not in Options.config["browser_unsupported_audio_codecs"]
        )
    )
    return result


def may_make_image_os_copy(single_media, format):
    return is_same_image_mime_type(single_media, format) and can_expose_all_metadata(
        single_media
    )


# def must_make_converted_fullsize_copy(single_media, format):
#     return (
#         Options.config["expose_original_media"]
#         and not original_media_is_enough(single_media)
#         or Options.config["expose_full_size_media_in_cache"]
#         and not may_make_image_os_copy(single_media, format)
#     )


# def extension(mime_type):
#     if mime_type == "image/jpeg":
#         return "jpg"
#     elif mime_type == "image/png":
#         return "png"
#     elif mime_type == "image/webp":
#         return "webp"
#     elif mime_type == "image/avif" or mime_type == "image/heic":
#         return "avif"


def cache_image_formats_ending_with_jpg():
    return [
        format for format in Options.config["cache_images_formats"] if format != "jpg"
    ] + ["jpg"]


def image_cache_name(single_media, size, format, thumb_type="", mobile_bigger=False):
    # this function is used for video thumbnails too
    # with size == 0,
    # it can be called for image formats not supported by the browser and for full size copies
    image_suffix = Options.config["cache_folder_separator"]
    if size == 0:
        if (
            not Options.config["expose_original_media"]
            and not Options.config["expose_full_size_media_in_cache"]
        ):
            return None

        if Options.config["expose_original_media"] and original_media_is_enough(
            single_media
        ):
            return single_media.media_path

        image_suffix += "fs"
    else:
        actual_size = size
        if mobile_bigger:
            actual_size = int(
                round(actual_size * Options.config["mobile_thumbnail_factor"])
            )
        if (
            thumb_type == "media_fixed_height"
            and not single_media.is_audio
            and single_media.size[0] > single_media.size[1]
        ):
            actual_size = int(
                round(actual_size * single_media.size[0] / single_media.size[1])
            )
        image_suffix += str(actual_size)

        if (
            single_media.is_audio
            or single_media.size[0] == single_media.size[1]
            or thumb_type.endswith("_square")
            or format.find("sq.") == 0
        ):
            image_suffix += "sq"
    if format.find("sq.") == 0:
        # the "sq" suffix has been added to the format, remove it
        format = format[3:]
    image_suffix += "."
    image_suffix += format

    return single_media.cache_base + image_suffix


def audio_cache_copy_name(audio):
    original_extension = audio.media_file_name[audio.media_file_name.rfind(".") + 1 :]
    return (
        audio.cache_base
        + Options.config["cache_folder_separator"]
        + "fs."
        + original_extension.lower()
    )


def video_cache_copy_name(video):
    original_extension = video.media_file_name[video.media_file_name.rfind(".") + 1 :]
    return (
        video.cache_base
        + Options.config["cache_folder_separator"]
        + "fs."
        + original_extension.lower()
    )


def audio_full_size_transcoded_name(audio):
    return audio.cache_base + Options.config["cache_folder_separator"] + "fs.mp3"


def video_full_size_transcoded_name(video):
    return video.cache_base + Options.config["cache_folder_separator"] + "fs.mp4"


def audio_transcoded_name(audio):
    return (
        audio.cache_base + Options.config["cache_folder_separator"] + "transcoded.mp3"
    )


def video_transcoded_name(video):
    return (
        video.cache_base + Options.config["cache_folder_separator"] + "transcoded.mp4"
    )


def last_modification_time(path):
    maximum = 0
    for root, dirs, files in os.walk(path):
        maximum = max(maximum, os.path.getmtime(root))
        if files:
            maximum = max(
                maximum,
                max(os.path.getmtime(os.path.join(root, file)) for file in files),
            )
    last_mtime = datetime.fromtimestamp(maximum)
    return last_mtime


def checksum(file_pointer):
    block_size = 65536
    hasher = hashlib.md5()
    # with open(path, 'rb') as afile:
    buf = file_pointer.read(block_size)
    while len(buf) > 0:
        hasher.update(buf)
        buf = file_pointer.read(block_size)
    return hasher.hexdigest()


# def square_thumbnail_sizes():
#     # collect all the square sizes needed
#
#     # album size: square thumbnail are generated anyway, because they are needed by the code that generates composite images for sharing albums
#     # the second element in the tuple_arg is `mobile_bigger`.
#     square_sizes = [(Options.config["album_thumb_size"], False)]
#     if Options.config["mobile_thumbnail_factor"] > 1:
#         square_sizes.append((Options.config["album_thumb_size"], True))
#     square_sizes.append((Options.config["media_thumb_size"], False))
#     if (
#         Options.config["mobile_thumbnail_factor"] > 1
#         and (Options.config["media_thumb_size"], True) not in square_sizes
#     ):
#         square_sizes.append((Options.config["media_thumb_size"], True))
#     # sort sizes descending
#     square_sizes = sorted(square_sizes, key=modified_size, reverse=True)
#     return square_sizes


def modified_size(tuple_arg):
    (size, mobile_bigger) = tuple_arg
    if mobile_bigger:
        return int(round(size * Options.config["mobile_thumbnail_factor"]))
    else:
        return size
