# -*- coding: utf-8 -*-
# do not remove previous line: it's not a comment!

from datetime import datetime
from pprint import pprint
import sys
import os
import stat
import json
import subprocess

import Options


def unlink_file(file_name_with_path):
    try:
        os.unlink(file_name_with_path)
    except OSError:
        return False
    except FileNotFoundError:
        return False
    return True


def file_mtime(path):
    # Warning: the datetime returned is in UTC
    # when used to set the date for a video,
    # be conscious whether the timestamp of the image has tz specification or hasn't it
    return datetime.fromtimestamp(int(os.path.getmtime(path)))


def report_mem():
    return
    print(
        "MEM total, code, data >>>> "
        + os.popen("ps -p " + str(os.getpid()) + " -o rss,trs,drs|grep -v DRS").read()
    )


def set_permissions_according_to_umask(absolute_path):
    umask = os.umask(0)
    os.umask(umask)
    st = os.stat(absolute_path)
    os.chmod(absolute_path, 0o666 & ~umask)


def make_dir(absolute_path, message_part):
    # makes a subdir and manages errors
    next_level()
    relative_path = absolute_path[len(Options.config["index_html_path"]) + 1 :]
    if not os.path.exists(absolute_path):
        try:
            message("creating " + message_part + "...", "", 6)
            os.makedirs(absolute_path)
            indented_message(message_part + " created!", absolute_path, 5)
        except OSError:
            message(
                "FATAL ERROR",
                "couldn't create "
                + message_part
                + " ('"
                + absolute_path
                + "'), quitting",
                0,
            )
            sys.exit(1)
    else:
        message(message_part + " already existent", relative_path, 6)
    back_level()


def json_files_and_mtimes(cache_base):
    json_file_list = []
    mtimes = set()
    # min_mtime = None
    # max_mtime = None
    using_provisional_json_file = False
    if not Options.last_scanner_has_ended:
        provisional_json_file = (
            os.path.join(
                Options.config["regular_album_subdir"],
                cache_base,
            )
            + ".json.media_only"
        )
        provisional_json_file_with_path = os.path.join(
            Options.config["cache_path"], provisional_json_file
        )
        if os.path.exists(provisional_json_file_with_path):
            using_provisional_json_file = True

    if using_provisional_json_file:
        json_file = provisional_json_file
        json_file_with_path = provisional_json_file_with_path
    else:
        json_file = (
            os.path.join(
                Options.config["regular_album_subdir"],
                cache_base,
            )
            + ".json"
        )
        json_file_with_path = os.path.join(Options.config["cache_path"], json_file)

    if os.path.exists(json_file_with_path):
        json_file_list.append(json_file)
        mtimes.add(file_mtime(json_file_with_path))

    if not using_provisional_json_file:
        all_complex_combinations = [
            identifier_and_password_1["password_md5"]
            + ","
            + identifier_and_password_2["password_md5"]
            for identifier_and_password_1 in Options.identifiers_and_passwords
            for identifier_and_password_2 in Options.identifiers_and_passwords
        ]
        complex_combinations = [
            identifier_and_password["password_md5"]
            for identifier_and_password in Options.identifiers_and_passwords
        ]
        all_complex_combinations += [
            "," + complex_combination for complex_combination in complex_combinations
        ]
        all_complex_combinations += [
            complex_combination + "," for complex_combination in complex_combinations
        ]
        # all_complex_combinations += [',']
        all_complex_combinations = list(set(all_complex_combinations))
        for complex_combination in all_complex_combinations:
            protected_dir = (
                Options.config["protected_directories_prefix"] + complex_combination
            )
            protected_dir_with_path = os.path.join(
                Options.config["cache_path"],
                protected_dir,
            )
            if not os.path.exists(protected_dir_with_path):
                continue

            number = -1
            while True:
                number += 1
                protected_json_file = os.path.join(
                    protected_dir,
                    Options.config["regular_album_subdir"],
                    cache_base + "." + str(number) + ".json",
                )
                protected_json_file_with_path = os.path.join(
                    Options.config["cache_path"], protected_json_file
                )
                if os.path.exists(protected_json_file_with_path):
                    if not os.path.islink(protected_json_file_with_path):
                        json_file_list.append(protected_json_file)
                        mtime = file_mtime(protected_json_file_with_path)
                        mtimes.add(mtime)
                else:
                    break

    if not all(
        [
            os.access(os.path.join(Options.config["cache_path"], json_file), os.R_OK)
            for json_file in json_file_list
        ]
    ):
        message("FATAL ERROR", "some json file unreadable", 1)
        sys.exit(1)
    elif not all(
        [
            os.access(os.path.join(Options.config["cache_path"], json_file), os.W_OK)
            for json_file in json_file_list
        ]
    ):
        message(
            "FATAL ERROR",
            "some json file unwritable: '" + str(json_file_list) + "'",
            1,
        )
        sys.exit(1)

    return [json_file_list, mtimes, using_provisional_json_file]
    # return [json_file_list, min_mtime, max_mtime, using_provisional_json_file]


def touch_with_datetime(file_with_path):
    try:
        os.utime(
            file_with_path,
            times=(Options.timestamp_for_touching, Options.timestamp_for_touching),
        )
    except FileNotFoundError:
        pass


def touchtime_string(file_with_path):
    mtime = os.path.getmtime(file_with_path)
    return datetime.fromtimestamp(mtime).strftime(
        Options.timestamp_format_for_file_name
    )


def md5_products(complex_identifiers_combination):
    # return all the simple combinations for the given argument
    # example: for "a-b-c,c-d" it will return
    # ["a,c", "a,d", "b,c", "b,d", "c,c", "c,d"]

    (
        album_identifiers_combination,
        media_identifiers_combination,
    ) = complex_identifiers_combination.split(",")

    media_password_md5_list = sorted(
        convert_identifiers_set_to_md5s_set(
            convert_combination_to_set(media_identifiers_combination)
        )
    )
    album_password_md5_list = sorted(
        convert_identifiers_set_to_md5s_set(
            convert_combination_to_set(album_identifiers_combination)
        )
    )
    album_codes_set = convert_identifiers_set_to_codes_set(
        convert_combination_to_set(album_identifiers_combination)
    )
    media_codes_set = convert_identifiers_set_to_codes_set(
        convert_combination_to_set(media_identifiers_combination)
    )
    codes_complex_combination = complex_combination(
        convert_set_to_combination(album_codes_set),
        convert_set_to_combination(media_codes_set),
    )
    md5_product_list = []
    if len(album_password_md5_list) > 0 and len(media_password_md5_list) > 0:
        for couple in (
            (x, y) for x in album_password_md5_list for y in media_password_md5_list
        ):
            md5_product_list.append(",".join(couple))
    elif len(album_password_md5_list) > 0:
        for md5 in album_password_md5_list:
            md5_product_list.append(complex_combination(md5, ""))
    else:
        for md5 in media_password_md5_list:
            md5_product_list.append(complex_combination("", md5))
    return [md5_product_list, codes_complex_combination]


def create_complex_dir_and_subdirs(complex_dir):
    if complex_dir not in Options.created_dirs:
        make_dir(complex_dir, "protected content directory")

        regular_albums_subdir = os.path.join(
            complex_dir, Options.config["regular_album_subdir"]
        )
        if regular_albums_subdir not in Options.created_dirs:
            make_dir(regular_albums_subdir, "protected regular albums subdir")
            Options.created_dirs.append(regular_albums_subdir)

        if Options.config["expose_image_dates"]:
            by_date_subdir = os.path.join(
                complex_dir, Options.config["by_date_album_subdir"]
            )
            if by_date_subdir not in Options.created_dirs:
                make_dir(by_date_subdir, "protected by date subdir")
                Options.created_dirs.append(by_date_subdir)

        if Options.config["expose_image_positions"]:
            by_gps_subdir = os.path.join(
                complex_dir, Options.config["by_gps_album_subdir"]
            )
            if by_gps_subdir not in Options.created_dirs:
                make_dir(by_gps_subdir, "protected by gps subdir")
                Options.created_dirs.append(by_gps_subdir)

        by_search_subdir = os.path.join(
            complex_dir, Options.config["by_search_album_subdir"]
        )
        if by_search_subdir not in Options.created_dirs:
            make_dir(by_search_subdir, "protected search subdir")
            Options.created_dirs.append(by_search_subdir)

        cache_images_subdir = os.path.join(
            complex_dir, Options.config["default_cache_album"]
        )
        if cache_images_subdir not in Options.created_dirs:
            make_dir(cache_images_subdir, "cache images subdir")
            Options.created_dirs.append(cache_images_subdir)

        Options.created_dirs.append(complex_dir)


def determine_symlink_number_and_name(cache_base_with_path):
    number = -1
    extension = ".json"
    while True:
        number += 1
        symlink = (
            cache_base_with_path[: -len(extension)] + "." + str(number) + extension
        )
        if not os.path.exists(symlink):
            break
    return [number, symlink]


def calculate_media_file_name(json_file_name):
    splitted_json_file_name = json_file_name.split(".")
    return ".".join(splitted_json_file_name[:-1]) + ".media.json"


def convert_combination_to_set(combination):
    if combination == "":
        return set()
    return set(combination.split("-"))


def convert_set_to_combination(this_set):
    if this_set == set():
        return ""
    return "-".join(sorted(this_set))


def complex_combination(album_combination, media_combination):
    return ",".join([album_combination, media_combination])


def convert_identifiers_set_to_md5s_set(identifiers_set):
    if identifiers_set == set():
        return set()
    md5s_set = set()
    for identifier in identifiers_set:
        md5 = next(
            identifier_and_password["password_md5"]
            for identifier_and_password in Options.identifiers_and_passwords
            if identifier_and_password["identifier"] == identifier
        )
        md5s_set.add(md5)
    return md5s_set


def convert_identifiers_set_to_codes_set(identifiers_set):
    if identifiers_set == set():
        return set()
    codes_set = set()
    for identifier in identifiers_set:
        code = next(
            identifier_and_password["password_code"]
            for identifier_and_password in Options.identifiers_and_passwords
            if identifier_and_password["identifier"] == identifier
        )
        codes_set.add(code)
    return codes_set


def convert_old_codes_set_to_identifiers_set(codes_set):
    if codes_set == set():
        return set()
    identifiers_set = set()
    for code in codes_set:
        try:
            md5 = Options.old_password_codes[code]
            identifier = convert_md5_to_identifier(md5)
            identifiers_set.add(identifier)
        except KeyError:
            return None
    return identifiers_set


def convert_md5_to_identifier(md5):
    if md5 == "":
        return ""
    identifiers_list = [
        identifier_and_password["identifier"]
        for identifier_and_password in Options.identifiers_and_passwords
        if identifier_and_password["password_md5"] == md5
    ]
    return identifiers_list[0]


def convert_md5_to_code(md5):
    if md5 == "":
        return ""
    code_list = [
        identifier_and_password["password_code"]
        for identifier_and_password in Options.identifiers_and_passwords
        if identifier_and_password["password_md5"] == md5
    ]
    return code_list[0]


def convert_simple_md5_combination_to_simple_codes_combination(
    md5_simple_complex_combination,
):
    splitted_md5_simple_complex_combination = md5_simple_complex_combination.split(",")
    return ",".join(
        [
            convert_md5_to_code(splitted_md5_simple_complex_combination[0]),
            convert_md5_to_code(splitted_md5_simple_complex_combination[1]),
        ]
    )


def save_password_codes(first_time):
    message("Saving password/codes files...", "", 3)
    saved_files = []
    next_level()
    # remove the old single password files
    passwords_subdir_with_path = os.path.join(
        Options.config["cache_path"], Options.config["passwords_subdir"]
    )

    if first_time:
        message("Removing old password files...", "", 5)
        for password_file in sorted(os.listdir(passwords_subdir_with_path)):
            unlink_file(os.path.join(passwords_subdir_with_path, password_file))
        indented_message("Old password files removed", "", 4)

    # create the new single password files
    for identifier_and_password in Options.identifiers_and_passwords:
        if identifier_and_password["used"] or first_time:
            password_md5 = identifier_and_password["password_md5"]
            password_code = identifier_and_password["password_code"]
            password_file_with_dir = os.path.join(
                Options.config["passwords_subdir"], password_md5
            )
            message("creating new password file", "", 6)
            password_file_with_path = os.path.join(
                Options.config["cache_path"], password_file_with_dir
            )
            with open(password_file_with_path, "w") as password_file_pointer:
                json.dump({"passwordCode": password_code}, password_file_pointer)
            indented_message("New password file created", password_file_with_dir, 5)
            # touch the password file with the proper timestamp
            # touch_with_datetime(password_file_with_path)
            saved_files.append(password_file_with_dir)
    back_level()
    indented_message("Password/codes files saved!", "", 4)

    return saved_files


def merge_media(dict, dict_1):
    # this function only merges media in 2nd dictionary to 1st dictionary
    # if dict hasn't password_identifiers_set, then it's copied from dict_1

    if dict is None:
        return dict_1
        # return add_combination_to_dict(dict_1)
    if dict_1 is None:
        return dict
        # return add_combination_to_dict(dict)

    if "password_identifiers_set" not in dict and "password_identifiers_set" in dict_1:
        dict["password_identifiers_set"] = dict_1["password_identifiers_set"]

    for single_media_1 in dict_1["media"]:
        if single_media_1["name"] not in [
            single_media["name"] for single_media in dict["media"]
        ]:
            dict["media"].append(single_media_1)
    # subalbums_cache_bases = [subalbum["cacheBase"] for subalbum in dict["subalbums"]]
    # dict["subalbums"].extend(
    #     [
    #         subalbum
    #         for subalbum in dict_1["subalbums"]
    #         if subalbum["cacheBase"] not in subalbums_cache_bases
    #     ]
    # )
    return dict


def message(category, text, verbose=0):
    """
      Print a line of logging `text` if the `verbose` level is lower than the verbosity level
      defined in the configuration file. This message is prefixed by the `category` text and
      timing information.

      The format of the log line is
      ```
    2220 2018-02-04 17:17:38.517966   |  |--[album saved]     /var/www/html/myphotoshare/cache/_bd-2017-09-24.json
    ^    ^                                   ^                ^
        |    |                                   |                text
        |    |                                   indented category
    |    date and time
        microseconds from last message
      ```

      Elapsed time for each category is cumulated and can be printed with `report_times`.

      Verbosity levels:
      - 0 = fatal errors only
      - 1 = add non-fatal errors
      - 2 = add warnings
      - 3 = add info
      - 4 = add more info
    """

    sep = "   "
    try:
        message.max_verbose = Options.config["max_verbose"]
    except KeyError:
        message.max_verbose = 5
    except AttributeError:
        message.max_verbose = 0

    if verbose <= message.max_verbose:
        now = datetime.now()
        now_utc = datetime.utcnow()
        time_elapsed = now - Options.last_time
        Options.last_time = now
        microseconds = int(time_elapsed.total_seconds() * 1000000)
        if microseconds == 0:
            microseconds = ""
        else:
            try:
                Options.elapsed_times[category] += microseconds
                Options.elapsed_times_counter[category] += 1
            except KeyError:
                Options.elapsed_times[category] = microseconds
                Options.elapsed_times_counter[category] = 1
            _microseconds = str(microseconds)
        print(
            _microseconds.rjust(9),
            "%s %s[%s]%s%s"
            % (
                now.isoformat(" "),
                max(0, message.level) * "   ",
                str(category),
                max(1, (45 - len(str(category)))) * " ",
                str(text),
            ),
        )
        # print(_microseconds.rjust(9), "%s %s%s[%s]%s%s" % (now.isoformat(' '), max(0, message.level) * "  |", sep, str(category), max(1, (45 - len(str(category)))) * " ", str(text)))


"""
The verbosity level as defined by the user in the configuration file.
"""
message.max_verbose = 0


"""
The identation level printed by the message function.
"""
message.level = 0


def indented_message(category, text, verbose=0):
    next_level()
    message(category, text, verbose)
    back_level()


def next_level(verbose=0):
    """
    Increase the indentation level of log messages.
    """
    if verbose <= message.max_verbose:
        message.level += 1


def back_level(verbose=0):
    """
    Decrease the indentation level of log messages.
    """
    if verbose <= message.max_verbose:
        message.level -= 1


# find a file in file system, from https://stackoverflow.com/questions/1724693/find-a-file-in-python
def find(name):
    for root, dirnames, files in os.walk("/"):
        dirnames[:] = [
            dir for dir in dirnames if not os.path.ismount(os.path.join(root, dir))
        ]
        if name in files:
            return os.path.join(root, name)
    return False


def find_in_filesystem(file_name, dir):
    command = ["locate", file_name]

    output = subprocess.Popen(command, stdout=subprocess.PIPE).communicate()[0]
    output = output.decode()

    search_results = output.split("\n")
    search_results = [
        file_path
        for file_path in search_results
        if file_path.startswith(dir) and file_path.endswith("/" + file_name)
    ]

    if len(search_results) == 0:
        return False
    else:
        return search_results[0]


def time_totals(time):
    seconds = int(round(time / 1000000))
    if time <= 1800:
        _total_time = str(int(round(time))) + " μs"
    elif time <= 1800000:
        _total_time = str(int(round(time / 1000))) + "    ms"
    else:
        _total_time = str(seconds) + "       s "

    _total_time_m, _total_time_s = divmod(seconds, 60)
    _total_time_h, _total_time_m = divmod(_total_time_m, 60)

    _total_time_hours = str(_total_time_h) + "h " if _total_time_h else ""
    _total_time_minutes = str(_total_time_m) + "m " if _total_time_m else ""
    _total_time_seconds = str(_total_time_s) + "s" if _total_time_m else ""
    _total_time_unfolded = _total_time_hours + _total_time_minutes + _total_time_seconds
    if _total_time_unfolded:
        _total_time_unfolded = "= " + _total_time_unfolded
    return (_total_time, _total_time_unfolded)


def report_times(final=False):
    """
    Print a report with the total time spent on each `message()` categories and the number of times
    each category has been called. This report can be considered a poor man's profiler as it cumulates
    the number of times the `message()` function has been called instead of the real excution time of
    the code.
    This report is printed only when tracing level >= 3 and more or less detailed depending on `final`.
    The report includes a section at the end with the number of media processed by type and list the
    albums where media is not geotagged or has no EXIF.
    """
    # Print report for each album only when tracing level >= 4
    if message.max_verbose < 2:
        return
    if not final and message.max_verbose < 3:
        return

    if final:
        print()
        print(
            "message".rjust(50)
            + "total time".rjust(15)
            + "counter".rjust(15)
            + "average time".rjust(20)
        )
        print()
    time_till_now = 0
    time_till_now_pre = 0
    time_till_now_browsing = 0
    for category in sorted(
        Options.elapsed_times, key=Options.elapsed_times.get, reverse=True
    ):
        time = int(round(Options.elapsed_times[category]))
        (_time, _time_unfolded) = time_totals(time)

        if category[0:4] == "PRE ":
            time_till_now_pre += time
        else:
            time_till_now_browsing += time
        time_till_now += time

        counter = str(Options.elapsed_times_counter[category]) + " times"

        average_time = int(
            Options.elapsed_times[category] / Options.elapsed_times_counter[category]
        )
        if average_time == 0:
            _average_time = ""
        elif average_time <= 1800:
            _average_time = str(average_time) + " μs"
        elif average_time <= 1800000:
            _average_time = str(int(round(average_time / 1000))) + "    ms"
        else:
            _average_time = str(int(round(average_time / 1000000))) + "       s "
        if final:
            print(
                category.rjust(50)
                + _time.rjust(18)
                + counter.rjust(15)
                + _average_time.rjust(20)
            )

    (_time_till_now, _time_till_now_unfolded) = time_totals(time_till_now)
    (_time_till_now_pre, _time_till_now_unfolded_pre) = time_totals(time_till_now_pre)
    (_time_till_now_browsing, _time_till_now_unfolded_browsing) = time_totals(
        time_till_now_browsing
    )
    print()
    print(
        "time taken till now".rjust(50)
        + _time_till_now.rjust(18)
        + "     "
        + _time_till_now_unfolded
    )
    num_media = Options.num_image + Options.num_audio + Options.num_video

    _num_media = str(num_media)
    # do not print the report if browsing hasn't been done
    if num_media > 0 and Options.config["num_media_in_tree"] > 0:
        # normal run, print final report about images, audios, videos, geotags, dates
        try:
            time_missing = (
                time_till_now_browsing / num_media * Options.config["num_media_in_tree"]
                - time_till_now_browsing
            )
            if time_missing >= 0:
                (_time_missing, _time_missing_unfolded) = time_totals(time_missing)
                print(
                    "total time missing".rjust(50)
                    + _time_missing.rjust(18)
                    + "     "
                    + _time_missing_unfolded
                )
            time_total = time_till_now + time_missing
            if time_total > 0:
                (_time_total, _time_total_unfolded) = time_totals(time_total)
                print(
                    "total time".rjust(50)
                    + _time_total.rjust(18)
                    + "     "
                    + _time_total_unfolded
                )
        except ZeroDivisionError:
            pass
        print()

        _num_media = str(num_media)
        num_media_processed = (
            Options.num_image_processed
            + Options.num_audio_processed
            + Options.num_video_processed
        )
        _num_media_processed = str(num_media_processed)

        _num_image = str(Options.num_image)
        _num_image_processed = str(Options.num_image_processed)
        _num_image_with_geotags = str(Options.num_image_with_geotags)
        _num_image_with_exif_date = str(Options.num_image_with_exif_date)
        _num_image_with_exif_date_and_geotags = str(
            Options.num_image_with_exif_date_and_geotags
        )
        _num_image_with_exif_date_and_without_geotags = str(
            Options.num_image_with_exif_date_and_without_geotags
        )
        _num_image_without_exif_date_and_with_geotags = str(
            Options.num_image_without_exif_date_and_with_geotags
        )
        _num_image_without_exif_date_or_geotags = str(
            Options.num_image_without_exif_date_or_geotags
        )

        _num_audio = str(Options.num_audio)
        _num_audio_processed = str(Options.num_audio_processed)

        _num_audio_with_geotags = str(Options.num_audio_with_geotags)
        _num_audio_with_date_from_metadata_or_file_name = str(
            Options.num_audio_with_date_from_metadata_or_file_name
        )
        _num_audio_with_date_from_metadata_or_file_name_and_geotags = str(
            Options.num_audio_with_date_from_metadata_or_file_name_and_geotags
        )
        _num_audio_with_date_from_metadata_or_file_name_and_without_geotags = str(
            Options.num_audio_with_date_from_metadata_or_file_name_and_without_geotags
        )
        _num_audio_without_date_from_metadata_or_file_name_and_with_geotags = str(
            Options.num_audio_without_date_from_metadata_or_file_name_and_with_geotags
        )
        _num_audio_without_date_from_metadata_or_file_name_or_geotags = str(
            Options.num_audio_without_date_from_metadata_or_file_name_or_geotags
        )

        _num_video = str(Options.num_video)
        _num_video_processed = str(Options.num_video_processed)

        _num_video_with_geotags = str(Options.num_video_with_geotags)
        _num_video_with_date_from_metadata_or_file_name = str(
            Options.num_video_with_date_from_metadata_or_file_name
        )
        _num_video_with_date_from_metadata_or_file_name_and_geotags = str(
            Options.num_video_with_date_from_metadata_or_file_name_and_geotags
        )
        _num_video_with_date_from_metadata_or_file_name_and_without_geotags = str(
            Options.num_video_with_date_from_metadata_or_file_name_and_without_geotags
        )
        _num_video_without_date_from_metadata_or_file_name_and_with_geotags = str(
            Options.num_video_without_date_from_metadata_or_file_name_and_with_geotags
        )
        _num_video_without_date_from_metadata_or_file_name_or_geotags = str(
            Options.num_video_without_date_from_metadata_or_file_name_or_geotags
        )

        _num_unrecognized_files = str(Options.num_unrecognized_files)
        _num_decompression_bomb_error_files = str(
            Options.num_decompression_bomb_error_files
        )
        _num_big_size_for_format_warnings = str(
            Options.num_big_size_for_format_warnings
        )
        _num_other_errors = str(Options.num_other_errors)
        max_digit = len(_num_media)

        number_format = "{:" + str(max_digit) + ",d}"
        # print(number_format)
        media_count_and_time = (
            "Media    "
            + number_format.format(num_media)
            + " / "
            + number_format.format(Options.config["num_media_in_tree"])
            + " ("
            + str(int(num_media * 1000 / Options.config["num_media_in_tree"]) / 10)
            + "%)"
        )
        if num_media:
            mean_time = time_till_now / 1000000 / num_media
            media_count_and_time += ",      " + "{:2.6f}".format(mean_time) + " s/media"
            mean_speed_sec = int(1 / mean_time * 100) / 100
            media_count_and_time += (
                ",      " + "{:7.2f}".format(1 / mean_time) + " media/s"
            )
            mean_speed_min = 1 / mean_time * 60
            media_count_and_time += (
                ",      " + "{:n}".format(mean_speed_min) + " media/min"
            )
            mean_speed_hour = mean_speed_min * 60
            media_count_and_time += (
                ",      " + "{:n}".format(mean_speed_hour) + " media/hour"
            )

        print(media_count_and_time)
        media_count_and_time = "                  processed " + number_format.format(
            num_media_processed
        )
        if num_media_processed and num_media_processed != num_media:
            media_count_and_time += (
                ",      "
                + str(int(time_till_now / num_media_processed / 10000) / 100)
                + " s/processed media"
            )
        print(media_count_and_time)
        print()
        print("- Images " + number_format.format(Options.num_image))
        print(
            "                  processed "
            + number_format.format(Options.num_image_processed)
        )

        if Options.config["expose_image_dates"]:
            print(
                "  + with exif date                                            "
                + number_format.format(Options.num_image_with_exif_date)
            )
        if Options.config["expose_image_positions"]:
            print(
                "  + with geotags                                              "
                + number_format.format(Options.num_image_with_geotags)
            )
        if (
            Options.config["expose_image_dates"]
            and Options.config["expose_image_positions"]
        ):
            print(
                "  + with both exif date and geotags                           "
                + number_format.format(Options.num_image_with_exif_date_and_geotags)
            )
        if final:
            print()
        if Options.config["expose_image_positions"]:
            print(
                "  + missing only geotags                                      "
                + number_format.format(
                    Options.num_image_with_exif_date_and_without_geotags
                )
            )
            if final:
                for line in Options.images_with_exif_date_and_without_geotags:
                    # pprint(line)
                    print("    - " + line["intro"])
                    if "list" in line:
                        for image in line["list"]:
                            print("         - " + image)
                print()
        if Options.config["expose_image_dates"]:
            print(
                "  + missing only exif date                                    "
                + number_format.format(
                    Options.num_image_without_exif_date_and_with_geotags
                )
            )
            if final:
                for line in Options.images_without_exif_date_and_with_geotags:
                    # pprint(line)
                    print("    - " + line["intro"])
                    if "list" in line:
                        for image in line["list"]:
                            print("         - " + image)
                print()
        if (
            Options.config["expose_image_dates"]
            and Options.config["expose_image_positions"]
        ):
            print(
                "  + missing both exif date and geotag                         "
                + number_format.format(Options.num_image_without_exif_date_or_geotags)
            )
            if final:
                for line in Options.images_without_exif_date_or_geotags:
                    # pprint(line)
                    print("    - " + line["intro"])
                    if "list" in line:
                        for image in line["list"]:
                            print("         - " + image)
                print()

        print()
        print("- Audios " + number_format.format(Options.num_audio))
        print(
            "                  processed "
            + number_format.format(Options.num_audio_processed)
        )

        if Options.config["expose_image_dates"]:
            print(
                "  + with date from metadata or file name                      "
                + number_format.format(
                    Options.num_audio_with_date_from_metadata_or_file_name
                )
            )
        if Options.config["expose_image_positions"]:
            print(
                "  + with geotags                                              "
                + number_format.format(Options.num_audio_with_geotags)
            )
        if (
            Options.config["expose_image_dates"]
            and Options.config["expose_image_positions"]
        ):
            print(
                "  + with both date from metadata or file name and geotags     "
                + number_format.format(
                    Options.num_audio_with_date_from_metadata_or_file_name_and_geotags
                )
            )
        if final:
            print()
        if Options.config["expose_image_positions"]:
            print(
                "  + missing only geotags                                      "
                + number_format.format(
                    Options.num_audio_with_date_from_metadata_or_file_name_and_without_geotags
                )
            )
            if final:
                for line in Options.audios_with_exif_date_and_without_geotags:
                    # pprint(line)
                    print("    - " + line["intro"])
                    if "list" in line:
                        for audio in line["list"]:
                            print("         - " + audio)
                print()
        if Options.config["expose_image_dates"]:
            print(
                "  + missing only date from metadata or file name              "
                + number_format.format(
                    Options.num_audio_without_date_from_metadata_or_file_name_and_with_geotags
                )
            )
            if final:
                for line in Options.audios_without_exif_date_and_with_geotags:
                    # pprint(line)
                    print("    - " + line["intro"])
                    if "list" in line:
                        for audio in line["list"]:
                            print("         - " + audio)
                print()
        if (
            Options.config["expose_image_dates"]
            and Options.config["expose_image_positions"]
        ):
            print(
                "  + missing both date from metadata or file name and geotag   "
                + number_format.format(
                    Options.num_audio_without_date_from_metadata_or_file_name_or_geotags
                )
            )
            if final:
                for line in Options.audios_without_exif_date_or_geotags:
                    # pprint(line)
                    print("    - " + line["intro"])
                    if "list" in line:
                        for audio in line["list"]:
                            print("         - " + audio)
                print()

        print()
        print("- Videos " + number_format.format(Options.num_video))
        print(
            "                  processed "
            + number_format.format(Options.num_video_processed)
        )

        if Options.config["expose_image_dates"]:
            print(
                "  + with date from metadata or file name                      "
                + number_format.format(
                    Options.num_video_with_date_from_metadata_or_file_name
                )
            )
        if Options.config["expose_image_positions"]:
            print(
                "  + with geotags                                              "
                + number_format.format(Options.num_video_with_geotags)
            )
        if (
            Options.config["expose_image_dates"]
            and Options.config["expose_image_positions"]
        ):
            print(
                "  + with both date from metadata or file name and geotags     "
                + number_format.format(
                    Options.num_video_with_date_from_metadata_or_file_name_and_geotags
                )
            )
        if final:
            print()
        if Options.config["expose_image_positions"]:
            print(
                "  + missing only geotags                                      "
                + number_format.format(
                    Options.num_video_with_date_from_metadata_or_file_name_and_without_geotags
                )
            )
            if final:
                for line in Options.videos_with_exif_date_and_without_geotags:
                    # pprint(line)
                    print("    - " + line["intro"])
                    if "list" in line:
                        for video in line["list"]:
                            print("         - " + video)
                print()
        if Options.config["expose_image_dates"]:
            print(
                "  + missing only date from metadata or file name              "
                + number_format.format(
                    Options.num_video_without_date_from_metadata_or_file_name_and_with_geotags
                )
            )
            if final:
                for line in Options.videos_without_exif_date_and_with_geotags:
                    # pprint(line)
                    print("    - " + line["intro"])
                    if "list" in line:
                        for video in line["list"]:
                            print("         - " + video)
                print()
        if (
            Options.config["expose_image_dates"]
            and Options.config["expose_image_positions"]
        ):
            print(
                "  + missing both date from metadata or file name and geotag   "
                + number_format.format(
                    Options.num_video_without_date_from_metadata_or_file_name_or_geotags
                )
            )
            if final:
                for line in Options.videos_without_exif_date_or_geotags:
                    # pprint(line)
                    print("    - " + line["intro"])
                    if "list" in line:
                        for video in line["list"]:
                            print("         - " + video)
                print()

        print()
        print(
            "- Unrecognized files                                          "
            + number_format.format(Options.num_unrecognized_files)
        )
        if final:
            for line in Options.unrecognized_files:
                # pprint(line)
                print("    - " + line["intro"])
                if "list" in line:
                    for error in line["list"]:
                        print("         - " + error)
            print()

        print(
            "- PIL DecompressionBombError files                            "
            + number_format.format(Options.num_decompression_bomb_error_files)
        )
        if final:
            for line in Options.decompression_bomb_error_files:
                # pprint(line)
                print("    - " + line["intro"])
                if "list" in line:
                    for error in line["list"]:
                        print("         - " + error)
            print()

        print(
            "- big size for format warnings                                "
            + number_format.format(Options.num_big_size_for_format_warnings)
        )
        if final:
            for line in Options.big_size_for_format_warnings:
                # pprint(line)
                print("    - " + line["intro"])
                if "list" in line:
                    for error in line["list"]:
                        print("         - " + error)
            print()

        print(
            "- other errors                                                "
            + number_format.format(Options.num_other_errors)
        )
        if final:
            for line in Options.other_errors:
                # pprint(line)
                print("    - " + line["intro"])
                if "list" in line:
                    for error in line["list"]:
                        print("         - " + error)
            print()

        if final and not Options.ffmpeg_ffprobe_are_there:
            print("WARNING: ffmpeg isn't installed!")
            print(
                "         Videos haven't been managed, unless their cache files were there and valid"
            )
            print()

        print()
