# -*- coding: utf-8 -*-

import os
import os.path
import sys
import json
import re
import time
import random
import math
import fnmatch
import shutil
import copy
import glob

from datetime import datetime
from pprint import pprint

from PIL import Image, UnidentifiedImageError

from CachePath import remove_album_path, last_modification_time, trim_base_custom
from CachePath import image_cache_name, remove_folders_marker, original_media_is_enough
from CachePath import (
    transliterate_to_ascii,
    remove_accents,
    remove_non_alphabetic_characters,
    remove_new_lines_and_tags,
)
from CachePath import remove_digits, switch_to_lowercase, phrase_to_words, checksum
from Utilities import save_password_codes, json_files_and_mtimes, report_mem
from Utilities import (
    convert_identifiers_set_to_codes_set,
    convert_identifiers_set_to_md5s_set,
)
from Utilities import (
    create_complex_dir_and_subdirs,
    convert_combination_to_set,
    convert_set_to_combination,
    convert_md5_to_code,
)
from Utilities import (
    md5_products,
    convert_simple_md5_combination_to_simple_codes_combination,
    complex_combination,
)
from Utilities import determine_symlink_number_and_name
from Utilities import (
    message,
    indented_message,
    next_level,
    back_level,
)
from Utilities import (
    report_times,
    file_mtime,
    unlink_file,
    make_dir,
    touch_with_datetime,
    set_permissions_according_to_umask,
)
from PhotoAlbum import (
    SingleMedia,
    Album,
    PhotoAlbumEncoder,
    Position,
    Positions,
    NumsProtected,
    Sizes,
    SizesProtected,
)
from Geonames import Geonames
import Options


class TreeWalker:
    def __init__(self):
        def save_and_rename_options_file():
            # options must be saved again at the end, in order to update the number of positions
            # provisional options file must be renamed to its regular name or deleted, too
            num_positions = 0
            if Options.config["expose_image_positions"]:
                num_positions = len(folders_album.positions_and_media_in_tree.positions)
            self._save_json_options(num_positions)
            message("renaming provisionale options file", "", 6)
            os.rename(
                options_file_with_path_and_timestamp,
                options_file_with_path,
            )
            indented_message("provisionale options file renamed", "", 6)

            # end of second order functions

        random.seed()

        self.all_json_files = []

        first_time_true = True
        save_password_codes(first_time_true)
        # self.all_json_files.extend(saved_files)

        # saves the provisional options.json file, with the timestamp at the end of its name
        self._save_json_options(0)

        self.time_of_album_saving = None
        self.media_with_geonames_list = []
        self.all_media = []
        self.all_composite_images = []

        if Options.config["use_stop_words"]:
            TreeWalker.stopwords_file = os.path.join(
                Options.config["cache_path"], "stopwords.json"
            )
            self.all_json_files.append("stopwords.json")
            self.get_lowercase_stopwords()

        self.tree_by_search = {}

        regular_albums_subdir = os.path.join(
            Options.config["cache_path"], Options.config["regular_album_subdir"]
        )
        make_dir(regular_albums_subdir, "regular albums subdir")

        if Options.config["expose_image_dates"]:
            self.tree_by_date = {}
            by_date_subdir = os.path.join(
                Options.config["cache_path"], Options.config["by_date_album_subdir"]
            )
            make_dir(by_date_subdir, "by date subdir")

        if Options.config["expose_image_positions"]:
            geonames = Geonames()
            self.tree_by_geonames = {}
            by_gps_subdir = os.path.join(
                Options.config["cache_path"], Options.config["by_gps_album_subdir"]
            )
            make_dir(by_gps_subdir, "by gps subdir")

        by_search_subdir = os.path.join(
            Options.config["cache_path"], Options.config["by_search_album_subdir"]
        )
        make_dir(by_search_subdir, "search subdir")

        # create the directory where php will put album composite images
        composite_images_dir = os.path.join(
            Options.config["cache_path"], Options.config["composite_images_subdir"]
        )
        make_dir(composite_images_dir, "composite images subdir")

        # create the directory where javascript will put album composite images
        composite_images_dir_js = os.path.join(
            Options.config["cache_path"], Options.config["composite_images_subdir_js"]
        )
        make_dir(composite_images_dir_js, "javascript composite images subdir")
        os.chmod(composite_images_dir_js, 0o777)

        self.origin_album = Album(Options.config["album_path"])
        self.origin_album.cache_base = "root"

        if Options.config["pil_size_for_decompression_bomb_error"] <= 0:
            Image.MAX_IMAGE_PIXELS = None
        else:
            # We devide by 2 because PIL parameter triggers the warning,
            # the error is thrown when the size twice the warning threshold
            Image.MAX_IMAGE_PIXELS = int(
                Options.config["pil_size_for_decompression_bomb_error"] / 2
            )

        message("Browsing", "start!", 1)

        by_date_path = os.path.join(
            Options.config["album_path"], Options.config["by_date_string"]
        )
        self.by_date_album = Album(by_date_path)
        self.by_date_album.cache_base = Options.config["by_date_string"]

        by_geonames_path = os.path.join(
            Options.config["album_path"], Options.config["by_gps_string"]
        )
        self.by_geonames_album = Album(by_geonames_path)
        self.by_geonames_album.cache_base = Options.config["by_gps_string"]

        by_search_path = os.path.join(
            Options.config["album_path"], Options.config["by_search_string"]
        )
        self.by_search_album = Album(by_search_path)
        self.by_search_album.cache_base = Options.config["by_search_string"]

        next_level()  # OK
        [folders_album, passwords_or_album_ini_processed] = self.walk(
            Options.config["album_path"],
            Options.config["folders_string"],
            [],
            None,
            set(),
            self.origin_album,
        )
        back_level()  # OK

        # save the password codes
        first_time_false = False
        saved_files = save_password_codes(first_time_false)
        self.all_json_files.extend(saved_files)

        if folders_album is None:
            message("WARNING", "ALBUMS ROOT EXCLUDED BY MARKER FILE", 2)
        else:
            # # maybe nothing has changed and everything could end up here
            # if not passwords_or_album_ini_processed and not Options.num_image_processed and not Options.num_video_processed:
            # 	message("nothing has changed in the album tree!", "Execution may end up here", 3)
            # 	return

            # remove the timestamp from the options file
            options_file_with_path = os.path.join(
                Options.config["cache_path"], "options.json"
            )
            options_file_with_path_and_timestamp = (
                options_file_with_path + "." + Options.config["options_timestamp"]
            )

            if folders_album.clean and not Options.update[None]["transversal_albums"]:
                message(
                    "No need to go on",
                    "No change has occurred in the albums tree, no sensible option has changed",
                    3,
                )
                save_and_rename_options_file()
                # unlink_file(options_file_with_path_and_timestamp)
                provisional_options_files_list = glob.glob(
                    options_file_with_path + ".*"
                )
                for filePath in provisional_options_files_list:
                    os.remove(filePath)
            else:
                message_parts = []
                if not folders_album.clean:
                    message_parts.append("changes have occurred in the album tree")
                if Options.update[None]["transversal_albums"]:
                    message_parts.append("some sensible option has changed")
                message(
                    "We need to go on",
                    " and ".join(message_parts)
                    + ": "
                    + "I must save again the albums by date, by geonames, by search",
                    3,
                )
                next_level()  # OK

                if Options.config["expose_image_dates"]:
                    self.final_calculations_for_by_date_albums()
                    self.save_by_date_albums()
                    report_mem()

                if Options.config["expose_image_positions"]:
                    self.final_calculations_for_by_geonames_albums()
                    self.save_by_geonames_albums()
                    report_mem()

                self.final_calculations_for_by_search_albums()

                self.origin_album.nums_protected_media_in_sub_tree.merge(
                    folders_album.nums_protected_media_in_sub_tree
                )
                self.origin_album.sizes_protected_media_in_sub_tree.merge(
                    folders_album.sizes_protected_media_in_sub_tree
                )
                if Options.config["expose_image_positions"]:
                    self.origin_album.nums_protected_media_in_sub_tree_non_geotagged.merge(
                        folders_album.nums_protected_media_in_sub_tree_non_geotagged
                    )
                    self.origin_album.sizes_protected_media_in_sub_tree_non_geotagged.merge(
                        folders_album.sizes_protected_media_in_sub_tree_non_geotagged
                    )
                self.origin_album.add_subalbum(folders_album)

                # self.all_json_files.append(Options.config['folders_string'] + ".json")

                # message("generating by search albums...", "", 4)
                # by_search_album = self.generate_by_search_albums(self.origin_album)
                # indented_message("by search albums generated", "", 5)
                if self.by_search_album is not None and not self.by_search_album.empty:
                    # self.all_json_files.append(Options.config['by_search_string'] + ".json")
                    self.origin_album.nums_protected_media_in_sub_tree.merge(
                        self.by_search_album.nums_protected_media_in_sub_tree
                    )
                    self.origin_album.sizes_protected_media_in_sub_tree.merge(
                        self.by_search_album.sizes_protected_media_in_sub_tree
                    )
                    if Options.config["expose_image_positions"]:
                        self.origin_album.nums_protected_media_in_sub_tree_non_geotagged.merge(
                            self.by_search_album.nums_protected_media_in_sub_tree_non_geotagged
                        )
                        self.origin_album.sizes_protected_media_in_sub_tree_non_geotagged.merge(
                            self.by_search_album.sizes_protected_media_in_sub_tree_non_geotagged
                        )
                    self.origin_album.add_subalbum(self.by_search_album)
                report_mem()

                save_and_rename_options_file()

                self.all_json_files.append("options.json")

                for identifier_and_password in Options.identifiers_and_passwords:
                    # if identifier_and_password["used"]:
                    identifier = identifier_and_password["identifier"]
                    md5 = convert_identifiers_set_to_md5s_set(set([identifier])).pop()
                    self.all_json_files.append(
                        os.path.join(Options.config["passwords_subdir"], md5)
                    )

                self.all_json_files.append(Options.config["tiny_urls_file"])

                back_level()  # OK
                self.clean_up()

            report_mem()

            message("completed", "", 4)

    def check_unprotected_json_files(self, album):
        # works with a clean album
        # adds to self.all_json_files the various json files that represent the unprotected part of the album
        next_level()  # OK
        message(
            "checking unprotected json files corresponding to album...",
            "",
            3,
        )

        for argument in ["", "media", "positions"]:
            json_name = album.json_file(argument)
            if os.path.exists(os.path.join(Options.config["cache_path"], json_name)):
                self.all_json_files.append(json_name)

        indented_message(
            "unprotected json files corresponding to album checked!",
            "",
            4,
        )
        back_level()  # OK

    def check_protected_json_files(self, album, complex_identifiers_combination):
        # removes the various json files that represent the protected part of the album
        # if the album is clean look for the existing files and add them to self.all_json_files
        begin_initial_message = "removing"
        end_final_message = "removed"
        if album.clean:
            begin_initial_message = "checking"
            end_final_message = "checked"
        next_level()  # OK
        message(
            begin_initial_message
            + " old protected json files corresponding to album...",
            "",
            3,
        )

        cache_path = Options.config["cache_path"]

        [md5_product_list, codes_complex_combination] = md5_products(
            complex_identifiers_combination
        )
        for md5_combination in md5_product_list:
            protected_dir = (
                Options.config["protected_directories_prefix"] + md5_combination
            )
            absolute_dir = os.path.join(
                cache_path,
                protected_dir,
            )
            for argument in ["", "media", "positions"]:
                number = 0
                if album.clean:
                    while True:
                        json_name = album.json_file(argument, number)
                        if os.path.exists(os.path.join(absolute_dir, json_name)):
                            self.all_json_files.append(
                                os.path.join(protected_dir, json_name)
                            )
                            number += 1
                        else:
                            break

        indented_message(
            "old protected json files corresponding to album "
            + end_final_message
            + "!",
            "",
            4,
        )
        back_level()  # OK

    def save_album(
        self,
        album,
        complex_identifiers_combination,
        is_provisional_media,
        touch_only=False,
    ):
        # this function only saves the album itself, without any recursion
        # receives the album already restricted to protected/unprotected content

        # if touch_only == True, it doesn't save the album, it only touch it

        action = "saving"
        if touch_only:
            action = "touching"

        if is_provisional_media:
            message(
                action
                + " provisional media album"
                + (" to json file..." if not touch_only else ""),
                album.name,
                3,
            )
        elif complex_identifiers_combination == ",":
            message(
                action
                + " unprotected album"
                + (" to json file..." if not touch_only else ""),
                album.name,
                3,
            )
        else:
            message(
                action
                + " protected album"
                + (" to json file..." if not touch_only else ""),
                album.name + ", " + complex_identifiers_combination,
                3,
            )
        next_level()  # OK

        saved_files = []

        if album.nums_media_in_sub_tree.total() == 0 and (
            not album.cache_base.startswith(Options.config["by_search_string"])
            or album.cache_base != Options.config["by_search_string"]
            and (
                complex_identifiers_combination == ","
                and all(
                    subalbum.nums_protected_media_in_sub_tree.value(",").total() == 0
                    for subalbum in album.subalbums
                )
                or complex_identifiers_combination != ","
                and all(
                    complex_identifiers_combination
                    in subalbum.nums_protected_media_in_sub_tree.protected_keys()
                    and subalbum.nums_protected_media_in_sub_tree.value(
                        complex_identifiers_combination
                    ).total()
                    == 0
                    for subalbum in album.subalbums
                )
            )
        ):
            if complex_identifiers_combination == ",":
                indented_message(
                    "empty album, not " + action + " it", album.absolute_path, 4
                )
            else:
                indented_message(
                    "empty protected album, not " + action + " it",
                    album.absolute_path,
                    4,
                )
            back_level()  # OK
            return []

        if complex_identifiers_combination != ",":
            (
                album_identifiers_combination,
                media_identifiers_combination,
            ) = complex_identifiers_combination.split(",")

            if (
                album_identifiers_combination != ""
                and len(album.password_identifiers_set) > 0
                and set(album_identifiers_combination.split("-"))
                != album.password_identifiers_set
            ):
                # execution shouldn't arrive here
                message(
                    "album protected by another password combination, not "
                    + action
                    + " it",
                    album.absolute_path,
                    4,
                )
                back_level()  # OK
                return []

        json_media_name = ""
        json_positions_name = ""

        separate_media = not is_provisional_media and album.must_separate_media
        separate_positions = (
            not is_provisional_media
            and Options.config["expose_image_positions"]
            and album.must_separate_positions
        )

        if complex_identifiers_combination == ",":
            json_name = album.json_file("")
            if is_provisional_media:
                json_name += ".media_only"
            if separate_media:
                json_media_name = album.json_file("media")
            if separate_positions:
                json_positions_name = album.json_file("positions")

        symlinks = []
        positions_symlinks = []
        media_symlinks = []
        symlink_codes_and_numbers = []

        if complex_identifiers_combination != ",":
            [md5_product_list, codes_complex_combination] = md5_products(
                complex_identifiers_combination
            )

            first_md5_product = md5_product_list[0]

            first_dir = os.path.join(
                Options.config["cache_path"],
                Options.config["protected_directories_prefix"] + first_md5_product,
            )

            create_complex_dir_and_subdirs(first_dir)

            number = album.last_file_number[first_md5_product]
            album.last_file_number[first_md5_product] += 1

            json_name_with_path = (
                os.path.join(first_dir, album.json_file(""))[:-5]
                + "."
                + str(number)
                + ".json"
            )
            json_name = json_name_with_path[len(Options.config["cache_path"]) + 1 :]
            if separate_media:
                json_media_name = os.path.join(
                    Options.config["protected_directories_prefix"] + first_md5_product,
                    album.json_file("media", number),
                )
            if separate_positions:
                json_positions_name = os.path.join(
                    Options.config["protected_directories_prefix"] + first_md5_product,
                    album.json_file("positions", number),
                )
            symlink_codes_and_numbers.append(
                {
                    "codesSimpleCombination": convert_simple_md5_combination_to_simple_codes_combination(
                        first_md5_product
                    ),
                    "codesComplexCombination": codes_complex_combination,
                    "number": number,
                }
            )

            # symlinks must be generated in order to save the files with 2 or more passwords
            if len(md5_product_list) > 1:
                for complex_md5 in md5_product_list[1:]:
                    complex_dir = os.path.join(
                        Options.config["cache_path"],
                        Options.config["protected_directories_prefix"] + complex_md5,
                    )

                    create_complex_dir_and_subdirs(complex_dir)

                    number = album.last_file_number[complex_md5]
                    album.last_file_number[complex_md5] += 1

                    symlink_with_path = (
                        os.path.join(complex_dir, album.json_file(""))[:-5]
                        + "."
                        + str(number)
                        + ".json"
                    )
                    symlink = symlink_with_path[len(Options.config["cache_path"]) + 1 :]
                    if separate_media:
                        media_symlink = os.path.join(
                            Options.config["protected_directories_prefix"]
                            + complex_md5,
                            album.json_file("media", number),
                        )
                        media_symlinks.append(media_symlink)
                    if separate_positions:
                        positions_symlink = os.path.join(
                            Options.config["protected_directories_prefix"]
                            + complex_md5,
                            album.json_file("positions", number),
                        )
                        positions_symlinks.append(positions_symlink)

                    symlinks.append(symlink)
                    symlink_codes_and_numbers.append(
                        {
                            "codesSimpleCombination": convert_simple_md5_combination_to_simple_codes_combination(
                                complex_md5
                            ),
                            "codesComplexCombination": codes_complex_combination,
                            "number": number,
                        }
                    )

                for symlink in symlinks:
                    saved_files.append(symlink)
                if separate_media:
                    for symlink in media_symlinks:
                        saved_files.append(symlink)
                if separate_positions:
                    for symlink in positions_symlinks:
                        saved_files.append(symlink)

            album.symlink_codes_and_numbers = symlink_codes_and_numbers

        saved_files.append(json_name)
        if separate_media:
            saved_files.append(json_media_name)
        if separate_positions:
            saved_files.append(json_positions_name)

        album.to_json_files(
            json_name,
            json_positions_name,
            json_media_name,
            symlinks,
            positions_symlinks,
            media_symlinks,
            is_provisional_media,
            complex_identifiers_combination,
            touch_only,
        )
        if touch_only:
            indented_message("album's json file(s) touched!", "", 4)
        else:
            indented_message("album saved to json file(s)!", "", 4)
        back_level()  # OK
        if not is_provisional_media:
            self.all_json_files.extend(saved_files)

    def add_single_media_to_by_date_albums(self, single_media):
        next_level()  # OK
        message(
            "adding single media to by date album...",
            "",
            6,
        )

        by_date_path = os.path.join(
            Options.config["album_path"], Options.config["by_date_string"]
        )

        year_cache_base = (
            self.by_date_album.cache_base
            + Options.config["cache_folder_separator"]
            + single_media.year
        )
        year_path = os.path.join(by_date_path, str(single_media.year))

        found = False
        for subalbum in self.by_date_album.subalbums:
            if subalbum.cache_base == year_cache_base:
                year_album = subalbum
                found = True
                break
        if not found:
            year_album = Album(year_path)
            year_album.cache_base = year_cache_base
            year_album._name = single_media.year
            year_album.parent_cache_base = self.by_date_album.cache_base
            self.by_date_album.add_subalbum(year_album)

        month_cache_base = (
            year_cache_base
            + Options.config["cache_folder_separator"]
            + single_media.month
        )
        month_path = os.path.join(year_path, str(single_media.month))

        found = False
        for subalbum in year_album.subalbums:
            if subalbum.cache_base == month_cache_base:
                month_album = subalbum
                found = True
                break
        if not found:
            month_album = Album(month_path)
            month_album.cache_base = month_cache_base
            month_album._name = single_media.month
            month_album.parent_cache_base = year_album.cache_base
            year_album.add_subalbum(month_album)

        day_cache_base = (
            month_cache_base
            + Options.config["cache_folder_separator"]
            + single_media.day
        )
        day_path = os.path.join(month_path, str(single_media.day))

        found = False
        for subalbum in month_album.subalbums:
            if subalbum.cache_base == day_cache_base:
                day_album = subalbum
                found = True
                break
        if not found:
            day_album = Album(day_path)
            day_album.cache_base = day_cache_base
            day_album._name = single_media.day
            day_album.parent_cache_base = month_album.cache_base
            month_album.add_subalbum(day_album)

        single_media.day_album_cache_base = day_album.cache_base

        day_album.add_single_media_complete(single_media)
        month_album.add_single_media_complete(single_media)
        year_album.add_single_media_complete(single_media)
        self.by_date_album.add_single_media_complete(single_media)

        indented_message(
            "single media added to by date album!",
            single_media.year + "-" + single_media.month + "-" + single_media.day,
            5,
        )
        back_level()  # OK

    def final_calculations_for_by_date_albums(self):
        next_level()  # OK
        self.by_date_album.sort_subalbums_and_media(True)

        message(
            "working out by date albums...",
            "",
            4,
        )
        for year_album in self.by_date_album.subalbums:
            next_level()  # OK
            year_album.sort_subalbums_and_media(True)

            for month_album in year_album.subalbums:
                month_album.sort_subalbums_and_media(True)

                next_level()  # OK
                for day_album in month_album.subalbums:
                    next_level()  # OK
                    day_album.sort_subalbums_and_media(True)

                    message(
                        "working out day album...",
                        day_album.media_list[0].year
                        + "-"
                        + day_album.media_list[0].month
                        + "-"
                        + day_album.media_list[0].day,
                        4,
                    )
                    day_album.exif_date_max = day_album.album_exif_date_max()
                    day_album.exif_date_min = day_album.album_exif_date_min()
                    if Options.config["expose_original_media"]:
                        day_album.file_date_max = day_album.album_file_date_max()
                        day_album.file_date_min = day_album.album_file_date_min()
                    day_album.pixel_size_max = day_album.album_pixels_max()
                    day_album.pixel_size_min = day_album.album_pixels_min()
                    Options.all_albums.append(day_album)

                    day_album.split_protected_and_unprotected_content()
                    indented_message(
                        "day album worked out!",
                        "",
                        5,
                    )
                    back_level()  # OK

                message(
                    "working out month album...",
                    day_album.media_list[0].year + "-" + day_album.media_list[0].month,
                    4,
                )
                month_album.exif_date_max = month_album.album_exif_date_max()
                month_album.exif_date_min = month_album.album_exif_date_min()
                if Options.config["expose_original_media"]:
                    month_album.file_date_max = month_album.album_file_date_max()
                    month_album.file_date_min = month_album.album_file_date_min()
                month_album.pixel_size_max = month_album.album_pixels_max()
                month_album.pixel_size_min = month_album.album_pixels_min()
                Options.all_albums.append(month_album)

                month_album.split_protected_and_unprotected_content()
                indented_message(
                    "month album worked out!",
                    "",
                    5,
                )
                back_level()  # OK

            message(
                "working out year album...",
                day_album.media_list[0].year,
                4,
            )
            year_album.exif_date_max = year_album.album_exif_date_max()
            year_album.exif_date_min = year_album.album_exif_date_min()
            if Options.config["expose_original_media"]:
                year_album.file_date_max = year_album.album_file_date_max()
                year_album.file_date_min = year_album.album_file_date_min()
            year_album.pixel_size_max = year_album.album_pixels_max()
            year_album.pixel_size_min = year_album.album_pixels_min()
            Options.all_albums.append(year_album)

            year_album.split_protected_and_unprotected_content()
            indented_message(
                "year album worked out!",
                "",
                5,
            )
            back_level()  # OK
        message(
            "working out by dates album...",
            "",
            4,
        )
        self.by_date_album.exif_date_max = self.by_date_album.album_exif_date_max()
        self.by_date_album.exif_date_min = self.by_date_album.album_exif_date_min()
        if Options.config["expose_original_media"]:
            self.by_date_album.file_date_max = self.by_date_album.album_file_date_max()
            self.by_date_album.file_date_min = self.by_date_album.album_file_date_min()
        self.by_date_album.pixel_size_max = self.by_date_album.album_pixels_max()
        self.by_date_album.pixel_size_min = self.by_date_album.album_pixels_min()
        Options.all_albums.append(self.by_date_album)

        self.by_date_album.split_protected_and_unprotected_content()
        indented_message(
            "by dates album worked out!",
            "",
            5,
        )
        back_level()  # OK

    def save_by_date_albums(self):
        next_level()  # OK
        message(
            "saving by date albums...",
            "",
            4,
        )
        for year_album in self.by_date_album.subalbums:
            next_level()  # OK
            for month_album in year_album.subalbums:
                next_level()  # OK
                for day_album in month_album.subalbums:
                    next_level()  # OK
                    message(
                        "saving day album...",
                        day_album.media_list[0].year
                        + "-"
                        + day_album.media_list[0].month
                        + "-"
                        + day_album.media_list[0].day,
                        4,
                    )
                    self.save_protected_and_unprotected_albums(day_album)
                    indented_message(
                        "day album saved!",
                        "",
                        5,
                    )
                    back_level()  # OK

                message(
                    "saving month album...",
                    day_album.media_list[0].year + "-" + day_album.media_list[0].month,
                    4,
                )
                self.save_protected_and_unprotected_albums(month_album)
                indented_message(
                    "month album savd!",
                    "",
                    5,
                )
                back_level()  # OK

            message(
                "saving year album...",
                day_album.media_list[0].year,
                4,
            )
            self.save_protected_and_unprotected_albums(year_album)
            indented_message(
                "year album saved!",
                "",
                5,
            )
            back_level()  # OK
        message(
            "saving by dates album...",
            "",
            4,
        )
        self.save_protected_and_unprotected_albums(self.by_date_album)
        indented_message(
            "by dates album saved!",
            "",
            5,
        )
        back_level()  # OK

    def make_clusters(self, media_list, k, time_factor):
        # found = False
        # while len(media_list) > 0 and not found:
        cluster_list = Geonames.find_centers(media_list, k, time_factor)
        good_clusters = []
        remaining_media_list = []
        big_clusters = []
        # found = False
        for cluster in cluster_list:
            if len(cluster) <= Options.config["big_virtual_folders_threshold"]:
                good_clusters.append(cluster)
                # found = True
            else:
                big_clusters.append(cluster)
                remaining_media_list.extend(cluster)
        return [good_clusters, big_clusters, remaining_media_list]

    def make_clusters_recursively(
        self, cluster_list_ok, media_list, k, n_recursion, time_factor
    ):
        [good_clusters, big_clusters, remaining_media_list] = self.make_clusters(
            media_list, k, time_factor
        )

        for cluster in good_clusters:
            cluster_list_ok.append(cluster)
            indented_message(
                "found cluster n.",
                str(len(cluster_list_ok))
                + ", "
                + str(len(cluster))
                + " images, at recursion n. "
                + str(n_recursion),
                5,
            )
        for cluster in big_clusters:
            indented_message(
                "found big cluster",
                str(len(cluster)) + " images, at recursion n. " + str(n_recursion),
                5,
            )

        if len(big_clusters) == 0:
            return [[], []]
        else:
            if len(big_clusters) == 1 and len(media_list) == len(remaining_media_list):
                indented_message(
                    "no way to cluster any further",
                    "still "
                    + str(len(remaining_media_list))
                    + " images, at recursion n. "
                    + str(n_recursion),
                    5,
                )
                return [big_clusters, remaining_media_list]
            else:
                indented_message(
                    "big clusters:",
                    "still "
                    + str(len(big_clusters))
                    + " clusters, "
                    + str(len(remaining_media_list))
                    + " images, clustering them recursively",
                    5,
                )
                all_remaining_media = []
                remaining_clusters = []
                indented_message("cycling in big clusters...", "", 5)
                next_level()  # OK
                for cluster in big_clusters:
                    indented_message(
                        "working with a big cluster", str(len(cluster)) + " images", 5
                    )
                    [cluster_list, remaining_media] = self.make_clusters_recursively(
                        cluster_list_ok, cluster, k, n_recursion + 1, time_factor
                    )
                    if len(remaining_media) > 0:
                        remaining_clusters.extend(cluster_list)
                        all_remaining_media.extend(remaining_media)
                back_level()  # OK
                return [remaining_clusters, all_remaining_media]

    def make_k_means_cluster(self, cluster_list_ok, media_list, time_factor):
        max_cluster_length_list = [0, 0, 0]
        k = 4
        if Options.config["big_virtual_folders_threshold"] < k:
            k = 2
        while True:
            message(
                "clustering with k-means algorithm...",
                "time factor = " + str(time_factor) + ", k = " + str(k),
                5,
            )
            n_recursion = 1
            [big_clusters, remaining_media] = self.make_clusters_recursively(
                cluster_list_ok, media_list, k, n_recursion, time_factor
            )
            if len(remaining_media) == 0:
                break
            else:
                # detect no convergence
                max_cluster_length = max([len(cluster) for cluster in big_clusters])
                max_cluster_length_list.append(max_cluster_length)
                max_cluster_length_list.pop(0)
                if max(max_cluster_length_list) > 0 and max(
                    max_cluster_length_list
                ) == min(max_cluster_length_list):
                    # three times the same value: no convergence
                    indented_message(
                        "clustering with k-means algorithm failed",
                        "max cluster length doesn't converge, it's stuck at "
                        + str(max_cluster_length),
                        5,
                    )
                    break

                if k > len(media_list):
                    indented_message(
                        "clustering with k-means algorithm failed",
                        "clusters remain too big even with k > number of images ("
                        + str(len(media_list))
                        + ")",
                        5,
                    )
                    break
                indented_message(
                    "clustering with k-means algorithm not ok",
                    "biggest cluster has "
                    + str(max_cluster_length)
                    + " images, doubling the k factor",
                    5,
                )
                media_list = remaining_media
                k = k * 2
        return [big_clusters, remaining_media]

    def add_single_media_to_by_geonames_albums(self, single_media):
        next_level()  # OK
        message(
            "adding single media to by geonames album...",
            "",
            6,
        )

        by_geonames_path = os.path.join(
            Options.config["album_path"], Options.config["by_gps_string"]
        )

        country_cache_base = (
            self.by_geonames_album.cache_base
            + Options.config["cache_folder_separator"]
            + single_media.country_code.lower()
        )
        country_path = os.path.join(by_geonames_path, str(single_media.country_code))

        found = False
        for subalbum in self.by_geonames_album.subalbums:
            if subalbum.cache_base == country_cache_base:
                country_album = subalbum
                found = True
                break
        if not found:
            country_album = Album(country_path)
            country_album.cache_base = country_cache_base
            country_album._name = single_media.country_name
            # country_album.center = {}
            country_album.parent_cache_base = self.by_geonames_album.cache_base
            self.by_geonames_album.add_subalbum(country_album)

        region_cache_base = (
            country_cache_base
            + Options.config["cache_folder_separator"]
            + single_media.region_code.lower()
        )
        region_path = os.path.join(country_path, str(single_media.region_code))

        found = False
        for subalbum in country_album.subalbums:
            if subalbum.cache_base == region_cache_base:
                region_album = subalbum
                found = True
                break
        if not found:
            region_album = Album(region_path)
            region_album.cache_base = region_cache_base
            region_album._name = single_media.region_name
            region_album.parent_cache_base = country_album.cache_base
            country_album.add_subalbum(region_album)

        place_cache_base = (
            region_cache_base
            + Options.config["cache_folder_separator"]
            + str(single_media.place_code)
        )
        place_path = os.path.join(region_path, str(single_media.place_code))

        found = False
        for subalbum in region_album.subalbums:
            if subalbum.cache_base == place_cache_base:
                place_album = subalbum
                found = True
                break
        if not found:
            place_album = Album(place_path)
            place_album.cache_base = place_cache_base
            place_album._name = single_media.place_name
            # place_album.center = {}
            place_album.parent_cache_base = region_album.cache_base
            region_album.add_subalbum(place_album)

        single_media.gps_album_cache_base = place_album.cache_base
        single_media.gps_path = remove_album_path(place_path)
        single_media.alt_place_name = single_media.place_name

        place_album.add_single_media_complete(single_media)
        region_album.add_single_media_complete(single_media)
        country_album.add_single_media_complete(single_media)
        self.by_geonames_album.add_single_media_complete(single_media)

        indented_message(
            "single media added to by geonames album!",
            single_media.country_name
            + " - "
            + single_media.region_name
            + " - "
            + single_media.place_name,
            5,
        )
        back_level()  # OK

    def final_calculations_for_by_geonames_albums(self):
        def split_big_place_album():
            # big place album is now split into various "place 1", "place 2",
            # they are made as subalbums of place_album
            # clustering is made with the kmeans algorithm

            next_level()  # OK
            # TO DO: really is sorting by latitude needed?
            message("sorting media by latitude...", "", 5)
            media_list.sort(key=lambda m: m.latitude)
            indented_message("media sorted!", "", 6)

            # the array cluster_list_ok is used in order to detect when there is no convertion
            cluster_list_ok = []
            n_cluster = 0
            time_factor = 0
            [
                big_clusters,
                remaining_media_list,
            ] = self.make_k_means_cluster(cluster_list_ok, media_list, time_factor)
            if len(remaining_media_list) > 0:
                time_factor = 0.01
                [
                    big_clusters,
                    remaining_media_list,
                ] = self.make_k_means_cluster(
                    cluster_list_ok, remaining_media_list, time_factor
                )
                if len(remaining_media_list) > 0:
                    # we must split the big clusters into smaller ones
                    # but, first, sort media in cluster by date, so that we get more homogeneus clusters
                    message(
                        "splitting remaining big clusters into smaller ones...",
                        "",
                        5,
                    )
                    n = 0
                    for cluster in big_clusters:
                        n += 1
                        next_level()  # OK
                        integer_ratio = (
                            len(cluster)
                            // Options.config["big_virtual_folders_threshold"]
                        )
                        if (
                            integer_ratio
                            != len(cluster)
                            / Options.config["big_virtual_folders_threshold"]
                        ):
                            integer_ratio += 1
                        message(
                            "working with remaining clusters:",
                            "cluster n."
                            + str(n)
                            + ", "
                            + str(len(cluster))
                            + " media, splitting it into "
                            + str(integer_ratio)
                            + " clusters",
                            5,
                        )

                        # sort the cluster by date
                        next_level()  # OK
                        message("sorting cluster by date...", "", 5)
                        cluster.sort()
                        indented_message("cluster sorted by date", "", 6)

                        message("splitting cluster...", "", 5)
                        next_level()  # OK
                        new_length = len(cluster) // integer_ratio
                        if new_length != len(cluster) / integer_ratio:
                            new_length += 1
                        for index in range(integer_ratio):
                            n_cluster += 1
                            start = index * new_length
                            end = (index + 1) * new_length
                            new_cluster = cluster[start:end]
                            message(
                                "generating cluster n.",
                                str(n_cluster)
                                + ", containing "
                                + str(len(new_cluster))
                                + " images",
                                5,
                            )
                            cluster_list_ok.append(new_cluster)
                        back_level()  # OK
                        indented_message("cluster splitted", "", 6)
                        back_level()  # OK
                        back_level()  # OK
            back_level()
            return cluster_list_ok

        def add_cluster_to_place_album():
            next_level()  # OK
            message(
                "processing cluster n.",
                str(i + 1) + " (" + str(len(cluster)) + " images)",
                5,
            )
            alt_place_code = str(i + 1).zfill(num_digits)
            alt_place_name = place_name + "_" + str(i + 1).zfill(num_digits)

            place_path = os.path.join(
                Options.config["by_gps_string"],
                str(media_list[0].country_code),
                str(media_list[0].region_code),
                str(alt_place_code),
            )
            absolute_place_path = os.path.join(Options.config["album_path"], place_path)

            cluster_in_place_album = Album(absolute_place_path)
            cluster_in_place_album._name = place_name
            cluster_in_place_album.alt_name = alt_place_name
            # cluster_in_place_album.center = {}
            cluster_in_place_album.parent_cache_base = place_album.cache_base
            cluster_in_place_album.cache_base = place_album.generate_cache_base(
                os.path.join(place_album.path, alt_place_code)
            )

            for single_media in cluster:
                single_media.gps_path = place_path
                single_media.place_name = place_name
                single_media.alt_place_name = alt_place_name

                cluster_in_place_album.add_single_media_complete(single_media)

                single_media.gps_album_cache_base = cluster_in_place_album.cache_base
                if cluster_in_place_album.center == {}:
                    cluster_in_place_album.center = {
                        "latitude": single_media.latitude,
                        "longitude": single_media.longitude,
                    }
                else:
                    cluster_in_place_album.center = {
                        "latitude": Geonames.recalculate_mean(
                            cluster_in_place_album.center["latitude"],
                            len(cluster_in_place_album.media_list),
                            single_media.latitude,
                        ),
                        "longitude": Geonames.recalculate_mean(
                            place_album.center["longitude"],
                            len(place_album.media_list),
                            single_media.longitude,
                        ),
                    }

            cluster_in_place_album.sort_subalbums_and_media(True)

            if Options.config["expose_image_dates"]:
                cluster_in_place_album.exif_date_max = (
                    cluster_in_place_album.album_exif_date_max()
                )
                cluster_in_place_album.exif_date_min = (
                    cluster_in_place_album.album_exif_date_min()
                )
            if Options.config["expose_original_media"]:
                cluster_in_place_album.file_date_max = (
                    cluster_in_place_album.album_file_date_max()
                )
                cluster_in_place_album.file_date_min = (
                    cluster_in_place_album.album_file_date_min()
                )
            cluster_in_place_album.pixel_size_max = (
                cluster_in_place_album.album_pixels_max()
            )
            cluster_in_place_album.pixel_size_min = (
                cluster_in_place_album.album_pixels_min()
            )

            Options.all_albums.append(cluster_in_place_album)

            cluster_in_place_album.split_protected_and_unprotected_content()

            place_album.add_subalbum(cluster_in_place_album)
            indented_message(
                "cluster worked out",
                str(i + 1)
                + "-th cluster: "
                + cluster[0].country_name
                + " - "
                + cluster[0].region_name
                + " - "
                + cluster[0].place_name
                + " - "
                + alt_place_name,
                4,
            )

            back_level()  # OK
            return
            ####### end of nested functions

        ########### begin of function body
        next_level()  # OK
        message(
            "working out by geonames albums...",
            "",
            4,
        )
        self.by_geonames_album.sort_subalbums_and_media(True)

        for country_album in self.by_geonames_album.subalbums:
            next_level()  # OK
            country_album.sort_subalbums_and_media(True)

            for region_album in country_album.subalbums:
                next_level()  # OK
                region_album.sort_subalbums_and_media(True)

                for place_album in region_album.subalbums:
                    next_level()  # OK
                    place_album.sort_subalbums_and_media(True)

                    # check if some the place album has too many media
                    # in that case it will be split into various (clustering)

                    message(
                        "working out place album...",
                        "in "
                        + region_album.media_list[0].country_name
                        + " - "
                        + region_album.media_list[0].region_name
                        + " - "
                        + place_album.media_list[0].place_name,
                        4,
                    )

                    next_level()  # OK
                    message(
                        "checking if it's a big list...",
                        "",
                        6,
                    )
                    if (
                        len(place_album.media_list)
                        <= Options.config["big_virtual_folders_threshold"]
                    ):
                        indented_message(
                            "it's not a big list",
                            place_album.media_list[0].place_name,
                            5,
                        )
                    else:
                        indented_message(
                            "it's a big list!",
                            place_album.media_list[0].place_name
                            + " has "
                            + str(len(place_album.media_list))
                            + " images, limit is "
                            + str(Options.config["big_virtual_folders_threshold"]),
                            5,
                        )

                        media_list = place_album.media_list
                        place_code = str(media_list[0].place_code)
                        place_name = media_list[0].place_name

                        cluster_list = split_big_place_album()

                        max_cluster_length = max(
                            len(cluster) for cluster in cluster_list
                        )
                        message(
                            "clustering ok",
                            str(len(cluster_list))
                            + " clusters, biggest one has "
                            + str(max_cluster_length)
                            + " media",
                            5,
                        )

                        # iterate on cluster_list
                        num_digits = len(str(len(cluster_list)))
                        alt_place_code = place_code
                        alt_place_name = place_name
                        set_alt_place = len(cluster_list) > 1
                        for i, cluster in enumerate(cluster_list):
                            add_cluster_to_place_album()
                        # end of clustering

                    back_level()
                    message(
                        "working out place album...",
                        place_album.media_list[0].country_name
                        + " - "
                        + place_album.media_list[0].region_name
                        + " - "
                        + place_album.media_list[0].alt_place_name,
                        4,
                    )
                    if Options.config["expose_image_dates"]:
                        place_album.exif_date_max = place_album.album_exif_date_max()
                        place_album.exif_date_min = place_album.album_exif_date_min()
                    if Options.config["expose_original_media"]:
                        place_album.file_date_max = place_album.album_file_date_max()
                        place_album.file_date_min = place_album.album_file_date_min()
                    place_album.pixel_size_max = place_album.album_pixels_max()
                    place_album.pixel_size_min = place_album.album_pixels_min()
                    Options.all_albums.append(place_album)

                    place_album.split_protected_and_unprotected_content()
                    indented_message(
                        "place album worked out!",
                        "",
                        4,
                    )
                    back_level()  # OK

                message(
                    "working out region album...",
                    place_album.media_list[0].country_name
                    + " - "
                    + place_album.media_list[0].region_name,
                    4,
                )
                if Options.config["expose_image_dates"]:
                    region_album.exif_date_max = region_album.album_exif_date_max()
                    region_album.exif_date_min = region_album.album_exif_date_min()
                if Options.config["expose_original_media"]:
                    region_album.file_date_max = region_album.album_file_date_max()
                    region_album.file_date_min = region_album.album_file_date_min()
                region_album.pixel_size_max = region_album.album_pixels_max()
                region_album.pixel_size_min = region_album.album_pixels_min()
                Options.all_albums.append(region_album)

                region_album.split_protected_and_unprotected_content()
                indented_message(
                    "region album worked out!",
                    "",
                    4,
                )
                back_level()  # OK
            message(
                "working out country album...",
                place_album.media_list[0].country_name,
                4,
            )
            if Options.config["expose_image_dates"]:
                country_album.exif_date_max = country_album.album_exif_date_max()
                country_album.exif_date_min = country_album.album_exif_date_min()
            if Options.config["expose_original_media"]:
                country_album.file_date_max = country_album.album_file_date_max()
                country_album.file_date_min = country_album.album_file_date_min()
            country_album.pixel_size_max = country_album.album_pixels_max()
            country_album.pixel_size_min = country_album.album_pixels_min()
            Options.all_albums.append(country_album)

            country_album.split_protected_and_unprotected_content()
            indented_message(
                "country album worked out!",
                "",
                4,
            )
            back_level()  # OK
        message(
            "working out by geonames album...",
            "",
            4,
        )
        if Options.config["expose_image_dates"]:
            self.by_geonames_album.exif_date_max = (
                self.by_geonames_album.album_exif_date_max()
            )
            self.by_geonames_album.exif_date_min = (
                self.by_geonames_album.album_exif_date_min()
            )
        if Options.config["expose_original_media"]:
            self.by_geonames_album.file_date_max = (
                self.by_geonames_album.album_file_date_max()
            )
            self.by_geonames_album.file_date_min = (
                self.by_geonames_album.album_file_date_min()
            )
        self.by_geonames_album.pixel_size_max = (
            self.by_geonames_album.album_pixels_max()
        )
        self.by_geonames_album.pixel_size_min = (
            self.by_geonames_album.album_pixels_min()
        )
        Options.all_albums.append(self.by_geonames_album)

        self.by_geonames_album.split_protected_and_unprotected_content()
        indented_message(
            "by geonames album worked out!",
            "",
            4,
        )
        back_level()  # OK

    def save_by_geonames_albums(self):
        next_level()  # OK
        message(
            "saving by geonames albums...",
            "",
            4,
        )
        for country_album in self.by_geonames_album.subalbums:
            next_level()  # OK
            for region_album in country_album.subalbums:
                next_level()  # OK
                for place_album in region_album.subalbums:
                    next_level()  # OK
                    for cluster_album in place_album.subalbums:
                        next_level()  # OK
                        message(
                            "saving cluster album...",
                            place_album.media_list[0].country_name
                            + " - "
                            + place_album.media_list[0].region_name
                            + " - "
                            + place_album.media_list[0].place_name
                            + " - "
                            + cluster_album.media_list[0].alt_place_name,
                            4,
                        )
                        self.save_protected_and_unprotected_albums(cluster_album)
                        indented_message(
                            "cluster album saved!",
                            "",
                            4,
                        )
                        back_level()  # OK

                    message(
                        "saving place album...",
                        place_album.media_list[0].country_name
                        + " - "
                        + place_album.media_list[0].region_name
                        + " - "
                        + place_album.media_list[0].alt_place_name,
                        4,
                    )
                    self.save_protected_and_unprotected_albums(place_album)
                    indented_message(
                        "place album saved!",
                        "",
                        4,
                    )
                    back_level()  # OK

                message(
                    "saving region album...",
                    place_album.media_list[0].country_name
                    + " - "
                    + place_album.media_list[0].region_name,
                    4,
                )
                self.save_protected_and_unprotected_albums(region_album)
                indented_message(
                    "region album saved!",
                    "",
                    4,
                )
                back_level()  # OK
            message(
                "saving country album...",
                place_album.media_list[0].country_name,
                4,
            )
            self.save_protected_and_unprotected_albums(country_album)
            indented_message(
                "country album saved!",
                "",
                4,
            )
            back_level()  # OK
        message(
            "saving by geonames album...",
            "",
            4,
        )
        self.save_protected_and_unprotected_albums(self.by_geonames_album)
        indented_message(
            "by geonames album saved!",
            "",
            4,
        )
        back_level()  # OK

    def final_calculations_for_by_search_albums(self):
        next_level()  # OK
        self.by_search_album.sort_subalbums_and_media(True)

        message(
            "working out by search albums:",
            "",
            4,
        )
        for word_album in self.by_search_album.subalbums:
            next_level()  # OK
            message(
                "working out word album...",
                word_album.name,
                4,
            )
            if Options.config["expose_image_dates"]:
                word_album.exif_date_max = word_album.album_exif_date_max()
                word_album.exif_date_min = word_album.album_exif_date_min()
            if Options.config["expose_original_media"]:
                word_album.file_date_max = word_album.album_file_date_max()
                word_album.file_date_min = word_album.album_file_date_min()
            word_album.pixel_size_max = word_album.album_pixels_max()
            word_album.pixel_size_min = word_album.album_pixels_min()
            Options.all_albums.append(word_album)

            word_album.sort_subalbums_and_media(True)

            word_album.split_protected_and_unprotected_content()
            self.save_protected_and_unprotected_albums(word_album)
            indented_message(
                "place album worked out!",
                "",
                4,
            )
            back_level()  # OK

        message(
            "working out by search album...",
            "",
            4,
        )
        if Options.config["expose_image_dates"]:
            self.by_search_album.exif_date_max = (
                self.by_search_album.album_exif_date_max()
            )
            self.by_search_album.exif_date_min = (
                self.by_search_album.album_exif_date_min()
            )
        if Options.config["expose_original_media"]:
            self.by_search_album.file_date_max = (
                self.by_search_album.album_file_date_max()
            )
            self.by_search_album.file_date_min = (
                self.by_search_album.album_file_date_min()
            )
        self.by_search_album.pixel_size_max = self.by_search_album.album_pixels_max()
        self.by_search_album.pixel_size_min = self.by_search_album.album_pixels_min()
        Options.all_albums.append(self.by_search_album)

        self.by_search_album.split_protected_and_unprotected_content()
        self.save_protected_and_unprotected_albums(self.by_search_album)
        indented_message(
            "by search album worked out!",
            "",
            4,
        )
        back_level()  # OK

    def remove_stopwords(self, alphabetic_words, search_normalized_words, ascii_words):
        # remove the stopwords found in alphabetic_words, from search_normalized_words and ascii_words
        purged_alphabetic_words = list(
            set(alphabetic_words) - TreeWalker.lowercase_stopwords
        )
        purged_alphabetic_words.sort(key=str.lower)
        purged_search_normalized_words = []
        purged_ascii_words = []
        # alphabetic_words = list(alphabetic_words)
        # search_normalized_words = list(search_normalized_words)
        # ascii_words = list(ascii_words)
        for word_index in range(len(alphabetic_words)):
            if alphabetic_words[word_index] in purged_alphabetic_words:
                purged_search_normalized_words.append(
                    search_normalized_words[word_index]
                )
                purged_ascii_words.append(ascii_words[word_index])
            else:
                message("stopword removed", alphabetic_words[word_index], 5)

        return (
            purged_alphabetic_words,
            purged_search_normalized_words,
            purged_ascii_words,
        )

    # The dictionaries of stopwords for the user language
    lowercase_stopwords = {}

    @staticmethod
    def load_stopwords():
        """
        Load the list of stopwords for the user language into the set `lowercase_stopwords`
        The list of stopwords comes from https://github.com/stopwords-iso/stopwords-iso
        """
        language = (
            Options.config["language"]
            if Options.config["language"] != ""
            else os.getenv("LANG")[:2]
        )
        message("PRE working with stopwords", "Using language " + language, 4)

        stopwords = []
        stopwords_file = os.path.join(
            os.path.dirname(__file__), "resources/stopwords-iso.json"
        )
        next_level()  # OK
        message("PRE loading stopwords...", stopwords_file, 4)
        with open(stopwords_file, "r") as stopwords_p:
            stopwords = json.load(stopwords_p)

        if language in stopwords:
            phrase = " ".join(stopwords[language])
            TreeWalker.lowercase_stopwords = frozenset(
                switch_to_lowercase(phrase).split()
            )
            indented_message("PRE stopwords loaded", "", 4)
            TreeWalker.save_stopwords()
        else:
            indented_message("PRE stopwords: no stopwords for language", language, 4)
        back_level()  # OK
        return

    @staticmethod
    def save_stopwords():
        """
        Saves the list of stopwords for the user language into the cache directory
        """
        message("PRE saving stopwords to cache directory", TreeWalker.stopwords_file, 4)
        with open(TreeWalker.stopwords_file, "w") as stopwords_p:
            json.dump({"stopWords": list(TreeWalker.lowercase_stopwords)}, stopwords_p)
        indented_message("PRE stopwords saved!", "", 4)
        # touch the stopwords file with the proper timestamp
        # touch_with_datetime(TreeWalker.stopwords_file)
        return

    @staticmethod
    def get_lowercase_stopwords():
        """
        Get the set of lowercase stopwords used when searching albums.
        Loads the stopwords from resource file if necessary.
        """
        if isinstance(TreeWalker.lowercase_stopwords, dict):
            TreeWalker.load_stopwords()

    def add_album_to_by_search_albums(self, album):
        next_level()  # OK
        message("adding album to the various search albums...", "", 6)
        (
            words_for_word_list,
            unicode_words,
            words_for_search_album_name,
        ) = self.prepare_for_tree_by_search(album)
        album.words = words_for_word_list
        for word_index in range(len(words_for_search_album_name)):
            word = words_for_search_album_name[word_index]
            unicode_word = unicode_words[word_index]
            if word:
                message(
                    "adding album to search album...",
                    "",
                    6,
                )
                by_search_path = os.path.join(
                    Options.config["album_path"], Options.config["by_search_string"]
                )
                word_cache_base = (
                    self.by_search_album.cache_base
                    + Options.config["cache_folder_separator"]
                    + word
                )
                word_path = os.path.join(by_search_path, str(word))

                found = False
                for subalbum in self.by_search_album.subalbums:
                    if subalbum.cache_base == word_cache_base:
                        word_album = subalbum
                        found = True
                        break
                if not found:
                    word_album = Album(word_path)
                    word_album._name = str(word)
                    word_album.cache_base = word_cache_base
                    word_album.parent_cache_base = self.by_search_album.cache_base
                    self.by_search_album.add_subalbum(word_album)

                word_album.add_subalbum(album)
                if not hasattr(word_album, "unicode_words"):
                    word_album.unicode_words = []
                if unicode_word not in word_album.unicode_words:
                    word_album.unicode_words.append(unicode_word)

                word_album.nums_protected_media_in_sub_tree.merge(
                    album.nums_protected_media_in_sub_tree
                )
                word_album.sizes_protected_media_in_sub_tree.merge(
                    album.sizes_protected_media_in_sub_tree
                )
                word_album.sizes_protected_media_in_album.merge(
                    album.sizes_protected_media_in_album
                )

                if Options.config["expose_image_positions"]:
                    word_album.nums_protected_media_in_sub_tree_non_geotagged.merge(
                        album.nums_protected_media_in_sub_tree_non_geotagged
                    )
                    word_album.sizes_protected_media_in_sub_tree_non_geotagged.merge(
                        album.sizes_protected_media_in_sub_tree_non_geotagged
                    )
                    word_album.sizes_protected_media_in_album_non_geotagged.merge(
                        album.sizes_protected_media_in_album_non_geotagged
                    )
                indented_message(
                    "album added to by search album!",
                    "search album for word '" + word + "'",
                    5,
                )
        indented_message("album added to the various search albums!", "", 6)
        back_level()  # OK

    def add_single_media_to_by_search_albums(self, single_media):
        next_level()  # OK
        (
            words_for_word_list,
            unicode_words,
            words_for_search_album_name,
        ) = self.prepare_for_tree_by_search(single_media)
        single_media.words = words_for_word_list
        for word_index in range(len(words_for_search_album_name)):
            word = words_for_search_album_name[word_index]
            unicode_word = unicode_words[word_index]
            if word:
                message("adding single media to by search album...", "", 6)
                by_search_path = os.path.join(
                    Options.config["album_path"], Options.config["by_search_string"]
                )
                word_cache_base = (
                    self.by_search_album.cache_base
                    + Options.config["cache_folder_separator"]
                    + word
                )
                word_path = os.path.join(by_search_path, str(word))

                found = False
                for subalbum in self.by_search_album.subalbums:
                    if subalbum.cache_base == word_cache_base:
                        word_album = subalbum
                        found = True
                        break
                if not found:
                    word_album = Album(word_path)
                    word_album._name = str(word)
                    word_album.cache_base = word_cache_base
                    word_album.parent_cache_base = self.by_search_album.cache_base
                    self.by_search_album.add_subalbum(word_album)

                word_album.add_single_media_complete(single_media)
                self.by_search_album.add_single_media_complete(single_media)

                if not hasattr(word_album, "unicode_words"):
                    word_album.unicode_words = []
                if unicode_word not in word_album.unicode_words:
                    word_album.unicode_words.append(unicode_word)

                indented_message("single media added to by search album!", word, 5)

        back_level()  # OK

    def prepare_for_tree_by_search(self, media_or_album):
        # add the given media or album to a temporary structure where media or albums are organized by search terms
        # works on the words in the file/directory name and in album.ini's description, title, tags

        media_or_album_name = media_or_album.name
        if isinstance(media_or_album, SingleMedia):
            # remove the extension
            media_or_album_name = os.path.splitext(media_or_album_name)[0]

        alphabetic_words = phrase_to_words(
            remove_non_alphabetic_characters(media_or_album_name)
        )
        alphabetic_words.extend(
            phrase_to_words(
                remove_non_alphabetic_characters(
                    remove_new_lines_and_tags(media_or_album.description)
                )
            )
        )
        alphabetic_words.extend(
            phrase_to_words(
                remove_non_alphabetic_characters(
                    remove_new_lines_and_tags(media_or_album.title)
                )
            )
        )

        # never remove digits from tags, they must be respected as they are
        alphabetic_words.extend(
            list(
                map(
                    lambda tag: remove_non_alphabetic_characters(tag),
                    media_or_album.tags,
                )
            )
        )

        alphabetic_words = list(filter(None, alphabetic_words))
        alphabetic_words = list(set(alphabetic_words))
        alphabetic_words.sort(key=str.lower)
        lowercase_words = list(
            map(lambda word: switch_to_lowercase(word), alphabetic_words)
        )
        search_normalized_words = list(
            map(lambda word: remove_accents(word), lowercase_words)
        )
        ascii_words = list(
            map(lambda word: transliterate_to_ascii(word), search_normalized_words)
        )

        if Options.config["use_stop_words"] and isinstance(
            TreeWalker.lowercase_stopwords, dict
        ):
            # remove stop words: do it according to the words in lower case, different words could be removed if performing remotion from every list
            next_level()  # OK
            (
                alphabetic_words,
                search_normalized_words,
                ascii_words,
            ) = self.remove_stopwords(
                alphabetic_words, search_normalized_words, ascii_words
            )
            back_level()  # OK

        return alphabetic_words, search_normalized_words, ascii_words

    @staticmethod
    def _listdir_sorted_by_time(path):
        # this function returns the directory listing sorted by mtime
        # it takes into account the fact that the file is a symlink to an nonexistent file
        mtime = (
            lambda f: os.path.exists(os.path.join(path, f))
            and os.stat(os.path.join(path, f)).st_mtime
            or time.mktime(datetime.now().timetuple())
        )
        return list(sorted(os.listdir(path), key=mtime))

    @staticmethod
    def _listdir_sorted_alphabetically(path):
        # this function returns the directory listing sorted alphabetically
        return list(sorted(os.listdir(path)))

    @staticmethod
    def _parse_password_marker(passwords_file, patterns_and_passwords):
        for line in passwords_file.read().splitlines():
            # remove leading spaces
            line = line.lstrip()
            # lines beginning with # and space-only ones are ignored
            if line[0:1] == "#" or line.strip() == "":
                continue
            columns = line.split(" ")
            if len(columns) == 1:
                # it's a simple identifier: the album and all the subalbums will be protected with the corresponding password
                identifier = columns[0]
                repetitions = [
                    identifier_dict["identifier"]
                    for identifier_dict in Options.identifiers_and_passwords
                    if identifier_dict["identifier"] == identifier
                ]
                if len(repetitions) == 0:
                    indented_message(
                        "WARNING: unknown password identifier",
                        identifier + ": not protecting the directory",
                        2,
                    )
                elif len(repetitions) == 1:
                    patterns_and_passwords.append(
                        {
                            "pattern": "*",
                            "case_flag": "ci",
                            "whole_flag": "whole",
                            "what_flag": "both",
                            "identifier": identifier,
                        }
                    )
                    indented_message(
                        "Directory protection requested",
                        "identifier: " + identifier,
                        3,
                    )
                else:
                    indented_message(
                        "WARNING: password identifier used more than once",
                        identifier + ": not protecting the directory",
                        2,
                    )
            else:
                # a password identifier followed by the flags and a file pattern
                identifier = columns[0]
                remaining_columns = " ".join(columns[1:]).lstrip().split()
                flags = remaining_columns[0]
                case_flag = "ci"
                whole_flag = "part"
                what_flag = "both"
                case = "case insensitive"
                whole = "part of name"
                what = "files and dirs"
                if flags != "-":
                    flag_list = flags.split(",")
                    try:
                        index_ci = flag_list.index("ci")
                    except:
                        index_ci = -1
                    try:
                        index_cs = flag_list.index("cs")
                    except:
                        index_cs = -2
                    try:
                        flag_list.remove("cs")
                    except:
                        pass
                    try:
                        flag_list.remove("ci")
                    except:
                        pass

                    try:
                        index_whole = flag_list.index("whole")
                    except:
                        index_whole = -2
                    try:
                        index_part = flag_list.index("part")
                    except:
                        index_part = -1
                    try:
                        flag_list.remove("whole")
                    except:
                        pass
                    try:
                        flag_list.remove("part")
                    except:
                        pass

                    try:
                        index_filesonly = flag_list.index("filesonly")
                    except:
                        index_filesonly = -3
                    try:
                        index_dirsonly = flag_list.index("dirsonly")
                    except:
                        index_dirsonly = -2
                    try:
                        index_both = flag_list.index("both")
                    except:
                        index_both = -1
                    try:
                        flag_list.remove("filesonly")
                    except:
                        pass
                    try:
                        flag_list.remove("dirsonly")
                    except:
                        pass
                    try:
                        flag_list.remove("both")
                    except:
                        pass

                    if index_cs > index_ci:
                        case_flag = "cs"
                        case = "case sensitive"

                    if index_whole > index_part:
                        whole_flag = "whole"
                        whole = "whole name"

                    if (
                        index_filesonly > index_dirsonly
                        and index_filesonly > index_both
                    ):
                        what_flag = "filesonly"
                        what = "files only"
                    elif (
                        index_dirsonly > index_filesonly and index_dirsonly > index_both
                    ):
                        what_flag = "dirsonly"
                        what = "dirs only"

                    if len(flag_list) > 0:
                        indented_message(
                            "WARNING: unknown password flags",
                            ",".join(flag_list),
                            3,
                        )
                repetitions = [
                    identifier_dict["identifier"]
                    for identifier_dict in Options.identifiers_and_passwords
                    if identifier_dict["identifier"] == identifier
                ]
                # everything beginning with the first non-space character after the case flag
                # till the end of line (including the traling spaces) is the pattern
                pattern = " ".join(remaining_columns[1:]).lstrip()
                if len(repetitions) == 0:
                    indented_message(
                        "WARNING: using an unknown password identifier",
                        identifier + ": not protecting the directory",
                        2,
                    )
                elif len(repetitions) == 1:
                    flags_string = case + ", " + whole + ", " + what
                    indented_message(
                        "file(s) protection requested",
                        "identifier: '"
                        + identifier
                        + "', pattern: '"
                        + pattern
                        + "', "
                        + flags_string,
                        4,
                    )
                    patterns_and_passwords.append(
                        {
                            "pattern": pattern,
                            "case_flag": case_flag,
                            "whole_flag": whole_flag,
                            "what_flag": what_flag,
                            "identifier": identifier,
                        }
                    )
                else:
                    indented_message(
                        "WARNING: password identifier used more than once",
                        identifier + ": not using it here",
                        2,
                    )
        return patterns_and_passwords

    @staticmethod
    def _get_matching_album_passwords(
        patterns_and_passwords, inherited_passwords_identifiers, dir_name
    ):
        password_identifiers_set = inherited_passwords_identifiers
        next_level()  # OK
        message("processing passwords for album...", "", 4)
        for pattern_and_password in patterns_and_passwords:
            if pattern_and_password["what_flag"] == "filesonly":
                message(
                    "password not added to album",
                    "flag 'filesonly' prevents applying this pattern to dir names",
                    3,
                )
            else:
                case = "case insentitive"
                whole = "part of name"
                if pattern_and_password["case_flag"] == "cs":
                    if pattern_and_password["whole_flag"] == "whole":
                        match = fnmatch.fnmatchcase(
                            dir_name, pattern_and_password["pattern"]
                        )
                        whole = "whole name"
                    else:
                        match = fnmatch.fnmatchcase(
                            dir_name, "*" + pattern_and_password["pattern"] + "*"
                        )
                    case = "case sentitive"
                else:
                    if pattern_and_password["whole_flag"] == "whole":
                        match = re.match(
                            fnmatch.translate(pattern_and_password["pattern"]),
                            dir_name,
                            re.IGNORECASE,
                        )
                        whole = "whole name"
                    else:
                        match = re.match(
                            fnmatch.translate(
                                "*" + pattern_and_password["pattern"] + "*"
                            ),
                            dir_name,
                            re.IGNORECASE,
                        )

                # add the matching patterns
                if match:
                    identifier = pattern_and_password["identifier"]
                    Options.mark_identifier_as_used(identifier)
                    if identifier not in password_identifiers_set:
                        password_identifiers_set.add(identifier)
                        message(
                            "password added to album",
                            "'"
                            + dir_name
                            + "' matches '"
                            + pattern_and_password["pattern"]
                            + "' "
                            + case
                            + ", "
                            + whole
                            + ", identifier = "
                            + identifier,
                            3,
                        )
                    else:
                        message(
                            "password not added to album",
                            dir_name
                            + "' matches '"
                            + pattern_and_password["pattern"]
                            + "' "
                            + case
                            + ", "
                            + whole
                            + ", but identifier '"
                            + identifier
                            + "' already protects the album",
                            3,
                        )
        indented_message("passwords for album processed!", "", 5)
        back_level()  # OK
        return password_identifiers_set

    @staticmethod
    def is_symlink_inside_albums(symlink_path):
        symlink_target = os.path.realpath(symlink_path)
        albums_path = os.path.realpath(Options.config["album_path"])
        return os.path.commonpath([symlink_target, albums_path]) == albums_path

    # This functions is called recursively
    # it works on a directory and produces the album for the directory
    def walk(
        self,
        absolute_path,
        album_cache_base,
        patterns_and_passwords,
        inherited_passwords_mtime,
        inherited_passwords_identifiers,
        parent_album=None,
    ):
        provisional_json_file = None
        path = remove_album_path(absolute_path)
        splitted_path = path.split("/")
        parent_paths = [
            "/".join(splitted_path[:length])
            for length in list(range(len(splitted_path) + 1))
        ]
        exiting_directory_message = "<<<<<<<<<<<  Exiting directory"
        patterns_and_passwords = copy.deepcopy(patterns_and_passwords)
        inherited_passwords_identifiers = copy.deepcopy(inherited_passwords_identifiers)
        inherited_passwords_mtime = copy.deepcopy(inherited_passwords_mtime)
        passwords_or_album_ini_processed = False
        max_file_date = file_mtime(absolute_path)
        message(">>>>>>>>>>>  Entering directory", absolute_path, 1)
        next_level()  # OK
        message("cache base", album_cache_base, 4)
        if not os.access(absolute_path, os.R_OK | os.X_OK):
            message("access denied to directory", os.path.basename(absolute_path), 1)
            back_level()  # OK
            message(exiting_directory_message, absolute_path, 1)
            return [None, None]
        listdir = os.listdir(absolute_path)
        if Options.config["exclude_tree_marker"] in listdir:
            message(
                "excluded with subfolders by marker file",
                Options.config["exclude_tree_marker"],
                4,
            )
            back_level()  # OK
            message(exiting_directory_message, absolute_path, 1)
            return [None, None]
        skip_files = False
        if Options.config["exclude_files_marker"] in listdir:
            message(
                "files excluded by marker file",
                Options.config["exclude_files_marker"],
                4,
            )
            skip_files = True

        ############################################################
        # look for password marker and manage it
        ############################################################
        must_process_passwords = False
        passwords_marker_file = os.path.join(
            absolute_path, Options.config["passwords_marker"]
        )
        passwords_marker_file_mtime = inherited_passwords_mtime
        if (
            len(Options.identifiers_and_passwords)
            and Options.config["passwords_marker"] in listdir
        ):
            next_level()  # OK
            message(
                "passwords marker found",
                "'" + Options.config["passwords_marker"] + "', reading it",
                4,
            )
            if not os.access(passwords_marker_file, os.R_OK):
                indented_message(
                    "FATAL ERROR: unreadable file,  quitting", passwords_marker_file, 1
                )
                sys.exit(1)

            patterns_and_passwords_length = len(patterns_and_passwords)
            with open(passwords_marker_file, "r") as passwords_file:
                patterns_and_passwords = self._parse_password_marker(
                    passwords_file, patterns_and_passwords
                )
            if patterns_and_passwords_length != len(patterns_and_passwords):
                new_passwords_marker_mtime = file_mtime(passwords_marker_file)
                if passwords_marker_file_mtime is None:
                    passwords_marker_file_mtime = new_passwords_marker_mtime
                else:
                    passwords_marker_file_mtime = max(
                        passwords_marker_file_mtime, new_passwords_marker_mtime
                    )
            back_level()  # OK

        ############################################################
        # look for album.ini file in order to check json file validity against it
        ############################################################
        album_ini_file = os.path.join(
            absolute_path, Options.config["metadata_filename"]
        )
        album_ini_good = False

        must_process_album_ini = False
        if os.path.exists(album_ini_file):
            if not os.access(album_ini_file, os.R_OK):
                message("album.ini file unreadable", "", 2)
            elif os.path.getsize(album_ini_file) == 0:
                message("album.ini file has zero length", "", 2)
            else:
                album_ini_good = True

        ############################################################
        # look for album json file(s) and check its/their validity
        ############################################################
        (
            json_file_list,
            mtimes,
            using_provisional_json_file,
        ) = json_files_and_mtimes(album_cache_base)
        # if using_provisional_json_file:
        #     print(str(json_file_list))

        json_files_timestamp = None
        album_from_json_files = None
        album_cache_hit = False
        json_files_mtime = None

        if len(mtimes) > 1:
            pprint(
                [
                    mtime.strftime(Options.timestamp_format_for_file_name)
                    for mtime in mtimes
                ]
            )
            message(
                "not an album cache hit",
                "json files recreated because they had different timestamps: "
                + ", ".join(
                    sorted(
                        list(
                            map(
                                lambda mtime: mtime.strftime(
                                    Options.timestamp_format_for_file_name
                                ),
                                mtimes,
                            )
                        )
                    )
                ),
                3,
            )
        elif len(json_file_list) == 0:
            message("not an album cache hit", "json files not found in cache dir", 3)
        elif Options.regenerate_all_json_files:
            indented_message(
                "not an album cache hit", "regenerating all json files (debug mode)", 4
            )
        elif len(json_file_list) and len(mtimes) == 1:
            json_files_mtime = mtimes.pop()

            if (
                len(Options.config["provisional_date_strings"]) == 1
                and Options.config["provisional_date_strings"][0] is None
            ):
                indented_message(
                    "not an album cache hit",
                    "no options file exists",
                    4,
                )
            elif (
                json_files_mtime.strftime(Options.timestamp_format_for_file_name)
                not in Options.config["provisional_date_strings"]
            ):
                indented_message(
                    "not an album cache hit",
                    "no options file exists with a timestamp compatible with json file(s) timestamp",
                    4,
                )
                next_level()
                indented_message(
                    "json file(s) timestamp",
                    json_files_mtime.strftime(Options.timestamp_format_for_file_name),
                    5,
                )
                n = 1
                for options_timestamp in Options.config["provisional_date_strings"]:
                    indented_message(
                        "options file n. " + str(n) + " timestamp",
                        options_timestamp,
                        5,
                    )
                    n += 1
                back_level()
            elif album_ini_good and json_files_mtime <= file_mtime(album_ini_file):
                # a check on album_ini_file content would have been good:
                # execution comes here even if album.ini hasn't anything significant
                message(
                    "not an album cache hit",
                    "album.ini newer than json file(s)",
                    4,
                )
            elif json_files_mtime <= file_mtime(absolute_path):
                indented_message(
                    "not an album cache hit", "album dir newer than json file(s)", 4
                )
            else:
                json_files_timestamp = json_files_mtime.strftime(
                    # json_files_timestamp = file_mtime(json_file_list[0]).strftime(
                    Options.timestamp_format_for_file_name
                )
                # the json files should be good, the album will be retrieved from them

                if json_files_timestamp not in Options.update:
                    message(
                        "not an album cache hit",
                        "forced json file recreation because json file(s) timestamp doesn't correspond with options file(s) timestamp",
                        3,
                    )
                elif Options.update[json_files_timestamp]["json_files"]:
                    message(
                        "not an album cache hit",
                        "forced json file recreation because some option change requires that",
                        3,
                    )
                else:
                    files = "'" + "', '".join(json_file_list) + "'"

                    message(
                        "maybe an album cache hit",
                        "trying to import album from " + files,
                        5,
                    )

                    obsolete_json_version = False
                    [
                        album_from_json_files,
                        must_process_passwords,
                    ] = Album.get_media_from_json_files(
                        json_file_list,
                        json_files_mtime,
                        patterns_and_passwords,
                    )

                    if album_from_json_files is None:
                        additional_message = ""
                        if obsolete_json_version:
                            additional_message = (
                                " because of obsolete json_version value"
                            )
                            # Options.reset_obsolete_json_version_flag()

                        indented_message(
                            "not an album cache hit",
                            "working with json files produced null album"
                            + additional_message,
                            4,
                        )
                    else:
                        # indented_message("json file imported", "", 5)

                        try:
                            options_timestamp = album_from_json_files.options_timestamp
                        except AttributeError:
                            options_timestamp = None

                        if not hasattr(album_from_json_files, "absolute_path"):
                            indented_message(
                                "not an album cache hit",
                                "cached album hasn't absolute_path",
                                4,
                            )
                            album_from_json_files = None
                        elif album_from_json_files.absolute_path != absolute_path:
                            indented_message(
                                "not an album cache hit",
                                "cached album's absolute_path != absolute_path",
                                4,
                            )
                            album_from_json_files = None
                        elif any(
                            not _single_media.is_valid
                            for _single_media in album_from_json_files.media_list
                        ):
                            message(
                                "not an album cache hit",
                                "some media in cached album isn't valid (duplicates?)",
                                3,
                            )
                        else:
                            indented_message("maybe an album cache hit...", "", 4)
                            album = album_from_json_files
                            album_cache_hit = True

        if not album_cache_hit:
            must_process_album_ini = True

        if (
            album_cache_hit
            and not os.path.exists(album_ini_file)
            and album.album_ini_mtime is not None
        ):
            # an album.ini was used in the last scanner run but now it's not there
            message(
                "not an album cache hit",
                "album.ini absent, but values from it are in json file",
                2,
            )
            album_cache_hit = False
            album_ini_good = False

        if (
            album_cache_hit
            and all(
                [
                    not os.path.exists(
                        os.path.join(
                            Options.config["album_path"],
                            parent_path,
                            Options.config["passwords_marker"],
                        )
                    )
                    for parent_path in parent_paths
                ]
            )
            and passwords_marker_file_mtime is not None
        ):
            # a password marker were used in the last scanner run but now it's not there
            message(
                "not an album cache hit",
                "password marker absent, but was used in the last scanner run",
                2,
            )
            album_cache_hit = False

        if not album_cache_hit:
            message("generating internal album...", "", 5)
            album = Album(absolute_path)
            indented_message("internal album generated", "", 5)

            if album_ini_good:
                if must_process_album_ini:
                    album.read_album_ini(album_ini_file)
                    # save the album.ini mtime: it help know whether the file has been removed
                    album.album_ini_mtime = file_mtime(album_ini_file)
                    indented_message("album.ini read!", "", 2)
                else:
                    message("album.ini values already in json file", "", 2)

        album.max_file_date = max_file_date
        if parent_album is not None:
            album.parent_cache_base = parent_album.cache_base
        album.cache_base = album_cache_base

        dir_name = os.path.basename(absolute_path)

        ############################################################
        # check passwords validity
        ############################################################
        if not must_process_passwords:
            if json_files_mtime is None:
                indented_message(
                    "passwords must be processed", "because json files don't exist", 4
                )
                must_process_passwords = True
            elif not album_cache_hit:
                indented_message(
                    "passwords must be processed", "because album isn't a cache hit", 4
                )
                must_process_passwords = True
            elif (
                options_timestamp in Options.password_change
                and Options.password_change[options_timestamp]
            ):
                indented_message(
                    "passwords must be processed",
                    "passwords have changed",
                    4,
                )
                must_process_passwords = True
            elif (
                len(patterns_and_passwords) > 0
                and passwords_marker_file_mtime is not None
                and json_files_mtime <= passwords_marker_file_mtime
            ):
                indented_message(
                    "passwords must be processed",
                    "password marker '"
                    + Options.config["passwords_marker"]
                    + "' newer than json file(s)",
                    4,
                )
                must_process_passwords = True

        # check album name against passwords
        if (
            must_process_passwords
            and album_cache_base != Options.config["folders_string"]
        ):
            # if passwords are to be processed, this invalidates the albums from cache
            album_cache_hit = False

            # restarting with the inherited passwords, get the album matching passwords
            album.password_identifiers_set = self._get_matching_album_passwords(
                patterns_and_passwords, inherited_passwords_identifiers, dir_name
            )
            for identifier in album.password_identifiers_set:
                Options.mark_identifier_as_used(identifier)
        else:
            indented_message(
                "no need to process passwords for album",
                "",
                5,
            )

        if album_cache_hit:
            # check each media
            message(
                "checking the cache files for all the cached media...",
                "",
                4,
            )
            next_level()
            if not all(
                cached_single_media.check_all_cache_files(json_files_mtime)
                for cached_single_media in album.media
            ):
                album_cache_hit = False
            back_level()
            indented_message(
                "cached media checked!",
                "OK" if album_cache_hit else "some cache file is not OK",
                5,
            )

        if album_cache_hit:
            indented_message(
                "finally: album cache hit!",
                "",
                4,
            )
        else:
            indented_message(
                "finally: not an album cache hit!",
                "",
                4,
            )

        # # reset the protected media counts
        # album.nums_protected_media_in_sub_tree = NumsProtected()
        # album.sizes_protected_media_in_sub_tree = SizesProtected()
        # album.sizes_protected_media_in_album = SizesProtected()
        #
        # if Options.config["expose_image_positions"]:
        #     album.nums_protected_media_in_sub_tree_non_geotagged = NumsProtected()
        #     album.sizes_protected_media_in_sub_tree_non_geotagged = SizesProtected()
        #     album.sizes_protected_media_in_album_non_geotagged = SizesProtected()

        message("reading directory", path, 5)
        message("subdir for cache files", album.subdir, 4)

        num_image_in_dir = 0
        num_image_processed_in_dir = 0
        num_image_with_exif_date_in_dir = 0
        num_image_with_geotags_in_dir = 0
        num_image_with_exif_date_and_geotags_in_dir = 0
        images_with_exif_date_and_without_geotags_in_dir = []
        images_without_exif_date_and_with_geotags_in_dir = []
        images_without_exif_date_or_geotags_in_dir = []

        num_audio_in_dir = 0
        num_audio_processed_in_dir = 0
        num_audio_with_date_from_metadata_or_file_name_in_dir = 0
        num_audio_with_geotags_in_dir = 0
        num_audio_with_date_from_metadata_or_file_name_and_geotags_in_dir = 0
        audios_with_exif_date_and_without_geotags_in_dir = []
        audios_without_exif_date_and_with_geotags_in_dir = []
        audios_without_exif_date_or_geotags_in_dir = []

        num_video_in_dir = 0
        num_video_processed_in_dir = 0
        num_video_with_date_from_metadata_or_file_name_in_dir = 0
        num_video_with_geotags_in_dir = 0
        num_video_with_date_from_metadata_or_file_name_and_geotags_in_dir = 0
        videos_with_exif_date_and_without_geotags_in_dir = []
        videos_without_exif_date_and_with_geotags_in_dir = []
        videos_without_exif_date_or_geotags_in_dir = []

        files_in_dir = []
        dirs_in_dir = []
        unrecognized_files_in_dir = []
        decompression_bomb_error_files_in_dir = []
        big_size_for_format_warnings_in_dir = []
        other_errors_in_dir = []
        for entry in self._listdir_sorted_alphabetically(absolute_path):
            try:
                entry = os.fsdecode(entry)
            except KeyboardInterrupt:
                raise
            except:
                indented_message("UNICODE ERROR", entry, 1)
                continue

            if entry[0] == "." or entry == Options.config["metadata_filename"]:
                # skip hidden files/directories and  user's metadata file 'album.ini'
                continue

            if Options.global_pattern != "" and re.search(
                Options.global_pattern, entry
            ):
                # skip excluded files/directories
                indented_message(
                    "skipping file/directory matching a pattern",
                    "'"
                    + entry
                    + "' matches global pattern '"
                    + Options.global_pattern
                    + "'",
                    3,
                )
                continue

            entry_with_path = os.path.join(absolute_path, entry)
            if not os.path.exists(entry_with_path):
                indented_message(
                    "nonexistent file/dir, perhaps a symlink to non-existing file/dir, skipping",
                    entry_with_path,
                    2,
                )
            elif not os.access(entry_with_path, os.R_OK):
                indented_message("unreadable file/dir", entry_with_path, 2)
            elif (
                os.path.islink(entry_with_path)
                and not Options.config["follow_symlinks"]
            ):
                # TO DO: this way file symlinks are skipped too: may be symlinks can be checked only for directories?
                indented_message(
                    "symlink, skipping it as per 'follow_symlinks' option",
                    entry,
                    3,
                )
            elif os.path.islink(entry_with_path) and not self.is_symlink_inside_albums(
                entry_with_path
            ):
                indented_message(
                    "symlink external to albums path",
                    "'" + entry + "', skipping for security reasons",
                    3,
                )
            elif os.path.isdir(entry_with_path):
                if os.path.islink(entry_with_path):
                    indented_message(
                        "symlink to dir",
                        "'" + entry + "', following it as per 'follow_symlinks' option",
                        3,
                    )

                # save the directory name for the end of the cycle
                dirs_in_dir.append(entry)
            elif os.path.isfile(entry_with_path):
                if skip_files:
                    continue
                if os.path.islink(entry_with_path):
                    indented_message(
                        "symlink to file",
                        "'" + entry + "', following it as per 'follow_symlinks' option",
                        3,
                    )

                # save the file name for the end of the cycle
                files_in_dir.append(entry)

        Options.all_albums.append(album)
        all_media_are_clean = True

        if album_cache_hit and using_provisional_json_file:
            message(
                "the provisional file was there",
                "no need to work with the files in this dir",
                3,
            )
            provisional_json_file = json_file_list[0]
        elif len(files_in_dir):
            provisional_json_file = album.json_file("") + ".media_only"

            message("working with files in dir", path, 3)
            next_level()  # OK
            for entry in files_in_dir:
                entry_with_path = os.path.join(absolute_path, entry)
                message("working with file", entry, 3)
                next_level()  # OK
                single_media_cache_hit = True
                dirname = os.path.dirname(entry_with_path)
                try:
                    mtime = file_mtime(entry_with_path)
                    file_size = os.path.getsize(entry_with_path)
                except OSError:
                    indented_message(
                        "could not read file mtime", "skipping this media", 5
                    )
                    back_level()  # OK
                    continue
                except:
                    single_media = None
                    cached_single_media = None

                if Options.config["checksum"]:
                    # checksum is needed for all the media, calculate it anyway
                    message("calculating checksum...", "", 5)
                    with open(entry_with_path, "rb") as media_path_pointer:
                        media_checksum = checksum(media_path_pointer)
                    indented_message("checksum calculated!", "", 5)

                if not album_cache_hit:
                    message(
                        "not a single media cache hit",
                        "because the json file(s) wasn't/weren't a cache hit",
                        5,
                    )
                    single_media_cache_hit = False
                else:
                    message("getting single media from cached album...", "", 5)
                    cached_single_media = album.single_media_from_path(entry_with_path)
                    if cached_single_media is not None:
                        indented_message("single media got from cached album!", "", 5)
                    else:
                        if album_cache_hit:
                            # when the album is a cache hit, this single media could be a bad one
                            # supposedly it was there when the last scanner run
                            # just go the the next media
                            indented_message(
                                "not a single media cache hit",
                                "this media wasn't in the cached album, and the album is a cache hit",
                                5,
                            )
                            single_media_cache_hit = False
                        else:
                            indented_message(
                                "not a single media cache hit",
                                "no such media in cached album",
                                5,
                            )
                            single_media_cache_hit = False

                    if (
                        single_media_cache_hit
                        and cached_single_media.datetime_file_modified != mtime
                    ):
                        message(
                            "not a single media cache hit",
                            "(TreeWalker) modification time different: "
                            + str(mtime)
                            + " (on disk) != "
                            + str(cached_single_media.datetime_file_modified)
                            + " (in json file)",
                            5,
                        )
                        single_media_cache_hit = False

                    # check the size of the original image in the albums
                    if (
                        single_media_cache_hit
                        and Options.config["expose_original_media"]
                        and original_media_is_enough(cached_single_media)
                        and cached_single_media.file_size != file_size
                    ):
                        indented_message(
                            "not a single media cache hit",
                            "(TreeWalker) original media has file size different: "
                            + str(file_size)
                            + " (on disk) != "
                            + str(cached_single_media.file_size)
                            + " (in json file: fileSize property)",
                            5,
                        )
                        single_media_cache_hit = False

                    if single_media_cache_hit and Options.config["checksum"]:
                        try:
                            if cached_single_media.checksum == media_checksum:
                                indented_message("checksum OK!", "", 5)
                            else:
                                indented_message(
                                    "not a single media cache hit", "bad checksum", 5
                                )
                                single_media_cache_hit = False
                        except KeyError:
                            message(
                                "not a single media cache hit",
                                "no checksum in json file",
                                5,
                            )
                            single_media_cache_hit = False

                if single_media_cache_hit:
                    single_media = cached_single_media
                    message(
                        "single media cache hit!",
                        "",
                        4,
                    )
                else:
                    # not a single media cache hit
                    message("processing single media from file", "", 5)
                    single_media = SingleMedia(
                        album,
                        entry_with_path,
                        json_files_mtime,
                        patterns_and_passwords,
                        single_media_cache_hit,
                        must_process_passwords,
                    )

                    if single_media.is_valid:
                        single_media.clean = False
                        all_media_are_clean = False
                        message("single media dirty!", "not an album cache hit", 5)
                        if Options.config["checksum"]:
                            single_media._attributes["checksum"] = media_checksum

                try:
                    for error in single_media.errors["big_size_for_format_warnings"]:
                        big_size_for_format_warnings_in_dir.append(
                            "'" + entry_with_path + "': " + error
                        )
                except KeyError:
                    pass

                if single_media.is_valid:
                    # if not single_media_cache_hit or must_process_passwords:
                    #     [
                    #         single_media.password_identifiers_set,
                    #         single_media.album_identifiers_set,
                    #     ] = album.get_matching_media_passwords(
                    #         patterns_and_passwords, entry_with_path
                    #     )
                    #     for identifier in single_media.password_identifiers_set:
                    #         Options.mark_identifier_as_used(identifier)
                    #     for identifier in single_media.album_identifiers_set:
                    #         Options.mark_identifier_as_used(identifier)
                    # else:
                    #     indented_message(
                    #         "no need to process passwords for media", "", 5
                    #     )
                    #
                    if not single_media_cache_hit:
                        if single_media.is_image:
                            num_image_processed_in_dir += 1
                        elif single_media.is_audio:
                            num_audio_processed_in_dir += 1
                        elif single_media.is_video:
                            num_video_processed_in_dir += 1
                        album.media_list = [
                            _media
                            for _media in album.media_list
                            if single_media.media_file_name != _media.media_file_name
                        ]

                    if not any(
                        single_media.media_file_name == _media.media_file_name
                        for _media in album.media_list
                    ):
                        album.add_single_media(single_media)
                    else:
                        indented_message(
                            "single media not added to album...",
                            "it's a cache hit and it was already there",
                            5,
                        )
                else:
                    # here: single_media.is_valid == False
                    if Options.is_decompression_bomb_error:
                        indented_message(
                            "PIL: decompr. bomb error",
                            "big image: '" + entry_with_path + "'",
                            3,
                        )
                        decompression_bomb_error_files_in_dir.append(
                            "'"
                            + entry_with_path
                            + "',  mime type: "
                            + single_media.mime_type
                        )
                    else:
                        indented_message(
                            "not image nor audio nor video", entry_with_path, 3
                        )
                        unrecognized_files_in_dir.append(
                            "'"
                            + entry_with_path
                            + "', mime type: "
                            + single_media.mime_type
                        )

                    try:
                        for error in single_media.errors["other_errors"]:
                            other_errors_in_dir.append(
                                "'" + entry_with_path + "': " + error
                            )
                    except:
                        pass

                back_level()  # OK
            back_level()  # OK

            # end processing the files (media)

        for single_media in album.media_list:
            message("processing single media", single_media.name, 4)

            single_media.generate_types_and_names()

            single_media.fill_file_sizes_structure()

            self.all_media.append(single_media)

            if Options.config["expose_image_dates"]:
                self.add_single_media_to_by_date_albums(single_media)

            if Options.config["expose_image_positions"] and single_media.has_gps_data:
                self.add_single_media_to_by_geonames_albums(single_media)

            self.add_single_media_to_by_search_albums(single_media)

            album.update_media_counts(single_media)

            if single_media.is_image:
                num_image_in_dir += 1

                if (
                    Options.config["expose_image_dates"]
                    and single_media.has_date_from_metadata_or_file_name
                ):
                    num_image_with_exif_date_in_dir += 1
                if (
                    Options.config["expose_image_positions"]
                    and single_media.has_gps_data
                ):
                    num_image_with_geotags_in_dir += 1

                if (
                    Options.config["expose_image_positions"]
                    and Options.config["expose_image_dates"]
                ):
                    if single_media.has_date_from_metadata_or_file_name:
                        if single_media.has_gps_data:
                            num_image_with_exif_date_and_geotags_in_dir += 1
                        else:
                            images_with_exif_date_and_without_geotags_in_dir.append(
                                single_media.media_path
                            )
                    else:
                        if single_media.has_gps_data:
                            images_without_exif_date_and_with_geotags_in_dir.append(
                                single_media.media_path
                            )
                        else:
                            images_without_exif_date_or_geotags_in_dir.append(
                                single_media.media_path
                            )
            elif single_media.is_audio:
                num_audio_in_dir += 1

                if (
                    Options.config["expose_image_dates"]
                    and single_media.has_date_from_metadata_or_file_name
                ):
                    num_audio_with_date_from_metadata_or_file_name_in_dir += 1
                if (
                    Options.config["expose_image_positions"]
                    and single_media.has_gps_data
                ):
                    num_audio_with_geotags_in_dir += 1

                if (
                    Options.config["expose_image_positions"]
                    and Options.config["expose_image_dates"]
                ):
                    if single_media.has_date_from_metadata_or_file_name:
                        if single_media.has_gps_data:
                            num_audio_with_date_from_metadata_or_file_name_and_geotags_in_dir += (
                                1
                            )
                        else:
                            audios_with_exif_date_and_without_geotags_in_dir.append(
                                single_media.media_path
                            )
                    else:
                        if single_media.has_gps_data:
                            audios_without_exif_date_and_with_geotags_in_dir.append(
                                single_media.media_path
                            )
                        else:
                            audios_without_exif_date_or_geotags_in_dir.append(
                                single_media.media_path
                            )
            elif single_media.is_video:
                num_video_in_dir += 1

                if (
                    Options.config["expose_image_dates"]
                    and single_media.has_date_from_metadata_or_file_name
                ):
                    num_video_with_date_from_metadata_or_file_name_in_dir += 1
                if (
                    Options.config["expose_image_positions"]
                    and single_media.has_gps_data
                ):
                    num_video_with_geotags_in_dir += 1

                if (
                    Options.config["expose_image_positions"]
                    and Options.config["expose_image_dates"]
                ):
                    if single_media.has_date_from_metadata_or_file_name:
                        if single_media.has_gps_data:
                            num_video_with_date_from_metadata_or_file_name_and_geotags_in_dir += (
                                1
                            )
                        else:
                            videos_with_exif_date_and_without_geotags_in_dir.append(
                                single_media.media_path
                            )
                    else:
                        if single_media.has_gps_data:
                            videos_without_exif_date_and_with_geotags_in_dir.append(
                                single_media.media_path
                            )
                        else:
                            videos_without_exif_date_or_geotags_in_dir.append(
                                single_media.media_path
                            )

        if not using_provisional_json_file and len(files_in_dir):
            # save the provisional album, possibly with only the media
            # it's saved in order not to reprocess the media if already processed
            # it will be deleted after processing the subalbums
            if (
                not album.empty
                and provisional_json_file is not None
                and len(dirs_in_dir)
            ):
                is_provisional_media = True
                self.save_album(
                    album,
                    ",",
                    is_provisional_media,
                )

        Options.num_image += num_image_in_dir
        Options.num_audio += num_audio_in_dir
        Options.num_video += num_video_in_dir
        Options.num_image_processed += num_image_processed_in_dir
        Options.num_audio_processed += num_audio_processed_in_dir
        Options.num_video_processed += num_video_processed_in_dir

        report_times()
        report_mem()

        # now process the subdirectories (subalbums)
        if len(dirs_in_dir):
            message("working with directories in dir", path, 3)
            next_level()  # OK
            for entry in dirs_in_dir:
                entry_with_path = os.path.join(absolute_path, entry)

                trimmed_path = trim_base_custom(
                    absolute_path, Options.config["album_path"]
                )
                entry_for_cache_base = os.path.join(
                    Options.config["folders_string"], trimmed_path, entry
                )
                next_album_cache_base = album.generate_cache_base(entry_for_cache_base)
                [
                    next_walked_album,
                    _passwords_or_album_ini_processed,
                ] = self.walk(
                    entry_with_path,
                    next_album_cache_base,
                    patterns_and_passwords,
                    passwords_marker_file_mtime,
                    album.password_identifiers_set,
                    album,
                )
                passwords_or_album_ini_processed = (
                    passwords_or_album_ini_processed
                    or _passwords_or_album_ini_processed
                )
                if next_walked_album is not None:
                    album.max_file_date = max(
                        album.max_file_date, next_walked_album.max_file_date
                    )
                    album.nums_protected_media_in_sub_tree.merge(
                        next_walked_album.nums_protected_media_in_sub_tree
                    )
                    album.sizes_protected_media_in_sub_tree.merge(
                        next_walked_album.sizes_protected_media_in_sub_tree
                    )
                    album.nums_media_in_sub_tree.sum(
                        next_walked_album.nums_media_in_sub_tree
                    )

                    if Options.config["expose_image_positions"]:
                        album.nums_protected_media_in_sub_tree_non_geotagged.merge(
                            next_walked_album.nums_protected_media_in_sub_tree_non_geotagged
                        )
                        album.sizes_protected_media_in_sub_tree_non_geotagged.merge(
                            next_walked_album.sizes_protected_media_in_sub_tree_non_geotagged
                        )
                        album.nums_media_in_sub_tree_non_geotagged.sum(
                            next_walked_album.nums_media_in_sub_tree_non_geotagged
                        )

                    if Options.config["expose_image_positions"]:
                        album.positions_and_media_in_tree.merge(
                            next_walked_album.positions_and_media_in_tree
                        )

                    album.add_subalbum(next_walked_album)
            back_level()  # OK
        # end processing the subdirectories (subalbums)

        if (
            not album_cache_hit
            or not all_media_are_clean
            or any(not subalbum.clean for subalbum in album.subalbums)
        ):
            message("album dirty!", album.cache_base, 5)
            album.clean = False
        else:
            message("album clean!", album.cache_base, 5)
            album.clean = True

        if album.nums_media_in_sub_tree.total() == 0:
            back_level()  # OK
            return [None, None]

        self.add_album_to_by_search_albums(album)

        album.sort_subalbums_and_media(True)

        # separate protected and unprotected content:
        # actually a dictionary `protected_albums` is added to the album, its keys are
        # - "," for unprotected content
        # - album_identifiers_combination + "," + media_identifiers_combination for protected content
        # every subalbum already has this dictionary added, so the function doesn't need to act recursively
        album.split_protected_and_unprotected_content()

        if provisional_json_file is not None:
            provisional_json_file_with_path = os.path.join(
                Options.config["cache_path"],
                provisional_json_file,
            )
            unlink_file(provisional_json_file_with_path)

        self.save_protected_and_unprotected_albums(album)

        max_digit = len(str(Options.config["num_media_in_tree"]))

        if num_image_in_dir:
            Options.num_image_with_exif_date += num_image_with_exif_date_in_dir
            Options.num_image_with_geotags += num_image_with_geotags_in_dir
            Options.num_image_with_exif_date_and_geotags += (
                num_image_with_exif_date_and_geotags_in_dir
            )

            Options.num_image_with_exif_date_and_without_geotags += len(
                images_with_exif_date_and_without_geotags_in_dir
            )

            _lpadded_num_image_in_dir = str(num_image_in_dir).rjust(max_digit)
            _rpadded_num_image_in_dir = str(num_image_in_dir).ljust(max_digit)
            _all_images_in_path = (
                _lpadded_num_image_in_dir
                + "/"
                + _rpadded_num_image_in_dir
                + " images in '"
                + absolute_path
                + "'"
                + ":"
            )
            _some_image_in_path = (
                "/" + _rpadded_num_image_in_dir + " images in '" + absolute_path + "':"
            )

            if num_image_in_dir == len(
                images_with_exif_date_and_without_geotags_in_dir
            ):
                Options.images_with_exif_date_and_without_geotags.append(
                    {
                        "intro": _all_images_in_path,
                        "list": images_with_exif_date_and_without_geotags_in_dir,
                    }
                )
            elif len(images_with_exif_date_and_without_geotags_in_dir):
                Options.images_with_exif_date_and_without_geotags.append(
                    {
                        "intro": str(
                            len(images_with_exif_date_and_without_geotags_in_dir)
                        ).rjust(max_digit)
                        + _some_image_in_path,
                        "list": images_with_exif_date_and_without_geotags_in_dir,
                    }
                )

            Options.num_image_without_exif_date_and_with_geotags += len(
                images_without_exif_date_and_with_geotags_in_dir
            )
            if num_image_in_dir == len(
                images_without_exif_date_and_with_geotags_in_dir
            ):
                Options.images_without_exif_date_and_with_geotags.append(
                    {
                        "intro": _all_images_in_path,
                        "list": images_without_exif_date_and_with_geotags_in_dir,
                    }
                )
            elif len(images_without_exif_date_and_with_geotags_in_dir):
                Options.images_without_exif_date_and_with_geotags.append(
                    {
                        "intro": str(
                            len(images_without_exif_date_and_with_geotags_in_dir)
                        ).rjust(max_digit)
                        + _some_image_in_path,
                        "list": images_without_exif_date_and_with_geotags_in_dir,
                    }
                )

            Options.num_image_without_exif_date_or_geotags += len(
                images_without_exif_date_or_geotags_in_dir
            )
            if num_image_in_dir == len(images_without_exif_date_or_geotags_in_dir):
                Options.images_without_exif_date_or_geotags.append(
                    {
                        "intro": _all_images_in_path,
                        "list": images_without_exif_date_or_geotags_in_dir,
                    }
                )
            elif len(images_without_exif_date_or_geotags_in_dir):
                Options.images_without_exif_date_or_geotags.append(
                    {
                        "intro": str(
                            len(images_without_exif_date_or_geotags_in_dir)
                        ).rjust(max_digit)
                        + _some_image_in_path,
                        "list": images_without_exif_date_or_geotags_in_dir,
                    }
                )

        if num_audio_in_dir:
            Options.num_audio_with_date_from_metadata_or_file_name += (
                num_audio_with_date_from_metadata_or_file_name_in_dir
            )
            Options.num_audio_with_geotags += num_audio_with_geotags_in_dir
            Options.num_audio_with_date_from_metadata_or_file_name_and_geotags += (
                num_audio_with_date_from_metadata_or_file_name_and_geotags_in_dir
            )

            Options.num_audio_with_date_from_metadata_or_file_name_and_without_geotags += len(
                audios_with_exif_date_and_without_geotags_in_dir
            )

            _lpadded_num_audio_in_dir = str(num_audio_in_dir).rjust(max_digit)
            _rpadded_num_audio_in_dir = str(num_audio_in_dir).ljust(max_digit)
            _all_audios_in_path = (
                _lpadded_num_audio_in_dir
                + "/"
                + _rpadded_num_audio_in_dir
                + " audios in "
                + absolute_path
                + ":"
            )
            _some_audio_in_path = (
                "/" + _rpadded_num_audio_in_dir + " audios in " + absolute_path + ":"
            )

            if num_audio_in_dir == len(
                audios_with_exif_date_and_without_geotags_in_dir
            ):
                Options.audios_with_exif_date_and_without_geotags.append(
                    {
                        "intro": _all_audios_in_path,
                        "list": audios_with_exif_date_and_without_geotags_in_dir,
                    }
                )
            elif len(audios_with_exif_date_and_without_geotags_in_dir):
                Options.audios_with_exif_date_and_without_geotags.append(
                    {
                        "intro": str(
                            len(audios_with_exif_date_and_without_geotags_in_dir)
                        ).rjust(max_digit)
                        + _some_audio_in_path,
                        "list": audios_with_exif_date_and_without_geotags_in_dir,
                    }
                )

            Options.num_audio_without_date_from_metadata_or_file_name_and_with_geotags += len(
                audios_without_exif_date_and_with_geotags_in_dir
            )
            if num_audio_in_dir == len(
                audios_without_exif_date_and_with_geotags_in_dir
            ):
                Options.audios_without_exif_date_and_with_geotags.append(
                    {
                        "intro": _all_audios_in_path,
                        "list": audios_without_exif_date_and_with_geotags_in_dir,
                    }
                )
            elif len(audios_without_exif_date_and_with_geotags_in_dir):
                Options.audios_without_exif_date_and_with_geotags.append(
                    {
                        "intro": str(
                            len(audios_without_exif_date_and_with_geotags_in_dir)
                        ).rjust(max_digit)
                        + _some_audio_in_path,
                        "list": audios_without_exif_date_and_with_geotags_in_dir,
                    }
                )

            Options.num_audio_without_date_from_metadata_or_file_name_or_geotags += len(
                audios_without_exif_date_or_geotags_in_dir
            )
            if num_audio_in_dir == len(audios_without_exif_date_or_geotags_in_dir):
                Options.audios_without_exif_date_or_geotags.append(
                    {
                        "intro": _all_audios_in_path,
                        "list": audios_without_exif_date_or_geotags_in_dir,
                    }
                )
            elif len(audios_without_exif_date_or_geotags_in_dir):
                Options.audios_without_exif_date_or_geotags.append(
                    {
                        "intro": str(
                            len(audios_without_exif_date_or_geotags_in_dir)
                        ).rjust(max_digit)
                        + _some_audio_in_path,
                        "list": audios_without_exif_date_or_geotags_in_dir,
                    }
                )

        if num_video_in_dir:
            Options.num_video_with_date_from_metadata_or_file_name += (
                num_video_with_date_from_metadata_or_file_name_in_dir
            )
            Options.num_video_with_geotags += num_video_with_geotags_in_dir
            Options.num_video_with_date_from_metadata_or_file_name_and_geotags += (
                num_video_with_date_from_metadata_or_file_name_and_geotags_in_dir
            )

            Options.num_video_with_date_from_metadata_or_file_name_and_without_geotags += len(
                videos_with_exif_date_and_without_geotags_in_dir
            )

            _lpadded_num_video_in_dir = str(num_video_in_dir).rjust(max_digit)
            _rpadded_num_video_in_dir = str(num_video_in_dir).ljust(max_digit)
            _all_videos_in_path = (
                _lpadded_num_video_in_dir
                + "/"
                + _rpadded_num_video_in_dir
                + " videos in "
                + absolute_path
                + ":"
            )
            _some_video_in_path = (
                "/" + _rpadded_num_video_in_dir + " videos in " + absolute_path + ":"
            )

            if num_video_in_dir == len(
                videos_with_exif_date_and_without_geotags_in_dir
            ):
                Options.videos_with_exif_date_and_without_geotags.append(
                    {
                        "intro": _all_videos_in_path,
                        "list": videos_with_exif_date_and_without_geotags_in_dir,
                    }
                )
            elif len(videos_with_exif_date_and_without_geotags_in_dir):
                Options.videos_with_exif_date_and_without_geotags.append(
                    {
                        "intro": str(
                            len(videos_with_exif_date_and_without_geotags_in_dir)
                        ).rjust(max_digit)
                        + _some_video_in_path,
                        "list": videos_with_exif_date_and_without_geotags_in_dir,
                    }
                )

            Options.num_video_without_date_from_metadata_or_file_name_and_with_geotags += len(
                videos_without_exif_date_and_with_geotags_in_dir
            )
            if num_video_in_dir == len(
                videos_without_exif_date_and_with_geotags_in_dir
            ):
                Options.videos_without_exif_date_and_with_geotags.append(
                    {
                        "intro": _all_videos_in_path,
                        "list": videos_without_exif_date_and_with_geotags_in_dir,
                    }
                )
            elif len(videos_without_exif_date_and_with_geotags_in_dir):
                Options.videos_without_exif_date_and_with_geotags.append(
                    {
                        "intro": str(
                            len(videos_without_exif_date_and_with_geotags_in_dir)
                        ).rjust(max_digit)
                        + _some_video_in_path,
                        "list": videos_without_exif_date_and_with_geotags_in_dir,
                    }
                )

            Options.num_video_without_date_from_metadata_or_file_name_or_geotags += len(
                videos_without_exif_date_or_geotags_in_dir
            )
            if num_video_in_dir == len(videos_without_exif_date_or_geotags_in_dir):
                Options.videos_without_exif_date_or_geotags.append(
                    {
                        "intro": _all_videos_in_path,
                        "list": videos_without_exif_date_or_geotags_in_dir,
                    }
                )
            elif len(videos_without_exif_date_or_geotags_in_dir):
                Options.videos_without_exif_date_or_geotags.append(
                    {
                        "intro": str(
                            len(videos_without_exif_date_or_geotags_in_dir)
                        ).rjust(max_digit)
                        + _some_video_in_path,
                        "list": videos_without_exif_date_or_geotags_in_dir,
                    }
                )

        if len(unrecognized_files_in_dir):
            Options.num_unrecognized_files += len(unrecognized_files_in_dir)
            Options.unrecognized_files.append(
                {
                    "intro": str(len(unrecognized_files_in_dir)).rjust(max_digit)
                    + " files in "
                    + "'"
                    + absolute_path
                    + "'"
                    + ":",
                    "list": unrecognized_files_in_dir,
                }
            )
            Options.config["num_media_in_tree"] -= len(unrecognized_files_in_dir)

        if len(decompression_bomb_error_files_in_dir):
            Options.num_decompression_bomb_error_files += len(
                decompression_bomb_error_files_in_dir
            )
            Options.decompression_bomb_error_files.append(
                {
                    "intro": str(len(decompression_bomb_error_files_in_dir)).rjust(
                        max_digit
                    )
                    + " files in "
                    + "'"
                    + absolute_path
                    + "'"
                    + ":",
                    "list": decompression_bomb_error_files_in_dir,
                }
            )
            Options.config["num_media_in_tree"] -= len(
                decompression_bomb_error_files_in_dir
            )

        if len(big_size_for_format_warnings_in_dir):
            Options.num_big_size_for_format_warnings += len(
                big_size_for_format_warnings_in_dir
            )
            Options.big_size_for_format_warnings.append(
                {
                    "intro": str(len(big_size_for_format_warnings_in_dir)).rjust(
                        max_digit
                    )
                    + " files in "
                    + "'"
                    + absolute_path
                    + "'"
                    + ":",
                    "list": big_size_for_format_warnings_in_dir,
                }
            )
            Options.config["num_media_in_tree"] -= len(
                big_size_for_format_warnings_in_dir
            )

        if len(other_errors_in_dir):
            Options.num_other_errors += len(other_errors_in_dir)
            Options.other_errors.append(
                {
                    "intro": str(len(other_errors_in_dir)).rjust(max_digit)
                    + " files in "
                    + "'"
                    + absolute_path
                    + "'"
                    + ":",
                    "list": other_errors_in_dir,
                }
            )
            Options.config["num_media_in_tree"] -= len(other_errors_in_dir)

        if Options.config["expose_image_dates"]:
            album.exif_date_max = album.album_exif_date_max()
            album.exif_date_min = album.album_exif_date_min()
        if Options.config["expose_original_media"]:
            album.file_date_max = album.album_file_date_max()
            album.file_date_min = album.album_file_date_min()
        album.pixel_size_max = album.album_pixels_max()
        album.pixel_size_min = album.album_pixels_min()

        back_level()  # OK

        message(exiting_directory_message, absolute_path, 1)

        report_times()

        passwords_or_album_ini_processed = (
            passwords_or_album_ini_processed
            or must_process_passwords
            or must_process_album_ini
        )
        return [album, passwords_or_album_ini_processed]

    def generate_composite_image_and_save(
        self,
        album,
        complex_identifiers_combination,
        is_provisional_media,
        touch_only,
    ):
        if album.max_file_date is not None:
            # generate the album composite image for sharing
            # actually is generated only if there is unprotected content
            # anyway, the random media is choosen
            self.generate_composite_image_and_choose_random_media(
                album, complex_identifiers_combination
            )
            if album.composite_image_size is None:
                del album.composite_image_size

        self.save_album(
            album,
            complex_identifiers_combination,
            is_provisional_media,
            touch_only,
        )

    def save_protected_and_unprotected_albums(self, album):
        touch_only = False
        if album.clean:
            self.check_unprotected_json_files(album)
            self.generate_composite_image_and_choose_random_media(
                album.protected_albums[","], ","
            )
            # json files for a clean album must be touched
            touch_only = True
        # save the album: the unprotected one...

        is_provisional_media = False
        self.generate_composite_image_and_save(
            album.protected_albums[","],
            ",",
            is_provisional_media,
            touch_only,
        )

        # remove (or simply check, if the album is clean) the old protected json files for this album
        for (
            complex_identifiers_combination
        ) in album.nums_protected_media_in_sub_tree.used_identifier_combinations():
            if album.clean:
                self.check_protected_json_files(album, complex_identifiers_combination)
            # generate the structure that holds the numbers to be added to the file name
            [md5_product_list, _] = md5_products(complex_identifiers_combination)
            for md5_combination in md5_product_list:
                album.last_file_number[md5_combination] = 0

        for (
            complex_identifiers_combination
        ) in album.nums_protected_media_in_sub_tree.used_identifier_combinations():
            touch_only = False
            if album.clean:
                self.generate_composite_image_and_choose_random_media(
                    album.protected_albums[complex_identifiers_combination],
                    complex_identifiers_combination,
                )
                # json files for a clean album must be touched
                touch_only = True
            is_provisional_media = False
            self.generate_composite_image_and_save(
                album.protected_albums[complex_identifiers_combination],
                complex_identifiers_combination,
                is_provisional_media,
                touch_only,
            )

    @staticmethod
    def _index_to_coords(
        index, tile_width, px_between_tiles, side_off_set, linear_number_of_tiles
    ):
        x = side_off_set + (index % linear_number_of_tiles) * (
            tile_width + px_between_tiles
        )
        y = side_off_set + int(index / linear_number_of_tiles) * (
            tile_width + px_between_tiles
        )
        return [x, y]

    def pick_image_according_to_random_number(self, album, random_number):
        if random_number < len(album.media_list):
            return [album.media_list[random_number], random_number]
        else:
            random_number -= len(album.media_list)
            for subalbum in album.subalbums_list:
                if random_number < subalbum.nums_media_in_sub_tree.total():
                    [
                        picked_image,
                        random_number,
                    ] = self.pick_image_according_to_random_number(
                        subalbum, random_number
                    )
                    if picked_image:
                        return [picked_image, random_number]
                random_number -= subalbum.nums_media_in_sub_tree.total()
        # if execution arrives here an error has occurred
        return [None, random_number]

    def _build_and_save_composite_image(
        self, random_thumbnails, max_thumbnail_number, composite_image_path
    ):
        # generate the composite image
        # following code inspired from
        # https://stackoverflow.com/questions/30429383/combine-16-images-into-1-big-image-with-php#30429557
        # thanks to Adarsh Vardhan who wrote it!

        message("generating jpg composite image...", "", 5)
        tile_width = Options.config["album_thumb_size"]

        # INIT BASE IMAGE FILLED WITH BACKGROUND COLOR
        linear_number_of_tiles = int(math.sqrt(max_thumbnail_number))
        px_between_tiles = 1
        side_off_set = 1

        composite_image_size = (
            side_off_set
            + (tile_width + px_between_tiles) * linear_number_of_tiles
            - px_between_tiles
            + side_off_set
        )
        img = Image.new("RGB", (composite_image_size, composite_image_size), "white")

        # PUT SRC IMAGES ON BASE IMAGE
        index = -1
        logo_already_resized = False
        audio_clipart_already_resized = False
        for thumbnail in random_thumbnails:
            index += 1
            with Image.open(thumbnail) as tile:
                tile.load()
            tile_img_width = tile.size[0]
            tile_img_height = tile.size[1]
            # the logo has size 1024x1024: reduce it
            if tile_img_width == 1024:
                if not logo_already_resized:
                    logo = tile.resize(
                        (
                            Options.config["album_thumb_size"],
                            Options.config["album_thumb_size"],
                        )
                    )
                    logo_already_resized = True
                tile = logo
            # the audio clilpart has size 500x500: reduce it
            if tile_img_width == 500:
                if not audio_clipart_already_resized:
                    audio = tile.resize(
                        (
                            Options.config["album_thumb_size"],
                            Options.config["album_thumb_size"],
                        )
                    )
                    audio_clipart_already_resized = True
                tile = audio
            [x, y] = self._index_to_coords(
                index,
                tile_width,
                px_between_tiles,
                side_off_set,
                linear_number_of_tiles,
            )
            if tile_img_width < tile_width:
                x += int(float(tile_width - tile_img_width) / 2)
            if tile_img_height < tile_width:
                y += int(float(tile_width - tile_img_height) / 2)
            img.paste(tile, (x, y))

        message("saving and touching jpg composite image...", "", 5)
        img.save(composite_image_path, quality=Options.config["jpeg_quality"])
        touch_with_datetime(composite_image_path)
        message("jpg composite image saved and touched", "", 5)

        set_permissions_according_to_umask(composite_image_path)
        # touch_with_datetime(composite_image_path)
        indented_message("jpg composite image generated!", "", 6)

        return img.size[0]

    def generate_composite_image_and_choose_random_media(
        self, album, complex_identifiers_combination
    ):
        # picks a maximum of `max_composite_image_thumbnails_number` random images in album and subalbums
        # and generates a square composite image

        # returns the size of the image and a random media which will be put (actually its cache base) in the subalbums' json files

        # the album parameter may be the protected or the unprotected content of album

        first_random_media = None
        logo = os.path.join(
            os.path.dirname(os.path.dirname(__file__)), "web/img/myphotoshareLogo.jpg"
        )
        audio = os.path.join(
            os.path.dirname(os.path.dirname(__file__)), "web/img/audio.png"
        )
        random_thumbnails = []

        num_media = album.nums_media_in_sub_tree.total()

        next_level()  # OK
        message("Working with composite image...", "", 4)
        composite_image_size = None

        # always use jpg format for to the composite image
        composite_image_name = album.cache_base + ".jpg"
        composite_image_name_with_subdir = os.path.join(
            Options.config["composite_images_subdir"],
            composite_image_name,
        )
        composite_image_path = os.path.join(
            Options.config["cache_path"],
            composite_image_name_with_subdir,
        )
        if complex_identifiers_combination == ",":
            self.all_composite_images.append(composite_image_name_with_subdir)
        json_file_with_path = os.path.join(
            Options.config["cache_path"], album.json_file("")
        )

        composite_image_ok = False

        if (
            os.path.exists(composite_image_path)
            and (
                album.max_file_date is None
                or file_mtime(composite_image_path) > album.max_file_date
            )
            and os.path.exists(json_file_with_path)
            and file_mtime(json_file_with_path) < file_mtime(composite_image_path)
        ):
            next_level()  # OK
            message("composite image OK, touching it...", "", 5)
            touch_with_datetime(composite_image_path)
            indented_message(
                "composite image touched!", composite_image_name_with_subdir, 4
            )
            back_level()  # OK
            try:
                with Image.open(composite_image_path) as im:
                    im.load()
                composite_image_size = im.size[0]
                composite_image_ok = True
            except UnidentifiedImageError:
                # this error happens when a previous run has been interrupted while saving the composite image
                pass
            except OSError:
                # maybe same cause as the former exception
                pass

        if num_media == 0:
            max_thumbnail_number = 1
            random_thumbnails.append(logo)
        else:
            # this code is for composite image, which isn't generated when there isn't any unprotected content
            if (
                num_media < 4
                or Options.config["max_composite_image_thumbnails_number"] == 1
            ):
                max_thumbnail_number = 1
            elif (
                num_media < 9
                or Options.config["max_composite_image_thumbnails_number"] == 4
            ):
                max_thumbnail_number = 4
            elif (
                num_media < 16
                or Options.config["max_composite_image_thumbnails_number"] == 9
            ):
                max_thumbnail_number = 9
            elif (
                num_media < 25
                or Options.config["max_composite_image_thumbnails_number"] == 16
            ):
                max_thumbnail_number = 16
            elif (
                num_media < 36
                or Options.config["max_composite_image_thumbnails_number"] == 25
            ):
                max_thumbnail_number = 25
            else:
                max_thumbnail_number = Options.config[
                    "max_composite_image_thumbnails_number"
                ]

            # pick max_thumbnail_number random square album thumbnails
            random_list = []
            bad_list = []
            good_media_number = num_media
            num_random_thumbnails = min(max_thumbnail_number, good_media_number)
            i = 0
            while True:
                if i >= good_media_number:
                    break
                while True:
                    random_number = random.randint(0, good_media_number - 1)
                    if (
                        random_number
                        not in random_list
                        # and random_number not in bad_list
                    ):
                        break
                random_list.append(random_number)
                [
                    random_media,
                    _,
                ] = self.pick_image_according_to_random_number(album, random_number)
                if i == 0:
                    first_random_media = random_media
                if num_media == 0 or composite_image_ok:
                    # the composite image is already there or must not be generated
                    break

                album_prefix = remove_folders_marker(random_media.album.cache_base)
                if album_prefix:
                    album_prefix += Options.config["cache_folder_separator"]
                # always use jpg format for building the composite image
                if random_media.is_audio:
                    thumbnail = audio
                else:

                    cache_subdir = random_media.album.subdir
                    if random_media.first_protected_directory is not None:
                        cache_subdir = os.path.join(
                            random_media.first_protected_directory, cache_subdir
                        )

                    thumbnail = os.path.join(
                        Options.config["cache_path"],
                        cache_subdir,
                        album_prefix
                        + image_cache_name(
                            random_media,
                            Options.config["album_thumb_size"],
                            "jpg",
                            "album_square",
                        ),
                    )

                if os.path.exists(thumbnail):
                    random_thumbnails.append(thumbnail)
                    i += 1
                    if i == num_random_thumbnails:
                        break
                else:
                    message(
                        "nonexistent thumbnail",
                        thumbnail
                        + " - i="
                        + str(i)
                        + ", good="
                        + str(good_media_number),
                        5,
                    )
                    bad_list.append(thumbnail)
                    good_media_number -= 1

            if not composite_image_ok:
                # this code is for composite image, which isn't generated when there isn't any unprotected content
                if len(random_thumbnails) == 0:
                    # missing images: use the myphotoshare logo
                    random_thumbnails.append(logo)
                if len(random_thumbnails) < max_thumbnail_number:
                    # reduce the number of used images, so that the logo isn't used
                    max_thumbnail_number = int(math.log(len(random_thumbnails), 2))
                    random_thumbnails = random_thumbnails[:max_thumbnail_number]

        if not composite_image_ok:
            composite_image_size = self._build_and_save_composite_image(
                random_thumbnails, max_thumbnail_number, composite_image_path
            )

        back_level()  # OK
        album.composite_image_size = (composite_image_size,)
        album.random_media = first_random_media

    @staticmethod
    def _save_json_options(num):
        json_options_file = (
            os.path.join(Options.config["cache_path"], "options.json")
            + "."
            + Options.config["options_timestamp"]
        )
        message("saving json options file...", json_options_file, 4)
        # some option must not be saved
        options_to_save = {}
        for key, value in list(Options.config.items()):
            if key not in Options.options_not_to_be_saved:
                options_to_save[key] = value
        # add the total count of positions, so that reading another json file is not needed in order to know if gps data exist
        options_to_save["num_positions_in_tree"] = num
        options_to_save["version"] = Options.version
        options_to_save["json_version"] = Options.json_version

        with open(json_options_file, "w") as option_file:
            json.dump(options_to_save, option_file)
        indented_message("json options file saved!", "", 5)
        # touch the options file with the proper timestamp
        touch_with_datetime(json_options_file)

    def create_keys_for_directories(self, splitted_file_name, dict):
        if len(splitted_file_name) == 0:
            return

        if "files" not in dict:
            dict["files"] = list()
        if "dirs" not in dict:
            dict["dirs"] = {}

        if len(splitted_file_name) == 1:
            dict["files"].append(splitted_file_name[0])
        else:
            if splitted_file_name[0] not in dict["dirs"]:
                dict["dirs"][splitted_file_name[0]] = {}
            self.create_keys_for_directories(
                splitted_file_name[1:], dict["dirs"][splitted_file_name[0]]
            )
        return dict

    def build_good_file_structure(self):
        message("building cache file list...", "", 5)
        dirs_and_files_dict = {}
        for file_name in self.all_json_files:
            splitted_file_name = file_name.split("/")
            dirs_and_files_dict = self.create_keys_for_directories(
                splitted_file_name, dirs_and_files_dict
            )

        for file_name in self.all_composite_images:
            splitted_file_name = file_name.split("/")
            dirs_and_files_dict = self.create_keys_for_directories(
                splitted_file_name, dirs_and_files_dict
            )

        for single_media in self.all_media:
            for file_name in single_media.cache_files_for_single_media:
                splitted_file_name = file_name.split("/")
                dirs_and_files_dict = self.create_keys_for_directories(
                    splitted_file_name, dirs_and_files_dict
                )
        indented_message("cache file list built!", "", 6)
        # pprint(dirs_and_files_dict)
        return dirs_and_files_dict

    def clean_up(self):
        message("cleaning up...", "", 3)
        next_level()  # OK
        dirs_and_files_dict = self.build_good_file_structure()
        # pprint(dirs_and_files_dict)

        self.remove_stale("", dirs_and_files_dict)
        indented_message("cleaned up!", "", 5)

        back_level()

    def remove_stale(self, subdir, dirs_and_files_dict):
        if subdir == "":
            info = "in cache path"
        else:
            info = "in subdir " + subdir

        # next_level()  # OK
        message("searching for stale cache files", info, 4)

        dir_with_path = os.path.join(Options.config["cache_path"], subdir)
        files_and_dirs = list(sorted(os.listdir(dir_with_path)))
        files = [
            entry
            for entry in files_and_dirs
            if not os.path.isdir(os.path.join(dir_with_path, entry))
        ]
        dirs = [
            entry
            for entry in files_and_dirs
            if os.path.isdir(os.path.join(dir_with_path, entry))
        ]
        for cache_file in files:
            # only delete json's, transcoded videos, reduced images and thumbnails
            next_level()  # OK
            cache_file = os.fsdecode(cache_file)

            if cache_file not in dirs_and_files_dict["files"]:
                message("removing stale cache file...", "", 6)
                file_to_delete = os.path.join(
                    Options.config["cache_path"], subdir, cache_file
                )
                unlink_file(file_to_delete)
                indented_message("stale cache file removed", cache_file, 5)
            back_level()  # OK
        for cache_dir in dirs:
            cache_dir = os.fsdecode(cache_dir)
            if (
                subdir == ""
                and cache_dir == Options.config["composite_images_subdir_js"]
            ):
                # do not enter the directory where selection is saved
                # back_level()  # OK
                continue

            next_level()  # OK
            if (
                "dirs" in dirs_and_files_dict
                and cache_dir in dirs_and_files_dict["dirs"]
            ):
                self.remove_stale(
                    os.path.join(subdir, cache_dir),
                    dirs_and_files_dict["dirs"][cache_dir],
                )
                if not os.listdir(
                    os.path.join(Options.config["cache_path"], subdir, cache_dir)
                ):
                    next_level()  # OK
                    message("empty subdir, deleting it...", "", 6)
                    dir_to_delete = os.path.join(
                        Options.config["cache_path"], subdir, cache_dir
                    )
                    os.rmdir(dir_to_delete)
                    indented_message("empty subdir deleted!", subdir, 5)
                    back_level()  # OK
            else:
                # a directory which isn't reported must be deleted with all its content
                deletable_dir = os.path.join(
                    Options.config["cache_path"], subdir, cache_dir
                )
                message(
                    "deleting non-reported subdir...",
                    "",
                    6,
                )
                try:
                    shutil.rmtree(deletable_dir)
                    indented_message(
                        "non-reported subdir deleted!",
                        deletable_dir,
                        5,
                    )
                except FileNotFoundError:
                    pass
            back_level()  # OK
        # back_level()  # OK
