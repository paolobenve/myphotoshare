# -*- coding: utf-8 -*-


# gps code got from https://gist.github.com/erans/983821

import copy
import hashlib
import json
import locale
import os
import os.path
import re
import sys
import tempfile
from datetime import datetime

import humanize
import magic

try:
    import cv2
except:
    pass

import configparser
from configparser import NoOptionError

from pprint import pprint

import math
import fnmatch
import numpy as np

from CachePath import remove_album_path, remove_folders_marker, trim_base_custom
from CachePath import (
    image_cache_name,
    original_media_is_enough,
    may_make_image_os_copy,
    # must_make_converted_fullsize_copy,
    cache_image_formats_ending_with_jpg,
)
from CachePath import (
    audio_transcoded_name,
    video_transcoded_name,
    audio_cache_copy_name,
    video_cache_copy_name,
    audio_full_size_transcoded_name,
    video_full_size_transcoded_name,
)
from CachePath import (
    transliterate_to_ascii,
    remove_accents,
)
from CachePath import (
    remove_all_but_alphanumeric_chars_dashes_slashes,
    switch_to_lowercase,
)
from Utilities import (
    message,
    indented_message,
    next_level,
    back_level,
)
from Utilities import (
    file_mtime,
    unlink_file,
    make_dir,
    touch_with_datetime,
    set_permissions_according_to_umask,
)
from Utilities import (
    merge_media,
    calculate_media_file_name,
)
from Utilities import (
    convert_identifiers_set_to_md5s_set,
    create_complex_dir_and_subdirs,
    md5_products,
    convert_identifiers_set_to_codes_set,
    convert_old_codes_set_to_identifiers_set,
)
from Utilities import (
    convert_combination_to_set,
    convert_set_to_combination,
    complex_combination,
)
from Geonames import Geonames
from PIL import Image, ImageOps, UnidentifiedImageError

from PIL.ExifTags import TAGS, GPSTAGS
from PIL.TiffImagePlugin import IFDRational, Fraction

from VideoToolWrapper import VideoProbeCaller, VideoTranscodeCaller

import Options


if Options.config["avif_supported"]:
    import pillow_avif

if Options.config["jxl_supported"]:
    import pillow_jxl

if Options.config["heic_supported"]:
    from pillow_heif import register_heif_opener

    register_heif_opener()

# WARNING: pyexiftool has been modified, do not overwrite with new versions unless you know what you are doing
import PyExifTool
from PyExifTool.exceptions import ExifToolExecuteError

import shutil

try:
    locale.setlocale(locale.LC_ALL, "")
except locale.Error:
    message("FATAL ERROR", "the default locale is unset on the system, quitting", 0)
    sys.exit(1)


class Album(object):
    def __init__(self, path):
        if path is None:
            return

        if path[-1:] == "/":
            path = path[0:-1]
        self.absolute_path = path
        self.baseless_path = remove_album_path(path)
        self._name = os.path.basename(os.path.normpath(self.baseless_path))
        self.cache_base = ""
        self.media_list = []
        self.subalbums_list = []
        self._subdir = ""
        self.nums_media_in_sub_tree = ImagesAudiosVideos()
        self.sizes_of_sub_tree = Sizes()
        self.sizes_of_album = Sizes()
        if Options.config["expose_image_positions"]:
            self.nums_media_in_sub_tree_non_geotagged = ImagesAudiosVideos()
            self.sizes_of_sub_tree_non_geotagged = Sizes()
            self.sizes_of_album_non_geotagged = Sizes()
        self.complex_combination = ","
        self.nums_protected_media_in_sub_tree = NumsProtected()
        self.sizes_protected_media_in_sub_tree = SizesProtected()
        self.sizes_protected_media_in_album = SizesProtected()
        if Options.config["expose_image_positions"]:
            self.nums_protected_media_in_sub_tree_non_geotagged = NumsProtected()
            self.sizes_protected_media_in_sub_tree_non_geotagged = SizesProtected()
            self.sizes_protected_media_in_album_non_geotagged = SizesProtected()
            self.positions_and_media_in_tree = Positions(None)
        self.parent_cache_base = None
        self.album_ini = None
        self._attributes = {}
        self._attributes["metadata"] = {}
        self.json_version = -1
        self.password_identifiers_set = set()
        self.passwords_marker_mtime = None
        self.album_ini_mtime = None
        self.max_file_date = None
        self.clean = False
        self.last_file_number = {}
        self.center = {}
        if Options.config["expose_image_dates"]:
            self.exif_date_max = None
            self.exif_date_min = None
        if Options.config["expose_original_media"]:
            self.file_date_max = None
            self.file_date_min = None
        self.pixel_size_max = 0
        self.pixel_size_min = 0

        if Options.config["subdir_method"] in ("md5", "folder") and (
            self.baseless_path.find(Options.config["by_date_string"]) != 0
            or self.baseless_path.find(Options.config["by_gps_string"]) != 0
            or self.baseless_path.find(Options.config["by_search_string"]) != 0
        ):
            if Options.config["subdir_method"] == "md5":
                if Options.config["cache_folders_num_digits_array"] == []:
                    self._subdir = Options.config["default_cache_album"]
                else:
                    hash = hashlib.md5(os.fsencode(path)).hexdigest()
                    self._subdir = Options.config["default_cache_album"]
                    previous_digits = 0
                    for digits in Options.config["cache_folders_num_digits_array"]:
                        self._subdir = os.path.join(
                            self._subdir,
                            hash[previous_digits : previous_digits + digits],
                        )
                        previous_digits = digits
            elif Options.config["subdir_method"] == "folder":
                if path.find("/") == -1:
                    self._subdir = "__"
                else:
                    self._subdir = path[path.rfind("/") + 1 :][:2].replace(" ", "_")
                    if len(self._subdir) == 1:
                        self._subdir += "_"

    @property
    def attributes(self):
        return self._attributes

    @property
    def name(self):
        if hasattr(self, "alt_name"):
            return self.alt_name
        else:
            return self._name

    @property
    def title(self):
        if "metadata" in self._attributes and "title" in self._attributes["metadata"]:
            return self._attributes["metadata"]["title"]
        else:
            return ""

    @property
    def description(self):
        if (
            "metadata" in self._attributes
            and "description" in self._attributes["metadata"]
        ):
            return self._attributes["metadata"]["description"]
        else:
            return ""

    @property
    def tags(self):
        if "metadata" in self._attributes and "tags" in self._attributes["metadata"]:
            return self._attributes["metadata"]["tags"]
        else:
            return ""

    @property
    def media(self):
        return self.media_list

    @property
    def subalbums(self):
        return self.subalbums_list

    @property
    def path(self):
        return self.baseless_path

    def __str__(self):
        if hasattr(self, "_name"):
            return self._name
        else:
            return self.path

    def json_file(self, media_or_positions, number=None):
        json_file = self.cache_base
        if number is not None:
            json_file += "." + str(number)
        if media_or_positions != "":
            json_file += "." + media_or_positions
        json_file += ".json"

        if json_file.find(Options.config["by_date_string"]) == 0:
            # add the by date subdirectory
            json_file = os.path.join(Options.config["by_date_album_subdir"], json_file)
        elif json_file.find(Options.config["by_gps_string"]) == 0:
            # add the by gps subdirectory
            json_file = os.path.join(Options.config["by_gps_album_subdir"], json_file)
        elif json_file.find(Options.config["by_search_string"]) == 0:
            # add the search subdirectory
            json_file = os.path.join(
                Options.config["by_search_album_subdir"], json_file
            )
        else:
            # add the regular albums subdirectory
            json_file = os.path.join(Options.config["regular_album_subdir"], json_file)

        return json_file

    @property
    def subdir(self):
        return self._subdir

    # @property
    def album_exif_date_max(self):
        exif_dates = [
            subalbum.exif_date_max
            for subalbum in self.subalbums_list
            if subalbum.exif_date_max is not None
        ]
        exif_dates.extend([single_media.exif_date for single_media in self.media_list])
        if len(exif_dates) == 0:
            return None
        else:
            return max(exif_dates)

    def album_exif_date_min(self):
        exif_dates = [
            subalbum.exif_date_min
            for subalbum in self.subalbums_list
            if subalbum.exif_date_max is not None
        ]
        exif_dates.extend([single_media.exif_date for single_media in self.media_list])
        if len(exif_dates) == 0:
            return None
        else:
            return min(exif_dates)

    # @property
    def album_file_date_max(self):
        file_dates = [
            subalbum.file_date_max
            for subalbum in self.subalbums_list
            if subalbum.exif_date_max is not None
        ]
        file_dates.extend([single_media.file_date for single_media in self.media_list])
        if len(file_dates) == 0:
            return None
        else:
            return max(file_dates)

    # @property
    def album_file_date_min(self):
        file_dates = [
            subalbum.file_date_min
            for subalbum in self.subalbums_list
            if subalbum.exif_date_max is not None
        ]
        file_dates.extend([single_media.file_date for single_media in self.media_list])
        if len(file_dates) == 0:
            return None
        else:
            return min(file_dates)

    # @property
    def album_pixels_max(self):
        pixel_data = [subalbum.pixel_size_max for subalbum in self.subalbums_list]
        pixel_data.extend(
            [
                max(single_media.size)
                for single_media in self.media_list
                if not single_media.is_audio
            ]
        )
        if len(pixel_data) == 0:
            return 0
        else:
            return max(pixel_data)

    # @property
    def album_pixels_min(self):
        pixel_data = [subalbum.pixel_size_min for subalbum in self.subalbums_list]
        pixel_data.extend(
            [
                min(single_media.size)
                for single_media in self.media_list
                if not single_media.is_audio
            ]
        )
        if len(pixel_data) == 0:
            return 0
        else:
            return min(pixel_data)

    @property
    def exif_date_max_string(self):
        date_str = str(self.exif_date_max)
        while len(date_str) < 19:
            date_str = "0" + date_str
        return date_str

    @property
    def exif_date_min_string(self):
        date_str = str(self.exif_date_min)
        while len(date_str) < 19:
            date_str = "0" + date_str
        return date_str

    @property
    def file_date_max_string(self):
        date_str = str(self.file_date_max)
        while len(date_str) < 19:
            date_str = "0" + date_str
        return date_str

    @property
    def file_date_min_string(self):
        date_str = str(self.file_date_min)
        while len(date_str) < 19:
            date_str = "0" + date_str
        return date_str

    def __eq__(self, other):
        return self.path == other.path

    # def __ne__(self, other):
    # 	return not self.__eq__(other)

    def __lt__(self, other):
        try:
            if (
                Options.force_sort_name_ascending
                or Options.config["default_album_name_sort"]
                or self.exif_date_max == other.exif_date_max
            ):
                if (
                    Options.force_sort_name_ascending
                    or not Options.config["default_album_reverse_sort"]
                ):
                    return self.name < other.name
                else:
                    return self.name > other.name
            else:
                if not Options.config["default_album_reverse_sort"]:
                    return self.exif_date_max < other.exif_date_max
                else:
                    return self.exif_date_max > other.exif_date_max
            # return self.exif_date_max < other.exif_date_max
        except TypeError:
            return False

    def read_album_ini(self, file_name):
        """
        Read the 'album.ini' file in the directory 'self.absolute_path' to
        get user defined metadata for the album and pictures.
        """
        self.album_ini = configparser.ConfigParser(allow_no_value=True)
        message("reading album.ini...", file_name, 5)
        self.album_ini.read(file_name)
        indented_message("album.ini read", "", 5)

        next_level()  # OK
        message("adding album.ini metadata values to album...", "", 5)
        Metadata.set_metadata_from_album_ini("album", self)
        indented_message("metadata values from album.ini added to album...", "", 5)
        back_level()  # OK

    def is_already_in_tree_by_search(self, tree_by_search_word):
        albums_list = tree_by_search_word["albums_list"]
        return (
            len([album for album in albums_list if self.cache_base == album.cache_base])
            == 1
        )

    def get_matching_media_passwords(self, patterns_and_passwords, entry_with_path):
        next_level()  # OK
        message("processing passwords for single media...", "", 4)
        password_identifiers_set = set()
        album_identifiers_set = set()
        file_name = os.path.basename(entry_with_path)

        # apply the album password to the media
        for album_identifier in sorted(self.password_identifiers_set):
            if album_identifier not in album_identifiers_set:
                album_identifiers_set.add(album_identifier)
                password_md5 = convert_identifiers_set_to_md5s_set(
                    set([album_identifier])
                ).pop()
                message(
                    "album password added to media",
                    "identifier = "
                    + album_identifier
                    + ", encrypted password = "
                    + password_md5,
                    3,
                )
            else:
                message(
                    "album password not added to media",
                    "identifier '" + album_identifier + "' already protects this media",
                    3,
                )

        # apply the password to the media if it matches the media name
        for pattern_and_password in patterns_and_passwords:
            if pattern_and_password["what_flag"] == "dirsonly":
                message(
                    "password not added to media",
                    "flag 'dirsonly' prevents applying this pattern to file names",
                    3,
                )
            else:
                case = "case insentitive"
                whole = "part of name"
                if pattern_and_password["case_flag"] == "cs":
                    if pattern_and_password["whole_flag"] == "whole":
                        match = fnmatch.fnmatchcase(
                            file_name, pattern_and_password["pattern"]
                        )
                        whole = "whole name"
                    else:
                        match = fnmatch.fnmatchcase(
                            file_name,
                            "*" + pattern_and_password["pattern"] + "*",
                        )
                    case = "case sentitive"
                else:
                    if pattern_and_password["whole_flag"] == "whole":
                        match = re.match(
                            fnmatch.translate(pattern_and_password["pattern"]),
                            file_name,
                            re.IGNORECASE,
                        )
                        whole = "whole name"
                    else:
                        match = re.match(
                            fnmatch.translate(
                                "*" + pattern_and_password["pattern"] + "*"
                            ),
                            file_name,
                            re.IGNORECASE,
                        )

                if match:
                    identifier = pattern_and_password["identifier"]
                    Options.mark_identifier_as_used(identifier)
                    if identifier not in password_identifiers_set:
                        password_identifiers_set.add(identifier)
                        message(
                            "password and code added to media",
                            "'"
                            + file_name
                            + "' matches '"
                            + pattern_and_password["pattern"]
                            + "' "
                            + case
                            + ", "
                            + whole
                            + ", identifier = "
                            + identifier,
                            4,
                        )
                    else:
                        message(
                            "password and code not added to media",
                            "'"
                            + file_name
                            + "' matches '"
                            + pattern_and_password["pattern"]
                            + "' "
                            + case
                            + ", "
                            + whole
                            + ", but identifier '"
                            + identifier
                            + "'  already protects the media",
                            3,
                        )
        indented_message("passwords for single media processed!", "", 5)
        back_level()  # OK
        return [
            password_identifiers_set,
            album_identifiers_set,
        ]

    def add_single_media(self, single_media):
        # before adding the media, remove any media with the same file name
        # it could be there because the album was a cache hit but the media wasn't
        # self.media_list = [
        #     _media
        #     for _media in self.media_list
        #     if single_media.media_file_name != _media.media_file_name
        # ]
        self.media_list.append(single_media)

    def add_single_media_complete(self, single_media):
        self.add_single_media(single_media)

        self.update_media_counts(single_media)

    def add_subalbum(self, album):
        self.subalbums_list.append(album)

    def update_media_counts(self, single_media):
        complex_identifiers_combination = complex_combination(
            convert_set_to_combination(single_media.album_identifiers_set),
            convert_set_to_combination(single_media.password_identifiers_set),
        )

        if single_media.is_image:
            self.nums_protected_media_in_sub_tree.increment_images(
                complex_identifiers_combination
            )
        elif single_media.is_audio:
            self.nums_protected_media_in_sub_tree.increment_audios(
                complex_identifiers_combination
            )
        elif single_media.is_video:
            self.nums_protected_media_in_sub_tree.increment_videos(
                complex_identifiers_combination
            )

        self.sizes_protected_media_in_sub_tree.sum(
            complex_identifiers_combination, single_media.file_sizes
        )
        self.sizes_protected_media_in_album.sum(
            complex_identifiers_combination, single_media.file_sizes
        )

        single_media_date = max(
            single_media.datetime_file_modified, single_media.datetime_dir
        )
        if self.max_file_date:
            self.max_file_date = max(self.max_file_date, single_media_date)
        else:
            self.max_file_date = single_media_date

        if Options.config["expose_image_positions"]:
            if single_media.has_gps_data:
                self.positions_and_media_in_tree.add_single_media(single_media)

                if self.center == {}:
                    self.center["latitude"] = single_media.latitude
                    self.center["longitude"] = single_media.longitude
                else:
                    self.center["latitude"] = Geonames.recalculate_mean(
                        self.center["latitude"],
                        len(self.media_list),
                        single_media.latitude,
                    )
                    self.center["longitude"] = Geonames.recalculate_mean(
                        self.center["longitude"],
                        len(self.media_list),
                        single_media.longitude,
                    )
            else:
                if single_media.is_image:
                    self.nums_media_in_sub_tree_non_geotagged.increment_images()
                    self.nums_protected_media_in_sub_tree_non_geotagged.increment_images(
                        complex_identifiers_combination
                    )
                elif single_media.is_audio:
                    self.nums_media_in_sub_tree_non_geotagged.increment_audios()
                    self.nums_protected_media_in_sub_tree_non_geotagged.increment_audios(
                        complex_identifiers_combination
                    )
                elif single_media.is_video:
                    self.nums_media_in_sub_tree_non_geotagged.increment_videos()
                    self.nums_protected_media_in_sub_tree_non_geotagged.increment_videos(
                        complex_identifiers_combination
                    )

                self.sizes_protected_media_in_sub_tree_non_geotagged.sum(
                    complex_identifiers_combination,
                    single_media.file_sizes,
                )
                self.sizes_protected_media_in_album_non_geotagged.sum(
                    complex_identifiers_combination,
                    single_media.file_sizes,
                )

        if single_media.is_image:
            self.nums_media_in_sub_tree.increment_images()
        elif single_media.is_audio:
            self.nums_media_in_sub_tree.increment_audios()
        elif single_media.is_video:
            self.nums_media_in_sub_tree.increment_videos()

    def sort_subalbums_and_media(self, force_sort_name_ascending=False):
        Options.force_sort_name_ascending = force_sort_name_ascending
        self.media_list.sort()
        self.subalbums_list.sort()

    @property
    def empty(self):
        if len(self.media_list) != 0:
            return False
        if len(self.subalbums_list) == 0:
            return True
        for subalbum in self.subalbums:
            if not subalbum.empty:
                return False
        return True

    def copy(self):
        album = Album(None)
        for key, value in list(self.__dict__.items()):
            if key == "subalbums_list":
                # subalbums must be new objects
                setattr(album, key, [subalbum.copy() for subalbum in value])
            elif isinstance(value, list):
                # media are the same object, but the list is a copy
                setattr(album, key, value[:])
                # album[key] = value[:]
            elif isinstance(value, Positions) or isinstance(value, NumsProtected):
                setattr(album, key, value.copy())
            elif isinstance(value, dict):
                setattr(album, key, copy.deepcopy(value))
            else:
                setattr(album, key, value)

        return album

    def extract_protected_content(self, complex_identifiers_combination):
        # returns a selection of subalbums, media matching the given complex_identifiers_combination
        # positions_and_media_in_tree structure is generated accordingly
        next_level()
        album = Album(self.absolute_path)
        album.cache_base = self.cache_base
        album.clean = self.clean
        album.nums_protected_media_in_sub_tree = self.nums_protected_media_in_sub_tree
        album.complex_combination = complex_identifiers_combination
        album.max_file_date = self.max_file_date
        if hasattr(self, "last_file_number"):
            album.last_file_number = self.last_file_number
        if hasattr(self, "center"):
            album.center = self.center
        if hasattr(self, "name"):
            album._name = self._name
        if hasattr(self, "alt_name"):
            album.alt_name = self.alt_name
        if hasattr(self, "ancestors_names"):
            album.ancestors_names = self.ancestors_names
        if hasattr(self, "ancestors_titles"):
            album.ancestors_titles = self.ancestors_titles
        if hasattr(self, "ancestors_centers"):
            album.ancestors_centers = self.ancestors_centers
        if hasattr(self, "parent_cache_base"):
            album.parent_cache_base = self.parent_cache_base
        if hasattr(self, "words"):
            album.words = self.words
        if hasattr(self, "unicode_words"):
            album.unicode_words = self.unicode_words
        if hasattr(self, "composite_image_size"):
            album.composite_image_size = self.composite_image_size
        if self.title:
            album._attributes["metadata"]["title"] = self.title
        if self.description:
            album._attributes["metadata"]["description"] = self.description
        if self.tags:
            album._attributes["metadata"]["tags"] = self.tags

        (
            album_identifiers_combination,
            media_identifiers_combination,
        ) = complex_identifiers_combination.split(",")
        album_identifiers_set = convert_combination_to_set(
            album_identifiers_combination
        )
        media_identifiers_set = convert_combination_to_set(
            media_identifiers_combination
        )

        album.subalbums_list = [
            subalbum.protected_albums[complex_identifiers_combination]
            for subalbum in self.subalbums_list
            if complex_identifiers_combination in subalbum.protected_albums
            and subalbum.nums_protected_media_in_sub_tree.total(
                complex_identifiers_combination
            )
        ]

        album.media_list = [
            single_media
            for single_media in self.media
            if single_media.password_identifiers_set == media_identifiers_set
            and single_media.album_identifiers_set == album_identifiers_set
        ]

        if Options.config["expose_image_positions"]:
            # generate positions
            album.positions_and_media_in_tree = Positions(None)

            if (
                not self.cache_base.startswith(Options.config["by_date_string"])
                and not self.cache_base.startswith(Options.config["by_gps_string"])
            ) or len(
                self.cache_base.split(Options.config["cache_folder_separator"])
            ) >= 4:
                for single_media in album.media_list:
                    if single_media.has_gps_data:
                        album.positions_and_media_in_tree.add_single_media(single_media)

            if self.cache_base == Options.config[
                "by_search_string"
            ] or not self.cache_base.startswith(Options.config["by_search_string"]):
                for subalbum in album.subalbums_list:
                    album.positions_and_media_in_tree.merge(
                        subalbum.positions_and_media_in_tree
                    )

        if complex_identifiers_combination in list(
            self.nums_protected_media_in_sub_tree.keys()
        ):
            album.nums_media_in_sub_tree = self.nums_protected_media_in_sub_tree.value(
                complex_identifiers_combination
            )
            album.sizes_of_sub_tree = self.sizes_protected_media_in_sub_tree.sizes(
                complex_identifiers_combination
            )
        else:
            album.nums_media_in_sub_tree = ImagesAudiosVideos()
            album.sizes_of_sub_tree = Sizes()
        if complex_identifiers_combination in list(
            self.sizes_protected_media_in_album.keys()
        ):
            album.sizes_of_album = self.sizes_protected_media_in_album.sizes(
                complex_identifiers_combination
            )
        else:
            self.sizes_of_album = Sizes()

        if Options.config["expose_image_positions"]:
            if complex_identifiers_combination in list(
                self.nums_protected_media_in_sub_tree_non_geotagged.keys()
            ):
                album.nums_media_in_sub_tree_non_geotagged = (
                    self.nums_protected_media_in_sub_tree_non_geotagged.value(
                        complex_identifiers_combination
                    )
                )
                album.sizes_of_sub_tree_non_geotagged = (
                    self.sizes_protected_media_in_sub_tree_non_geotagged.sizes(
                        complex_identifiers_combination
                    )
                )
            else:
                self.nums_media_in_sub_tree_non_geotagged = ImagesAudiosVideos()
                self.sizes_of_sub_tree_non_geotagged = Sizes()

            if complex_identifiers_combination in list(
                self.sizes_protected_media_in_album_non_geotagged.keys()
            ):
                album.sizes_of_album_non_geotagged = (
                    self.sizes_protected_media_in_album_non_geotagged.sizes(
                        complex_identifiers_combination
                    )
                )
            else:
                self.sizes_of_album_non_geotagged = Sizes()

        if Options.config["expose_image_dates"]:
            album.exif_date_max = self.album_exif_date_max()
            album.exif_date_min = self.album_exif_date_min()
        if Options.config["expose_original_media"]:
            album.file_date_max = self.album_file_date_max()
            album.file_date_min = self.album_file_date_min()
        album.pixel_size_max = self.album_pixels_max()
        album.pixel_size_min = self.album_pixels_min()

        back_level()
        return album

    def split_protected_and_unprotected_content(self):
        self.protected_albums = {}
        next_level()  # OK

        used_identifier_combinations = (
            self.nums_protected_media_in_sub_tree.used_identifier_combinations()
        )
        if len(used_identifier_combinations):
            message(
                "working with protected content...",
                self.name,
                4,
            )

            for complex_identifiers_combination in used_identifier_combinations:
                # (
                #     album_identifiers_combination,
                #     media_identifiers_combination,
                # ) = complex_identifiers_combination.split(",")
                indented_message(
                    "working with complex combination...",
                    complex_identifiers_combination,
                    4,
                )

                self.protected_albums[complex_identifiers_combination] = (
                    self.extract_protected_content(complex_identifiers_combination)
                )
        else:
            message(
                "no protected content to work for this album",
                self.name,
                3,
            )

        message(
            "working with unprotected content...",
            self.name,
            4,
        )
        self.protected_albums[","] = self.extract_protected_content(",")
        back_level()  # OK

    @property
    def must_separate_media(self):
        return (
            not Options.config["save_data"]
            and len(self.media) > Options.config["max_media_in_json_file"]
        )

    @property
    def must_separate_positions(self):
        count = self.positions_and_media_in_tree.count_media()
        return count > 0 and (
            Options.config["save_data"]
            or count > Options.config["max_media_from_positions_in_json_file"]
        )

    def to_json_files(
        self,
        json_name,
        positions_json_name,
        media_json_name,
        symlinks,
        positions_symlinks,
        media_symlinks,
        is_provisional_media,
        complex_identifiers_combination,
        touch_only,
    ):

        action_ing = "touching"
        action_ed = "touched"
        if not touch_only:
            action_ing = "saving and " + action_ing
            action_ed = "saved and " + action_ed

        save_begin = action_ing + " album..."
        save_end = "album " + action_ed + "!"
        save_positions_begin = action_ing + " positions album..."
        save_positions_end = "positions album " + action_ed + "!"
        save_media_begin = action_ing + " media album..."
        save_media_end = "media album " + action_ed + "!"
        if complex_identifiers_combination != ",":
            save_begin = action_ing + " protected album..."
            save_end = "protected album " + action_ed + "!"
            save_positions_begin = action_ing + " protected positions album..."
            save_positions_end = "protected positions album " + action_ed + "!"
            save_media_begin = action_ing + " protected media album..."
            save_media_end = "protected media album " + action_ed + "!"

        # media and positions: if few, they can be saved inside the normal json file
        # otherwise, save them in its own files

        separate_media = not is_provisional_media and self.must_separate_media
        separate_positions = (
            not is_provisional_media
            and Options.config["expose_image_positions"]
            and self.must_separate_positions
        )

        json_file_with_path = os.path.join(Options.config["cache_path"], json_name)
        if os.path.exists(json_file_with_path) and not os.access(
            json_file_with_path, os.W_OK
        ):
            message("FATAL ERROR", json_file_with_path + " not writable, quitting", 0)
            sys.exit(1)
        message(save_begin, json_name, 5)

        if not touch_only:
            with open(json_file_with_path, "w") as file:
                json.dump(
                    self,
                    file,
                    sep_pos=separate_positions,
                    sep_media=separate_media,
                    cls=PhotoAlbumEncoder,
                )

        # touch the file with the proper timestamp
        touch_with_datetime(json_file_with_path)
        message(save_end, "", 5)

        for symlink in symlinks:
            symlink_with_path = os.path.join(Options.config["cache_path"], symlink)
            message(save_begin + " (symlink)", symlink, 5)
            if not touch_only:
                make_dir(os.path.dirname(symlink_with_path), "symlink subdir")
                if os.path.exists(symlink_with_path):
                    os.unlink(symlink_with_path)
                os.symlink(json_file_with_path, symlink_with_path)
            # touch the synlink with the proper timestamp
            touch_with_datetime(symlink_with_path)
            message(save_end + " (symlink)", "", 5)

        indented_message(save_end, "", 4)

        if separate_positions:
            json_positions_file_with_path = os.path.join(
                Options.config["cache_path"], positions_json_name
            )
            if os.path.exists(json_positions_file_with_path) and not os.access(
                json_positions_file_with_path, os.W_OK
            ):
                message(
                    "FATAL ERROR",
                    json_positions_file_with_path + " not writable, quitting",
                    0,
                )
                sys.exit(1)
            message(save_positions_begin, positions_json_name, 5)
            if not touch_only:
                with open(json_positions_file_with_path, "w") as positions_file:
                    cache_base_root = self.cache_base.split(
                        Options.config["cache_folder_separator"]
                    )[0]
                    json.dump(
                        self.positions_and_media_in_tree,
                        positions_file,
                        type=cache_base_root,
                        cls=PhotoAlbumEncoder,
                    )

            # touch the positions file with the proper timestamp
            touch_with_datetime(json_positions_file_with_path)
            message(save_positions_end, "", 5)

            for symlink in positions_symlinks:
                symlink_with_path = os.path.join(Options.config["cache_path"], symlink)
                message(save_positions_begin + " (symlink)", symlink, 5)
                if not touch_only:
                    make_dir(os.path.dirname(symlink_with_path), "symlink subdir")
                    if os.path.exists(symlink_with_path):
                        os.unlink(symlink_with_path)
                    os.symlink(json_positions_file_with_path, symlink_with_path)
                touch_with_datetime(symlink_with_path)
                message(save_positions_end + " (symlink)", "", 5)

            indented_message(save_positions_end, "", 4)

        if separate_media and self.cache_base not in (
            Options.config["by_search_string"],
        ):
            json_media_file_with_path = os.path.join(
                Options.config["cache_path"], media_json_name
            )
            if os.path.exists(json_media_file_with_path) and not os.access(
                json_media_file_with_path, os.W_OK
            ):
                message(
                    "FATAL ERROR",
                    json_media_file_with_path + " not writable, quitting",
                    0,
                )
                sys.exit(1)
            message(save_media_begin, media_json_name, 5)
            if not touch_only:
                with open(json_media_file_with_path, "w") as media_file:
                    cache_base_root = self.cache_base.split(
                        Options.config["cache_folder_separator"]
                    )[0]
                    json.dump(
                        self.media,
                        media_file,
                        type=cache_base_root,
                        cls=PhotoAlbumEncoder,
                    )

            # touch the media file with the proper timestamp
            touch_with_datetime(json_media_file_with_path)
            indented_message(save_media_end, "", 5)

            for symlink in media_symlinks:
                next_level()
                symlink_with_path = os.path.join(Options.config["cache_path"], symlink)
                message(save_media_begin + " (symlink)", symlink, 5)
                if not touch_only:
                    make_dir(os.path.dirname(symlink_with_path), "symlink subdir")
                    if os.path.exists(symlink_with_path):
                        os.unlink(symlink_with_path)
                    os.symlink(json_media_file_with_path, symlink_with_path)
                touch_with_datetime(symlink_with_path)
                indented_message(save_media_end + " (symlink)", "", 5)
                back_level()

            indented_message(save_media_end, "", 4)

    @staticmethod
    def get_media_from_json_files(json_files, json_files_mtime, patterns_and_passwords):
        next_level()  # OK
        is_provisional_media = False
        files = ", ".join(json_files)
        if len(json_files) == 1:
            suffix = ".json.media_only"
            if files[-len(suffix) :] == suffix:
                is_provisional_media = True
        message(
            "reading album from " + str(len(json_files)) + " json files...", files, 5
        )
        # json_files is the list of the existing files for that cache base
        dictionary = None
        must_process_passwords = False
        for json_file in json_files:
            next_level()  # OK
            message("reading json file...", json_file, 4)
            json_file_with_path = os.path.join(Options.config["cache_path"], json_file)
            with open(json_file_with_path, "r") as json_file_pointer:
                try:
                    json_file_dict = json.load(json_file_pointer)
                except json.decoder.JSONDecodeError:
                    indented_message("not a valid json file: corrupted", json_file, 4)
                    back_level()  # OK
                    back_level()  # OK
                    return [None, True]

            if "jsonVersion" not in json_file_dict:
                indented_message(
                    "not an album cache hit", "nonexistent json_version in json file", 4
                )
                obsolete_json_version = True
                back_level()  # OK
                back_level()  # OK
                return [None, True]

            if json_file_dict["jsonVersion"] != Options.json_version:
                indented_message(
                    "not an album cache hit",
                    "json_version = "
                    + str(json_file_dict["jsonVersion"])
                    + " while in options.json is "
                    + str(Options.json_version),
                    4,
                )
                obsolete_json_version = True
                back_level()  # OK
                back_level()  # OK
                return [None, True]

            if not is_provisional_media:
                codes_combinations = [
                    key
                    for key in json_file_dict["numsProtectedMediaInSubTree"].keys()
                    if json_file_dict["numsProtectedMediaInSubTree"][key]["images"]
                    + json_file_dict["numsProtectedMediaInSubTree"][key]["audios"]
                    + json_file_dict["numsProtectedMediaInSubTree"][key]["videos"]
                    > 0
                ]

                if len(codes_combinations) != len(json_files):
                    indented_message(
                        "not an album cache hit",
                        "json files number ("
                        + str(len(json_files))
                        + ") different from numsProtectedMediaInSubTree keys number("
                        + str(len(codes_combinations))
                        + ") ",
                        4,
                    )
                    back_level()  # OK
                    back_level()  # OK
                    return [None, True]

            if "media" not in json_file_dict:
                media_json_file = calculate_media_file_name(json_file)
                media_json_file_with_path = os.path.join(
                    Options.config["cache_path"], media_json_file
                )
                if os.path.exists(media_json_file_with_path):
                    message(
                        "opening and importing media json file...", media_json_file, 4
                    )
                    with open(media_json_file_with_path, "r") as media_file_pointer:
                        try:
                            json_file_dict["media"] = json.load(media_file_pointer)
                            indented_message("media json file imported!", "", 4)
                        except json.decoder.JSONDecodeError:
                            indented_message(
                                "not an album cache hit: media json file corrupted",
                                media_json_file,
                                4,
                            )
                            back_level()  # OK
                            back_level()  # OK
                            return [None, True]
                else:
                    back_level()  # OK
                    message(
                        "not an album cache hit: nonexistent media json file",
                        media_json_file,
                        4,
                    )
                    back_level()  # OK
                    return [None, True]

            if "codesComplexCombination" in json_file_dict:
                for i in range(len(json_file_dict["media"])):
                    old_album_codes_combination = json_file_dict[
                        "codesComplexCombination"
                    ].split(",")[0]
                    old_media_codes_combination = json_file_dict[
                        "codesComplexCombination"
                    ].split(",")[1]
                    old_album_identifiers_set = (
                        convert_old_codes_set_to_identifiers_set(
                            convert_combination_to_set(old_album_codes_combination)
                        )
                    )
                    old_media_identifiers_set = (
                        convert_old_codes_set_to_identifiers_set(
                            convert_combination_to_set(old_media_codes_combination)
                        )
                    )
                    if (
                        old_media_identifiers_set is None
                        or old_album_identifiers_set is None
                    ):
                        indented_message(
                            "passwords must be processed",
                            "error in going up from code to identifier",
                            4,
                        )
                        must_process_passwords = True
                        break
                    else:
                        json_file_dict["password_identifiers_set"] = (
                            old_album_identifiers_set
                        )
                        json_file_dict["media"][i][
                            "password_identifiers_set"
                        ] = old_media_identifiers_set
                        json_file_dict["media"][i][
                            "album_identifiers_set"
                        ] = old_album_identifiers_set
            else:
                for i in range(len(json_file_dict["media"])):
                    json_file_dict["media"][i]["password_identifiers_set"] = set()
                    json_file_dict["media"][i]["album_identifiers_set"] = set()
                json_file_dict["password_identifiers_set"] = set()

            dictionary = merge_media(dictionary, json_file_dict)
            back_level()  # OK

        indented_message("album read from json file(s)!", "", 5)
        back_level()  # OK
        if dictionary is None:
            indented_message("json files not usable as a cache hit", files, 4)
            return [None, True]
        else:
            # generate the album from the json file loaded into dictionary
            # subalbums are not generated yet
            message("loading media from json file(s)...", "", 5)
            cached_album = Album.media_from_dict(
                dictionary,
                json_files_mtime,
                patterns_and_passwords,
            )
            if cached_album is None:
                must_process_passwords = True
            else:
                n_media = len(cached_album.media)
            if cached_album is not None and n_media:
                indented_message(
                    "media loaded!",
                    str(n_media) + " media from json file(s) " + files,
                    4,
                )
            else:
                indented_message(
                    "no media loaded!",
                    "from json file(s) " + files + ", perhaps json file(s) had trouble",
                    4,
                )
            return [cached_album, must_process_passwords]

    @staticmethod
    def media_from_dict(dictionary, json_files_mtime, patterns_and_passwords):
        must_process_passwords = False
        if "physicalPath" in dictionary:
            path = dictionary["physicalPath"]
        else:
            path = dictionary["path"]
        # Don't use cache if version has changed
        cached_album = Album(os.path.join(Options.config["album_path"], path))
        cached_album.cache_base = dictionary["cacheBase"]
        # cached_album.json_version = dictionary["jsonVersion"]
        if "password_identifiers_set" in dictionary:
            cached_album.password_identifiers_set = dictionary[
                "password_identifiers_set"
            ]

        for single_media_dict in dictionary["media"]:
            if Options.global_pattern != "" and re.search(
                Options.global_pattern, single_media_dict["name"]
            ):
                continue

            new_media = SingleMedia.from_dict(
                cached_album,
                single_media_dict,
                os.path.join(
                    Options.config["album_path"],
                    remove_folders_marker(cached_album.baseless_path),
                ),
                json_files_mtime,
                patterns_and_passwords,
                False,
            )
            if (
                new_media is not False
                and new_media is not None
                and new_media.is_valid
                and not any(
                    new_media.media_file_name == single_media.media_file_name
                    for single_media in cached_album.media_list
                )
            ):
                cached_album.add_single_media(new_media)
            else:
                pass

        cached_album.cache_base = dictionary["cacheBase"]
        cached_album.album_ini_mtime = dictionary["albumIniMTime"]
        cached_album.passwords_marker_mtime = dictionary["passwordMarkerMTime"]
        if "symlinkCodesAndNumbers" in dictionary:
            cached_album.symlink_codes_and_numbers = dictionary[
                "symlinkCodesAndNumbers"
            ]
        if "compositeImageSize" in dictionary:
            cached_album.composite_image_size = dictionary["compositeImageSize"]
        if "optionsTimestamp" in dictionary:
            cached_album.options_timestamp = dictionary["optionsTimestamp"]

        if "title" in dictionary and dictionary["title"]:
            cached_album._attributes["metadata"]["title"] = dictionary["title"]
        if "description" in dictionary and dictionary["description"]:
            cached_album._attributes["metadata"]["description"] = dictionary[
                "description"
            ]
        if (
            "tags" in dictionary
            and len(dictionary["tags"])
            and not (len(dictionary["tags"]) == 1 and dictionary["tags"][0] == "")
        ):
            cached_album._attributes["metadata"]["tags"] = dictionary["tags"]

        # cached_album.sort_subalbums_and_media()

        return cached_album

    def to_dict(self, separate_positions, separate_media):
        # with the root search album, a minimal set of properties is put in the file
        # - cacheBase
        # - numsProtectedMediaInSubTree
        # - subalbums.unicodeWords
        # - subalbums.cacheBase
        # this way the file is much lighter and it loads much faster

        self.sort_subalbums_and_media()
        subalbums = []

        for subalbum in self.subalbums_list:
            if not subalbum.empty:
                if self.cache_base not in (Options.config["by_search_string"],):
                    # path_to_dict = trim_base_custom(subalbum.path, self.baseless_path)
                    # if path_to_dict == "":
                    # 	path_to_dict = Options.config['folders_string']
                    path_to_dict = subalbum.path
                    folder_position = path_to_dict.find(
                        Options.config["folders_string"]
                    )
                    by_date_position = path_to_dict.find(
                        Options.config["by_date_string"]
                    )
                    by_gps_position = path_to_dict.find(Options.config["by_gps_string"])
                    by_search_position = path_to_dict.find(
                        Options.config["by_search_string"]
                    )
                    if not path_to_dict:
                        path_to_dict = Options.config["folders_string"]
                    elif (
                        path_to_dict
                        and by_date_position == -1
                        and by_gps_position == -1
                        and by_search_position == -1
                        and subalbum.cache_base != "root"
                        and folder_position != 0
                    ):
                        path_to_dict = (
                            Options.config["folders_string"] + "/" + path_to_dict
                        )

                sub_dict = {
                    # "path": path_to_dict,
                    "cacheBase": subalbum.cache_base,
                    # "numsMediaInSubTree": subalbum.nums_media_in_sub_tree,
                    # "sizesOfSubTree": subalbum.sizes_of_sub_tree,
                    # "sizesOfAlbum": subalbum.sizes_of_album,
                }
                if self.cache_base not in (Options.config["by_search_string"],):
                    sub_dict["path"] = path_to_dict
                    sub_dict["sizesOfSubTree"] = subalbum.sizes_of_sub_tree
                    sub_dict["sizesOfAlbum"] = subalbum.sizes_of_album
                    sub_dict["numsMediaInSubTree"] = subalbum.nums_media_in_sub_tree
                    if Options.config["expose_image_dates"]:
                        sub_dict["exifDateMax"] = subalbum.exif_date_max_string
                        sub_dict["exifDateMin"] = subalbum.exif_date_min_string
                    if Options.config["expose_original_media"]:
                        sub_dict["fileDateMax"] = subalbum.file_date_max_string
                        sub_dict["fileDateMin"] = subalbum.file_date_min_string
                    sub_dict["pixelSizeMax"] = subalbum.pixel_size_max
                    sub_dict["pixelSizeMin"] = subalbum.pixel_size_min
                    if Options.config["expose_image_positions"]:
                        sub_dict["numPositionsInTree"] = len(
                            subalbum.positions_and_media_in_tree.positions
                        )
                        sub_dict["nonGeotagged"] = {
                            "numsMediaInSubTree": subalbum.nums_media_in_sub_tree_non_geotagged,
                            # "sizesOfSubTree": subalbum.sizes_of_sub_tree_non_geotagged,
                            # "sizesOfAlbum": subalbum.sizes_of_album_non_geotagged,
                        }
                        if self.cache_base not in (Options.config["by_search_string"],):
                            sub_dict["nonGeotagged"][
                                "sizesOfSubTree"
                            ] = subalbum.sizes_of_sub_tree_non_geotagged
                            sub_dict["nonGeotagged"][
                                "sizesOfAlbum"
                            ] = subalbum.sizes_of_album_non_geotagged

                    nums_protected_by_code = {}
                    for complex_identifiers_combination in list(
                        subalbum.nums_protected_media_in_sub_tree.keys()
                    ):
                        if complex_identifiers_combination == ",":
                            nums_protected_by_code[complex_identifiers_combination] = (
                                subalbum.nums_protected_media_in_sub_tree.value(
                                    complex_identifiers_combination
                                )
                            )
                        else:
                            album_identifiers_combination = (
                                complex_identifiers_combination.split(",")[0]
                            )
                            media_identifiers_combination = (
                                complex_identifiers_combination.split(",")[1]
                            )
                            album_codes_combination = convert_set_to_combination(
                                convert_identifiers_set_to_codes_set(
                                    convert_combination_to_set(
                                        album_identifiers_combination
                                    )
                                )
                            )
                            codes_combination = convert_set_to_combination(
                                convert_identifiers_set_to_codes_set(
                                    convert_combination_to_set(
                                        media_identifiers_combination
                                    )
                                )
                            )
                            complex_codes_combination = complex_combination(
                                album_codes_combination, codes_combination
                            )
                            nums_protected_by_code[complex_codes_combination] = (
                                subalbum.nums_protected_media_in_sub_tree.value(
                                    complex_identifiers_combination
                                )
                            )
                    sub_dict["numsProtectedMediaInSubTree"] = nums_protected_by_code

                    sub_dict["numsMedia"] = ImagesAudiosVideos()
                    sub_dict["numsMedia"].set_image_number(
                        len(
                            [
                                _media
                                for _media in subalbum.media_list
                                if _media.is_image
                            ]
                        )
                    )
                    sub_dict["numsMedia"].set_audio_number(
                        len(
                            [
                                _media
                                for _media in subalbum.media_list
                                if _media.is_audio
                            ]
                        )
                    )
                    sub_dict["numsMedia"].set_video_number(
                        len(
                            [
                                _media
                                for _media in subalbum.media_list
                                if _media.is_video
                            ]
                        )
                    )
                    if Options.config["expose_image_positions"]:
                        sub_dict["nonGeotagged"]["numsMedia"] = ImagesAudiosVideos()
                        sub_dict["nonGeotagged"]["numsMedia"].set_image_number(
                            len(
                                [
                                    _media
                                    for _media in subalbum.media_list
                                    if _media.is_image and not _media.has_gps_data
                                ]
                            )
                        )
                        sub_dict["nonGeotagged"]["numsMedia"].set_audio_number(
                            len(
                                [
                                    _media
                                    for _media in subalbum.media_list
                                    if _media.is_audio and not _media.has_gps_data
                                ]
                            )
                        )
                        sub_dict["nonGeotagged"]["numsMedia"].set_video_number(
                            len(
                                [
                                    _media
                                    for _media in subalbum.media_list
                                    if _media.is_video and not _media.has_gps_data
                                ]
                            )
                        )
                    if hasattr(subalbum, "center"):
                        sub_dict["center"] = subalbum.center
                    if hasattr(subalbum, "name"):
                        sub_dict["name"] = subalbum.name
                    if hasattr(subalbum, "alt_name"):
                        sub_dict["altName"] = subalbum.alt_name
                    if hasattr(subalbum, "words"):
                        sub_dict["words"] = subalbum.words
                    if hasattr(subalbum, "random_media"):
                        sub_dict["randomMedia"] = subalbum.random_media
                    if subalbum.title:
                        sub_dict["title"] = subalbum.title
                    if subalbum.description:
                        sub_dict["description"] = subalbum.description
                    if subalbum.tags:
                        sub_dict["tags"] = subalbum.tags

                if hasattr(subalbum, "unicode_words"):
                    sub_dict["unicodeWords"] = subalbum.unicode_words
                subalbums.append(sub_dict)

        if self.cache_base not in (Options.config["by_search_string"],):
            path_without_folders_marker = remove_folders_marker(self.path)

            path_to_dict = self.path
            folder_position = path_to_dict.find(Options.config["folders_string"])
            by_date_position = path_to_dict.find(Options.config["by_date_string"])
            by_gps_position = path_to_dict.find(Options.config["by_gps_string"])
            by_search_position = path_to_dict.find(Options.config["by_search_string"])
            if not path_to_dict:
                path_to_dict = Options.config["folders_string"]
            elif (
                path_to_dict
                and by_date_position == -1
                and by_gps_position == -1
                and by_search_position == -1
                and self.cache_base != "root"
                and folder_position != 0
            ):
                path_to_dict = Options.config["folders_string"] + "/" + path_to_dict

            # ancestors_cache_base = list()
            ancestors_names = list()
            if self.cache_base.find(Options.config["folders_string"]) == 0:
                ancestors_titles = list()
            ancestors_centers = list()
            _parent = self
            while True:
                # ancestors_cache_base.append(_parent.cache_base)

                if hasattr(_parent, "alt_name"):
                    ancestors_names.append(_parent.alt_name)
                elif hasattr(_parent, "name"):
                    ancestors_names.append(_parent.name)

                if self.cache_base.find(Options.config["folders_string"]) == 0:
                    if hasattr(_parent, "title"):
                        ancestors_titles.append(_parent.title)
                    elif hasattr(_parent, "alt_name"):
                        ancestors_titles.append(_parent.alt_name)
                    elif hasattr(_parent, "name"):
                        ancestors_titles.append(_parent.name)

                if hasattr(_parent, "center"):
                    ancestors_centers.append(_parent.center)
                else:
                    ancestors_centers.append("")

                if _parent.parent_cache_base is None:
                    break
                _parent = next(
                    (
                        album
                        for album in Options.all_albums
                        if album.cache_base == _parent.parent_cache_base
                    ),
                    None,
                )
                if _parent is None:
                    break
            # ancestors_cache_base.reverse()
            ancestors_names.reverse()
            if self.cache_base.find(Options.config["folders_string"]) == 0:
                ancestors_titles.reverse()
            ancestors_centers.reverse()

            dictionary = {
                "name": self.name,
                "path": path_to_dict,
                "cacheSubdir": self._subdir,
                "subalbums": subalbums,
                "cacheBase": self.cache_base,
                "ancestorsNames": ancestors_names,
                "physicalPath": path_without_folders_marker,
                "numsMediaInSubTree": self.nums_media_in_sub_tree,
                "sizesOfSubTree": self.sizes_of_sub_tree,
                "sizesOfAlbum": self.sizes_of_album,
                "albumIniMTime": self.album_ini_mtime,
                "passwordMarkerMTime": self.passwords_marker_mtime,
                "jsonVersion": Options.json_version,
            }
            if Options.config["expose_image_dates"]:
                dictionary["exifDateMax"] = self.exif_date_max_string
                dictionary["exifDateMin"] = self.exif_date_min_string
            if Options.config["expose_original_media"]:
                dictionary["fileDateMax"] = self.file_date_max_string
                dictionary["fileDateMin"] = self.file_date_min_string
            dictionary["pixelSizeMax"] = self.pixel_size_max
            dictionary["pixelSizeMin"] = self.pixel_size_min
            if Options.config["expose_image_positions"]:
                dictionary["numPositionsInTree"] = len(
                    self.positions_and_media_in_tree.positions
                )
                dictionary["nonGeotagged"] = {
                    "numsMediaInSubTree": self.nums_media_in_sub_tree_non_geotagged,
                    "sizesOfSubTree": self.sizes_of_sub_tree_non_geotagged,
                    "sizesOfAlbum": self.sizes_of_album_non_geotagged,
                }
        else:
            dictionary = {
                "cacheBase": self.cache_base,
                "subalbums": subalbums,
            }

        nums_protected_by_code = {}
        for complex_identifiers_combination in list(
            self.nums_protected_media_in_sub_tree.keys()
        ):
            if complex_identifiers_combination == ",":
                nums_protected_by_code[","] = (
                    self.nums_protected_media_in_sub_tree.value(
                        complex_identifiers_combination
                    )
                )
            else:
                album_identifiers_combination = complex_identifiers_combination.split(
                    ","
                )[0]
                media_identifiers_combination = complex_identifiers_combination.split(
                    ","
                )[1]
                album_codes_combination = convert_set_to_combination(
                    convert_identifiers_set_to_codes_set(
                        convert_combination_to_set(album_identifiers_combination)
                    )
                )
                codes_combination = convert_set_to_combination(
                    convert_identifiers_set_to_codes_set(
                        convert_combination_to_set(media_identifiers_combination)
                    )
                )
                complex_codes_combination = complex_combination(
                    album_codes_combination, codes_combination
                )
                nums_protected_by_code[complex_codes_combination] = (
                    self.nums_protected_media_in_sub_tree.value(
                        complex_identifiers_combination
                    )
                )
        dictionary["numsProtectedMediaInSubTree"] = nums_protected_by_code

        if self.cache_base not in (Options.config["by_search_string"],):
            if self.complex_combination != ",":
                album_identifiers_combination = self.complex_combination.split(",")[0]
                media_identifiers_combination = self.complex_combination.split(",")[1]
                album_codes_combination = convert_set_to_combination(
                    convert_identifiers_set_to_codes_set(
                        convert_combination_to_set(album_identifiers_combination)
                    )
                )
                codes_combination = convert_set_to_combination(
                    convert_identifiers_set_to_codes_set(
                        convert_combination_to_set(media_identifiers_combination)
                    )
                )
                dictionary["codesComplexCombination"] = complex_combination(
                    album_codes_combination, codes_combination
                )
            if not separate_positions and Options.config["expose_image_positions"]:
                dictionary["positionsAndMediaInTree"] = self.positions_and_media_in_tree
            if not separate_media and self.cache_base not in (
                Options.config["by_search_string"],
            ):
                dictionary["media"] = self.media_list
            else:
                dictionary["numsMedia"] = ImagesAudiosVideos()
                dictionary["numsMedia"].set_image_number(
                    len([_media for _media in self.media_list if _media.is_image])
                )
                dictionary["numsMedia"].set_audio_number(
                    len([_media for _media in self.media_list if _media.is_audio])
                )
                dictionary["numsMedia"].set_video_number(
                    len([_media for _media in self.media_list if _media.is_video])
                )
            if Options.config["expose_image_positions"]:
                dictionary["nonGeotagged"]["numsMedia"] = ImagesAudiosVideos()
                dictionary["nonGeotagged"]["numsMedia"].set_image_number(
                    len(
                        [
                            _media
                            for _media in self.media_list
                            if _media.is_image and not _media.has_gps_data
                        ]
                    )
                )
                dictionary["nonGeotagged"]["numsMedia"].set_audio_number(
                    len(
                        [
                            _media
                            for _media in self.media_list
                            if _media.is_audio and not _media.has_gps_data
                        ]
                    )
                )
                dictionary["nonGeotagged"]["numsMedia"].set_video_number(
                    len(
                        [
                            _media
                            for _media in self.media_list
                            if _media.is_video and not _media.has_gps_data
                        ]
                    )
                )
            if hasattr(self, "symlink_codes_and_numbers"):
                dictionary["symlinkCodesAndNumbers"] = self.symlink_codes_and_numbers
            if hasattr(self, "composite_image_size"):
                dictionary["compositeImageSize"] = self.composite_image_size
            if hasattr(self, "center"):
                dictionary["center"] = self.center
            if hasattr(self, "name"):
                dictionary["name"] = self.name
            if hasattr(self, "alt_name"):
                dictionary["altName"] = self.alt_name
            if hasattr(self, "words"):
                dictionary["words"] = self.words
            if hasattr(self, "random_media"):
                dictionary["randomMedia"] = self.random_media
            if hasattr(self, "unicode_words"):
                dictionary["unicodeWords"] = self.unicode_words
            if self.title:
                dictionary["title"] = self.title
            if self.description:
                dictionary["description"] = self.description
            if self.tags:
                dictionary["tags"] = self.tags
            if self.cache_base.find(Options.config["folders_string"]) == 0:
                dictionary["ancestorsTitles"] = ancestors_titles
            if self.cache_base.find(Options.config["by_gps_string"]) == 0:
                dictionary["ancestorsCenters"] = ancestors_centers
            if hasattr(self, "options_timestamp"):
                dictionary["optionsTimestamp"] = Options.config["options_timestamp"]

        # dictionary["optionsUpdatingJsonFile"] = Options.options_updating_json_file

        return dictionary

    def single_media_from_path(self, path):
        _path = remove_album_path(path)
        for single_media in self.media_list:
            if _path == single_media.media_file_name:
                return single_media
        return None

    def generate_cache_base(self, subalbum_or_media_path, media_file_name=None):
        # this method calculate the cache base for a subalbum or a single media
        # for a single media, the parameter media_file_name has to be given;
        # in this case subalbum_or_media_path is the single media file name without any path info
        # result only has ascii characters

        prefix = ""
        if media_file_name is None:
            # save and remove the folders/date/gps/search prefix in order to respect it
            for _prefix in [
                Options.config["folders_string"],
                Options.config["by_date_string"],
                Options.config["by_gps_string"],
                Options.config["by_search_string"],
            ]:
                position = subalbum_or_media_path.find(_prefix)
                if position == 0:
                    prefix = _prefix
                    subalbum_or_media_path = subalbum_or_media_path[len(prefix) :]
                    break

        # respect alphanumeric characters, substitute non-alphanumeric (but not slashes) with underscore
        subalbum_or_media_path = switch_to_lowercase(
            remove_accents(
                remove_all_but_alphanumeric_chars_dashes_slashes(subalbum_or_media_path)
            )
        )

        # subalbums: convert the character which is used as slash replacement
        if media_file_name is None:
            subalbum_or_media_path = subalbum_or_media_path.replace(
                Options.config["cache_folder_separator"], " "
            )

        # convert spaces and dashes to underscores
        subalbum_or_media_path = subalbum_or_media_path.replace(" ", "_")
        subalbum_or_media_path = subalbum_or_media_path.replace(
            Options.config["cache_folder_separator"], "_"
        )

        # convert to ascii only characters
        subalbum_or_media_path = "/".join(
            [
                transliterate_to_ascii(part).replace("/", "_")
                for part in subalbum_or_media_path.split("/")
            ]
        )

        # convert slashes to proper separator
        subalbum_or_media_path = subalbum_or_media_path.replace(
            "/", Options.config["cache_folder_separator"]
        )

        while (
            subalbum_or_media_path.find("__") != -1
            or
            # subalbum_or_media_path.find(Options.config['cache_folder_separator'] + "_") != -1 or
            # subalbum_or_media_path.find("_" + Options.config['cache_folder_separator']) != -1 or
            subalbum_or_media_path.find(
                Options.config["cache_folder_separator"]
                + Options.config["cache_folder_separator"]
            )
            != -1
        ):
            subalbum_or_media_path = subalbum_or_media_path.replace("__", "_")
            # subalbum_or_media_path = subalbum_or_media_path.replace(Options.config['cache_folder_separator'] + '_', Options.config['cache_folder_separator'])
            # subalbum_or_media_path = subalbum_or_media_path.replace('_' + Options.config['cache_folder_separator'], Options.config['cache_folder_separator'])
            subalbum_or_media_path = subalbum_or_media_path.replace(
                Options.config["cache_folder_separator"]
                + Options.config["cache_folder_separator"],
                Options.config["cache_folder_separator"],
            )

        # restore the saved prefix
        subalbum_or_media_path = prefix + subalbum_or_media_path

        if (
            media_file_name is None
            and hasattr(self, "subalbums_list")
            or media_file_name is not None
            and hasattr(self, "media_list")
        ):
            # let's avoid that different album/media with equivalent names have the same cache base
            distinguish_suffix = 0
            while True:
                _path = subalbum_or_media_path
                if distinguish_suffix:
                    _path += "_" + str(distinguish_suffix)
                if (
                    media_file_name is None
                    and any(
                        _path == _subalbum.cache_base
                        and self.absolute_path != _subalbum.absolute_path
                        for _subalbum in self.subalbums_list
                    )
                    or media_file_name is not None
                    and any(
                        _path == _media.cache_base
                        and media_file_name != _media.media_file_name
                        for _media in self.media_list
                    )
                ):
                    distinguish_suffix += 1
                else:
                    subalbum_or_media_path = _path
                    break

        return subalbum_or_media_path


class Position(object):
    def __init__(self, single_media):
        self.lng = single_media.longitude
        self.lat = single_media.latitude
        self.mediaList = [single_media]

    def belongs(self, single_media):
        # checks whether the single media given as argument can belong to the position
        return self.lng == single_media.longitude and self.lat == single_media.latitude

    def add(self, single_media):
        # the media to add is supposed to have the same lat and lng as the position
        self.mediaList.append(single_media)

    def extend_media(self, position):
        self.mediaList.extend(position.mediaList)

    def copy(self):
        new_position = Position(self.mediaList[0])
        new_position.mediaList = self.mediaList[:]
        return new_position


class Positions(object):
    def __init__(self, single_media, positions=None):
        self.positions = []
        self.last_used_index = None

        if single_media is not None:
            self.add_single_media(single_media)
        elif positions is not None:
            self.merge(positions)

    def add_position(self, position):
        for index, _position in enumerate(self.positions):
            if position.lat == _position.lat and position.lng == _position.lng:
                self.positions[index].extend_media(position)
                self.last_used_index = index
                return
        self.positions.insert(0, position.copy())
        self.last_used_index = 0

    def merge(self, positions):
        # adds the media position and name to the positions list received as second argument
        for position in positions.positions:
            self.add_position(position)

    def add_single_media(self, single_media):
        # print(self.last_used_index, len(self.positions))
        if (
            self.last_used_index is not None
            and self.last_used_index < len(self.positions)
            and self.positions[self.last_used_index].belongs(single_media)
        ):
            self.positions[self.last_used_index].add(single_media)
            return

        # for index, position in enumerate(self.positions):
        #     if self.positions[index].belongs(single_media):
        #         self.positions[index].add(single_media)
        #         self.last_used_index = index
        #         print(self.last_used_index)
        #         return

        self.add_position(Position(single_media))

    def remove_empty_positions(self):
        self.positions = [
            position for position in self.positions if len(position.mediaList) > 0
        ]

    def to_dict(self, type_string=None):
        positions = []
        for position in self.positions:
            position_dict = {"lat": position.lat, "lng": position.lng, "mediaList": []}
            for single_media in position.mediaList:
                # media_album_cache_base = single_media.album.cache_base
                # if type_string == Options.config['by_date_string']:
                # 	media_album_cache_base = single_media.day_album_cache_base
                # elif type_string == Options.config['by_gps_string']:
                # 	media_album_cache_base = single_media.gps_album_cache_base
                position_dict["mediaList"].append(
                    {
                        "cacheBase": single_media.cache_base,
                        "foldersCacheBase": single_media.album.cache_base,
                    }
                )
            positions.append(position_dict)

        return positions

    def copy(self):
        new_positions = Positions(None)
        new_positions.positions = [position.copy() for position in self.positions]
        return new_positions

    def count_media(self):
        count = 0
        for position in self.positions:
            count += len(position.mediaList)
        return count


class NumsProtected(object):
    def __init__(self):
        self.nums_protected = {}
        self.nums_protected[","] = ImagesAudiosVideos()

    def increment_images(self, complex_identifiers_combination):
        try:
            self.nums_protected[complex_identifiers_combination].increment_images()
        except KeyError:
            self.nums_protected[complex_identifiers_combination] = ImagesAudiosVideos()
            self.nums_protected[complex_identifiers_combination].increment_images()

    def increment_audios(self, complex_identifiers_combination):
        try:
            self.nums_protected[complex_identifiers_combination].increment_audios()
        except KeyError:
            self.nums_protected[complex_identifiers_combination] = ImagesAudiosVideos()
            self.nums_protected[complex_identifiers_combination].increment_audios()

    def increment_videos(self, complex_identifiers_combination):
        try:
            self.nums_protected[complex_identifiers_combination].increment_videos()
        except KeyError:
            self.nums_protected[complex_identifiers_combination] = ImagesAudiosVideos()
            self.nums_protected[complex_identifiers_combination].increment_videos()

    def total(self, complex_identifiers_combination):
        return (
            self.nums_protected[complex_identifiers_combination].get_image_number()
            + self.nums_protected[complex_identifiers_combination].get_video_number()
            + self.nums_protected[complex_identifiers_combination].get_audio_number()
        )

    def value(self, complex_identifiers_combination):
        return self.nums_protected[complex_identifiers_combination]

    def keys(self):
        return list(self.nums_protected.keys())

    def protected_keys(self):
        return [key for key in list(self.keys()) if key != ","]

    def merge(self, nums_protected):
        for complex_identifiers_combination in list(nums_protected.keys()):
            try:
                self.nums_protected[complex_identifiers_combination].sum(
                    nums_protected.nums_protected[complex_identifiers_combination]
                )
            except KeyError:
                self.nums_protected[complex_identifiers_combination] = (
                    ImagesAudiosVideos()
                )
                self.nums_protected[complex_identifiers_combination].sum(
                    nums_protected.nums_protected[complex_identifiers_combination]
                )

    def used_identifier_combinations(self):
        keys = self.protected_keys()
        keys = sorted(sorted(keys), key=lambda single_key: len(single_key.split("-")))
        return keys

    def copy(self):
        return copy.deepcopy(self)

    def to_dict(self):
        return {k: self.nums_protected[k].to_dict() for k in set(self.nums_protected)}


class ImagesAudiosVideos(object):
    def __init__(self):
        self.sizes = {"images": 0, "audios": 0, "videos": 0}

    def copy(self):
        return copy.deepcopy(self)

    def sum(self, other):
        if other is not None:
            self.sizes = {
                "images": self.get_image_number() + other.get_image_number(),
                "audios": self.get_audio_number() + other.get_audio_number(),
                "videos": self.get_video_number() + other.get_video_number(),
            }
        else:
            return self.copy()

    def set_image_number(self, value):
        self.sizes["images"] = value

    def set_audio_number(self, value):
        self.sizes["audios"] = value

    def set_video_number(self, value):
        self.sizes["videos"] = value

    def total(self):
        return (
            self.get_image_number() + self.get_video_number() + self.get_audio_number()
        )

    def increment_images(self):
        self.sizes = {
            "images": self.sizes["images"] + 1,
            "audios": self.sizes["audios"],
            "videos": self.sizes["videos"],
        }

    def increment_audios(self):
        self.sizes = {
            "images": self.sizes["images"],
            "audios": self.sizes["audios"] + 1,
            "videos": self.sizes["videos"],
        }

    def increment_videos(self):
        self.sizes = {
            "images": self.sizes["images"],
            "audios": self.sizes["audios"],
            "videos": self.sizes["videos"] + 1,
        }

    def get_image_number(self):
        return self.sizes["images"]

    def get_audio_number(self):
        return self.sizes["audios"]

    def get_video_number(self):
        return self.sizes["videos"]

    def from_dict(self, dict):
        self.sizes = dict

    def to_dict(self):
        return self.sizes


class Size(object):
    def __init__(self):
        self.sizes = {"images": 0, "audios": 0, "videos": 0}

    def copy(self):
        return copy.deepcopy(self)

    def sum(self, other):
        if other is not None:
            self.sizes = {
                "images": self.get_image_size() + other.get_image_size(),
                "audios": self.get_audio_size() + other.get_audio_size(),
                "videos": self.get_video_size() + other.get_video_size(),
            }
        else:
            return self.copy()

    def set_image_size(self, value):
        self.sizes["images"] = value

    def set_audio_size(self, value):
        self.sizes["audios"] = value

    def set_video_size(self, value):
        self.sizes["videos"] = value

    def get_image_size(self):
        return self.sizes["images"]

    def get_audio_size(self):
        return self.sizes["audios"]

    def get_video_size(self):
        return self.sizes["videos"]

    def total(self):
        return self.get_image_size() + self.get_audio_size() + self.get_video_size()

    def from_dict(self, dict):
        self.sizes = dict

    def to_dict(self):
        return self.sizes


class Sizes(object):
    def __init__(self, single_media=None):
        self.sizes = {}
        if Options.config["expose_original_media"]:
            self.sizes["original"] = Size()
        for format in Options.config["cache_images_formats"]:
            self.sizes[format] = {}
            if (
                Options.config["expose_original_media"]
                or Options.config["expose_full_size_media_in_cache"]
            ):
                self.sizes[format][0] = Size()
            for thumb_size in Options.config["reduced_sizes"]:
                self.sizes[format][thumb_size] = Size()

    def sum(self, other):
        if other is not None:
            for format in self.sizes:
                if format == "original":
                    self.sizes["original"].sum(other.sizes["original"])
                else:
                    for size in set(self.sizes[format]):
                        try:
                            self.sizes[format][size].sum(other.sizes[format][size])
                        except KeyError:
                            pass

    def copy(self):
        return copy.deepcopy(self)

    def set_images_size(self, format, size, value):
        self.sizes[format][size].set_image_size(value)

    def set_audios_size(self, size, value):
        for format in Options.config["cache_images_formats"]:
            self.sizes[format][size].set_audio_size(value)

    def set_videos_size(self, size, value):
        for format in Options.config["cache_images_formats"]:
            self.sizes[format][size].set_video_size(value)

    def get_full_size_media_size(self, format):
        return self.get_size(format, 0)

    def get_size(self, format, size):
        try:
            image_size = self.sizes[format][size].get_image_size()
        except KeyError:
            image_size = 0

        try:
            audio_size = self.sizes[format][size].get_audio_size()
        except KeyError:
            audio_size = 0

        try:
            video_size = self.sizes[format][size].get_video_size()
        except KeyError:
            video_size = 0

        return image_size + audio_size + video_size

    def set_original_image_size(self, value):
        self.sizes["original"].set_image_size(value)

    def set_original_audio_size(self, value):
        self.sizes["original"].set_audio_size(value)

    def set_original_video_size(self, value):
        self.sizes["original"].set_video_size(value)

    def get_original_size(self):
        return self.sizes["original"]

    def from_dict(self, dict):
        for format in dict:
            if format == "original":
                self.sizes["original"] = dict["original"]
            else:
                for size in dict[format]:
                    if format in self.sizes:
                        size_int = int(size)
                        if size_int in self.sizes[format]:
                            self.sizes[format][size_int].from_dict(dict[format][size])

    def to_dict(self):
        result = {}
        for format in self.sizes:
            if format == "original":
                result["original"] = self.sizes["original"]
            else:
                result[format] = {
                    size: self.sizes[format][size].to_dict()
                    for size in set(self.sizes[format])
                    # if self.get_size(format, size) > 0
                }
        return result


class SizesProtected(object):
    def __init__(self):
        self.sizes_protected = {}
        self.sizes_protected[","] = Sizes()

    def sum(self, complex_identifiers_combination, sizes):
        if complex_identifiers_combination not in self.sizes_protected:
            self.sizes_protected[complex_identifiers_combination] = Sizes()
        self.sizes_protected[complex_identifiers_combination].sum(sizes)

    def sizes(self, complex_identifiers_combination):
        return self.sizes_protected[complex_identifiers_combination]

    def keys(self):
        return list(self.sizes_protected.keys())

    def protected_keys(self):
        return [key for key in list(self.keys()) if key != ","]

    def merge(self, sizes_protected):
        if sizes_protected is not None:
            for complex_identifiers_combination in list(sizes_protected.keys()):
                if complex_identifiers_combination not in self.sizes_protected:
                    self.sizes_protected[complex_identifiers_combination] = Sizes()
                self.sizes_protected[complex_identifiers_combination].sum(
                    sizes_protected.sizes_protected[complex_identifiers_combination]
                )

    def copy(self):
        return copy.deepcopy(self)


class SingleMedia(object):
    def __init__(
        self,
        album,
        media_path,
        json_files_mtime,
        patterns_and_passwords,
        single_media_cache_hit,
        must_process_passwords,
        dictionary=None,
    ):
        self.password_identifiers_set = set()
        self.album_identifiers_set = set()
        self.clean = True
        self.errors = {}
        self.album = album
        self.media_path = media_path
        self.media_file_name = remove_album_path(media_path)
        dirname = os.path.dirname(media_path)
        self.folders = remove_album_path(dirname)
        self.album_path = os.path.join("albums", self.media_file_name)

        if dictionary is not None:
            # media generation from json cache
            self.generate_single_media_from_cache(dictionary)
        else:
            # media generation from file
            if not single_media_cache_hit or must_process_passwords:
                [
                    self.password_identifiers_set,
                    self.album_identifiers_set,
                ] = album.get_matching_media_passwords(
                    patterns_and_passwords, media_path
                )
                for identifier in self.password_identifiers_set:
                    Options.mark_identifier_as_used(identifier)
                for identifier in self.album_identifiers_set:
                    Options.mark_identifier_as_used(identifier)
            else:
                indented_message("no need to process passwords for media", "", 5)

            image = self.generate_single_media_from_file()
            if self.is_valid:
                self.generate_cache_files(image, json_files_mtime)

                if self.has_gps_data:
                    message("looking for geonames...", "", 5)
                    self.get_geonames()

    def check_all_cache_files(self, json_files_mtime):
        # check if the cache files actually exist and are valid
        single_media_cache_hit = True
        for type_group_name in self.types_and_names:
            if not single_media_cache_hit:
                break
            for type_and_name in self.types_and_names[type_group_name]:
                if not single_media_cache_hit:
                    break
                for format in type_and_name["file names"]:
                    if not single_media_cache_hit:
                        break
                    if not self.cache_file_and_symlinks_are_ok(
                        type_group_name,
                        type_and_name,
                        format,
                        json_files_mtime,
                    ):
                        single_media_cache_hit = False

        return single_media_cache_hit

    @staticmethod
    def my_get_size(absolute_file_name):
        try:
            size = os.path.getsize(absolute_file_name)
        except FileNotFoundError:
            size = False
        return size

    def fill_file_sizes_structure(self):
        file_sizes = Sizes()
        original_size = os.path.getsize(self.media_path)
        if Options.config["expose_original_media"]:
            # fill with the original media size
            if self.is_image:
                file_sizes.set_original_image_size(original_size)
            elif self.is_audio:
                file_sizes.set_original_audio_size(original_size)
            elif self.is_video:
                file_sizes.set_original_video_size(original_size)

        if self.is_audio:
            for type_and_name in self.types_and_names["os copy types"]:
                absolute_full_size_name = os.path.join(
                    Options.config["cache_path"],
                    type_and_name["file names"]["copy"]["name"],
                )
                file_sizes.set_audios_size(0, self.my_get_size(absolute_full_size_name))
            for type_and_name in self.types_and_names["audio types"]:
                if type_and_name["type"] == "full size audio":
                    absolute_full_size_name = os.path.join(
                        Options.config["cache_path"],
                        type_and_name["file names"]["mp3"]["name"],
                    )
                    file_sizes.set_audios_size(
                        0, self.my_get_size(absolute_full_size_name)
                    )
                if type_and_name["type"] == "transcoded audio":
                    absolute_transcoded_name = os.path.join(
                        Options.config["cache_path"],
                        type_and_name["file names"]["mp3"]["name"],
                    )
                    for size in Options.config["reduced_sizes"]:
                        file_sizes.set_audios_size(
                            size, self.my_get_size(absolute_transcoded_name)
                        )
        if self.is_video:
            for type_and_name in self.types_and_names["os copy types"]:
                absolute_full_size_name = os.path.join(
                    Options.config["cache_path"],
                    type_and_name["file names"]["copy"]["name"],
                )
                file_sizes.set_videos_size(0, self.my_get_size(absolute_full_size_name))
            for type_and_name in self.types_and_names["video types"]:
                if type_and_name["type"] == "full size video":
                    absolute_full_size_name = os.path.join(
                        Options.config["cache_path"],
                        type_and_name["file names"]["mp4"]["name"],
                    )
                    file_sizes.set_videos_size(
                        0, self.my_get_size(absolute_full_size_name)
                    )
                if type_and_name["type"] == "transcoded video":
                    absolute_transcoded_name = os.path.join(
                        Options.config["cache_path"],
                        type_and_name["file names"]["mp4"]["name"],
                    )
                    for size in Options.config["reduced_sizes"]:
                        file_sizes.set_videos_size(
                            size, self.my_get_size(absolute_transcoded_name)
                        )
        elif self.is_image:
            formats_minus_full_size_formats = set(
                Options.config["cache_images_formats"]
            )
            for type_group_name in ("square types", "non square types"):
                if type_group_name in self.types_and_names:
                    for type_and_name in self.types_and_names[type_group_name]:
                        if type_and_name["type"] == "full size image":
                            for format in type_and_name["file names"]:
                                absolute_full_size_name = os.path.join(
                                    Options.config["cache_path"],
                                    type_and_name["file names"][format]["name"],
                                )
                                file_sizes.set_images_size(
                                    format,
                                    0,
                                    self.my_get_size(absolute_full_size_name),
                                )
                                formats_minus_full_size_formats.remove(format)

                        if "reduction" in type_and_name["type"]:
                            for format in type_and_name["file names"]:
                                absolute_name = os.path.join(
                                    Options.config["cache_path"],
                                    type_and_name["file names"][format]["name"],
                                )
                                file_sizes.set_images_size(
                                    format,
                                    type_and_name["size"],
                                    self.my_get_size(absolute_name),
                                )
            for type_and_name in self.types_and_names["os copy types"]:
                # the os copy could be there, too, and it must correspond to the format not present in full size images
                absolute_full_size_name = os.path.join(
                    Options.config["cache_path"],
                    type_and_name["file names"]["copy"]["name"],
                )
                file_sizes.set_images_size(
                    list(formats_minus_full_size_formats)[0],
                    0,
                    self.my_get_size(absolute_full_size_name),
                )

        self._attributes["fileSizes"] = file_sizes
        return True

    def generate_single_media_from_cache(self, dictionary):
        self.cache_base = dictionary["cacheBase"]
        self.mime_type = dictionary["mimeType"]
        if self.is_video or self.is_audio:
            # try:
            self.codec = dictionary["codec"]
            # except KeyError:
            #     return False

        if "password_identifiers_set" in dictionary:
            self.password_identifiers_set = dictionary["password_identifiers_set"]
        # else:
        # 	self.password_identifiers_set = set()
        if "album_identifiers_set" in dictionary:
            self.album_identifiers_set = dictionary["album_identifiers_set"]
        # else:
        # 	self.album_identifiers_set = set()
        if "imageSizeForSharing" in dictionary:
            self.image_size_for_sharing = dictionary["imageSizeForSharing"]

        self.is_valid = True

        self._attributes = dictionary
        file_sizes_dict = self._attributes["fileSizes"]
        self._attributes["fileSizes"] = Sizes()
        self.file_sizes.from_dict(file_sizes_dict)

        self.generate_types_and_names()

        return True

    def cache_file_and_symlinks_are_ok(
        self, type_group_name, type_and_name, format, json_files_mtime
    ):
        # pprint(type_and_name["file names"][format])
        result = self.cache_file_is_ok(
            type_group_name, type_and_name, format, json_files_mtime
        ) and (
            not "symlinks" in type_and_name["file names"][format]
            or self.cache_symlinks_are_ok(type_group_name, type_and_name, format)
        )
        return result

    def cache_file_is_ok(
        self, type_group_name, type_and_name, format, json_files_mtime
    ):
        if type_and_name["file names"][format]["ok"] is not None:
            return type_and_name["file names"][format]["ok"]

        # pprint(type_and_name)

        _type = type_and_name["type"]
        type_group_name_for_message = type_group_name
        position = type_group_name_for_message.find(" types")
        if position != -1:
            type_group_name_for_message = type_group_name_for_message[0:position]
        _type_for_message = type_group_name_for_message + ", " + _type
        file_name = type_and_name["file names"][format]["name"]
        absolute_file_name = os.path.join(
            Options.config["cache_path"],
            file_name,
        )
        absolute_file_name_exists = os.path.exists(absolute_file_name)
        absolute_subdir = os.path.dirname(absolute_file_name)

        if absolute_file_name_exists and not os.access(absolute_file_name, os.W_OK):
            indented_message(
                "FATAL ERROR", absolute_file_name + " not writable, quitting"
            )
            sys.exit(1)

        json_files_timestamp = ""
        if not absolute_file_name_exists:
            indented_message(
                "not a single media cache hit",
                "nonexistent " + _type_for_message + ": " + file_name,
                5,
            )
        else:
            next_level()  # OK
            message("checking " + _type_for_message + "...", file_name, 6)

            file_size = self.my_get_size(absolute_file_name)
            reduction_size = type_and_name["actual size"]
            if type_and_name["type"].find("full size") == 0:
                reduction_size = 0
            if file_size == 0 or file_size == False:
                indented_message(
                    "not a single media cache hit",
                    _type_for_message + " has 0 length or doesn't exist",
                    5,
                )
            elif (
                (
                    reduction_size == 0
                    and (
                        Options.config["expose_original_media"]
                        and not original_media_is_enough(self)
                        or Options.config["expose_full_size_media_in_cache"]
                    )
                )
                and self.file_sizes.get_full_size_media_size(format) > 0
                and self.file_sizes.get_full_size_media_size(format) != file_size
            ):
                indented_message(
                    "not a single media cache hit",
                    "full size media in cache has file size different: "
                    + str(file_size)
                    + " (on disk) != "
                    + str(self.file_sizes.get_full_size_media_size(format))
                    + " (in json file, fileSizes["
                    + format
                    + "][0])",
                    5,
                )
            elif (
                type_and_name["type"] == "reduction"
                and type_and_name["size"] in Options.config["reduced_sizes"]
                and self.file_sizes.get_size(format, reduction_size) != file_size
            ):
                indented_message(
                    "not a single media cache hit",
                    format
                    + " "
                    + str(reduction_size)
                    + " px reduction in cache has file size different: "
                    + str(file_size)
                    + " (on disk) != "
                    + str(self.file_sizes.get_size(format, reduction_size))
                    + " (in json file, fileSizes["
                    + format
                    + "]["
                    + str(reduction_size)
                    + "])",
                    5,
                )
            elif file_mtime(absolute_file_name) < self.datetime_file_modified:
                indented_message(
                    "not a single media cache hit",
                    _type_for_message + " older than original media",
                    5,
                )
            elif json_files_mtime is not None and (
                json_files_mtime != file_mtime(absolute_file_name)
            ):
                indented_message(
                    "not a single media cache hit",
                    _type_for_message
                    + " newer than json file(s): "
                    + str(json_files_mtime)
                    + " (json), "
                    + str(file_mtime(absolute_file_name))
                    + " ("
                    + _type_for_message
                    + ")",
                    5,
                )
            elif (
                json_files_mtime is not None
                and json_files_mtime.strftime(Options.timestamp_format_for_file_name)
                in Options.config["provisional_date_strings"]
            ):
                json_files_timestamp = json_files_mtime.strftime(
                    Options.timestamp_format_for_file_name
                )

                if (
                    format == "jpg"
                    and Options.update[json_files_timestamp]["jpg"]
                    or format == "webp"
                    and Options.update[json_files_timestamp]["webp"]
                    or Options.config["avif_supported"]
                    and format == "avif"
                    and Options.update[json_files_timestamp]["avif"]
                    or Options.config["jxl_supported"]
                    and format == "jxl"
                    and Options.update[json_files_timestamp]["jxl"]
                    or Options.config["heic_supported"]
                    and format == "heic"
                    and Options.update[json_files_timestamp]["heic"]
                    or format == "png"
                    and Options.update[json_files_timestamp]["png"]
                ):
                    indented_message(
                        "not a single media cache hit",
                        "some option change requests '" + format + "' recreation",
                        5,
                    )
                elif (
                    _type.find("reduction") > -1
                    and Options.update[json_files_timestamp]["reduced_images"]
                    or _type.endswith("thumbnail")
                    and Options.update[json_files_timestamp]["thumbnails"]
                    or self.is_image
                    and _type == "full size image"
                    and Options.update[json_files_timestamp]["full_size_image_copies"]
                    or self.is_audio
                    and (
                        _type.endswith("audio")
                        and Options.update[json_files_timestamp]["transcoded_audios"]
                    )
                    or self.is_video
                    and (
                        _type.endswith("video")
                        and Options.update[json_files_timestamp]["transcoded_videos"]
                    )
                ):
                    indented_message(
                        "not a single media cache hit",
                        "some option change requests "
                        + _type_for_message
                        + " recreation",
                        5,
                    )
                else:
                    # the reduced image/thumbnail is there and is valid
                    indented_message(
                        _type_for_message + " OK",
                        str(type_and_name["actual size"]) + " px, " + format,
                        5,
                    )
                    back_level()
                    type_and_name["file names"][format]["ok"] = True
                    touch_with_datetime(absolute_file_name)
                    return True
            back_level()

        type_and_name["file names"][format]["ok"] = False
        return False

    def cache_symlinks_are_ok(self, type_group_name, type_and_name, format):
        if all(
            [
                symlink["ok"] is not None and symlink["ok"]
                for symlink in type_and_name["file names"][format]["symlinks"]
            ]
        ):
            return True

        _type = type_and_name["type"]
        type_group_name_for_message = type_group_name
        position = type_group_name_for_message.find(" types")
        if position != -1:
            type_group_name_for_message = type_group_name_for_message[0:position]
        _type_for_message = type_group_name_for_message + ", " + _type

        file_name = type_and_name["file names"][format]["name"]
        absolute_file_name = os.path.join(
            Options.config["cache_path"],
            file_name,
        )

        next_level()
        message("checking symlinks for " + _type_for_message + "...", file_name, 5)
        next_level()  # OK
        for symlink_object in type_and_name["file names"][format]["symlinks"]:
            symlink = symlink_object["name"]
            absolute_symlink = os.path.join(
                Options.config["cache_path"],
                symlink,
            )

            absolute_symlink_exists = os.path.exists(absolute_symlink)

            message("checking symlink for " + _type_for_message + "...", symlink, 5)
            if not absolute_symlink_exists:
                indented_message(
                    "not a single media cache hit",
                    "nonexistent symlink for " + _type_for_message + ": " + symlink,
                    5,
                )
            elif not os.path.islink(absolute_symlink):
                indented_message(
                    "not a single media cache hit",
                    "symlink for "
                    + _type_for_message
                    + " is not a symlink: "
                    + symlink,
                    5,
                )
            elif os.readlink(absolute_symlink) != absolute_file_name:
                indented_message(
                    "not a single media cache hit",
                    "incorrect symlink '"
                    + symlink
                    + "' for "
                    + _type_for_message
                    + ": it points to '"
                    + os.readlink(symlink)
                    + ", it should point to '"
                    + absolute_file_name
                    + "'",
                    5,
                )
            else:
                indented_message(
                    "symlink for " + _type_for_message + " OK",
                    symlink,
                    5,
                )
                back_level()
                back_level()
                symlink_object["ok"] = True
                # touch_with_datetime(absolute_symlink)
                return True
        back_level()

        symlink_object["ok"] = False
        return False

    def generate_single_media_from_file(self):
        def validate_gps_exif():
            # check gps values for validity
            wrong_latitude_or_longitude = False
            wrong_altitude = False
            for k in [tag for tag in tags_for_codes if tag in exif]:
                if (
                    (k == "EXIF:GPSLongitude" or k == "EXIF:GPSLatitude")
                    and isinstance(exif[k], str)
                    and exif[k].isnumeric()
                    or k == "EXIF:GPSLongitudeRef"
                    and exif[k] != "E"
                    and exif[k] != "W"
                    or k == "EXIF:GPSLatitudeRef"
                    and exif[k] != "N"
                    and exif[k] != "S"
                ):
                    wrong_latitude_or_longitude = True

                try:
                    if (
                        k == "EXIF:GPSAltitude"
                        and isinstance(exif[k], str)
                        and exif[k].isnumeric()
                        or k == "EXIF:GPSAltitudeRef"
                        and float(exif[k]) != 0
                        and float(exif[k]) != 1
                    ):
                        wrong_altitude = True
                except ValueError:
                    wrong_altitude = True

                # exiftool has a bug https://exiftool.org/forum/index.php?topic=15769.0:
                # sometimes it produces string values instead of float ones
                if (
                    k == "EXIF:GPSAltitude"
                    and not wrong_altitude
                    or k
                    in [
                        "EXIF:GPSLatitude",
                        "EXIF:GPSLongitude",
                    ]
                    and not wrong_latitude_or_longitude
                    and isinstance(exif[k], str)
                ):
                    exif[k] = float(exif[k])

            if wrong_latitude_or_longitude:
                for k in [
                    "EXIF:GPSLatitude",
                    "EXIF:GPSLatitudeRef",
                    "EXIF:GPSLongitude",
                    "EXIF:GPSLongitudeRef",
                ]:
                    try:
                        del exif[k]
                    except KeyError:
                        pass
            if wrong_altitude:
                for k in [
                    "EXIF:GPSAltitude",
                    "EXIF:GPSAltitudeRef",
                ]:
                    try:
                        del exif[k]
                    except KeyError:
                        pass
            return

        # end of nested function

        next_level()  # OK

        # the mime type I get here is provisional for images, further it will be detected with PIL
        message("detecting mime type...", "", 6)
        self.mime_type = magic.from_file(self.media_path, mime=True)
        message("mime type detected!", self.mime_type, 5)

        # temporary fix for "ISO Media, MPEG-4 (.MP4) for SonyPSP" files whose mime type is incorrectly reported as audio/mp4
        # TO DO: remove when libmagic1's Bug#941420 is fixed
        if self.mime_type == "audio/mp4":
            self.mime_type = "video/mp4"

        if self.mime_type in ["video/x-ifo"]:
            self.is_valid = False
            back_level()
            return None

        try:
            with PyExifTool.ExifToolHelper(common_args=["-G"]) as et:
                exif = et.get_metadata(self.media_path)[0]
        except ExifToolExecuteError as e:
            indented_message(
                "Error extracting metadata",
                str(e)
                + ", maybe the file isn't a media file"
                + (str(e.stderr) if hasattr(e, "stderr") else ""),
                3,
            )
            self.is_valid = False
            back_level()
            return None
        with PyExifTool.ExifToolHelper() as et:
            exif_codes = et.get_metadata(self.media_path)[0]

        prefixes_to_be_removed = [
            "ExifInteroperabilityOffset",
            "ExifTool:ExifToolVersion",
            "Interoperability",
            "MakerNote",
            "Tag ",
            "Thumbnail",
            "Unknown",
        ]
        for key in sorted(exif.keys()):
            if any(
                key.startswith(prefix) for prefix in prefixes_to_be_removed
            ) or isinstance(key, int):
                del exif[key]

        tags_for_codes = [
            "EXIF:GPSAltitude",
            "EXIF:GPSAltitudeRef",
            "EXIF:GPSLatitude",
            "EXIF:GPSLatitudeRef",
            "EXIF:GPSLongitude",
            "EXIF:GPSLongitudeRef",
            "EXIF:MediaDuration",
        ]
        if self.is_image:
            try:
                exif["EXIF:OrientationText"] = exif["EXIF:Orientation"]
            except KeyError:
                pass
            tags_for_codes.append("EXIF:Orientation")

        for k in tags_for_codes:
            # The output of "exiftool -n" for gps isn't easy to manage, better use the "raw" value
            try:
                exif[k] = exif_codes[k]
            except KeyError:
                pass

        validate_gps_exif()

        dirname = os.path.dirname(self.media_path)

        Options.is_decompression_bomb_error = False

        message("entered SingleMedia init", "", 5)
        self.cache_base = self.album.generate_cache_base(
            trim_base_custom(self.media_path, self.album.absolute_path),
            self.media_file_name,
        )

        self.is_valid = True

        try:
            message("reading file time and size, and dir time...", "", 6)
            mtime = file_mtime(self.media_path)
            file_size = os.path.getsize(self.media_path)
            dir_mtime = file_mtime(dirname)
            indented_message("file and dir times read!", "", 6)
        except KeyboardInterrupt:
            raise
        except OSError:
            indented_message("could not read file time or size or dir mtime", "", 6)
            self.is_valid = False
            back_level()  # OK
            return None

        self._attributes = {}
        self._attributes["metadata"] = {}
        self._attributes["dateTimeFile"] = mtime
        self._attributes["fileSize"] = file_size
        self._attributes["dateTimeDir"] = dir_mtime

        image = None

        if self.is_video:
            # try video detection
            type = self.probe_audio_video()

            if type != "video" or not self.is_valid:
                indented_message("error probing, not a video?", "", 5)
            else:
                self.detect_codec()
                # self.video_metadata_by_avconv_ffmpeg(True)
                self.audio_video_metadata_by_exiftool(exif)

                if self.is_valid:
                    message(
                        "video size, file size, codec",
                        str(self.size[0])
                        + " x "
                        + str(self.size[1])
                        + ", "
                        + humanize.naturalsize(file_size)
                        + ", "
                        + self.codec,
                        4,
                    )

        if self.is_audio:
            # try audio detection
            type = self.probe_audio_video()

            if type != "audio" or not self.is_valid:
                indented_message("error probing, not an audio?", "", 5)
            else:
                self.detect_codec()
                # self.video_metadata_by_avconv_ffmpeg(True)
                self.audio_video_metadata_by_exiftool(exif)

                if self._attributes["metadata"].get("duration", None) is not None:
                    message(
                        "audio duration and codec",
                        str(self._attributes["metadata"]["duration"])
                        + "s"
                        + ", "
                        + self.codec,
                        4,
                    )
                else:
                    message(
                        "codec",
                        self.codec + ", no duration information",
                        4,
                    )

        self._attributes["fileSizes"] = Sizes()

        if self.is_image or (not self.is_video and not self.is_audio):
            if self.is_image:
                indented_message("it's an image!", self.mime_type, 5)
            else:
                indented_message("it could be an image...", self.mime_type, 5)
            message("opening image file...", "", 5)
            next_level()  # OK
            message("opening the image with PIL...", "", 6)
            try:
                with Image.open(self.media_path) as image:
                    image.load()

                    indented_message("image opened with PIL!", "", 5)
                    if hasattr(image, "format") and image.format in Image.MIME:
                        mime_type = Image.MIME[image.format]
                    else:
                        mime_type = "image/x-unknown"
                # the following commented out line should give the same result
                # mime_type = image.get_format_mimetype()
                if mime_type != self.mime_type:
                    self.mime_type = mime_type
                    indented_message(
                        "PIL detected that it actually is an image", self.mime_type, 5
                    )
            except UnidentifiedImageError:
                indented_message("PIL error identifying the image type", "", 3)
                self.is_valid = False
                # sys.exit()
            except IOError:
                indented_message("PIL IOError opening the image", "", 3)
                self.is_valid = False
            except ValueError:
                # PIL cannot read this file (seen for .xpm file)
                # next lines will detect that the image is invalid
                indented_message("PIL ValueError opening the image", "", 3)
                self.is_valid = False
            except OSError:
                # PIL throws this exception with svg files
                indented_message(
                    "PIL OSError opening the image", "is it an svg image?", 3
                )
                self.is_valid = False
            except RuntimeError:
                # PIL throws this exception with truncated webp files
                indented_message(
                    "PIL: RuntimeError opening the image",
                    "maybe a truncated WebP image?",
                    3,
                )
                self.is_valid = False
            except AttributeError:
                # PIL throws this exception with PCD files
                indented_message(
                    "PIL: RuntimeError opening the image",
                    "maybe a PCD image?",
                    3,
                )
                self.is_valid = False
            except Image.DecompressionBombError:
                # PIL throws this exception when the image size is greater
                # than the value given in pil_size_for_decompression_bomb_error option
                limit_mb = str(
                    int(
                        Options.config["pil_size_for_decompression_bomb_error"]
                        / 1024
                        / 1024
                        * 1000
                    )
                    / 1000
                )
                indented_message(
                    "PIL: DecompressionBombError opening the image",
                    "image size is greater than " + limit_mb + "MB, not using it",
                    5,
                )
                Options.is_decompression_bomb_error = True
                self.is_valid = False

            if self.is_valid and isinstance(image, Image.Image):
                message("setting metadata extracted with exiftool", "", 6)
                self.set_image_metadata(image, exif)
                indented_message("metadata set!", "", 6)

                message(
                    "image size",
                    str(self.size[0]) + " x " + str(self.size[1]),
                    4,
                )

                image = self.transpose_image(
                    image,
                    self.media_path,
                )
                if self._attributes["metadata"]["orientation"] in (5, 6, 7, 8):
                    self._attributes["metadata"]["size"] = tuple(
                        reversed(self._attributes["metadata"]["size"])
                    )
                    # self._attributes["metadata"]["orientation"] = 1

                if Options.config["copy_exif_into_reductions"]:
                    try:
                        self.exif_by_PIL = image.info["exif"]
                    except KeyError:
                        pass

            back_level()

        back_level()
        return image

    def generate_cache_files(self, image, json_files_mtime):
        next_level()
        self.generate_types_and_names()

        if len(self.types_and_names["os copy types"]):
            type_and_name = self.types_and_names["os copy types"][0]
            # this list self.types_and_names["os copy types"] has 1 or 0 elements
            if not self.cache_file_and_symlinks_are_ok(
                "os copy types",
                type_and_name,
                "copy",
                json_files_mtime,
            ):
                # message("os copy not ok", "", 5)
                absolute_file_name = self.make_os_copy()
                touch_with_datetime(absolute_file_name)
                # pprint(type_and_name["file names"]["copy"])
                if "symlinks" in type_and_name["file names"]["copy"]:
                    for symlink_object in type_and_name["file names"]["copy"][
                        "symlinks"
                    ]:
                        symlink = symlink_object["name"]
                        absolute_symlink = os.path.join(
                            Options.config["cache_path"],
                            symlink,
                        )
                        make_dir(os.path.dirname(absolute_symlink), "symlink subdir")
                        if os.path.exists(absolute_symlink):
                            os.unlink(absolute_symlink)
                        os.symlink(absolute_file_name, absolute_symlink)
                        touch_with_datetime(absolute_symlink)

        if self.is_video:
            if not self.all_same_type_cache_files_are_ok(
                "video types",
                self.types_and_names["video types"],
                json_files_mtime,
            ):
                for type_and_name in self.types_and_names["video types"]:
                    if type_and_name["type"] == "full size video":
                        full_size = True
                    elif type_and_name["type"] == "transcoded video":
                        full_size = False
                    transcode_path = self.audio_video_transcode(
                        self.media_path,
                        json_files_mtime,
                        full_size,
                    )
                    touch_with_datetime(transcode_path)

                    if "symlinks" in type_and_name["file names"]["mp4"]:
                        for symlink_object in type_and_name["file names"]["mp4"][
                            "symlinks"
                        ]:
                            symlink = symlink_object["name"]
                            absolute_symlink = os.path.join(
                                Options.config["cache_path"],
                                symlink,
                            )
                            make_dir(
                                os.path.dirname(absolute_symlink), "symlink subdir"
                            )
                            if os.path.exists(absolute_symlink):
                                os.unlink(absolute_symlink)
                            os.symlink(absolute_file_name, absolute_symlink)
                            touch_with_datetime(absolute_symlink)

            image = self.video_frame(self.media_path, json_files_mtime)
            if image is None:
                back_level()
                return

        if self.is_audio:
            if not self.all_same_type_cache_files_are_ok(
                "audio types",
                self.types_and_names["audio types"],
                json_files_mtime,
            ):
                for type_and_name in self.types_and_names["audio types"]:
                    if type_and_name["type"] == "full size audio":
                        full_size = True
                    elif type_and_name["type"] == "transcoded audio":
                        full_size = False
                    transcode_path = self.audio_video_transcode(
                        self.media_path,
                        json_files_mtime,
                        full_size,
                    )
                    touch_with_datetime(transcode_path)

                    if "symlinks" in type_and_name["file names"]["mp3"]:
                        for symlink_object in type_and_name["file names"]["mp3"][
                            "symlinks"
                        ]:
                            symlink = symlink_object["name"]
                            absolute_symlink = os.path.join(
                                Options.config["cache_path"],
                                symlink,
                            )
                            make_dir(
                                os.path.dirname(absolute_symlink), "symlink subdir"
                            )
                            if os.path.exists(absolute_symlink):
                                os.unlink(absolute_symlink)
                            os.symlink(absolute_file_name, absolute_symlink)
                            touch_with_datetime(absolute_symlink)

            with Image.open(
                os.path.join(os.path.dirname(__file__), "../web/img/audio.png")
            ) as image:
                image.load()

            if not self.is_valid:
                back_level()
                return

        self.fill_file_sizes_structure()

        # square types and non square types
        reduced_size_images = []
        reduced_size_image = image.copy()
        is_first_square = False
        is_first_square_after_non_square = False
        type_groups = list(
            type_group_name
            for type_group_name in ("non square types", "square types")
            if type_group_name in self.types_and_names
        )
        for type_group_name in type_groups:
            if type_group_name == "square types":
                is_first_square = True
                if len(type_groups) == 2:
                    is_first_square_after_non_square = True
            message("checking " + type_group_name + "...", "", 5)
            next_level()

            if self.all_same_type_cache_files_are_ok(
                type_group_name,
                self.types_and_names[type_group_name],
                json_files_mtime,
            ):
                # read one non square reduction in order to begin reducing from a smaller image
                if type_group_name == "non square types" and len(type_groups) == 2:
                    types_and_names_without_thumbnails = [
                        type_and_name
                        for type_and_name in self.types_and_names[type_group_name]
                        if not type_and_name["type"].endswith("thumbnail")
                    ]
                    types_and_names_without_thumbnails.reverse()
                    for index, type_and_name in enumerate(
                        types_and_names_without_thumbnails
                    ):
                        # decide what non-square reduced size image to load
                        reduction_min_size = (
                            type_and_name["actual size"]
                            / max(self.size)
                            * min(self.size)
                        )
                        if (
                            index == len(types_and_names_without_thumbnails) - 1
                            or reduction_min_size
                            >= self.types_and_names["square types"][0]["actual size"]
                        ):
                            try:
                                absolute_file_name = os.path.join(
                                    Options.config["cache_path"],
                                    type_and_name["file names"]["jpg"]["name"],
                                )
                            except KeyError:
                                absolute_file_name = os.path.join(
                                    Options.config["cache_path"],
                                    self.types_and_names["os copy types"][0][
                                        "file names"
                                    ]["copy"]["name"],
                                )
                            message(
                                "reading " + type_and_name["type"] + " from disk...",
                                "",
                                6,
                            )
                            with Image.open(
                                absolute_file_name,
                            ) as reduced_size_image:
                                reduced_size_image.load()

                            reduced_size_images = [reduced_size_image]
                            indented_message(
                                type_and_name["type"] + " read from disk!",
                                str(reduced_size_image.size[0])
                                + "x"
                                + str(reduced_size_image.size[1]),
                                5,
                            )
                            break
            else:
                for type_and_name in self.types_and_names[type_group_name]:
                    size = type_and_name["actual size"]
                    if is_first_square:
                        if is_first_square_after_non_square:
                            found = False
                            # decide what non-square reduced size image use as initial image for square ones
                            # reduced_size_images are sorted ascendingly
                            for index, reduction in enumerate(reduced_size_images):
                                reduction_min_size = min(reduction.size)
                                if (
                                    index == len(reduced_size_images) - 1
                                    or reduction_min_size >= size
                                ):
                                    reduced_size_image = reduction
                                    found = True
                                    break
                            if not found:
                                reduced_size_image = image.copy()
                        else:
                            reduced_size_image = image.copy()
                        is_first_square = False

                    if size and self.all_same_type_cache_files_are_ok(
                        type_group_name,
                        self.types_and_names[type_group_name],
                        json_files_mtime,
                        size,
                    ):
                        # avoid going on with smaller sizes in this group if they all are ok
                        indented_message(
                            "all " + type_group_name + " are ok",
                            "for sizes <= " + str(size),
                            5,
                        )
                        break

                    next_level()
                    message(
                        "reducing and saving...",
                        "size "
                        + str(size)
                        + ", from size "
                        + str(max(reduced_size_image.size)),
                        4,
                    )
                    next_level()
                    [
                        reduced_size_image,
                        absolute_file_names,
                    ] = self.reduce_size_and_save(
                        reduced_size_image,
                        self.media_path,
                        type_group_name,
                        type_and_name,
                        json_files_mtime,
                        is_first_square_after_non_square,
                    )
                    back_level()
                    indented_message(
                        "reduced and saved!",
                        "",
                        4,
                    )
                    back_level()

                    if not self.is_valid:
                        back_level()
                        back_level()
                        return

                    reduced_size_images = [reduced_size_image] + reduced_size_images
                    if is_first_square_after_non_square:
                        is_first_square_after_non_square = False

            indented_message(type_group_name + " checked!", "", 5)
            back_level()

        back_level()  # OK
        return

    def all_same_type_cache_files_are_ok(
        self,
        type_group_name,
        type_group,
        json_files_mtime,
        max_size=None,
    ):
        for type_and_name in type_group:
            if max_size is not None and type_and_name["actual size"] > max_size:
                continue

            if any(
                not self.cache_file_and_symlinks_are_ok(
                    type_group_name,
                    type_and_name,
                    format,
                    json_files_mtime,
                )
                for format in type_and_name["file names"]
            ):
                message(
                    "some cache files not ok",
                    "type: '" + type_group_name + "'",
                    5,
                )
                return False
        message("all cache files ok", "type: '" + type_group_name + "'", 5)
        return True

    @property
    def datetime_file_modified(self):
        return self._attributes["dateTimeFile"]

    @property
    def file_size(self):
        return self._attributes["fileSize"]

    @property
    def file_sizes(self):
        return self._attributes["fileSizes"]

    @property
    def datetime_dir(self):
        return self._attributes["dateTimeDir"]

    def get_geonames(self):
        self._attributes["geoname"] = Geonames.lookup_nearby_place(
            self.latitude, self.longitude
        )
        # self._attributes["geoname"] is a dictionary with this data:
        #  'country_name': the country name in given language
        #  'country_code': the ISO country code
        #  'region_name': the administrative name (the region in normal states, the state in federative states) in given language
        #  'region_code': the corresponding geonames code
        #  'place_name': the nearby place name
        #  'place_code': the nearby place geonames id
        #  'distance': the distance between given coordinates and nearby place geonames coordinates

        # Overwrite with album.ini values when album has been read from file
        if self.album.album_ini:
            Metadata.set_geoname_from_album_ini(
                self.name, self._attributes, self.album.album_ini
            )

    def generate_protected_directory_and_symlinks(
        self, complex_identifiers_combination
    ):
        # determines where the protected media chache files have to be saved

        (
            album_identifiers_combination,
            media_identifiers_combination,
        ) = complex_identifiers_combination.split(",")

        if (
            album_identifiers_combination != ""
            and len(self.album.password_identifiers_set) > 0
            and set(album_identifiers_combination.split("-"))
            != self.album.password_identifiers_set
        ):
            # execution shouldn't arrive here
            indented_message(
                "album protected by another password combination, not "
                + action
                + " it",
                self.album.absolute_path,
                4,
            )
            return

        [md5_product_list, codes_complex_combination] = md5_products(
            complex_identifiers_combination
        )

        first_md5_product = md5_product_list[0]

        self.first_protected_directory = (
            Options.config["protected_directories_prefix"] + first_md5_product
        )
        first_dir = os.path.join(
            Options.config["protected_directories_prefix"] + first_md5_product,
            Options.config["cache_path"],
            self.first_protected_directory,
        )

        create_complex_dir_and_subdirs(first_dir)

        # symlinks must be generated in order to save the media with 2 or more passwords
        if len(md5_product_list) > 1:
            for complex_md5 in md5_product_list[1:]:
                symlink = Options.config["protected_directories_prefix"] + complex_md5
                complex_dir = os.path.join(
                    Options.config["cache_path"],
                    symlink,
                )

                create_complex_dir_and_subdirs(complex_dir)

                self.protected_directory_symlinks.append(symlink)

    def set_image_metadata(self, image, exif):
        message("setting metadata extracted with exiftool", "", 6)

        self._attributes["metadata"]["size"] = image.size
        # self._orientation_original = 1

        # Orientation codes:
        # 1 = 0 degrees: the correct orientation, no adjustment is required.
        # 2 = 0 degrees, mirrored: image has been flipped back-to-front.
        # 3 = 180 degrees: image is upside down.
        # 4 = 180 degrees, mirrored: image has been flipped back-to-front and is upside down.
        # 5 = 90 degrees CW: image has been flipped back-to-front and is on its side.
        # 6 = 90 degrees CW, mirrored: image is on its side.
        # 7 = 270 degrees CW: image has been flipped back-to-front and is on its far side.
        # 8 = 270 degrees CW, mirrored: image is on its far side.

        # if "EXIF:Rotation" in exif and not "EXIF:Orientation" in exif:
        #     rotation = exif["EXIF:Rotation"]
        #     if type(rotation) == type("string"):
        #         parts = rotation.split()  # Split the string into a list of words
        #         for part in parts:
        #             if part.isdigit():
        #                 rotation = int(part)
        #                 break
        #
        #     if rotation == 0:
        #         exif["EXIF:Orientation"] = 1
        #     if rotation == 90:
        #         exif["EXIF:Orientation"] = 5
        #     if rotation == 180:
        #         exif["EXIF:Orientation"] = 3
        #     if rotation == 270:
        #         exif["EXIF:Orientation"] = 7

        if "EXIF:Orientation" in exif and exif["EXIF:Orientation"] not in range(1, 9):
            exif["EXIF:Orientation"] = 1
            exif["EXIF:OrientationText"] = "Horizontal (normal)"

        try:
            exif["EXIF:Orientation"]
        except KeyError:
            exif["EXIF:Orientation"] = 1
            exif["EXIF:OrientationText"] = "Horizontal (normal)"

        # orientation_list = [
        #     "Horizontal (normal)",
        #     "Mirror horizontal",
        #     "Rotate 180",
        #     "Mirror vertical",
        #     "Mirror horizontal and rotate 270 CW",
        #     "Rotate 90 CW",
        #     "Mirror horizontal and rotate 90 CW",
        #     "Rotate 270 CW",
        # ]
        self._attributes["metadata"]["orientation"] = exif["EXIF:Orientation"]
        self._attributes["metadata"]["orientationText"] = exif["EXIF:OrientationText"]
        # if exif["EXIF:Orientation"] - 1 < len(orientation_list):
        #     self._attributes["metadata"]["orientationText"] = orientation_list[
        #         exif["EXIF:Orientation"] - 1
        #     ]

        # self._orientation_original = exif["EXIF:Orientation"]

        if "EXIF:Make" in exif:
            self._attributes["metadata"]["make"] = exif["EXIF:Make"]
        if "EXIF:Model" in exif:
            self._attributes["metadata"]["model"] = exif["EXIF:Model"]
        if "EXIF:ApertureValue" in exif:
            self._attributes["metadata"]["aperture"] = exif["EXIF:ApertureValue"]
        elif "EXIF:FNumber" in exif:
            self._attributes["metadata"]["aperture"] = exif["EXIF:FNumber"]
        if "EXIF:FocalLength" in exif:
            self._attributes["metadata"]["focalLength"] = exif["EXIF:FocalLength"]
        if "EXIF:ISO" in exif:
            self._attributes["metadata"]["iso"] = exif["EXIF:ISO"]
        if "EXIF:ISOSettings" in exif:
            self._attributes["metadata"]["iso"] = exif["EXIF:ISOSettings"]
        if "EXIF:ISOSpeedRatings" in exif:
            self._attributes["metadata"]["iso"] = exif["EXIF:ISOSpeedRatings"]
        if "EXIF:MakerNote ISO" in exif:
            self._attributes["metadata"]["iso"] = exif["EXIF:MakerNote ISO"]
        if "EXIF:PhotographicSensitivity" in exif:
            self._attributes["metadata"]["iso"] = exif["EXIF:PhotographicSensitivity"]
        if "EXIF:ExposureTime" in exif:
            self._attributes["metadata"]["exposureTime"] = exif["EXIF:ExposureTime"]
        if "EXIF:Flash" in exif:
            self._attributes["metadata"]["flash"] = exif["EXIF:Flash"]
        if "EXIF:LightSource" in exif:
            self._attributes["metadata"]["lightSource"] = exif["EXIF:LightSource"]
        if "EXIF:ExposureProgram" in exif:
            self._attributes["metadata"]["exposureProgram"] = exif[
                "EXIF:ExposureProgram"
            ]
        if "EXIF:SpectralSensitivity" in exif:
            self._attributes["metadata"]["spectralSensitivity"] = exif[
                "EXIF:SpectralSensitivity"
            ]
        if "EXIF:MeteringMode" in exif:
            self._attributes["metadata"]["meteringMode"] = exif["EXIF:MeteringMode"]
        if "EXIF:SensingMethod" in exif:
            self._attributes["metadata"]["sensingMethod"] = exif["EXIF:SensingMethod"]
        if "EXIF:SceneCaptureType" in exif:
            self._attributes["metadata"]["sceneCaptureType"] = exif[
                "EXIF:SceneCaptureType"
            ]
        if "EXIF:SubjectDistanceRange" in exif:
            self._attributes["metadata"]["subjectDistanceRange"] = exif[
                "EXIF:SubjectDistanceRange"
            ]
        if "EXIF:ExposureCompensation" in exif:
            self._attributes["metadata"]["exposureCompensation"] = exif[
                "EXIF:ExposureCompensation"
            ]
        if "EXIF:ExposureBiasValue" in exif:
            self._attributes["metadata"]["exposureCompensation"] = exif[
                "EXIF:ExposureBiasValue"
            ]

        if Options.config["expose_image_dates"] and "EXIF:DateTimeOriginal" in exif:
            try:
                self._attributes["metadata"]["dateTime"] = datetime.strptime(
                    exif["EXIF:DateTimeOriginal"], Options.timestamp_format_for_exif
                )
            except ValueError:
                if "EXIF:DateTimeDigitized" in exif:
                    try:
                        self._attributes["metadata"]["dateTime"] = datetime.strptime(
                            exif["EXIF:DateTimeDigitized"],
                            Options.timestamp_format_for_exif,
                        )
                    except ValueError:
                        # value isn't usable, forget it
                        if "EXIF:DateTime" in exif:
                            try:
                                self._attributes["metadata"]["dateTime"] = (
                                    datetime.strptime(
                                        exif["EXIF:DateTime"],
                                        Options.timestamp_format_for_exif,
                                    )
                                )
                            except ValueError:
                                # value isn't usable, forget it
                                pass

        if Options.config["expose_image_positions"]:
            self.set_gps_data(exif)

        # Overwrite with album.ini values when it has been read from file
        if self.album.album_ini:
            next_level()  # OK
            message("adding album.ini metadata values to image...", "", 5)
            Metadata.set_metadata_from_album_ini(self.name, self)
            indented_message("metadata values from album.ini added to image...", "", 5)
            back_level()  # OK

        indented_message("metadata set!", "", 6)

    def set_gps_data(self, exif):
        gps_altitude = None
        gps_altitude_ref = None
        if "EXIF:GPSAltitude" in exif:
            gps_altitude = exif["EXIF:GPSAltitude"]
            if "EXIF:GPSAltitudeRef" in exif:
                gps_altitude_ref = exif["EXIF:GPSAltitudeRef"]

        gps_latitude = None
        gps_latitude_ref = None
        if "EXIF:GPSLatitude" in exif:
            gps_latitude = exif["EXIF:GPSLatitude"]
            if "EXIF:GPSLatitudeRef" in exif:
                gps_latitude_ref = exif["EXIF:GPSLatitudeRef"]

        gps_longitude = None
        gps_longitude_ref = None
        if "EXIF:GPSLongitude" in exif:
            gps_longitude = exif["EXIF:GPSLongitude"]
            if "EXIF:GPSLongitudeRef" in exif:
                gps_longitude_ref = exif["EXIF:GPSLongitudeRef"]

        # Issue https://gitlab.com/paolobenve/myphotoshare/-/issues/218
        infinitesimal = 0.00001
        if (
            gps_latitude is not None
            and gps_longitude is not None
            and abs(gps_latitude) < infinitesimal
            and abs(gps_longitude) < infinitesimal
        ):
            gps_latitude = None
            gps_latitude_ref = None
            gps_longitude = None
            gps_longitude_ref = None
        # if gps_altitude is not None and	gps_altitude < infinitesimal:
        # 	gps_altitude = None
        # 	gps_altitude_ref = None

        if (
            gps_latitude is not None
            and gps_latitude_ref is not None
            and gps_longitude is not None
            and gps_longitude_ref is not None
        ):
            self._attributes["metadata"]["latitude"] = gps_latitude
            if gps_latitude_ref is not None and gps_latitude_ref == "S":
                self._attributes["metadata"]["latitude"] = -self._attributes[
                    "metadata"
                ]["latitude"]
            self._attributes["metadata"]["latitudeMS"] = (
                Metadata.convert_decimal_to_degrees_minutes_seconds(
                    gps_latitude, gps_latitude_ref
                )
            )

            self._attributes["metadata"]["longitude"] = gps_longitude
            if gps_longitude_ref is not None and gps_longitude_ref == "W":
                self._attributes["metadata"]["longitude"] = -self._attributes[
                    "metadata"
                ]["longitude"]
            self._attributes["metadata"]["longitudeMS"] = (
                Metadata.convert_decimal_to_degrees_minutes_seconds(
                    gps_longitude, gps_longitude_ref
                )
            )
        if gps_altitude is not None:
            # if gps_altitude is not None and gps_altitude_ref is not None:
            self._attributes["metadata"]["altitude"] = gps_altitude
            if gps_altitude_ref is not None and gps_altitude_ref != 0:
                self._attributes["metadata"]["altitude"] = -self._attributes[
                    "metadata"
                ]["altitude"]

        return

    def audio_video_metadata_by_exiftool(self, exif):
        def get_duration():
            duration = None
            for duration_tag in [
                "QuickTime:Duration",
                "Matroska:Duration",
                "Composite:Duration",
            ]:
                try:
                    duration = exif[duration_tag]
                    break
                except KeyError:
                    continue
                except ValueError:
                    pass

            return duration

        def get_video_rotation():
            rotation = None
            for rotation_tag in [
                "QuickTime:Rotation",
                "Composite:Rotation",
                # "EXIF:Rotate",
            ]:
                try:
                    rotation = exif[rotation_tag]
                    break
                except KeyError:
                    continue

            return rotation

        def get_date_time_from_file_name(file_name):
            punctuation_re = "[^0-9a-zA-Z]*"
            date_time_re = (
                "[0-2][0-9]{3}"  # year
                + punctuation_re
                + "(?!00)[0-1][0-9]"  # month
                + punctuation_re
                + "(?!00)[0-3][0-9]"  # day
                + punctuation_re
                + "[0-2][0-9]"  # hours
                + punctuation_re
                + "[0-6][0-9]"  # minutes
                + punctuation_re
                + "[0-6][0-9]"  # seconds
            )
            match = re.search(date_time_re, file_name)
            if match is None:
                message(
                    "no date",
                    "in '" + file_name + "'",
                    5,
                )
                return None
            else:
                date_time = match.group(0)

                # remove punctuation
                date_time_without_separators = "".join(
                    [c for c in date_time if c.isdigit()]
                )
                try:
                    date_time_object = datetime.strptime(
                        date_time_without_separators,
                        Options.timestamp_format_without_separators,
                    )
                    message(
                        "date read from file name!",
                        str(date_time_object) + ", file name: '" + file_name + "'",
                        5,
                    )
                    return date_time_object
                except ValueError:
                    message(
                        "wrong date in file name!",
                        date_time + ", from file name '" + file_name + "'",
                        5,
                    )
                    return None

        def get_date_time_from_metadata(exif):
            tag = ""
            found = False
            try:
                tz_tag = ""
                try:
                    tz_tag = "EXIF:OffsetTime"
                    tz = exif["EXIF:OffsetTime"]
                except KeyError:
                    tz_tag = "EXIF:TimeZone"
                    tz = exif["EXIF:TimeZone"]
                    # tz = "".join(["" if c == ":" else c for c in tz])
                tag = "EXIF:MediaCreateDate"
                tag_with_tz = tag + " + " + tz_tag
                date_time = exif[tag] + tz
                date_time_creation = datetime.strptime(
                    date_time, Options.timestamp_format_for_exif_with_TZ
                ).replace(tzinfo=None)
                message(
                    "date read from metadata!",
                    str(date_time_creation) + ", from " + tag + " tag",
                    5,
                )
                found = True
            except KeyError:
                pass
            except ValueError:
                message("invalid date", exif[tag] + " in " + tag + " tag", 5)
                pass

            if not found:
                for tag in ["QuickTime:CreateDate", "EXIF:CreateDate"]:
                    try:
                        date_time_creation = datetime.strptime(
                            exif[tag],
                            Options.timestamp_format_for_exif,
                        )
                        message(
                            "date read from metadata!",
                            exif[tag] + ", from " + tag + " tag",
                            5,
                        )
                        found = True
                    except KeyError:
                        continue
                    except ValueError:
                        message(
                            "invalid date",
                            exif[tag] + " in " + tag + " tag",
                            5,
                        )
                        continue

            if not found:
                message(
                    "no creation date tag found",
                    "file modified date will be used",
                    4,
                )
                return None

            return date_time_creation

        # end of nested functions

        if Options.config["expose_image_dates"]:
            for method in Options.config["audio_video_date_look_for_preference"]:
                if method == "file_modification_date":
                    date_time = self.datetime_file_modified
                    info = "file modification date time"
                elif method == "file_name":
                    date_time = get_date_time_from_file_name(self.name)
                    info = "file name"
                elif method == "metadata":
                    date_time = get_date_time_from_metadata(exif)
                    info = "metadata"

                if date_time is not None:
                    break

            message("using date/time from " + info, "", 5)
            self._attributes["metadata"]["dateTimeCreation"] = date_time

        self._attributes["metadata"]["duration"] = get_duration()

        if self.is_video:
            rotation = get_video_rotation()
            if rotation is not None:
                self._attributes["metadata"]["rotation"] = rotation

            for size_tags in [
                ["QuickTime:ImageWidth", "QuickTime:ImageHeight"],
                ["QuickTime:SourceImageWidth", "QuickTime:SourceImageHeight"],
                ["MPEG:ImageWidth", "MPEG:ImageHeight"],
                ["Matroska:ImageWidth", "Matroska:ImageHeight"],
                ["File:ImageWidth", "File:ImageHeight"],
                ["ASF:ImageWidth", "ASF:ImageHeight"],
            ]:
                try:
                    width = exif[size_tags[0]]
                    height = exif[size_tags[1]]
                    break
                except KeyError:
                    continue

            try:
                width
            except NameError:
                widthxheight = exif["Composite:ImageSize"]
                width, height = widthxheight.split("x")

            if rotation is not None and rotation not in (0, 180):
                width, height = height, width
            self._attributes["metadata"]["size"] = [width, height]

            self._attributes["metadata"]["originalSize"] = self._attributes["metadata"][
                "size"
            ]

        if Options.config["expose_image_positions"]:
            self.set_gps_data(exif)

        # Video should also contain metadata like GPS information, at least in QuickTime and MP4 files...
        if self.album.album_ini:
            next_level()  # OK
            message(
                "adding album.ini metadata values to "
                + ("video" if self.is_video else "audio")
                + "...",
                "",
                5,
            )
            Metadata.set_metadata_from_album_ini(self.name, self)
            indented_message(
                "metadata values from album.ini added to "
                + ("video" if self.is_video else "audio")
                + "!",
                "",
                5,
            )
            back_level()  # OK

    def detect_codec(self):
        message(
            "detecting " + ("video" if self.is_video else "audio") + " codec...", "", 5
        )
        try:
            arguments = [
                "-v",
                "error",
                "-select_streams",
                ("v" if self.is_video else "a") + ":0",
                "-show_entries",
                "stream=codec_name",
                "-of",
                "default=noprint_wrappers=1:nokey=1",
                self.media_path,
            ]
            return_code = VideoProbeCaller().call(*arguments)
        except KeyboardInterrupt:
            raise

        if return_code == False:
            indented_message(
                "couldn't  detect "
                + ("video" if self.is_video else "audio")
                + " codec",
                os.path.basename(self.media_path),
                1,
            )
            self.add_error(
                "other_errors",
                self.media_path,
                "",
                "",
                "",
                "coudn't detect codec",
            )

            self.is_valid = False
            return None

        # the output of ffprobe is a sequence of bytes, convert to string
        codec = return_code.decode("utf-8")
        # there is a traling \n, remove it
        self.codec = codec[: len(codec) - 1]

        indented_message("codec detected!", self.codec, 4)

    def probe_audio_video(self):
        message("probing " + ("video" if self.is_video else "audio"), "", 5)
        try:
            arguments = [
                "-loglevel",
                "0",
                "-show_entries",
                "stream=codec_type",
                "-of",
                "csv=p=0",
                self.media_path,
            ]
            return_code = VideoProbeCaller().call(*arguments)
        except KeyboardInterrupt:
            raise

        if return_code == False:
            indented_message(
                "couldn't  probe " + ("video" if self.is_video else "audio"),
                "",
                1,
            )
            self.add_error(
                "other_errors",
                self.media_path,
                "",
                "",
                "",
                "coudn't probe " + ("video" if self.is_video else "audio"),
            )

            self.is_valid = False
            return None

        # the output of ffprobe is a sequence of bytes, convert to string
        type = return_code.decode("utf-8")
        if not type in ["audio", "video"] and type.find("video") > -1:
            type = "video"
        elif not type in ["audio", "video"] and type.find("audio") > -1:
            type = "audio"
        if not type or not type in ["audio", "video"]:
            indented_message(
                "error probing, '" + type + "' != 'audio', 'video'", self.media_path, 4
            )
            self.add_error(
                "other_errors",
                self.media_path,
                "",
                "",
                "",
                "probed media is not audio nor video",
            )

            self.is_valid = False
            return None
        indented_message(type + " OK!", self.media_path, 4)
        return type

    # def video_metadata_by_avconv_ffmpeg(self, get_original_size):
    #     message("estracting metadata from video", self.media_path, 4)
    #     return_code = VideoProbeCaller().call(
    #         "-show_format",
    #         "-show_entries",
    #         "stream_tags=creation_time",
    #         "-show_streams",
    #         "-of",
    #         "json",
    #         "-loglevel",
    #         "0",
    #         self.media_path,
    #     )
    #
    #     info = json.loads(return_code.decode(sys.getdefaultencoding()))
    #     for s in info["streams"]:
    #         if "codec_type" in s:
    #             indented_message("debug: s[codec_type]", s["codec_type"], 5)
    #         if "codec_type" in s and s["codec_type"] == "video":
    #             self._attributes["metadata"]["size"] = (
    #                 int(s["width"]),
    #                 int(s["height"]),
    #             )
    #             if "duration" in s:
    #                 self._attributes["metadata"]["duration"] = int(
    #                     round(float(s["duration"]) * 10) / 10
    #                 )
    #             if "tags" in s and "rotate" in s["tags"]:
    #                 self._attributes["metadata"]["rotate"] = s["tags"]["rotate"]
    #             if get_original_size:
    #                 self._attributes["metadata"]["originalSize"] = (
    #                     int(s["width"]),
    #                     int(s["height"]),
    #                 )
    #             break
    #     if "tags" in info["format"] and "creation_time" in info["format"]["tags"]:
    #         # .avi files haven't the "tags" key
    #         # TO DO: creation time is in UTC: how do I get the TZ????
    #         try:
    #             self._attributes["metadata"]["dateTimeCreation"] = datetime.strptime(
    #                 info["format"]["tags"]["creation_time"],
    #                 Options.timestamp_format_for_videos,
    #             )
    #         except ValueError:
    #             self._attributes["metadata"]["dateTimeCreation"] = datetime.strptime(
    #                 info["format"]["tags"]["creation_time"],
    #                 Options.timestamp_format_for_videos_ending_in_z,
    #             )
    #
    #     # Video should also contain metadata like GPS information, at least in QuickTime and MP4 files...
    #     if self.album.album_ini:
    #         next_level()  # OK
    #         message("adding album.ini metadata values to video...", "", 5)
    #         Metadata.set_metadata_from_album_ini(self.name, self)
    #         indented_message("metadata values from album.ini added to video...", "", 5)
    #         back_level()  # OK

    def transpose_image(
        self,
        image,
        image_path,
    ):
        # give image the correct orientation
        try:
            message(
                "image orientation",
                "transposing the image according to exif orientation tag",
                5,
            )
            transposed_image = ImageOps.exif_transpose(image)
            return transposed_image
        except IOError:
            # https://gitlab.com/paolobenve/myphotoshare/issues/46: some image may raise this exception
            message("WARNING: Image couldn't be trasposed", image_path, 2)
            return image

    @staticmethod
    def collect_types_from_type_groups(type_groups_to_be_added):
        collected_types = []
        actual_sizes_added = []
        for type_group in type_groups_to_be_added:
            for type_and_name in type_group:
                if (
                    len(type_and_name["file names"])
                    and not type_and_name["actual size"] in actual_sizes_added
                ):
                    collected_types.append(type_and_name)
                    actual_sizes_added.append(type_and_name["actual size"])

        # sort by size
        collected_types.sort(key=lambda _type: _type["actual size"])
        collected_types.reverse()

        return collected_types

    def generate_thumbnail_types_and_sizes(self, cache_subdir, cache_symlinks_subdirs):
        # collect all the sizes needed for the thumbnail of either the video or the image
        # "actual size": the size that takes into account mobile_bigger, fixed_height:
        #                is the size that the original image will be reduced to

        square_types = []
        non_square_types = []

        is_square = (
            self.is_audio
            or self.size[0] == self.size[1]
            or Options.config["only_square_thumbnails"]
        )

        cache_images_formats = cache_image_formats_ending_with_jpg()

        album_prefix = remove_folders_marker(self.album.cache_base)
        if album_prefix:
            album_prefix += Options.config["cache_folder_separator"]

        album_corrected_size = Options.config["album_thumb_size"]

        media_corrected_size = Options.config["media_thumb_size"]
        if self.is_audio:
            media_size_for_fixed_height = media_corrected_size
        else:
            media_size_for_fixed_height = int(
                round(
                    media_corrected_size
                    if self.size[0] <= self.size[1]
                    else media_corrected_size / self.size[1] * self.size[0]
                )
            )
        for mobile_bigger in (False, True):
            if self.is_audio and mobile_bigger:
                continue
            if mobile_bigger:
                if Options.config["mobile_thumbnail_factor"] > 1:
                    album_corrected_size = int(
                        round(
                            Options.config["mobile_thumbnail_factor"]
                            * album_corrected_size
                        )
                    )

                    media_corrected_size = int(
                        round(
                            Options.config["mobile_thumbnail_factor"]
                            * media_corrected_size
                        )
                    )
                    media_size_for_fixed_height = int(
                        round(
                            Options.config["mobile_thumbnail_factor"]
                            * media_size_for_fixed_height
                        )
                    )
                else:
                    continue

            by_format = {
                "type": "album_square_thumbnail",
                "size": Options.config["album_thumb_size"],
                "mobile bigger": mobile_bigger,
                "actual size": album_corrected_size,
                "file names": {},
            }
            for format in cache_images_formats:
                by_format["file names"][format] = {
                    "name": os.path.join(
                        cache_subdir,
                        album_prefix
                        + image_cache_name(
                            self,
                            Options.config["album_thumb_size"],
                            format,
                            "album_square",
                            mobile_bigger,
                        ),
                    ),
                    "ok": None,
                }
                if len(cache_symlinks_subdirs):
                    by_format["file names"][format]["symlinks"] = []
                    for cache_symlink in cache_symlinks_subdirs:
                        by_format["file names"][format]["symlinks"].append(
                            {
                                "name": os.path.join(
                                    cache_symlink,
                                    album_prefix
                                    + image_cache_name(
                                        self,
                                        Options.config["album_thumb_size"],
                                        format,
                                        "album_square",
                                        mobile_bigger,
                                    ),
                                ),
                                "ok": None,
                            }
                        )
            square_types.append(by_format)

            by_format = {
                "type": "media_square_thumbnail",
                "size": Options.config["media_thumb_size"],
                "actual size": media_corrected_size,
                "mobile bigger": mobile_bigger,
                "file names": {},
            }
            for format in cache_images_formats:
                by_format["file names"][format] = {
                    "name": os.path.join(
                        cache_subdir,
                        album_prefix
                        + image_cache_name(
                            self,
                            Options.config["media_thumb_size"],
                            format,
                            "media_square",
                            mobile_bigger,
                        ),
                    ),
                    "ok": None,
                }
                if len(cache_symlinks_subdirs):
                    by_format["file names"][format]["symlinks"] = []
                    for cache_symlink in cache_symlinks_subdirs:
                        by_format["file names"][format]["symlinks"].append(
                            {
                                "name": os.path.join(
                                    cache_symlink,
                                    album_prefix
                                    + image_cache_name(
                                        self,
                                        Options.config["media_thumb_size"],
                                        format,
                                        "media_square",
                                        mobile_bigger,
                                    ),
                                ),
                                "ok": None,
                            }
                        )

            square_types.append(by_format)

            if not is_square:
                by_format = {
                    "type": "album_fit_thumbnail",
                    "size": Options.config["album_thumb_size"],
                    "actual size": album_corrected_size,
                    "mobile bigger": mobile_bigger,
                    "file names": {},
                }
                for format in cache_images_formats:
                    by_format["file names"][format] = {
                        "name": os.path.join(
                            cache_subdir,
                            album_prefix
                            + image_cache_name(
                                self,
                                Options.config["album_thumb_size"],
                                format,
                                "album_fit",
                                mobile_bigger,
                            ),
                        ),
                        "ok": None,
                    }
                    if len(cache_symlinks_subdirs):
                        by_format["file names"][format]["symlinks"] = []
                        for cache_symlink in cache_symlinks_subdirs:
                            by_format["file names"][format]["symlinks"].append(
                                {
                                    "name": os.path.join(
                                        cache_symlink,
                                        album_prefix
                                        + image_cache_name(
                                            self,
                                            Options.config["album_thumb_size"],
                                            format,
                                            "album_fit",
                                            mobile_bigger,
                                        ),
                                    ),
                                    "ok": None,
                                }
                            )
                non_square_types.append(by_format)

                by_format = {
                    "type": "media_fixed_height_thumbnail",
                    "size": Options.config["media_thumb_size"],
                    "actual size": media_size_for_fixed_height,
                    "mobile bigger": mobile_bigger,
                    "file names": {},
                }
                for format in cache_images_formats:
                    by_format["file names"][format] = {
                        "name": os.path.join(
                            cache_subdir,
                            album_prefix
                            + image_cache_name(
                                self,
                                Options.config["media_thumb_size"],
                                format,
                                "media_fixed_height",
                                mobile_bigger,
                            ),
                        ),
                        "ok": None,
                    }
                    if len(cache_symlinks_subdirs):
                        by_format["file names"][format]["symlinks"] = []
                        for cache_symlink in cache_symlinks_subdirs:
                            by_format["file names"][format]["symlinks"].append(
                                {
                                    "name": os.path.join(
                                        cache_symlink,
                                        album_prefix
                                        + image_cache_name(
                                            self,
                                            Options.config["media_thumb_size"],
                                            format,
                                            "media_fixed_height",
                                            mobile_bigger,
                                        ),
                                    ),
                                    "ok": None,
                                }
                            )
                non_square_types.append(by_format)

        return square_types, non_square_types

    def generate_types_and_names(self):
        # generates the copies/reductions/thumbnails types for the given media

        if hasattr(self, "types_and_names"):
            return

        self.first_protected_directory = None
        self.protected_directory_symlinks = []
        self.complex_identifiers_combination = complex_combination(
            convert_set_to_combination(self.album_identifiers_set),
            convert_set_to_combination(self.password_identifiers_set),
        )
        if len(self.album_identifiers_set) or len(self.password_identifiers_set):
            self.generate_protected_directory_and_symlinks(
                self.complex_identifiers_combination
            )

        cache_images_formats = cache_image_formats_ending_with_jpg()

        album_prefix = remove_folders_marker(self.album.cache_base)
        if album_prefix:
            album_prefix += Options.config["cache_folder_separator"]

        self.calculate_image_size_for_sharing()
        if self.is_audio:
            if Options.config["reduced_size_for_sharing"]:
                size = Options.config["reduced_size_for_sharing"]
            else:
                size = Options.config["album_thumb_size"]
        else:
            size = max(self.size)

        square_sharing_types = []
        non_square_sharing_types = []
        os_copy_types = []
        SingleMedia.file_names_added = []

        cache_subdir = self.album.subdir
        cache_symlinks_subdirs = []
        if self.first_protected_directory is not None:
            cache_subdir = os.path.join(
                self.first_protected_directory, self.album.subdir
            )
            cache_symlinks_subdirs = [
                os.path.join(symlink, self.album.subdir)
                for symlink in self.protected_directory_symlinks
            ]

        if self.is_audio:
            audio_types = []
            # extension = self.media_path[self.media_path.rfind(".") + 1 :]
            if (
                Options.config["expose_original_media"]
                and not original_media_is_enough(self)
                or Options.config["expose_full_size_media_in_cache"]
            ):
                # a full size audio is needed in cache
                if (
                    Options.config["expose_full_size_media_in_cache"]
                    and self.mime_type
                    not in Options.config["browser_unsupported_mime_types"]
                    and self.codec.lower()
                    not in map(
                        lambda x: x.lower(),
                        Options.config["browser_unsupported_audio_codecs"],
                    )
                ):
                    # os copy
                    audio_name = audio_cache_copy_name(self)
                    os_copy_type = {
                        "type": "os copy",
                        "size": size,
                        "actual size": size,
                        "file names": {
                            "copy": {
                                "name": os.path.join(
                                    cache_subdir,
                                    album_prefix + audio_name,
                                ),
                                "ok": None,
                            }
                        },
                    }
                    if len(cache_symlinks_subdirs):
                        os_copy_type["file names"]["copy"]["symlinks"] = []
                        for cache_symlink in cache_symlinks_subdirs:
                            os_copy_type["file names"]["copy"]["symlinks"].append(
                                {
                                    "name": os.path.join(
                                        cache_symlink,
                                        album_prefix + audio_name,
                                    ),
                                    "ok": None,
                                }
                            )
                    os_copy_types.append(os_copy_type)
                else:
                    # transcoded full size audio
                    audio_name = audio_full_size_transcoded_name(self)
                    full_type = {
                        "type": "full size audio",
                        "size": size,
                        "actual size": size,
                        "file names": {
                            "mp3": {
                                "name": os.path.join(
                                    cache_subdir,
                                    album_prefix + audio_name,
                                ),
                                "ok": None,
                            }
                        },
                    }
                    if len(cache_symlinks_subdirs):
                        full_type["file names"]["mp3"]["symlinks"] = []
                        for cache_symlink in cache_symlinks_subdirs:
                            full_type["file names"]["mp3"]["symlinks"].append(
                                {
                                    "name": os.path.join(
                                        cache_symlink,
                                        album_prefix + audio_name,
                                    ),
                                    "ok": None,
                                }
                            )
                    audio_types.append(full_type)

            # transcoded reduced audio
            audio_name = audio_transcoded_name(self)
            transcoded_type = {
                "type": "transcoded audio",
                "size": size,
                "actual size": size,
                "file names": {
                    "mp3": {
                        "name": os.path.join(
                            cache_subdir,
                            album_prefix + audio_name,
                        ),
                        "ok": None,
                    }
                },
            }
            if len(cache_symlinks_subdirs):
                transcoded_type["file names"]["mp3"]["symlinks"] = []
                for cache_symlink in cache_symlinks_subdirs:
                    transcoded_type["file names"]["mp3"]["symlinks"].append(
                        {
                            "name": os.path.join(
                                cache_symlink,
                                album_prefix + audio_name,
                            ),
                            "ok": None,
                        }
                    )
            audio_types.append(transcoded_type)

            # # image for sharing on social media
            # sharing_types.append(
            #     {
            #         "type": "reduction for sharing",
            #         "size": Options.config["reduced_size_for_sharing"],
            #         "actual size": Options.config["reduced_size_for_sharing"],
            #         "file names": {
            #             "jpg": {
            #                 "name": os.path.join(
            #                     cache_subdir,
            #                     album_prefix
            #                     + image_cache_name(
            #                         self, self.image_size_for_sharing, "jpg"
            #                     ),
            #                 ),
            #                 "ok": None,
            #             }
            #         },
            #     }
            # )

            # (
            #     square_thumbnail_types,
            #     non_square_thumbnail_types,
            # ) = self.generate_thumbnail_types_and_sizes(cache_subdir, cache_symlinks_subdirs)

            # square_types = SingleMedia.collect_types_from_type_groups(
            #     [
            #         sharing_types,
            #         square_thumbnail_types,
            #     ]
            # )

            types_and_names = {
                "os copy types": os_copy_types,
                "audio types": audio_types,
                # "square types": square_types,
            }
        elif self.is_video:
            video_types = []
            # extension = self.media_path[self.media_path.rfind(".") + 1 :]
            if (
                Options.config["expose_original_media"]
                and not original_media_is_enough(self)
                or Options.config["expose_full_size_media_in_cache"]
            ):
                # a full size video is needed in cache
                if (
                    Options.config["expose_full_size_media_in_cache"]
                    and self.mime_type
                    not in Options.config["browser_unsupported_mime_types"]
                    and self.codec.lower()
                    not in map(
                        lambda x: x.lower(),
                        Options.config["browser_unsupported_video_codecs"],
                    )
                ):
                    # os copy
                    video_name = video_cache_copy_name(self)
                    os_copy_type = {
                        "type": "os copy",
                        "size": size,
                        "actual size": size,
                        "file names": {
                            "copy": {
                                "name": os.path.join(
                                    cache_subdir,
                                    album_prefix + video_name,
                                ),
                                "ok": None,
                            }
                        },
                    }
                    if len(cache_symlinks_subdirs):
                        os_copy_type["file names"]["copy"]["symlinks"] = []
                        for cache_symlink in cache_symlinks_subdirs:
                            os_copy_type["file names"]["copy"]["symlinks"].append(
                                {
                                    "name": os.path.join(
                                        cache_symlink, album_prefix + video_name
                                    ),
                                    "ok": None,
                                }
                            )

                    os_copy_types.append(os_copy_type)
                else:
                    # transcoded full size video
                    video_name = video_full_size_transcoded_name(self)
                    video_type = {
                        "type": "full size video",
                        "size": size,
                        "actual size": size,
                        "file names": {
                            "mp4": {
                                "name": os.path.join(
                                    cache_subdir,
                                    album_prefix + video_name,
                                ),
                                "ok": None,
                            }
                        },
                    }
                    if len(cache_symlinks_subdirs):
                        video_type["file names"]["mp4"]["symlinks"] = []
                        for cache_symlink in cache_symlinks_subdirs:
                            video_type["file names"]["mp4"]["symlinks"].append(
                                {
                                    "name": os.path.join(
                                        cache_symlink,
                                        album_prefix + video_name,
                                    ),
                                    "ok": None,
                                }
                            )
                    video_types.append(video_type)

            # transcoded reduced video
            video_name = video_transcoded_name(self)
            video_type = {
                "type": "transcoded video",
                "size": size,
                "actual size": size,
                "file names": {
                    "mp4": {
                        "name": os.path.join(
                            cache_subdir,
                            album_prefix + video_name,
                        ),
                        "ok": None,
                    }
                },
            }
            if len(cache_symlinks_subdirs):
                video_type["file names"]["mp4"]["symlinks"] = []
                for cache_symlink in cache_symlinks_subdirs:
                    video_type["file names"]["mp4"]["symlinks"].append(
                        {
                            "name": os.path.join(
                                cache_symlink,
                                album_prefix + video_name,
                            ),
                            "ok": None,
                        }
                    )
            video_types.append(video_type)

            if Options.config["reduced_size_for_sharing"]:
                # image for sharing on social media
                sharing_name = image_cache_name(
                    self,
                    Options.config["reduced_size_for_sharing"],
                    "sq.jpg",
                )
                sharing_type = {
                    "type": "square reduction for sharing",
                    "size": Options.config["reduced_size_for_sharing"],
                    "actual size": Options.config["reduced_size_for_sharing"],
                    "file names": {
                        "jpg": {
                            "name": os.path.join(
                                cache_subdir,
                                album_prefix + sharing_name,
                            ),
                            "ok": None,
                        }
                    },
                }
                if len(cache_symlinks_subdirs):
                    sharing_type["file names"]["jpg"]["symlinks"] = []
                    for cache_symlink in cache_symlinks_subdirs:
                        sharing_type["file names"]["jpg"]["symlinks"].append(
                            {
                                "name": os.path.join(
                                    cache_symlink,
                                    album_prefix + sharing_name,
                                ),
                                "ok": None,
                            }
                        )
                square_sharing_types.append(sharing_type)

            (
                square_thumbnail_types,
                non_square_thumbnail_types,
            ) = self.generate_thumbnail_types_and_sizes(
                cache_subdir, cache_symlinks_subdirs
            )

            if self.size[0] == self.size[1]:
                # square video: the reduction for sharing and all the thumbnails are square

                square_types = SingleMedia.collect_types_from_type_groups(
                    [
                        square_sharing_types,
                        square_thumbnail_types,
                    ]
                )

                types_and_names = {
                    "os copy types": os_copy_types,
                    "video types": video_types,
                    "square types": square_types,
                }
            else:
                # non square video

                if Options.config["reduced_size_for_sharing"]:
                    sharing_name = image_cache_name(
                        self,
                        Options.config["reduced_size_for_sharing"],
                        "jpg",
                    )
                    sharing_type = {
                        "type": "reduction for sharing",
                        "size": Options.config["reduced_size_for_sharing"],
                        "actual size": Options.config["reduced_size_for_sharing"],
                        "file names": {
                            "jpg": {
                                "name": os.path.join(
                                    cache_subdir,
                                    album_prefix + sharing_name,
                                ),
                                "ok": None,
                            }
                        },
                    }
                    if len(cache_symlinks_subdirs):
                        sharing_type["file names"]["jpg"]["symlinks"] = []
                        for cache_symlink in cache_symlinks_subdirs:
                            sharing_type["file names"]["jpg"]["symlinks"].append(
                                {
                                    "name": os.path.join(
                                        cache_symlink,
                                        album_prefix + sharing_name,
                                    ),
                                    "ok": None,
                                }
                            )
                        non_square_sharing_types.append(sharing_type)

                non_square_types = SingleMedia.collect_types_from_type_groups(
                    [
                        non_square_sharing_types,
                        non_square_thumbnail_types,
                    ]
                )

                square_types = SingleMedia.collect_types_from_type_groups(
                    [
                        square_sharing_types,
                        square_thumbnail_types,
                    ]
                )

                types_and_names = {
                    "os copy types": os_copy_types,
                    "video types": video_types,
                    "square types": square_types,
                    "non square types": non_square_types,
                }
        elif self.is_image:
            full_size_types = []
            reduction_types = []

            if (
                Options.config["expose_original_media"]
                and not original_media_is_enough(self)
                or Options.config["expose_full_size_media_in_cache"]
            ):
                formats_for_full_size_images = Options.config["cache_images_formats"]

                for format in cache_images_formats:
                    image_name = image_cache_name(self, 0, format)
                    if Options.config[
                        "expose_full_size_media_in_cache"
                    ] and may_make_image_os_copy(self, format):
                        copy_type = {
                            "type": "os copy",
                            "size": size,
                            "actual size": size,
                            "file names": {
                                "copy": {
                                    "name": os.path.join(
                                        cache_subdir,
                                        album_prefix + image_name,
                                    ),
                                    "ok": None,
                                }
                            },
                        }
                        if len(cache_symlinks_subdirs):
                            copy_type["file names"]["copy"]["symlinks"] = []
                            for cache_symlink in cache_symlinks_subdirs:
                                copy_type["file names"]["copy"]["symlinks"].append(
                                    {
                                        "name": os.path.join(
                                            cache_symlink,
                                            album_prefix + image_name,
                                        ),
                                        "ok": None,
                                    }
                                )
                        os_copy_types.append(copy_type)
                        formats_for_full_size_images = [
                            format1
                            for format1 in Options.config["cache_images_formats"]
                            if format1 != format
                        ]

                by_format = {
                    "type": "full size image",
                    "size": size,
                    "actual size": size,
                    "file names": {},
                }
                for format in formats_for_full_size_images:
                    image_name = image_cache_name(self, 0, format)
                    by_format["file names"][format] = {
                        "name": os.path.join(
                            cache_subdir,
                            album_prefix + image_name,
                        ),
                        "ok": None,
                    }
                    if len(cache_symlinks_subdirs):
                        by_format["file names"][format]["symlinks"] = []
                        for cache_symlink in cache_symlinks_subdirs:
                            by_format["file names"][format]["symlinks"].append(
                                {
                                    "name": os.path.join(
                                        cache_symlink,
                                        album_prefix + image_name,
                                    ),
                                    "ok": None,
                                }
                            )
                full_size_types.append(by_format)

            # reduced sizes
            reduction_type = "reduction"
            if self.size[0] == self.size[1]:
                reduction_type = "square reduction"

            for _size in Options.config["reduced_sizes"]:
                by_format = {
                    "type": reduction_type,
                    "size": _size,
                    "actual size": _size,
                    "file names": {},
                }
                for format in cache_images_formats:
                    image_name = image_cache_name(self, _size, format)
                    by_format["file names"][format] = {
                        "name": os.path.join(
                            cache_subdir,
                            album_prefix + image_name,
                        ),
                        "ok": None,
                    }
                    if len(cache_symlinks_subdirs):
                        by_format["file names"][format]["symlinks"] = []
                        for cache_symlink in cache_symlinks_subdirs:
                            by_format["file names"][format]["symlinks"].append(
                                {
                                    "name": os.path.join(
                                        cache_symlink,
                                        album_prefix + image_name,
                                    ),
                                    "ok": None,
                                }
                            )
                if len(by_format["file names"]):
                    reduction_types.append(by_format)

            if Options.config["reduced_size_for_sharing"]:
                # image for sharing on social media
                sharing_name = image_cache_name(
                    self, Options.config["reduced_size_for_sharing"], "sq.jpg"
                )
                sharing_type = {
                    "type": "square reduction for sharing",
                    "size": Options.config["reduced_size_for_sharing"],
                    "actual size": Options.config["reduced_size_for_sharing"],
                    "file names": {
                        "jpg": {
                            "name": os.path.join(
                                cache_subdir,
                                album_prefix + sharing_name,
                            ),
                            "ok": None,
                        }
                    },
                }
                if len(cache_symlinks_subdirs):
                    sharing_type["file names"]["jpg"]["symlinks"] = []
                    for cache_symlink in cache_symlinks_subdirs:
                        square_file_to_add = os.path.join(
                            cache_symlink, square_file_to_add
                        )
                        sharing_type["file names"]["jpg"]["symlinks"].append(
                            {
                                "name": os.path.join(
                                    cache_symlink,
                                    album_prefix + sharing_name,
                                ),
                                "ok": None,
                            }
                        )
                square_sharing_types.append(sharing_type)

            (
                square_thumbnail_types,
                non_square_thumbnail_types,
            ) = self.generate_thumbnail_types_and_sizes(
                cache_subdir, cache_symlinks_subdirs
            )

            if self.size[0] == self.size[1]:
                # square image: all copies/reductions/thumbnails are square
                square_types = SingleMedia.collect_types_from_type_groups(
                    [
                        reduction_types,
                        square_sharing_types,
                        square_thumbnail_types,
                    ]
                )
                square_types = full_size_types + square_types

                types_and_names = {
                    "os copy types": os_copy_types,
                    "square types": square_types,
                }
            else:
                if Options.config["reduced_size_for_sharing"]:
                    sharing_name = image_cache_name(
                        self, Options.config["reduced_size_for_sharing"], "jpg"
                    )
                    sharing_type = {
                        "type": "reduction for sharing",
                        "size": Options.config["reduced_size_for_sharing"],
                        "actual size": Options.config["reduced_size_for_sharing"],
                        "file names": {
                            "jpg": {
                                "name": os.path.join(
                                    cache_subdir,
                                    album_prefix + sharing_name,
                                ),
                                "ok": None,
                            }
                        },
                    }
                    if len(cache_symlinks_subdirs):
                        sharing_type["file names"]["jpg"]["symlinks"] = []
                        for cache_symlink in cache_symlinks_subdirs:
                            sharing_type["file names"]["jpg"]["symlinks"].append(
                                {
                                    "name": os.path.join(
                                        cache_symlink,
                                        album_prefix + sharing_name,
                                    ),
                                    "ok": None,
                                }
                            )
                    non_square_sharing_types.append(sharing_type)

                non_square_types = SingleMedia.collect_types_from_type_groups(
                    [
                        reduction_types,
                        non_square_sharing_types,
                        non_square_thumbnail_types,
                    ]
                )
                non_square_types = full_size_types + non_square_types

                square_types = SingleMedia.collect_types_from_type_groups(
                    [
                        square_sharing_types,
                        square_thumbnail_types,
                    ]
                )

                types_and_names = {
                    "os copy types": os_copy_types,
                    "square types": square_types,
                    "non square types": non_square_types,
                }
        self.types_and_names = types_and_names
        # print()
        # pprint(self.types_and_names)
        # print()

    @staticmethod
    def is_thumbnail(thumb_type):
        _is_thumbnail = thumb_type != ""
        return _is_thumbnail

    def face_center(self, faces, image_size):
        length = len(faces)
        (x0, y0, w0, h0) = faces[0]
        if length == 1:
            # return the only face
            return (int(x0 + w0 / 2), int(y0 + h0 / 2))
        elif length == 2:
            (x1, y1, w1, h1) = faces[1]
            center0_x = int(x0 + w0 / 2)
            center0_y = int(y0 + h0 / 2)
            center1_x = int(x1 + w1 / 2)
            center1_y = int(y1 + h1 / 2)
            dist_x = max(x0, x1) + (w0 + w1) / 2 - min(x0, x1)
            dist_y = max(y0, y1) + (h0 + h1) / 2 - min(y0, y1)
            if dist_x > image_size or dist_y > image_size:
                # the faces are too far each other, choose one: return the bigger one
                if w1 > w0:
                    return (center1_x, center1_y)
                else:
                    return (center0_x, center0_y)
            else:
                return (
                    int((center1_x + center0_x) / 2),
                    int((center1_y + center0_y) / 2),
                )
        else:
            dist_x = max([x for (x, y, w, h) in faces]) - min(
                [x for (x, y, w, h) in faces]
            )
            dist_y = max([y for (x, y, w, h) in faces]) - min(
                [y for (x, y, w, h) in faces]
            )
            if dist_x < image_size and dist_y < image_size:
                # all the faces are within the square, get the mean point
                return (
                    int(np.mean([x + w / 2 for (x, y, w, h) in faces])),
                    int(np.mean([y + h / 2 for (x, y, w, h) in faces])),
                )
            else:
                # remove the farther faces and then return the agerage point of the remaining group
                distances = np.empty((length, length))
                positions = np.empty(length, dtype=object)
                max_sum_of_distances = 0
                for k1, f1 in enumerate(faces):
                    (x, y, w, h) = f1
                    x_pos = x + int(w / 2)
                    y_pos = y + int(h / 2)
                    positions[k1] = np.array([x_pos, y_pos])
                    sum_of_distances = 0
                    for k2, f2 in enumerate(faces):
                        distances[k1, k2] = math.sqrt(
                            (f1[0] - f2[0]) ** 2 + (f1[1] - f2[1]) ** 2
                        )
                        sum_of_distances += distances[k1, k2]
                    if sum_of_distances > max_sum_of_distances:
                        max_sum_of_distances = sum_of_distances
                        max_key = k1

                mean_distance = np.mean(distances)
                if max_sum_of_distances / length > 2 * mean_distance:
                    # remove the face
                    faces.pop(max_key)
                    return self.face_center(faces, image_size)
                else:
                    return np.mean(np.asarray(positions)).tolist()

    def reduce_size_and_save(
        self,
        start_image,
        original_path,
        type_group_name,
        type_and_name,
        json_files_mtime,
        is_first_square_after_non_square,
        single_format=None,
        corrected_size=None,
    ):
        info_string = "size "
        original_thumb_size = type_and_name["size"]
        if corrected_size is not None:
            actual_size = corrected_size
            info_string += str(actual_size)
        else:
            actual_size = type_and_name["actual size"]
            info_string += str(original_thumb_size)
        _type = type_and_name["type"]
        thumb_size = type_and_name["actual size"]

        _is_full_size_copy = _type == "full size image"
        _is_thumbnail = _type.endswith("thumbnail")
        _is_reduced_copy = not _is_full_size_copy and not _is_thumbnail

        start_image_width = start_image.size[0]
        start_image_height = start_image.size[1]

        if _type.find("square") > -1:
            info_string += ", square"

        must_add_actual_size = False
        if _type.find("fit") > -1:
            info_string += ", fit size"
        elif _type.find("fixed") > -1:
            info_string += ", fixed height"
            must_add_actual_size = True

        if "mobile bigger" in type_and_name and type_and_name["mobile bigger"]:
            info_string += " (mobile)"
            must_add_actual_size = True

        if must_add_actual_size:
            info_string += " -> " + str(actual_size)

        start_image_copy = SingleMedia.copy_image(start_image, info_string)
        if start_image_copy is None:
            return [None, True]

        if _type.find("square") > -1 and is_first_square_after_non_square:
            # image has to be cropped
            # if opencv is installed, the crop function will take into account the faces
            if max(start_image_width, start_image_height) > actual_size:
                start_image_copy_cropped = self.crop_image(
                    start_image_copy, actual_size, info_string
                )
            else:
                # image is square, or is smaller than the square thumbnail, don't crop it
                thumbnail_width = start_image_width
                start_image_copy_cropped = start_image_copy
        else:
            start_image_copy_cropped = start_image_copy

        # now thumbnail_width and thumbnail_height are the values the thumbnail will get,
        # and if the thumbnail isn't a square one, their ratio is the same of the original image

        # both width and height of thumbnail are less then width and height of start_image, no blurring will happen

        if (
            not self.is_audio
            and not _is_full_size_copy
            and max(start_image_copy_cropped.size) <= actual_size
        ):
            # no resizing
            # resizing to thumbnail size an image smaller than the thumbnail we must produce would return a blurred image
            if _is_reduced_copy:
                message(
                    "small image, no reduction",
                    info_string
                    + ", image is "
                    + str(self._attributes["metadata"]["size"][0])
                    + " x "
                    + str(self._attributes["metadata"]["size"][1]),
                    4,
                )
            elif _is_thumbnail and _type.startswith("album"):
                message("small image, no thumbing for album", info_string, 4)
            elif _is_thumbnail and _type.startswith("media"):
                message("small image, no thumbing for media", info_string, 4)
        else:
            # resizing
            if _is_reduced_copy:
                message("reducing PIL image size...", info_string, 6)
            elif _is_thumbnail and _type.startswith("album"):
                message("thumbing for subalbums...", "", 6)
            elif _is_thumbnail and _type.startswith("media"):
                message("thumbing for media...", "", 6)

            start_image_copy_cropped.thumbnail(
                (actual_size, actual_size), Image.LANCZOS
            )

            if _is_reduced_copy:
                indented_message(
                    "image reduced!", "size " + str(original_thumb_size), 5
                )
            elif _is_thumbnail and _type.startswith("album"):
                indented_message("image thumbed for subalbums!", info_string, 5)
            elif _is_thumbnail and _type.startswith("media"):
                indented_message(
                    "image thumbed for media!", "size " + str(original_thumb_size), 4
                )

        if (
            (_type.find("square") > -1)
            # and start_image_copy_cropped.size[0] != start_image_copy_cropped.size[1]
            and min(start_image_copy_cropped.size) < actual_size
        ):
            start_image_copy_cropped_filled = self.fill_small_image(
                start_image_copy_cropped, actual_size, _type
            )
        else:
            start_image_copy_cropped_filled = start_image_copy_cropped

        start_image_copy_for_saving = start_image_copy_cropped_filled.copy()
        if self.is_video:
            message("adding video transparency...", "", 5)
            transparency_file = os.path.join(
                os.path.dirname(__file__), "../web/img/play_button_100_62.png"
            )
            with Image.open(transparency_file) as video_transparency:
                video_transparency.load()
            x = int(
                (start_image_copy_cropped_filled.size[0] - video_transparency.size[0])
                / 2
            )
            y = int(
                (start_image_copy_cropped_filled.size[1] - video_transparency.size[1])
                / 2
            )
            start_image_copy_for_saving.paste(
                video_transparency, (x, y), video_transparency
            )
            indented_message("video transparency added", "", 4)

        start_image_copy_for_saving = start_image_copy_for_saving.convert("RGB")

        exif_for_this_reduction = None
        if Options.config["copy_exif_into_reductions"]:
            try:
                exif_for_this_reduction = self.exif_by_PIL
            except AttributeError:
                pass

        # the subdir hadn't been created when creating the album in order to avoid creation of empty directories
        absolute_thumbs_path_with_subdir = os.path.join(
            Options.config["cache_path"], self.album.subdir
        )
        make_dir(absolute_thumbs_path_with_subdir, "cache subdir")

        absolute_file_names = {}
        num_formats_worked = 0
        file_name_with_paths = {}
        if single_format is not None:
            used_formats = [single_format]
        else:
            used_formats = list(type_and_name["file names"].keys())

        for format in used_formats:
            num_formats_worked += 1
            absolute_file_name = os.path.join(
                Options.config["cache_path"],
                type_and_name["file names"][format]["name"],
            )
            absolute_file_names[format] = absolute_file_name

            if actual_size > max(Options.max_sizes[format]):
                file_name = type_and_name["file names"][format]["name"]
                error = (
                    "size "
                    + str(actual_size)
                    + " > "
                    + str(max(Options.max_sizes[format]))
                    + ", the max allowed value for "
                    + format
                    + "; reducing size to max allowed;"
                )
                message("WARNING: big size for " + format, error, 4)
                self.add_error(
                    "big_size_for_format_warnings",
                    file_name,
                    original_thumb_size,
                    max(Options.max_sizes[format]),
                    format,
                    error,
                )

                next_level()
                message(
                    "reducing and saving for size "
                    + str(max(Options.max_sizes[format])),
                    "from size " + str(max(start_image.size)),
                    4,
                )
                [_, corrected_absolute_file_names] = self.reduce_size_and_save(
                    start_image,
                    original_path,
                    type_group_name,
                    type_and_name,
                    json_files_mtime,
                    is_first_square_after_non_square,
                    format,
                    max(Options.max_sizes[format]),
                )
                indented_message(
                    "reduced and saved!",
                    "for size " + str(max(Options.max_sizes[format])),
                    4,
                )
                back_level()

                absolute_file_names[format] = corrected_absolute_file_names[format]

                if num_formats_worked < len(used_formats):
                    continue
                else:
                    return [start_image_copy_cropped, absolute_file_names]

            if self.cache_file_and_symlinks_are_ok(
                type_group_name, type_and_name, format, json_files_mtime
            ):
                self.is_valid = True
                message(_type + " OK, skipping", "", 5)
                if num_formats_worked < len(used_formats):
                    continue
                else:
                    touch_only = True
                    message("touching " + _type, "", 5)
                    file_name_with_path, err = self.save_image(
                        start_image_copy_for_saving,
                        type_and_name,
                        format,
                        info_string,
                        exif_for_this_reduction,
                        touch_only,
                    )

                    message("reading " + _type + " from disk...", "", 6)
                    with Image.open(absolute_file_name) as reduced_image:
                        reduced_image.load()

                    indented_message(
                        _type + " read from disk!",
                        str(reduced_image.size[0]) + "x" + str(reduced_image.size[1]),
                        5,
                    )
                    return [reduced_image, absolute_file_names]

            message("so " + _type + " is not OK, (re)creating it...", "", 5)

            file_name_with_path, err = self.save_image(
                start_image_copy_for_saving,
                type_and_name,
                format,
                info_string,
                exif_for_this_reduction,
            )
            file_name_with_paths[format] = file_name_with_path

            if num_formats_worked < len(used_formats):
                continue
            else:
                if not any(
                    file_name_with_path_1 is not None
                    and format_1 not in Options.config["browser_unsupported_mime_types"]
                    for format_1, file_name_with_path_1 in file_name_with_paths.items()
                ):
                    self.is_valid = False
                    self.add_error(
                        "other_errors",
                        file_name,
                        thumb_size,
                        actual_size,
                        format,
                        err,
                    )

                for format1, file_name_with_path in absolute_file_names.items():
                    if file_name_with_path is None:
                        absolute_file_names[format1] = absolute_file_names["jpg"]
                return [start_image_copy_cropped, absolute_file_names]

    def crop_image(self, start_image, current_thumb_size, info_string):
        try_shifting = False
        start_image_width = start_image.size[0]
        start_image_height = start_image.size[1]

        if Options.config["cv2_installed"]:
            # if the reduced size images were generated in a previous scanner run, start_image is the original image,
            # and detecting the faces is very very very time consuming, so resize it to an appropriate value before detecting the faces
            smaller_size = max(
                self.image_size_for_sharing,
                int(
                    Options.config["album_thumb_size"]
                    * Options.config["mobile_thumbnail_factor"]
                    * 1.5
                ),
            )
            start_image_copy_for_detecting = start_image.copy()

            width_for_detecting = start_image_width
            height_for_detecting = start_image_height
            if min(start_image_width, start_image_height) > smaller_size:
                longer_size = int(
                    smaller_size
                    * max(start_image_width, start_image_height)
                    / min(start_image_width, start_image_height)
                )
                width_for_detecting = (
                    smaller_size
                    if start_image_width < start_image_height
                    else longer_size
                )
                height_for_detecting = (
                    longer_size
                    if start_image_width < start_image_height
                    else smaller_size
                )
                sizes_change = (
                    "from "
                    + str(start_image_width)
                    + "x"
                    + str(start_image_height)
                    + " to "
                    + str(width_for_detecting)
                    + "x"
                    + str(height_for_detecting)
                )
                message("reducing size for face detection...", sizes_change, 5)
                start_image_copy_for_detecting.thumbnail(
                    (longer_size, longer_size), Image.LANCZOS
                )
                indented_message("size reduced", "", 6)

            # opencv!
            # see http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_objdetect/py_face_detection/py_face_detection.html#haar-cascade-detection-in-opencv
            try:
                opencv_image = np.array(start_image_copy_for_detecting.convert("RGB"))[
                    :, :, ::-1
                ].copy()
                gray_opencv_image = cv2.cvtColor(opencv_image, cv2.COLOR_BGR2GRAY)
            except cv2.error:
                # this happens with gif's... weird...
                pass
            else:
                try_shifting = True

                # detect faces
                message(
                    "opencv: detecting faces...",
                    "from "
                    + str(width_for_detecting)
                    + "x"
                    + str(height_for_detecting),
                    4,
                )
                # from https://docs.opencv.org/2.4/modules/objdetect/doc/cascade_classification.html:
                # detectMultiScale(image[, scaleFactor[, minNeighbors[, flags[, minSize[, maxSize]]]]])
                # - scaleFactor – Parameter specifying how much the image size is reduced at each image scale.
                # - minNeighbors – Parameter specifying how many neighbors each candidate rectangle should have to retain it.
                # - flags – Parameter with the same meaning for an old cascade as in the function cvHaarDetectObjects. It is not used for a new cascade.
                # - minSize – Minimum possible object size. Objects smaller than that are ignored.
                # - maxSize – Maximum possible object size. Objects larger than that are ignored.
                # You should read the beginning of the page in order to understand the parameters
                faces = Options.face_cascade.detectMultiScale(
                    gray_opencv_image,
                    Options.config["face_cascade_scale_factor"],
                    5,
                )
                if len(faces) and Options.config["show_faces"]:
                    img = opencv_image
                    for x, y, w, h in faces:
                        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
                        roi_gray = gray_opencv_image[y : y + h, x : x + w]
                        roi_color = img[y : y + h, x : x + w]
                        eyes = eye_cascade.detectMultiScale(roi_gray)
                        for ex, ey, ew, eh in eyes:
                            cv2.rectangle(
                                roi_color,
                                (ex, ey),
                                (ex + ew, ey + eh),
                                (0, 255, 0),
                                2,
                            )
                    cv2.imshow("img", img)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                # get the position of the center of the faces
                if len(faces):
                    next_level()  # OK
                    message("faces detected", str(len(faces)) + " faces", 4)
                    (x_center, y_center) = self.face_center(
                        faces.tolist(), current_thumb_size
                    )
                    indented_message(
                        "opencv",
                        "center: " + str(x_center) + ", " + str(y_center),
                        4,
                    )
                    back_level()  # OK
                else:
                    try_shifting = False
                    indented_message("no faces detected", "", 4)

        if min(start_image_width, start_image_height) >= current_thumb_size:
            # image is bigger than the square which will result from cropping
            if start_image_width > start_image_height:
                # wide image
                top = 0
                bottom = start_image_height
                left = int((start_image_width - start_image_height) / 2)
                right = start_image_width - left
                if Options.config["cv2_installed"] and try_shifting:
                    # maybe the position of the square could be modified so that it includes more faces
                    # center on the faces
                    shift = int(x_center - start_image_width / 2)
                    message(
                        "cropping for square",
                        "shifting horizontally by " + str(shift) + " px",
                        4,
                    )
                    left += shift
                    if left < 0:
                        left = 0
                        right = start_image_height
                    else:
                        right += shift
                        if right > start_image_width:
                            right = start_image_width
                            left = start_image_width - start_image_height
            else:
                # tall image
                left = 0
                right = start_image_width
                top = int((start_image_height - start_image_width) / 2)
                bottom = start_image_height - top
                if Options.config["cv2_installed"] and try_shifting:
                    # maybe the position of the square could be modified so that it includes more faces
                    # center on the faces
                    shift = int(y_center - start_image_height / 2)
                    message(
                        "cropping for square",
                        "shifting vertically by " + str(shift) + " px",
                        4,
                    )
                    top += shift
                    if top < 0:
                        top = 0
                        bottom = start_image_width
                    else:
                        bottom += shift
                        if bottom > start_image_height:
                            bottom = start_image_height
                            top = start_image_height - start_image_width
            thumbnail_width = current_thumb_size
        elif max(start_image_width, start_image_height) >= current_thumb_size:
            # image smallest size is smaller than the square which would result from cropping
            # cropped image will not be square
            if start_image_width > start_image_height:
                # wide image
                top = 0
                bottom = start_image_height
                left = int((start_image_width - current_thumb_size) / 2)
                right = left + current_thumb_size
                if Options.config["cv2_installed"] and try_shifting:
                    # maybe the position of the crop could be modified so that it includes more faces
                    # center on the faces
                    shift = int(x_center - start_image_width / 2)
                    message(
                        "cropping wide image",
                        "shifting horizontally by " + str(shift) + " px",
                        4,
                    )
                    left += shift
                    if left < 0:
                        left = 0
                        right = current_thumb_size
                    else:
                        right += shift
                        if right > start_image_width:
                            right = start_image_width
                            left = right - current_thumb_size
                thumbnail_width = current_thumb_size
            else:
                # tall image
                left = 0
                right = start_image_width
                top = int((start_image_height - current_thumb_size) / 2)
                bottom = top + current_thumb_size
                if Options.config["cv2_installed"] and try_shifting:
                    # maybe the position of the crop could be modified so that it includes more faces
                    # center on the faces
                    shift = int(y_center - start_image_height / 2)
                    message(
                        "cropping tall image",
                        "shifting vertically by " + str(shift) + " px",
                        4,
                    )
                    top += shift
                    if top < 0:
                        top = 0
                        bottom = current_thumb_size
                    else:
                        bottom += shift
                        if bottom > start_image_height:
                            bottom = start_image_height
                            top = bottom - current_thumb_size
                thumbnail_width = start_image_width

        message("cropping...", "", 6)
        cropped_image = start_image.crop((left, top, right, bottom))
        indented_message("cropped!", info_string, 5)

        return cropped_image

    @staticmethod
    def copy_image(start_image, info_string):
        try:
            try:
                start_image_copy = start_image.copy()
            except OSError:
                indented_message(
                    "PIL Error with the image", "maybe a truncated one?", 4
                )
                self.is_valid = False
                return None
        except KeyboardInterrupt:
            raise
        # except:
        #     # we try again to work around PIL bug
        #     message("making copy (2nd try)...", info_string, 5)
        #     start_image_copy = start_image.copy()
        #     indented_message("copy created (2nd try)", info_string, 5)

        return start_image_copy

    def fill_small_image(self, image, actual_size, _type):

        # if the crop results smaller than the required size, extend it with a background
        image_filled = image
        if (
            (_type.find("square") > -1)
            and image.size[0] != image.size[1]
            and min(image.size) < actual_size
        ):
            # it's smaller than the square we need: fill it
            message(
                "small crop: filling...",
                "background color: "
                + Options.config["small_square_crops_background_color"],
                5,
            )
            new_image = Image.new(
                "RGBA",
                (actual_size, actual_size),
                Options.config["small_square_crops_background_color"],
            )
            new_image.paste(
                image,
                (
                    int((actual_size - image.size[0]) / 2),
                    int((actual_size - image.size[1]) / 2),
                ),
            )
            image_filled = new_image
            indented_message("filled!", "", 5)
        return image_filled

    def save_image(
        self,
        image,
        type_and_name,
        format,
        info_string,
        exif_for_this_reduction,
        touch_only=False,
    ):
        _type = type_and_name["type"]
        file_name = type_and_name["file names"][format]["name"]
        file_name_with_path = os.path.join(Options.config["cache_path"], file_name)
        make_dir(os.path.dirname(file_name_with_path), "cache subdir")

        original_thumb_size = type_and_name["size"]
        info_string_with_format = (
            info_string[: len(str(original_thumb_size))]
            + ", "
            + format
            + info_string[len(str(original_thumb_size)) + 1 :]
        )

        num_times = 1
        addendum = ""
        while True:
            if touch_only:
                part_of_message = "touching"
            else:
                part_of_message = "saving"
                if num_times > 1:
                    part_of_message += " (" + str(num_times) + "° try)"
            message(part_of_message + "...", "", 6)
            try:
                if not touch_only:
                    if (
                        format == "jpg"
                        or format == "webp"
                        or Options.config["avif_supported"]
                        and format == "avif"
                        or Options.config["jxl_supported"]
                        and format == "jxl"
                        or Options.config["heic_supported"]
                        and format == "heic"
                    ):
                        if format == "jpg":
                            quality = Options.config["jpeg_quality"]
                        elif format == "webp":
                            quality = Options.config["webp_quality"]
                        elif format == "avif":
                            quality = Options.config["avif_quality"]
                        elif format == "jxl":
                            quality = Options.config["jxl_quality"]
                        elif format == "heic":
                            quality = Options.config["heic_quality"]
                        try:
                            if exif_for_this_reduction is not None:
                                image.save(
                                    file_name_with_path,
                                    quality=quality,
                                    exif=exif_for_this_reduction,
                                )
                            else:
                                image.save(
                                    file_name_with_path,
                                    quality=quality,
                                )
                        except (TypeError, AttributeError) as e:
                            # AttributeError comes from None exif value
                            image.save(file_name_with_path, quality=quality)
                        # touch_with_datetime(file_name_with_path)
                    elif format == "png":
                        try:
                            if exif_for_this_reduction is not None:
                                image.save(
                                    file_name_with_path,
                                    compress_level=Options.config["png_compress_level"],
                                    exif=exif_for_this_reduction,
                                )
                            else:
                                image.save(
                                    file_name_with_path,
                                    compress_level=Options.config["png_compress_level"],
                                )
                        except (TypeError, AttributeError) as e:
                            # AttributeError comes from None exif value
                            image.save(
                                file_name_with_path,
                                compress_level=Options.config["png_compress_level"],
                            )
                        # touch_with_datetime(file_name_with_path)
                    set_permissions_according_to_umask(file_name_with_path)

                    if "symlinks" in type_and_name["file names"][format]:
                        addendum = " with its symlinks"
                        for symlink_object in type_and_name["file names"][format][
                            "symlinks"
                        ]:
                            symlink = symlink_object["name"]
                            absolute_symlink = os.path.join(
                                Options.config["cache_path"],
                                symlink,
                            )
                            make_dir(
                                os.path.dirname(absolute_symlink), "symlink subdir"
                            )
                            if os.path.exists(absolute_symlink):
                                os.unlink(absolute_symlink)
                            os.symlink(file_name_with_path, absolute_symlink)
                            touch_with_datetime(absolute_symlink)

                touch_with_datetime(file_name_with_path)

                first_message = (
                    _type + " " + ("saved and " if not touch_only else "") + "touched"
                )
                if addendum:
                    first_message += addendum
                if hasattr(image, "exif_by_PIL") and not touch_only:
                    first_message += " with exif data"
                first_message += "!"
                indented_message(first_message, "", 5)
                break
            except KeyboardInterrupt:
                unlink_file(file_name_with_path)
                raise
            # except IOError:
            #     num_times += 1
            # except Exception as err:
            #     first_message = _type + " save failure with error: " + str(err)
            #     indented_message(
            #         first_message,
            #         str(original_thumb_size)
            #         + " -> "
            #         + os.path.basename(file_name_with_path),
            #         2,
            #     )
            #     unlink_file(file_name_with_path)
            #     file_name_with_path = ""
            #     return file_name_with_path, err

        return file_name_with_path, None

    def make_os_copy(self):

        next_level()
        message("making os copy...", "", 5)
        file_name = self.types_and_names["os copy types"][0]["file names"]["copy"][
            "name"
        ]

        # we can simply make an os copy of the original image

        absolute_file_name = os.path.join(
            Options.config["cache_path"],
            file_name,
        )

        # the subdir hadn't been created when creating the album in order to avoid creation of empty directories
        make_dir(os.path.dirname(absolute_file_name), "cache subdir")

        shutil.copy(
            self.media_path,
            absolute_file_name,
        )
        set_permissions_according_to_umask(absolute_file_name)

        indented_message("os copy made!", file_name, 4)
        back_level()
        return absolute_file_name

    def add_error(
        self, error_type, thumb_path, thumb_size, current_thumb_size, format, e
    ):
        try:
            self.errors[error_type]
        except KeyError:
            self.errors[error_type] = []

        if thumb_size == 0:
            self.errors[error_type].append(
                thumb_path + ": " + "warning"
                if error_type.endswith("warnings")
                else "WARNING: '" + str(e) + "'"
            )
        else:
            self.errors[error_type].append(
                thumb_path
                + ": error '"
                + str(e)
                + "' saving "
                + format
                + ", "
                + str(current_thumb_size)
            )

    def video_frame(
        self,
        original_path,
        json_files_mtime,
    ):
        (_, tfn) = tempfile.mkstemp()
        message("extracting frame from video...", "", 4)
        try:
            arguments = [
                "-i",
                original_path,  # original file to extract thumbs from
                "-f",
                "image2",  # extract image
                "-vsync",
                "1",  # CRF
                "-vframes",
                "1",  # extrat 1 single frame
                "-an",  # disable audio
                "-loglevel",
                "quiet",  # don't display anything
                "-y",  # don't prompt for overwrite
                tfn,  # temporary file to store extracted image
            ]
            return_code = VideoTranscodeCaller().call(*arguments)
        except KeyboardInterrupt:
            raise

        if return_code == False:
            indented_message(
                "couldn't extract frame from video", os.path.basename(original_path), 1
            )
            self.add_error(
                "other_errors",
                original_path,
                "",
                "",
                "",
                "coudn't extract video frame",
            )

            unlink_file(tfn)
            self.is_valid = False
            return None
        indented_message("frame from video extracted!", "", 4)
        try:
            with Image.open(tfn) as image:
                image.load()
        except KeyboardInterrupt:
            unlink_file(tfn)
            raise
        except:
            indented_message(
                "error opening video frame", tfn + " from " + original_path, 5
            )
            self.add_error(
                "other_errors",
                original_path,
                "",
                "",
                "",
                "error opening video frame",
            )
            self.is_valid = False
            unlink_file(tfn)
            return None
        mirror = image
        if "rotation" in self._attributes:
            if self._attributes["metadata"]["rotation"] == "90":
                mirror = image.transpose(Image.ROTATE_270)
            elif self._attributes["metadata"]["rotation"] == "180":
                mirror = image.transpose(Image.ROTATE_180)
            elif self._attributes["metadata"]["rotation"] == "270":
                mirror = image.transpose(Image.ROTATE_90)

        return mirror

    def calculate_image_size_for_sharing(self):
        if not hasattr(self, "image_size_for_sharing"):
            if Options.config["reduced_size_for_sharing"]:
                size_for_sharing = Options.config["reduced_size_for_sharing"]
            else:
                size_for_sharing = Options.config["album_thumb_size"]
            if not self.is_audio:
                width, height = self.size
                if max(width, height) < size_for_sharing:
                    size_for_sharing = max(width, height)
            self.image_size_for_sharing = size_for_sharing

    def audio_video_transcode(
        self,
        original_path,
        json_files_mtime,
        full_size,
    ):
        thumbs_path_with_subdir = self.album.subdir
        if self.first_protected_directory is not None:
            thumbs_path_with_subdir = os.path.join(
                self.first_protected_directory, thumbs_path_with_subdir
            )
        absolute_thumbs_path_with_subdir = os.path.join(
            Options.config["cache_path"], thumbs_path_with_subdir
        )
        if os.path.exists(absolute_thumbs_path_with_subdir):
            if not os.access(absolute_thumbs_path_with_subdir, os.W_OK):
                message(
                    "FATAL ERROR",
                    absolute_thumbs_path_with_subdir + " not writable, quitting",
                )
                sys.exit(1)
        else:
            make_dir(absolute_thumbs_path_with_subdir, "cache subdir")

        album_prefix = remove_folders_marker(self.album.cache_base)
        if album_prefix:
            album_prefix += Options.config["cache_folder_separator"]

        info_string = ""
        if full_size:
            transcode_path = os.path.join(
                thumbs_path_with_subdir,
                album_prefix
                + (
                    video_full_size_transcoded_name(self)
                    if self.is_video
                    else audio_full_size_transcoded_name(self)
                ),
            )
            absolute_transcode_path = os.path.join(
                Options.config["cache_path"],
                transcode_path,
            )
        else:
            info_string = (
                "mp4, h264, " + Options.config["video_transcode_bitrate"]
                if self.is_video
                else "mp3, " + Options.config["audio_transcode_bitrate"]
            ) + " bit/sec"
            if self.is_video:
                info_string += ", crf=" + str(Options.config["video_crf"])
            transcode_path = os.path.join(
                thumbs_path_with_subdir,
                album_prefix
                + (
                    video_transcoded_name(self)
                    if self.is_video
                    else audio_transcoded_name(self)
                ),
            )
            absolute_transcode_path = os.path.join(
                Options.config["cache_path"],
                transcode_path,
            )

        _type = "video" if self.is_video else "audio"
        message("checking existing transcoded " + _type + "...", "", 3)

        if full_size:
            _type = "full size " + _type
        else:
            _type = "transcoded " + _type
        if self.cache_file_and_symlinks_are_ok(
            _type,
            [
                type_and_name
                for type_and_name in (
                    self.types_and_names["video types"]
                    if self.is_video
                    else self.types_and_names["audio types"]
                )
                if type_and_name["type"] == _type
            ][0],
            ("mp4" if self.is_video else "mp3"),
            json_files_mtime,
        ):
            # indented_message("video cache hit!", "", 4)
            # self.video_metadata_by_avconv_ffmpeg(False)
            return absolute_transcode_path

        if self.is_audio:
            if full_size:
                transcode_cmd = [
                    "-i",
                    original_path,  # original file to be encoded
                    "-acodec",
                    "libmp3lame",  # codec mp3
                    "-threads",
                    str(Options.config["num_processors"]),  # number of cores to use
                    "-loglevel",
                    "quiet",  # don't display anything
                    "-y",  # don't prompt for overwrite
                ]
            else:
                transcode_cmd = [
                    "-i",
                    original_path,  # original file to be encoded
                    "-acodec",
                    "libmp3lame",  # codec mp3
                    "-strict",
                    "experimental",
                    "-ab",
                    str(Options.config["audio_transcode_bitrate"]),
                    "-f",
                    "mp3",  # fileformat mp3
                    "-threads",
                    str(Options.config["num_processors"]),  # number of cores to use
                    "-loglevel",
                    "quiet",  # don't display anything
                    "-y",  # don't prompt for overwrite
                ]
        elif self.is_video:
            if full_size:
                transcode_cmd = [
                    "-i",
                    original_path,  # original file to be encoded
                    "-c:v",
                    "libx264",  # set h264 as videocodec
                    "-f",
                    "mp4",  # fileformat mp4
                    "-threads",
                    str(Options.config["num_processors"]),  # number of cores to use
                    "-loglevel",
                    "quiet",  # don't display anything
                    "-y",  # don't prompt for overwrite
                ]
            else:
                transcode_cmd = [
                    "-i",
                    original_path,  # original file to be encoded
                    "-c:v",
                    "libx264",  # set h264 as videocodec
                    "-preset",
                    str(
                        Options.config["video_preset"]
                    ),  # set specific preset that provides a certain encoding speed to compression ratio
                    "-profile:v",
                    str(
                        Options.config["video_profile"]
                    ),  # set output to specific h264 profile
                    "-level",
                    str(
                        Options.config["video_profile_level"]
                    ),  # sets highest compatibility with target devices
                    "-crf",
                    str(Options.config["video_crf"]),  # set quality
                    "-b:v",
                    Options.config["video_transcode_bitrate"],  # set videobitrate
                    "-strict",
                    "experimental",  # allow native aac codec below
                    "-c:a",
                    "aac",  # set aac as audiocodec
                    "-ac",
                    str(Options.config["video_audio_ac"]),  # force two audiochannels
                    "-ab",
                    str(
                        Options.config["video_audio_ab"]
                    ),  # set audiobitrate to 160Kbps
                    "-maxrate",
                    str(
                        Options.config["video_maxrate"]
                    ),  # limits max rate, will degrade CRF if needed
                    "-bufsize",
                    str(
                        Options.config["video_bufsize"]
                    ),  # define how much the client should buffer
                    "-f",
                    "mp4",  # fileformat mp4
                    "-threads",
                    str(Options.config["num_processors"]),  # number of cores to use
                    "-loglevel",
                    "quiet",  # don't display anything
                    "-y",  # don't prompt for overwrite
                ]

        filters = []

        if not full_size and self.is_video:
            # Limit frame size. Default is HD 720p
            frame_maxsize = Options.config["video_frame_maxsize"]
            if frame_maxsize == "hd480":
                dim_max_size = 480
            elif frame_maxsize == "hd1080":
                dim_max_size = 1080
            else:
                dim_max_size = 720
                frame_maxsize = "hd720"
            if (
                "originalSize" in self._attributes["metadata"]
                and self._attributes["metadata"]["originalSize"][1] > dim_max_size
            ):
                transcode_cmd.append("-s")
                transcode_cmd.append(frame_maxsize)

            if len(filters):
                transcode_cmd.append("-vf")
                transcode_cmd.append(",".join(filters))

            # Add user-defined options
            if len(str(Options.config["video_add_options"])):
                transcode_cmd.append(str(Options.config["video_add_options"]))

        next_level()  # OK
        message("transcoding...", info_string, 5)
        tmp_transcode_cmd = transcode_cmd[:]
        transcode_cmd.append(absolute_transcode_path)
        # avoid ffmpeg stopping if the scanner is running interactively
        # transcode_cmd.append('< /dev/null')
        # The previous line makes the first transcoding attempt fail. I don't understand what
        # it is supposed to do and why ffmpeg would be interactive with -y option...
        # next_level()  # OK
        # message("transcoding command", transcode_cmd, 4)
        try:
            return_code = VideoTranscodeCaller().call(*transcode_cmd)
        except KeyboardInterrupt:
            raise

        method = ""
        if return_code == False:
            if self.is_audio:
                indented_message(
                    "audio transcoding failure", os.path.basename(original_path), 1
                )
                unlink_file(absolute_transcode_path)
                self.is_valid = False
                return None
            elif self.is_video:
                # add another option, try transcoding again
                # done to avoid this error;
                # x264 [error]: baseline profile doesn't support 4:2:2
                # next_level()  # OK
                message(
                    "video transcoding failure, trying yuv420p...",
                    os.path.basename(original_path),
                    3,
                )
                tmp_transcode_cmd.append("-pix_fmt")
                method = "yuv420p"
                tmp_transcode_cmd.append(method)
                tmp_transcode_cmd.append(absolute_transcode_path)
                # message("transcoding command", tmp_transcode_cmd, 4)
                try:
                    return_code = VideoTranscodeCaller().call(*tmp_transcode_cmd)
                except KeyboardInterrupt:
                    raise

                if return_code == False:
                    indented_message(
                        "video transcoding failure with " + method,
                        os.path.basename(original_path),
                        1,
                    )
                    unlink_file(absolute_transcode_path)
                    self.is_valid = False
                    return None

        indented_message(
            ("video" if self.is_video else "audio")
            + " transcoded"
            + (" with " + method if method else method)
            + "!",
            "",
            4,
        )
        back_level()  # OK
        return absolute_transcode_path

    @property
    def name(self):
        return os.path.basename(self.media_file_name)

    @property
    def checksum(self):
        return self._attributes["checksum"]

    @property
    def title(self):
        if "metadata" in self._attributes and "title" in self._attributes["metadata"]:
            return self._attributes["metadata"]["title"]
        else:
            return ""

    @property
    def description(self):
        if (
            "metadata" in self._attributes
            and "description" in self._attributes["metadata"]
        ):
            return self._attributes["metadata"]["description"]
        else:
            return ""

    @property
    def tags(self):
        if "metadata" in self._attributes and "tags" in self._attributes["metadata"]:
            return self._attributes["metadata"]["tags"]
        else:
            return ""

    @property
    def size(self):
        if self.is_audio:
            return self.image_size_for_sharing
        else:
            return self._attributes["metadata"]["size"]

    @property
    def is_image(self):
        return self.mime_type.find("image/") == 0

    @property
    def is_audio(self):
        return self.mime_type.find("audio/") == 0

    @property
    def is_video(self):
        return self.mime_type.find("video/") == 0 and self.mime_type != "video/x-ifo"

    def __str__(self):
        return self.name

    @property
    def path(self):
        return self.media_file_name

    @property
    def cache_files_for_single_media(self):

        cache_files = set()
        for type_group_name in self.types_and_names:
            for type_and_name in self.types_and_names[type_group_name]:
                for format in type_and_name["file names"]:
                    cache_files.add(type_and_name["file names"][format]["name"])
                    if "symlinks" in type_and_name["file names"][format]:
                        for symlink_object in type_and_name["file names"][format][
                            "symlinks"
                        ]:
                            cache_files.add(symlink_object["name"])

        return cache_files

    @property
    def exif_date(self):
        if hasattr(self, "_exif_date"):
            return self._exif_date
        self._exif_date = None
        if not self.is_valid:
            self._exif_date = None
        elif self.is_image:
            if "dateTimeOriginal" in self._attributes["metadata"]:
                self._exif_date = self._attributes["metadata"]["dateTimeOriginal"]
            elif "dateTime" in self._attributes["metadata"]:
                self._exif_date = self._attributes["metadata"]["dateTime"]
            else:
                self._exif_date = self.datetime_file_modified
        elif self.is_audio:
            if "dateTimeCreation" in self._attributes["metadata"]:
                self._exif_date = self._attributes["metadata"]["dateTimeCreation"]
            else:
                self._exif_date = self.datetime_file_modified
        elif self.is_video:
            if "dateTimeCreation" in self._attributes["metadata"]:
                self._exif_date = self._attributes["metadata"]["dateTimeCreation"]
            else:
                self._exif_date = self.datetime_file_modified
        return self._exif_date

    @property
    def file_date(self):
        if hasattr(self, "_file_date"):
            return self._file_date
        self._file_date = None
        if not self.is_valid:
            self._file_date = None
        self._file_date = self.datetime_file_modified
        return self._file_date

    @property
    def exif_date_string(self):
        date_str = str(self.exif_date)
        while len(date_str) < 19:
            date_str = "0" + date_str
        return date_str

    @property
    def file_date_string(self):
        date_str = str(self.file_date)
        while len(date_str) < 19:
            date_str = "0" + date_str
        return date_str

    @property
    def has_gps_data(self):
        return (
            Options.config["expose_image_positions"]
            and "latitude" in self._attributes["metadata"]
            and "longitude" in self._attributes["metadata"]
        )

    @property
    def has_date_from_metadata_or_file_name(self):
        return (
            Options.config["expose_image_dates"]
            and self.is_image
            and (
                "dateTimeOriginal" in self._attributes["metadata"]
                or "dateTime" in self._attributes["metadata"]
            )
            or self.is_video
            and "dateTimeCreation" in self._attributes["metadata"]
        )

    @property
    def latitude(self):
        return self._attributes["metadata"]["latitude"]

    @property
    def longitude(self):
        return self._attributes["metadata"]["longitude"]

    @property
    def year(self):
        year_str = str(self.exif_date.year)
        while len(year_str) < 4:
            year_str = "0" + year_str
        return year_str

    @property
    def month(self):
        return str(self.exif_date.month).zfill(2)

    @property
    def day(self):
        return str(self.exif_date.day).zfill(2)

    @property
    def country_name(self):
        return self._attributes["geoname"]["country_name"]

    @property
    def country_code(self):
        return self._attributes["geoname"]["country_code"]

    @property
    def region_name(self):
        return self._attributes["geoname"]["region_name"]

    @property
    def region_code(self):
        return self._attributes["geoname"]["region_code"]

    @property
    def place_name(self):
        return self._attributes["geoname"]["place_name"]

    @place_name.setter
    def place_name(self, value):
        self._attributes["geoname"]["place_name"] = value

    @property
    def place_code(self):
        return self._attributes["geoname"]["place_code"]

    @property
    def alt_place_name(self):
        return self._attributes["geoname"]["alt_place_name"]

    @alt_place_name.setter
    def alt_place_name(self, value):
        self._attributes["geoname"]["alt_place_name"] = value

    @property
    def year_album_path(self):
        return Options.config["by_date_string"] + "/" + self.year

    @property
    def month_album_path(self):
        return self.year_album_path + "/" + self.month

    @property
    def day_album_path(self):
        return self.month_album_path + "/" + self.day

    @property
    def country_album_path(self):
        return Options.config["by_gps_string"] + "/" + self.country_code

    @property
    def region_album_path(self):
        return self.country_album_path + "/" + self.region_code

    @property
    def place_album_path(self):
        return self.region_album_path + "/" + self.place_code

    @property
    def gps_album_path(self):
        if hasattr(self, "gps_path"):
            return self.gps_path
        else:
            return ""

    def __eq__(self, other):
        return self.album_path == other.album_path

    # def __ne__(self, other):
    # 	return not self.__eq__(other)

    def __lt__(self, other):
        try:
            if (
                Options.force_sort_name_ascending
                or Options.config["default_media_name_sort"]
                or self.exif_date == other.exif_date
            ):
                if (
                    Options.force_sort_name_ascending
                    or not Options.config["default_media_reverse_sort"]
                ):
                    return self.name < other.name
                else:
                    return self.name > other.name
            else:
                if not Options.config["default_media_reverse_sort"]:
                    return self.exif_date < other.exif_date
                else:
                    return self.exif_date > other.exif_date
        except TypeError:
            return False

    @property
    def attributes(self):
        return self._attributes

    @staticmethod
    def from_dict(
        album,
        dictionary,
        basepath,
        json_files_mtime,
        patterns_and_passwords,
        single_media_cache_hit,
    ):
        try:
            del dictionary["exifDateMax"]
            del dictionary["exifDateMin"]
        except KeyError:
            pass
        except TypeError:
            # a json file for some test version could bring here
            # single_media = SingleMedia(
            #     album, basepath, json_files_mtime, dictionary
            # )
            # single_media.is_valid = False
            return None

        media_path = os.path.join(basepath, dictionary["name"])
        if not os.path.exists(media_path):
            return None

        # del dictionary["name"]
        for key, value in list(dictionary.items()):
            if key.startswith("dateTime"):
                try:
                    dictionary[key] = datetime.strptime(
                        value, Options.timestamp_format_for_json_file
                    )
                except KeyboardInterrupt:
                    raise
                except ValueError:
                    pass
            if key == "metadata":
                for key1, value1 in list(value.items()):
                    if key1.startswith("dateTime"):
                        while True:
                            try:
                                dictionary[key][key1] = datetime.strptime(
                                    value1, Options.timestamp_format_for_json_file
                                )
                                break
                            except KeyboardInterrupt:
                                raise
                            except ValueError:
                                # year < 1000 incorrectly inserted in json file ("31" instead of "0031")
                                value1 = "0" + value1

        next_level()
        message("processing single media from cached album", dictionary["name"], 5)
        try:
            cached_single_media = SingleMedia(
                album,
                media_path,
                json_files_mtime,
                patterns_and_passwords,
                single_media_cache_hit,
                True,
                dictionary,
            )
        except KeyError:
            next_level()
            indented_message("codec attribute absent", "from cached single media", 4)
            back_level()
            cached_single_media = SingleMedia(
                album,
                media_path,
                json_files_mtime,
                patterns_and_passwords,
                single_media_cache_hit,
                True,
            )

        result = cached_single_media.check_all_cache_files(json_files_mtime)
        back_level()

        if result:
            return cached_single_media
        else:
            return False

    def to_dict(self):
        folders_album = Options.config["folders_string"]
        if self.folders:
            folders_album = os.path.join(folders_album, self.folders)

        single_media_dict = self.attributes

        try:
            del single_media_dict["metadata"]["orientation"]
        except KeyError:
            pass

        if not Options.config["expose_image_metadata"]:
            for attr in list(single_media_dict["metadata"].keys()):
                if attr not in [
                    "size",
                    "originalSize",
                    "duration",
                    "orientationText",
                    "rotation",
                    "title",
                    "description",
                    "dateTime",
                    "dateTimeOriginal",
                    "dateTimeCreation",
                    "tags",
                    "latitude",
                    "longitude",
                    "latitudeMS",
                    "longitudeMS",
                    "altitude",
                    "altitudeRef",
                ]:
                    del single_media_dict["metadata"][attr]

        if not Options.config["expose_image_dates"]:
            for attr in list(single_media_dict["metadata"].keys()):
                if attr in [
                    "dateTime",
                    "dateTimeOriginal",
                    "dateTimeCreation",
                ]:
                    del single_media_dict["metadata"][attr]

        if not Options.config["expose_image_positions"]:
            for attr in list(single_media_dict["metadata"].keys()):
                if attr in [
                    "latitude",
                    "longitude",
                    "latitudeMS",
                    "longitudeMS",
                    "altitude",
                    "altitudeRef",
                ]:
                    del single_media_dict["metadata"][attr]

        try:
            del single_media_dict["password_identifiers_set"]
        except:
            pass
        try:
            del single_media_dict["album_identifiers_set"]
        except:
            pass

        single_media_dict["name"] = self.name
        single_media_dict["cacheBase"] = self.cache_base
        if Options.config["expose_image_dates"]:
            single_media_dict["exifDate"] = self.exif_date_string
            single_media_dict["dayAlbum"] = self.day_album_path
            try:
                single_media_dict["dayAlbumCacheBase"] = self.day_album_cache_base
            except AttributeError:
                pass
        if Options.config["expose_original_media"]:
            single_media_dict["fileDate"] = self.file_date_string
        single_media_dict["fileSizes"] = self.file_sizes
        if Options.config["expose_image_positions"] and self.gps_album_path:
            single_media_dict["gpsAlbum"] = self.gps_album_path
            single_media_dict["gpsAlbumCacheBase"] = self.gps_album_cache_base
        if hasattr(self, "words"):
            single_media_dict["words"] = self.words
        if Options.config["checksum"]:
            single_media_dict["checksum"] = self.checksum

        # the following data don't belong properly to the single media, but to album, but they must be put here in order to work with date, gps and search structure
        single_media_dict["albumName"] = self.album_path[
            : len(self.album_path) - len(self.name) - 1
        ]
        single_media_dict["foldersCacheBase"] = self.album.cache_base
        single_media_dict["cacheSubdir"] = self.album.subdir
        single_media_dict["mimeType"] = self.mime_type

        if hasattr(self, "image_size_for_sharing"):
            single_media_dict["imageSizeForSharing"] = self.image_size_for_sharing

        if self.is_video or self.is_audio:
            single_media_dict["codec"] = self.codec

        return single_media_dict


class PhotoAlbumEncoder(json.JSONEncoder):
    # the _init_ function is in order to pass an argument in json.dumps
    def __init__(self, sep_pos=False, sep_media=False, type=None, **kwargs):
        super(PhotoAlbumEncoder, self).__init__(**kwargs)
        self.type = type
        self.separate_positions = sep_pos
        self.separate_media = sep_media

    def default(self, obj):
        if isinstance(obj, datetime):
            # there was the line:
            # return obj.strftime("%Y-%m-%d %H:%M:%S")
            # but strftime throws an exception in python2 if year < 1900
            date = (
                str(obj.year)
                + "-"
                + str(obj.month).zfill(2)
                + "-"
                + str(obj.day).zfill(2)
            )
            date = (
                date
                + " "
                + str(obj.hour).zfill(2)
                + ":"
                + str(obj.minute).zfill(2)
                + ":"
                + str(obj.second).zfill(2)
            )
            return date
        if isinstance(obj, Album):
            return obj.to_dict(self.separate_positions, self.separate_media)
        if isinstance(obj, SingleMedia):
            return obj.to_dict()
        if isinstance(obj, Positions):
            return obj.to_dict(self.type)
        if isinstance(obj, Size):
            return obj.to_dict()
        if isinstance(obj, Sizes):
            return obj.to_dict()
        if isinstance(obj, ImagesAudiosVideos):
            return obj.to_dict()
        if isinstance(obj, NumsProtected):
            return obj.to_dict()
        if isinstance(obj, set):
            return list(obj)
        if isinstance(obj, (IFDRational, Fraction)):
            try:
                return float(obj)
            except ZeroDivisionError:
                return float(0)
        return json.JSONEncoder.default(self, obj)


class Metadata(object):
    @staticmethod
    def set_metadata_from_album_ini(name, album_or_single_media):
        """
        Set the 'attributes' dictionary for album or media named 'name'
        with the metadata values from the ConfigParser 'album_ini'.

        The metadata than can be overloaded by values in 'album.ini' file are:
                * title: the caption of the album or media
                * description: a long description whose words can be searched.
                * date: a YYYY-MM-DD date replacing the one from EXIF
                * latitude: for when the media is not geotagged
                * longitude: for when the media is not geotagged
                * altitude: for when the media is not geotagged
                * tags: a ','-separated list of terms
        """

        attributes = album_or_single_media.attributes
        try:
            album_ini = album_or_single_media.album_ini
        except AttributeError:
            album_ini = album_or_single_media.album.album_ini

        # Initialize with album.ini defaults

        # With Python2, section names are string. As we retrieve file names as unicode,
        # we can't find them in the ConfigParser dictionary

        # Title
        if album_ini.has_section(name):
            try:
                attributes["metadata"]["title"] = album_ini.get(name, "title")
            except NoOptionError:
                pass
        elif "title" in album_ini.defaults():
            attributes["metadata"]["title"] = album_ini.get("DEFAULT", "title")

        # Description
        if album_ini.has_section(name):
            try:
                attributes["metadata"]["description"] = album_ini.get(
                    name, "description"
                )
            except NoOptionError:
                pass
        elif "description" in album_ini.defaults():
            attributes["metadata"]["description"] = album_ini.get(
                "DEFAULT", "description"
            )

        # Date
        if album_ini.has_section(name):
            try:
                attributes["metadata"]["dateTime"] = datetime.strptime(
                    album_ini.get(name, "date"), "%Y-%m-%d"
                )
            except ValueError:
                message(
                    "ERROR",
                    "Incorrect date in ["
                    + name
                    + "] in '"
                    + Options.config["metadata_filename"]
                    + "'",
                    1,
                )
            except NoOptionError:
                pass
        elif "date" in album_ini.defaults():
            try:
                attributes["metadata"]["dateTime"] = datetime.strptime(
                    album_ini.get("DEFAULT", "date"), "%Y-%m-%d"
                )
            except ValueError:
                message(
                    "ERROR",
                    "Incorrect date in [DEFAULT] in '"
                    + Options.config["metadata_filename"]
                    + "'",
                    1,
                )

        if Options.config["expose_image_positions"]:
            # Latitude and longitude
            gps_latitude = None
            gps_latitude_ref = None
            gps_longitude = None
            gps_longitude_ref = None
            gps_altitude = None
            gps_altitude_ref = None
            if album_ini.has_section(name):
                try:
                    gps_latitude = album_ini.getfloat(name, "latitude")
                    gps_latitude_ref = "N" if gps_latitude > 0.0 else "S"
                except ValueError:
                    message(
                        "ERROR",
                        "Incorrect latitude in ["
                        + name
                        + "] in '"
                        + Options.config["metadata_filename"]
                        + "'",
                        1,
                    )
                except NoOptionError:
                    pass
            elif "latitude" in album_ini.defaults():
                try:
                    gps_latitude = album_ini.getfloat("DEFAULT", "latitude")
                    gps_latitude_ref = "N" if gps_latitude > 0.0 else "S"
                except ValueError:
                    message(
                        "ERROR",
                        "Incorrect latitude in ["
                        + name
                        + "] in '"
                        + Options.config["metadata_filename"]
                        + "'",
                        1,
                    )
            if album_ini.has_section(name):
                try:
                    gps_longitude = album_ini.getfloat(name, "longitude")
                    gps_longitude_ref = "E" if gps_longitude > 0.0 else "W"
                except ValueError:
                    message(
                        "ERROR",
                        "Incorrect longitude in ["
                        + name
                        + "] in '"
                        + Options.config["metadata_filename"]
                        + "'",
                        1,
                    )
                except NoOptionError:
                    pass
            elif "longitude" in album_ini.defaults():
                try:
                    gps_longitude = album_ini.getfloat("DEFAULT", "longitude")
                    gps_longitude_ref = "E" if gps_longitude > 0.0 else "W"
                except ValueError:
                    message(
                        "ERROR",
                        "Incorrect longitude in ["
                        + name
                        + "] in '"
                        + Options.config["metadata_filename"]
                        + "'",
                        1,
                    )
            if album_ini.has_section(name):
                try:
                    gps_altitude = album_ini.getfloat(name, "altitude")
                    # gps_altitude_ref = "0" if gps_altitude > 0.0 else "1"
                except ValueError:
                    message(
                        "ERROR",
                        "Incorrect altitude in ["
                        + name
                        + "] in '"
                        + Options.config["metadata_filename"]
                        + "'",
                        1,
                    )
                except NoOptionError:
                    pass
            elif "altitude" in album_ini.defaults():
                try:
                    gps_altitude = album_ini.getfloat("DEFAULT", "altitude")
                    gps_altitude_ref = "0" if gps_altitude > 0.0 else "1"
                except ValueError:
                    message(
                        "ERROR",
                        "Incorrect altitude in ["
                        + name
                        + "] in '"
                        + Options.config["metadata_filename"]
                        + "'",
                        1,
                    )

            if (
                gps_latitude is not None
                and gps_latitude_ref is not None
                and gps_longitude is not None
                and gps_longitude_ref is not None
            ):
                attributes["metadata"]["latitude"] = gps_latitude
                attributes["metadata"]["latitudeMS"] = (
                    Metadata.convert_decimal_to_degrees_minutes_seconds(
                        gps_latitude, gps_latitude_ref
                    )
                )
                attributes["metadata"]["longitude"] = gps_longitude
                attributes["metadata"]["longitudeMS"] = (
                    Metadata.convert_decimal_to_degrees_minutes_seconds(
                        gps_longitude, gps_longitude_ref
                    )
                )
            if gps_altitude is not None:
                attributes["metadata"]["altitude"] = gps_altitude
                # attributes["metadata"]["altitude"] = abs(gps_altitude)
                # attributes["metadata"]["altitudeRef"] = gps_altitude_ref

        # Tags
        if album_ini.has_section(name):
            try:
                attributes["metadata"]["tags"] = [
                    tag
                    for tag in set(
                        [x.strip() for x in album_ini.get(name, "tags").split(",")]
                    )
                    if tag
                ]
            except NoOptionError:
                pass
        elif "tags" in album_ini.defaults():
            attributes["metadata"]["tags"] = [
                tag
                for tag in set(
                    [x.strip() for x in album_ini.get("DEFAULT", "tags").split(",")]
                )
                if tag
            ]

    @staticmethod
    def set_geoname_from_album_ini(name, attributes, album_ini):
        """
        Set the 'attributes' dictionnary for album or media named 'name'
        with the geoname values from the ConfigParser 'album_ini'.

        The geoname values that can be set from album.ini are:
                * country_name: The name of the country
                * region_name: The name of the region
                * place_name: The name of the nearest place (town or city) calculated
                from latitude/longitude getotag.
        The geonames values that are not visible to the user, like 'country_code'
        can't be changed. We only overwrite the visible values displayed to the user.

        The geonames values must be overwrittent *after* the 'metadata' values
        because the geonames are retrieved from _attributes['metadata']['latitude'] and
        _attributes['metadata']['longitude']. You can use SingleMedia.has_gps_data to
        determine if you can call this procedure.
        """
        # Country_name
        if album_ini.has_section(name):
            try:
                attributes["geoname"]["country_name"] = album_ini.get(
                    name, "country_name"
                )
            except NoOptionError:
                pass
        elif "country_name" in album_ini.defaults():
            attributes["geoname"]["country_name"] = album_ini.get(
                "DEFAULT", "country_name"
            )

        # Region_name
        if album_ini.has_section(name):
            try:
                attributes["geoname"]["region_name"] = album_ini.get(
                    name, "region_name"
                )
            except NoOptionError:
                pass
        elif "region_name" in album_ini.defaults():
            attributes["geoname"]["region_name"] = album_ini.get(
                "DEFAULT", "region_name"
            )

        # Place_name
        if album_ini.has_section(name):
            try:
                attributes["geoname"]["place_name"] = album_ini.get(name, "place_name")
            except NoOptionError:
                pass
        elif "place_name" in album_ini.defaults():
            attributes["geoname"]["place_name"] = album_ini.get("DEFAULT", "place_name")

    @staticmethod
    def convert_to_degrees_minutes_seconds(value, ref):
        """
        Helper function to convert the GPS coordinates stored in the EXIF to degrees, minutes and seconds.
        """

        # Degrees
        d = value[0]
        # Minutes
        m = value[1]
        # Seconds
        s = int(value[2] * 1000) / 1000

        result = str(d) + "º " + str(m) + "' " + str(s) + '" ' + ref

        return result

    @staticmethod
    def convert_decimal_to_degrees_minutes_seconds(value, ref):
        """
        Helper function to convert the GPS coordinates stored in the EXIF to degrees, minutes and seconds.
        """

        if value < 0:
            value = -value

        # Degrees
        d = int(value)
        # Minutes
        m = int((value - d) * 60)
        # Seconds
        s = int((value - d - m / 60) * 3600 * 100) / 100

        result = str(d) + "º " + str(m) + "' " + str(s) + '" ' + ref

        return result

    @staticmethod
    def convert_to_degrees_decimal(value, ref):
        """
        Helper function to convert the GPS coordinates stored in the EXIF to degrees in float format.
        """

        # Degrees
        d = float(value[0])
        # Minutes
        m = float(value[1])
        # Seconds
        s = float(value[2])

        result = d + (m / 60) + (s / 3600)

        # limit decimal digits to what is needed by openstreetmap
        six_zeros = 1000000
        result = int(result * six_zeros) / six_zeros
        if ref == "S" or ref == "W":
            result = -result

        return result

    @staticmethod
    def convert_tuple_to_degrees_decimal(value, ref):
        """
        Helper function to convert the GPS coordinates stored in the EXIF to degrees in float format.
        """

        # Degrees
        d = value[0][0] / value[0][1]
        # Minutes
        m = value[1][0] / value[1][1]
        # Seconds
        s = value[2][0] / value[2][1]

        result = d + m / 60 + s / 3600

        # limit decimal digits to what is needed by openstreetmap
        six_zeros = 1000000
        result = int(result * six_zeros) / six_zeros
        if ref == "S" or ref == "W":
            result = -result

        return result

    @staticmethod
    def convert_array_degrees_minutes_seconds_to_degrees_decimal(array_string):
        # the argument is like u'[44, 25, 26495533/1000000]'

        array = array_string[1:-1].split(", ")
        d = int(array[0])
        m = int(array[1])
        s = eval(array[2])
        result = d + m / 60 + s / 3600

        return result
